/* Rename MS SQL server (will be docker container ID by default) to `mac`*/
/* The old name will cause some issues when connecting to SSMS in parallels VM */
/* For the connection to work properly, you should have a host in the VM mapped to `mac` */
DECLARE @var1 nvarchar(50)
SET @var1 = convert(nvarchar(50),@@SERVERNAME)
EXEC sp_dropserver @var1
EXEC sp_addserver 'mac', 'local'
GO
