#!/bin/bash

# Wait for MS SQL Server to start up
sleep 10s 

echo $'\nInitializing Database...\n'

# Execute all *.sql files in `mssql` dir in alphabetical order
for file in /$WKDIR/*.sql; do
  echo $file
  /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -i $file 
done
