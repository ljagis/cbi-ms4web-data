USE [cbi_ms4web]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([Id], [Href], [Username], [Email], [GivenName], [MiddleName], [Surname], [Status], [CreatedAt], [ModifiedAt], [OktaId]) VALUES (1, N'https://dev-523618.oktapreview.com/api/v1/users/00uignd41gSF0TG9F0h7', N'gisdev@lja.com', N'gisdev@lja.com', N'GIS', NULL, N'Developer', N'ENABLED', CAST(N'2018-12-19T16:48:32.0000000' AS DateTime2), CAST(N'2018-12-19T16:48:33.0000000' AS DateTime2), N'00uignd41gSF0TG9F0h7')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
INSERT [dbo].[Customers] ([Id], [FullName], [StateAbbr], [MapServiceUrl], [LogoFileName], [LogoThumbnailName], [Plan], [DefaultRoute], [DateAdded], [Features], [HiddenFields], [UnlimitedCustomFormTemplates], [UnlimitedAssets], [CountryCode], [AerialProvider]) VALUES (N'DEV', N'Dev Customer', N'TX', NULL, NULL, NULL, N'premium', NULL, CAST(N'2018-12-19T16:44:22.027' AS DateTime), N'{"calendar":true,"fileGallery":true,"bmps":true,"reports":true,"constructionSites":true,"outfalls":true,"structuralControls":true,"facilities":true,"illicitDischarges":true,"citizenReports":true,"extraLayers":true,"customFormTemplates":true,"exporting":true,"customFields":true}', '{ "ConstructionSite": [], "Outfall": [], "Structure": [], "Facility": [], "IllicitDischarge": [], "CitizenReport": [], "Inspection": [] }', 1, 1, N'US', N'MAPBOX')
GO
SET IDENTITY_INSERT [dbo].[UsersCustomers] ON 
GO
INSERT [dbo].[UsersCustomers] ([Id], [UserId], [CustomerId], [Role]) VALUES (1, 1, N'DEV', N'super')
GO
SET IDENTITY_INSERT [dbo].[UsersCustomers] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomFormTemplates] ON 
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (1, N'DEV', N'ConstructionSiteInspection', N'Default Construction Site Inspection', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (2, N'DEV', N'FacilityInspection', N'Default Facility Inspection', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (3, N'DEV', N'OutfallInspection', N'Default Outfall Inspection', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (4, N'DEV', N'StructureInspection', N'Default Structure Inspection', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (5, N'DEV', N'CitizenReport', N'Default Citizen Report', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (6, N'DEV', N'IllicitDischarge', N'Default Illicit Discharge', 0, 1, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (7, N'DEV', N'ConstructionSiteInspection', N'Sample Follow Up / Corrective Action Form', 0, 0, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (8, N'DEV', N'FacilityInspection', N'Sample Follow Up / Corrective Action Form', 0, 0, 0)
GO
INSERT [dbo].[CustomFormTemplates] ([Id], [CustomerId], [EntityType], [Name], [IsDeleted], [IsSystem], [IsArchived]) VALUES (9, N'DEV', N'StructureInspection', N'Sample Follow Up / Corrective Action Form', 0, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[CustomFormTemplates] OFF
GO
SET IDENTITY_INSERT [dbo].[Facilities] ON 
GO
INSERT [dbo].[Facilities] ([Id], [GlobalId], [CustomerId], [Name], [Geometry], [ComplianceStatus], [DateAdded], [IsDeleted], [PhysicalAddress1], [PhysicalAddress2], [PhysicalCity], [PhysicalState], [PhysicalZip], [AdditionalInformation], [Sector], [SICCode], [IsHighRisk], [InspectionFrequencyId], [YearBuilt], [SquareFootage], [CommunityId], [WatershedId], [SubwatershedId], [ReceivingWatersId], [LatestInspectionDate], [OriginalId], [TrackingId]) VALUES (1, N'8f7e7241-e834-4a5b-b9fb-1b43d3ad070b', N'DEV', N'Sample Facility', 0xE6100000010CD04543C6A3D757C065726A6798C23D40, N'Compliant', CAST(N'2018-12-19T16:44:25.400' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Facilities] OFF
GO
SET IDENTITY_INSERT [dbo].[Outfalls] ON 
GO
INSERT [dbo].[Outfalls] ([Id], [GlobalId], [CustomerId], [Location], [NearAddress], [Geometry], [ComplianceStatus], [DateAdded], [IsDeleted], [TrackingId], [Condition], [Material], [Obstruction], [OutfallType], [OutfallSizeIn], [OutfallWidthIn], [OutfallHeightIn], [AdditionalInformation], [CommunityId], [WatershedId], [SubwatershedId], [ReceivingWatersId], [LatestInspectionDate], [OriginalId]) VALUES (1, N'c099a3a1-df80-41de-87c8-cedb4df33ca1', N'DEV', N'Sample Outfall', NULL, 0xE6100000010CD04543C6A3D757C065726A6798C23D40, N'Compliant', CAST(N'2018-12-19T16:44:25.447' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Outfalls] OFF
GO
SET IDENTITY_INSERT [dbo].[Structures] ON 
GO
INSERT [dbo].[Structures] ([Id], [GlobalId], [CustomerId], [Geometry], [TrackingId], [Name], [ControlType], [ComplianceStatus], [DateAdded], [IsDeleted], [ProjectId], [ProjectType], [PhysicalAddress1], [PhysicalAddress2], [PhysicalCity], [PhysicalState], [PhysicalZip], [PermitStatus], [InspectionFrequencyId], [MaintenanceAgreement], [CommunityId], [WatershedId], [SubwatershedId], [ReceivingWatersId], [AdditionalInformation], [LatestInspectionDate], [OriginalId]) VALUES (1, N'007bf9d9-0e30-44a0-b66d-b753055e97f1', N'DEV', 0xE6100000010CDAAD65321CD757C09CF9D51C20C43D40, NULL, N'Sample Structural Control', NULL, N'Compliant', CAST(N'2018-12-19T16:44:25.430' AS DateTime), 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Structures] OFF
GO
SET IDENTITY_INSERT [dbo].[BmpControlMeasures] ON 
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (1, N'Public Education and Outreach', N'DEV', 0, NULL, NULL)
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (2, N'Public Participation/ Involvement', N'DEV', 0, NULL, NULL)
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (3, N'Illicit Discharge Detection and Elimination', N'DEV', 0, NULL, NULL)
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (4, N'Construction Site Runoff Control', N'DEV', 0, NULL, NULL)
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (5, N'Post-Construction Runoff Control', N'DEV', 0, NULL, NULL)
GO
INSERT [dbo].[BmpControlMeasures] ([Id], [Name], [CustomerId], [IsDeleted], [OriginalId], [Sort]) VALUES (6, N'Pollution Prevention/Good Housekeeping', N'DEV', 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[BmpControlMeasures] OFF
GO
SET IDENTITY_INSERT [dbo].[ConstructionSites] ON 
GO
INSERT [dbo].[ConstructionSites] ([Id], [GlobalId], [CustomerId], [TrackingId], [Name], [PhysicalAddress1], [PhysicalAddress2], [PhysicalCity], [PhysicalState], [PhysicalZip], [ProjectId], [ProjectType], [PermitStatus], [ProjectArea], [DisturbedArea], [StartDate], [EstimatedCompletionDate], [CompletionDate], [ProjectStatus], [CommunityId], [WatershedId], [SubwatershedId], [ReceivingWatersId], [NPDES], [USACE404], [State401], [USACENWP], [AdditionalInformation], [Geometry], [ComplianceStatus], [DateAdded], [LatestInspectionDate], [IsDeleted], [OriginalId]) VALUES (1, N'e1ae5636-ad96-4a40-9ba1-8ebf5806846a', N'DEV', NULL, N'Sample Construction Site', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0xE6100000010CDD955D30B8D657C0261B0FB6D8C13D40, N'Compliant', CAST(N'2018-12-19T16:44:25.233' AS DateTime), NULL, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[ConstructionSites] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomFields] ON 
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (1, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'Date', N'date', N'Date Resolved', NULL, 1, 0, 1, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (2, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Is Site Active', NULL, 2, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (3, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Is Site Permitted', NULL, 3, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (4, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Erosion Controls', NULL, 4, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (5, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Local Controls', NULL, 5, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (6, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Has Plan On Site', NULL, 6, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (7, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Non-stormwater Controls', NULL, 7, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (8, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Outfall Velocity Controls', NULL, 8, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (9, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Stabilization Controls', NULL, 9, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (10, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Structural Controls', NULL, 10, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (11, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Tracking Controls', NULL, 11, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (12, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Maintenance of Controls', NULL, 12, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (13, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Acceptable Waste Management', NULL, 13, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (14, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Has Current Plan Records', NULL, 14, 0, 1, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (15, N'DEV', N'Customer', NULL, N'FacilityInspection', N'Date', N'date', N'Date Resolved', NULL, 1, 0, 2, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (16, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Facility Active', NULL, 2, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (17, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Facility Permitted', NULL, 3, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (18, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Erosion Present', NULL, 4, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (19, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Downstream Erosion Present', NULL, 5, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (20, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Floatables Present', NULL, 6, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (21, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Illicit Discharges Present', NULL, 7, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (22, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Non-Stormwater Disharges Present', NULL, 8, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (23, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Washouts Present', NULL, 9, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (24, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Records Current', NULL, 10, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (25, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Are Sampling Data Evaluation Acceptable', NULL, 11, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (26, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is BMP Acceptable', NULL, 12, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (27, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Good House Keeping', NULL, 13, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (28, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is Return Inspection Required', NULL, 14, 0, 2, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (29, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Is SWPP On Site', NULL, 15, 0, 2, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (30, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Dry or Wet Weather', N'Dry|Wet', 1, 0, 3, NULL, N'Dry')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (31, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Days Since Last Rain', NULL, 2, 0, 3, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (32, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Color', N'Clear|Light Gray|Brown|Tan|Dark Grey', 3, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (33, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Clarity', N'Clear|Transparent|Murky|Opaque', 4, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (34, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Odor', N'Sewer|Sulfur|Chemical|Rotten Eggs|Unknown', 5, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (35, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Foam', N'Light|Medium|Heavy', 6, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (36, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Sheen', N'Light|Medium|Heavy', 7, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (37, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Suspended Solids', N'Light|Medium|Heavy', 8, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (38, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Settled Solids', N'Light|Medium|Heavy', 9, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (39, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'select', N'Floating Solids', N'Light|Medium|Heavy', 10, 0, 3, N'1. Visual Observations', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (40, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'text', N'PH', NULL, 11, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (41, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Temperature(F)', NULL, 12, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (42, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'DO (mg/L)', NULL, 13, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (43, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Turbidity (NTU)', NULL, 14, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (44, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Cond (mOhms)', NULL, 15, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (45, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'DO (%Sat)', NULL, 16, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (46, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Flowrate (GPM)', NULL, 17, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (47, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Copper (mg/L)', NULL, 18, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (48, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Phenols (mg/L)', NULL, 19, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (49, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Ammonia (mg/L)', NULL, 20, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (50, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Detergents (mg/L)', NULL, 21, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (51, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'T.PO4 (mg/L)', NULL, 22, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (52, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Cl2 (mg/L)', NULL, 23, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (53, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'BOD (mg/L)', NULL, 24, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (54, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'COD (mg/L)', NULL, 25, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (55, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'TSS (mg/L)', NULL, 26, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (56, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'NO3 (mg/L)', NULL, 27, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (57, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'Fecal Coliform (col/100mL)', NULL, 28, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (58, N'DEV', N'Customer', NULL, N'OutfallInspection', N'number', N'text', N'E. Coli (col/100mL)', NULL, 29, 0, 3, N'2. Test Results', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (59, N'DEV', N'Customer', NULL, N'OutfallInspection', N'string', N'textarea', N'Discharge Description', NULL, 30, 0, 3, N'3. Discharge Description', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (60, N'DEV', N'Customer', NULL, N'StructureInspection', N'Date', N'date', N'Date Resolved', NULL, 1, 0, 4, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (61, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Control Active', NULL, 2, 0, 4, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (62, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Are Washouts Present', NULL, 3, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (63, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Removal of Floatables Required', NULL, 4, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (64, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Built within Specification', NULL, 5, 0, 4, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (65, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Depth of Sediment Acceptable', NULL, 6, 0, 4, NULL, N'true')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (66, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Downstream Erosion Present', NULL, 7, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (67, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Erosion Present', NULL, 8, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (68, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Illict Discharge Present', NULL, 9, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (69, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Outlet Clogged', NULL, 10, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (70, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Return Inspection Recommended', NULL, 11, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (71, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Is Standing Water Present', NULL, 12, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (72, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Has Structural Damage', NULL, 13, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (73, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Requires Maintenance', NULL, 14, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (74, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Requires Repairs', NULL, 15, 0, 4, NULL, N'false')
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (75, N'DEV', N'Customer', NULL, N'CitizenReport', N'string', N'text', N'Phone Number', NULL, 1, 0, 5, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (76, N'DEV', N'Customer', NULL, N'CitizenReport', N'string', N'text', N'Cell Number', NULL, 2, 0, 5, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (77, N'DEV', N'Customer', NULL, N'CitizenReport', N'string', N'text', N'Fax Number', NULL, 3, 0, 5, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (78, N'DEV', N'Customer', NULL, N'CitizenReport', N'Date', N'date', N'Response Date', NULL, 4, 0, 5, N'2. Response', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (79, N'DEV', N'Customer', NULL, N'CitizenReport', N'string', N'textarea', N'Response', NULL, 5, 0, 5, N'2. Response', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (80, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'Date', N'date', N'Date Eliminated', NULL, 1, 0, 6, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (81, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'string', N'text', N'Phone Number', NULL, 2, 0, 6, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (82, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'string', N'text', N'Cell Number', NULL, 3, 0, 6, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (83, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'string', N'text', N'Fax Number', NULL, 4, 0, 6, N'1. Contact Information', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (84, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'string', N'textarea', N'Conversation', NULL, 5, 0, 6, N'2. Conversation', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (85, N'DEV', N'Customer', NULL, N'IllicitDischarge', N'string', N'textarea', N'Corrective Action', NULL, 6, 0, 6, N'3. Corrective Action', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (86, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'boolean', N'checkbox', N'Have the deficiencies from the previous inspection been corrected?', NULL, 1, 0, 7, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (87, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'string', N'textarea', N'If no, explain:', NULL, 2, 0, 7, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (88, N'DEV', N'Customer', NULL, N'ConstructionSiteInspection', N'section', N'section', N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL, 3, 0, 7, N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (89, N'DEV', N'Customer', NULL, N'FacilityInspection', N'boolean', N'checkbox', N'Have the deficiencies from the previous inspection been corrected?', NULL, 1, 0, 8, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (90, N'DEV', N'Customer', NULL, N'FacilityInspection', N'string', N'textarea', N'If no, explain:', NULL, 2, 0, 8, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (91, N'DEV', N'Customer', NULL, N'FacilityInspection', N'section', N'section', N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL, 3, 0, 8, N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (92, N'DEV', N'Customer', NULL, N'StructureInspection', N'boolean', N'checkbox', N'Have the deficiencies from the previous inspection been corrected?', NULL, 1, 0, 9, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (93, N'DEV', N'Customer', NULL, N'StructureInspection', N'string', N'textarea', N'If no, explain:', NULL, 2, 0, 9, NULL, NULL)
GO
INSERT [dbo].[CustomFields] ([Id], [CustomerId], [CustomFieldType], [StateAbbr], [EntityType], [DataType], [InputType], [FieldLabel], [FieldOptions], [FieldOrder], [IsDeleted], [CustomFormTemplateId], [SectionName], [DefaultValue]) VALUES (94, N'DEV', N'Customer', NULL, N'StructureInspection', N'section', N'section', N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL, 3, 0, 9, N'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.', NULL)
GO
SET IDENTITY_INSERT [dbo].[CustomFields] OFF
GO
SET IDENTITY_INSERT [dbo].[InspectionType] ON 
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (1, N'DEV', N'Default Construction Site Inspection', NULL, N'ConstructionSiteInspection', 1, 0, 0)
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (2, N'DEV', N'Default Facility Inspection', NULL, N'FacilityInspection', 2, 0, 0)
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (3, N'DEV', N'Default Outfall Inspection', NULL, N'OutfallInspection', 3, 0, 0)
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (4, N'DEV', N'Default Structure Inspection', NULL, N'StructureInspection', 4, 0, 0)
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (5, N'DEV', N'Default Citizen Report', NULL, N'CitizenReport', 5, 0, 0)
GO
INSERT [dbo].[InspectionType] ([Id], [CustomerId], [Name], [Sort], [EntityType], [CustomFormTemplateId], [IsDeleted], [IsArchived]) VALUES (6, N'DEV', N'Default Illicit Discharge', NULL, N'IllicitDischarge', 6, 0, 0)
GO
SET IDENTITY_INSERT [dbo].[InspectionType] OFF
GO
SET IDENTITY_INSERT [dbo].[ComplianceStatuses] ON 
GO
INSERT [dbo].[ComplianceStatuses] ([Id], [Name], [Level], [EntityType], [CustomerIds], [ExcludeCustomerIds]) VALUES (1, N'Compliant', 1, N'Facility', N'[]', N'[]')
GO
INSERT [dbo].[ComplianceStatuses] ([Id], [Name], [Level], [EntityType], [CustomerIds], [ExcludeCustomerIds]) VALUES (2, N'Non-compliant', 3, N'Facility', N'[]', N'[]')
GO
INSERT [dbo].[ComplianceStatuses] ([Id], [Name], [Level], [EntityType], [CustomerIds], [ExcludeCustomerIds]) VALUES (3, N'Unsatisfactory', 3, N'Facility', N'[]', N'[]')
GO
INSERT [dbo].[ComplianceStatuses] ([Id], [Name], [Level], [EntityType], [CustomerIds], [ExcludeCustomerIds]) VALUES (4, N'Marginal', 2, N'Facility', N'[]', N'[]')
GO
SET IDENTITY_INSERT [dbo].[ComplianceStatuses] OFF
GO
