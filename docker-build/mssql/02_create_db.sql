/* Directly from production MS4web running Tasks > Generate Scripts */
/* Only changed `FILENAME` in lines 10 and 12 to match local file system */

USE [master]
GO
/****** Object:  Database [cbi_ms4web]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE DATABASE [cbi_ms4web]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'cbi_ms4web', FILENAME = N'/var/opt/mssql/data/cbi_ms4web.mdf' , SIZE = 563200KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'cbi_ms4web_log', FILENAME = N'/var/opt/mssql/data/cbi_ms4web_log.ldf' , SIZE = 1743744KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [cbi_ms4web] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cbi_ms4web].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cbi_ms4web] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [cbi_ms4web] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [cbi_ms4web] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [cbi_ms4web] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [cbi_ms4web] SET ARITHABORT OFF 
GO
ALTER DATABASE [cbi_ms4web] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [cbi_ms4web] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [cbi_ms4web] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [cbi_ms4web] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [cbi_ms4web] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [cbi_ms4web] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [cbi_ms4web] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [cbi_ms4web] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [cbi_ms4web] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [cbi_ms4web] SET  DISABLE_BROKER 
GO
ALTER DATABASE [cbi_ms4web] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [cbi_ms4web] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [cbi_ms4web] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [cbi_ms4web] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [cbi_ms4web] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [cbi_ms4web] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [cbi_ms4web] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [cbi_ms4web] SET RECOVERY FULL 
GO
ALTER DATABASE [cbi_ms4web] SET  MULTI_USER 
GO
ALTER DATABASE [cbi_ms4web] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [cbi_ms4web] SET DB_CHAINING OFF 
GO
ALTER DATABASE [cbi_ms4web] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [cbi_ms4web] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [cbi_ms4web] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [cbi_ms4web] SET QUERY_STORE = OFF
GO
USE [cbi_ms4web]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [cbi_ms4web]
GO
/****** Object:  User [ms4web]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE USER [ms4web] FOR LOGIN [ms4web] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [gisweb]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE USER [gisweb] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [BUILTIN\Administrators]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE USER [BUILTIN\Administrators] FOR LOGIN [BUILTIN\Administrators] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [ms4web]
GO
ALTER ROLE [db_datareader] ADD MEMBER [ms4web]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [ms4web]
GO
ALTER ROLE [db_datareader] ADD MEMBER [gisweb]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [gisweb]
GO
ALTER ROLE [db_owner] ADD MEMBER [BUILTIN\Administrators]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[Id] [int] NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[ForeignGlobalId] [uniqueidentifier] NOT NULL,
	[ChangeType] [nchar](10) NOT NULL,
	[Date] [datetime] NOT NULL,
	[UserId] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BmpActivities]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BmpActivities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[DateAdded] [datetime2](7) NULL,
	[CompletedDate] [datetime2](7) NULL,
	[DueDate] [datetime2](7) NULL,
	[Comments] [nvarchar](3000) NULL,
	[Description] [nvarchar](3000) NULL,
	[IsCompleted] [bit] NULL,
	[Title] [nvarchar](500) NULL,
	[IsDeleted] [bit] NOT NULL,
	[TaskId] [int] NULL,
 CONSTRAINT [PK_Bmps] PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_Bmps] UNIQUE NONCLUSTERED
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BMPActivityLogs]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BMPActivityLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BmpDetailId] [int] NOT NULL,
	[DateAdded] [datetime] NULL,
	[ActivityDate] [datetime] NOT NULL,
	[DataType] [nvarchar](800) NULL,
	[Quantity] [numeric](18, 2) NULL,
	[Comments] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BmpControlMeasures]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BmpControlMeasures](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[OriginalId] [int] NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_BmpControlMeasures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BMPDataTypes]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BMPDataTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [nvarchar](800) NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BMPDetails]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BMPDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NULL,
	[ControlMeasureId] [int] NOT NULL,
	[Name] [nvarchar](1000) NULL,
	[Description] [nvarchar](max) NULL,
	[DateAdded] [datetime] NULL,
	[DueDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BmpTasks]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BmpTasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[ControlMeasureId] [int] NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_BmpTasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CitizenReports]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CitizenReports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Geometry] [geometry] NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime2](7) NOT NULL,
	[DateReported] [datetime2](7) NOT NULL,
	[CitizenName] [nvarchar](255) NULL,
	[PhysicalAddress1] [nvarchar](255) NULL,
	[PhysicalAddress2] [nvarchar](50) NULL,
	[PhysicalCity] [nvarchar](255) NULL,
	[PhysicalState] [nvarchar](50) NULL,
	[PhysicalZip] [nvarchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[CellNumber] [varchar](50) NULL,
	[FaxNumber] [varchar](50) NULL,
	[Report] [nvarchar](max) NULL,
	[ResponseDate] [datetime2](7) NULL,
	[Response] [nvarchar](max) NULL,
	[InvestigatorId] [int] NULL,
	[Investigator2Id] [int] NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[ConstructionSiteId] [int] NULL,
	[FollowUpDate] [datetime] NULL,
 CONSTRAINT [PK_CitizenReports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comments]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [int] NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[FGlobalId] [uniqueidentifier] NOT NULL,
	[Comment] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Communities]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Communities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_Communities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ComplianceStatuses]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComplianceStatuses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerIds] [nvarchar](4000) NOT NULL,
	[ExcludeCustomerIds] [nvarchar](4000) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Level] [int] NOT NULL,
	[EntityType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ComplianceStatuses] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConstructionSiteInspections]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConstructionSiteInspections](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ConstructionSiteId] [int] NOT NULL,
	[InspectionDate] [datetime2](7) NULL,
	[TimeIn] [datetime2](7) NULL,
	[TimeOut] [datetime2](7) NULL,
	[ScheduledInspectionDate] [datetime2](7) NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[InspectorId] [int] NULL,
	[InspectorId2] [int] NULL,
	[FollowUpInspectionId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateResolved] [datetime2](7) NULL,
	[IsSiteActive] [bit] NULL,
	[IsPlanOnSite] [bit] NULL,
	[IsSitePermitted] [bit] NULL,
	[ArePlanRecordsCurrent] [bit] NULL,
	[AreErosionControlsAcceptable] [bit] NULL,
	[AreStructuralControlsAcceptable] [bit] NULL,
	[IsWasteManagementAcceptable] [bit] NULL,
	[IsMaintenanceOfControlsAcceptable] [bit] NULL,
	[AreLocalControlsAcceptable] [bit] NULL,
	[AreStabilizationControlsAcceptable] [bit] NULL,
	[AreTrackingControlsAcceptable] [bit] NULL,
	[AreOutfallVelocityControlsAcceptable] [bit] NULL,
	[AreNonStormWaterControlsAcceptable] [bit] NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[WeatherCondition] [nvarchar](255) NULL,
	[WeatherTemperatureF] [numeric](18, 2) NULL,
	[WeatherPrecipitationIn] [numeric](18, 2) NULL,
	[WeatherLast72] [numeric](18, 3) NULL,
	[WeatherLast24] [numeric](18, 3) NULL,
	[WindDirection] [varchar](50) NULL,
	[WindSpeedMph] [numeric](18, 2) NULL,
	[WeatherStationId] [varchar](50) NULL,
	[WeatherDateTime] [datetime2](7) NULL,
	[InspectionType] [varchar](255) NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[Geometry] [geometry] NULL,
	[GeometryError] [int] NULL,
 CONSTRAINT [PK_ConstructionSiteInspections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConstructionSites]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConstructionSites](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[TrackingId] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[PhysicalAddress1] [nvarchar](255) NULL,
	[PhysicalAddress2] [nvarchar](50) NULL,
	[PhysicalCity] [nvarchar](255) NULL,
	[PhysicalState] [nvarchar](50) NULL,
	[PhysicalZip] [nvarchar](50) NULL,
	[ProjectId] [int] NULL,
	[ProjectType] [varchar](50) NULL,
	[PermitStatus] [varchar](50) NULL,
	[ProjectArea] [numeric](18, 2) NULL,
	[DisturbedArea] [numeric](18, 2) NULL,
	[StartDate] [datetime] NULL,
	[EstimatedCompletionDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[ProjectStatus] [varchar](50) NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[NPDES] [bit] NULL,
	[USACE404] [bit] NULL,
	[State401] [bit] NULL,
	[USACENWP] [bit] NULL,
	[AdditionalInformation] [nvarchar](3000) NULL,
	[Geometry] [geometry] NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[LatestInspectionId] [int] NULL,
	[LatestInspectionDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_ConstructionSites] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[ContactType] [varchar](50) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Phone] [varchar](50) NULL,
	[MobilePhone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[Email] [nvarchar](255) NULL,
	[MailingAddressLine1] [nvarchar](255) NULL,
	[MailingAddressLine2] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](50) NULL,
	[MailingState] [varchar](50) NULL,
	[MailingZip] [varchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
	[Company] [nvarchar](255) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Countries]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Code] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [varchar](50) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[StateAbbr] [varchar](10) NULL,
	[MapServiceUrl] [nvarchar](1024) NULL,
	[LogoFileName] [nvarchar](255) NULL,
	[LogoThumbnailName] [nvarchar](255) NULL,
	[Plan] [nvarchar](255) NULL,
	[DefaultRoute] [nvarchar](20) NULL,
	[Timezone] [nvarchar](20) NULL,
	[DateAdded] [datetime] NOT NULL,
	[Features] [nvarchar](4000) NOT NULL,
	[HiddenFields] [nvarchar](4000) NOT NULL,
	[UnlimitedCustomFormTemplates] [bit] NOT NULL,
	[UnlimitedAssets] [bit] NOT NULL,
	[CountryCode] [varchar](10) NOT NULL,
	[AerialProvider] [varchar](10) NOT NULL,
	[DailyInspectorEmail] [bit] NOT NULL DEFAULT 1,
	[MondayInspectorEmail] [bit] NOT NULL DEFAULT 1,
	[MondayAdminEmail] [bit] NOT NULL DEFAULT 1,
	[SwmpEnabled] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomFields]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomFields](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[CustomFieldType] [varchar](50) NOT NULL,
	[StateAbbr] [varchar](10) NULL,
	[EntityType] [varchar](50) NOT NULL,
	[DataType] [varchar](50) NULL,
	[InputType] [varchar](50) NOT NULL,
	[FieldLabel] [nvarchar](max) NULL,
	[FieldOptions] [nvarchar](max) NULL,
	[FieldOrder] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CustomFormTemplateId] [int] NULL,
	[MaxLengthEnabled] [bit] NOT NULL DEFAULT 0,
	[MaxLength] [int] NOT NULL DEFAULT 150,
	[SectionName] [nvarchar](2000) NULL,
	[SectionSubtitle] [nvarchar](2000) NULL,
	[DefaultValue] [nvarchar](max) NULL,
	[IsRequired] [bit] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_CustomFields] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomFieldValues]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomFieldValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomFieldId] [int] NOT NULL,
	[GlobalIdFK] [uniqueidentifier] NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_CustomFieldValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_CustomFieldValues_CustomFieldId_GlobalIdFK] UNIQUE NONCLUSTERED 
(
	[CustomFieldId] ASC,
	[GlobalIdFK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomFormTemplates]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomFormTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[EntityType] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[IsArchived] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EntityContacts]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityContacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalIdFk] [uniqueidentifier] NOT NULL,
	[EntityType] [varchar](255) NOT NULL,
	[ContactId] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateAdded] [datetime] NULL,
	[DateRemoved] [datetime] NULL,
 CONSTRAINT [PK_EntityContacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ_EntityContacts_GlobalIdFK_ContactId] UNIQUE NONCLUSTERED 
(
	[GlobalIdFk] ASC,
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Facilities]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Name] [nvarchar](255) NULL,
	[Geometry] [geometry] NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[PhysicalAddress1] [nvarchar](255) NULL,
	[PhysicalAddress2] [nvarchar](50) NULL,
	[PhysicalCity] [nvarchar](255) NULL,
	[PhysicalState] [nvarchar](50) NULL,
	[PhysicalZip] [nvarchar](50) NULL,
	[AdditionalInformation] [nvarchar](3000) NULL,
	[Sector] [nvarchar](50) NULL,
	[SICCode] [varchar](50) NULL,
	[IsHighRisk] [bit] NULL,
	[InspectionFrequencyId] [int] NULL,
	[YearBuilt] [int] NULL,
	[SquareFootage] [numeric](18, 2) NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[LatestInspectionId] [int] NULL,
	[LatestInspectionDate] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
	[TrackingId] [nvarchar](255) NULL,
 CONSTRAINT [PK_Facilities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FacilityInspections]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacilityInspections](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[FacilityId] [int] NOT NULL,
	[InspectionDate] [datetime2](7) NULL,
	[TimeIn] [datetime2](7) NULL,
	[TimeOut] [datetime2](7) NULL,
	[ScheduledInspectionDate] [datetime2](7) NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[InspectorId] [int] NULL,
	[InspectorId2] [int] NULL,
	[FollowUpInspectionId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateResolved] [datetime2](7) NULL,
	[IsFacilityActive] [bit] NULL,
	[IsFacilityPermitted] [bit] NULL,
	[IsSWPPOnSite] [bit] NULL,
	[AreRecordsCurrent] [bit] NULL,
	[AreSamplingDataEvaluationAcceptable] [bit] NULL,
	[IsBMPAcceptable] [bit] NULL,
	[IsGoodHouseKeeping] [bit] NULL,
	[IsErosionPresent] [bit] NULL,
	[AreWashoutsPresent] [bit] NULL,
	[IsDownstreamErosionPresent] [bit] NULL,
	[AreFloatablesPresent] [bit] NULL,
	[AreIllicitDischargesPresent] [bit] NULL,
	[AreNonStormWaterDischargePresent] [bit] NULL,
	[IsReturnInspectionRequired] [bit] NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[WeatherCondition] [nvarchar](255) NULL,
	[WeatherTemperatureF] [numeric](18, 2) NULL,
	[WeatherPrecipitationIn] [numeric](18, 2) NULL,
	[WeatherLast72] [numeric](18, 3) NULL,
	[WeatherLast24] [numeric](18, 3) NULL,
	[WindDirection] [varchar](50) NULL,
	[WindSpeedMph] [numeric](18, 2) NULL,
	[WeatherStationId] [varchar](50) NULL,
	[WeatherDateTime] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[Geometry] [geometry] NULL,
	[GeometryError] [int] NULL,
 CONSTRAINT [PK_FacilityInspections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Files]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Files](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[GlobalIdFK] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[EntityType] [varchar](255) NULL,
	[OriginalFileName] [nvarchar](260) NULL,
	[FileSize] [int] NULL,
	[FileName] [nvarchar](260) NULL,
	[ThumbnailName] [nvarchar](260) NULL,
	[MIMEType] [nvarchar](255) NULL,
	[AddedDate] [datetime2](7) NOT NULL,
	[AddedBy] [nvarchar](255) NULL,
	[IsDeleted] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Emails]    Script Date: 5/3/19 3:23:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[GlobalIdFK] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[EntityType] [varchar](255) NULL,
	[To] [nvarchar](500) NULL,
	[CC] [nvarchar](500) NULL,
	[AddedDate] [datetime2](7) NOT NULL,
	[AddedBy] [nvarchar](255) NULL,
	[Subject] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NULL,
 CONSTRAINT [PK_Emails] PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IllicitDischarges]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IllicitDischarges](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Name] [nvarchar](255) NULL,
	[Geometry] [geometry] NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateReported] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[PhysicalAddress1] [nvarchar](255) NULL,
	[PhysicalAddress2] [nvarchar](50) NULL,
	[PhysicalCity] [nvarchar](50) NULL,
	[PhysicalState] [nvarchar](50) NULL,
	[PhysicalZip] [nvarchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[CellNumber] [varchar](50) NULL,
	[FaxNumber] [varchar](50) NULL,
	[Conversation] [nvarchar](max) NULL,
	[DischargeDescription] [nvarchar](max) NULL,
	[RequestCorrectiveAction] [nvarchar](max) NULL,
	[DateEliminated] [datetime2](7) NULL,
	[InvestigatorId] [int] NULL,
	[Investigator2Id] [int] NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[ConstructionSiteId] [int] NULL,
	[FollowUpDate] [datetime] NULL,
 CONSTRAINT [PK_IllicitDischarges] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InspectionFrequencies]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionFrequencies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Sort] [int] NULL,
	[Days] [int] NULL,
 CONSTRAINT [PK_InspectionFrequencies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InspectionType]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InspectionType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Sort] [int] NULL,
	[EntityType] [varchar](50) NULL,
	[CustomFormTemplateId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsArchived] [bit] NOT NULL,
 CONSTRAINT [PK_InspectionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[knex_migrations]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[knex_migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[batch] [int] NULL,
	[migration_time] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[knex_migrations_lock]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[knex_migrations_lock](
	[is_locked] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MonitoringLocations]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringLocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Name] [nvarchar](255) NULL,
	[Geometry] [geometry] NULL,
	[Client] [nvarchar](50) NULL,
	[Address] [nvarchar](255) NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MonitoringLocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OutfallInspections]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OutfallInspections](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[OutfallId] [int] NOT NULL,
	[InspectionDate] [datetime2](7) NULL,
	[TimeIn] [datetime2](7) NULL,
	[TimeOut] [datetime2](7) NULL,
	[ScheduledInspectionDate] [datetime2](7) NULL,
	[ComplianceStatus] [varchar](50) NULL,
	[Inspector] [nvarchar](255) NULL,
	[Inspector2] [nvarchar](255) NULL,
	[InspectorId] [int] NULL,
	[InspectorId2] [int] NULL,
	[FollowUpInspectionId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DaysSinceLastRain] [int] NULL,
	[DischargeDescription] [nvarchar](500) NULL,
	[DryOrWetWeatherInspection] [varchar](50) NULL,
	[Color] [varchar](50) NULL,
	[Clarity] [varchar](50) NULL,
	[Odor] [varchar](50) NULL,
	[Foam] [varchar](50) NULL,
	[Sheen] [varchar](50) NULL,
	[SustainedSolids] [varchar](50) NULL,
	[SetSolids] [varchar](50) NULL,
	[FloatingSolids] [varchar](50) NULL,
	[PH] [numeric](18, 2) NULL,
	[TemperatureF] [numeric](18, 2) NULL,
	[DOmgl] [numeric](18, 2) NULL,
	[Turbidity] [varchar](50) NULL,
	[Cond] [varchar](50) NULL,
	[DOSaturation] [numeric](18, 2) NULL,
	[FlowrateGPM] [numeric](18, 2) NULL,
	[Copper] [numeric](18, 2) NULL,
	[Phenols] [numeric](18, 2) NULL,
	[Ammonia] [numeric](18, 2) NULL,
	[Detergents] [numeric](18, 2) NULL,
	[TPO4] [numeric](18, 2) NULL,
	[CL2] [numeric](18, 2) NULL,
	[BOD] [numeric](18, 2) NULL,
	[COD] [numeric](18, 2) NULL,
	[TSS] [numeric](18, 2) NULL,
	[NO3] [numeric](18, 2) NULL,
	[FecalColiform] [numeric](18, 2) NULL,
	[EColi] [numeric](18, 2) NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[WeatherCondition] [nvarchar](255) NULL,
	[WeatherTemperatureF] [numeric](18, 2) NULL,
	[WeatherPrecipitationIn] [numeric](18, 2) NULL,
	[WeatherLast72] [numeric](18, 3) NULL,
	[WeatherLast24] [numeric](18, 3) NULL,
	[WindDirection] [varchar](50) NULL,
	[WindSpeedMph] [numeric](18, 2) NULL,
	[WeatherStationId] [varchar](50) NULL,
	[WeatherDateTime] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[Geometry] [geometry] NULL,
	[GeometryError] [int] NULL,
 CONSTRAINT [PK_OutfallInspections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[OutfallInspections] ADD DEFAULT ('Dry') FOR [DryOrWetWeatherInspection]
GO

/****** Object:  Table [dbo].[OutfallMaterials]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OutfallMaterials](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_OutfallMaterials] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Outfalls]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Outfalls](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Location] [nvarchar](255) NULL,
	[NearAddress] [nvarchar](255) NULL,
	[Geometry] [geometry] NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[TrackingId] [nvarchar](255) NULL,
	[Condition] [varchar](255) NULL,
	[Material] [varchar](255) NULL,
	[Obstruction] [varchar](255) NULL,
	[OutfallType] [varchar](255) NULL,
	[OutfallSizeIn] [numeric](18, 2) NULL,
	[OutfallWidthIn] [numeric](18, 2) NULL,
	[OutfallHeightIn] [numeric](18, 2) NULL,
	[AdditionalInformation] [nvarchar](3000) NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[LatestInspectionId] [int] NULL,
	[LatestInspectionDate] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_Outfalls] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OutfallTypes]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OutfallTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_OutfallTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReceivingWaters]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceivingWaters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_ReceivingWaters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sectors]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sectors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_Sectors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[States]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Abbr] [varchar](10) NOT NULL,
	[CountryCode] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StructureControlTypes]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StructureControlTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[CustomerId] [varchar](50) NULL,
 CONSTRAINT [PK_StructureControlTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StructureInspections]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StructureInspections](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[StructureId] [int] NOT NULL,
	[InspectionDate] [datetime2](7) NULL,
	[TimeIn] [datetime2](7) NULL,
	[TimeOut] [datetime2](7) NULL,
	[ScheduledInspectionDate] [datetime2](7) NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[InspectorId] [int] NULL,
	[InspectorId2] [int] NULL,
	[FollowUpInspectionId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateResolved] [datetime2](7) NULL,
	[AdditionalInformation] [nvarchar](max) NULL,
	[IsControlActive] [bit] NULL,
	[IsBuiltWithinSpecification] [bit] NULL,
	[IsDepthOfSedimentAcceptable] [bit] NULL,
	[RequiresMaintenance] [bit] NULL,
	[RequiresRepairs] [bit] NULL,
	[IsStandingWaterPresent] [bit] NULL,
	[AreWashoutsPresent] [bit] NULL,
	[FloatablesRemovalRequired] [bit] NULL,
	[IsOutletClogged] [bit] NULL,
	[IsStructuralDamage] [bit] NULL,
	[IsErosionPresent] [bit] NULL,
	[IsDownstreamErosionPresent] [bit] NULL,
	[IsIllicitDischargePresent] [bit] NULL,
	[IsReturnInspectionRecommended] [bit] NULL,
	[WeatherCondition] [nvarchar](255) NULL,
	[WeatherTemperatureF] [numeric](18, 2) NULL,
	[WeatherPrecipitationIn] [numeric](18, 2) NULL,
	[WeatherLast72] [numeric](18, 3) NULL,
	[WeatherLast24] [numeric](18, 3) NULL,
	[WindDirection] [varchar](50) NULL,
	[WindSpeedMph] [numeric](18, 2) NULL,
	[WeatherStationId] [varchar](50) NULL,
	[WeatherDateTime] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
	[CustomFormTemplateId] [int] NOT NULL,
	[InspectionTypeId] [int] NOT NULL,
	[Geometry] [geometry] NULL,
	[GeometryError] [int] NULL,
 CONSTRAINT [PK_StructureInspections] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Structures]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Structures](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NOT NULL,
	[CustomerId] [varchar](50) NULL,
	[Geometry] [geometry] NULL,
	[TrackingId] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[ControlType] [varchar](50) NULL,
	[ComplianceStatus] [nvarchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ProjectId] [int] NULL,
	[ProjectType] [varchar](50) NULL,
	[PhysicalAddress1] [nvarchar](255) NULL,
	[PhysicalAddress2] [nvarchar](50) NULL,
	[PhysicalCity] [nvarchar](255) NULL,
	[PhysicalState] [nvarchar](50) NULL,
	[PhysicalZip] [nvarchar](50) NULL,
	[PermitStatus] [varchar](50) NULL,
	[InspectionFrequencyId] [int] NULL,
	[MaintenanceAgreement] [nvarchar](3000) NULL,
	[CommunityId] [int] NULL,
	[WatershedId] [int] NULL,
	[SubwatershedId] [int] NULL,
	[ReceivingWatersId] [int] NULL,
	[AdditionalInformation] [nvarchar](3000) NULL,
	[LatestInspectionId] [int] NULL,
	[LatestInspectionDate] [datetime2](7) NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_Structures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Href] [varchar](1024) NULL,
	[Username] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[GivenName] [nvarchar](255) NULL,
	[MiddleName] [nvarchar](255) NULL,
	[Surname] [nvarchar](255) NULL,
	[Status] [varchar](50) NULL,
	[CreatedAt] [datetime2](7) NULL,
	[ModifiedAt] [datetime2](7) NULL,
	[OktaId] [nvarchar](999) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsersCustomers]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersCustomers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Role] [nvarchar](510) NULL,
 CONSTRAINT [UQ_UsersCustomers_UserId_CustomerId] UNIQUE NONCLUSTERED 
(
	[UserId] ASC,
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Watersheds]    Script Date: 12/19/2018 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Watersheds](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsSubWatershed] [bit] NOT NULL,
	[OriginalId] [int] NULL,
 CONSTRAINT [PK_Watersheds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Ojbect: Table [dbo].[SWMP]    Script Date: 05/12/2020 10:20:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SWMP](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[Year] [int] NULL,
	[Number] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[OperatorLevel] [tinyint] NULL,
	[ContactName] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[MailingAddress] [nvarchar](255) NULL,
	[AnnualReportingYear] [datetime] NULL,
	[Status] [nvarchar](20) NULL,
	[UpdatedDate] [datetime] NULL,
	[PlanLength] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SWMP] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [swmp_customerid_index] ON [dbo].[SWMP]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SWMP]  WITH CHECK ADD  CONSTRAINT [swmp_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[SWMP] CHECK CONSTRAINT [swmp_customerid_foreign]
GO
ALTER TABLE [dbo].[SWMP] ADD DEFAULT (5) FOR [PlanLength]
GO
GO
ALTER TABLE [dbo].[SWMP] ADD DEFAULT (1) FOR [OperatorLevel]
GO
/****** Ojbect: Table [dbo].[BMP]    Script Date: 05/12/2020 10:22:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BMP](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SwmpId] [int] NOT NULL,
	[Number] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[StartYear] [int] NULL,
	[MinimumControlMeasures] [nvarchar](800) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BMP] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [bmp_swmpid_index] ON [dbo].[BMP]
(
	[SwmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BMP]  WITH CHECK ADD  CONSTRAINT [bmp_swmpid_foreign] FOREIGN KEY([SwmpId])
REFERENCES [dbo].[SWMP] ([Id])
GO
ALTER TABLE [dbo].[BMP] CHECK CONSTRAINT [bmp_swmpid_foreign]
GO
/****** Ojbect: Table [dbo].[MG]    Script Date: 05/12/2020 10:25:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MG](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BmpId] [int] NOT NULL,
	[Number] [nvarchar](255) NULL,
	[Title] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[YearsImplemented] [nvarchar](20) NULL,
	[IsQuantityRequired] [bit] NULL,
	[InformationUsed] [nvarchar](255) NULL,
	[Units] [nvarchar](255) NULL,
	[AnnualGoal] [nvarchar](10) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MG] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [mg_bmpid_index] ON [dbo].[MG]
(
	[BmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MG]  WITH CHECK ADD  CONSTRAINT [mg_bmpid_foreign] FOREIGN KEY([BmpId])
REFERENCES [dbo].[BMP] ([Id])
GO
ALTER TABLE [dbo].[MG] CHECK CONSTRAINT [mg_bmpid_foreign]
GO
/****** Ojbect: Table [dbo].[SwmpActivity]    Script Date: 05/12/2020 10:27:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SwmpActivity](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GlobalId] [uniqueidentifier] NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[SwmpId] [int] NOT NULL,
	[MgId] [int] NOT NULL,
	[Date] [date] NULL,
	[Quantity] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SwmpActivity] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [swmpactivity_customerid_index] ON [dbo].[SwmpActivity]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [swmpactivity_globalid_index] ON [dbo].[SwmpActivity]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [swmpactivity_mgid_index] ON [dbo].[SwmpActivity]
(
	[MgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [swmpactivity_swmpid_index] ON [dbo].[SwmpActivity]
(
	[SwmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SwmpActivity] ADD  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[SwmpActivity] ADD  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[SwmpActivity]  WITH CHECK ADD  CONSTRAINT [swmpactivity_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[SwmpActivity] CHECK CONSTRAINT [swmpactivity_customerid_foreign]
GO
ALTER TABLE [dbo].[SwmpActivity]  WITH CHECK ADD  CONSTRAINT [swmpactivity_mgid_foreign] FOREIGN KEY([MgId])
REFERENCES [dbo].[MG] ([Id])
GO
ALTER TABLE [dbo].[SwmpActivity] CHECK CONSTRAINT [swmpactivity_mgid_foreign]
GO
ALTER TABLE [dbo].[SwmpActivity]  WITH CHECK ADD  CONSTRAINT [swmpactivity_swmpid_foreign] FOREIGN KEY([SwmpId])
REFERENCES [dbo].[SWMP] ([Id])
GO
ALTER TABLE [dbo].[SwmpActivity] CHECK CONSTRAINT [swmpactivity_swmpid_foreign]
GO
/****** Object:  Table [dbo].[AnnualReport]    Script Date: 05/21/2020 7:47:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AnnualReport](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[SwmpId] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Report] [nvarchar](max) NULL,
	[ContactName] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[MailingAddress] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnnualReport] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [annualreport_customerid_index] ON [dbo].[AnnualReport]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [annualreport_swmpid_index] ON [dbo].[AnnualReport]
(
	[SwmpId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AnnualReport]  WITH CHECK ADD  CONSTRAINT [annualreport_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[AnnualReport] CHECK CONSTRAINT [annualreport_customerid_foreign]
GO
ALTER TABLE [dbo].[AnnualReport]  WITH CHECK ADD  CONSTRAINT [annualreport_swmpid_foreign] FOREIGN KEY([SwmpId])
REFERENCES [dbo].[SWMP] ([Id])
GO
ALTER TABLE [dbo].[AnnualReport] CHECK CONSTRAINT [annualreport_swmpid_foreign]
GO
/****** Object:  Table [dbo].[CustomerInspectionEmail]    Script Date: 03/23/2021 10:26:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerInspectionEmail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [varchar](50) NOT NULL,
	[InspectionType] [varchar](50) NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerInspectionEmail] ADD PRIMARY KEY CLUSTERED
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [customerinspectionemail_customerid_index] ON [dbo].[CustomerInspectionEmail]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerInspectionEmail]  WITH CHECK ADD  CONSTRAINT [customerinspectionemail_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[CustomerInspectionEmail] CHECK CONSTRAINT [customerinspectionemail_customerid_foreign]
GO
/****** Object:  Index [bmpactivitylogs_bmpdetailid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [bmpactivitylogs_bmpdetailid_index] ON [dbo].[BMPActivityLogs]
(
	[BmpDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [bmpdatatypes_customerid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [bmpdatatypes_customerid_index] ON [dbo].[BMPDataTypes]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [bmpdetails_controlmeasureid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [bmpdetails_controlmeasureid_index] ON [dbo].[BMPDetails]
(
	[ControlMeasureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [bmpdetails_globalid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [bmpdetails_globalid_index] ON [dbo].[BMPDetails]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [citizenreports_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [citizenreports_customformtemplateid_index] ON [dbo].[CitizenReports]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [citizenreports_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [citizenreports_inspectiontypeid_index] ON [dbo].[CitizenReports]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CitizenReports_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_CitizenReports_CustomerId] ON [dbo].[CitizenReports]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CitizenReports_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_CitizenReports_GlobalId] ON [dbo].[CitizenReports]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Communities]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Communities] ON [dbo].[Communities]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [constructionsiteinspections_constructionsiteid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [constructionsiteinspections_constructionsiteid_index] ON [dbo].[ConstructionSiteInspections]
(
	[ConstructionSiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [constructionsiteinspections_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [constructionsiteinspections_customformtemplateid_index] ON [dbo].[ConstructionSiteInspections]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [constructionsiteinspections_followupinspectionid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [constructionsiteinspections_followupinspectionid_index] ON [dbo].[ConstructionSiteInspections]
(
	[FollowUpInspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [constructionsiteinspections_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [constructionsiteinspections_inspectiontypeid_index] ON [dbo].[ConstructionSiteInspections]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ConstructionSites_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_ConstructionSites_CustomerId] ON [dbo].[ConstructionSites]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ConstructionSites_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_ConstructionSites_GlobalId] ON [dbo].[ConstructionSites]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [customfields_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [customfields_customformtemplateid_index] ON [dbo].[CustomFields]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [customfields_entitytype_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [customfields_entitytype_index] ON [dbo].[CustomFields]
(
	[EntityType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [customfieldvalues_globalidfk_customfieldid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [customfieldvalues_globalidfk_customfieldid_index] ON [dbo].[CustomFieldValues]
(
	[GlobalIdFK] ASC,
	[CustomFieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [customformtemplates_customerid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [customformtemplates_customerid_index] ON [dbo].[CustomFormTemplates]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Facilities_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Facilities_CustomerId] ON [dbo].[Facilities]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Facilities_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Facilities_GlobalId] ON [dbo].[Facilities]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [facilityinspections_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [facilityinspections_customformtemplateid_index] ON [dbo].[FacilityInspections]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [facilityinspections_facilityid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [facilityinspections_facilityid_index] ON [dbo].[FacilityInspections]
(
	[FacilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [facilityinspections_followupinspectionid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [facilityinspections_followupinspectionid_index] ON [dbo].[FacilityInspections]
(
	[FollowUpInspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [facilityinspections_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [facilityinspections_inspectiontypeid_index] ON [dbo].[FacilityInspections]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [illicitdischarges_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [illicitdischarges_customformtemplateid_index] ON [dbo].[IllicitDischarges]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [illicitdischarges_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [illicitdischarges_inspectiontypeid_index] ON [dbo].[IllicitDischarges]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_IllicitDischarges_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_IllicitDischarges_CustomerId] ON [dbo].[IllicitDischarges]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_IllicitDischarges_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_IllicitDischarges_GlobalId] ON [dbo].[IllicitDischarges]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [inspectiontype_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [inspectiontype_customformtemplateid_index] ON [dbo].[InspectionType]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MonitoringLocations_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringLocations_CustomerId] ON [dbo].[MonitoringLocations]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MonitoringLocations_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_MonitoringLocations_GlobalId] ON [dbo].[MonitoringLocations]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [outfallinspections_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [outfallinspections_customformtemplateid_index] ON [dbo].[OutfallInspections]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [outfallinspections_followupinspectionid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [outfallinspections_followupinspectionid_index] ON [dbo].[OutfallInspections]
(
	[FollowUpInspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [outfallinspections_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [outfallinspections_inspectiontypeid_index] ON [dbo].[OutfallInspections]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [outfallinspections_outfallid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [outfallinspections_outfallid_index] ON [dbo].[OutfallInspections]
(
	[OutfallId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Outfalls_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Outfalls_CustomerId] ON [dbo].[Outfalls]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Outfalls_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Outfalls_GlobalId] ON [dbo].[Outfalls]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ReceivingWaters]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_ReceivingWaters] ON [dbo].[ReceivingWaters]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [structureinspections_customformtemplateid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [structureinspections_customformtemplateid_index] ON [dbo].[StructureInspections]
(
	[CustomFormTemplateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [structureinspections_followupinspectionid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [structureinspections_followupinspectionid_index] ON [dbo].[StructureInspections]
(
	[FollowUpInspectionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [structureinspections_inspectiontypeid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [structureinspections_inspectiontypeid_index] ON [dbo].[StructureInspections]
(
	[InspectionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [structureinspections_structureid_index]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [structureinspections_structureid_index] ON [dbo].[StructureInspections]
(
	[StructureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Structures_CustomerId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Structures_CustomerId] ON [dbo].[Structures]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Structures_GlobalId]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Structures_GlobalId] ON [dbo].[Structures]
(
	[GlobalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Watersheds]    Script Date: 12/19/2018 7:47:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_Watersheds] ON [dbo].[Watersheds]
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Activity] ADD  CONSTRAINT [DF_Activity_Date]  DEFAULT (getutcdate()) FOR [Date]
GO
ALTER TABLE [dbo].[BmpActivities] ADD  CONSTRAINT [DF_Bmps_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[BmpActivities] ADD  CONSTRAINT [DF_Bmps_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[BmpActivities] ADD  CONSTRAINT [DF_Bmps_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BMPActivityLogs] ADD  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[BMPActivityLogs] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BmpControlMeasures] ADD  CONSTRAINT [DF_BmpControlMeasures_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BMPDataTypes] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BMPDetails] ADD  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[BMPDetails] ADD  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[BMPDetails] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BmpTasks] ADD  CONSTRAINT [DF_BmpTasks_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CitizenReports] ADD  CONSTRAINT [DF_CitizenReports_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[CitizenReports] ADD  CONSTRAINT [DF_CitizenReports_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[CitizenReports] ADD  CONSTRAINT [DF_CitizenReports_DateReported]  DEFAULT (getutcdate()) FOR [DateReported]
GO
ALTER TABLE [dbo].[CitizenReports] ADD  CONSTRAINT [DF_CitizenReports_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_SiteIsActive]  DEFAULT ((0)) FOR [IsSiteActive]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_IsPlanOnSite]  DEFAULT ((0)) FOR [IsPlanOnSite]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_IsSitePermitted]  DEFAULT ((0)) FOR [IsSitePermitted]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_ArePlanRecordsCurrent]  DEFAULT ((0)) FOR [ArePlanRecordsCurrent]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreErosionControlsAcceptable]  DEFAULT ((0)) FOR [AreErosionControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreStructuralControlsAcceptable]  DEFAULT ((0)) FOR [AreStructuralControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_IsWasteManagementAcceptable]  DEFAULT ((0)) FOR [IsWasteManagementAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_IsMaintenanceOfControlsAcceptable]  DEFAULT ((0)) FOR [IsMaintenanceOfControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreLocalControlsAcceptable]  DEFAULT ((0)) FOR [AreLocalControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreStabilizationControlsAcceptable]  DEFAULT ((0)) FOR [AreStabilizationControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreTrackingControlsAcceptable]  DEFAULT ((0)) FOR [AreTrackingControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreOutfallVelocityControlsAcceptable]  DEFAULT ((0)) FOR [AreOutfallVelocityControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] ADD  CONSTRAINT [DF_ConstructionSiteInspections_AreNonStormWaterControlsAcceptable]  DEFAULT ((0)) FOR [AreNonStormWaterControlsAcceptable]
GO
ALTER TABLE [dbo].[ConstructionSites] ADD  CONSTRAINT [DF_ConstructionSites_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[ConstructionSites] ADD  CONSTRAINT [DF_ConstructionSites_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[ConstructionSites] ADD  CONSTRAINT [DF_ConstructionSites_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Contacts] ADD  CONSTRAINT [DF_Contacts_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('{}') FOR [Features]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('{}') FOR [HiddenFields]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('0') FOR [UnlimitedCustomFormTemplates]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('0') FOR [UnlimitedAssets]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('US') FOR [CountryCode]
GO
ALTER TABLE [dbo].[Customers] ADD  DEFAULT ('MAPBOX') FOR [AerialProvider]
GO
ALTER TABLE [dbo].[CustomFields] ADD  CONSTRAINT [DF_CustomFields_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CustomFormTemplates] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CustomFormTemplates] ADD  DEFAULT ('0') FOR [IsSystem]
GO
ALTER TABLE [dbo].[CustomFormTemplates] ADD  DEFAULT ('0') FOR [IsArchived]
GO
ALTER TABLE [dbo].[EntityContacts] ADD  CONSTRAINT [DF_EntityContacts_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
GO
ALTER TABLE [dbo].[EntityContacts] ADD  CONSTRAINT [DF_EntityContacts_IsPrimary]  DEFAULT ((1)) FOR [IsPrimary]
GO
ALTER TABLE [dbo].[Facilities] ADD  CONSTRAINT [DF_Facilities_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[Facilities] ADD  CONSTRAINT [DF_Facilities_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[Facilities] ADD  CONSTRAINT [DF_Facilities_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Facilities] ADD  CONSTRAINT [DF_Facilities_isHighRisk]  DEFAULT ((0)) FOR [IsHighRisk]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsFacilityActive]  DEFAULT ((0)) FOR [IsFacilityActive]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsFacilityPermitted]  DEFAULT ((0)) FOR [IsFacilityPermitted]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsSWPPOnSite]  DEFAULT ((0)) FOR [IsSWPPOnSite]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreRecordsCurrent]  DEFAULT ((0)) FOR [AreRecordsCurrent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreSamplingDataEvaluationAcceptable]  DEFAULT ((0)) FOR [AreSamplingDataEvaluationAcceptable]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsBMPAcceptable]  DEFAULT ((0)) FOR [IsBMPAcceptable]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsGoodHouseKeeping]  DEFAULT ((0)) FOR [IsGoodHouseKeeping]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsErosionPresent]  DEFAULT ((0)) FOR [IsErosionPresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreWashoutsPresent]  DEFAULT ((0)) FOR [AreWashoutsPresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsDownstreamErosionPresent]  DEFAULT ((0)) FOR [IsDownstreamErosionPresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreFloatablesPresent]  DEFAULT ((0)) FOR [AreFloatablesPresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreIllicitDischargesPresent]  DEFAULT ((0)) FOR [AreIllicitDischargesPresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_AreNonStormWaterDischargePresent]  DEFAULT ((0)) FOR [AreNonStormWaterDischargePresent]
GO
ALTER TABLE [dbo].[FacilityInspections] ADD  CONSTRAINT [DF_FacilityInspections_IsReturnInspectionRequired]  DEFAULT ((0)) FOR [IsReturnInspectionRequired]
GO
ALTER TABLE [dbo].[Files] ADD  CONSTRAINT [DF_Files_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[Files] ADD  CONSTRAINT [DF_Files_AddedDate]  DEFAULT (getutcdate()) FOR [AddedDate]
GO
ALTER TABLE [dbo].[Files] ADD  CONSTRAINT [DF_Files_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[Emails] ADD  CONSTRAINT [DF_Emails_AddedDate]  DEFAULT (getutcdate()) FOR [AddedDate]
GO
ALTER TABLE [dbo].[IllicitDischarges] ADD  CONSTRAINT [DF_IllicitDischarges_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[IllicitDischarges] ADD  CONSTRAINT [DF_IllicitDischarges_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[IllicitDischarges] ADD  CONSTRAINT [DF_IllicitDischarges_DateReported]  DEFAULT (getdate()) FOR [DateReported]
GO
ALTER TABLE [dbo].[IllicitDischarges] ADD  CONSTRAINT [DF_IllicitDischarges_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[InspectionType] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[InspectionType] ADD  DEFAULT ('0') FOR [IsArchived]
GO
ALTER TABLE [dbo].[MonitoringLocations] ADD  CONSTRAINT [DF_MonitoringLocations_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[MonitoringLocations] ADD  CONSTRAINT [DF_MonitoringLocations_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[MonitoringLocations] ADD  CONSTRAINT [DF_MonitoringLocations_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[OutfallInspections] ADD  CONSTRAINT [DF_OutfallInspections_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[OutfallInspections] ADD  CONSTRAINT [DF_OutfallInspections_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[OutfallInspections] ADD  CONSTRAINT [DF_OutfallInspections_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Outfalls] ADD  CONSTRAINT [DF_Outfalls_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[Outfalls] ADD  CONSTRAINT [DF_Outfalls_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[Outfalls] ADD  CONSTRAINT [DF_Outfalls_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsControlActive]  DEFAULT ((0)) FOR [IsControlActive]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsBuiltWithinSpecification]  DEFAULT ((0)) FOR [IsBuiltWithinSpecification]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsDepthOfSedimentAcceptable]  DEFAULT ((0)) FOR [IsDepthOfSedimentAcceptable]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_RequiresMaintenance]  DEFAULT ((0)) FOR [RequiresMaintenance]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_RequiresRepairs]  DEFAULT ((0)) FOR [RequiresRepairs]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsStandingWaterPresent]  DEFAULT ((0)) FOR [IsStandingWaterPresent]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_AreWashoutsPresent]  DEFAULT ((0)) FOR [AreWashoutsPresent]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_FloatablesRemovalRequired]  DEFAULT ((0)) FOR [FloatablesRemovalRequired]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsOutletClogged]  DEFAULT ((0)) FOR [IsOutletClogged]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsStructuralDamage]  DEFAULT ((0)) FOR [IsStructuralDamage]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsErosionPresent]  DEFAULT ((0)) FOR [IsErosionPresent]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsDownstreamErosionPresent]  DEFAULT ((0)) FOR [IsDownstreamErosionPresent]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsIllicitDischargePresent]  DEFAULT ((0)) FOR [IsIllicitDischargePresent]
GO
ALTER TABLE [dbo].[StructureInspections] ADD  CONSTRAINT [DF_StructureInspections_IsReturnInspectionRecommended]  DEFAULT ((0)) FOR [IsReturnInspectionRecommended]
GO
ALTER TABLE [dbo].[Structures] ADD  CONSTRAINT [DF_Structures_GlobalId]  DEFAULT (newid()) FOR [GlobalId]
GO
ALTER TABLE [dbo].[Structures] ADD  CONSTRAINT [DF_Structures_DateAdded]  DEFAULT (getutcdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[Structures] ADD  CONSTRAINT [DF_Structures_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Watersheds] ADD  CONSTRAINT [DF_Watersheds_IsSubWatershed]  DEFAULT ((0)) FOR [IsSubWatershed]
GO
ALTER TABLE [dbo].[SWMP] ADD  DEFAULT ('0') FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BmpActivities]  WITH CHECK ADD  CONSTRAINT [FK_Bmps_BmpTasks] FOREIGN KEY([TaskId])
REFERENCES [dbo].[BmpTasks] ([Id])
GO
ALTER TABLE [dbo].[BmpActivities] CHECK CONSTRAINT [FK_Bmps_BmpTasks]
GO
ALTER TABLE [dbo].[BmpActivities]  WITH CHECK ADD  CONSTRAINT [FK_Bmps_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[BmpActivities] CHECK CONSTRAINT [FK_Bmps_Customers]
GO
ALTER TABLE [dbo].[BMPActivityLogs]  WITH CHECK ADD  CONSTRAINT [bmpactivitylogs_bmpdetailid_foreign] FOREIGN KEY([BmpDetailId])
REFERENCES [dbo].[BMPDetails] ([Id])
GO
ALTER TABLE [dbo].[BMPActivityLogs] CHECK CONSTRAINT [bmpactivitylogs_bmpdetailid_foreign]
GO
ALTER TABLE [dbo].[BmpControlMeasures]  WITH CHECK ADD  CONSTRAINT [FK_BmpControlMeasures_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[BmpControlMeasures] CHECK CONSTRAINT [FK_BmpControlMeasures_Customers]
GO
ALTER TABLE [dbo].[BMPDataTypes]  WITH CHECK ADD  CONSTRAINT [bmpdatatypes_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[BMPDataTypes] CHECK CONSTRAINT [bmpdatatypes_customerid_foreign]
GO
ALTER TABLE [dbo].[BMPDetails]  WITH CHECK ADD  CONSTRAINT [bmpdetails_controlmeasureid_foreign] FOREIGN KEY([ControlMeasureId])
REFERENCES [dbo].[BmpControlMeasures] ([Id])
GO
ALTER TABLE [dbo].[BMPDetails] CHECK CONSTRAINT [bmpdetails_controlmeasureid_foreign]
GO
ALTER TABLE [dbo].[BmpTasks]  WITH CHECK ADD  CONSTRAINT [FK_BmpTasks_BmpTasks] FOREIGN KEY([ControlMeasureId])
REFERENCES [dbo].[BmpControlMeasures] ([Id])
GO
ALTER TABLE [dbo].[BmpTasks] CHECK CONSTRAINT [FK_BmpTasks_BmpTasks]
GO
ALTER TABLE [dbo].[BmpTasks]  WITH CHECK ADD  CONSTRAINT [FK_BmpTasks_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[BmpTasks] CHECK CONSTRAINT [FK_BmpTasks_Customers]
GO
ALTER TABLE [dbo].[CitizenReports]  WITH CHECK ADD  CONSTRAINT [citizenreports_constructionsiteid_foreign] FOREIGN KEY([ConstructionSiteId])
REFERENCES [dbo].[ConstructionSites] ([Id])
GO
ALTER TABLE [dbo].[CitizenReports] CHECK CONSTRAINT [citizenreports_constructionsiteid_foreign]
GO
ALTER TABLE [dbo].[CitizenReports]  WITH CHECK ADD  CONSTRAINT [citizenreports_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[CitizenReports] CHECK CONSTRAINT [citizenreports_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[CitizenReports]  WITH CHECK ADD  CONSTRAINT [citizenreports_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[CitizenReports] CHECK CONSTRAINT [citizenreports_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[CitizenReports]  WITH CHECK ADD  CONSTRAINT [FK_CitizenReports_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[CitizenReports] CHECK CONSTRAINT [FK_CitizenReports_Customers]
GO
ALTER TABLE [dbo].[Communities]  WITH CHECK ADD  CONSTRAINT [FK_Communities_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Communities] CHECK CONSTRAINT [FK_Communities_Customers]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections]  WITH CHECK ADD  CONSTRAINT [constructionsiteinspections_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] CHECK CONSTRAINT [constructionsiteinspections_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections]  WITH CHECK ADD  CONSTRAINT [constructionsiteinspections_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] CHECK CONSTRAINT [constructionsiteinspections_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[ConstructionSiteInspections]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSiteInspections_ConstructionSites] FOREIGN KEY([ConstructionSiteId])
REFERENCES [dbo].[ConstructionSites] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSiteInspections] CHECK CONSTRAINT [FK_ConstructionSiteInspections_ConstructionSites]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_Communities] FOREIGN KEY([CommunityId])
REFERENCES [dbo].[Communities] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_Communities]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_Customers]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_Projects] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_Projects]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_ReceivingWaters] FOREIGN KEY([ReceivingWatersId])
REFERENCES [dbo].[ReceivingWaters] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_ReceivingWaters]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_SubWatersheds] FOREIGN KEY([SubwatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_SubWatersheds]
GO
ALTER TABLE [dbo].[ConstructionSites]  WITH CHECK ADD  CONSTRAINT [FK_ConstructionSites_Watersheds] FOREIGN KEY([WatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[ConstructionSites] CHECK CONSTRAINT [FK_ConstructionSites_Watersheds]
GO
ALTER TABLE [dbo].[Contacts]  WITH CHECK ADD  CONSTRAINT [FK_Contacts_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Contacts] CHECK CONSTRAINT [FK_Contacts_Customers]
GO
ALTER TABLE [dbo].[CustomFields]  WITH CHECK ADD  CONSTRAINT [customfields_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[CustomFields] CHECK CONSTRAINT [customfields_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[CustomFieldValues]  WITH CHECK ADD  CONSTRAINT [FK_CustomFieldValues_CustomFields] FOREIGN KEY([CustomFieldId])
REFERENCES [dbo].[CustomFields] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomFieldValues] CHECK CONSTRAINT [FK_CustomFieldValues_CustomFields]
GO
ALTER TABLE [dbo].[CustomFormTemplates]  WITH CHECK ADD  CONSTRAINT [customformtemplates_customerid_foreign] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[CustomFormTemplates] CHECK CONSTRAINT [customformtemplates_customerid_foreign]
GO
ALTER TABLE [dbo].[EntityContacts]  WITH CHECK ADD  CONSTRAINT [FK_EntityContacts_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contacts] ([Id])
GO
ALTER TABLE [dbo].[EntityContacts] CHECK CONSTRAINT [FK_EntityContacts_Contact]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_Communities] FOREIGN KEY([CommunityId])
REFERENCES [dbo].[Communities] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_Communities]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_Customers]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_InspectionFrequencies] FOREIGN KEY([InspectionFrequencyId])
REFERENCES [dbo].[InspectionFrequencies] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_InspectionFrequencies]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_ReceivingWaters] FOREIGN KEY([ReceivingWatersId])
REFERENCES [dbo].[ReceivingWaters] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_ReceivingWaters]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_SubWatersheds] FOREIGN KEY([SubwatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_SubWatersheds]
GO
ALTER TABLE [dbo].[Facilities]  WITH CHECK ADD  CONSTRAINT [FK_Facilities_Watersheds] FOREIGN KEY([WatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Facilities] CHECK CONSTRAINT [FK_Facilities_Watersheds]
GO
ALTER TABLE [dbo].[FacilityInspections]  WITH CHECK ADD  CONSTRAINT [facilityinspections_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[FacilityInspections] CHECK CONSTRAINT [facilityinspections_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[FacilityInspections]  WITH CHECK ADD  CONSTRAINT [facilityinspections_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[FacilityInspections] CHECK CONSTRAINT [facilityinspections_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[FacilityInspections]  WITH CHECK ADD  CONSTRAINT [FK_FacilityInspections_Facilities] FOREIGN KEY([FacilityId])
REFERENCES [dbo].[Facilities] ([Id])
GO
ALTER TABLE [dbo].[FacilityInspections] CHECK CONSTRAINT [FK_FacilityInspections_Facilities]
GO
ALTER TABLE [dbo].[IllicitDischarges]  WITH CHECK ADD  CONSTRAINT [FK_IllicitDischarges_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[IllicitDischarges] CHECK CONSTRAINT [FK_IllicitDischarges_Customers]
GO
ALTER TABLE [dbo].[IllicitDischarges]  WITH CHECK ADD  CONSTRAINT [illicitdischarges_constructionsiteid_foreign] FOREIGN KEY([ConstructionSiteId])
REFERENCES [dbo].[ConstructionSites] ([Id])
GO
ALTER TABLE [dbo].[IllicitDischarges] CHECK CONSTRAINT [illicitdischarges_constructionsiteid_foreign]
GO
ALTER TABLE [dbo].[IllicitDischarges]  WITH CHECK ADD  CONSTRAINT [illicitdischarges_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[IllicitDischarges] CHECK CONSTRAINT [illicitdischarges_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[IllicitDischarges]  WITH CHECK ADD  CONSTRAINT [illicitdischarges_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[IllicitDischarges] CHECK CONSTRAINT [illicitdischarges_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[InspectionType]  WITH CHECK ADD  CONSTRAINT [inspectiontype_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[InspectionType] CHECK CONSTRAINT [inspectiontype_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[MonitoringLocations]  WITH CHECK ADD  CONSTRAINT [FK_MonitoringLocations_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[MonitoringLocations] CHECK CONSTRAINT [FK_MonitoringLocations_Customers]
GO
ALTER TABLE [dbo].[OutfallInspections]  WITH CHECK ADD  CONSTRAINT [FK_OutfallInspections_Outfalls] FOREIGN KEY([OutfallId])
REFERENCES [dbo].[Outfalls] ([Id])
GO
ALTER TABLE [dbo].[OutfallInspections] CHECK CONSTRAINT [FK_OutfallInspections_Outfalls]
GO
ALTER TABLE [dbo].[OutfallInspections]  WITH CHECK ADD  CONSTRAINT [outfallinspections_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[OutfallInspections] CHECK CONSTRAINT [outfallinspections_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[OutfallInspections]  WITH CHECK ADD  CONSTRAINT [outfallinspections_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[OutfallInspections] CHECK CONSTRAINT [outfallinspections_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[Outfalls]  WITH CHECK ADD  CONSTRAINT [FK_Outfalls_Communities] FOREIGN KEY([CommunityId])
REFERENCES [dbo].[Communities] ([Id])
GO
ALTER TABLE [dbo].[Outfalls] CHECK CONSTRAINT [FK_Outfalls_Communities]
GO
ALTER TABLE [dbo].[Outfalls]  WITH CHECK ADD  CONSTRAINT [FK_Outfalls_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Outfalls] CHECK CONSTRAINT [FK_Outfalls_Customers]
GO
ALTER TABLE [dbo].[Outfalls]  WITH CHECK ADD  CONSTRAINT [FK_Outfalls_ReceivingWaters] FOREIGN KEY([ReceivingWatersId])
REFERENCES [dbo].[ReceivingWaters] ([Id])
GO
ALTER TABLE [dbo].[Outfalls] CHECK CONSTRAINT [FK_Outfalls_ReceivingWaters]
GO
ALTER TABLE [dbo].[Outfalls]  WITH CHECK ADD  CONSTRAINT [FK_Outfalls_SubWatersheds] FOREIGN KEY([SubwatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Outfalls] CHECK CONSTRAINT [FK_Outfalls_SubWatersheds]
GO
ALTER TABLE [dbo].[Outfalls]  WITH CHECK ADD  CONSTRAINT [FK_Outfalls_Watersheds] FOREIGN KEY([WatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Outfalls] CHECK CONSTRAINT [FK_Outfalls_Watersheds]
GO
ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Customers]
GO
ALTER TABLE [dbo].[ReceivingWaters]  WITH CHECK ADD  CONSTRAINT [FK_ReceivingWaters_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[ReceivingWaters] CHECK CONSTRAINT [FK_ReceivingWaters_Customers]
GO
ALTER TABLE [dbo].[StructureControlTypes]  WITH CHECK ADD  CONSTRAINT [FK_StructureControlTypes_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[StructureControlTypes] CHECK CONSTRAINT [FK_StructureControlTypes_Customers]
GO
ALTER TABLE [dbo].[StructureInspections]  WITH CHECK ADD  CONSTRAINT [FK_StructureInspections_Structures] FOREIGN KEY([StructureId])
REFERENCES [dbo].[Structures] ([Id])
GO
ALTER TABLE [dbo].[StructureInspections] CHECK CONSTRAINT [FK_StructureInspections_Structures]
GO
ALTER TABLE [dbo].[StructureInspections]  WITH CHECK ADD  CONSTRAINT [structureinspections_customformtemplateid_foreign] FOREIGN KEY([CustomFormTemplateId])
REFERENCES [dbo].[CustomFormTemplates] ([Id])
GO
ALTER TABLE [dbo].[StructureInspections] CHECK CONSTRAINT [structureinspections_customformtemplateid_foreign]
GO
ALTER TABLE [dbo].[StructureInspections]  WITH CHECK ADD  CONSTRAINT [structureinspections_inspectiontypeid_foreign] FOREIGN KEY([InspectionTypeId])
REFERENCES [dbo].[InspectionType] ([Id])
GO
ALTER TABLE [dbo].[StructureInspections] CHECK CONSTRAINT [structureinspections_inspectiontypeid_foreign]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_Communities] FOREIGN KEY([CommunityId])
REFERENCES [dbo].[Communities] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_Communities]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_Customers]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_InspectionFrequencies] FOREIGN KEY([InspectionFrequencyId])
REFERENCES [dbo].[InspectionFrequencies] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_InspectionFrequencies]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_ReceivingWaters] FOREIGN KEY([ReceivingWatersId])
REFERENCES [dbo].[ReceivingWaters] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_ReceivingWaters]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_SubWatersheds] FOREIGN KEY([SubwatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_SubWatersheds]
GO
ALTER TABLE [dbo].[Structures]  WITH CHECK ADD  CONSTRAINT [FK_Structures_Watersheds] FOREIGN KEY([WatershedId])
REFERENCES [dbo].[Watersheds] ([Id])
GO
ALTER TABLE [dbo].[Structures] CHECK CONSTRAINT [FK_Structures_Watersheds]
GO
ALTER TABLE [dbo].[UsersCustomers]  WITH CHECK ADD  CONSTRAINT [FK_UsersCustomers_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[UsersCustomers] CHECK CONSTRAINT [FK_UsersCustomers_Customers]
GO
ALTER TABLE [dbo].[UsersCustomers]  WITH CHECK ADD  CONSTRAINT [FK_UsersCustomers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UsersCustomers] CHECK CONSTRAINT [FK_UsersCustomers_Users]
GO
ALTER TABLE [dbo].[Watersheds]  WITH CHECK ADD  CONSTRAINT [FK_Watersheds_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Watersheds] CHECK CONSTRAINT [FK_Watersheds_Customers]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [Features_JSON] CHECK  ((isjson([Features])=(1)))
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [Features_JSON]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [HiddenFields_JSON] CHECK  ((isjson([HiddenFields])=(1)))
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [HiddenFields_JSON]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'State or Customer custom field type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomFields', @level2type=N'COLUMN',@level2name=N'CustomFieldType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ConstructionSite, Structure, etc...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustomFields', @level2type=N'COLUMN',@level2name=N'EntityType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key of the entity that this is related to.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Files'
GO
USE [master]
GO
ALTER DATABASE [cbi_ms4web] SET  READ_WRITE 
GO
