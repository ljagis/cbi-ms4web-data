import * as Knex from 'knex';
const customFieldsTableName = 'CustomFields';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customFieldsTableName, table => {
    table.string('DefaultValue');
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customFieldsTableName, table => {
    table.dropColumn('DefaultValue');
  });
};
