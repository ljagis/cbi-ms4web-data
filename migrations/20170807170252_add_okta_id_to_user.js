exports.up = function(knex, Promise) {
  return knex.schema.table('Users', table => {
    table.string('OktaId', 999);
  });
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('Users', table => {
      table.dropColumn('OktaId');
    }),
  ]);
};
