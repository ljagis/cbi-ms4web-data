import * as Knex from 'knex';
const customFieldsTableName = 'CustomFields';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customFieldsTableName, table => {
    table.string('SectionName', 2000);
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customFieldsTableName, table => {
    table.dropColumns('SectionName');
  });
};
