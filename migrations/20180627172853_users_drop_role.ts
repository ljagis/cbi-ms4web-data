import * as Knex from 'knex';

exports.up = async function(knex: Knex): Promise<any> {
  const script = `
  ALTER TABLE users
    DROP COLUMN role
  `;
  await knex.raw(script);
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};
