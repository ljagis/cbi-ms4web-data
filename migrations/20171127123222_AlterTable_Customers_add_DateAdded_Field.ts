import * as Knex from 'knex';
const customerTableName = 'Customers';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customerTableName, table => {
    table
      .dateTime('DateAdded')
      .notNullable()
      .defaultTo(knex.raw('(getutcdate())'));
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.table(customerTableName, table => {
    table.dropColumn('DateAdded');
  });
};
