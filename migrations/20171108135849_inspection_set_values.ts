import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
import {
  Customer,
  CustomField,
  ConstructionSiteInspection,
  ConstructionSite,
  CustomFieldValue,
  FacilityInspection,
  Facility,
  OutfallInspection,
  Outfall,
  StructureInspection,
  Structure,
  CitizenReport,
  IllicitDischarge,
} from '../src/models';
import { getSqlFields2 } from '../src/decorators/decorator-helpers';
import { EntityTypes } from '../src/shared/entity-types';
import * as moment from 'moment';
import { objectToSqlData } from '../src/shared/object-to-sql';

const batchInsertChunkSize = 30;

exports.up = async function(knex: Knex): Bluebird<any> {
  // no typings for alterTable
  await knex.raw(`
    ALTER TABLE CustomFieldValues
    ALTER COLUMN [VALUE] NVARCHAR(MAX)
  `);

  const customers: Customer[] = await knex(Customer.tableName).select(
    getSqlFields2(Customer, { asProperty: true })
  );

  const customFields: CustomField[] = await knex(CustomField.tableName)
    .where({
      [CustomField.sqlField(f => f.isDeleted)]: 0,
      [CustomField.sqlField(f => f.customFieldType)]: 'Customer',
    })
    .select(getSqlFields2(CustomField, { asProperty: true }));

  await Bluebird.each(customers, async customer => {
    await addCSInspectionValues(
      knex,
      customer,
      customFields.filter(
        cf => cf.entityType === EntityTypes.ConstructionSiteInspection
      )
    );
    await addFacilityInspectionValues(
      knex,
      customer,
      customFields.filter(
        cf => cf.entityType === EntityTypes.FacilityInspection
      )
    );
    await addOutfallInspectionValues(
      knex,
      customer,
      customFields.filter(cf => cf.entityType === EntityTypes.OutfallInspection)
    );
    await addStructureInspectionValues(
      knex,
      customer,
      customFields.filter(
        cf => cf.entityType === EntityTypes.StructureInspection
      )
    );
    await addCitizenReportValues(
      knex,
      customer,
      customFields.filter(cf => cf.entityType === EntityTypes.CitizenReport)
    );
    await addIllicitDischargeValues(
      knex,
      customer,
      customFields.filter(cf => cf.entityType === EntityTypes.IllicitDischarge)
    );
  });

  // throw Error('ROLLBACK');
};

exports.down = async function(knex: Knex): Bluebird<any> {
  // good luck
};

async function addCSInspectionValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const csInspections: ConstructionSiteInspection[] = await knex(
    ConstructionSiteInspection.tableName
  )
    .join(
      ConstructionSite.tableName,
      ConstructionSite.sqlField(f => f.id, true),
      ConstructionSiteInspection.sqlField(f => f.constructionSiteId, true)
    )
    .where({
      [ConstructionSite.sqlField(f => f.customerId)]: customer.id,
    })
    .select(
      getSqlFields2(ConstructionSiteInspection, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  csInspections.forEach(inspection => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: cf('Is Site Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSiteActive),
      },
      {
        customFieldId: cf('Is Site Permitted').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSitePermitted),
      },
      {
        customFieldId: cf('Acceptable Erosion Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areErosionControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Local Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areLocalControlsAcceptable),
      },
      {
        customFieldId: cf('Has Plan On Site').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isPlanOnSite),
      },
      {
        customFieldId: cf('Acceptable Non-stormwater Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areNonStormWaterControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Outfall Velocity Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areOutfallVelocityControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Stabilization Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areStabilizationControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Structural Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areStructuralControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Tracking Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areTrackingControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Maintenance of Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isMaintenanceOfControlsAcceptable),
      },
      {
        customFieldId: cf('Acceptable Waste Management').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isWasteManagementAcceptable),
      },
      {
        customFieldId: cf('Has Current Plan Records').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.arePlanRecordsCurrent),
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

async function addCitizenReportValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const crs: CitizenReport[] = await knex(CitizenReport.tableName)
    .where({ [CitizenReport.sqlField(f => f.customerId)]: customer.id })
    .select(
      getSqlFields2(CitizenReport, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  crs.forEach(cr => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === cr.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Phone Number').id,
        globalIdFk: cr.globalId,
        value: cr.phoneNumber,
      },
      {
        customFieldId: cf('Cell Number').id,
        globalIdFk: cr.globalId,
        value: cr.cellNumber,
      },
      {
        customFieldId: cf('Fax Number').id,
        globalIdFk: cr.globalId,
        value: cr.faxNumber,
      },
      {
        customFieldId: cf('Response Date').id,
        globalIdFk: cr.globalId,
        value: dateToStringOrNull(cr.responseDate),
      },
      {
        customFieldId: cf('Response').id,
        globalIdFk: cr.globalId,
        value: cr.response,
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

async function addIllicitDischargeValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const crs: IllicitDischarge[] = await knex(IllicitDischarge.tableName)
    .where({ [IllicitDischarge.sqlField(f => f.customerId)]: customer.id })
    .select(
      getSqlFields2(IllicitDischarge, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  crs.forEach(cr => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === cr.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Date Eliminated').id,
        globalIdFk: cr.globalId,
        value: dateToStringOrNull(cr.dateEliminated),
      },
      {
        customFieldId: cf('Phone Number').id,
        globalIdFk: cr.globalId,
        value: cr.phoneNumber,
      },
      {
        customFieldId: cf('Cell Number').id,
        globalIdFk: cr.globalId,
        value: cr.cellNumber,
      },
      {
        customFieldId: cf('Fax Number').id,
        globalIdFk: cr.globalId,
        value: cr.faxNumber,
      },
      {
        customFieldId: cf('Conversation').id,
        globalIdFk: cr.globalId,
        value: cr.conversation,
      },
      {
        customFieldId: cf('Corrective Action').id,
        globalIdFk: cr.globalId,
        value: cr.requestCorrectiveAction,
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

async function addStructureInspectionValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const inspections: StructureInspection[] = await knex(
    StructureInspection.tableName
  )
    .join(
      Structure.tableName,
      Structure.sqlField(f => f.id, true),
      StructureInspection.sqlField(f => f.structureId, true)
    )
    .where({
      [Structure.sqlField(f => f.customerId)]: customer.id,
    })
    .select(
      getSqlFields2(StructureInspection, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  inspections.forEach(inspection => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: cf('Is Control Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isControlActive),
      },
      {
        customFieldId: cf('Are Washouts Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areWashoutsPresent),
      },
      {
        customFieldId: cf('Removal of Floatables Required').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.floatablesRemovalRequired),
      },
      {
        customFieldId: cf('Is Built within Specification').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isBuiltWithinSpecification),
      },
      {
        customFieldId: cf('Is Depth of Sediment Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDepthOfSedimentAcceptable),
      },
      {
        customFieldId: cf('Is Downstream Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDownstreamErosionPresent),
      },
      {
        customFieldId: cf('Is Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isErosionPresent),
      },
      {
        customFieldId: cf('Is Illict Discharge Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isIllicitDischargePresent),
      },
      {
        customFieldId: cf('Is Outlet Clogged').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isOutletClogged),
      },
      {
        customFieldId: cf('Is Return Inspection Recommended').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isReturnInspectionRecommended),
      },
      {
        customFieldId: cf('Is Standing Water Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isStandingWaterPresent),
      },
      {
        customFieldId: cf('Has Structural Damage').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isStructuralDamage),
      },
      {
        customFieldId: cf('Requires Maintenance').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.requiresMaintenance),
      },
      {
        customFieldId: cf('Requires Repairs').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.requiresRepairs),
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

async function addFacilityInspectionValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const inspections: FacilityInspection[] = await knex(
    FacilityInspection.tableName
  )
    .join(
      Facility.tableName,
      Facility.sqlField(f => f.id, true),
      FacilityInspection.sqlField(f => f.facilityId, true)
    )
    .where({
      [Facility.sqlField(f => f.customerId)]: customer.id,
    })
    .select(
      getSqlFields2(FacilityInspection, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  inspections.forEach(inspection => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: cf('Is Facility Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isFacilityActive),
      },
      {
        customFieldId: cf('Is Facility Permitted').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isFacilityPermitted),
      },
      {
        customFieldId: cf('Is Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isErosionPresent),
      },
      {
        customFieldId: cf('Is Downstream Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDownstreamErosionPresent),
      },
      {
        customFieldId: cf('Are Floatables Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areFloatablesPresent),
      },
      {
        customFieldId: cf('Are Illicit Discharges Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areIllicitDischargesPresent),
      },
      {
        customFieldId: cf('Are Non-Stormwater Disharges Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areNonStormWaterDischargePresent),
      },
      {
        customFieldId: cf('Are Washouts Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areWashoutsPresent),
      },
      {
        customFieldId: cf('Are Records Current').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areRecordsCurrent),
      },
      {
        customFieldId: cf('Are Sampling Data Evaluation Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areSamplingDataEvaluationAcceptable),
      },
      {
        customFieldId: cf('Is BMP Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isBmpAcceptable),
      },
      {
        customFieldId: cf('Is Good House Keeping').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isGoodHouseKeeping),
      },
      {
        customFieldId: cf('Is Return Inspection Required').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isReturnInspectionRequired),
      },
      {
        customFieldId: cf('Is SWPP On Site').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSwppOnSite),
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

async function addOutfallInspectionValues(
  knex: Knex,
  customer: Customer,
  customFields: CustomField[]
) {
  const inspections: OutfallInspection[] = await knex(
    OutfallInspection.tableName
  )
    .join(
      Outfall.tableName,
      Outfall.sqlField(f => f.id, true),
      OutfallInspection.sqlField(f => f.outfallId, true)
    )
    .where({
      [Outfall.sqlField(f => f.customerId)]: customer.id,
    })
    .select(
      getSqlFields2(OutfallInspection, {
        includeTable: true,
        asProperty: true,
        includeInternalField: true,
      })
    );

  let cfValues: Partial<CustomFieldValue>[] = [];
  inspections.forEach(inspection => {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    cfValues = [
      ...cfValues,
      {
        customFieldId: cf('Dry or Wet Weather').id,
        globalIdFk: inspection.globalId,
        value: inspection.dryOrWetWeatherInspection,
      },
      {
        customFieldId: cf('Days Since Last Rain').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.daysSinceLastRain),
      },
      {
        customFieldId: cf('Color').id,
        globalIdFk: inspection.globalId,
        value: inspection.color,
      },
      {
        customFieldId: cf('Clarity').id,
        globalIdFk: inspection.globalId,
        value: inspection.clarity,
      },
      {
        customFieldId: cf('Odor').id,
        globalIdFk: inspection.globalId,
        value: inspection.odor,
      },
      {
        customFieldId: cf('Foam').id,
        globalIdFk: inspection.globalId,
        value: inspection.foam,
      },
      {
        customFieldId: cf('Sheen').id,
        globalIdFk: inspection.globalId,
        value: inspection.sheen,
      },
      {
        customFieldId: cf('Suspended Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.sustainedSolids,
      },
      {
        customFieldId: cf('Settled Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.setSolids,
      },
      {
        customFieldId: cf('Floating Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.floatingSolids,
      },
      {
        customFieldId: cf('PH').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.pH),
      },
      {
        customFieldId: cf('Temperature(F)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.temperatureF),
      },
      {
        customFieldId: cf('DO (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.domgl),
      },
      {
        customFieldId: cf('Turbidity (NTU)').id,
        globalIdFk: inspection.globalId,
        value: inspection.turbidity,
      },
      {
        customFieldId: cf('Cond (mOhms)').id,
        globalIdFk: inspection.globalId,
        value: inspection.cond,
      },
      {
        customFieldId: cf('DO (%Sat)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.doSaturation),
      },
      {
        customFieldId: cf('Flowrate (GPM)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.flowrateGpm),
      },
      {
        customFieldId: cf('Copper (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.copper),
      },
      {
        customFieldId: cf('Phenols (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.phenols),
      },
      {
        customFieldId: cf('Ammonia (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.ammonia),
      },
      {
        customFieldId: cf('Detergents (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.detergents),
      },
      {
        customFieldId: cf('T.PO4 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.tpo4),
      },
      {
        customFieldId: cf('Cl2 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.cl2),
      },
      {
        customFieldId: cf('BOD (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.bod),
      },
      {
        customFieldId: cf('COD (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.cod),
      },
      {
        customFieldId: cf('TSS (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.tss),
      },
      {
        customFieldId: cf('NO3 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.no3),
      },
      {
        customFieldId: cf('Fecal Coliform (col/100mL)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.fecalColiform),
      },
      {
        customFieldId: cf('E. Coli (col/100mL)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.ecoli),
      },
      {
        customFieldId: cf('Discharge Description').id,
        globalIdFk: inspection.globalId,
        value: inspection.dischargeDescription,
      },
    ];
  });
  const rowsToInsert = cfValues.map(v =>
    objectToSqlData(CustomFieldValue, v, knex)
  );
  await knex.batchInsert(
    CustomFieldValue.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
}

function dateToStringOrNull(date: Date): string {
  if (!date) {
    return null;
  }
  const m = moment.utc(date);
  return m.isValid() ? m.toISOString() : null;
}

function booleanToString(b: boolean): string {
  if (b === true) {
    return 'true';
  }
  if (b === false) {
    return 'false';
  }
  return null;
}

function numberToString(num: number): string {
  if (num === null || num === undefined) {
    return null;
  }
  return num.toString();
}
