import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

const customFormTemplateTableName = 'CustomFormTemplates';

exports.up = async function(knex: Knex, Promise: Bluebird<any>) {
  // Create CustomFormTemplates table
  await knex.schema.createTableIfNotExists(
    customFormTemplateTableName,
    table => {
      table.increments('Id').primary();
      table
        .specificType('CustomerId', 'varchar(50)')
        .notNullable()
        .index()
        .references('Id')
        .inTable('Customers');

      table.string('EntityType', 255).notNullable();
      table.specificType('Name', 'nvarchar(max)');
      table
        .boolean('IsDeleted')
        .notNullable()
        .defaultTo(0);
    }
  );

  // Alter CustomFields
  await knex.schema.table('CustomFields', table => {
    table
      .integer('CustomFormTemplateId')
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter ConstructionSiteInspections
  await knex.schema.table('ConstructionSiteInspections', table => {
    table
      .integer('CustomFormTemplateId')
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter FacilityInspections
  await knex.schema.table('FacilityInspections', table => {
    table
      .integer('CustomFormTemplateId')
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter OutfallInspections
  await knex.schema.table('OutfallInspections', table => {
    table
      .integer('CustomFormTemplateId') // TODO: Add not null
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter StructureInspections
  await knex.schema.table('StructureInspections', table => {
    table
      .integer('CustomFormTemplateId') // TODO: Add not null
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter CitizenReports
  await knex.schema.table('CitizenReports', table => {
    table
      .integer('CustomFormTemplateId') // TODO: Add not null
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });

  // Alter IllicitDischarges
  await knex.schema.table('IllicitDischarges', table => {
    table
      .integer('CustomFormTemplateId') // TODO: Add not null
      .index()
      .references('Id')
      .inTable(customFormTemplateTableName);
  });
};

exports.down = async function(knex: Knex, Promise: Promise<any>) {
  // DELETE constraints/indexes
  await knex.schema.table('ConstructionSiteInspections', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });
  await knex.schema.table('FacilityInspections', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });
  await knex.schema.table('OutfallInspections', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });
  await knex.schema.table('StructureInspections', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });
  await knex.schema.table('CitizenReports', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });
  await knex.schema.table('IllicitDischarges', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });

  await knex.schema.table('CustomFields', table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');
  });

  await knex.schema.table(customFormTemplateTableName, table => {
    table.dropIndex(['CustomerId']);
  });
  await knex.schema.dropTable(customFormTemplateTableName);
};
