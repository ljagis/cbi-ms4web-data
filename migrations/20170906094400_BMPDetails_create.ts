import * as Knex from 'knex';
const BMP_DETAILS = 'BMPDetails';
const BMP_ACTIVITY_LOGS = 'BMPActivityLogs';

exports.up = async function(knex: Knex): Promise<any> {
  await createBmpDetails(knex);
  await createBmpActivityLogs(knex);
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.dropTable(BMP_ACTIVITY_LOGS);
  await knex.schema.dropTable(BMP_DETAILS);
};

async function createBmpDetails(knex: Knex) {
  await knex.schema.createTable(BMP_DETAILS, table => {
    table.increments('Id').primary();

    table
      .uuid('GlobalId')
      .index()
      .defaultTo(knex.raw('(newid())'));

    table
      .integer('ControlMeasureId')
      .notNullable()
      .index()
      .references('Id')
      .inTable('BMPControlMeasures');

    table.string('Name', 1000);
    table.specificType('Description', 'nvarchar(MAX)').nullable();
    table.dateTime('DateAdded').defaultTo(knex.raw('(getutcdate())'));
    table.dateTime('DueDate').nullable();
    table.dateTime('CompletionDate').nullable();

    table.boolean('IsDeleted').defaultTo(0);
  });
}

async function createBmpActivityLogs(knex: Knex) {
  await knex.schema.createTable(BMP_ACTIVITY_LOGS, table => {
    table.increments('Id').primary();
    table
      .integer('BmpDetailId')
      .notNullable()
      .index()
      .references('Id')
      .inTable(BMP_DETAILS);
    table.dateTime('DateAdded').defaultTo(knex.raw('(getutcdate())'));
    table.dateTime('ActivityDate').notNullable();
    table.string('DataType', 800);
    table.specificType('Quantity', 'numeric(18,2)');
    table.specificType('Comments', 'nvarchar(MAX)');
    table.boolean('IsDeleted').defaultTo(0);
  });
}
