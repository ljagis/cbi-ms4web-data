exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('Customers', table => {
      table.string('Plan');
    }),
    knex('Customers').update('Plan', 'premium'),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('Customers', table => {
      table.dropColumn('Plan');
    }),
  ]);
};
