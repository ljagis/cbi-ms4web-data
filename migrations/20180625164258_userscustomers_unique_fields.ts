import * as Knex from 'knex';

exports.up = async function(knex: Knex): Promise<any> {
  const query = `
  ALTER TABLE UsersCustomers
  add CONSTRAINT UQ_UsersCustomers_UserId_CustomerId UNIQUE(UserId, CustomerId)
  `;
  await knex.raw(query);
};

exports.down = async function(_knex: Knex): Promise<any> {
  //
};
