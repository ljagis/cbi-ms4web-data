import * as Knex from 'knex';

const STATES_TABLE = 'STATES';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(STATES_TABLE, table => {
    table.specificType('CountryCode', 'varchar(10)');
  });

  // update all current states to US
  await knex.table(STATES_TABLE).update({
    CountryCode: 'US',
  });

  await knex.schema.table(STATES_TABLE, table => {
    table
      .string('CountryCode')
      .notNullable()
      .alter();
  });

  await knex.raw(
    `ALTER TABLE ${STATES_TABLE} ALTER COLUMN Abbr varchar(10) NOT NULL`
  );

  // add candata states to states table
  const canadaData = getCanadaProvinces().map(p => ({
    Name: p.name,
    Abbr: p.abbreviation,
    CountryCode: 'CA',
  }));
  // add aus states
  const ausData = getAustraliaStates().map(p => ({
    Name: p.name,
    Abbr: p.abbreviation,
    CountryCode: 'AU',
  }));

  await knex.batchInsert(STATES_TABLE, [...canadaData, ...ausData], 1000);
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};

function getCanadaProvinces() {
  return [
    {
      name: 'Alberta',
      abbreviation: 'AB',
    },
    {
      name: 'British Columbia',
      abbreviation: 'BC',
    },
    {
      name: 'Manitoba',
      abbreviation: 'MB',
    },
    {
      name: 'New Brunswick',
      abbreviation: 'NB',
    },
    {
      name: 'Newfoundland and Labrador',
      abbreviation: 'NL',
    },
    {
      name: 'Northwest Territories',
      abbreviation: 'NT',
    },
    {
      name: 'Nova Scotia',
      abbreviation: 'NS',
    },
    {
      name: 'Nunavut',
      abbreviation: 'NU',
    },
    {
      name: 'Ontario',
      abbreviation: 'ON',
    },
    {
      name: 'Prince Edward Island',
      abbreviation: 'PE',
    },
    {
      name: 'Quebec',
      abbreviation: 'QC',
    },
    {
      name: 'Saskatchewan',
      abbreviation: 'SK',
    },
    {
      name: 'Yukon Territory',
      abbreviation: 'YT',
    },
  ];
}

function getAustraliaStates() {
  return [
    {
      name: 'New South Wales',
      abbreviation: 'NSW',
    },
    {
      name: 'Victoria',
      abbreviation: 'VIC',
    },
    {
      name: 'Queensland',
      abbreviation: 'QLD',
    },
    {
      name: 'Tasmania',
      abbreviation: 'TAS',
    },
    {
      name: 'South Australia',
      abbreviation: 'SA',
    },
    {
      name: 'Western Australia',
      abbreviation: 'WA',
    },
    {
      name: 'Northern Territory',
      abbreviation: 'NT',
    },
    {
      name: 'Australian Capital Territory',
      abbreviation: 'ACT',
    },
  ];
}
