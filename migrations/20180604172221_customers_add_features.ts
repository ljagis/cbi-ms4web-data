import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.raw(`
  ALTER TABLE Customers ADD Features NVARCHAR(4000) NOT NULL
  CONSTRAINT [Features_JSON] CHECK (isjson(Features)=1)
  DEFAULT '{}'
  `);
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
