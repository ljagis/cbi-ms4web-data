import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
import { EntityTypes } from '../src/shared/entity-types';
import {
  CustomField,
  ConstructionSiteInspection,
  Facility,
  FacilityInspection,
  ConstructionSite,
  OutfallInspection,
  Outfall,
  StructureInspection,
  Structure,
  CitizenReport,
  IllicitDischarge,
  InspectionType,
} from '../src/models';
import { objectToSqlData } from '../src/shared/object-to-sql';
import { dropDFConstraint } from '../src/shared/drop-constraint';
import { validateModel } from '../src/shared/validate';
import { getSqlFields2 } from '../src/decorators/decorator-helpers';

const customFormTemplateTableName = 'CustomFormTemplates';

interface Customer {
  Id: string;
}

const batchInsertChunkSize = 100;

const entities = [
  CitizenReport,
  IllicitDischarge,
  ConstructionSiteInspection,
  FacilityInspection,
  OutfallInspection,
  StructureInspection,
];

// prettier-ignore
const templateNameMapping = {
  [EntityTypes.ConstructionSiteInspection]: 'MS4web Default Construction Site Inspection',
  [EntityTypes.FacilityInspection]: 'MS4web Default Facility Inspection',
  [EntityTypes.OutfallInspection]: 'MS4web Default Outfall Inspection',
  [EntityTypes.StructureInspection]: 'MS4web Default Structural Control Inspection',
  [EntityTypes.CitizenReport]: 'MS4web Default Citizen Report',
  [EntityTypes.IllicitDischarge]: 'MS4web Default Illicit Discharge',
};

//#region up
exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.schema.table(customFormTemplateTableName, table => {
    table
      .boolean('IsSystem')
      .notNullable()
      .defaultTo(0);
  });

  // Add InspectionTypeId to inspections/investigations
  await Bluebird.each(entities, async entity => {
    await knex.schema.table(entity.tableName, table => {
      table
        .integer('InspectionTypeId')
        .index()
        .references('Id')
        .inTable('InspectionType');
    });
  });

  // Add default template data
  const customers: Customer[] = await knex('Customers');
  await Bluebird.each(customers, async customer => {
    await createTemplateForCSInspection(knex, customer);
    await createTemplateForFacilityInspection(knex, customer);
    await createTemplateForOutfallInspection(knex, customer);
    await createTemplateForStructureInspection(knex, customer);

    await createTemplateForCitizenReport(knex, customer);
    await createTemplateForIllicitDischarge(knex, customer);
  });
};
//#endregion

//#region down
exports.down = async function(knex: Knex): Bluebird<any> {
  // assign all existing CS inspections to null
  await knex(ConstructionSiteInspection.tableName)
    .join(
      ConstructionSite.tableName,
      ConstructionSite.sqlField(f => f.id, true),
      ConstructionSiteInspection.sqlField(f => f.constructionSiteId, true)
    )
    .update(
      ConstructionSiteInspection.sqlField(f => f.customFormTemplateId),
      null
    );

  await knex(FacilityInspection.tableName)
    .join(
      Facility.tableName,
      Facility.sqlField(f => f.id, true),
      FacilityInspection.sqlField(f => f.facilityId, true)
    )
    .update(FacilityInspection.sqlField(f => f.customFormTemplateId), null);

  await knex(OutfallInspection.tableName)
    .join(
      Outfall.tableName,
      Outfall.sqlField(f => f.id, true),
      OutfallInspection.sqlField(f => f.outfallId, true)
    )
    .update(OutfallInspection.sqlField(f => f.customFormTemplateId), null);

  await knex(StructureInspection.tableName)
    .join(
      Structure.tableName,
      Structure.sqlField(f => f.id, true),
      StructureInspection.sqlField(f => f.structureId, true)
    )
    .update(StructureInspection.sqlField(f => f.customFormTemplateId), null);

  await knex(CitizenReport.tableName).update(
    CitizenReport.sqlField(f => f.customFormTemplateId),
    null
  );
  await knex(IllicitDischarge.tableName).update(
    IllicitDischarge.sqlField(f => f.customFormTemplateId),
    null
  );

  await knex(CustomField.tableName)
    .del()
    .where('CustomFormTemplateId', '>', 0);

  await knex(customFormTemplateTableName)
    .del()
    .whereIn(
      'Name',
      Object.keys(templateNameMapping).map(k => templateNameMapping[k])
    )
    .where({
      IsSystem: 1,
    });

  await dropDFConstraint(knex, customFormTemplateTableName, 'IsSystem');
  await knex.schema.table(customFormTemplateTableName, table => {
    table.dropColumn('IsSystem');
  });
};
//#endregion

//#region private functions
async function createTemplateForCSInspection(knex: Knex, customer: Customer) {
  const entityType = EntityTypes.ConstructionSiteInspection;
  const defaultCsTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Construction Site Inspection`,
      CustomFormTemplateId: defaultCsTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  await knex(InspectionType.tableName)
    .update({
      CustomFormTemplateId: defaultCsTemplateId,
      EntityType: entityType,
    })
    .where({
      CustomerId: customer.Id,
    });

  let fieldOrder = 0;

  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Date Resolved',
      dataType: 'Date',
      inputType: 'date',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Site Active',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Site Permitted',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Erosion Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Local Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Has Plan On Site',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Non-stormwater Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Outfall Velocity Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Stabilization Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Structural Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Tracking Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Maintenance of Controls',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Acceptable Waste Management',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Has Current Plan Records',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultCsTemplateId,
      entityType: entityType,
    }),
  ];

  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );

  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultCsTemplateId,
    fieldOrder,
    null
  );

  // assign all existing CS inspections to this template
  await knex('ConstructionSiteInspections')
    .join(
      'ConstructionSites',
      'ConstructionSites.Id',
      'ConstructionSiteInspections.ConstructionSiteId'
    )
    .update('CustomFormTemplateId', defaultCsTemplateId)
    .update('InspectionTypeId', inspectionTypeId)
    .where('ConstructionSites.CustomerId', customer.Id);

  const inspectionTypes: any[] = await knex('InspectionType').where({
    CustomerId: customer.Id,
    EntityType: EntityTypes.ConstructionSiteInspection,
  });
  await Bluebird.each(inspectionTypes, async inspectionType => {
    await knex('ConstructionSiteInspections')
      .join(
        'ConstructionSites',
        'ConstructionSites.Id',
        'ConstructionSiteInspections.ConstructionSiteId'
      )
      .update('InspectionTypeId', inspectionType.Id)
      .where('ConstructionSites.CustomerId', customer.Id)
      .where('ConstructionSiteInspections.InspectionType', inspectionType.Name);
  });
}

async function createTemplateForFacilityInspection(
  knex: Knex,
  customer: Customer
) {
  const entityType = EntityTypes.FacilityInspection;
  const defaultTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Facility Inspection`,
      CustomFormTemplateId: defaultTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  let fieldOrder = 0;

  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Date Resolved',
      dataType: 'Date',
      inputType: 'date',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Facility Active',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Facility Permitted',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Erosion Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Downstream Erosion Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Floatables Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Illicit Discharges Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Non-Stormwater Disharges Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Washouts Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Records Current',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Sampling Data Evaluation Acceptable',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is BMP Acceptable',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Good House Keeping',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Return Inspection Required',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is SWPP On Site',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
  ];

  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultTemplateId,
    fieldOrder,
    null
  );

  await knex('FacilityInspections')
    .join('Facilities', 'Facilities.Id', 'FacilityInspections.FacilityId')
    .update('CustomFormTemplateId', defaultTemplateId)
    .update('InspectionTypeId', inspectionTypeId)
    .where('Facilities.CustomerId', customer.Id);
}

async function createTemplateForStructureInspection(
  knex: Knex,
  customer: Customer
) {
  const entityType = EntityTypes.StructureInspection;
  const defaultTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Structure Inspection`,
      CustomFormTemplateId: defaultTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  let fieldOrder = 0;
  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Date Resolved',
      dataType: 'Date',
      inputType: 'date',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Control Active',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Are Washouts Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Removal of Floatables Required',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Built within Specification',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Depth of Sediment Acceptable',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'true',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Downstream Erosion Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Erosion Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Illict Discharge Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Outlet Clogged',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Return Inspection Recommended',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Is Standing Water Present',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Has Structural Damage',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Requires Maintenance',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Requires Repairs',
      dataType: 'boolean',
      inputType: 'checkbox',
      defaultValue: 'false',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
  ];

  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultTemplateId,
    fieldOrder,
    null
  );

  await knex('StructureInspections')
    .join('Structures', 'Structures.Id', 'StructureInspections.StructureId')
    .update('CustomFormTemplateId', defaultTemplateId)
    .where('Structures.CustomerId', customer.Id)
    .update('InspectionTypeId', inspectionTypeId);
}

async function createTemplateForOutfallInspection(
  knex: Knex,
  customer: Customer
) {
  const entityType = EntityTypes.OutfallInspection;
  const defaultTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Outfall Inspection`,
      CustomFormTemplateId: defaultTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  let fieldOrder = 0;
  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Dry or Wet Weather',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Dry|Wet',
      defaultValue: 'Dry',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Days Since Last Rain',
      dataType: 'number',
      inputType: 'text',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Color',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Clear|Light Gray|Brown|Tan|Dark Grey',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Clarity',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Clear|Transparent|Murky|Opaque',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Odor',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Sewer|Sulfur|Chemical|Rotten Eggs|Unknown',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Foam',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Light|Medium|Heavy',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Sheen',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Light|Medium|Heavy',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Suspended Solids',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Light|Medium|Heavy',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Settled Solids',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Light|Medium|Heavy',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Floating Solids',
      dataType: 'string',
      inputType: 'select',
      fieldOptions: 'Light|Medium|Heavy',
      sectionName: '1. Visual Observations',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'PH',
      dataType: 'string',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Temperature(F)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'DO (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Turbidity (NTU)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Cond (mOhms)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'DO (%Sat)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Flowrate (GPM)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Copper (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Phenols (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Ammonia (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Detergents (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'T.PO4 (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Cl2 (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'BOD (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'COD (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'TSS (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'NO3 (mg/L)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Fecal Coliform (col/100mL)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'E. Coli (col/100mL)',
      dataType: 'number',
      inputType: 'text',
      sectionName: '2. Test Results',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Discharge Description',
      dataType: 'string',
      inputType: 'textarea',
      sectionName: '3. Discharge Description',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
  ];

  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultTemplateId,
    fieldOrder,
    '4. Custom'
  );

  await knex(OutfallInspection.tableName)
    .join(Outfall.tableName, 'Outfalls.Id', 'OutfallInspections.OutfallId')
    .update('CustomFormTemplateId', defaultTemplateId)
    .update('InspectionTypeId', inspectionTypeId)
    .where('Outfalls.CustomerId', customer.Id);
}

async function createTemplateForCitizenReport(knex: Knex, customer: Customer) {
  const entityType = EntityTypes.CitizenReport;
  const defaultTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Citizen Report`,
      CustomFormTemplateId: defaultTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  let fieldOrder = 0;

  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Phone Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Cell Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Fax Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Response Date',
      dataType: 'Date',
      inputType: 'date',
      sectionName: '2. Response',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Response',
      dataType: 'string',
      inputType: 'textarea',
      sectionName: '2. Response',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
  ];
  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultTemplateId,
    fieldOrder,
    '4. Custom'
  );

  await knex(CitizenReport.tableName)
    .update(
      CitizenReport.sqlField(f => f.customFormTemplateId),
      defaultTemplateId
    )
    .update(CitizenReport.sqlField(f => f.inspectionTypeId), inspectionTypeId)
    .where(CitizenReport.sqlField(f => f.customerId), customer.Id);
}

async function createTemplateForIllicitDischarge(
  knex: Knex,
  customer: Customer
) {
  const entityType = EntityTypes.IllicitDischarge;
  const defaultTemplateId = await knex(customFormTemplateTableName)
    .insert({
      EntityType: entityType,
      CustomerId: customer.Id,
      IsSystem: 1,
      Name: templateNameMapping[entityType],
    })
    .returning('Id');

  const inspectionTypeId = await knex(InspectionType.tableName)
    .insert({
      CustomerId: customer.Id,
      Name: `Default Illicit Discharge`,
      CustomFormTemplateId: defaultTemplateId,
      EntityType: entityType,
    })
    .returning('Id');

  let fieldOrder = 0;

  const rowsToInsert = [
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Date Eliminated',
      dataType: 'Date',
      inputType: 'date',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),

    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Phone Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Cell Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Fax Number',
      dataType: 'string',
      inputType: 'text',
      sectionName: '1. Contact Information',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Conversation',
      dataType: 'string',
      inputType: 'textarea',
      sectionName: '2. Conversation',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
    await createCustomFieldSqlData(knex, {
      fieldLabel: 'Corrective Action',
      dataType: 'string',
      inputType: 'textarea',
      sectionName: '3. Corrective Action',
      fieldOrder: ++fieldOrder,
      customerId: customer.Id,
      customFormTemplateId: defaultTemplateId,
      entityType: entityType,
    }),
  ];
  await knex.batchInsert(
    CustomField.tableName,
    rowsToInsert,
    batchInsertChunkSize
  );
  await associateCustomFieldsToTemplateId(
    knex,
    customer,
    entityType,
    defaultTemplateId,
    fieldOrder,
    '4. Custom'
  );

  await knex(IllicitDischarge.tableName)
    .update(
      IllicitDischarge.sqlField(f => f.customFormTemplateId),
      defaultTemplateId
    )
    .update(
      IllicitDischarge.sqlField(f => f.inspectionTypeId),
      inspectionTypeId
    )
    .where(IllicitDischarge.sqlField(f => f.customerId), customer.Id);
}

async function createCustomFieldSqlData(knex: Knex, cf: Partial<CustomField>) {
  const customField = Object.assign(new CustomField(), cf);
  await validateModel(customField);
  Object.keys(customField).forEach(k => {
    if (customField[k] === undefined) {
      delete customField[k];
    }
  });
  return objectToSqlData(CustomField, customField, knex);
}

//#endregion

async function associateCustomFieldsToTemplateId(
  knex: Knex,
  customer: Customer,
  entityType: string,
  templateId: number,
  fieldOrder: number,
  sectionName: string
) {
  if (!templateId) {
    throw Error('templateId undefined');
  }
  const customFields: CustomField[] = await knex(CustomField.tableName)
    .where({
      [CustomField.sqlField(f => f.customerId)]: customer.Id,
      [CustomField.sqlField(f => f.entityType)]: entityType,
    })
    .whereNull(CustomField.sqlField(f => f.customFormTemplateId))
    .select(
      getSqlFields2(CustomField, { properties: ['id'], asProperty: true })
    );

  await Bluebird.each(customFields, async cf => {
    await knex(CustomField.tableName)
      .update({
        [CustomField.sqlField(f => f.customFormTemplateId)]: templateId,
        [CustomField.sqlField(f => f.fieldOrder)]: ++fieldOrder,
        [CustomField.sqlField(f => f.sectionName)]: sectionName,
      })
      .where({
        [CustomField.sqlField(f => f.id)]: cf.id,
      });
  });
}
