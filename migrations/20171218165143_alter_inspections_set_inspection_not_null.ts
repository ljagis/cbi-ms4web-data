import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
exports.up = async function(knex: Knex): Bluebird<any> {
  const inspectionTables = [
    'ConstructionSiteInspections',
    'FacilityInspections',
    'OutfallInspections',
    'StructureInspections',
    'CitizenReports',
    'IllicitDischarges',
  ];
  await Bluebird.each(inspectionTables, async inspectionTableName => {
    // 1 remove index and foreign key constraintss
    await knex.schema.table(inspectionTableName, table => {
      table.dropForeign(['InspectionTypeId']);
      table.dropIndex(['InspectionTypeId']);
    });

    // 2 add not null constraint
    await knex.raw(
      `ALTER TABLE ${inspectionTableName} ALTER COLUMN [InspectionTypeId] INTEGER NOT NULL`
    );

    // 3 re-add the FK constraint and index
    await knex.schema.table(inspectionTableName, table => {
      table
        .foreign('InspectionTypeId')
        .references('Id')
        .inTable('InspectionType');
      table.index(['InspectionTypeId']);
    });
  });
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
