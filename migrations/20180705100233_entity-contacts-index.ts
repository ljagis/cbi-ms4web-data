import * as Knex from 'knex';

exports.up = async function(knex: Knex): Promise<any> {
  const script = `
  CREATE INDEX IX_EntityContacts_GlobalIdFk
    ON EntityContacts (GlobalIdFk);
  CREATE INDEX IX_EntityContacts_ContactId
    ON EntityContacts (ContactId);
  `;
  knex.raw(script);
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};
