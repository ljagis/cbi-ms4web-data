import * as Knex from 'knex';
const citizenReportsTableName = 'CitizenReports';
const illicitDischargesTableName = 'IllicitDischarges';
const followUpDateColumnName = 'FollowUpDate';

exports.up = async function(knex: Knex): Promise<any> {
  await addFollowUpDateColumn(knex, citizenReportsTableName);
  await addFollowUpDateColumn(knex, illicitDischargesTableName);
};

exports.down = async function(knex: Knex): Promise<any> {
  await removeColumn(knex, citizenReportsTableName, followUpDateColumnName);
  await removeColumn(knex, illicitDischargesTableName, followUpDateColumnName);
};

async function addFollowUpDateColumn(knex: Knex, tableName: string) {
  await knex.schema.table(tableName, table => {
    table.dateTime(followUpDateColumnName);
  });
}

async function removeColumn(knex: Knex, tableName: string, columnName: string) {
  await knex.schema.table(tableName, table => {
    table.dropColumn(columnName);
  });
}
