exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('Users', table => {
      table.string('Role');
    }),
    knex('Users').update('Role', 'admin'),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('Users', table => {
      table.dropColumn('Role');
    }),
  ]);
};
