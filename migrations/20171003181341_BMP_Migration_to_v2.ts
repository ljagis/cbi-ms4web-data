import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
import * as _ from 'lodash';

import {
  BmpDetail,
  BmpTask,
  BmpActivity,
  BmpActivityLog,
  FileEntity,
} from '../src/models';
import { getSqlFields2 } from '../src/decorators/decorator-helpers';
import { objectToSqlData, EntityTypes } from '../src/shared';

exports.up = async function(knex: Knex): Bluebird<any> {
  // const controlMeasures: BmpControlMeasure[] = await knex(BmpControlMeasure.getTableName())
  //   .where(BmpControlMeasure.sqlField(f => f.isDeleted), 0)
  //   .select(getSqlFields2(BmpControlMeasure, { asProperty: true, includeInternalField: true }));

  const bmpTasks: BmpTask[] = await knex(BmpTask.getTableName())
    .where(BmpTask.sqlField(f => f.isDeleted), 0)
    .select(
      getSqlFields2(BmpTask, { asProperty: true, includeInternalField: true })
    );

  const bmpActivities: BmpActivity[] = await knex(BmpActivity.getTableName())
    .where(BmpActivity.sqlField(f => f.isDeleted), 0)
    .select(
      getSqlFields2(BmpActivity, {
        asProperty: true,
        includeInternalField: true,
      })
    );

  await Bluebird.mapSeries(bmpActivities, async activity => {
    const task = bmpTasks.find(t => t.id === activity.taskId);
    if (!task) {
      console.error(activity);
      // throw 'Task not found activity';
      return;
    }

    const detail = _.assign(new BmpDetail(), {
      controlMeasureId: task.controlMeasureId,
      name: task.name,
      description: [activity.title, activity.description].join('\r\n'),
      dateAdded: activity.dateAdded,
      dueDate: activity.dueDate,
      completionDate: activity.completedDate,
    } as BmpDetail);

    // insert BMPDetail record
    const detailData = objectToSqlData(BmpDetail, detail, knex);
    const returnedDetail = await knex(BmpDetail.getTableName())
      .insert(detailData)
      .returning(
        getSqlFields2(BmpDetail, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .get<BmpDetail>(0);

    // associate BMPActivity files to BMPDetail files
    const fileEntities: FileEntity[] = await knex(FileEntity.getTableName())
      .where(FileEntity.sqlField(f => f.globalIdFk), activity.globalId)
      .where(FileEntity.sqlField(f => f.customerId), activity.customerId)
      .where(FileEntity.sqlField(f => f.entityType), EntityTypes.BmpActivity)
      .select(
        getSqlFields2(FileEntity, {
          asProperty: true,
          includeInternalField: true,
        })
      );

    await Bluebird.each(fileEntities, async fileEntity => {
      const newFile = {
        globalIdFk: returnedDetail.globalId,
        customerId: activity.customerId,
        entityType: EntityTypes.BmpDetail,
        originalFileName: fileEntity.originalFileName,
        fileSize: fileEntity.fileSize,
        fileName: fileEntity.fileName,
        thumbnailName: fileEntity.thumbnailName,
        mimeType: fileEntity.mimeType,
        addedDate: fileEntity.addedDate,
        addedBy: fileEntity.addedBy,
        isDeleted: fileEntity.isDeleted,
        description: fileEntity.description,
      } as FileEntity;
      const newFileData = objectToSqlData(FileEntity, newFile, knex);
      await knex(FileEntity.getTableName()).insert(newFileData);
    });

    // Per Ty: BMP Activity comments becomes a new BMP Activity Log
    if (activity.comments) {
      const activityLog: Partial<BmpActivityLog> = {
        bmpDetailId: returnedDetail.id,
        dateAdded: activity.dateAdded,
        activityDate: activity.dateAdded,
        comments: activity.comments,
      };
      const activityLogData = objectToSqlData(
        BmpActivityLog,
        activityLog,
        knex
      );
      await knex(BmpActivityLog.getTableName())
        .insert(activityLogData)
        .returning(
          getSqlFields2(BmpActivityLog, {
            asProperty: true,
            includeInternalField: true,
          })
        )
        .get<BmpActivityLog>(0);
    }
  });
};

exports.down = async function(knex: Knex): Bluebird<any> {
  await knex(BmpActivityLog.getTableName()).del();
  await knex(BmpDetail.getTableName()).del();
};
