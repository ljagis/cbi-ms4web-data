import * as Knex from 'knex';
const BMP_CONTROL_MEASURES = 'BmpControlMeasures';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(BMP_CONTROL_MEASURES, table => {
    table.integer('Sort');
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.table(BMP_CONTROL_MEASURES, table => {
    table.dropColumn('Sort');
  });
};
