import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.schema.table('ConstructionSiteInspections', table => {
    table.index(['ConstructionSiteId']);
    table.index(['FollowUpInspectionId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('FacilityInspections', table => {
    table.index(['FacilityId']);
    table.index(['FollowUpInspectionId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('OutfallInspections', table => {
    table.index(['OutfallId']);
    table.index(['FollowUpInspectionId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('StructureInspections', table => {
    table.index(['StructureId']);
    table.index(['FollowUpInspectionId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('CitizenReports', table => {
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('IllicitDischarges', table => {
    table.dropForeign(['CustomFormTemplateId']);
    table.dropForeign(['InspectionTypeId']);
    table.dropIndex(['CustomFormTemplateId']);
    table.dropIndex(['InspectionTypeId']);
  });

  await knex.schema.table('CustomFields', table => {
    table.index(['EntityType']);
  });

  await knex.schema.table('CustomFieldValues', table => {
    table.index(['GlobalIdFk', 'CustomFieldId']);
  });

  const inspectionTables = [
    'ConstructionSiteInspections',
    'FacilityInspections',
    'OutfallInspections',
    'StructureInspections',
    'CitizenReports',
    'IllicitDischarges',
  ];
  await Bluebird.each(inspectionTables, async inspectionTableName => {
    await knex.raw(
      `ALTER TABLE ${inspectionTableName} ALTER COLUMN [CustomFormTemplateId] INTEGER NOT NULL`
    );
    await knex.schema.table(inspectionTableName, table => {
      table
        .foreign('CustomFormTemplateId')
        .references('Id')
        .inTable('CustomFormTemplates');
      table
        .foreign('InspectionTypeId')
        .references('Id')
        .inTable('InspectionType');
      table.index(['CustomFormTemplateId']);
      table.index(['InspectionTypeId']);
    });
  });
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
