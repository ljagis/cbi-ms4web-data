import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

exports.up = async function(knex: Knex): Bluebird<any> {
  /* tslint:disable */
  const script =
    `UPDATE Customers ` +
    `SET Features = '{"calendar":true,"fileGallery":true,"bmps":true,"reports":true,"constructionSites":true,"outfalls":true,"structuralControls":true,"facilities":true,"illicitDischarges":true,"citizenReports":true,"extraLayers":true,"customFormTemplates":true,"exporting":true,"customFields":true}'`;
  /* tslint:enable */
  await knex.raw(script);
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
