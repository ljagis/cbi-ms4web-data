import * as Knex from 'knex';
import { dropDFConstraint } from '../src/shared/drop-constraint';
const tableToAlter = 'OutfallInspections';
const columnsToAlter = [
  'PriorityNumber',
  'IsAcceptable',
  'IsDischarging',
  'IsIllicit',
];

exports.up = async function(knex: Knex): Promise<any> {
  // PriorityNumber doesn't have a default constraint.
  await dropDFConstraint(knex, tableToAlter, columnsToAlter[1]);
  await dropDFConstraint(knex, tableToAlter, columnsToAlter[2]);
  await dropDFConstraint(knex, tableToAlter, columnsToAlter[3]);
  await knex.schema.table(tableToAlter, table => {
    table.dropColumns(...columnsToAlter);
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};
