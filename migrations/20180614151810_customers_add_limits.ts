import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.schema.table('Customers', table => {
    table
      .boolean('UnlimitedCustomFormTemplates')
      .notNullable()
      .defaultTo(0);

    table
      .boolean('UnlimitedAssets')
      .notNullable()
      .defaultTo(0);
  });

  await knex.table('Customers').update({
    UnlimitedCustomFormTemplates: 1,
    UnlimitedAssets: 1,
  });
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
