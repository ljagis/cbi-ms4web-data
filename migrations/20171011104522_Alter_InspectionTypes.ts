import * as Knex from 'knex';
import { dropDFConstraint } from '../src/shared/drop-constraint';
const CUSTOM_FORM_TEMPLATE_TABLE_NAME = 'CustomFormTemplates';
const INSPECTION_TYPE_TABLE_NAME = 'InspectionType';
exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.table(INSPECTION_TYPE_TABLE_NAME, table => {
    table.specificType('EntityType', 'varchar(50)');
    table
      .integer('CustomFormTemplateId')
      .index()
      .references('Id')
      .inTable(CUSTOM_FORM_TEMPLATE_TABLE_NAME);

    table
      .boolean('IsDeleted')
      .notNullable()
      .defaultTo(false);
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await dropDFConstraint(knex, INSPECTION_TYPE_TABLE_NAME, 'IsDeleted');

  await knex.schema.table(INSPECTION_TYPE_TABLE_NAME, table => {
    table.dropIndex(['CustomFormTemplateId']);
    table.dropForeign(['CustomFormTemplateId']);
    table.dropColumn('CustomFormTemplateId');

    table.dropColumn('IsDeleted');
  });
};
