import * as Knex from 'knex';

exports.up = async function(knex: Knex): Promise<any> {
  const addColumn = `
  ALTER TABLE UsersCustomers
  add Role NVARCHAR(510)
  `;
  await knex.raw(addColumn);

  const updateRoles = `
  update UsersCustomers
  SET UsersCustomers.Role = Users.Role
  from Users where Users.Id = UsersCustomers.UserId
  `;
  await knex.raw(updateRoles);
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};
