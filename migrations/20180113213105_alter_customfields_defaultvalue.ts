import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.raw(
    'ALTER TABLE CustomFields ALTER COLUMN DefaultValue nvarchar(max)'
  );
  await knex.raw('ALTER TABLE Files ALTER COLUMN Description nvarchar(max)');
  await knex.raw(
    'ALTER TABLE ConstructionSiteInspections ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
  await knex.raw(
    'ALTER TABLE FacilityInspections ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
  await knex.raw(
    'ALTER TABLE OutfallInspections ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
  await knex.raw(
    'ALTER TABLE StructureInspections ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
  await knex.raw(
    'ALTER TABLE IllicitDischarges ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
  await knex.raw(
    'ALTER TABLE CitizenReports ALTER COLUMN AdditionalInformation nvarchar(max)'
  );
};

exports.down = async function(knex: Knex): Bluebird<any> {
  // none
};
