import * as Knex from 'knex';
const tableToAlter = 'Facilities';
const columnToAlter = 'TrackingId';

exports.up = async function(knex: Knex): Promise<any> {
  await addColumn(knex, tableToAlter);
};

exports.down = async function(knex: Knex): Promise<any> {
  await removeColumn(knex, tableToAlter, columnToAlter);
};

async function addColumn(knex: Knex, tableName: string) {
  await knex.schema.table(tableName, table => {
    table.string(columnToAlter, 255);
  });
}

async function removeColumn(knex: Knex, tableName: string, columnName: string) {
  await knex.schema.table(tableName, table => {
    table.dropColumn(columnName);
  });
}
