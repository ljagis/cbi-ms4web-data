import * as Knex from 'knex';
const BMP_DATA_TYPES = 'BMPDataTypes';

exports.up = async function(knex: Knex): Promise<any> {
  await knex.schema.createTable(BMP_DATA_TYPES, table => {
    table.increments('Id').primary();

    table
      .specificType('CustomerId', 'varchar(50)')
      .notNullable()
      .index()
      .references('Id')
      .inTable('Customers');

    table.string('Name', 800);

    table.boolean('IsDeleted').defaultTo(0);
  });
};

exports.down = async function(knex: Knex): Promise<any> {
  await knex.schema.dropTable(BMP_DATA_TYPES);
};
