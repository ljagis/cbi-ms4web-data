import * as Knex from 'knex';
import { CustomField } from '../src/models';

exports.up = async function(knex: Knex): Promise<any> {
  await knex(CustomField.tableName)
    .update({
      InputType: 'text',
    })
    .whereRaw(`InputType = 'TextBox' COLLATE SQL_Latin1_General_CP1_CS_AS`);
};

exports.down = async function(knex: Knex): Promise<any> {
  //
};
