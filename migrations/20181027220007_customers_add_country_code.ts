import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

const CUSTOMER_TABLE = 'Customers';
exports.up = async function(knex: Knex): Bluebird<any> {
  await knex.schema.table(CUSTOMER_TABLE, table => {
    table
      .specificType('CountryCode', 'varchar(10)')
      .notNullable()
      .defaultTo('US');
  });

  await knex.table(CUSTOMER_TABLE).update({
    CountryCode: 'US',
  });
};

exports.down = async function(knex: Knex): Bluebird<any> {
  //
};
