import * as Knex from 'knex';
import { dropDFConstraint } from '../src/shared/drop-constraint';
const inspectionTypeTableName = 'InspectionType';
const isArchivedColumnName = 'IsArchived';

exports.up = async function(knex: Knex): Promise<any> {
  await addIsArchivedColumn(knex, inspectionTypeTableName);
};

exports.down = async function(knex: Knex): Promise<any> {
  await removeColumn(knex, inspectionTypeTableName, isArchivedColumnName);
};

async function addIsArchivedColumn(knex: Knex, tableName: string) {
  await knex.schema.table(tableName, table => {
    table
      .boolean(isArchivedColumnName)
      .notNullable()
      .defaultTo(0);
  });
}

async function removeColumn(knex: Knex, tableName: string, columnName: string) {
  await dropDFConstraint(knex, inspectionTypeTableName, isArchivedColumnName);
  await knex.schema.table(tableName, table => {
    table.dropColumn(columnName);
  });
}
