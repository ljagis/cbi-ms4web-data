'use strict';

var _ = require('lodash');

require('dotenv').config({
  path: '.env.yml',
});

module.exports = function(grunt) {
  var scripts = ['src/**/*.ts'];

  // relative to `src`
  var staticFiles = [
    'public/**',
    'views/**',
    'fonts/**',
    'web.config',
    'iisnode.yml',
  ];

  var dist = 'dist/';

  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    user: process.env.USER || process.env.USERNAME,
    now: new Date(),
    gitinfo: {},
    clean: {
      dist: [dist],
    },

    concurrent: {
      options: {
        logConcurrentOutput: true,
        spawn: false,
      },
      dev: {
        tasks: [
          'run:lint',
          'run:tsc-watch',
          'watch:staticfiles',
          'watch:scripts',
          'nodemon',
        ],
      },
    },

    run: {
      options: {
        spawn: false,
      },
      'tsc-watch': {
        cmd: 'yarn',
        args: ['run', 'tsc-watch'],
      },
      'tsc-prod': {
        cmd: 'yarn',
        args: ['run', 'tsc-prod'],
      },
      lint: {
        cmd: 'yarn',
        args: ['run', 'lint'],
      },
    },

    // DEPRECATED - use run:tsc-watch
    ts: {
      options: {
        // verbose: true
      },
      dev: {
        outDir: dist,
        tsconfig: true,
        passThrough: true,
        options: {
          fast: 'never',
          inlineSourceMap: true,
          spawn: false,
        },
      },
      prod: {
        outDir: dist,
        tsconfig: true,
        options: {
          fast: 'never',
          inlineSourceMap: false,
        },
      },
    },

    sync: {
      staticfiles: {
        files: [
          {
            cwd: 'src/',
            src: staticFiles,
            dest: dist,
          },
          {
            src: ['package.json', 'yarn.lock'],
            dest: dist,
          },
        ],
        verbose: true,
      },
    },

    writefile: {
      build: {
        dest: dist + 'build.txt',
        content:
          '<%= user %>\n<%= now %>\n<%= pkg.name %> <%= pkg.version %> (<%= pkg.repository.url %>), Branch <%= gitinfo.local.branch.current.name %>, Commit <%= gitinfo.local.branch.current.shortSHA %>',
      },
    },

    nodemon: {
      dev: {
        script: '<%= pkg.main %>',
        options: {
          args: ['dev'],
          nodeArgs: ['--inspect'], // Node 8
          callback: function(nodemon) {
            nodemon.on('log', function(event) {
              console.log(event.colour);
            });
          },
          env: {
            PORT: process.env.PORT || 9000,
          },
          ignore: [
            'node_modules/**',
            'src/views/**',
            'src/public/**',
            '**/*.spec.js',
          ],
          // cwd: dist,
          watch: ['dist'],
          delay: 1000,
        },
      },
    },

    shell: {
      initDist: {
        command: 'mkdir dist && touch dist/server.js',
      },
    },

    watch: {
      options: {
        spawn: false,
      },
      gruntfile: {
        files: ['Gruntfile.js'],
        options: {
          reload: true,
        },
      },
      staticfiles: {
        files: staticFiles,
        tasks: ['sync:staticfiles'],
        options: {
          spawn: false,
          cwd: {
            files: 'src/',
            spawn: 'src/',
          },
        },
      },
      scripts: {
        files: scripts,
        tasks: ['run:lint'],
      },
    },

    bump: {
      options: {
        files: ['package.json'],
        commit: false,
        createTag: false,
        push: false,
      },
    },
  });

  grunt.registerMultiTask('writefile', 'Writes build info', function() {
    grunt.file.write(this.data.dest, this.data.content);
  });

  grunt.registerTask('serve-dev', ['build-dev', 'concurrent']);

  grunt.registerTask('build-dev', [
    'clean',
    'shell:initDist',
    'sync:staticfiles',
    'gitinfo',
    'writefile:build',
  ]);

  grunt.registerTask('build-prod', [
    'clean',
    'run:lint',
    'run:tsc-prod',
    'sync:staticfiles',
    'gitinfo',
    'writefile:build',
  ]);

  grunt.registerTask('serve', ['serve-dev']);
  grunt.registerTask('default', ['build-prod']);
};
