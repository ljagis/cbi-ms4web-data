# cbi-ms4web-data

# Stormpath Information

PROD Okta Org Url: https://dev-523618.oktapreview.com/

Dev Okta Org Url: https://dev-523618.oktapreview.com/

Okta token will expire if not used within 30 days. To get a new token, go to https://dev-523618-admin.oktapreview.com/admin/access/api/tokens and click "Create Token".

# pre requisites

* `mocha` in global `npm i -g mocha`
* `yarn` https://yarnpkg.com/en/
* `.env.yml` file in the root directory. See Slack channel for file.

# dev MS SQL Server

run `yarn mssql` to start a MS SQL Server instance in a docker container for dev use. Stop with `yarn mssql:stop` or just CTRL+C.

The instance is available on localhost and the standard 1433 MSSQL Server port.

To configure your environment (.env.yml) for this connection, use:

```
DB_SERVER=localhost
DB_DATABASE=cbi_ms4web
DB_USER='sa'
DB_PASSWORD=[Password set in Dockerfile.mssql]
```

The instance can be accessed from SSMS in parallels (Windows only) if you have an alias mapped to "mac" in your windows etc/hosts file. Use the server "mac" when creating the connection. Use "sa" and the password defined in `Dockerfile.mssql`.

The database is a clone of the production schema but only loaded with a single user, single customer, and minimal assets data.

You may want to seed additional data into this database. You can do this by placing any SQL queries (.sql) in the `mssql` directory. They are executed in alphabetical order, so you probably want to prefix with "04" to execute after all other queries.

To log in to the app with this database, use 'gisdev@lja.com' and password 'gisPassw0rd!'. Make sure you are using the dev Okta tenant.

# install

run `yarn` to install dependencies

`yarn start` - Start server in `development` `NODE_ENV`

# Test

run `mocha` or `yarn run test` - mocha should by default read `test/mocha.opts` file.

You can also run `yarn run test-ts-node` which runs ts-node

There is a VSCode launch config "Mocha Current File ts-node" which runs mocha test under the current opened file.

# Deployment

`grunt` - Build to the `dist` directory.
cd into the `dist` dir and do `yarn install --production`

**NOTE:** This project uses an image library that uses `node-gyp`. This means the build machine needs to be the same node version as the production machine.

# Database migrations

See [Knex Migrations](http://knexjs.org/#Migrations)

See `README.md` in `migration` folder

# See the [WIKI](https://bitbucket.org/ljagis/cbi-ms4web-data/wiki/)
