import { Principal } from '../src/models';
import 'express';

declare module 'express' {
  interface Request {
    principal: Principal;
  }
}
