import 'express';
import * as core from 'express-serve-static-core';
import { Account } from 'stormpath';
import { UserFull } from '../models';

interface FullAccount extends Account {
  role: string;
}

declare module 'express' {
  export interface Request extends core.Request {
    user: FullAccount;
  }
}
