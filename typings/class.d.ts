/**
 * an alias for new (...args: any[]): T;
 */
interface ClassType<T = any> extends Function {
  new (...args: any[]): T;
}
