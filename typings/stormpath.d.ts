/**
 * Alex's stormpath typings
 */

declare module 'stormpath' {
  import 'express';

  export interface CollectionQueryOptions {
    expand?: string;
    limit?: number;
    offset?: number;
    orderBy?: string;
    q?: string;
  }

  interface AccountCollectionQueryOptions extends CollectionQueryOptions {
    name?: string;
  }

  interface GroupCollectionQueryOptions extends CollectionQueryOptions {
    name?: string;
    description?: string;
    status?: string;
  }

  export interface AccountData {
    customData?: any;
    email?: string;
    givenName?: string;
    middleName?: string;
    password?: string;
    surname?: string;
    status?: string;
    username?: string;
  }

  /**
   * @link https://docs.stormpath.com/rest/product-guide/latest/reference.html?#account
   *
   * @export
   * @class Account
   */
  export class Account implements AccountData {
    customData?: any;
    createdAt: string;
    email: string;
    emailVerificationStatus: string;
    fullName?: string;
    givenName: string;
    groups: CollectionResource<Group>;
    href: string;
    middleName?: string;
    modifiedAt: string;
    password?: string;
    status: string;
    surname: string;
    username: string;

    delete(callback: ((err) => any));
    getGroups(
      options: GroupCollectionQueryOptions,
      callback: CollectionResourceCallback<Group>
    );
    addToGroup(
      groupOrGroupHref: Group | string,
      callback?: (err, groupMembership) => any
    );
    save(callback: ResourceCallback<Account>);
    getGroupMemberships(
      options: CollectionQueryOptions,
      callback?: CollectionResourceCallback<GroupMembership>
    );
  }

  export class Group {
    description: string;
    href: string;
    name: string;
    status: string;

    getAccounts(
      options: AccountCollectionQueryOptions,
      callback: CollectionResourceCallback<Account>
    );
  }

  export class GroupMembership {
    href: string;
    account: Account;
    group: Group;

    delete(callback?: (err) => any);
    getAccount(...args);
    getGroup(...args);
    invalidate(...args);
  }

  interface IteratorNextFunction {
    (err, next);
  }

  interface CollectionResourceIteratorFunction<T> {
    (resource: T, next: IteratorNextFunction);
  }

  export interface CollectionResource<T> {
    href: string;
    items: T[];
    limit: number;
    offset: number;
    query: CollectionQueryOptions;
    size: number;

    concat(iterator: CollectionResourceIteratorFunction<T>, doneCallback);
    detect(iterator: (resource: T, next: Function) => void, doneCallback);
  }

  export interface CollectionResourceCallback<T> {
    (err: any, collection: CollectionResource<T>);
  }

  interface ResourceCallback<T> {
    (err: any, resource: T);
  }

  export class StormpathApplication {
    createAccount(
      accountData: AccountData,
      requestOptions?: any,
      callback?: ResourceCallback<Account>
    );
    createGroup(group: Group, callback?: ResourceCallback<Group>);
    getGroups(
      options: GroupCollectionQueryOptions,
      callback?: CollectionResourceCallback<Group>
    );
  }

  export class Client {
    constructor(clientOptions?: {
      apiKey?: {
        id: string;
        secret: string;
      };
    });

    getApplication(
      href: string,
      callback?: ResourceCallback<StormpathApplication>
    );
    getApplication(
      href: string,
      expansionOptions?,
      callback?: ResourceCallback<StormpathApplication>
    );
    getAccount(
      href: string,
      expansionOptions?: any,
      callback?: ResourceCallback<Account>
    );
    getGroups(
      options: GroupCollectionQueryOptions,
      callback: CollectionResourceCallback<Group>
    );
    on(event: string, listener: Function);
  }
}
