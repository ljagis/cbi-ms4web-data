/**
 * Okta typings.  Work in progress. - alex
 */

declare module '@okta/okta-sdk-nodejs' {
  interface ClientOptions {
    orgUrl: string;
    token: string;
  }

  interface Group {
    id: string;
    lastMembershipUpdated: string;
    lastUpdated: string;
    type: string;
    profile: {
      name: string;
      description?: string;
    };
  }

  export class Http {
    post(uri, request);
    postJson(uri, request): Promise<any>;
  }

  export class User {
    id: string;
    activated: string;
    created: string;
    lastUpdated: string;
    status:
      | 'STAGED'
      | 'PROVISIONED'
      | 'ACTIVE'
      | 'RECOVERY'
      | 'LOCKED_OUT'
      | 'PASSWORD_EXPIRED'
      | 'SUSPENDED'
      | 'DEPROVISIONED';
    statusChanged: string;

    profile: {
      currentCustomer?: string;
      displayName?: string;
      email: string;
      emailVerificationStatus?: string;
      emailVerificationToken?: string;
      firstName?: string;
      lastName?: string;
      login: string;
      mobilePhone?: string;
      secondEmail?: string;
      stormpathHref?: string;
      stormpathMigrationRecoveryAnswer?: string;
    };

    credentials?: {
      password?: { value: string };
      recovery_question?: { question: string; answer: string };
    };

    _links: {
      self: {
        href: string;
      };
    };

    activate(queryParams?: { sendEmail: boolean }): Promise<void>;

    addToGroup(groupId: string): Promise<void>;

    suspend(): Promise<void>;

    unsuspend(): Promise<void>;

    deactivate(): Promise<void>;

    update(): Promise<void>;

    resetPassword(queryParams: { sendEmail?: boolean }): Promise<void>;
  }

  export class Client {
    apiToken: string;
    baseUrl: string;

    http: Http;
    constructor(options: ClientOptions);

    createUser(
      user: Partial<User>,
      queryParams?: {
        activate: boolean;
        sendEmail: boolean;
      }
    ): Promise<User>;
    getUser(idOrlogin: string): Promise<User>;

    createGroup(group: Partial<Group>): Promise<Group>;
  }
}
