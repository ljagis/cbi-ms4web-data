// via https://github.com/DefinitelyTyped/DefinitelyTyped/issues/10801
declare module 'bluebird-global' {
  import * as Bluebird from 'bluebird';
  global {
    export interface Promise<T> extends Bluebird<T> {}
  }
}
