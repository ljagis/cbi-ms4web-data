/**
 * Alex's express-stormpath typings
 */

declare module 'express-stormpath' {
  import * as core from 'express-serve-static-core';
  export function init(app: core.Express, options: any): any;

  export function authenticationRequired(...args);
  export function getUser(...args);
}
