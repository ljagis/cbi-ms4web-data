const axios = require('axios');

const ACCESS_TOKEN =
  'eyJraWQiOiJMUGk2RUtScDBPSUtYcXduRGZkREV3c3lpUHNvQWplczF3SEh2bVVKODdZIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULjZ4cVJ4TW8wdUFmRllRSFRJMDN6WmF2NWhwN1M0dlNzSkVvTEswQkdBVFkub2FyMW9uanNqZlBKbjJmUUEwaDciLCJpc3MiOiJodHRwczovL2Rldi0xNjI1NzQub2t0YXByZXZpZXcuY29tL29hdXRoMi9hdXNia3IxdWR1ekZ0WnRSYzBoNyIsImF1ZCI6Imh0dHBzOi8vYXBpLnN0b3JtcGF0aC5jb20vdjEvYXBwbGljYXRpb25zLzV3bFJ3SThJRUpRYkJ1NnRlZ3JaIiwiaWF0IjoxNjU2NDU1Mjk4LCJleHAiOjE2NTY0NTg4OTgsImNpZCI6IjBvYWJraTdjbHUzdjZZM3NGMGg3IiwidWlkIjoiMDB1YmtyNTd2M2pkanB0eU0waDciLCJzY3AiOlsib2ZmbGluZV9hY2Nlc3MiLCJwcm9maWxlIiwib3BlbmlkIl0sImF1dGhfdGltZSI6MTY1NjQzODg0Nywic3ViIjoiYmNvcGVsYW5kQGxqYWVuZ2luZWVyaW5nLmNvbSJ9.gQ6NbPCsLmMuFXIsnF2kfkwaQPlML0UhD3VdHTRvVpfvWQYPv6fu6Wsj5pc5o3dOE_um-zcgmbsh61Bpgz5LRUfUOhaPCe2u3ABt_AnsMzt9nfEkkoS5eb_HKXjl77W98E2VD19uAsZoU_TpAQjg8n7cZRBB2eiybS2ahYcR_gLg2_9XDIQoRS0P8gGPzSJQnsvWq2obhHT9x01t9ne7d4e0aoRfmX2LCZVVH9qxx6-9vTTY7YPTlH73HKYSN5hzUbwXalWMcKfO205YVxFowprDw97rj1sGP7AlQ5otnwQa7fLPTlIJOCQQYbgooU7Bk5TbuwWCIrr71e91evwl2A';

const axiosClient = axios.create({
  baseURL: 'http://localhost:9000/api/',
  headers: {
    'Content-Type': 'application/json',
    Cookie: `access_token=${ACCESS_TOKEN}`,
  },
});

const entityType = 'ConstructionSiteInspection';

const createCustomFieldsPayload = [
  {
    dataType: 'string',
    inputType: 'text',
    fieldLabel: 'PMIS ID',
  },
  {
    dataType: 'string',
    inputType: 'text',
    fieldLabel: 'CoC Compliance',
  },
  {
    dataType: 'string',
    inputType: 'text',
    fieldLabel: 'Time of Inspection',
  },
  {
    dataType: 'string',
    inputType: 'text',
    fieldLabel: 'Follow Up Inspection Date',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: 'Construction Stage',
    fieldOptions:
      'Initial Grading and Utilities and Infrastructure|Buildings under Construction|Final Site Stabilization',
  },
  {
    dataType: 'string',
    inputType: 'textarea',
    fieldLabel: 'Individuals Met On Site',
  },
  {
    dataType: 'section',
    inputType: 'section',
    fieldLabel:
      'CONSTRUCTION SITE MANAGEMENT FOR EROSION AND SEDIMENT CONTROL (ITEMS 1 - 10)',
    sectionName:
      'CONSTRUCTION SITE MANAGEMENT FOR EROSION AND SEDIMENT CONTROL (ITEMS 1 - 10)',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '1. Disturbed areas have been adequately protected through seeding or other appropriate erosion and sediment control measures',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '2. Appropriate perimeter sediment control measures have been implemented',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '3. Conveyance channels have been stabilized or protected with appropriate sediment control measures',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '4. Erosion & sediment control measures are installed properly',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '5. Storm drain inlets have been adequately protected',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '6. Outlets have been adequately stabilized',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '7. Existing erosion & sediment control measures are being maintained',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '8. Public & private roadways are being kept clear of accumulated sediment or tracked soil',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '9. Erosion & sediment control measures have been installed and maintained on individual building sites',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '10 (a). Weekly inspection reports are completed',
    fieldOptions: 'Satisfactory|Marginal|Unsatisfactory|Not Applicable',
  },
  {
    dataType: 'string',
    inputType: 'textarea',
    fieldLabel: '10 (b). Weekly Reporting Comments',
  },
  {
    dataType: 'section',
    inputType: 'section',
    fieldLabel: 'STATUS OF SEDIMENT RETENTION ON-SITE (ITEMS 1 -2)',
    sectionName: 'STATUS OF SEDIMENT RETENTION ON-SITE (ITEMS 1 -2)',
  },
  {
    dataType: 'string',
    inputType: 'textarea',
    fieldLabel:
      '1. Site conditions present a high potential for off-site sedimentation',
  },
  {
    dataType: 'string',
    inputType: 'textarea',
    fieldLabel: '2. There is evidence of off-site sedimentation',
  },
  {
    dataType: 'section',
    inputType: 'section',
    fieldLabel:
      'ON-SITE EVALUATION FOR EROSION AND SEDIMENT CONTROL (ITEMS 1 - 13)',
    sectionName:
      'ON-SITE EVALUATION FOR EROSION AND SEDIMENT CONTROL (ITEMS 1 - 13)',
  },
  {
    dataType: 'Date',
    inputType: 'date',
    fieldLabel: 'ITEMS 1 - 13 THAT ARE MARKED WITH AN "X" MUST BE RESOLVED BY',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '1. Install an appropriate sediment control practice between construction areas and lower lying areas',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '2. Replace/Repair silt fence and/or straw bale barriers',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '3. Entrench silt fence (6-8 in.) and/or straw bale barriers (4 –6 in.) and stake straw bales',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '4. Remove accumulated sediment from:  sediment traps/basins,  behind silt fence/straw bales,  around  storm drain inlet protection devices,  streets and gutters (do not flush with water)',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '5. Temporary seed, fertilize, and/or mulch',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '6. Permanent seed, fertilize, and mulch areas that are at final grade',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '7. Protect storm drain inlets:  curb inlets,  drop inlets',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '8. Reshape and stabilize side slopes of:  sediment traps/basins,  detention/retention basins',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '9. Install/Maintain construction entrance(s)',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel: '10. Reshape and stabilize conveyance channels',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '11. Place greater emphasis on erosion and sediment control on building sites; contractors, subcontractors, material vendors, and others should be made aware of erosion and sediment control requirements.  Appropriate perimeter sediment controls (e.g. silt fence) and stable construction entrances shall be utilized on all sites.  Sediment tracked into the street shall be removed (do not flush with water) at the end of each workday',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'select',
    fieldLabel:
      '12. Remove all temporary control structures that are no longer needed',
    fieldOptions: 'Yes|No',
  },
  {
    dataType: 'string',
    inputType: 'textarea',
    fieldLabel: '13. Other',
  },
  {
    dataType: 'section',
    inputType: 'section',
    fieldLabel:
      'This evaluation is intended to assess the level of compliance with Chapter 53: Stormwater Management Department, Fort Wayne City Code.  It is also intended to identify areas where additional measures may be required to control erosion and sedimentation.  All practices recommended in this report should be evaluated as to their feasibility by a qualified individual, with structural practices designed by a qualified engineer. All erosion and sediment control measures shall meet the design criteria, standards, and specifications outlined in the “Indiana Handbook for Erosion Control in Developing Areas” or similar guidance documents.',
    sectionName:
      'This evaluation is intended to assess the level of compliance with Chapter 53: Stormwater Management Department, Fort Wayne City Code.  It is also intended to identify areas where additional measures may be required to control erosion and sedimentation.  All practices recommended in this report should be evaluated as to their feasibility by a qualified individual, with structural practices designed by a qualified engineer. All erosion and sediment control measures shall meet the design criteria, standards, and specifications outlined in the “Indiana Handbook for Erosion Control in Developing Areas” or similar guidance documents.',
  },
];

(async function() {
  try {
    const formFieldIds = [];

    const createTemplateResponse = await axiosClient.post(
      '/customformtemplates',
      {
        entityType,
        name: 'PMIS Inspection',
      }
    );

    const customFormTemplateId = Number(createTemplateResponse.data.id);

    console.log('Creating form template... 🙂');

    for (let i = 0; i < createCustomFieldsPayload.length; i++) {
      const createFormFieldResponse = await axiosClient.post('/customfields', {
        ...createCustomFieldsPayload[i],
        entityType,
        fieldOrder: i,
      });

      formFieldIds.push(createFormFieldResponse.data.id);
    }

    const creteInspectionTypeResponse = await axiosClient.post(
      '/inspectiontypes',
      {
        name: 'PMIS Inspection',
        sort: 1000,
        entityType,
        customFormTemplateId,
      }
    );

    const updateFormFieldPromises = formFieldIds.map(id =>
      axiosClient.put(`/customfields/${id}`, {
        customFormTemplateId,
      })
    );

    await Promise.all(updateFormFieldPromises);

    console.log(`😎 SWPPP Custom Form Template Id -> ${customFormTemplateId}`);
    console.log(`Inspection Type Id -> ${creteInspectionTypeResponse.data.id}`);
    console.log(`SWPPP Custom Form Field Ids\n${formFieldIds}`);
  } catch (error) {
    console.log('😱 error', error);
  }
})();
