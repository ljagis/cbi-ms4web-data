const assetIds = require('./assetIds.json');
const axios = require('axios');
const accessToken =
  'eyJraWQiOiJkUXZBZ1F2cDlMWnJaZWpUZnM5a0otV19qU2RoWUw4WUhwbG4tekk4MHlFIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmtKaXMwVjlZTjBfNXVjS1ZtMy1wQWhvcWJjaEJyWjdBNDM2YzZna0J4SHcub2FyMTg3dGdzZTA0ZXgxNTEwaDciLCJpc3MiOiJodHRwczovL2Rldi0xNjI1NzQub2t0YXByZXZpZXcuY29tL29hdXRoMi9hdXNia3IxdWR1ekZ0WnRSYzBoNyIsImF1ZCI6Imh0dHBzOi8vYXBpLnN0b3JtcGF0aC5jb20vdjEvYXBwbGljYXRpb25zLzV3bFJ3SThJRUpRYkJ1NnRlZ3JaIiwiaWF0IjoxNjMwMzQ3MDg4LCJleHAiOjE2MzAzNTA2ODgsImNpZCI6IjBvYWJraTdjbHUzdjZZM3NGMGg3IiwidWlkIjoiMDB1YmtyNTd2M2pkanB0eU0waDciLCJzY3AiOlsib2ZmbGluZV9hY2Nlc3MiLCJvcGVuaWQiLCJwcm9maWxlIl0sInN1YiI6ImJjb3BlbGFuZEBsamFlbmdpbmVlcmluZy5jb20ifQ.Bz4QR6R8FvjFGHRRAaF2Mo6kCf3jdFVzNIDE412kHjKAcCR0iiAQirbROj0X7UA3vCGr2IeKtv7xWle7g_q4BkzaAfgkbJFZRwEcNs9rvmS9X-XpUwUALgwBA4PRnlOVmZAjbq3l8NSzetcf-Tvzuir2yIC0cjSwp41Qp8pOsm5tSH-JxtjRe8Td3ihj_-yC6c2sxVp3Cs-yTAiU_O23j6ppPVuNgyUHVh0gyzRkaCCendB_QQG1b0ShsuUjMexxkET9awYDRFSLrf0e1_oM5nDuecxxYweeq1yhQYXcuocZCFCc8JBGlwEC83EtGxqSVJmgoO3cO8PZANtqydN-Wg';

(async function() {
  for (let i = 0; i < assetIds.length; i++) {
    try {
      await axios({
        method: 'delete',
        url: `http://localhost:9000/api/structures/${assetIds[i]}`,
        headers: {
          'Content-Type': 'application/json',
          Cookie: `access_token=${accessToken}`,
        },
      });

      console.log(`${assetIds[i]} has been removed 😎`);
    } catch (error) {
      console.log('😱 error', assetIds[i], error.response.statusText);
    }
  }
})();
