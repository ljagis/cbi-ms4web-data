const axios = require('axios');
const accessToken =
  'eyJraWQiOiJkUXZBZ1F2cDlMWnJaZWpUZnM5a0otV19qU2RoWUw4WUhwbG4tekk4MHlFIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmRFZk1kenlld21oUUNIZnpZNElWRUhZRDlrVHZhcW43ZjFFLVVNdkhSSWsub2FyMTg3dGdzZTA0ZXgxNTEwaDciLCJpc3MiOiJodHRwczovL2Rldi0xNjI1NzQub2t0YXByZXZpZXcuY29tL29hdXRoMi9hdXNia3IxdWR1ekZ0WnRSYzBoNyIsImF1ZCI6Imh0dHBzOi8vYXBpLnN0b3JtcGF0aC5jb20vdjEvYXBwbGljYXRpb25zLzV3bFJ3SThJRUpRYkJ1NnRlZ3JaIiwiaWF0IjoxNjMwMzQzNDIyLCJleHAiOjE2MzAzNDcwMjIsImNpZCI6IjBvYWJraTdjbHUzdjZZM3NGMGg3IiwidWlkIjoiMDB1YmtyNTd2M2pkanB0eU0waDciLCJzY3AiOlsib2ZmbGluZV9hY2Nlc3MiLCJwcm9maWxlIiwib3BlbmlkIl0sInN1YiI6ImJjb3BlbGFuZEBsamFlbmdpbmVlcmluZy5jb20ifQ.hVPVe3HQaqCkTfQxJrkac5fP69U2vsXkY_09A5n3g0juAcSl5jc5KvLbTlfiM7sLayGthC4P7730a3MhV8AwO1fxPELsz2MXkbK7w1Amp61mxoWajv3_dscxX4FqRbLYBYXYSr7bEZw1CoO6MGX8T6WaQBPVtv_A6fFW6-5G-FIWKSFjqawoaa4HWUeclYutUBfyip3G2XdtjcFU2XVsxuSyIq6WzToAeFtU9dSZQqKRe5psnqpbCDCL11xso4QLtsf69Ju8aGe--OH6Atgb-STPrjdaNuo2KS3Ojd9bq6_GbUu0Mugz22HO1JcTZzg2oESkf5ntxiPH2XXdHtpbzA';
const CUSTOMER = 'KY_UNIVERSITYOF';

(async function() {
  const payload = {
    userId: 6,
    role: 'super',
  };

  await axios({
    method: 'post',
    url: `http://localhost:9000/api/customers/${CUSTOMER}/users`,
    headers: {
      'Content-Type': 'application/json',
      Cookie: `access_token=${accessToken}`,
    },
    data: payload,
  })
    .then(() => console.log(`${CUSTOMER} has been added 😎`))
    .catch(error => console.log('😱 error', error.response.statusText));
})();
