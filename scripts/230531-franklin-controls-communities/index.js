const communities = require('./communities.json');
const structures = require('./structures.json');

const cookie =
  'access_token=eyJraWQiOiJrSTUzWVRRNVl6X2Mzd2VWU0JBU3c1NTlSa19xSkhFQ3BrdEFxeGhreUhvIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULlBjWWE4dG9tRENtMmdDcGE2U29rVUF2ODdPS0VuY0RTODVCS002X0xaV3cub2FyMjFkdG13eldydEx4bVUwaDciLCJpc3MiOiJodHRwczovL2Rldi0xNjI1NzQub2t0YXByZXZpZXcuY29tL29hdXRoMi9hdXNia3IxdWR1ekZ0WnRSYzBoNyIsImF1ZCI6Imh0dHBzOi8vYXBpLnN0b3JtcGF0aC5jb20vdjEvYXBwbGljYXRpb25zLzV3bFJ3SThJRUpRYkJ1NnRlZ3JaIiwiaWF0IjoxNjg1NTQ2OTA1LCJleHAiOjE2ODU1NTA1MDUsImNpZCI6IjBvYWJraTdjbHUzdjZZM3NGMGg3IiwidWlkIjoiMDB1YmtyNTd2M2pkanB0eU0waDciLCJzY3AiOlsib2ZmbGluZV9hY2Nlc3MiLCJvcGVuaWQiLCJwcm9maWxlIl0sImF1dGhfdGltZSI6MTY4MDU3ODQ3NCwic3ViIjoiYmNvcGVsYW5kQGxqYWVuZ2luZWVyaW5nLmNvbSJ9.JHFiBI8hpJLsOsqJoH86Kfr24Cab6nsOYxXBAb2awbouYKYacFn3ItDhmocEshfFHmqm4T3I0x1cGm-ERGlg0sOphK49Tv4pfPK2Oy32tVe1GyPneSv0CkC-IWuyLAt3r6WYFOUCONI7yykfCG8UY7Vd9sP8bHfXbvQOdFjHJY0I7FZpSBlRamH0OC02CEvvswaF6CMBbso3oXPbF9LYhij2eCHFmOelmV-liblrZaDVYcEU_b56gYVgmHvffcrTuviaUJ7EpD987k8mkTVWvbWEi2t46PX8eBsBhPXUc94pxrEL-ZZ5E8dZqDj4jomUqpevyb0wjgxBWOOb5dSkLw; hblid=T1rIdqjsnoSENDW56m1zo0VA88AK11FA; _ga=GA1.1.981876463.1670961671; olfsk=olfsk07973648417808998; _ga=GA1.3.981876463.1670961671; refresh_token=AKB4vmMU3e8SHBbOejwfnW5BRWCzRafIWfr_KMd8Wis; wcsid=IcGz0WWO3nidVcEk6m1zo0UoKFBASUAA; _oklv=1685548282733%2CIcGz0WWO3nidVcEk6m1zo0UoKFBASUAA; _okdetect=%7B%22token%22%3A%2216848671313490%22%2C%22proto%22%3A%22about%3A%22%2C%22host%22%3A%22%22%7D; _ok=3731-506-10-1486; _gid=GA1.1.1955305482.1685460385; _gid=GA1.3.1955305482.1685460385; _okbk=cd4%3Dtrue%2Cvi5%3D0%2Cvi4%3D1685546906055%2Cvi3%3Dactive%2Cvi2%3Dfalse%2Cvi1%3Dfalse%2Ccd8%3Dchat%2Ccd6%3D0%2Ccd5%3Daway%2Ccd3%3Dfalse%2Ccd2%3D0%2Ccd1%3D0%2C; _gat=1';

const communitiesMap = communities.reduce(
  (acc, community) => ({ ...acc, [community.name]: community }),
  {}
);
const structuresMap = structures.reduce(
  (acc, structure) => ({
    ...acc,
    [`${structure.lng.toString().substring(0, 10)}:${structure.lat
      .toString()
      .substring(0, 10)}`]: structure,
  }),
  {}
);

const axiosClient = require('axios').create({
  baseURL: 'https://stormwater.ms4web.com/v1/api/',
  headers: { 'Content-Type': 'application/json', Cookie: cookie },
});

function getControls() {
  return axiosClient
    .get('assetmap')
    .then((response) => response.data.structures);
}

function update(control) {
  if (!(control.lng && control.lat)) {
    console.log('NO COORDS', control.id, control.name);
    return;
  }

  const structureMatch =
    structuresMap[
      `${control.lng.toString().substring(0, 10)}:${control.lat
        .toString()
        .substring(0, 10)}`
    ];

  if (!structureMatch) {
    console.log(
      'NO STRUCTURE MATCH',
      control.id,
      control.name,
      `${control.lng.toString().substring(0, 10)}:${control.lat
        .toString()
        .substring(0, 10)}`
    );
    return;
  }

  const communityMatch = communitiesMap[structureMatch.community];

  if (!communityMatch) {
    console.log(
      'NO COMMUNITY MATCH',
      control.id,
      control.name,
      structureMatch.community
    );
    return;
  }

  return axiosClient
    .put('/structures/' + control.id, { communityId: communityMatch.id })
    .catch(console.log);
}

(function main() {
  getControls().then((controls) => Promise.all(controls.map(update)));
})();
