const newOutfalls = require('./discharge.json');
const axios = require('axios');

/* 
  Comment out Auth protection for outfall post outfall.controller.ts
  lines 166-168. Change customerId to desired receiver ex. 'NE_NORFOLK'
*/
newOutfalls.features.forEach(async (outfall, i) => {
  const data = {
    complianceStatus: 'Not Screened',
    additionalInformation: outfall.properties.GPS_Commen,
    lat: outfall.geometry.coordinates[1],
    lng: outfall.geometry.coordinates[0],
  };

  await axios({
    method: 'post',
    url: 'http://localhost:9000/api/outfalls',
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  }).catch(error => console.log(`${error} index: ${i}`));
});

// Number of changes console logged
console.log(newOutfalls.features.length);
