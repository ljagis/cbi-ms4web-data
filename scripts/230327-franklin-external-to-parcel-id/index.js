var cookie =
  'access_token=eyJraWQiOiJSeGI3VUpaeU9xeUxzaXVHUjI3N0RqS3VZYVFyc0tGOXFKNUpNczJVVDVBIiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULmNsRWZVckxSS1laTUF2ZGpuZmpwbzdqbzlzSUdGTkxxOUppVXM4VGJ2WGsub2FyMXk0Ymp0a2pUWTBjVXQwaDciLCJpc3MiOiJodHRwczovL2Rldi0xNjI1NzQub2t0YXByZXZpZXcuY29tL29hdXRoMi9hdXNia3IxdWR1ekZ0WnRSYzBoNyIsImF1ZCI6Imh0dHBzOi8vYXBpLnN0b3JtcGF0aC5jb20vdjEvYXBwbGljYXRpb25zLzV3bFJ3SThJRUpRYkJ1NnRlZ3JaIiwiaWF0IjoxNjc5OTMxODk5LCJleHAiOjE2Nzk5MzU0OTksImNpZCI6IjBvYWJraTdjbHUzdjZZM3NGMGg3IiwidWlkIjoiMDB1YmtyNTd2M2pkanB0eU0waDciLCJzY3AiOlsicHJvZmlsZSIsIm9mZmxpbmVfYWNjZXNzIiwib3BlbmlkIl0sImF1dGhfdGltZSI6MTY3NTE5NDUzMSwic3ViIjoiYmNvcGVsYW5kQGxqYWVuZ2luZWVyaW5nLmNvbSJ9.gHqA0kWJ-arSbCUVIlQZhd5YEVxmsG0VU0xO-Hlx0oNVP-qpIg1A1ZbyRFOElu96Ta2Q4TwE1FhdSU89Fhl-96VZwWWdGveG5DR1PHikZnUhg-_bXLqSoxkztwt9bZvQFUnrhqTubEiLrpC2sXz4LI6nth6zfLsznWKEBERdO7ViEnMUk_LOkGBDOXqSCmVnqHJXBhXs67SEWmVXVZU8euXUiwGrrTbfn-ZisGo71h3zPdquegpxw4MIlf3S005QzJj1xFG-c_kg4rdssI7naXyOmLWlpfRShAfuzLVHtF5rg0_A0RgFdKoWOZ8k9c2wa2bP-zuyLCiG9qSpZReR1Q; hblid=T1rIdqjsnoSENDW56m1zo0VA88AK11FA; _ga=GA1.1.981876463.1670961671; olfsk=olfsk07973648417808998; _ga=GA1.3.981876463.1670961671; refresh_token=9pSvtaPMtp0QmU4JymSlUlS-Bjvo9WCz44RzeEEeQX8; wcsid=6ckQan6XDGJw00NV6m1zo0UB1FKAAoAA; _oklv=1679932099634%2C6ckQan6XDGJw00NV6m1zo0UB1FKAAoAA; _okdetect=%7B%22token%22%3A%2216799318994280%22%2C%22proto%22%3A%22about%3A%22%2C%22host%22%3A%22%22%7D; _okbk=cd4%3Dtrue%2Cvi5%3D0%2Cvi4%3D1679931899599%2Cvi3%3Dactive%2Cvi2%3Dfalse%2Cvi1%3Dfalse%2Ccd8%3Dchat%2Ccd6%3D0%2Ccd5%3Daway%2Ccd3%3Dfalse%2Ccd2%3D0%2Ccd1%3D0%2C; _ok=3731-506-10-1486; _gid=GA1.1.75866325.1679931900; _gid=GA1.3.75866325.1679931900; _gat=1';

var axiosClient = require('axios').create({
  baseURL: 'https://stormwater.ms4web.com/v1/api/',
  headers: {
    'Content-Type': 'application/json',
    Cookie: cookie,
  },
});

function getSites() {
  return axiosClient
    .get('assetmap')
    .then(response => response.data.constructionSites);
}

function filterSitesWithTrackingId(sites) {
  return sites.filter(site => Boolean(site.trackingId));
}

function postParcelId(site) {
  return axiosClient
    .put('/constructionsites/' + site.id + '/customfields', [
      { id: 100898, value: site.trackingId },
    ])
    .then(site.id)
    .catch(console.log);
}

(function main() {
  getSites()
    .then(filterSitesWithTrackingId)
    .then(sites => Promise.all(sites.map(postParcelId)));
})();
