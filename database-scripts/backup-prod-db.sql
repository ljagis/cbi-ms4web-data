/**
  Run this on prod to backup database
*/

declare @filename varchar(1000)
select @filename = N'C:\MSSQL\Backups\cbi_ms4web_' + (select REPLACE(CONVERT(VARCHAR, GETDATE(), 120), ':', '-')) + '.bak'
BACKUP DATABASE [cbi_ms4web] TO  DISK = @filename WITH NOFORMAT, NOINIT,  NAME = N'cbi_ms4web-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10

