/**
Script to delete user from the database. After delete, remove him from Okta
**/


declare @email nvarchar(255) = 'tgarmon@cbi-systems.com'
declare @userId int

select
  @userId = (select
    id
  from
    Users
  where email =@email)

-- delete
delete from UsersCustomers where UserId=@userId
delete from users where
Email=@email
