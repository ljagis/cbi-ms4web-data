declare @customerId nvarchar(50) = 'TX_ANGLETONDD'

-- select * from customfields where customerid=@customerid

begin tran
--Custom fields
delete from CustomFieldValues where CustomFieldId in (select
  id
from
  customfields
where customerid=@customerid)
delete from customfields where customerid=@customerid

-- Structural Control Types
delete from structurecontroltypes where customerid=@customerid

-- 
delete UsersCustomers where CustomerId = @customerid

-- BMP
delete from BMPDataTypes where CustomerId = @customerId
delete from BmpActivities where customerid=@customerId
delete from BmpTasks where customerid=@customerid
delete from BMPActivityLogs where BmpDetailId in (
	select
  id
from
  bmpdetails
where ControlMeasureId in (
		select
  id
from
  BmpControlMeasures
where CustomerId = @customerid
	)
)
delete from bmpdetails where ControlMeasureId in (select
  id
from
  BmpControlMeasures
where CustomerId = @customerid)
delete from BmpControlMeasures where CustomerId = @customerid

/** investigations */
delete from citizenreports where customerid=@customerId

delete from illicitdischarges where customerid=@customerId


-- Construction Sites
delete from ConstructionSiteInspections where constructionsiteid in (select
  id
from
  ConstructionSites
where customerid=@customerId)
delete from ConstructionSites where customerid=@customerid


-- Facilities
delete from FacilityInspections where facilityid in (select
  id
from
  facilities
where customerid=@customerId)
delete from facilities where customerid=@customerid

-- Outfalls
delete from OutfallInspections where outfallid in (select
  id
from
  outfalls
where customerid=@customerId)
delete from outfalls where customerid=@customerid

-- Structural Controls
delete from StructureInspections where StructureId in (select
  id
from
  Structures
where customerid=@customerId)
delete from Structures where customerid=@customerid

-- 
delete from ReceivingWaters where customerid=@customerId
delete from Watersheds where customerid=@customerId
delete from Communities where customerid=@customerId

delete from Projects where customerId=@customerid

-- Contacts
delete from entitycontacts where contactid in (select
  id
from
  Contacts
where customerId=@customerid)
delete from Contacts where customerId=@customerid

delete from InspectionType where CustomerId=@customerid
delete from CustomFormTemplates where CustomerId = @customerid

delete from customers where id=@customerId

rollback
--commit