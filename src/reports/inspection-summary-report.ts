import { injectable } from 'inversify';
import { chain, countBy, groupBy, sumBy, uniq } from 'lodash';
import * as moment from 'moment';
import {
  CitizenReportRepository,
  ConstructionSiteInspectionSummaryRepository,
  FacilityInspectionSummaryRepository,
  IllicitDischargeRepository,
  OutfallInspectionSummaryRepository,
  StructureInspectionSummaryRepository,
} from '../data';
import {
  Customer,
  InspectionSummaryModel,
  InspectionSummaryRecord,
} from '../models';
import { ClientError, EntityTypes, Env } from '../shared';
import { DefaultPdfReport } from './default-pdf-report';
import { InspectionSummaryReportOptions } from './report-options';

@injectable()
export class InspectionSummaryReport {
  env = process.env as Env;

  constructor(
    private _csisRepo: ConstructionSiteInspectionSummaryRepository,
    private _fisRepo: FacilityInspectionSummaryRepository,
    private _oisRepo: OutfallInspectionSummaryRepository,
    private _sisRepo: StructureInspectionSummaryRepository,
    private _defaultReport: DefaultPdfReport,
    private _crRepo: CitizenReportRepository,
    private _illicitRepo: IllicitDischargeRepository
  ) {}

  async generate(customer: Customer, options: InspectionSummaryReportOptions) {
    const customerId = customer.id;
    // 8/15/2019 date format
    const fromFormatted = moment(options.from).format('l');
    const toFormatted = moment(options.to).format('l');
    // convert request dates to UTC on each specific day to maintain daylight savings modifier
    const fromQuery = moment(options.from).toISOString();
    const toQuery = moment(options.to).toISOString();

    const repos = {
      [EntityTypes.ConstructionSiteInspection]: this._csisRepo,
      [EntityTypes.FacilityInspection]: this._fisRepo,
      [EntityTypes.OutfallInspection]: this._oisRepo,
      [EntityTypes.StructureInspection]: this._sisRepo,
    };

    const rptOptions = { from: fromQuery, to: toQuery };

    const repo = repos[options.entityType];
    if (!repo) {
      throw new ClientError('Unsupported Inspection Type');
    }
    const values = await repo.getInspectionSummaryRecords(
      customerId,
      rptOptions
    );

    const inspections = await repo.getInspections(customerId, rptOptions);

    const valuesByInspectionTypeId = groupBy(values, v => v.inspectionTypeId);

    const assetText = {
      [EntityTypes.ConstructionSiteInspection]: 'Construction Site',
      [EntityTypes.FacilityInspection]: 'Facility',
      [EntityTypes.OutfallInspection]: 'Outfall',
      [EntityTypes.StructureInspection]: 'Structural Control',
    };

    const content: any[] = [
      // { image: image, fit: [200, 100] },
      {
        text: `${assetText[options.entityType]} Inspection Summary Report`,
        style: 'header',
      },
      { text: `From ${fromFormatted} to ${toFormatted}` },
      { text: '', margin: 4 },
    ];

    content.push(' '); // blank line separator

    const reportTableWidths = ['*', 100, 80];

    const tableHeaders: any[] = [
      { text: '', style: 'tableHeader' },
      { text: '', style: ['tableHeader', 'optionStyle'] },
      { text: '', style: ['tableHeader', 'valueStyle'] },
    ];

    function newTableObject() {
      return {
        table: { widths: reportTableWidths, body: [tableHeaders] },
        layout: {
          hLineWidth: (i, node) =>
            i === 0 || i === node.table.body.length ? 1 : 0.5,
          vLineWidth: (i, node) => 0,
          hLineColor: (i, node) =>
            i === 0 || i === node.table.body.length ? 'black' : '#EDEDED',
          vLineColor: (i, node) =>
            i === 0 || i === node.table.widths.length ? 'black' : '#EDEDED',
        },
      };
    }

    const headerTable = newTableObject();
    const questionsTable = newTableObject();

    content.push(headerTable);

    let investigations = null;

    if (options.entityType === EntityTypes.ConstructionSiteInspection) {
      const constructionSiteIds = uniq(inspections.map(i => i.assetId));

      const citizenReports = await this._crRepo.fetch(customerId, {
        constructionSiteIds,
      });
      const illicitDischarges = await this._illicitRepo.fetch(customerId, {
        constructionSiteIds,
      });

      investigations = {
        citizenReports: citizenReports.length,
        illicitDischarges: illicitDischarges.length,
      };
    }

    const headerRows = this._getHeaderRowsForAllInspectionTypes(
      inspections,
      investigations
    );

    headerTable.table.body.push(...headerRows);

    content.push(questionsTable);

    // loop through each InspectionType to generate rows
    Object.entries(valuesByInspectionTypeId).forEach(
      ([inspectionTypeId, records]) => {
        const rows = this._getRowsForEachInspectionType(records, inspections);
        questionsTable.table.body.push(...rows);
      }
    );

    const styles = {
      ...this._defaultReport.styles,
      fieldStyle: {
        fillColor: '#EDEDED',
        bold: true,
        marginLeft: 5,
        marginTop: 5,
        marginBottom: 2.5,
      },
      optionStyle: { alignment: 'right', marginTop: 5 },
      valueStyle: { alignment: 'right', marginRight: 5, marginTop: 5 },
      inspectionTotalFieldStyle: { fontSize: 12, bold: true, marginLeft: 5 },
      countValueStyle: {
        fontSize: 12,
        bold: true,
        alignment: 'right',
        marginRight: 5,
      },
    };

    const docDefinition = {
      header: this._defaultReport.defaultHeader(customer.fullName),
      content,
      footer: this._defaultReport.defaultFooter(),
      styles,
      defaultStyle: this._defaultReport.defaultStyle,
    };

    // pdfDoc is a stream so you can pipe it to the file system
    const pdfDoc = this._defaultReport.pdfPrinter.createPdfKitDocument(
      docDefinition
    );
    return pdfDoc;
  }

  private _getHeaderRowsForAllInspectionTypes(
    inspections: InspectionSummaryModel[],
    investigations: { citizenReports: number; illicitDischarges: number } | null
  ) {
    const rows = [];
    const inspectionTotals = [
      {
        text: 'Inspections during the report period: ',
        margin: [0, 10],
        colSpan: 2,
        style: 'inspectionTotalFieldStyle',
      },
      null,
      {
        text: this._defaultReport.formatNumber(inspections.length),
        margin: [0, 10],
        style: 'countValueStyle',
      },
    ];

    const inspectionTime = inspections.reduce(
      (acc, inspection) =>
        inspection.timeIn && inspection.timeOut
          ? {
              seconds:
                acc.seconds +
                moment(inspection.timeOut).diff(inspection.timeIn, 'seconds'),
              count: acc.count + 1,
            }
          : acc,
      { seconds: 0, count: 0 }
    );

    const inspectionTypesContent = this._getInspectionTypeContent(inspections);
    const complianceStatusesContent = this._getComplianceStatusesContent(
      inspections
    );
    rows.push(inspectionTotals);

    if (investigations) {
      rows.push([
        {
          text: 'Citizen Reports: ',
          margin: [0, 10],
          colSpan: 2,
          style: 'inspectionTotalFieldStyle',
        },
        null,
        {
          text: this._defaultReport.formatNumber(investigations.citizenReports),
          margin: [0, 10],
          style: 'countValueStyle',
        },
      ]);

      rows.push([
        {
          text: 'Illicit Discharges: ',
          margin: [0, 10],
          colSpan: 2,
          style: 'inspectionTotalFieldStyle',
        },
        null,
        {
          text: this._defaultReport.formatNumber(
            investigations.illicitDischarges
          ),
          margin: [0, 10],
          style: 'countValueStyle',
        },
      ]);
    }

    if (inspectionTime.count > 0) {
      const averageMinutes = Math.round(
        inspectionTime.seconds / 60 / inspectionTime.count
      );

      const inspectionTimeTotal = [
        {
          text: 'Average Inspection Time (time out - time in): ',
          margin: [0, 10],
          style: 'inspectionTotalFieldStyle',
        },
        {
          text: `${averageMinutes} minute${averageMinutes === 1 ? '' : 's'} (${
            inspectionTime.count
          } inspection${inspectionTime.count === 1 ? '' : 's'})`,
          colSpan: 2,
          margin: [0, 10],
          style: 'countValueStyle',
        },
      ];

      rows.push(inspectionTimeTotal);
    }

    rows.push(...inspectionTypesContent);
    rows.push(...complianceStatusesContent);

    return rows;
  }

  private _getRowsForEachInspectionType(
    records: InspectionSummaryRecord[],
    inspections: any
  ) {
    const rows = [];
    const aggregateRecords = records.filter(
      r =>
        r.dataType === 'boolean' ||
        r.dataType === 'number' ||
        r.inputType === 'select'
    );
    if (!aggregateRecords.length) {
      return rows;
    }

    const inspectionTypeName = aggregateRecords[0].inspectionTypeName;

    // Table Header eg. `Inspection Type: Outfall Inspection    1705`
    const inspectionTotals = this._getInspectionTotals(
      inspections,
      inspectionTypeName
    );

    rows.push(...inspectionTotals);

    const valuesByCustomFieldId = groupBy(aggregateRecords, v =>
      v.fieldLabel.trim()
    );

    const complianceStatusesContent = this._getComplianceStatusesContent(
      inspections,
      inspectionTypeName
    );

    rows.push(...complianceStatusesContent);

    // Loop through each customFieldKey
    Object.entries(valuesByCustomFieldId).forEach(([cfId, cfValues]) => {
      const cf = valuesByCustomFieldId[cfId][0];
      const { dataType, fieldLabel, inputType } = cf;
      if (dataType === 'boolean') {
        rows.push(...this._getBooleanRows(cfValues));
      } else if (dataType === 'number') {
        rows.push(...this._getNumberRows(cfValues, fieldLabel));
      } else if (inputType === 'select') {
        rows.push(...this._getSelectRows(cfValues));
      }
    });

    return rows;
  }

  private _getBooleanRows(cfValues: InspectionSummaryRecord[]) {
    const cf = cfValues[0];
    const rows = [];
    if (!cfValues.length) {
      return rows;
    }

    const countsByValue = countBy(cfValues, v => v.value);

    const booleanMap = {
      null: 'N/A',
      true: 'Yes',
      false: 'No',
    };

    rows.push([
      {
        text: cf.fieldLabel,
        colSpan: 3,
        style: 'fieldStyle',
      },
      null,
      null,
    ]);

    Object.entries(countsByValue).forEach(([key, count], index) => {
      rows.push([
        null,
        { text: booleanMap[key], style: 'optionStyle' },
        {
          text: this._defaultReport.formatNumber(count),
          style: 'valueStyle',
        },
      ]);
    });

    return rows;
  }

  private _getNumberRows(
    cfValues: InspectionSummaryRecord[],
    fieldLabel: string
  ) {
    const rows = [];

    if (!cfValues.length) {
      return rows;
    }

    const cf = cfValues[0];

    let total = sumBy(cfValues, cfValue => parseAsNumber(cfValue.value));

    let mean = chain(cfValues)
      .filter(cfValue => {
        return fieldLabel.includes('Temperature') // No `0` values allowed for Temperature
          ? parseAsNumber(cfValue.value) > 0
          : cfValue;
      })
      .meanBy(cfValue => parseAsNumber(cfValue.value))
      .value();

    let min = chain(cfValues)
      .filter(cfValue => {
        return fieldLabel.includes('Temperature') // No `0` values allowed for Temperature
          ? parseAsNumber(cfValue.value) > 0
          : cfValue;
      })
      .minBy(cfValue => parseAsNumber(cfValue.value))
      .get('value')
      .round(0)
      .value();

    let max = chain(cfValues)
      .filter(cfValue => {
        return fieldLabel.includes('Temperature') // No `0` values allowed for Temperature
          ? parseAsNumber(cfValue.value) > 0
          : cfValue;
      })
      .maxBy(cfValue => parseAsNumber(cfValue.value))
      .get('value')
      .value();

    if (fieldLabel.includes('Temperature')) {
      mean = Math.round(mean);
      min = Math.round(min);
      max = Math.round(max);
    }

    rows.push(
      [
        {
          text: cf.fieldLabel,
          colSpan: 3,
          style: 'fieldStyle',
        },
        null,
        null,
      ],
      [
        null,
        {
          text: 'Total',
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(total),
          style: 'valueStyle',
        },
      ],
      [
        null,
        {
          text: 'Average',
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(mean),
          style: 'valueStyle',
        },
      ],
      [
        null,
        {
          text: 'Minimum Value',
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(min),
          style: 'valueStyle',
        },
      ],
      [
        null,
        {
          text: 'Maximum Value',
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(max),
          style: 'valueStyle',
        },
      ]
    );
    return rows;
  }

  private _getSelectRows(cfValues: InspectionSummaryRecord[]) {
    const rows = [];
    if (!cfValues.length) {
      return rows;
    }
    const cf = cfValues[0];

    const countsByValue = countBy(cfValues, cfValue => cfValue.value);

    // merge 'null' and 'N/A'
    if (countsByValue['null']) {
      countsByValue['N/A'] =
        (countsByValue['N/A'] || 0) + (countsByValue['null'] || 0);
      delete countsByValue['null'];
    }

    rows.push([
      {
        text: cf.fieldLabel,
        colSpan: 3,
        style: 'fieldStyle',
      },
      null,
      null,
    ]);

    Object.entries(countsByValue)
      .filter(([o, count]) => count > 0)
      .forEach(([option, count], index) => {
        rows.push([
          {
            text: option,
            style: 'optionStyle',
            colSpan: 2,
          },
          null,
          {
            text: this._defaultReport.formatNumber(count),
            style: 'valueStyle',
          },
        ]);
      });
    return rows;
  }

  private _getInspectionTotals(inspections, inspectionTypeName: string) {
    const inspectionTypeCountByName = countBy(
      inspections,
      i => i.inspectionTypeName
    );

    const rows = [];
    rows.push([
      {
        text: `Inspection Type: ${inspectionTypeName}`,
        margin: [0, 10],
        colSpan: 2,
        style: 'inspectionTotalFieldStyle',
      },
      null,
      {
        text: `${inspectionTypeCountByName[inspectionTypeName]}`,
        margin: [0, 10],
        style: 'countValueStyle',
      },
    ]);
    return rows;
  }

  private _getInspectionTypeContent(inspections: InspectionSummaryModel[]) {
    const inspectionTypeCountByName = countBy(
      inspections,
      i => i.inspectionTypeName
    );
    const sortedEntries = Object.entries(inspectionTypeCountByName).sort(
      ([key1, ct1], [k2, ct2]) => ct2 - ct1 // reverse
    );
    const textArray: any[] = [];
    textArray.push([
      {
        text: `Inspection Types`,
        colSpan: 3,
        style: 'fieldStyle',
      },
    ]);
    sortedEntries.forEach(([name, ct]) => {
      const percentage = (ct / inspections.length * 100).toFixed(2);
      textArray.push([
        null,
        {
          text: name,
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(ct) + ` (${percentage}%)`,
          style: 'valueStyle',
        },
      ]);
    });
    return textArray;
  }

  private _getComplianceStatusesContent(
    totalInspections: InspectionSummaryModel[],
    inspectionTypeName?: string
  ) {
    let inspections = totalInspections;

    if (inspectionTypeName) {
      inspections = inspections.filter(
        p => p.inspectionTypeName === inspectionTypeName
      );
    }

    const complianceStatusesByName = countBy(
      inspections,
      i => i.complianceStatus
    );
    const sortedEntries = Object.entries(complianceStatusesByName).sort(
      ([key1, ct1], [k2, ct2]) => ct2 - ct1 // reverse
    );
    const textArray: any[] = [];
    textArray.push([
      {
        text: `Compliance Status`,
        colSpan: 3,
        style: 'fieldStyle',
      },
      null,
      null,
    ]);
    sortedEntries.forEach(([name, ct]) => {
      const fixedName = name === 'null' ? 'N/A' : name;
      const percentage = (ct / inspections.length * 100).toFixed(2);
      textArray.push([
        null,
        {
          text: fixedName,
          style: 'optionStyle',
        },
        {
          text: this._defaultReport.formatNumber(ct) + ` (${percentage}%)`,
          style: 'valueStyle',
        },
      ]);
    });
    return textArray;
  }
}

// parses string to a number, otherwise null
function parseAsNumber(s: string) {
  const num = +s;
  return num === NaN ? null : num;
}
