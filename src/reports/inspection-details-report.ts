import * as moment from 'moment';
import 'moment-timezone';
import * as _ from 'lodash';
import {
  Customer,
  Contact,
  CustomField,
  ConstructionSite,
  ConstructionSiteInspection,
  FacilityInspection,
  Facility,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
  User,
  FileWithLinks,
  ComplianceStatus,
  Entity,
} from '../models';
import {
  ComplianceSummary,
  EntityTypes,
  Env,
  getComplianceSummary,
} from '../shared';
import { DefaultPdfReport } from './default-pdf-report';
import { injectable } from 'inversify';
import * as fs from 'fs';
import * as sharp from 'sharp';
import {
  CommunityRepository,
  CustomFieldRepository,
  ReceivingWaterRepository,
} from '../data';

type EntityType =
  | typeof EntityTypes.ConstructionSite
  | typeof EntityTypes.Facility
  | typeof EntityTypes.Outfall
  | typeof EntityTypes.Structure;

type EntityInspectionType =
  | typeof EntityTypes.ConstructionSiteInspection
  | typeof EntityTypes.FacilityInspection
  | typeof EntityTypes.OutfallInspection
  | typeof EntityTypes.StructureInspection;

type Inspections =
  | ConstructionSiteInspection
  | FacilityInspection
  | OutfallInspection
  | StructureInspection;

interface Details {
  constructionSite?: ConstructionSite;
  constructionSiteInspection?: ConstructionSiteInspection;
  facility?: Facility;
  facilityInspection?: FacilityInspection;
  outfall?: Outfall;
  outfallInspection?: OutfallInspection;
  structure?: Structure;
  structureInspection?: StructureInspection;
  followUpInspection: Inspections | null;
  parentInspection: Inspections | null;
  contacts?: Contact[];
  inspectors: User[];
  files: FileWithLinks[];
  customFields: CustomField[];
}

const FRANKLIN_CUSTOMER = 'OH_FSWCD';

const FRANKLIN_CUSTOMER_EMAIL = 'bfrusher@franklinswcd.org';

const FRANKLIN_CUSTOMER_EMAIL_BODY =
  'Please see attached report and photos from the last sediment and erosion control inspection performed at your site.';

const FRANKLIN_CUSTOMER_MAX_NAME_LENGTH_LIMIT = 40;

const TEXT_LENGTH_LIMIT = 70;

const FRANKLIN_CUSTOMER_CUSTOM_FIELDS_TITLES = [
  'Construction Entrance',
  'Roads/Off-Site Tracking',
  'Silt Fence/Straw Wattle/Berm',
  'Inlet Protection',
  'Sediment Basins/Traps',
  'Stabilization: Temporary',
  'Stabilization: Permanent',
  'Dewatering',
  'Non-Sediment Controls',
  'Inspection Logs',
];

const entityClassMap: Record<EntityType, typeof Entity> = {
  [EntityTypes.ConstructionSite]: ConstructionSite,
  [EntityTypes.Facility]: Facility,
  [EntityTypes.Outfall]: Outfall,
  [EntityTypes.Structure]: Structure,
};

@injectable()
export class InspectionDetailsReport {
  env = process.env as Env;

  constructor(
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _defaultRpt: DefaultPdfReport
  ) {}

  async generate(
    type: EntityType,
    details: Details,
    customer: Customer,
    complianceStatuses: ComplianceStatus[]
  ) {
    const docDefinition = {
      header: customer.id === FRANKLIN_CUSTOMER ? null : this._header(customer),
      content: await this._content(type, details, customer, complianceStatuses),
      footer: this._footer.bind(this),
      styles: this._styles(),
      defaultStyle: this._defaultRpt.defaultStyle,
      pageSize: customer.id === FRANKLIN_CUSTOMER ? 'LEGAL' : 'A4',
      pageMargins: [40, customer.id === FRANKLIN_CUSTOMER ? 20 : 40, 40, 60],
    };

    const pdfDoc = this._defaultRpt.pdfPrinter.createPdfKitDocument(
      docDefinition,
      {
        tableLayouts: {
          headerFirstHalfInputsForFranklin: {
            defaultBorder: false,
            hLineWidth: () => 0.5,
            vLineWidth: () => 0.5,
            hLineStyle: () => ({ dash: { length: 0.5, space: 1 } }),
            vLineStyle: () => ({ dash: { length: 0.5, space: 1 } }),
            paddingLeft: () => 5,
            paddingRight: () => 5,
            hLineColor: () => '#999999',
            vLineColor: () => '#999999',
          },
          headerSecondHalfInputsForFranklin: {
            defaultBorder: false,
            hLineWidth: () => 0.5,
            vLineWidth: () => 0.5,
            hLineStyle: () => ({ dash: { length: 0.5, space: 1 } }),
            vLineStyle: () => ({ dash: { length: 0.5, space: 1 } }),
            paddingLeft: () => 5,
            paddingRight: () => 5,
            fillColor: () => '#EBF1DE',
            hLineColor: () => '#999999',
            vLineColor: () => '#999999',
          },
          primaryContactForFranklin: {
            defaultBorder: false,
            hLineWidth: () => 0.5,
            vLineWidth: () => 0.5,
            fillColor: () => '#F2DCDB',
            paddingLeft: () => 5,
            paddingRight: () => 5,
          },
        },
      }
    );

    return pdfDoc;
  }

  async multiGenerate(
    type: EntityType,
    details: Details[],
    customer: Customer,
    complianceStatuses: ComplianceStatus[]
  ) {
    const contents = [];

    const complianceSummary = getComplianceSummary(
      details.map(detail => this._getValue(`${type}Inspection`, null, detail)),
      type,
      complianceStatuses
    );

    for (let i = 0; i < details.length; i++) {
      const content = await this._content(
        type,
        details[i],
        customer,
        complianceStatuses,
        complianceSummary
      );

      if (i !== details.length - 1) {
        content['pageBreak'] = 'after';
      }

      contents.push(content);
    }

    const docDefinition = {
      header: customer.id === FRANKLIN_CUSTOMER ? null : this._header(customer),
      content: contents,
      footer: this._footer.bind(this),
      styles: this._styles(),
      defaultStyle: this._defaultRpt.defaultStyle,
      pageSize: customer.id === FRANKLIN_CUSTOMER ? 'LEGAL' : 'A4',
      pageMargins: [40, customer.id === FRANKLIN_CUSTOMER ? 20 : 40, 40, 60],
    };

    const pdfDoc = this._defaultRpt.pdfPrinter.createPdfKitDocument(
      docDefinition
    );

    return pdfDoc;
  }

  private async _content(
    type: EntityType,
    details: Details,
    customer: Customer,
    complianceStatuses: ComplianceStatus[],
    complianceSummary?: ComplianceSummary
  ) {
    const customerBanner = await this._getCustomerBanner(
      type,
      details,
      customer
    );

    if (
      customer.id === FRANKLIN_CUSTOMER &&
      type === EntityTypes.ConstructionSite
    ) {
      const contents = await this._contentForFranklin(type, details, customer);

      return { stack: [customerBanner, ...contents] };
    }

    const hiddenFields: string[] = customer.hiddenFields['Inspection'];
    const detailsHeader = {
      text: this._getHeaderText(type) + ' Details',
      style: 'header',
    };

    const {
      assetStatusColor,
      inspectionStatusColor,
    } = this._getComplianceStatusColor(complianceStatuses, type, details);

    const detailsTableContent = this._getDetailsTableContent(
      type,
      details,
      assetStatusColor,
      customer.timezone,
      complianceSummary
    )
      .map((val: any[]) => this._toKeyValueColumn(val))
      .reduce(this._setTableColumns(2), []);

    const detailsTable = {
      table: {
        body: [['', ''], ...detailsTableContent],
        widths: ['*', '*'],
        headerRows: 1,
        dontBreakRows: true,
      },
      layout: this._layout(),
    };

    const inspectionsHeader = {
      text: 'Inspection Properties',
      style: 'subheader',
    };

    const inspectionsTableContent = [
      ...this._getInspectionTableContent(
        type + 'Inspection',
        details,
        hiddenFields,
        inspectionStatusColor,
        customer.timezone
      ),
    ]
      .map((val: any[]) => this._toKeyValueColumn(val))
      .reduce(this._setTableColumns(2), []);

    const inspectionTable = {
      table: {
        body: [['', ''], ...inspectionsTableContent],
        widths: ['*', '*'],
        headerRows: 1,
        dontBreakRows: true,
      },
      layout: this._layout(),
    };

    const customFields = this._getCustomFieldsContent(
      details,
      customer.timezone
    );

    const additionalInfoHeader = {
      text: 'Additional Information',
      style: 'subheader',
    };

    const additionalInfoContent = this._getValue(
      type + 'Inspection',
      'additionalInformation',
      details,
      'No additional information to display.'
    );

    const additionalInfoTable = {
      table: {
        body: [[''], [additionalInfoContent]],
        widths: ['*'],
        headerRows: 1,
      },
      layout: this._layout(),
    };

    const weatherHeader = { text: 'Weather', style: 'subheader' };

    const weatherTableContent = this._getWeatherTableContent(type, details)
      .map((val: any[]) => this._toKeyValueColumn(val))
      .reduce(this._setTableColumns(2), []);

    const weatherTable = {
      table: {
        body: [['', ''], ...weatherTableContent],
        widths: ['*', '*'],
        headerRows: 1,
      },
      layout: this._layout(),
    };

    const contactsHeader = { text: 'Contacts', style: 'subheader' };

    const contactsTableContent = this._getContactsTableContent(details);

    const contactsContent = contactsTableContent.length
      ? {
          table: {
            body: [['', '', '', ''], ...contactsTableContent],
            headerRows: 1,
            widths: ['auto', 'auto', 'auto', '*'],
          },
          layout: this._layout(),
        }
      : {
          table: {
            body: [
              [''],
              [
                {
                  text: 'No contacts to display.',
                  color: '#616161',
                },
              ],
            ],
            widths: ['*'],
            headerRows: 1,
          },
          layout: this._layout(),
        };

    const locationHeader = { text: 'Location', style: 'subheader' };

    const location = [
      this._getValue(type + 'Inspection', 'lat', details),
      this._getValue(type + 'Inspection', 'lng', details),
    ]
      .filter(Boolean)
      .join(', ');

    const locationContent = {
      table: {
        body: [
          [''],
          [
            {
              text: location || 'No location to display.',
              color: !location && '#616161',
            },
          ],
        ],
        widths: ['*'],
        headerRows: 1,
      },
      layout: this._layout(),
    };

    const photosHeader = { text: 'Photos', style: 'subheader' };

    const photoTableContent = await this._getPhotoContent(
      details,
      customer
    ).reduce(this._setTableColumns(3), []);

    const photoTable = {
      table: {
        body: [Array(3).fill(''), ...photoTableContent],
        widths: Array(3).fill('*'),
        headerRows: 1,
      },
      layout: this._photoLayout(),
    };

    const spacer = { text: '', margin: [0, 8] };

    const content = {
      stack: [
        customerBanner,
        spacer,
        detailsHeader,
        detailsTable,
        spacer,
        inspectionsHeader,
        inspectionTable,
        customFields,
        spacer,
        additionalInfoHeader,
        additionalInfoTable,
        spacer,
        weatherHeader,
        weatherTable,
        spacer,
        contactsHeader,
        contactsContent,
        spacer,
        locationHeader,
        locationContent,
        spacer,
        photosHeader,
        photoTable,
      ],
    };

    return content;
  }

  private async _contentForFranklin(
    type: EntityType,
    details: Details,
    customer: Customer
  ) {
    const { customFields } = details;
    const inspectionType = `${type}Inspection`;
    const spacer = { text: '', margin: [0, 8] };
    const dischargeOccurringCustomField = customFields.find(
      cf => cf.fieldLabel === 'Discharge Occurring'
    );
    const additionalInspector1CustomField = customFields.find(
      cf => cf.fieldLabel === 'Additional Inspector 1'
    );
    const additionalInspector2CustomField = customFields.find(
      cf => cf.fieldLabel === 'Additional Inspector 2'
    );

    const entityCustomFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
      entityClassMap[type],
      this._getValue(type, 'id', details),
      customer.id
    );

    const parcelIdCustomField = entityCustomFields.find(
      cf => cf['fieldLabel'] === 'Parcel ID/Address/Lot #'
    );
    const noiCustomField = entityCustomFields.find(
      cf => cf['fieldLabel'] === 'NOI'
    );
    const ldrPermitField = entityCustomFields.find(
      cf => cf['fieldLabel'] === 'LDR Permit'
    );

    const parcelId = parcelIdCustomField
      ? parcelIdCustomField.value || 'NA'
      : 'NA';
    const noi = noiCustomField ? noiCustomField.value || 'NA' : 'NA';
    const ldrPermit = ldrPermitField ? ldrPermitField.value || 'NA' : 'NA';

    let community = 'NA';
    let receivingWaters = 'NA';

    if (this._getValue(type, 'communityId', details)) {
      const communityEntity = await this._communityRepo.fetchById(
        this._getValue(type, 'communityId', details),
        customer.id
      );

      if (communityEntity) {
        community = communityEntity.name;
      }
    }

    if (this._getValue(type, 'receivingWatersId', details)) {
      const receivingWatersEntity = await this._receivingWaterRepo.fetchById(
        this._getValue(type, 'receivingWatersId', details),
        customer.id
      );

      if (receivingWatersEntity) {
        receivingWaters = receivingWatersEntity.name;
      }
    }

    const primaryContact = (details.contacts || []).find(
      c => (c as any).isPrimary
    );

    const detailTable = {
      table: {
        body: [
          [
            [
              {
                layout: 'headerFirstHalfInputsForFranklin',
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Project Name:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: this._getValue(type, 'name', details),
                        style: 'headerValueForFranklin',
                        border: [true, false, false, true],
                        maxHeight: 20,
                        fontSize:
                          this._getValue(type, 'name', details).length >=
                          FRANKLIN_CUSTOMER_MAX_NAME_LENGTH_LIMIT
                            ? 8
                            : 9,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerFirstHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Municipality/Township:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: community,
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerFirstHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Parcel ID/Address/Lot #:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: parcelId,
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerFirstHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*', 'auto', 'auto'],
                  body: [
                    [
                      {
                        text: 'NOI:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        // No breakable characters in string. Break ourselves.
                        // 12 char should never be reached based on format.
                        text: noi.substring(0, 12),
                        height: '20',
                        style: 'headerValueForFranklin',
                        border: [true, true, true, true],
                        maxHeight: 20,
                      },
                      {
                        text: 'Acreage Disturbed:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text:
                          this._getValue(type, 'disturbedArea', details) ||
                          'NA',
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerFirstHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Receiving Waters:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: receivingWaters,
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerFirstHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: [
                          'LDR Permit ',
                          {
                            text: '(Prairie Township Only):',
                            bold: false,
                          },
                        ],
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: ldrPermit,
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'primaryContactForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Primary Contact:',
                        style: 'headerLabelForFranklin',
                        border: [false, true, false, false],
                      },
                      {
                        text: primaryContact
                          ? [primaryContact.name, primaryContact.phone]
                              .filter(Boolean)
                              .join(', ')
                          : '',
                        style: 'headerValueForFranklin',
                        border: [false, true, false, false],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
            ],
            [
              {
                layout: 'headerSecondHalfInputsForFranklin',
                table: {
                  widths: [60, '*'],
                  body: [
                    [
                      {
                        text: 'Inspector(s)',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: this._getInspector(inspectionType, details),
                        style: 'headerValueForFranklin',
                        border: [true, false, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: [60, '*'],
                  body: [
                    [
                      {
                        text: 'Enter up to 3',
                        style: 'headerLabelForFranklin',
                        italics: true,
                        bold: false,
                        fontSize: 8,
                        alignment: 'center',
                      },
                      {
                        text:
                          additionalInspector1CustomField &&
                          additionalInspector1CustomField['value']
                            ? additionalInspector1CustomField['value']
                            : 'Select Inspectors',
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        color:
                          additionalInspector1CustomField &&
                          additionalInspector1CustomField['value']
                            ? 'black'
                            : 'grey',
                        italics:
                          additionalInspector1CustomField &&
                          additionalInspector1CustomField['value']
                            ? false
                            : true,
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: [60, '*'],
                  body: [
                    [
                      {
                        text: '',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text:
                          additionalInspector2CustomField &&
                          additionalInspector2CustomField['value']
                            ? additionalInspector2CustomField['value']
                            : 'Select Inspectors',
                        style: 'headerValueForFranklin',
                        color:
                          additionalInspector2CustomField &&
                          additionalInspector2CustomField['value']
                            ? 'black'
                            : 'grey',
                        italics:
                          additionalInspector2CustomField &&
                          additionalInspector2CustomField['value']
                            ? false
                            : true,
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Date/Time:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: this._toFranklinCustomerDateTime(
                          this._getValue(
                            inspectionType,
                            'inspectionDate',
                            details
                          ),
                          this._getValue(inspectionType, 'timeIn', details),
                          customer.timezone
                        ),
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*', 'auto', '*'],
                  body: [
                    [
                      {
                        text: 'Temp:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text: this._getValue(
                          inspectionType,
                          'weatherTemperatureF',
                          details
                        )
                          ? `${this._getValue(
                              inspectionType,
                              'weatherTemperatureF',
                              details
                            )}°`
                          : 'NA',
                        height: '20',
                        style: 'headerValueForFranklin',
                        border: [true, true, true, true],
                        maxHeight: 20,
                      },
                      {
                        text: 'Climate:',
                        style: 'headerLabelForFranklin',
                        border: [false, false, false, true],
                      },
                      {
                        text:
                          this._getValue(
                            inspectionType,
                            'weatherCondition',
                            details
                          ) || 'NA',
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Discharge Occurring?',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text:
                          dischargeOccurringCustomField &&
                          dischargeOccurringCustomField['value'] === 'true'
                            ? 'Yes'
                            : 'No',
                        style: 'headerValueForFranklin',
                        border: [true, true, false, true],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
              {
                layout: 'headerSecondHalfInputsForFranklin',
                margin: [0, -0.5, 0, 0],
                table: {
                  widths: ['auto', '*'],
                  body: [
                    [
                      {
                        text: 'Type of Inspection:',
                        style: 'headerLabelForFranklin',
                      },
                      {
                        text:
                          this._getValue(
                            inspectionType,
                            'inspectionType',
                            details
                          ) || 'NA',
                        style: 'headerValueForFranklin',
                        border: [true, true, false, false],
                        maxHeight: 20,
                      },
                    ],
                  ],
                },
              },
            ],
          ],
        ],
        widths: [265, 264],
      },
      layout: {
        paddingLeft: () => 0,
        paddingRight: () => 0,
        paddingTop: () => 0,
        paddingBottom: () => 0,
      },
    };

    const inspectionCheckListHeader = {
      table: {
        body: [
          [
            {
              text: 'Inspection Checklist',
              colSpan: 3,
              fillColor: '#d9d9d9',
              alignment: 'center',
              bold: true,
              fontSize: 13,
              margin: [0, 2, 0, 0],
              border: [true, false, true, true],
            },
            '',
            '',
          ],
          [
            {
              text: 'BMP/Activity',
              fillColor: '#c5d9f1',
              bold: true,
              fontSize: 11,
              alignment: 'center',
              margin: [0, 7, 0, 0],
              border: [true, true, false, true],
            },
            {
              text: [
                {
                  text: 'Maintenance Needed?\n',
                  alignment: 'center',
                  bold: true,
                  fontSize: 11,
                },
                {
                  text: 'Timeline for Action',
                  italics: true,
                  alignment: 'center',
                  fontSize: 7,
                },
              ],
              fillColor: '#c5d9f1',
              border: [true, true, false, true],
            },
            {
              text: [
                {
                  text: 'Comments\n',
                  alignment: 'center',
                  bold: true,
                  fontSize: 12,
                },
                {
                  text: 'Any applicable photos are attached',
                  alignment: 'center',
                  italics: true,
                  fontSize: 7,
                },
              ],
              fillColor: '#c5d9f1',
              border: [true, true, true, true],
            },
          ],
        ],
        widths: [140, 120, '*'],
      },
      layout: {
        hLineWidth: i => (i === 2 ? 0.35 : 1),
      },
    };

    const inspectionCheckListContent = FRANKLIN_CUSTOMER_CUSTOM_FIELDS_TITLES.map(
      (field, index) => ({
        table: {
          body: [
            [
              {
                text: [
                  { text: `${field}\n`, fontSize: 10, bold: true },
                  {
                    text: customFields[(index + 1) * 4 - 1][
                      'sectionSubtitle'
                    ].substring(0, 130),
                    italics: true,
                    fontSize: 6,
                  },
                ],
                rowSpan: 2,
              },
              {
                text: customFields[(index + 1) * 4]['value'],
                ...this._stylesForFranklinMaintenanceNeed(
                  customFields[(index + 1) * 4]['value']
                ),
                alignment: 'center',
                fontSize: 10,
                bold: true,
                margin: [0, 2],
              },
              {
                text: customFields[(index + 1) * 4 + 2]['value'] || '',
                fillColor: '#eeece1',
                rowSpan: 2,
                maxHeight: 50,
                fontSize: 9,
              },
            ],
            [
              '',
              {
                text:
                  customFields[(index + 1) * 4 + 1]['value'] ||
                  'No Action Required',
                alignment: 'center',
                ...this._stylesForFranklinTimelineAction(
                  customFields[(index + 1) * 4 + 1]['value']
                ),
                fontSize: 9,
                margin: [0, 2, 0, 0],
              },
              '',
            ],
          ],
          widths: [140, 120, '*'],
        },
        layout: {
          hLineWidth: () => 0.65,
        },
        unbreakable: true,
      })
    );

    const otherNotesCustomField = customFields.find(
      cf => cf.fieldLabel === 'Other Notes/Observations'
    );

    const otherNotes = {
      table: {
        body: [
          [
            {
              text: 'Other Notes/Observations:',
              fillColor: '#ffff99',
              fontSize: 10,
              bold: true,
              alignment: 'center',
              margin: [0, 5],
            },
            {
              text: otherNotesCustomField ? otherNotesCustomField['value'] : '',
              fillColor: '#eeece1',
              maxHeight: 50,
              fontSize: 9,
            },
          ],
        ],
        widths: [80, '*'],
      },
      unbreakable: true,
    };

    const actionNotes = {
      table: {
        body: [
          [
            {
              text: [
                {
                  text: 'Type of Action(s) Taken ',
                  fontSize: 9,
                  bold: true,
                  alignment: 'center',
                },
                {
                  text: '(select at least 1 action)',
                  fontSize: 9,
                  italics: true,
                  alignment: 'center',
                },
              ],
              fillColor: '#c5d9f1',
            },
            {
              text: 'Name/Date/Other Details',
              fontSize: 9,
              bold: true,
              fillColor: '#c5d9f1',
              alignment: 'center',
            },
          ],
          ...Array.from({ length: 5 }).map((_empty, index) => [
            {
              text: customFields[index * 2 + 46]['value'],
              alignment: 'center',
              ...this._stylesForFranklinActionType(
                customFields[index * 2 + 46]['value']
              ),
            },
            {
              text: customFields[index * 2 + 47]['value'],
              alignment: 'center',
              fillColor: customFields[index * 2 + 47]['value']
                ? '#fff'
                : '#eeece1',
              maxHeight: 20,
            },
          ]),
        ],
        widths: ['*', '*'],
        heights: 8,
      },
    };

    const reportInfoContent = {
      table: {
        body: [
          [
            {
              text: `Site Inspection Report | ${this._getValue(
                type,
                'name',
                details
              )}`.substring(0, 100),
              fillColor: '#fde9da',
              margin: [10, 0, 0, 0],
              italics: true,
            },
            {
              text: 'Open Outlook',
              fillColor: '#fde9da',
              alignment: 'right',
              color: '#0607FF',
              decoration: 'underline',
              bold: true,
              margin: [0, 0, 10, 0],
              link: `mailto:?subject=Site Inspection Report | ${this._getValue(
                type,
                'name',
                details
              )}&bcc=${FRANKLIN_CUSTOMER_EMAIL}&body=${FRANKLIN_CUSTOMER_EMAIL_BODY}`,
            },
          ],
        ],
        widths: ['*', 'auto'],
      },
      layout: 'noBorders',
    };

    const contents: any[] = [
      detailTable,
      inspectionCheckListHeader,
      inspectionCheckListContent,
      spacer,
      otherNotes,
      spacer,
      actionNotes,
      spacer,
      reportInfoContent,
    ];

    const photoTableContent = await this._getPhotoContent(
      details,
      customer,
      true
    ).reduce(this._setTableColumns(2), []);

    if (photoTableContent.length) {
      const photosHeader = {
        text: '*Any applicable photos are attached',
        fontSize: 9,
        bold: true,
      };

      const photoTable = {
        table: {
          body: [Array(2).fill(''), ...photoTableContent],
          widths: Array(2).fill('*'),
          headerRows: 1,
        },
        layout: this._photoLayout(true),
      };

      contents.push(spacer, photosHeader, photoTable);
    }

    return contents;
  }

  private _stylesForFranklinMaintenanceNeed(value: string) {
    switch (value) {
      case 'Yes (1st Notice)':
        return { fillColor: '#ffff00', color: '#000' };

      case 'Yes (2nd Notice)':
        return { fillColor: '#fec000', color: '#000' };

      case 'Yes (3rd Notice)':
        return { fillColor: '#ff0200', color: '#fff' };

      default:
        return { fillColor: '#00b050', color: '#fff' };
    }
  }

  private _stylesForFranklinTimelineAction(value: string) {
    switch (value) {
      case 'Within 2-Days':
      case 'Within 3-Days':
      case 'Within 7-Days':
      case 'Within 10-Days':
      case 'Install BMP Within 10-Days':
        return { fillColor: '#fff', color: '#000', italics: true, bold: true };

      case '3-Day Follow-up Scheduled':
        return { fillColor: '#fff', color: '#c00000', bold: true };

      case 'In Enforcement':
        return { fillColor: '#f2dcda', color: '#c00000', bold: true };

      default:
        return { fillColor: '#fff', color: '#000', italics: true };
    }
  }

  private _stylesForFranklinActionType(value: string) {
    switch (value) {
      case 'Written inspection sent to builder/owner':
      case 'Verbal compliance with on-site contact':
        return { fillColor: '#fff', color: '#000' };

      case 'Follow-up inspection scheduled':
        return { fillColor: '#fff', color: '#c00000', bold: true };

      case 'Temporary stop-work order recommended':
        return { fillColor: '#f2dcda', color: '#c00000', bold: true };

      case 'All building inspections & permits suspended':
        return { fillColor: '#c00000', color: '#fff', bold: true };

      default: {
        return { fillColor: '#eeece1' };
      }
    }
  }

  private _styles() {
    return {
      ...this._defaultRpt.styles,
      bannerHeader: {
        fontSize: 19,
        bold: true,
      },
      bannerSubheader: {
        fontSize: 13,
      },
      headerLabelForFranklin: {
        margin: [0, 0, 10, 0],
        fontSize: 9,
        bold: true,
        noWrap: true,
      },
      headerValueForFranklin: {
        fontSize: 9,
      },
    };
  }

  private _header(customer: Customer) {
    return this._defaultRpt.defaultHeader(customer.fullName);
  }

  private _footer(currentPage, pageCount) {
    return {
      columns: [
        {
          text: currentPage.toString() + ' of ' + pageCount,
          alignment: 'right',
          margin: [0, 20, 40, 0],
        },
      ],
    };
  }

  private _layout(isContinuation?: boolean) {
    return {
      hLineWidth: function(i, node) {
        if (i === 0 || i === node.table.body.length) {
          return 0;
        }
        return i === node.table.headerRows ? 2 : 1;
      },
      vLineWidth: function(i) {
        return 0;
      },
      hLineColor: function(i) {
        return i !== 1 || isContinuation ? '#aaa' : 'black';
      },
      paddingLeft: function(i) {
        return i === 0 ? 0 : 8;
      },
      paddingRight: function(i, node) {
        return i === node.table.widths.length - 1 ? 0 : 8;
      },
      paddingTop: function(i, node) {
        return i === 0 ? 2 : 5;
      },
      paddingBottom: function(i, node) {
        return i === 0 ? 0 : 5;
      },
    };
  }

  private _photoLayout(noHeaderRow?: boolean) {
    const padding = 5;
    return {
      hLineWidth: function(i, node) {
        if (i === 0 || i === node.table.body.length) {
          return 0;
        }
        return !noHeaderRow && i === node.table.headerRows ? 2 : 0;
      },
      vLineWidth: function(i) {
        return 0;
      },
      hLineColor: function(i) {
        return i === 1 ? 'black' : 'white';
      },
      paddingLeft: function(i) {
        return i === 0 ? 0 : padding;
      },
      paddingRight: function(i, node) {
        return i === node.table.widths.length - 1 ? 0 : padding;
      },
      paddingTop: function(i, node) {
        return i === 0 ? 0 : padding;
      },
      paddingBottom: function(i, node) {
        return i === 0 ? 0 : padding;
      },
    };
  }

  private async _getCustomerBanner(
    type: string,
    details: Details,
    customer: Customer
  ) {
    const name =
      type === 'Outfall'
        ? this._getValue(type, 'location', details)
        : this._getValue(type, 'name', details);

    const margin = [0, 3]; // [left/write, top/bottom]

    let logoBanner;

    if (
      customer.id === FRANKLIN_CUSTOMER &&
      type === EntityTypes.ConstructionSite
    ) {
      logoBanner = {
        table: {
          body: [
            [
              await this._getCustomerLogo(customer, type),
              {
                text:
                  'Construction Site Erosion and Sediment Control Inspection',
                alignment: 'center',
                fontSize: 14,
                bold: true,
                margin: [0, 8, 0, 0],
                border: [false, true, true, false],
                fillColor: '#bfbfbf',
              },
            ],
          ],
          widths: ['auto', '*'],
        },
      };
    } else {
      logoBanner = {
        table: {
          body: [
            ['', ''],
            [
              await this._getCustomerLogo(customer, type),
              {
                stack: [
                  {
                    text: name,
                    style: 'bannerHeader',
                    margin,
                  },
                  {
                    text: this._getValue(
                      type + 'Inspection',
                      'inspectionType',
                      details
                    ),
                    style: 'bannerSubheader',
                    margin,
                  },
                  { text: customer.fullName, style: 'bannerSubheader', margin },
                ],
              },
            ],
          ],
          widths: ['auto', '*'],
        },
        layout: 'noBorders',
      };
    }

    const noLogoBanner = {
      stack: [
        { text: name, style: 'bannerHeader', margin },
        {
          text: this._getValue(type + 'Inspection', 'inspectionType', details),
          style: 'bannerSubheader',
          margin,
        },
        { text: customer.fullName, style: 'bannerSubheader', margin },
      ],
    };

    return !customer.logoFileName ? noLogoBanner : logoBanner;
  }

  private async _getCustomerLogo(customer: Customer, type: string) {
    if (!customer.logoFileName) {
      return null;
    }

    const url = `${this.env.FILE_STORAGE_PATH}/${customer.id}/${
      customer.logoThumbnailName
    }`;

    const imageData = await this._toBase64(url);

    // `imageData` may be null if the original image does not exist on disk
    if (!imageData) {
      return null;
    }

    const mimeType = this._getMimeType(customer.logoThumbnailName);

    if (
      customer.id === FRANKLIN_CUSTOMER &&
      type === EntityTypes.ConstructionSite
    ) {
      return {
        image: `data:${mimeType};base64,${imageData}`,
        fit: [150, 130],
        border: [true, true, false, false],
        fillColor: '#bfbfbf',
      };
    }

    return {
      image: `data:${mimeType};base64,${imageData}`,
      fit: [80, 80],
      margin: [0, 0, 20, 10],
    };
  }

  private _getHeaderText(type: EntityType) {
    const headerMap = new Map<EntityType, string>([
      ['ConstructionSite', 'Construction Site'],
      ['Facility', 'Facility'],
      ['Outfall', 'Outfall'],
      ['Structure', 'Structure'],
    ]);
    return headerMap.get(type);
  }

  private _getDetailsTableContent(
    type: EntityType,
    details: Details,
    complianceStatusColor: string,
    timezone: string,
    complianceSummary?: ComplianceSummary
  ) {
    let content = [];
    switch (type) {
      case EntityTypes.ConstructionSite:
      case EntityTypes.Structure:
      case EntityTypes.Facility: {
        const physicalAddress = this._getAddress(type, details);
        content = [
          ['Name', this._getValue(type, 'name', details)],
          [
            'Compliance',
            {
              text: this._getValue(type, 'complianceStatus', details),
              color: complianceStatusColor,
            },
          ],
          [
            'Added',
            this._toCustomerDate(
              this._getValue(type, 'dateAdded', details),
              timezone
            ),
          ],
          [
            'Previously Inspected On',
            this._toCustomerDate(
              this._getValue(type, 'previousInspectionDate', details),
              timezone
            ),
          ],
          [
            'Site Physical Address',
            physicalAddress !== '' ? physicalAddress : 'NA',
          ],
          ['Tracking ID', this._getValue(type, 'trackingId', details) || 'NA'],
        ];
        break;
      }
      case EntityTypes.Outfall: {
        content = [
          ['Location', this._getValue(type, 'location', details)],
          [
            'Compliance',
            {
              text: this._getValue(type, 'complianceStatus', details),
              color: complianceStatusColor,
            },
          ],
          [
            'Added',
            this._toCustomerDate(
              this._getValue(type, 'dateAdded', details),
              timezone
            ),
          ],
          [
            'Previously Inspected On',
            this._toCustomerDate(
              this._getValue(type, 'previousInspectionDate', details),
              timezone
            ),
          ],
          ['Tracking ID', this._getValue(type, 'trackingId', details) || 'NA'],
        ];
        break;
      }
    }

    if (complianceSummary) {
      content.push([
        'Compliance Summary',
        {
          text: complianceSummary.map((summary, index) => ({
            text: `${summary.complianceStatus} ${summary.count} (${
              summary.percents
            })%${index !== complianceSummary.length - 1 ? '\n' : ''}`,
            color: this._getColorByComplianceLevel(summary.level),
          })),
        },
      ]);
    }
    return content.map(this._mapValues.bind(this));
  }

  private _getInspectionTableContent(
    type: EntityType,
    details: Details,
    hiddenFields: string[] = [],
    complianceStatusColor: string,
    timezone: string
  ) {
    let insType: string;
    switch (type) {
      case EntityTypes.ConstructionSiteInspection:
        insType = 'C-';
        break;
      case EntityTypes.StructureInspection:
        insType = 'S-';
        break;
      case EntityTypes.FacilityInspection:
        insType = 'F-';
        break;
      case EntityTypes.OutfallInspection:
        insType = 'O-';
    }
    const content = [
      ['Inspector', this._getInspector(type, details)],
      ['Inspection ID', `${insType}${this._getValue(type, 'id', details)}`],
      ['Inspection Type', this._getValue(type, 'inspectionType', details)],
      [
        'Inspection Date',
        this._toCustomerDate(
          this._getValue(type, 'inspectionDate', details),
          timezone
        ),
      ],
      [
        'Scheduled Inspection Date',
        this._toCustomerDate(
          this._getValue(type, 'scheduledInspectionDate', details),
          timezone
        ),
      ],
      [
        'Compliance Status',
        {
          text: this._getValue(type, 'complianceStatus', details),
          color: complianceStatusColor,
        },
      ],
      [
        'Time In',
        this._toCustomerTime(this._getValue(type, 'timeIn', details), timezone),
      ],
      [
        'Time Out',
        this._toCustomerTime(
          this._getValue(type, 'timeOut', details),
          timezone
        ),
      ],
      [
        'Follow Up Inspection Date',
        this._toCustomerDate(
          _.get(details, 'followUpInspection.scheduledInspectionDate'),
          timezone
        ),
      ],
    ].filter(c => !hiddenFields.includes(_.camelCase(c[0])));

    return content.map(this._mapValues.bind(this));
  }

  private _mapCustomFormFields = (formFields: any) => {
    return formFields
      .map(this._mapValues.bind(this))
      .map((val: any[]) => this._toKeyValueColumn(val))
      .reduce(this._setTableColumns(2), []);
  };

  private _addCustomTable = (formFields, isContinuation?: boolean) => {
    return {
      table: {
        body: [['', ''], ...this._mapCustomFormFields(formFields)],
        widths: ['*', '*'],
        headerRows: isContinuation ? 0 : 1,
      },
      layout: this._layout(isContinuation && true),
    };
  };

  private _getCustomFieldsContent(details: Details, timezone: string) {
    const { customFields } = details;
    const allDetails: any[] = [];

    let formFields: any[] = [];
    let isContinuation = true;

    customFields.forEach((field, i) => {
      if (field.sectionName && field.sectionName === field.fieldLabel) {
        if (formFields.length) {
          allDetails.push(this._addCustomTable(formFields, isContinuation));
          formFields = [];
        }
        // new Section means it is not continued from "Inspection Properties" section
        isContinuation = false;
        const spacer = { text: '', margin: [0, 8] };
        const header = { text: field.sectionName, style: 'subheader' };
        allDetails.push(spacer, header);
      } else {
        if (field.dataType === 'Date') {
          if ((<any>field).value === null || (<any>field).value === undefined) {
            formFields.push([field.fieldLabel, 'NA']);
          } else {
            const date = new Date((<any>field).value);
            formFields.push([
              field.fieldLabel,
              this._toCustomerDate(date, timezone) || 'NA',
            ]);
          }
        } else {
          let fieldValue = (<any>field).value || '';

          if (field.inputType === 'multi-select') {
            fieldValue = fieldValue.split('|').join('\n\n');
          }

          if (field.inputType === 'conditional-text') {
            const splittedValue = fieldValue.split('|');

            if (splittedValue[0] === 'true') {
              fieldValue = splittedValue[0];
            } else if (splittedValue[0] === 'false') {
              fieldValue = ['No', splittedValue[1], splittedValue[2]]
                .filter(Boolean)
                .join('\n\n');
            }
          }

          formFields.push([field.fieldLabel, fieldValue || 'NA']);
        }
      }
    });
    // Any formFields not made a table yet, added to last section
    if (formFields.length) {
      allDetails.push(this._addCustomTable(formFields, isContinuation));
    }

    return allDetails;
  }

  private _getWeatherTableContent(type: EntityType, details: Details) {
    const inspectionType = type + 'Inspection';
    const content = [
      [
        'Weather Condition',
        this._getValue(inspectionType, 'weatherCondition', details),
      ],
      [
        'Temperature (F)',
        this._getValue(inspectionType, 'weatherTemperatureF', details),
      ],
      [
        'Precipitation (in)',
        this._getValue(inspectionType, 'weatherPrecipitationIn', details),
      ],
      [
        'Precipitation Last 72 Hours (in)',
        this._getValue(inspectionType, 'weatherLast72', details),
      ],
      [
        'Precipitation Last 24 Hours (in)',
        this._getValue(inspectionType, 'weatherLast24', details),
      ],
    ];
    return content.map(this._mapValues.bind(this));
  }

  private _getContactsTableContent(details: Details) {
    const contacts = details.contacts || [];
    const content = contacts.map(contact => {
      return [
        contact.name || '',
        contact.contactType || '',
        contact.phone ? `${contact.phone} - Direct` : '',
        contact.mobilePhone ? `${contact.mobilePhone} - Mobile` : '',
      ];
    });

    return content;
  }

  private async _getPhotoContent(
    details: Details,
    customer: Customer,
    largeWidth?: boolean
  ) {
    const files = details.files || [];

    const photos = files.filter(
      f => f.mimeType && f.mimeType.indexOf('image') !== -1
    );

    const content: ({ image: string; width: number })[] = [];
    const IMAGE_WIDTH = largeWidth ? 260 : 160;

    for (let i = 0; i < photos.length; i++) {
      const url = `${this.env.FILE_STORAGE_PATH}/${customer.id}/${
        photos[i].fileName
      }`;
      const imageData = await this._toBase64(url, { width: IMAGE_WIDTH });

      if (imageData) {
        content.push({
          image: `data:${photos[i].mimeType};base64,${imageData}`,
          width: IMAGE_WIDTH,
        });
      }
    }

    return content;
  }

  private _getInspector(type: EntityType, details: Details): string {
    const inspectorId = this._getValue(type, 'inspectorId', details);
    if (inspectorId) {
      const selectedInspector = details.inspectors.find(
        inspector => inspector.id === inspectorId
      );
      return selectedInspector ? selectedInspector.fullName : null;
    }
    return null;
  }

  private _getAddress(type: EntityType, details: Details): string {
    const physicalAddress1 = this._getValue(type, 'physicalAddress1', details);
    const physicalAddress2 = this._getValue(type, 'physicalAddress2', details);
    const physicalCity = this._getValue(type, 'physicalCity', details);
    const physicalZip = this._getValue(type, 'physicalZip', details);
    const physicalState = this._getValue(type, 'physicalState', details);

    const fullAddress = [
      physicalAddress1,
      physicalAddress2,
      physicalCity,
      physicalZip,
      physicalState,
    ]
      .filter(x => !!x)
      .join(', ');

    return fullAddress;
  }

  private _getValue(
    type: EntityType | EntityInspectionType,
    accessor: string,
    details: Details,
    defaultValue = null
  ): any {
    const typeMap = new Map<EntityType | EntityInspectionType, string>([
      [EntityTypes.ConstructionSite, 'constructionSite'],
      [EntityTypes.Facility, 'facility'],
      [EntityTypes.Outfall, 'outfall'],
      [EntityTypes.Structure, 'structure'],
      [EntityTypes.ConstructionSiteInspection, 'constructionSiteInspection'],
      [EntityTypes.FacilityInspection, 'facilityInspection'],
      [EntityTypes.OutfallInspection, 'outfallInspection'],
      [EntityTypes.StructureInspection, 'structureInspection'],
    ]);

    if (!accessor) {
      return details[typeMap.get(type)] || defaultValue;
    }

    accessor = `${typeMap.get(type)}.${accessor}`;
    const value = _.get(details, accessor, defaultValue);
    return value;
  }

  private _mapValues(row: any[]) {
    const defaultValue = 'NA';
    const key = { text: `${row[0]}:   `, color: '#616161' };
    const value = row[1];
    if (value === null || (_.has(value, 'text') && value.text === null)) {
      return [key, defaultValue];
    }
    if (value === 'true') {
      return [key, 'Yes'];
    }
    if (value === 'false') {
      return [key, 'No'];
    }
    return [key, value];
  }

  private _toCustomerDate(value: Date, timezone: string): string {
    if (value === null || value === undefined) {
      return null;
    }
    const m = timezone ? moment(value).tz(timezone) : moment(value);
    return m.isValid() ? m.format('MM/DD/YYYY') : null;
  }

  private _toCustomerTime(value: Date, timezone: string): string {
    if (!value) {
      return null;
    }

    const m = timezone ? moment(value).tz(timezone) : moment(value);
    return m.isValid() ? m.format('h:mm A') : null;
  }

  private _toFranklinCustomerDateTime(
    inspectionDate: Date,
    timeIn: Date,
    timezone: string
  ): string {
    if (!inspectionDate) {
      return null;
    }

    const m = timezone
      ? moment(inspectionDate).tz(timezone)
      : moment(inspectionDate);
    const time = this._toCustomerTime(timeIn, timezone);

    return m.isValid() ? `${m.format('dddd M/D/YY')} ${time || ''}` : null;
  }

  private async _toBase64(url: string, sharpOptions?: { width: number }) {
    try {
      const bitmap = fs.readFileSync(url);

      if (sharpOptions) {
        const buffer = await sharp(new Buffer(bitmap))
          .resize(sharpOptions.width * 3)
          .toBuffer();
        return buffer.toString('base64');
      }

      return new Buffer(bitmap).toString('base64');
    } catch {
      return null;
    }
  }

  // Converts a flat array into an array with nested arrays of column length;
  // Empty strings are used to fill out the last nested array to make it column length.
  // Example for 2 columns: ['', '', '', '', ''...], => [['', ''], ['', ''], ['', '']....]
  // Example for 3 columns: ['', '', '', '', ''...], => [['', '', ''], ['', '', ''], ['', '', '']....]
  private _columns(memo: any, val: any, index: number, columns: number) {
    const { length } = memo;
    const rowPosition = index % columns;

    if (rowPosition === 0) {
      memo.push([val, ...Array(columns - 1).fill('')]);
    } else {
      memo[length - 1][rowPosition] = val;
    }

    return memo;
  }

  // Utility function for _columns
  private _setTableColumns(columns: number) {
    return (memo, val, index) => this._columns(memo, val, index, columns);
  }

  private _toKeyValueColumn(val: any[]) {
    const isLongOrConditionalText =
      typeof val[1] === 'string' &&
      (val[1].length >= TEXT_LENGTH_LIMIT ||
        val[1].includes('\n\nComments:\n\n'));

    if (isLongOrConditionalText) {
      return {
        text: [
          { text: `${val[0].text}\n`, color: val[0].color },
          { text: val[1] },
        ],
        margin: [0, 3, 0, 1],
      };
    }

    return {
      colSpan: 1,
      alignment: 'left',
      columns: [
        { text: val[0], width: 'auto', margin: [0, 3, 0, 1] },
        {
          text: val[1],
          width: '*',
          alignment: 'right',
          margin: [8, 3, 0, 1],
          noWrap: (val[1] || '').length < 10, // Don't wrap short text, difficult to read
        },
      ],
    };
  }

  private _getMimeType(url: string) {
    if (!url) {
      return null;
    }
    const valid = ['jpg', 'jpeg', 'png', 'gif'];
    const arr = url.split('.');
    const fileType = arr[arr.length - 1];
    return valid.includes(fileType) ? `image/${fileType}` : null;
  }

  private _getColorByComplianceLevel(level: number) {
    switch (level) {
      case 1:
        return '#4CAF50';

      case 2:
        return '#FBC02D';

      case 3:
        return '#D32F2F';

      default:
        return '#000';
    }
  }

  private _getComplianceStatusColor(
    statuses: ComplianceStatus[],
    type: EntityType,
    details: Details
  ) {
    const assetComplianceStatus = this._getValue(
      type,
      'complianceStatus',
      details
    );
    const inspectionComplianceStatus = this._getValue(
      `${type}Inspection`,
      'complianceStatus',
      details
    );

    const assetStatus = statuses.find(
      s => s.entityType === type && s.name === assetComplianceStatus
    );
    const inspectionStatus = statuses.find(
      s => s.entityType === type && s.name === inspectionComplianceStatus
    );

    return {
      assetStatusColor: this._getColorByComplianceLevel(
        assetStatus && assetStatus.level
      ),
      inspectionStatusColor: this._getColorByComplianceLevel(
        inspectionStatus && inspectionStatus.level
      ),
    };
  }
}
