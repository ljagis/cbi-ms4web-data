import * as fs from 'fs-extra';
import * as moment from 'moment';
import * as sharp from 'sharp';

import { IllicitDischarge, IllicitDischargeDetailsResult } from '../models';

import { Customer } from '../models';
import { DefaultPdfReport } from './default-pdf-report';
import { Env } from '../shared';
import { injectable } from 'inversify';

const TEXT_LENGTH_LIMIT = 70;

@injectable()
export class IllicitDischargeReport extends DefaultPdfReport {
  constructor() {
    super();
  }

  async generate(
    customer: Customer,
    details: IllicitDischargeDetailsResult,
    investigator: string
  ) {
    const docDefinition = {
      header: this.createHeader(customer),
      content: await this.createContent(customer, details, investigator),
      footer: this.createFooter.bind(this),
      styles: this.createStyles(),
      defaultStyle: this.defaultStyle,
      pageSize: 'A4',
      pageMargins: [40, 40, 40, 60],
    };

    return this.pdfPrinter.createPdfKitDocument(docDefinition);
  }

  private createHeader({ fullName }: Customer) {
    return this.defaultHeader(fullName);
  }

  private async createContent(
    customer: Customer,
    details: IllicitDischargeDetailsResult,
    investigator: string
  ) {
    const stack = [];

    const spacing = { text: '', margin: [0, 8] };

    // Banner
    const customerBanner = await this.createCustomerBanner(customer, details);
    stack.push([customerBanner, spacing]);

    // Illicit Discharge Details
    const detailsHeader = {
      text: 'Illicit Discharge Details',
      style: 'subheader',
    };
    const detailsTableContent = this.getDetailsTableContent(details);
    const detailsTable = this.createTable(detailsTableContent);
    stack.push([detailsHeader, detailsTable, spacing]);

    // Discharge Description
    const descriptionHeader = {
      text: 'Discharge Description',
      style: 'subheader',
    };
    const { dischargeDescription } = details.illicitDischarge;
    const description = this.createSingle(dischargeDescription);
    stack.push(descriptionHeader, description, spacing);

    // Inspection Properties
    const inspectionHeader = {
      text: 'Inspection Properties',
      style: 'subheader',
    };
    const inspectionTableContent = this.getInspectionTableContent(
      details,
      investigator
    );
    const inspectionTable = this.createTable(inspectionTableContent);
    const customFields = this.getCustomFields(details);
    stack.push([inspectionHeader, inspectionTable, customFields, spacing]);

    // Additional Information
    const additionalInfoHeader = {
      text: 'Additional Information',
      style: 'subheader',
    };
    const { additionalInformation } = details.illicitDischarge;
    const additionalInfoTable = this.createSingle(
      additionalInformation || 'No additional information recorded'
    );
    stack.push(additionalInfoHeader, additionalInfoTable, spacing);

    // Photos
    const photosHeader = {
      text: 'Photos',
      style: 'subheader',
    };
    const photos = await this.getPhotos(customer, details);
    const photoTable = this.createTable(photos, { numColumns: 3 });
    stack.push([photosHeader, photoTable]);

    return stack;
  }

  private createFooter(currentPage: number, pageCount: number) {
    return {
      columns: [
        {
          text: `${currentPage} of ${pageCount}`,
          alignment: 'right',
          margin: [0, 20, 40, 0],
        },
      ],
    };
  }

  private createTable(
    content: any[],
    { numColumns = 2, isContinuation = false } = {}
  ) {
    const longTextField = content.some(
      c => (c.columns && c.columns[1].text || '').length >= TEXT_LENGTH_LIMIT
    );

    if (longTextField) {
      numColumns = 1;
    }

    const mappedContent: any = content.reduce(this.columnsReducer(numColumns), [
      Array(numColumns).fill(''),
    ]);

    return {
      table: {
        body: [Array(numColumns).fill(''), ...mappedContent],
        dontBreakRows: true,
        headerRows: isContinuation ? 0 : 1,
        widths: Array(numColumns).fill('*'),
      },
      layout: this.createLayout(),
    };
  }

  private createSingle(content: any) {
    return {
      table: {
        body: [[''], [content]],
        widths: ['*'],
        headerRows: 1,
      },
      layout: this.createLayout(),
    };
  }

  private createLayout(isContinuation?: boolean) {
    return {
      hLineWidth: (i: number, node: any) => {
        if (i === 0 || i === node.table.body.length) {
          return 0;
        }
        return i === node.table.headerRows ? 2 : 1;
      },
      vLineWidth: () => 0,
      hLineColor: (i: number) => {
        return i !== 1 || isContinuation ? '#aaa' : 'black';
      },
      paddingLeft: (i: number) => {
        return i === 0 ? 0 : 8;
      },
      paddingRight: (i: number, node: any) => {
        return i === node.table.widths.length - 1 ? 0 : 8;
      },
      paddingTop: (i: number) => {
        return i === 0 ? 2 : 5;
      },
      paddingBottom: (i: number) => {
        return i === 0 ? 0 : 5;
      },
    };
  }

  private createStyles() {
    return {
      ...this.styles,
      bannerHeader: {
        fontSize: 19,
        bold: true,
      },
      bannerSubheader: {
        fontSize: 13,
      },
    };
  }

  private async createCustomerBanner(
    customer: Customer,
    { illicitDischarge }: IllicitDischargeDetailsResult
  ) {
    const stack = [];
    stack.push({
      text: [illicitDischarge.name, this.getFullAddress(illicitDischarge)]
        .filter(Boolean)
        .join(' - '),
      style: 'bannerHeader',
      margin: [0, 3],
    });
    stack.push({
      text: illicitDischarge.inspectionType,
      style: 'bannerSubheader',
      margin: [0, 3],
    });
    stack.push({
      text: customer.fullName,
      style: 'bannerSubheader',
      margin: [0, 3],
    });

    if (!customer.logoFileName) {
      return { stack };
    }

    return {
      table: {
        body: [['', ''], [await this.getCustomerLogo(customer), { stack }]],
        widths: ['auto', '*'],
      },
      layout: 'noBorders',
    };
  }

  private async getCustomerLogo({
    id: customerId,
    logoThumbnailName,
  }: Customer) {
    if (!logoThumbnailName) {
      return;
    }

    const { FILE_STORAGE_PATH } = process.env as Env;

    const url = `${FILE_STORAGE_PATH}/${customerId}/${logoThumbnailName}`;
    const mimeType = this.getMimeType(url);
    const imageData = await this.getImageData(url);

    if (!imageData) {
      return null;
    }

    return {
      image: `data:${mimeType};base64,${imageData}`,
      fit: [80, 80],
      margin: [0, 0, 20, 10],
    };
  }

  private async getPhotos(
    { id: customerId }: Customer,
    { files }: IllicitDischargeDetailsResult
  ) {
    const photos = files.filter(
      f => f.mimeType && f.mimeType.indexOf('image') !== -1
    );

    const { FILE_STORAGE_PATH } = process.env as Env;
    const content: ({ image: string; width: number })[] = [];
    const IMAGE_WIDTH = 160;

    for (let i = 0; i < photos.length; i++) {
      const url = `${FILE_STORAGE_PATH}/${customerId}/${photos[i].fileName}`;
      const imageData = await this.getImageData(url, { width: IMAGE_WIDTH });

      if (imageData) {
        content.push({
          image: `data:${photos[i].mimeType};base64,${imageData}`,
          width: IMAGE_WIDTH,
        });
      }
    }

    return content;
  }

  private getDetailsTableContent({
    illicitDischarge,
  }: IllicitDischargeDetailsResult) {
    const fullAddress = this.getFullAddress(illicitDischarge);
    const siteName = [illicitDischarge.name, fullAddress]
      .filter(Boolean)
      .join(' - ');

    const content = [
      ['Name', siteName],
      ['Compliance', illicitDischarge.complianceStatus],
      ['Reported', this.getUTCDate(illicitDischarge.dateReported)],
      ['Site Physical Address', fullAddress],
    ];

    // TODO: These can probably be simplified into a single mapper
    return content
      .map(this.valuesMapper.bind(this))
      .map(this.contentMapper.bind(this));
  }

  private getInspectionTableContent(
    { illicitDischarge }: IllicitDischargeDetailsResult,
    investigator: string
  ) {
    const content = [
      ['Investigator', investigator],
      ['Inspection ID', illicitDischarge.id.toString()],
      ['Inspection Type', illicitDischarge.inspectionType],
      ['Compliance Status', illicitDischarge.complianceStatus],
      [
        'Follow Up Inspection Date',
        this.getUTCDate(illicitDischarge.followUpDate),
      ],
    ];

    // TODO: These can probably be simplified into a single mapper
    return content
      .map(this.valuesMapper.bind(this))
      .map(this.contentMapper.bind(this));
  }

  private getCustomFields(details: any) {
    const { customFields } = details;
    const allDetails: any[] = [];
    let formFields: any[] = [];
    let isContinuation = true;

    customFields.map((field, i) => {
      if (field.sectionName && field.sectionName === field.fieldLabel) {
        if (formFields.length) {
          const mappedFields = formFields
            .map(this.valuesMapper.bind(this))
            .map(this.contentMapper.bind(this));
          allDetails.push(this.createTable(mappedFields, { isContinuation }));
          formFields = [];
        }
        // new Section means it is not continued from "Inspection Properties" section
        isContinuation = false;
        const spacer = { text: '', margin: [0, 8] };
        const header = { text: field.sectionName, style: 'subheader' };
        allDetails.push(spacer, header);
      } else {
        if (field.dataType === 'Date') {
          if ((<any>field).value === null || (<any>field).value === undefined) {
            formFields.push([field.fieldLabel, 'NA']);
          } else {
            const date = new Date((<any>field).value);
            formFields.push([field.fieldLabel, this.getUTCDate(date) || 'NA']);
          }
        } else {
          let fieldValue = (<any>field).value || '';

          if (field.inputType === 'multi-select') {
            fieldValue = fieldValue.split('|').join('\n\n');
          }

          if (field.inputType === 'conditional-text') {
            const splittedValue = fieldValue.split('|');

            if (splittedValue[0] === 'true') {
              fieldValue = splittedValue[0];
            } else if (splittedValue[0] === 'false') {
              fieldValue = ['No', splittedValue[1], splittedValue[2]]
                .filter(Boolean)
                .join('\n\n');
            }
          }

          formFields.push([field.fieldLabel, fieldValue || 'NA']);
        }
      }
    });
    // Any formFields not made a table yet, added to last section
    if (formFields.length) {
      const mappedFields = formFields
        .map(this.valuesMapper.bind(this))
        .map(this.contentMapper.bind(this));
      allDetails.push(this.createTable(mappedFields, { isContinuation }));
    }
    return allDetails;
  }

  private getFullAddress(illicitDischarge: IllicitDischarge) {
    const fullAddress = [
      illicitDischarge.physicalAddress1,
      illicitDischarge.physicalAddress2,
      illicitDischarge.physicalCity,
      illicitDischarge.physicalZip,
      illicitDischarge.physicalState,
    ];

    return fullAddress.filter(part => !!part).join(', ');
  }

  private getMimeType(url: string) {
    const validTypes = ['jpg', 'jpeg', 'png', 'gif'];
    const fileType = url.split('.').pop();
    return validTypes.includes(fileType) ? `image/${fileType}` : null;
  }

  private async getImageData(url: string, sharpOptions?: { width: number }) {
    try {
      const bitmap = fs.readFileSync(url);

      if (sharpOptions) {
        const buffer = await sharp(new Buffer(bitmap))
          .resize(sharpOptions.width * 3)
          .toBuffer();
        return buffer.toString('base64');
      }

      return new Buffer(bitmap).toString('base64');
    } catch {
      return null;
    }
  }

  private getUTCDate(date: Date): string {
    const utc = moment.utc(date);
    return utc.isValid() ? utc.format('MM/DD/YYYY') : null;
  }

  private valuesMapper(row: any[]) {
    const key = { text: `${row[0]}:   `, color: '#616161' };
    const value = row[1];
    if (value === null) {
      return [key, 'NA'];
    }
    if (value === 'true') {
      return [key, 'Yes'];
    }
    if (value === 'false') {
      return [key, 'No'];
    }
    return [key, value];
  }

  // Map key-value pairs to docDefinition
  private contentMapper(value: any[]) {
    return {
      alignment: 'left',
      columns: [
        { text: value[0], width: 'auto', margin: [0, 3, 0, 1] },
        {
          text: value[1],
          width: '*',
          alignment: 'right',
          margin: [8, 3, 0, 1],
          noWrap: (value[1] || '').length < 10,
        },
      ],
    };
  }

  /**
   * Converts a 2D array to a multi-dimensional array
   * Example with 2 columns: ['', '', '', '',...], => [['', ''], ['', ''],....]
   */
  private createColumns(
    acc: any,
    cur: any,
    idx: number,
    arr: any[],
    numColumns: number
  ) {
    const row = idx % numColumns;

    acc[acc.length - 1][row] = cur;

    if (idx !== arr.length - 1 && row === numColumns - 1) {
      const newRow = Array(numColumns)
        .fill(cur, 0, 1)
        .fill('', 1);
      acc.push(newRow);
    }

    return acc;
  }

  private columnsReducer(numColumns: number) {
    return (acc, cur, idx, arr) =>
      this.createColumns(acc, cur, idx, arr, numColumns);
  }
}
