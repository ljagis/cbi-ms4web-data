import * as moment from 'moment';

import { BmpActivityLog, Customer } from '../models';
import {
  BmpActivityLogRepository,
  BmpControlMeasureRepository,
  BmpDetailRepository,
} from '../data';
import { each, groupBy, sortBy, sumBy } from 'lodash';

import { DefaultPdfReport } from './default-pdf-report';
import { Env } from '../shared';
import { injectable } from 'inversify';
import { BmpActivitySummaryReportOptions } from './report-options';

/**
 * BMP Activity Summary report - See bottom for layout example
 */
@injectable()
export class BmpActivitySummaryReport {
  env = process.env as Env;

  constructor(
    private _actLogRepo: BmpActivityLogRepository,
    private _mcmRepo: BmpControlMeasureRepository,
    private _bmpDetailRepo: BmpDetailRepository,
    private _defaultRpt: DefaultPdfReport
  ) {}

  async generate(customer: Customer, options: BmpActivitySummaryReportOptions) {
    const activityLogRows = await this.generateActivityLogRows(
      customer,
      options
    );
    // 8/15/2019 date format
    const fromFormatted = moment(options.activityFrom).format('l');
    const toFormatted = moment(options.activityTo).format('l');
    const content: any[] = [
      { text: 'BMP Activity Summary Report', style: 'header' },
      { text: `From ${fromFormatted} to ${toFormatted}` },
    ];
    const reportTableWidths = [70, '*', 130, 70]; // [date, comments, data type, totals]
    const reportContent = generateTableWithHeaderContent(reportTableWidths);
    each(activityLogRows, activityLogRow => {
      let columnData: any[] = [];
      switch (activityLogRow._rowType) {
        case 'mcm': {
          columnData = [
            {
              text: activityLogRow.name,
              colSpan: 4,
              style: 'mcm',
              decoration: 'underline',
            },
          ];
          break;
        }
        case 'bmp': {
          columnData = [
            {
              text: activityLogRow.name,
              colSpan: 4,
              style: 'bmp',
              decoration: 'underline',
              margin: [10, 0, 0, 0],
            },
          ];
          break;
        }
        case 'log': {
          const dataTypeFormatted =
            activityLogRow.dataType === 'null'
              ? 'N/A'
              : activityLogRow.dataType;
          columnData = [
            {
              text: moment(activityLogRow.dateAdded).format('l'),
              margin: [10, 0, 0, 0],
            },
            {
              text: activityLogRow.comments,
              style: 'comment',
            },
            {
              text: dataTypeFormatted,
            },
            {
              text: this._defaultRpt.numberFormat.format(
                activityLogRow.quantity
              ),
              style: 'subtotal',
              alignment: 'right',
            },
          ];
          break;
        }
        case 'total': {
          columnData = [
            {},
            {},
            {
              text: 'Total',
              style: 'total',
              alignment: 'right',
            },
            {
              text: this._defaultRpt.numberFormat.format(activityLogRow.total),
              style: 'total',
              alignment: 'right',
              border: [false, true, false, false], // left, top, right, bottom
            },
          ];
          break;
        }
      }
      reportContent.table.body.push(columnData);
    });
    content.push(reportContent);

    const styles = {
      ...this._defaultRpt.styles,
      mcm: {
        fontSize: 12,
        bold: true,
      },
      bmp: {
        fontSize: 11,
        bold: true,
      },
      subtotal: {
        // italics: true,
      },
      total: {
        bold: true,
      },
      comment: {
        fontSize: 9,
        italics: true,
      },
    };

    const docDefinition = {
      header: this._defaultRpt.defaultHeader(customer.fullName),
      content,
      footer: this._defaultRpt.defaultFooter(),
      styles,
      defaultStyle: this._defaultRpt.defaultStyle,
    };

    // pdfDoc is a stream so you can pipe it to the file system
    const pdfDoc = this._defaultRpt.pdfPrinter.createPdfKitDocument(
      docDefinition
    );
    return pdfDoc;
  }

  async generateActivityLogRows(
    customer: Customer,
    options: BmpActivitySummaryReportOptions
  ) {
    const customerId = customer.id;
    const mcms = await this._mcmRepo.fetch(customerId);
    const sortedMcms = sortBy(mcms, c => c.sort, c => c.name);
    const bmpDetails = await this._bmpDetailRepo.fetch(customerId);
    const bmpDetailsByMcmId = groupBy(bmpDetails, d => d.controlMeasureId);
    // convert request dates to UTC on each specific day to maintain daylight savings modifier
    const queryFrom = moment(options.activityFrom).toISOString();
    const queryTo = moment(options.activityTo).toISOString();
    const bmpActivityLogs = await this._actLogRepo.fetch(customerId, {
      activityFrom: queryFrom,
      activityTo: queryTo,
    });
    const bmpActivityLogsSorted = sortBy(
      bmpActivityLogs,
      act => act.activityDate
    );
    const logsByBmpDetailId = groupBy(
      bmpActivityLogsSorted,
      l => l.bmpDetailId
    );
    const activityLogRows: (ActivityLogRow | SimpleRow)[] = [];

    each(sortedMcms, mcm => {
      const detailsForMcm = bmpDetailsByMcmId[mcm.id];
      if (!detailsForMcm) {
        // check if detail exist
        return;
      }

      // skip mcm if log doesnt exist
      let hasLogs = false;
      each(detailsForMcm, detail => {
        if (
          logsByBmpDetailId[detail.id] &&
          logsByBmpDetailId[detail.id].length > 0
        ) {
          hasLogs = true;
          return;
        }
      });
      if (!hasLogs) {
        return;
      }

      // add mcm row
      activityLogRows.push({
        _rowType: 'mcm',
        name: mcm.name,
      });

      // loop through each bmpDetails
      each(detailsForMcm, bmpDetail => {
        const logsForDetails = logsByBmpDetailId[bmpDetail.id];
        if (!logsForDetails || !logsForDetails.length) {
          // check if exists
          return;
        }
        // add bmp detail row
        activityLogRows.push({
          _rowType: 'bmp',
          name: bmpDetail.name,
        });

        // group by dataType
        const logsByType = groupBy(logsForDetails, l => l.dataType);

        each(logsByType, (logs, dataType) => {
          // list by the data type
          each(logs, log => {
            activityLogRows.push({
              _rowType: 'log',
              mcmName: mcm.name,
              bmpName: bmpDetail.name,
              dateAdded: log.activityDate,
              comments: log.comments,
              dataType: dataType,
              quantity: log.quantity,
            });
          });

          // add total at the end
          const quantityTotal = sumBy(logs, l => l.quantity);
          if (quantityTotal) {
            activityLogRows.push({
              _rowType: 'total',
              total: quantityTotal,
            });
          }
        });
      });
    });

    return activityLogRows;
  }
}

/** PRIVATES */

/**
 * Generates the table headers: date | comments | data types | quantity
 * @param reportTableWidths
 */
function generateTableWithHeaderContent(
  reportTableWidths: (string | number)[]
): any {
  return {
    table: {
      widths: reportTableWidths,
      body: [
        [
          {
            text: 'Date',
            decoration: 'underline',
            style: 'normal',
          },
          {
            text: 'Comments',
            decoration: 'underline',
            style: 'normal',
          },
          { text: 'Data Types', decoration: 'underline', style: 'normal' },
          {
            text: 'Quantities',
            decoration: 'underline',
            style: 'normal',
            alignment: 'right',
          },
        ],
      ],
    },
    layout: {
      defaultBorder: false,
    },
  };
}

/**
 * generates individual log rows
 * @param bmpDetail
 * @param logs
 * @param dataType
 */
// function generateLogContents(
//   dataType: string,
//   logs: BmpActivityLog[],
//   bmpDetail: BmpDetail,
//   numberFormat: Intl.NumberFormat
// ) {
//   const dataTypeFormatted = dataType === 'null' ? 'N/A' : dataType;
//   const sum = sumBy(logs, l => l.quantity);
//   const sumFormatted = sum ? numberFormat.format(sum) : sum;
//   const columns = [
//     { text: bmpDetail.name, style: 'normal', margin: [10, 0, 0, 0] },
//     { text: dataTypeFormatted, style: 'normal' },
//     { text: sumFormatted, style: 'normal', alignment: 'right' },
//   ];
//   const commentRows = logs.filter(log => log.comments).map(log => {
//     const commentColumns = [
//       {
//         text: log.comments,
//         style: 'logComment',
//         margin: [20, 0, 0, 0],
//         colSpan: 3,
//       },
//       {}, // col
//       {},
//     ];
//     return commentColumns;
//   });
//   const rows: any[] = [columns];
//   if (commentRows.length) {
//     rows.push(...commentRows);
//   }
//   return rows;
// }

interface SimpleRow {
  _rowType: 'mcm' | 'bmp' | 'total';
  name?: string;
  total?: number;
}

interface ActivityLogRow
  extends Pick<
      BmpActivityLog,
      'dateAdded' | 'dataType' | 'quantity' | 'comments'
    > {
  _rowType: 'log';
  mcmName: string;
  bmpName: string;
}

/**
 *
Example report layout:

BMP Activity Summary Report
From 9/17/2017 to 10/17/2018

                          Data Type           Total
MCM A
----------------------------------------------------
  Bmp Detail Title A        Miles driven              10
  Bmp Detail Title A        Bags collected            4

MCM B
----------------------------------------------------
  Bmp Detail Title B        Miles driven              10
  Bmp Detail Title B      Bags collected            4

 */
