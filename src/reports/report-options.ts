export interface InspectionSummaryReportOptions {
  from: string;
  to: string;
  entityType: string;
}

export interface BmpActivitySummaryReportOptions {
  activityFrom: string;
  activityTo: string;
}
