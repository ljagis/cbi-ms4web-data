import {
  AnnualReport,
  AnnualReportData,
  BMP,
  Customer,
  MG,
  SWMP,
} from '../models';
import { AnnualReportRepository, SwmpRepository } from '../data';

import { DefaultPdfReport } from './default-pdf-report';
import { injectable } from 'inversify';

import moment = require('moment');

/**
 * Swmp Annual Report
 */
@injectable()
export class SwmpAnnualReport {
  constructor(
    private _defaultRpt: DefaultPdfReport,
    private _annualReportRepo: AnnualReportRepository,
    private _swmpRepo: SwmpRepository
  ) {}

  async generate(customer: Customer, id: number) {
    const content: any[] = [];
    const annualReport: AnnualReport = await this._annualReportRepo.fetchByIdAndCustomer(
      id,
      customer.id
    );
    const report: AnnualReportData = JSON.parse(annualReport.report);

    content.push(
      await this.generateGeneralInfo(annualReport, customer, report)
    );

    [
      'generateComplianceStatus',
      'generateDataSummary',
      'generateImpairedWaterBodes',
      'generateStormwaterActivities',
      'generateSwmpModifications',
      'generateAdditionalBmps',
      'generateAdditionalInformation',
      'generateConstructionActivities',
      'generateCertification',
    ].forEach(functionName => {
      content.push(this[functionName](report));
    });

    const styles = {
      header: {
        fontSize: 16,
        bold: true,
      },
      title: {
        fontSize: 12,
        bold: true,
      },
      subTitle: {
        fontSize: 11,
      },
      tableHeader: {
        bold: true,
        alignment: 'center',
        fillColor: '#F2F2F2',
      },
      question: {
        margin: [0, 20, 0, 0],
      },
      answer: {
        bold: true,
        // horizontally align with numbered question text
        margin: [14, 10, 0, 0],
      },
      marginVertSm: {
        margin: [0, 10, 0, 0],
      },
      marginVertMd: {
        margin: [0, 20, 0, 0],
      },
      marginVertLg: {
        margin: [0, 30, 0, 0],
      },
    };

    const defaultStyle = {
      fontSize: 10,
      lineHeight: 1.4,
    };

    const docDefinition = {
      pageSize: 'LETTER',
      pageOrientation: 'portrait',
      pageMargins: [40, 40, 40, 60],
      content,
      footer: currentPage => ({
        color: '#999999',
        margin: [40, 20, 40, 0],
        columns: [
          { text: 'TCEQ-20561 (Rev May 2019)' },
          { text: `Page ${currentPage}`, alignment: 'right' },
        ],
      }),
      styles,
      defaultStyle,
    };

    // pdfDoc is a stream so you can pipe it to the file system
    const pdfDoc = this._defaultRpt.pdfPrinter.createPdfKitDocument(
      docDefinition
    );

    return pdfDoc;
  }

  async generateGeneralInfo(
    annualReport: AnnualReport,
    customer: Customer,
    report: AnnualReportData
  ) {
    const swmp: SWMP = await this._swmpRepo.fetchByIdAndCustomer(
      annualReport.swmpId,
      customer.id
    );

    const beginReportDate = swmp.annualReportingYear
      ? `${moment(swmp.annualReportingYear).format('MM/DD')}/${
          annualReport.year
        }`
      : `01/01/${annualReport.year}`;
    const endReportDate = moment(beginReportDate, 'MM/DD/YYYY')
      .add(1, 'year')
      .subtract(1, 'day')
      .format('MM/DD/YYYY');

    return [
      {
        text: 'Phase II (Small) MS4 Annual Report Form',
        style: 'header',
        alignment: 'center',
      },
      {
        text: `TPDES General Permit Number ${swmp.number}`,
        style: ['title'],
        margin: [0, 5, 0, 0],
        alignment: 'center',
      },
      {
        text: 'A. General Information',
        style: ['title', 'marginVertLg'],
      },
      {
        style: 'marginVertMd',
        ...this.generateSingleLineAnswer('Authorization Number:', swmp.number),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Reporting year (year will be either 1, 2, 3, 4, or 5):',
          `${annualReport.year ? annualReport.year - swmp.year + 1 : ''}`
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Annual Reporting Year Option Selected by MS4 (calendar, permit, or fiscal):',
          `${swmp.annualReportingYear ? 'Fiscal' : 'Calendar'}`
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Last day of fiscal year, if applicable:',
          `${
            swmp.annualReportingYear
              ? moment(swmp.annualReportingYear).format('MM/DD')
              : 'NA'
          }`
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Reporting period beginning date (month/date/year):',
          beginReportDate
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Reporting period end date (month/date/year):',
          endReportDate
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'MS4 Operator Level:',
          swmp.operatorLevel
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer('Name of MS4:', swmp.name),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Contact Name:',
          annualReport.contactName || swmp.contactName
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Telephone Number:',
          annualReport.phone || swmp.phone
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Mailing Address:',
          annualReport.mailingAddress || swmp.mailingAddress
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Email Address:',
          annualReport.emailAddress || swmp.emailAddress
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'A copy of the annual report was submitted to the TCEQ Region:',
          this.getYesNoString(report.general.isCopyReportSubmitted)
        ),
      },
      {
        style: 'marginVertSm',
        ...this.generateSingleLineAnswer(
          'Region the annual report was submitted to:',
          report.general.reportSubmittedRegion
        ),
      },
    ];
  }

  generateComplianceStatus(report: AnnualReportData) {
    const { complianceStatus } = report;
    const content: any[] = [
      {
        text: 'B. Status of Compliance with the MS4 GP and SWMP',
        style: ['title', 'marginVertLg'],
      },
    ];

    const table1 = this.generateTable(
      ['40%', 50, 50, '*'],
      ['', 'Yes', 'No', 'Explain']
    );

    content.push(
      {
        ...this.generateNumberedLine(
          1,
          'Provide information on the status of complying with permit conditions: (TXR040000 Part IV.B.2)'
        ),
        style: 'question',
      },
      table1
    );

    const permitConditions = {
      submittedSwmp:
        'Permittee is currently in compliance with the SWMP as submitted to and approved by the TCEQ.',
      recordKeepingRequirements:
        'Permittee is currently in compliance with recordkeeping and reporting requirements.',
      eligibilityRequirements:
        'Permittee meets the eligibility requirements of the permit (e.g., TMDL requirements, Edwards ' +
        'Aquifer limitations, compliance history, etc.).',
      conductedAnnualReview:
        'Permittee conducted an annual review of its SWMP in conjunction with preparation of the annual report',
    };

    Object.keys(permitConditions).forEach(key => {
      const row = [permitConditions[key]];

      if (complianceStatus[key].value === true) {
        row.push({ text: 'Yes', alignment: 'center' }, '');
      } else if (complianceStatus[key].value === false) {
        row.push('', { text: 'No', alignment: 'center' });
      } else {
        row.push('', '');
      }

      row.push(complianceStatus[key].description);
      table1.table.body.push(row);
    });

    const table2 = this.generateTable(
      [40, '*', '*'],
      [
        'MCM',
        'BMP',
        'BMP is appropriate for reducing the discharge of pollutants in stormwater (Answer Yes or No and explain)',
      ]
    );

    content.push(
      {
        ...this.generateNumberedLine(2, [
          'Provide a general assessment of the appropriateness of the selected BMPs. You may use the table below to meet this requirement (',
          { text: 'see Example 1 in instructions', bold: true },
          '):',
        ]),
        style: 'question',
      },
      table2
    );

    complianceStatus.generalAssessments.forEach(g => {
      table2.table.body.push([
        (g.bmp.minimumControlMeasures as string[]).join(', '),
        g.bmp.title,
        g.description,
      ]);
    });

    const table3 = this.generateTable(
      [40, 100, 70, 50, 50, '*'],
      [
        'MCM',
        'BMP',
        'Information Used',
        'Quantity',
        'Units',
        'Does the BMP Demonstrate a Direct Reduction in Pollutants? (Answer Yes or No and explain)',
      ]
    );

    content.push(
      {
        ...this.generateNumberedLine(3, [
          // tslint:disable-next-line: max-line-length
          'Describe progress towards reducing the discharge of pollutants to the maximum extent practicable. Summarize any information used (such as visual observation, amount of materials removed or prevented from entering the MS4, or, if required, monitoring data, etc.) to evaluate reductions in the discharge of pollutants. You may use the table below to meet this requirement (',
          { text: 'see Example 2 in instructions', bold: true },
          '):',
        ]),
        style: 'question',
      },
      table3
    );

    complianceStatus.progressReducingPollutants.forEach(p => {
      table3.table.body.push([
        (p.mg.bmp.minimumControlMeasures as string[]).join(', '),
        p.mg.title,
        p.informationUsed,
        p.quantity,
        p.units,
        p.description,
      ]);
    });

    const table4 = this.generateTable(
      [40, 100, '*'],
      [
        'MCM',
        'Measurable Goals',
        'Explain progress toward goal or how goal was archieved. If goal was not accomplished, please explain.',
      ]
    );

    content.push(
      {
        ...this.generateNumberedLine(4, [
          // tslint:disable-next-line: max-line-length
          'Provide the measurable goals for each of the MCMs, and an evaluation of the success of the implementation of the measurable goals (',
          { text: 'see Example 3 in instructions', bold: true },
          '):',
        ]),
        style: 'question',
      },
      table4
    );

    complianceStatus.successImplementations.forEach(s => {
      table4.table.body.push([
        (s.mg.bmp.minimumControlMeasures as string[]).join(', '),
        s.mg.title,
        s.description,
      ]);
    });

    return content;
  }

  generateDataSummary({ dataSummary }: AnnualReportData) {
    return [
      {
        text: 'C. Stormwater Data Summary',
        style: ['title', 'marginVertLg'],
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'Provide a summary of all information used, including any lab results (if sampling was conducted) to assess the success of the SWMP at reducing the discharge of pollutants to the MEP. For example, did the MS4 conduct visual inspections, clean the inlets, look for illicit discharge, clean streets, look for flow during dry weather, etc.?',
        style: 'question',
      },
      {
        text: dataSummary.description,
        style: 'answer',
      },
    ];
  }

  generateImpairedWaterBodes({ impairedWaterBodes }: AnnualReportData) {
    const content: any[] = [
      {
        text: 'D. Impaired Waterbodies',
        style: ['title', 'marginVertLg'],
      },
      {
        ...this.generateNumberedLine(
          1,
          // tslint:disable-next-line: max-line-length
          'Identify whether an impaired water within the permitted area was added to the latest EPA-approved 303(d) list or the Texas Integrated Report of Surface Water Quality for CWA Sections 305(b) and 303(d). List any newly-identified impaired waters below by including the name of the water body and the cause of impairment.'
        ),
        style: 'question',
      },
      {
        text: impairedWaterBodes.identifyImpairedWater,
        style: 'answer',
      },
      {
        ...this.generateNumberedLine(
          2,
          // tslint:disable-next-line: max-line-length
          'If applicable, explain below any activities taken to address the discharge to impaired waterbodies, including any sampling results and a summary of the small MS4’s BMPs used to address the pollutant of concern.'
        ),
        style: 'question',
      },
      {
        text: impairedWaterBodes.activitiesToDischarge,
        style: 'answer',
      },
      {
        ...this.generateNumberedLine(
          3,
          'Describe the implementation of targeted controls if the small MS4 discharges to an impaired water body with an approved TMDL.'
        ),
        style: 'question',
      },
      {
        text: impairedWaterBodes.implementationOfTargetedControls,
        style: 'answer',
      },
    ];

    content.push(
      {
        ...this.generateNumberedLine(
          4,
          'Report the benchmark identified by the MS4 and assessment activities:'
        ),
        style: 'question',
      },
      // 8
      this.generateTable(
        [70, 70, '*', 70],
        [
          'Benchmark Parameter (Ex: Total Suspended Solids)',
          'Benchmark Value',
          'Description of additional sampling or other assessment activities',
          'Year(s) conducted',
        ]
      )
    );

    impairedWaterBodes.identifiedBenchmarks.forEach(b => {
      content[8].table.body.push([
        b.benchmarkParameter,
        b.benchmarkValue,
        b.description,
        b.yearsConducted,
      ]);
    });

    content.push(
      {
        ...this.generateNumberedLine(
          5,
          'Provide an analysis of how the selected BMPs will be effective in contributing to achieving the benchmark:'
        ),
        style: 'question',
      },
      // 10
      this.generateTable(
        ['*', '*', '*'],
        [
          'Benchmark Parameter',
          'Selected BMP',
          'Contributing to archieving Benchmark',
        ]
      )
    );

    impairedWaterBodes.archievingBenchmarkAnalyses.forEach(b => {
      content[10].table.body.push([
        b.benchmarkParameter,
        b.bmp.title,
        b.description,
      ]);
    });

    content.push(
      {
        ...this.generateNumberedLine(
          6,
          'If applicable, report on focused BMPs to address impairment for bacteria:'
        ),
        style: 'question',
      },
      // 12
      this.generateTable(
        ['auto', '*'],
        ['Description of bacteria-focused BMP', 'Comments/Discussion']
      )
    );

    impairedWaterBodes.impairmentBacteriaReports.forEach(b => {
      content[12].table.body.push([b.bmp.title, b.description]);
    });

    content.push(
      {
        ...this.generateNumberedLine(
          7,
          'Assess the progress to determine BMP’s effectiveness in achieving the benchmark.'
        ),
        style: 'question',
      },
      {
        text:
          'For example, the MS4 may use the following benchmark indicators:',
        margin: [11, 2, 0, 0],
      },
      {
        margin: [22, 2, 0, 0],
        ul: [
          'number of sources identified or eliminated;',
          'number of illegal dumpings;',
          'increase in illegal dumping reported;',
          'number of educational opportunities conducted;',
          'reductions in sanitary sewer flows (SSOs);  /or',
          'increase in illegal discharge detection through dry screening.',
        ],
      },
      // 16
      this.generateTable(
        [150, '*'],
        ['Benchmark Indicator', 'Description/Comments']
      )
    );

    impairedWaterBodes.bmpEffectivenessProgresses.forEach(p => {
      content[16].table.body.push([p.benchmarkParameter, p.description]);
    });

    return content;
  }

  generateStormwaterActivities({ stormwaterActivities }: AnnualReportData) {
    const content: any[] = [
      {
        text: 'E. Stormwater Activities',
        style: ['title', 'marginVertLg'],
      },
      {
        text: 'Describe activities planned for the next reporting year:',
        style: 'question',
      },
      // 2
      this.generateTable(
        [70, 80, 90, '*'],
        ['MCM(s)', 'BMP', 'Stormwater Activity', 'Description/Comments']
      ),
    ];

    stormwaterActivities.activities.forEach(a => {
      content[2].table.body.push([
        (a.bmp.minimumControlMeasures as string[]).join(', '),
        a.bmp.title,
        a.stormwaterActivity,
        a.description,
      ]);
    });

    return content;
  }

  generateSwmpModifications({ swmpModifications }: AnnualReportData) {
    const content: any[] = [
      {
        text: 'F. SWMP Modifications',
        style: ['title', 'marginVertLg'],
      },
      {
        ...this.generateNumberedLine(
          1,
          'The SWMP and MCM implementation procedures are reviewed each year:',
          this.getYesNoString(
            swmpModifications.isImplementationReviewedEachYear
          )
        ),
        style: 'question',
      },
      {
        ...this.generateNumberedLine(
          2,
          // tslint:disable-next-line: max-line-length
          "Changes have been made or are proposed to the SWMP since the NOI or the last annual report, including changes in response to TCEQ's review:",
          this.getYesNoString(swmpModifications.hasChanges)
        ),
        style: 'question',
      },
    ];

    if (swmpModifications.hasChanges) {
      content.push(
        this.generateTable(
          [70, 90, '*'],
          [
            'MCM(s)',
            'Measurable Goal(s) or BMP(s)',
            'Implemented or Proposed Changes (Submit NOC as needed)',
          ]
        )
      );

      swmpModifications.entityChanges.forEach(e => {
        content[3].table.body.push([
          e.isBmp
            ? ((e.entity as BMP).minimumControlMeasures as string[]).join(', ')
            : ((e.entity as MG).bmp.minimumControlMeasures as string[]).join(
                ', '
              ),
          e.entity.title,
          e.description,
        ]);
      });
    }

    content.push({
      margin: [14, 10, 14, 0],
      text: [
        { text: 'Note: ', bold: true },
        // tslint:disable-next-line: max-line-length
        'If changes include additions or substitutions of BMPs, include a written analysis explaining why the original BMP is ineffective or not feasible, and why the replacement BMP is expected to achieve the goals of the original BMP.',
      ],
    });

    content.push(
      {
        ...this.generateNumberedLine(
          3,
          'Explain additional changes or proposed changes not previously mentioned (i.e. dates, contacts, procedures, annexation of land, etc.).'
        ),
        style: 'question',
      },
      {
        text: swmpModifications.additionalChangesDescription,
        style: 'answer',
      }
    );

    return content;
  }

  generateAdditionalBmps({ additionalBmps }: AnnualReportData) {
    const content: any[] = [
      {
        text: 'G. Additional BMPs for TMDLs and I-Plans',
        style: ['title', 'marginVertLg'],
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'Provide a description and schedule for implementations of additional BMPs that may be necessary, based on monitoring results, to ensure compliance with applicable TMDLs and implementation plans.',
        style: 'question',
      },
    ];

    content.push(
      this.generateTable(
        [70, '*', 90, '*'],
        [
          'BMP',
          'Description',
          'Implementation schedule (start date, etc.)',
          'Status/Completion Date (completed, in progress, not started)',
        ]
      )
    );

    additionalBmps.additionalBmpsImplementations.forEach(b => {
      content[2].table.body.push([
        b.bmp,
        b.description,
        b.implementationSchedule,
        b.status,
      ]);
    });

    return content;
  }

  generateAdditionalInformation(report: AnnualReportData) {
    const { additionalInformation } = report;
    const content: any[] = [
      {
        text: 'H. Additional Information',
        style: ['title', 'marginVertLg'],
      },
      {
        ...this.generateNumberedLine(
          1,
          'Is the permittee replying on another entity to satisfy any permit obligations?:',
          this.getYesNoString(
            additionalInformation.isPermitteeRelyingOnEntities
          )
        ),
        style: 'question',
      },
    ];

    content.push({
      text:
        'If “Yes,” provide the name(s) of other entities and an explanation of their responsibilities (add more spaces or pages if needed).',
      style: 'question',
    });

    // 3
    content.push(this.generateTable([100, '*'], ['Name', 'Explanation']));

    additionalInformation.relyingEntities.forEach(e => {
      content[3].table.body.push([e.name, e.explanation]);
    });

    content.push(
      {
        ...this.generateNumberedLine(
          '2.a',
          'Is permittee part of a group sharing a SWMP with other entities?:',
          this.getYesNoString(additionalInformation.isPermitteePartOfGroup)
        ),
        style: 'question',
      },
      {
        ...this.generateNumberedLine(
          '2.b',
          'Is this a system-wide annual report including information for all permittees?:',
          this.getYesNoString(
            additionalInformation.isReportIncludingAllPermittees
          )
        ),
        style: 'question',
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'If “Yes,” list all associated authorization numbers, permittee names, and SWMP responsibilities of each member (add additional spaces or pages if needed):',
        margin: [14, 10, 14, 0],
      },
      this.generateTable([100, '*'], ['Authorization Number', 'Permittee'])
    );

    additionalInformation.associatedResponsibilities.forEach(r => {
      content[7].table.body.push([r.authorizationNumber, r.permittee]);
    });

    return content;
  }

  generateConstructionActivities(report: AnnualReportData) {
    const { constructionActivities } = report;
    const content: any[] = [
      {
        text: 'I. Construction Activities',
        style: ['title', 'marginVertLg'],
      },
      {
        ...this.generateNumberedLine(
          1,
          // tslint:disable-next-line: max-line-length
          'The number of construction activities that occurred in the jurisdictional area of the MS4 (Large and Small Site Notices submitted by construction site operators).'
        ),
        style: 'question',
      },
      {
        text: constructionActivities.numberOfConstructionActivities,
        style: 'answer',
      },
      {
        ...this.generateNumberedLine(
          '2.a',
          'Does the Permittee utilize the optional seventh MCM related to construction?:',
          this.getYesNoString(constructionActivities.utilizeSeventhMcm)
        ),
        style: 'question',
      },
      {
        ...this.generateNumberedLine(
          '2.b',
          'If “yes,” then provide the following information for this permit year:'
        ),
        style: 'question',
      },
      {
        ...this.generateSingleLineAnswer(
          'The number of municipal construction activities authorized under this general permit:',
          constructionActivities.numberOfMunicipalActivities
        ),
        margin: [14, 10, 14, 0],
      },
      {
        ...this.generateSingleLineAnswer(
          'The total number of acres disturbed for municipal construction projects:',
          ''
        ),
        margin: [14, 10, 14, 0],
      },
      {
        margin: [14, 10, 14, 0],
        text: [
          { text: 'Note: ', bold: true },
          'Though the seventh MCM is optional, implementation must be requested on the NOI or on a NOC and approved by the TCEQ',
        ],
      },
    ];

    return content;
  }

  generateCertification({ additionalInformation }: AnnualReportData) {
    return [
      {
        text: 'J. Certification',
        style: ['title', 'marginVertLg'],
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'If this is this a system-wide annual report including information for all permittees, each permittee shall sign and certify the annual report in accordance with 30 TAC §305.128 (relating to Signatories to Reports).',
        style: 'marginVertMd',
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'I certify under penalty of law that this document and all attachments were prepared under my direction or supervision in accordance with a system designed to assure that qualified personnel properly gathered and evaluated the information submitted. Based on my inquiry of the person or persons who manage the system, or those persons directly responsible for gathering the information, the information submitted is, to the best of my knowledge and belief, true, accurate, and complete. I am aware that there are significant penalties for submitting false information, including the possibility of fine and imprisonment for knowing violations.',
        italics: true,
        style: 'marginVertMd',
      },
      ...Array.from(
        {
          length:
            additionalInformation.isPermitteeRelyingOnEntities ||
            additionalInformation.isPermitteePartOfGroup
              ? 5
              : 2,
        },
        () => ({
          ...this.generateSignatureLine(),
          style: 'marginVertMd',
        })
      ),
      {
        text:
          'If you have questions on how to fill out this form or about the Stormwater Permitting program, please contact us at 512-239-4671.',
        bold: true,
        style: 'marginVertLg',
      },
      {
        text:
          // tslint:disable-next-line: max-line-length
          'Individuals are entitled to request and review their personal information that the agency gathers on its forms. They may also have any errors in their information corrected. To review such information, contact us at 512-239-3282.',
        style: 'marginVertMd',
      },
    ];
  }

  generateTable(columnWidths: any[], headerTitle: string[]) {
    return {
      margin: [0, 20, 0, 10],
      layout: {
        hLineWidth: (i, node) =>
          i === 0 || i === node.table.body.length ? 1 : 0.5,
        vLineWidth: i => (i === 0 || i === columnWidths.length ? 1 : 0.5),
        paddingTop: () => 8,
        paddingBottom: () => 5,
        paddingLeft: () => 10,
        paddingRight: () => 10,
      },
      table: {
        widths: columnWidths,
        headerRows: 1,
        keepWithHeaderRows: true,
        body: [
          headerTitle.map(title => ({
            text: title,
            style: 'tableHeader',
            margin: [5, 5],
          })),
        ],
      },
    } as any;
  }

  generateSingleLineAnswer(question, value) {
    const columns: any[] = [
      {
        width: '*',
        text: question,
      },
      {
        width: 'auto',
        text: value,
        bold: true,
      },
    ];

    return {
      columnGap: 20,
      columns,
    };
  }

  generateNumberedLine(num, title, value?) {
    const columns: any[] = [
      {
        width: 'auto',
        text: `${num}.`,
      },
      value !== undefined
        ? this.generateSingleLineAnswer(title, value)
        : {
            width: '*',
            text: title,
          },
    ];

    return {
      columnGap: 5,
      columns,
    };
  }

  generateSignatureLine() {
    const layout = {
      vLineWidth: () => 0,
      paddingLeft: i => (i === 0 ? 0 : 10),
      paddingBottom: () => 0,
    };

    return {
      columnGap: 20,
      lineHeight: 1,
      columns: [
        [
          {
            table: {
              widths: ['auto', '*'],
              body: [
                [
                  {
                    text: 'Name (printed):',
                    border: [false, false, false, false],
                  },
                  { text: '', border: [false, false, false, true] },
                ],
              ],
            },
            layout,
            style: 'marginVertSm',
          },
          {
            table: {
              widths: ['auto', '*'],
              body: [
                [
                  {
                    text: 'Signature:',
                    border: [false, false, false, false],
                  },
                  { text: '', border: [false, false, false, true] },
                ],
              ],
            },
            layout,
            style: 'marginVertSm',
          },
          {
            table: {
              widths: ['auto', '*'],
              body: [
                [
                  {
                    text: 'Name of MS4:',
                    border: [false, false, false, false],
                  },
                  { text: '', border: [false, false, false, true] },
                ],
              ],
            },
            layout,
            style: 'marginVertSm',
          },
        ],
        [
          {
            table: {
              widths: ['auto', '*'],
              body: [
                [
                  {
                    text: 'Title:',
                    border: [false, false, false, false],
                  },
                  { text: '', border: [false, false, false, true] },
                ],
              ],
            },
            layout,
            style: 'marginVertSm',
          },
          {
            table: {
              widths: ['auto', '*'],
              body: [
                [
                  {
                    text: 'Date:',
                    border: [false, false, false, false],
                  },
                  { text: '', border: [false, false, false, true] },
                ],
              ],
            },
            layout,
            style: 'marginVertSm',
          },
        ],
      ],
    };
  }

  getYesNoString(param: boolean) {
    return param ? 'YES' : 'NO';
  }
}
