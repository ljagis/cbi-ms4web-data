import * as path from 'path';

// folder where server.ts is
export const serverFolder = path.dirname(process.argv[1]);

export const pdfmakeFonts = {
  Roboto: {
    normal: serverFolder + '/fonts/Roboto-Regular.ttf',
    bold: serverFolder + '/fonts/Roboto-Medium.ttf',
    italics: serverFolder + '/fonts/Roboto-Italic.ttf',
    bolditalics: serverFolder + '/fonts/Roboto-MediumItalic.ttf',
  },
};
