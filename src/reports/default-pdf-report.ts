import { injectable } from 'inversify';
import { pdfmakeFonts } from './pdfmake-fonts';
import PdfPrinter = require('pdfmake');

@injectable()
export class DefaultPdfReport {
  numberFormat = new Intl.NumberFormat();

  pdfPrinter = new PdfPrinter(pdfmakeFonts);

  styles = {
    header: {
      fontSize: 16,
      bold: true,
    },
    subheader: {
      fontSize: 15,
      bold: true,
    },
    normal: {},
    small: {
      fontSize: 10,
    },
    tableHeader: {
      bold: true,
    },
    footer: {
      fontSize: 8,
    },
  };

  defaultStyle = {
    fontSize: 10,
    lineHeight: 1.2,
  };

  formatNumber(value: number) {
    return this.numberFormat.format(value);
  }

  defaultHeader(customer: string) {
    const header = {
      text: customer,
      margin: [40, 20, 40, 20],
      alignment: 'right',
      bold: true,
    };
    return header;
  }

  defaultFooter() {
    const footer = (currentPage, pageCount) => {
      return {
        margin: [24, 0],
        columns: [
          {
            width: '*',
            text: ``, // remove date generated
            style: 'footer',
            italics: true,
          },
          {
            width: 48,
            italics: true,
            alignment: 'right',
            style: 'footer',
            text: `${currentPage}/${pageCount}`,
          },
        ],
      };
      return footer;
    };
    return footer;
  }
}
