const CronJob = require('cron').CronJob;

import * as Knex from 'knex';
import * as fs from 'fs';

import {
  ConstructionSiteInspectionRepository,
  ConstructionSiteRepository,
  CustomFieldRepository,
} from './data';
import { inject, injectable } from 'inversify';

import { ConstructionSiteInspection } from './models';
import axios from 'axios';

const {
  PMIS_API_URL = '',
  PMIS_USERNAME = '',
  PMIS_PASSWORD = '',
  PMIS_SLACK_WEBHOOK: SLACK_WEBHOOK = '',
  PMIS_CUSTOM_FORM_TEMPLATE_ID: CUSTOM_FORM_TEMPLATE_ID = '',
  PMIS_INSPECTION_TYPE_ID: INSPECTION_TYPE_ID = '',
  PMIS_FIRST_CUSTOM_FIELD_ID: FIRST_CUSTOM_FIELD_ID = '',
  PMIS_DATE_LOG: DATE_LOG = 'pmis-date.log',
  PMIS_REPORT_ID_LOG: REPORT_ID_LOG = 'pmis-reportid.log',
} = process.env;

const CUSTOMER = 'FORTWAYNE';

type PMISValue = string | number | null;
type PMISResponse = {
  'SWPPP Inspection': { COLUMNS: string[]; DATA: PMISValue[][] };
};

const removeNumberPrefix = value => {
  if (!value) {
    return null;
  }
  return value.split('. ')[1];
};

const convertToYesOrNo = value => {
  if (!value) {
    return null;
  }
  if (isNaN(Number(value))) {
    return value;
  }
  return Number(value) ? 'Yes' : 'No';
};

const postToSlack = (webhookUrl: string, message: string) =>
  axios.post(webhookUrl, { text: message });

@injectable()
export class PMISCronJobRepository {
  @inject(Knex) protected knex: Knex;
  @inject(ConstructionSiteRepository)
  private _constructionSiteRepo: ConstructionSiteRepository;
  @inject(ConstructionSiteInspectionRepository)
  private _constructionSiteInspectionRepo: ConstructionSiteInspectionRepository;
  @inject(CustomFieldRepository)
  private _customFieldRepo: CustomFieldRepository;

  start() {
    new CronJob(
      '0 9 * * *', // 9am daily
      async () => {
        const isLastFetchedDateExist = fs.existsSync(DATE_LOG);
        let lastFetchedDate: Date | null = null;

        if (isLastFetchedDateExist) {
          const lastFetchedDateLogged = fs.readFileSync(DATE_LOG, {
            encoding: 'utf8',
          });
          lastFetchedDate = new Date(lastFetchedDateLogged);
        }

        const isReportIdLogExist = fs.existsSync(REPORT_ID_LOG);
        let reportIdWithInspections: { [key: string]: number[] } = {};

        if (isReportIdLogExist) {
          const reportIdLog = fs.readFileSync(REPORT_ID_LOG, {
            encoding: 'utf8',
          });

          reportIdWithInspections = reportIdLog
            .split('\n')
            .filter(Boolean)
            .reduce(
              (acc, logEntry) => {
                const [reportId, inspectonId] = logEntry.split(':').map(Number);

                if (acc[reportId]) {
                  acc[reportId].push(inspectonId);
                } else {
                  acc[reportId] = [inspectonId];
                }

                return acc;
              },
              {} as { [key: string]: number[] }
            );
        }

        const credential = await axios.post<{ access_token: string }>(
          `${PMIS_API_URL}/login`,
          {
            username: PMIS_USERNAME,
            password: PMIS_PASSWORD,
          }
        );

        const accessToken = credential.data.access_token;

        const constructionSites = await this._constructionSiteRepo.fetch(
          CUSTOMER
        );

        const permits = await axios.get<
          { PermitNumber: string; WorkOrder: number }[]
        >(`${PMIS_API_URL}/dailyreport/swppppermits?token=${accessToken}`);

        const response = await axios.get<PMISResponse>(
          `${PMIS_API_URL}/ReportExport/9?token=${accessToken}${
            lastFetchedDate
              ? `&lastUpdatedDate=${lastFetchedDate.toISOString()}`
              : ''
          }`
        );

        const { COLUMNS, DATA } = response.data['SWPPP Inspection'];
        const now = new Date();
        const log: string[] = [];

        for (let i = 0; i < DATA.length; i++) {
          const fields = DATA[i];

          const data = COLUMNS.reduce(
            (acc, column, index) => ({ ...acc, [column]: fields[index] }),
            {} as Record<string, PMISValue>
          );

          const reportId = data['REPORTID'];

          if (!data['REPORTDATE']) {
            log.push(
              `${now.toISOString()}: Invalid "REPORTDATE" on ${reportId} report`
            );
            continue;
          }

          // PMIS lists surname first. For 3 or more word names, MS4web considers 1 work given and rest surname.
          // Ex. { "id": 2376, "givenName": "Oscar", "middleName": null, "surname": "Martinez Rubio", "fullName": "Oscar Martinez Rubio" }
          const inspectorSplit = (data['INSPECTOR'] as string).lastIndexOf(' ');
          const surname = (data['INSPECTOR'] as string).slice(
            0,
            inspectorSplit
          );
          const givenname = (data['INSPECTOR'] as string).slice(
            inspectorSplit + 1
          );
          const user = await this.knex('UsersCustomers')
            .where({ CustomerId: CUSTOMER })
            .join('Users', 'Users.Id', 'UsersCustomers.UserId')
            .where({ 'Users.GivenName': givenname, 'Users.Surname': surname })
            .select(['Users.*'])
            .first();

          if (!user) {
            log.push(
              `${now.toISOString()}: Invalid "INSPECTOR" on ${reportId} report`
            );
            continue;
          }

          const workOrder = Number(data['WORKORDER']);

          const isUpdate = !!reportIdWithInspections[reportId];

          const payload = {
            additionalInformation: data['ADDITIONALINFORMATION'] as string,
            areErosionControlsAcceptable: false,
            areLocalControlsAcceptable: false,
            areNonStormWaterControlsAcceptable: false,
            areOutfallVelocityControlsAcceptable: false,
            arePlanRecordsCurrent: false,
            areStabilizationControlsAcceptable: false,
            areStructuralControlsAcceptable: false,
            areTrackingControlsAcceptable: false,
            complianceStatus: data['COMPLIANCE'] as string,
            createdDate: new Date(data['REPORTDATE']),

            customFormTemplateId: Number(CUSTOM_FORM_TEMPLATE_ID),

            dateResolved: null,

            followUpInspectionId: null,
            geometryError: null,
            inspectionDate: data['INSPECTIONDATE']
              ? new Date(data['INSPECTIONDATE'])
              : null,
            inspectionType: data['INSPECTIONTYPE'] as string,

            inspectionTypeId: Number(INSPECTION_TYPE_ID),

            inspectorId: user.Id,

            inspectorId2: null,
            originalId: null,
            isMaintenanceOfControlsAcceptable: false,
            isPlanOnSite: false,
            isSiteActive: false,
            isSitePermitted: false,
            isWasteManagementAcceptable: false,
            lat: null,
            lng: null,
            scheduledInspectionDate: data['SCHEDULEDINSPECTIONDATE']
              ? new Date(data['SCHEDULEDINSPECTIONDATE'])
              : null,
            timeIn: data['TIMEIN'] ? new Date(data['TIMEIN']) : null,
            timeOut: data['TIMEOUT'] ? new Date(data['TIMEOUT']) : null,
            weatherCondition: data['WEATHERCONDITION'] as string,
            weatherDateTime: null,
            weatherLast24: data['PRECIPITATIONLAST24HOURSIN']
              ? Number(data['PRECIPITATIONLAST24HOURSIN'])
              : null,
            weatherLast72: data['PRECIPITATIONLAST72HOURSIN']
              ? Number(data['PRECIPITATIONLAST72HOURSIN'])
              : null,
            weatherPrecipitationIn: data['PRECIPITATIONIN']
              ? Number(data['PRECIPITATIONIN'])
              : null,
            weatherStationId: null,
            weatherTemperatureF: data['TEMPERATUREF']
              ? Number(data['TEMPERATUREF'])
              : null,
            windDirection: null,
            windSpeedMph: null,
          };

          let siteInspectionIds: number[] = [];

          if (isUpdate) {
            await Promise.all(
              reportIdWithInspections[reportId].map(insepctionId =>
                this._constructionSiteInspectionRepo.update(
                  insepctionId,
                  payload,
                  CUSTOMER
                )
              )
            );

            siteInspectionIds = reportIdWithInspections[reportId];
          } else {
            const permitNumbers = permits.data
              .filter(p => p.WorkOrder === workOrder)
              .map(p => p.PermitNumber);

            const sites = constructionSites.filter(
              c =>
                permitNumbers.some(p => c.name.includes(p)) ||
                permitNumbers.includes(c.trackingId)
            );

            const siteInspections = await Promise.all(
              sites.map(site =>
                this._constructionSiteInspectionRepo.insert(
                  { ...payload, constructionSiteId: site.id },
                  CUSTOMER
                )
              )
            );

            siteInspectionIds = siteInspections.map(
              inspection => inspection.id
            );
          }

          const firstCustomFieldId = Number(FIRST_CUSTOM_FIELD_ID);

          const customFieldUpdatePromises = siteInspectionIds.map(
            inspectionId =>
              this._customFieldRepo.updateCustomFieldValuesForInspection(
                ConstructionSiteInspection,
                inspectionId,
                [
                  { id: firstCustomFieldId, value: reportId },
                  { id: firstCustomFieldId + 1, value: data['COCCOMPLIANCE'] },
                  {
                    id: firstCustomFieldId + 2,
                    value: data['TIMEOFINSPECTION'],
                  },
                  {
                    id: firstCustomFieldId + 3,
                    value: data['FOLLOWUPINSPECTIONDATE'],
                  },
                  {
                    id: firstCustomFieldId + 4,
                    value: data['CONSTRUCTIONSTAGE'],
                  },
                  {
                    id: firstCustomFieldId + 5,
                    value: data['INDIVIDUALSMETONSITE'],
                  },
                  { id: firstCustomFieldId + 6, value: null }, // section field
                  {
                    id: firstCustomFieldId + 7,
                    value: removeNumberPrefix(
                      data[
                        'A1DISTURBEDAREASHAVEBEENADEQUATELYPROTECTEDTHROUGHSEEDINGOROTHERAPPROPRIATEEROSIONANDSEDIMENTCONTROLMEASURES'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 8,
                    value: removeNumberPrefix(
                      data[
                        'A2APPROPRIATEPERIMETERSEDIMENTCONTROLMEASURESHAVEBEENIMPLEMENTED'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 9,
                    value: removeNumberPrefix(
                      data[
                        'A3CONVEYANCECHANNELSHAVEBEENSTABILIZEDORPROTECTEDWITHAPPROPRIATESEDIMENTCONTROLMEASURES'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 10,
                    value: removeNumberPrefix(
                      data[
                        'A4EROSIONSEDIMENTCONTROLMEASURESAREINSTALLEDPROPERLY'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 11,
                    value: removeNumberPrefix(
                      data['A5STORMDRAININLETSHAVEBEENADEQUATELYPROTECTED']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 12,
                    value: removeNumberPrefix(
                      data['A6OUTLETSHAVEBEENADEQUATELYSTABILIZED']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 13,
                    value: removeNumberPrefix(
                      data[
                        'A7EXISTINGEROSIONSEDIMENTCONTROLMEASURESAREBEINGMAINTAINED'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 14,
                    value: removeNumberPrefix(
                      data[
                        'A8PUBLICPRIVATEROADWAYSAREBEINGKEPTCLEAROFACCUMULATEDSEDIMENTORTRACKEDSOIL'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 15,
                    value: removeNumberPrefix(
                      data[
                        'A9EROSIONSEDIMENTCONTROLMEASURESHAVEBEENINSTALLEDANDMAINTAINEDONINDIVIDUALBUILDINGSITES'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 16,
                    value: removeNumberPrefix(
                      data['A10AWEEKLYINSPECTIONREPORTSARECOMPLETED']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 17,
                    value: data['A10BWEEKLYREPORTINGCOMMENTS'] || null,
                  },
                  { id: firstCustomFieldId + 18, value: null }, // section field
                  {
                    id: firstCustomFieldId + 19,
                    value:
                      data[
                        'A1SITECONDITIONSPRESENTAHIGHPOTENTIALFOROFFSITESEDIMENTATION'
                      ] || null,
                  },
                  {
                    id: firstCustomFieldId + 20,
                    value:
                      data['A2THEREISEVIDENCEOFOFFSITESEDIMENTATION'] || null,
                  },
                  { id: firstCustomFieldId + 21, value: null }, // section field
                  {
                    id: firstCustomFieldId + 22,
                    value: data['ITEMS1TO13THATAREMARKEDBYYESMUSTBERESOLVEDBY']
                      ? new Date(
                          data['ITEMS1TO13THATAREMARKEDBYYESMUSTBERESOLVEDBY']
                        )
                      : null,
                  },
                  {
                    id: firstCustomFieldId + 23,
                    value: convertToYesOrNo(
                      data[
                        'A1INSTALLANAPPROPRIATESEDIMENTCONTROLPRACTICEBETWEENCONSTRUCTIONAREASANDLOWERLYINGAREAS'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 24,
                    value: convertToYesOrNo(
                      data['A2REPLACEREPAIRSILTFENCEANDORSTRAWBALEBARRIERS']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 25,
                    value: convertToYesOrNo(
                      data[
                        'A3ENTRENCHSILTFENCE68INANDORSTRAWBALEBARRIERS416INANDSTAKESTRAWBALES'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 26,
                    value: convertToYesOrNo(
                      data[
                        'A4REMOVEACCUMULATEDSEDIMENTFROMSEDIMENTTRAPSBASINSBEHINDSILTFENCESTRAWBALESAROUNDSTORMDRAININLETPROTECTION'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 27,
                    value: convertToYesOrNo(
                      data['A5TEMPORARYSEEDFERTILIZEANDORMULCH']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 28,
                    value: convertToYesOrNo(
                      data[
                        'A6PERMANENTSEEDFERTILIZEANDMULCHAREASTHATAREATFINALGRADE'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 29,
                    value: convertToYesOrNo(
                      data['A7PROTECTSTORMDRAININLETSCURBINLETSDROPINLETS']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 30,
                    value: convertToYesOrNo(
                      data[
                        'A8RESHAPEANDSTABILIZESIDESLOPESOFSEDIMENTTRAPSBASINSDETENTIONRETENTIONBASINS'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 31,
                    value: convertToYesOrNo(
                      data['A9INSTALLMAINTAINCONSTRUCTIONENTRANCES']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 32,
                    value: convertToYesOrNo(
                      data['A10RESHAPEANDSTABILIZECONVEYANCECHANNELS']
                    ),
                  },
                  {
                    id: firstCustomFieldId + 33,
                    value: convertToYesOrNo(
                      data[
                        'A11PLACEGREATEREMPHASISONEROSIONANDSEDIMENTCONTROLONBUILDINGSITESCONTRACTORSSUBCONTRACTORSMATERIALVENDORSAN'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 34,
                    value: convertToYesOrNo(
                      data[
                        'A12REMOVEALLTEMPORARYCONTROLSTRUCTURESTHATARENOLONGERNEEDED'
                      ]
                    ),
                  },
                  {
                    id: firstCustomFieldId + 35,
                    value: data['A13OTHER'] || null,
                  },
                  { id: firstCustomFieldId + 36, value: null }, // section field
                ],
                CUSTOMER
              )
          );

          await Promise.all(customFieldUpdatePromises);

          if (!isUpdate) {
            const reportIdLog = siteInspectionIds.map(
              inspectionId => `${reportId}:${inspectionId}\n`
            );
            fs.appendFileSync('pmis-reportId.log', reportIdLog.join('\n'));
            postToSlack(SLACK_WEBHOOK, reportIdLog.join('\n'));
          }
        }

        fs.writeFileSync('pmis-date.log', `${new Date()}\n`);
        postToSlack(SLACK_WEBHOOK, 'Completed');

        if (log.length) {
          fs.appendFileSync('pmis-sync-error.log', `${log.join('\n')}\n`);
          postToSlack(SLACK_WEBHOOK, log.join('\n'));
        }
      },
      null,
      true
    );
  }
}
