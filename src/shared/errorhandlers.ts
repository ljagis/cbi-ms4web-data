import * as express from 'express';
import * as validate from 'class-validator';
import * as _ from 'lodash';
import { HttpStatusCodes } from './httpstatuscodes';
import { NotFoundError, ClientError } from './errors';

/**
 * Express error catch all middleware
 * See: https://expressjs.com/en/guide/error-handling.html
 * pass `next(err)` in your routes to enable this error handler
 * [expressErrorHandler description]
 * @param  {[type]}               err  Error to send to response
 */
export function expressErrorHandler(
  err,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
): void {
  if (err instanceof NotFoundError) {
    res.status(HttpStatusCodes.NotFound).json({
      message: err.message,
      statusCode: HttpStatusCodes.NotFound,
    });
    return;
  }
  if (err instanceof Array && err[0] instanceof validate.ValidationError) {
    // validation error - handle accordingly
    let validationErrors = err as validate.ValidationError[];
    let errorMessages = _.reduce<validate.ValidationError, string[]>(
      validationErrors,
      (memo, e) => memo.concat(_.values(e.constraints)),
      []
    );
    res.status(HttpStatusCodes.BadRequest).json({
      message: errorMessages.join('.  '),
    });
    return;
  }
  if (err instanceof ClientError) {
    let errorObj: any = {
      message: err.message,
      statusCode: HttpStatusCodes.BadRequest,
    };
    if (process.env.NODE_ENV === 'development') {
      errorObj.stack = err.stack;
    }
    res.status(HttpStatusCodes.BadRequest).json(errorObj);
    return;
  }
  if (err && err.errorCauses instanceof Array) {
    // Okta errors
    const message =
      err.message ||
      err.errorCauses.map(errorCause => errorCause.errorSummary).join(',');
    let errorObj = {
      message: message,
      statusCode: HttpStatusCodes.BadRequest,
    };
    res.status(HttpStatusCodes.BadRequest).json(errorObj);
    return;
  }
  if (err) {
    let errorObj: any = {
      error: err.error || 'Internal Server Error',
      message: err.message,
      statusCode: HttpStatusCodes.InternalServerError,
    };
    if (process.env.NODE_ENV === 'development') {
      errorObj.stack = err.stack;
    }
    console.error(err);
    res.status(HttpStatusCodes.InternalServerError).json(errorObj);
    return;
  } else {
    next(err);
  }
}

/**
 * checks `err` for errors.  If it contains errors, this will throw a
 * formatted ValidationError into a single error object
 * @param  {validate.ValidationError} err [description]
 * @return {[type]}                       [description]
 */
export function throwErrorOnValidationError(
  errors: validate.ValidationError[]
): void {
  if (errors && errors.length) {
    throw errors;
  }
}
