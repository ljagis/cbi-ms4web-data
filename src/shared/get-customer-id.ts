import * as Bluebird from 'bluebird';

export async function getCustomerId(req: any): Bluebird<string> {
  if (req.principal && req.principal.selectedCustomer) {
    return req.principal.selectedCustomer.id;
  }
}
