import * as _ from 'lodash';

export default function errorLog(): void {
  // Can't use variable params `...args` until Node 6
  let messages = _.map(arguments, arg => {
    if (arg && arg.stack) {
      return arg.stack;
    }
    return _.isObject(arg) ? JSON.stringify(arg) : arg;
  });
  messages.unshift(new Date());
  console.error(messages.join(', '));
}
