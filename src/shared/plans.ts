export const plans = {
  plus: {
    maxActiveUsers: 1,
  },
  pro: {
    maxActiveUsers: 4,
  },
  premium: {
    maxActiveUsers: false,
  },
};

export const LIMIT_MAX_ACTIVE_CUSTOM_FORM_TEMPLATES = 3;
export const LIMIT_MAX_ACTIVE_RECORDS_PER_ASSET = 10;
