/**
 * Throw this error if the client did something wrong.
 * Example: bad request, invalid user name/password, etc
 *
 * @export
 * @class ClientError
 * @extends {Error}
 */
export class ClientError implements Error {
  name: string;
  message: string;
  stack?: string;

  constructor(message?: string) {
    Error.captureStackTrace(this);
    this.message = message;
    this.name = (this as any).constructor.name;
  }
}

export class NotFoundError extends ClientError {
  constructor(message = 'Resource not found') {
    super(message);
  }
}
