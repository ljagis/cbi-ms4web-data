/**
 * Exports entities to Array of Arrays for xlsx consumption
 *
 * @export
 * @param {any[]} entities
 */
export function entitiesToAoA(entities: any[]) {
  let rows = [];
  if (entities && entities.length) {
    // set up header
    const headerRow = Object.keys(entities[0]);
    rows = [headerRow];

    const records = entities.map(entity =>
      Object.keys(entity).map(k => entity[k])
    );
    rows.push(...records);
  } else {
    // empty aoa
    rows = [];
  }

  return rows;
}
