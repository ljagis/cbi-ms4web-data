import { validate, ValidatorOptions } from 'class-validator';
import * as Bluebird from 'bluebird';

/**
* Proxy method for class-validator.  If fails, throws error
* See https://github.com/pleerock/class-validator
* @param  {[type]}                     options [description]
* @return {Bluebird<ValidationError[]>}         [description]
*/
export async function validateModel<T>(
  obj: T,
  validatorOptions?: ValidatorOptions
): Bluebird<T> {
  const errors = await validate(obj, validatorOptions);
  if (errors && errors.length) {
    throw errors;
  }
  return obj;
}
