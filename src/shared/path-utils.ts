import * as fs from 'fs-extra';
import * as path from 'path';

export function getTempPath() {
  const root = path.dirname(process.argv[1]);
  const tempPath = path.join(root, 'public/temp');
  fs.ensureDirSync(tempPath);
  return tempPath;
}
