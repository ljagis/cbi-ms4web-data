/**
 * See https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 *
 * @export
 * @enum {number}
 */
export enum HttpStatusCodes {
  // Good
  OK = 200,
  Created = 201,
  Accepted = 202,

  MultipleChoices = 300,
  MovedPermanently = 301,
  Found = 302,
  SeeOther = 303,
  NotModified = 304,

  // Bad
  BadRequest = 400,
  Unauthorized = 401,
  PaymentRequired = 402,
  Forbidden = 403,
  NotFound = 404,
  MethodNotAllowed = 405,
  NotAcceptable = 406,
  ProxyAuthRequired = 407,
  RequestTimeout = 408,
  Conflict = 409,
  Gone = 410,

  InternalServerError = 500,
}
