export enum Role {
  super = 'super',
  admin = 'admin',
  inspector = 'inspector',
  readonly = 'readonly',
  contractor = 'contractor', // special role
  'inspector-no-delete' = 'inspector-no-delete',
}

export type FeaturePermissionUnion = keyof FeaturePermission;
export type PermissionUnion = keyof Permission;

interface Permission {
  view?: boolean;
  edit?: boolean;
  delete?: boolean;
}

interface FeaturePermission {
  inspections: Permission;
  bmp: Permission;
  activityLogs: Permission;
  assets: Permission;
  investigations: Permission;
  customFields?: Permission;
}

type RoleNames =
  | 'super'
  | 'admin'
  | 'inspector'
  | 'readonly'
  | 'contractor'
  | 'inspector-no-delete';

type RoleFeaturePermission = Record<RoleNames, FeaturePermission>;

const allPermissions: Permission = {
  view: true,
  edit: true,
  delete: true,
};
const readonlyPermission: Permission = {
  view: true,
  edit: false,
  delete: false,
};
const noPermission: Permission = {
  view: false,
  edit: false,
  delete: false,
};

export const roleFeaturePermissions: RoleFeaturePermission = {
  [Role.super]: {
    inspections: allPermissions,
    bmp: allPermissions,
    activityLogs: allPermissions,
    investigations: allPermissions,
    assets: allPermissions,
    customFields: allPermissions,
  },
  [Role.admin]: {
    inspections: allPermissions,
    bmp: allPermissions,
    activityLogs: allPermissions,
    investigations: allPermissions,
    assets: allPermissions,
    customFields: allPermissions,
  },
  [Role.inspector]: {
    inspections: allPermissions,
    bmp: {
      view: true,
      edit: false,
      delete: false,
    },
    activityLogs: {
      view: true,
      edit: true,
      delete: false,
    },
    investigations: {
      view: true,
      edit: true,
      delete: false,
    },
    assets: {
      view: true,
      edit: true,
      delete: false,
    },
    customFields: {
      view: true,
    },
  },
  [Role['inspector-no-delete']]: {
    inspections: {
      view: true,
      edit: true,
      delete: false,
    },
    bmp: {
      view: true,
      edit: false,
      delete: false,
    },
    activityLogs: {
      view: true,
      edit: true,
      delete: false,
    },
    investigations: {
      view: true,
      edit: true,
      delete: false,
    },
    assets: {
      view: true,
      edit: false,
      delete: false,
    },
    customFields: {
      view: true,
    },
  },
  [Role.readonly]: {
    inspections: readonlyPermission,
    bmp: readonlyPermission,
    activityLogs: readonlyPermission,
    investigations: readonlyPermission,
    assets: readonlyPermission,
    customFields: {
      view: true,
    },
  },
  [Role.contractor]: {
    inspections: {
      view: true,
      edit: true,
      delete: false,
    },
    bmp: noPermission,
    activityLogs: noPermission,
    investigations: {
      view: true,
      edit: true,
      delete: false,
    },
    assets: {
      view: true,
      edit: false,
      delete: false,
    },
    customFields: {
      view: true,
    },
  },
};
