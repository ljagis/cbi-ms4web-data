/**
 * Promisifies a stream
 * Usage: var stream = API.getStream(file);
    stream.pipe(endPoint);
    return streamToPromise(stream);
 * see https://github.com/petkaantonov/bluebird/issues/332
 * @param stream
 */
export function streamToPromise(stream) {
  return new Promise(function(resolve, reject) {
    stream.on('end', resolve);
    stream.on('error', reject);
  });
}
