export type InspectionType =
  | 'ConstructionSiteInspection'
  | 'FacilityInspection'
  | 'OutfallInspection'
  | 'StructureInspection';
