import * as Knex from 'knex';

export async function dropDFConstraint(knex: Knex, tableName, columnName) {
  const { constraintName } = await knex
    .raw(
      `
    SELECT
    df.name as constraintName ,
    t.name as tableName,
    c.NAME as columnName
  FROM sys.default_constraints df
  INNER JOIN sys.tables t ON df.parent_object_id = t.object_id
  INNER JOIN sys.columns c ON df.parent_object_id = c.object_id AND df.parent_column_id = c.column_id

  where t.name = '${tableName}'
  and c.name = '${columnName}'
  `
    )
    .get<any>(0);

  await knex.raw(`
    alter table ${tableName} drop constraint ${constraintName};
  `);
}
