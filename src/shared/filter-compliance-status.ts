import { ComplianceStatus } from '../models';

export const filterComplianceStatus = (customerId: string) => (
  status: ComplianceStatus
) => {
  const customerIds: string[] = JSON.parse(status.customerIds);
  const excludeCustomerIds: string[] = JSON.parse(status.excludeCustomerIds);

  if (
    (!customerIds.length || customerIds.includes(customerId)) &&
    !excludeCustomerIds.includes(customerId)
  ) {
    return true;
  }

  return false;
};
