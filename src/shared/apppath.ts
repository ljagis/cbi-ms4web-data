/*
 * Ensures there is a leading slash and NO trailing slash.
 * If no path then returns empty
 */
function getFixedAppPath() {
  let rawPath: string = process.env.APP_PATH || '';
  if (rawPath === '' || rawPath === '/') {
    return '';
  }
  let path = '/' + rawPath.replace(/^\/|\/$/g, '');
  return path;
}

export const APP_PATH = getFixedAppPath();
