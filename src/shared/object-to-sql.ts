import * as Knex from 'knex';
import * as _ from 'lodash';
import * as moment from 'moment';
import {
  getTableName,
  getIdField,
  getSqlMapping,
  getJsonAttribute,
} from '../decorators';

/**
 * object to sql representation
 *
 * @param {{ new ()}} EntityClass
 * @param {any} obj
 * @param options
 * @returns
 *
 */
export function objectToSqlData(
  EntityClass: { new () },
  obj,
  knex: Knex,
  options?
) {
  const tableName = getTableName(EntityClass);
  const sqlMappings = getSqlMapping(EntityClass);
  const idField = getIdField(EntityClass);
  const sqlData: any = {};

  Object.keys(obj).forEach(prop => {
    const sqlColumn = sqlMappings[prop];
    const fullColumnName = `${tableName}.${sqlColumn}`;
    // sql mapping does not exist; skip
    if (sqlColumn === undefined) {
      return;
    }
    const value = obj[prop];
    // skip id field
    if (prop === idField) {
      return;
    }

    // handle Geometry
    if (prop === 'lat' || prop === 'lng') {
      const lat = _.isNumber(obj.lat) ? _.toNumber(obj.lat) : null;
      const lng = _.isNumber(obj.lng) ? _.toNumber(obj.lng) : null;
      if (lat === null && lng === null) {
        sqlData[`${tableName}.Geometry`] = null;
      } else {
        sqlData[`${tableName}.Geometry`] = knex.raw(
          `geometry::Point(${lng}, ${lat}, 4326)`
        );
      }
      return;
    }

    // handle json attribute
    const isJsonAttribute = getJsonAttribute(EntityClass, prop);
    if (isJsonAttribute) {
      sqlData[fullColumnName] = JSON.stringify(value);
      return;
    }

    // fix issue with Date storing as local time zone in SQL
    if (value instanceof Date) {
      sqlData[fullColumnName] = moment(value).toISOString();
    } else if (typeof value === 'number') {
      // https://github.com/patriksimek/node-mssql/issues/341#issuecomment-254566296
      sqlData[fullColumnName] = value.toString();
    } else if (value === true) {
      // Note: SQL bit doesn't like true or false, convert to bit
      sqlData[fullColumnName] = 1;
    } else if (value === false) {
      sqlData[fullColumnName] = 0;
    } else {
      sqlData[fullColumnName] = value;
    }
  });

  return sqlData;
}
