import { BMP, MG } from '../models';

export const CURRENT_CUSTOMER_FIELD = 'currentCustomer';

export const SWMP_TEMPLATE: {
  bmps: Partial<BMP & { mgs: Partial<MG>[] }>[];
} = {
  bmps: [
    {
      number: '1-A',
      title: 'Storm Drain Marking',
      startYear: 0,
      minimumControlMeasures: '1',
      description:
        'Obtain assistance to epoxy a previously developed Storm Drain disk to each of the storm drains throughout the City. ' +
        'These markers advise against storm water pollution. The City may use volunteer groups to complete this BMP.',
      mgs: [
        {
          number: '1.1.1',
          title: 'Drain Marker',
          yearsImplemented: '1',
          isQuantityRequired: true,
          informationUsed: 'Drains Marked',
          units: 'Drains',
          annualGoal: '100',
        },
        {
          number: '1-A',
          title: 'Storm Drain Marking',
          description:
            'Year1-Year 5: Continue to identify existing unmarked inlets. Require new development to mark new inlets.',
          yearsImplemented: '1,2,3,4,5',
          isQuantityRequired: true,
          informationUsed: 'Drains Marked',
          units: 'Markers',
          annualGoal: '100',
        },
      ],
    },
    {
      number: '1-A',
      title: 'TEST',
      startYear: 0,
      minimumControlMeasures: '1,4',
      description: 'TEST',
      mgs: [
        {
          number: '1-A',
          title: 'TEST',
          description: 'TEST',
          yearsImplemented: '1,2,3,4,5',
          isQuantityRequired: true,
          informationUsed: 'Educational Brochures',
          units: 'Brochures',
          annualGoal: '500',
        },
      ],
    },
    {
      number: '1-B',
      title:
        'Storm Water Quality, Illegal Dumping, and Erosion Control Educational Brochures',
      startYear: 0,
      minimumControlMeasures: '1,2',
      description:
        'The NCTCOG, and the TCEQ have previously developed brochures explaining the effects of pollution in regards to ' +
        'Storm Water Quality. These government entities have stated that adjacent cities may use the existing brochures ' +
        'as long as their name and address remain on the brochure. \nThe City will obtain a copy of the desired brochures, ' +
        'have them reprinted, and distribute the brochures in the following ways: \n' +
        '• Include the brochures in with each water bill,\n' +
        '• Maintain a supply of brochures for the public at the City government buildings and the City Library (if applicable), and\n' +
        '• Maintain an ample supply of brochures at hotels / motels within the City',
      mgs: [
        {
          number: '1-B',
          title:
            'Storm Water Quality, Illegal Dumping, and Erosion Control Educational Brochures',
          description:
            'Year 1-Year 5: Obtain brochures. Distribute to public facilities. Count the number of brochures before and after ' +
            'distribution.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '1-C',
      title: 'SmartScape™ Brochure Distribution',
      startYear: 0,
      minimumControlMeasures: '1,4',
      description:
        'The NCTCOG has developed a brochure that advises the reader on aesthetically-pleasing, environmentally-friendly ' +
        'landscaping that will reduce residential lot erosion. The brochure may be distributed once a year, and may be in ' +
        'conjunction with a City-designated “SmartScape™ Month”. \nThe brochures will need to be distributed in at least one ' +
        'of the following ways:\n' +
        '• Include the brochures in with each water bill,\n' +
        '• Maintain a supply of brochures for the public at the City government buildings and the City Library (if applicable), and\n' +
        '• Maintain an ample supply of brochures at hotels / motels within the City',
      mgs: [
        {
          number: '1-C',
          title: 'SmartScape™ Brochure Distribution',
          description:
            'Year 1 through 5 – Obtain a copy of the brochure and distribute to public places. Count the number of brochures ' +
            'before and after distribution.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '1-D',
      title: 'Present Storm Water Information in Public Setting',
      startYear: 0,
      minimumControlMeasures: '1',
      description:
        'The TCEQ has previously developed informative posters regarding storm water pollution and storm water regulations. ' +
        'The City may obtain a copy of these posters to post within city government buildings and/or in public places. ' +
        'The posters will be periodically updated depending on available materials.',
      mgs: [
        {
          number: '1-D',
          title: 'Present Storm Water Information in Public Setting',
          description:
            'Year 1 through 5 – Obtain a copy of previously developed posters by the TCEQ or other organization or agency. ' +
            'Reprint and display in any or all of the above mentioned locations. Remove and replace poster as necessary.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '1-E',
      title: 'Storm Water Insert in Water Bill',
      startYear: 0,
      minimumControlMeasures: '1',
      description:
        'The City currently distributes a monthly water bill to all residents and businesses within the City and the City’s ETJ. ' +
        'The City will add a small column or “advertisement” regarding storm water pollution and related issues in the bill, or ' +
        'include one of the informational brochures. The City will also make available the storm water inserts to the General ' +
        'Public at the City Library and City offices. Although utility bills are distributed each month, the storm water inserts ' +
        'will only be placed in the bills once a year.',
      mgs: [
        {
          number: '1-E',
          title: 'Storm Water Insert in Water Bill',
          description:
            'Years 1 through 5 – The City will count the number of inserts sent out to various people.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '1-F',
      title:
        'Establish a Link on the City’s website for “Storm Drain Information”',
      startYear: 0,
      minimumControlMeasures: '1,2,4',
      description:
        'The City will continue to maintain a website with a stormwater page. The page will allow citizens to report illegal dumping. ' +
        'The link will also provide viewers with storm drain educational information. ' +
        'The website will allow citizens to provide feedback and input on the stormwater program.',
      mgs: [
        {
          number: '1-F',
          title:
            'Establish a Link on the City’s website for “Storm Drain Information”',
          description:
            'Year 1-5: Continue to manage the website and post stormwater related information.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '1-G',
      title: 'Local Clean-Up',
      startYear: 0,
      minimumControlMeasures: '1',
      description:
        'Annually the City has a local Clean-Up program for residents and businesses to participate to clean up the City.',
      mgs: [
        {
          number: '1-G',
          title: 'Local Clean-Up',
          description:
            'Annually the City has a local Clean-Up program for residents and businesses to participate to clean up the City.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '2-A',
      title:
        'Storm Water Quality, Illegal Dumping, and Erosion Control Educational Brochures',
      startYear: 0,
      minimumControlMeasures: '1,2',
      description:
        'See BMP 1-B for description and distribution practices for this BMP',
      mgs: [],
    },
    {
      number: '2-B',
      title:
        'Establish a Link on the City’s website for “Storm Drain Information”',
      startYear: 0,
      minimumControlMeasures: '1,2,4',
      description:
        'See BMP 1-F for description and distribution practices for this BMP.',
      mgs: [],
    },
    {
      number: '2-C',
      title: 'Mapping of Storm Sewer System',
      startYear: 0,
      minimumControlMeasures: '2',
      description:
        'The City will continue to map the storm drain systems throughout the City. The city currently has a map of the major ' +
        'streams/creeks.',
      mgs: [
        {
          number: '2-C',
          title: 'Mapping of Storm Sewer System',
          description:
            'Year 1-5: The City will continue to map new storm drain systems and major outfalls.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '2-D',
      title: 'Review of Site Plan submittal and SWPPP prior to construction.',
      startYear: 0,
      minimumControlMeasures: '2,4',
      description:
        'The City has contacted an outside consultant to review the Site Plan for proposed developments. With this storm water ' +
        'management program, the City will also require a review of the Site Plan, Erosion Control Plans, and SWPPP paying ' +
        'close attention to potential causes of runoff and anticipate illicit discharge due to increased storm water regulations. ' +
        'Part of the review will be to ensure the City’s current detention pond ordinance is being implemented in the design. ' +
        'The detention ponds will serve as a post-construction BMP for the City.',
      mgs: [
        {
          number: '2-D',
          title:
            'Review of Site Plan submittal and SWPPP prior to construction.',
          description:
            'Year 1-5– Increase submittal requirements to include the submission of Erosion Control Plans and an SWPPP with all ' +
            'construction plan sets. Reviewer will check discharges from site to prevent/prohibit illicit discharges. Also, require all' +
            ' sites to follow construction general permit in regards to allowable discharges and gaining authorization to discharge ' +
            'storm water.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '2-E',
      title:
        'Implement Ordinance and Enforcement Procedures for Eliminating Sources of Illicit Discharges',
      startYear: 0,
      minimumControlMeasures: '2',
      description:
        'An ordinance was developed during the previous permit cycle. The City will continue to implement the ordinance.',
      mgs: [
        {
          number: '2-E',
          title:
            'Implement Ordinance and Enforcement Procedures for Eliminating Sources of Illicit Discharges',
          description: 'Year 1-5: Continue to implement the ordinance.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '2-F',
      title:
        'Training of City Employees – Illicit Discharges (if specific training is available)',
      startYear: 0,
      minimumControlMeasures: '2',
      description:
        'Send one staff member to NCTCOG training, 3rd party training, or on line training.',
      mgs: [
        {
          number: '2-F',
          title:
            'Training of City Employees – Illicit Discharges (if specific training is available)',
          description: 'Year 1-5: Each year send one employee to training.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '3-A',
      title:
        'Training of City Employees – BMP Inspections (if specific training is available)',
      startYear: 0,
      minimumControlMeasures: '3',
      description:
        'Send at least one inspector to one of the two inspector trainings performed by NCTCOG in regards to inspection of ' +
        'construction BMPs. The training will consist of assisting the cities with ideas and initiatives to more effectively ' +
        'inspect required BMPs during construction.',
      mgs: [
        {
          number: '3-A',
          title:
            'Training of City Employees – BMP Inspections (if specific training is available)',
          description:
            'Years 1 through 5 – Send at least one (1) inspector for training with NCTCOG each year. Training is available ' +
            'two (2) times per year. The number of individuals trained may be increased at any time.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '3-B',
      title: 'Continue to implement the Erosion and Sediment Control Ordinance',
      startYear: 0,
      minimumControlMeasures: '3',
      description: 'Continue to enforce the erosion control ordinance',
      mgs: [
        {
          number: '3-B',
          title:
            'Continue to implement the Erosion and Sediment Control Ordinance',
          description:
            'Year 1 –5: Continue to implement the erosion control ordinance.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '3-C',
      title: 'Continue to Inspect Construction Sites',
      startYear: 0,
      minimumControlMeasures: '3',
      description:
        'Establish a schedule with a frequency for inspecting construction sites, not just for construction purposes, ' +
        'but also for erosion control compliance.',
      mgs: [
        {
          number: '3-C',
          title: 'Continue to Inspect Construction Sites',
          description: 'Year 1 – 5: Continue to inspect construction sites.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '3-D',
      title: 'Continue to use the Standard Inspection Form',
      startYear: 0,
      minimumControlMeasures: '3,4',
      description:
        'Continue to use the standard inspection form that was developed during the previous permit year.',
      mgs: [
        {
          number: '3-D',
          title: 'Continue to use the Standard Inspection Form',
          description:
            'Year 1-5: Continue to use the standard inspection form, keep for annual reporting purposes',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '4-A',
      title: 'SmartScape™ Brochure Distribution',
      startYear: 0,
      minimumControlMeasures: '1,4',
      description:
        'See BMP 1-C for description and distribution practices for this BMP.',
      mgs: [],
    },
    {
      number: '4-B',
      title:
        'Establish a Link on the City’s website for “Storm Drain Information”',
      startYear: 0,
      minimumControlMeasures: '1,2,4',
      description: 'See BMP 1-F for description of this BMP.',
      mgs: [],
    },
    {
      number: '4-C',
      title: 'Review of Site Plan submittal and SWPPP prior to construction.',
      startYear: 0,
      minimumControlMeasures: '2,4',
      description: 'See BMP 2-D for description of this BMP.',
      mgs: [],
    },
    {
      number: '5-A',
      title: 'Training of City Employees – Municipal Pollution Prevention',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'If training is available, the City will send at least one inspector to trainings performed by NCTCOG ' +
        'in regards to municipal pollution prevention. The training will consist of assisting the cities with ' +
        'ideas and initiatives to more effectively prevent pollution during municipal operations.',
      mgs: [
        {
          number: '5-A',
          title: 'Training of City Employees – Municipal Pollution Prevention',
          description:
            'Years 1-5: send one staff member to training each year or use online training',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-B',
      title: 'Develop Maintenance Schedule for Municipal Inspections',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'Establish a schedule of fleet inspection, machinery inspection, cleaning of catch basins and ' +
        'municipally-owned inlets, and cleaning of the litter in the maintenance yard, park areas, and streets.',
      mgs: [
        {
          number: '5-B',
          title: 'Develop Maintenance Schedule for Municipal Inspections',
          description:
            'Year1-Year 5: Keep track of the maintenance activities and submit with the annual report.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-C',
      title:
        'Develop Maintenance Schedule for items discussed in MCM- Pollution Prevention / Good Housekeeping of Municipal Operations',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'Establish an inspection and maintenance schedule for the municipally-responsible structural control devices.',
      mgs: [
        {
          number: '5-C',
          title:
            'Develop Maintenance Schedule for items discussed in MCM- Pollution Prevention / Good Housekeeping of Municipal Operations',
          description:
            'Year 1-Year 5: Continue to implement the maintenance program and keep track of the activities for annual ' +
            'reporting purposes.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-D',
      title: 'Develop Proper Disposal Procedures for Waste',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'Establish procedures for picking up spills (fuel, oil, chemical) and disposing of waste (floatables, accumulated ' +
        'sediments, dredge soils).',
      mgs: [
        {
          number: '5-D',
          title: 'Develop Proper Disposal Procedures for Waste',
          description:
            'Year 1-Year 5: Continue to implement the proper disposal procedures and keep track of the procedures for annual ' +
            'reporting purposes.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-E',
      title: 'Develop Recycling Program for Municipal Operations',
      startYear: 0,
      minimumControlMeasures: '1,5',
      description:
        'Establish schedule, procedures, and contract for collecting and discarding of recyclables. It is possible to work with ' +
        'adjacent cities to accomplish these tasks.',
      mgs: [
        {
          number: '5-E',
          title: 'Develop Recycling Program for Municipal Operations',
          description:
            'Year 1-5: Continue to participate in the recycling program',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-F',
      title: 'Parking Lot and Street Cleaning',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'The City will clean municipally owned parking lots and streets.',
      mgs: [
        {
          number: '5-F',
          title: 'Parking Lot and Street Cleaning',
          description:
            'Year 1-Year 5 Continue to sweep and clean parking lots and streets',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-G',
      title: 'Storm Drain Cleaning',
      startYear: 0,
      minimumControlMeasures: '1,5',
      description:
        'The City will clean municipally owned storm drain inlets, manholes, and culverts.',
      mgs: [
        {
          number: '5-G',
          title: 'Storm Drain Cleaning',
          description:
            'Year 1- Year 5: Continue the storm drain cleaning program.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-H',
      title: 'Municipal Vehicle and Equipment Washing and Maintenance',
      startYear: 0,
      minimumControlMeasures: '1,2,5',
      description:
        'The City will evaluate the current methods used to wash and maintain City owned equipment, machinery, ' +
        'and motor vehicles. The City will provide training to appropriate staff to communicate and educate the ' +
        'importance of storm drain pollution and illicit discharges. City staff will be required to document BMP ' +
        'techniques when used for reporting purposes.',
      mgs: [
        {
          number: '5-H',
          title: 'Municipal Vehicle and Equipment Washing and Maintenance',
          description:
            'Year 1-Year 5 Provide training to appropriate City staff to educate them on BMPs for washing, servicing, ' +
            'and maintaining machinery, equipment, and motor vehicles.',
          yearsImplemented: '1,2,3,4,5',
        },
      ],
    },
    {
      number: '5-I',
      title:
        'Prepare an Inventory of Permittee-Owned Facilities and Control Inventory',
      startYear: 0,
      minimumControlMeasures: '5',
      description:
        'The permit requires the City to develop and maintain an inventory of facilities and stormwater controls.',
      mgs: [
        {
          number: '5-I',
          title:
            'Prepare an Inventory of Permittee-Owned Facilities and Control Inventory',
          description:
            'Years 4 through 5 – develop an inventory list and update it in Year 5',
          yearsImplemented: '4,5',
        },
      ],
    },
  ],
};
