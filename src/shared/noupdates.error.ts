export default class NoUpdatesError extends Error {
  public code: number;
  constructor(message: string) {
    super(message);
    this.code = 400;
    this.name = 'NoUpdatesError';
    this.stack = (<any>new Error()).stack;
  }
}
