export class EntityTypes {
  static BmpActivity = 'BmpActivity';
  static BmpDetail = 'BmpDetail';
  static CitizenReport = 'CitizenReport';
  static ConstructionSite = 'ConstructionSite';
  static ConstructionSiteInspection = 'ConstructionSiteInspection';
  static Facility = 'Facility';
  static FacilityInspection = 'FacilityInspection';
  static IllicitDischarge = 'IllicitDischarge';
  static MonitoringLocation = 'MonitoringLocation';
  static Outfall = 'Outfall';
  static OutfallInspection = 'OutfallInspection';
  static Structure = 'Structure';
  static StructureInspection = 'StructureInspection';
  static SwmpActivity = 'SwmpActivity';
}
