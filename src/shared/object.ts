/**
 * Creates a new class of type T based on an object.  This ensures that extra properties from obj are not set
 * into the T object.  Note that the class property has to be explicity set (e.g. `myProp: string = undefined`) for this to work.
 *
 * @export
 * @template T
 * @param {{ new (...args): T}} TheClass
 * @param {*} obj From object
 * @param {any} args additional arguments
 * @returns {T}
 */
export function createClassFromObject<T>(
  TheClass: { new (...args): T },
  obj: any,
  ...args
): T {
  let theClass = new TheClass(...args);
  Object.keys(obj).forEach(key => {
    if (theClass.hasOwnProperty(key)) {
      theClass[key] = obj[key];
    }
  });
  return theClass;
}

const nameExtractorRegex = /return (.*);/;
const nameExtractorRegexNoTrailingSemi = /return (.*)/;
/**
 * Get the name using a lambda so that you don't have magic strings
 * https://github.com/basarat/typescript-book/issues/33
 *
 */
export function getName(nameLambda: (args?: any) => any): string {
  let m = nameExtractorRegex.exec(nameLambda + '');
  if (m == null) {
    m = nameExtractorRegexNoTrailingSemi.exec(nameLambda + '');
    if (m == null) {
      throw new Error(
        "The function does not contain a statement matching 'return variableName'"
      );
    }
  }
  let access = m[1].split('.');
  let name = access[access.length - 1];

  // prevent trailing whitespace, '}'
  while (name[name.length - 1] === '}' || name[name.length - 1] === ' ') {
    name = name.substr(0, name.length - 1);
  }

  return name;
}

/**
 * Usage: propertyNameFor<Contact>(c => c.name)
 * returns 'name'
 *
 */
export function propertyNameFor<T>(lambda: (o: T) => any): string {
  return getName(lambda);
}

/**
 * usage:
 * let x = { foo: 10, bar: "hello!" };
 * let foo = getProperty(x, "foo"); // 10
 * let bar = getProperty(x, "bar"); // "hello"
 *
 */
export function getProperty<T, K extends keyof T>(obj: T, key: K) {
  return obj[key]; // Inferred type is T[K]
}

/**
 * Typesafe way to get property
 * usage:
 * getPropertyName<User>('firstName') // firstName
 *
 */
export function getPropertyName<T>(key: keyof T) {
  return key;
}
