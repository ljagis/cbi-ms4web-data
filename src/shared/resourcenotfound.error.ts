export default class ResourceNotFoundError extends Error {
  public code: number;
  constructor(message: string) {
    super(message);
    this.code = 404;
    this.name = 'ResourceNotFoundError';
    this.stack = (<any>new Error()).stack;
  }
}
