import { FileEntity, FileWithLinks } from './../models';

/**
 * Returns a {FileWithLinks} object from a {File}
 *
 * @export
 * @param {File} file
 * @param {number} assetId
 * @param {string} customerId
 * @param {string} rootUrl
 * @returns {FileWithLinks}
 */
export function fileToFileLink(
  file: FileEntity,
  customerId: string,
  rootUrl: string
): FileWithLinks {
  let fileWithLinks = file as FileWithLinks;
  fileWithLinks.resourceUrl = `${rootUrl}/api/files/${file.id}`;
  fileWithLinks.fileUrl = `${rootUrl}/api/staticfiles/${customerId}/${file.fileName}`;
  if (file.thumbnailName) {
    fileWithLinks.thumbnailUrl = `${rootUrl}/api/staticfiles/${customerId}/${file.thumbnailName}`;
  }
  return fileWithLinks;
}
