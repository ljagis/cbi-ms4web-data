import { Request } from 'express';

export function getUsername(req: Request) {
  let user = (<any>req).user;
  if (user) {
    return user.username;
  }
  throw Error('User object is undefined.  Login?');
}
