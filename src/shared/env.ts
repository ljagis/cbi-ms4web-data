export interface Env extends NodeJS.ProcessEnv {
  [key: string]: any;

  APP_PATH: string;

  DB_SERVER: string;
  DB_DATABASE: string;
  DB_USER: string;
  DB_PASSWORD: string;
  FILE_STORAGE_PATH: string;
  BROWSER_BASE_URL: string;

  // Node
  PORT: number;

  OKTA_GROUP_ID: string;
  OKTA_APPLICATION_ID: string;
  OKTA_APITOKEN: string;
  OKTA_ORG: string;

  DEBUG_KNEX: boolean;

  DARK_SKY_API_KEY: string;

  SENDGRID_API_KEY: string;
  MAIL_FROM: string;
  WEATHER_KIT_TOKEN: string;
}
