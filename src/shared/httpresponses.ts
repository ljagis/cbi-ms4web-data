import * as express from 'express';
import { HttpStatusCodes } from './httpstatuscodes';

export function okay(res: express.Response, content?: any): void {
  res.status(HttpStatusCodes.OK).json(content);
}

export function created(
  res: express.Response,
  content?: any,
  location?: string
): void {
  res
    .status(HttpStatusCodes.Created)
    .location(location)
    .json(content);
}

export function updated(res: express.Response, content: any) {
  res.status(HttpStatusCodes.OK).json(content);
}

export function forbidden(res: express.Response, content?: any) {
  res.status(HttpStatusCodes.Forbidden).json(content);
}

export function badRequest(res: express.Response, content?: any) {
  res.status(HttpStatusCodes.BadRequest).json(content);
}

export function serverError(res: express.Response): void {
  res.status(HttpStatusCodes.InternalServerError).json({
    errorMessage: 'Internal server error',
  });
}

export function notFound(res: express.Response): void {
  res.status(HttpStatusCodes.NotFound).json({
    errorMessage: 'Resource does not exist',
  });
}

export function methodNotAllowed(res: express.Response, message: string) {
  res.status(HttpStatusCodes.MethodNotAllowed).json({
    message: message,
  });
}
