export class Defaults {
  // Application wide max results from a sql query
  static QUERY_MAX_RECORD_COUNT = 20000;
  static THUMBNAIL_MAX_WIDTH = 300; // Use null or undefined to auto-scale the width to match the height
  static THUMBNAIL_MAX_HEIGHT = 300;

  static IMAGE_MAX_WIDTH = 1080;
  static IMAGE_MAX_HEIGHT = 1080;

  static LOGO_MAX_WIDTH = 250; // Use null or undefined to auto-scale the width to match the height
  static LOGO_MAX_HEIGHT = 250;

  // 0-100. The output quality to use for lossy JPEG, WebP and TIFF output formats. The default quality is 80.
  static THUMBNAIL_QUALITY = 70;
  static LOGO_QUALITY = 70;
  static IMAGE_QUALITY = 100;

  /**
   * An advanced setting for the zlib compression level of the lossless PNG output format. The default level is 6
   * http://sharp.readthedocs.io/en/stable/api/#compressionlevelcompressionlevel
   *
   */
  static THUMBNAIL_COMPRESSION_LEVEL = 9;
  static LOGO_COMPRESSION_LEVEL = 6;
}
