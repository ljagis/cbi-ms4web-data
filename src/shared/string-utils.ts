export function compareCaseInsensitive(string1: string, string2: string) {
  if (string1 === string2) {
    return true;
  }
  return string1 && string2 && string1.toUpperCase() === string2.toUpperCase();
}
