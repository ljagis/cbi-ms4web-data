import { groupBy } from 'lodash';

import { ComplianceStatus, Inspection } from '../models';
import { EntityTypes } from './entity-types';

export type ComplianceSummary = {
  complianceStatus: string;
  count: number;
  percents: string;
  level: number;
}[];

export const getComplianceSummary = (
  allInspections: Inspection[],
  type: EntityTypes,
  statuses: ComplianceStatus[]
) => {
  const inspections = allInspections.filter(
    inspection =>
      !inspection.scheduledInspectionDate || inspection.inspectionDate
  );
  const groupedInspections = groupBy(inspections, 'complianceStatus');

  return Object.keys(groupedInspections).reduce(
    (acc, key) => [
      ...acc,
      {
        complianceStatus: key,
        count: groupedInspections[key].length,
        percents: (
          groupedInspections[key].length /
          inspections.length *
          100
        ).toFixed(2),
        level: statuses.find(
          s =>
            s.entityType === type &&
            s.name === groupedInspections[key][0].complianceStatus
        ).level,
      },
    ],
    []
  );
};
