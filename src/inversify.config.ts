import * as Knex from 'knex';

import { APP_PATH, Env } from './shared';
import {
  AssetDetailController,
  AssetMapController,
  AssetSummaryReportController,
  BmpActivityController,
  BmpActivityFileController,
  BmpActivityLogController,
  BmpControlMeasureController,
  BmpDataTypeController,
  BmpDetailController,
  BmpDetailCustomFieldController,
  BmpDetailFileController,
  BmpTaskController,
  CalendarController,
  CitizenReportController,
  CitizenReportCustomFieldController,
  CitizenReportFiles,
  CommunityController,
  ConstructionSiteContactController,
  ConstructionSiteController,
  ConstructionSiteCustomFieldController,
  ConstructionSiteFilesController,
  ConstructionSiteInspectionController,
  ConstructionSiteInspectionCustomFieldController,
  ConstructionSiteInspectionFilesController,
  ContactController,
  ContactReportController,
  CurrentUserController,
  CustomFieldController,
  CustomFormTemplateController,
  CloneCustomFormTemplateController,
  CustomerController,
  CustomerExportController,
  ExportController,
  FacilityContactController,
  FacilityController,
  FacilityCustomFieldController,
  FacilityFilesController,
  FacilityInspectionController,
  FacilityInspectionCustomFieldController,
  FacilityInspectionFilesController,
  FileController,
  IllicitDischargeController,
  IllicitDischargeCustomFieldController,
  IllicitDischargeFiles,
  InspectionTypeController,
  LogoController,
  MeController,
  MetadataController,
  MonitoringLocationCustomFieldController,
  SWBNOProxyController,
  OutfallController,
  OutfallCustomFieldController,
  OutfallFilesController,
  OutfallInspectionController,
  OutfallInspectionCustomFieldController,
  OutfallInspectionFilesController,
  ProjectController,
  ReceivingWaterController,
  ReportsController,
  StructureContactController,
  StructureControlTypesController,
  StructureController,
  StructureCustomFieldController,
  StructureFilesController,
  StructureInspectionController,
  StructureInspectionCustomFieldController,
  StructureInspectionFilesController,
  UserController,
  WatershedController,
  WeatherController,
  SwmpController,
  SwmpActivityController,
  SwmpActivityFilesController,
  AnnualReportController,
  BmpController,
  MgController,
} from './controllers';
import {
  BmpActivityLogRepository,
  BmpActivityRepository,
  BmpControlMeasureRepository,
  BmpDataTypeRepository,
  BmpDetailRepository,
  BmpTaskRepository,
  CitizenReportRepository,
  CommunityRepository,
  ComplianceStatusRepository,
  ConstructionSiteInspectionRepository,
  ConstructionSiteInspectionSummaryRepository,
  ConstructionSiteRepository,
  ContactRepository,
  CountryRepository,
  CustomFieldRepository,
  CustomFormTemplateRepository,
  CustomerRepository,
  EmailRepository,
  FacilityInspectionRepository,
  FacilityInspectionSummaryRepository,
  FacilityRepository,
  FileRepository,
  IllicitDischargeRepository,
  InspectionFrequencyRepository,
  InspectionTypeRepository,
  MonitoringLocationRepository,
  OutfallInspectionRepository,
  OutfallInspectionSummaryRepository,
  OutfallMaterialRepository,
  OutfallRepository,
  OutfallTypeRepository,
  ProjectRepository,
  ReceivingWaterRepository,
  SectorRepository,
  StateRepository,
  StructureControlTypeRepository,
  StructureInspectionRepository,
  StructureInspectionSummaryRepository,
  StructureRepository,
  UserCustomerRepository,
  UserFullRepository,
  UserRepository,
  WatershedRepository,
  SwmpRepository,
  BmpRepository,
  MgRepository,
  SwmpActivityRepository,
  AnnualReportRepository,
  CustomerInspectionEmailRepository,
} from './data';
import {
  BmpActivitySummaryReport,
  DefaultPdfReport,
  IllicitDischargeReport,
  InspectionDetailsReport,
  InspectionSummaryReport,
  SwmpAnnualReport,
} from './reports';
import { Container, ContainerModule } from 'inversify';

import { Client } from '@okta/okta-sdk-nodejs';
import { CustomAuthProvider } from './services';
import { DarkSkyService } from './services/dark-sky.service';
import { WeatherKitService } from './services/weather-kit.service';
import { EmailCronJobRepository } from './email-cron-job';
import { PMISCronJobRepository } from './pmis-cron-job';
import { TYPES } from './di/types';

const container = new Container();

const env: Env = process.env as any;

let knex = Knex({
  client: 'mssql',
  debug: env.DEBUG_KNEX,
  pool: { min: 0, max: 7 },
  connection: {
    host: env.DB_SERVER,
    database: env.DB_DATABASE,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
  },
});

const oktaClient = new Client({
  orgUrl: env.OKTA_ORG,
  token: env.OKTA_APITOKEN,
});

container.bind<string>('applicationPath').toConstantValue(APP_PATH);
container.bind<Knex>(Knex).toConstantValue(knex);
container.bind<Client>(Client).toConstantValue(oktaClient);
container.bind(TYPES.DarkSkyApiKey).toConstantValue(env.DARK_SKY_API_KEY);
container.load(repositoryModule());

bindServices(container);
bindReports(container);

container.bind(CustomAuthProvider).toSelf();
container.bind(EmailCronJobRepository).toSelf();
container.bind(PMISCronJobRepository).toSelf();

export { container };

/* leave this here so that controller metadata can be generated
  see: https://github.com/inversify/inversify-express-utils#important-information-about-the-controller-decorator
*/
export const controllerClasses = [
  AssetDetailController,
  AssetMapController,
  AssetSummaryReportController,
  BmpActivityController,
  BmpActivityFileController,
  BmpActivityLogController,
  BmpControlMeasureController,
  BmpDataTypeController,
  BmpDetailController,
  BmpDetailCustomFieldController,
  BmpDetailFileController,
  BmpTaskController,
  CalendarController,
  CitizenReportController,
  CitizenReportCustomFieldController,
  CitizenReportFiles,
  CommunityController,
  ConstructionSiteContactController,
  ConstructionSiteController,
  ConstructionSiteCustomFieldController,
  ConstructionSiteFilesController,
  ConstructionSiteInspectionController,
  ConstructionSiteInspectionCustomFieldController,
  ConstructionSiteInspectionFilesController,
  ContactController,
  ContactReportController,
  CurrentUserController,
  CustomerController,
  CustomerExportController,
  CustomFieldController,
  CustomFormTemplateController,
  CloneCustomFormTemplateController,
  EmailRepository,
  ExportController,
  FacilityContactController,
  FacilityController,
  FacilityCustomFieldController,
  FacilityFilesController,
  FacilityInspectionController,
  FacilityInspectionCustomFieldController,
  FacilityInspectionFilesController,
  FileController,
  IllicitDischargeController,
  IllicitDischargeCustomFieldController,
  IllicitDischargeFiles,
  InspectionDetailsReport,
  InspectionTypeController,
  LogoController,
  MeController,
  MetadataController,
  MonitoringLocationCustomFieldController,
  OutfallController,
  OutfallCustomFieldController,
  OutfallFilesController,
  OutfallInspectionController,
  OutfallInspectionCustomFieldController,
  OutfallInspectionFilesController,
  ProjectController,
  ReceivingWaterController,
  ReportsController,
  StructureContactController,
  StructureController,
  StructureControlTypesController,
  StructureCustomFieldController,
  StructureFilesController,
  StructureInspectionController,
  StructureInspectionCustomFieldController,
  StructureInspectionFilesController,
  UserController,
  WatershedController,
  WeatherController,
  SWBNOProxyController,
  SwmpController,
  SwmpActivityController,
  SwmpActivityFilesController,
  AnnualReportController,
  BmpController,
  MgController,
];

function bindReports(c: Container) {
  c.bind(DefaultPdfReport).toSelf();
  c.bind(BmpActivitySummaryReport).toSelf();
  c.bind(InspectionSummaryReport).toSelf();
  c.bind(IllicitDischargeReport).toSelf();
  c.bind(SwmpAnnualReport).toSelf();
}

function bindServices(c: Container) {
  c.bind(DarkSkyService).toSelf();
  c.bind(WeatherKitService).toSelf();
}

function repositoryModule() {
  return new ContainerModule(bind => {
    bind(BmpActivityLogRepository).toSelf();
    bind(BmpActivityRepository).toSelf();
    bind(BmpControlMeasureRepository).toSelf();
    bind(BmpDataTypeRepository).toSelf();
    bind(BmpDetailRepository).toSelf();
    bind(BmpTaskRepository).toSelf();
    bind(CitizenReportRepository).toSelf();
    bind(CommunityRepository).toSelf();
    bind(ComplianceStatusRepository).toSelf();
    bind(ConstructionSiteInspectionRepository).toSelf();
    bind(ConstructionSiteRepository).toSelf();
    bind(ContactRepository).toSelf();
    bind(CountryRepository).toSelf();
    bind(CustomerRepository).toSelf();
    bind(CustomFieldRepository).toSelf();
    bind(CustomFormTemplateRepository).toSelf();
    bind(EmailRepository).toSelf();
    bind(FacilityInspectionRepository).toSelf();
    bind(FacilityRepository).toSelf();
    bind(FileRepository).toSelf();
    bind(IllicitDischargeRepository).toSelf();
    bind(InspectionFrequencyRepository).toSelf();
    bind(ConstructionSiteInspectionSummaryRepository).toSelf();
    bind(FacilityInspectionSummaryRepository).toSelf();
    bind(OutfallInspectionSummaryRepository).toSelf();
    bind(StructureInspectionSummaryRepository).toSelf();
    bind(InspectionTypeRepository).toSelf();
    bind(MonitoringLocationRepository).toSelf();
    bind(OutfallInspectionRepository).toSelf();
    bind(OutfallMaterialRepository).toSelf();
    bind(OutfallRepository).toSelf();
    bind(OutfallTypeRepository).toSelf();
    bind(ProjectRepository).toSelf();
    bind(ReceivingWaterRepository).toSelf();
    bind(SectorRepository).toSelf();
    bind(StateRepository).toSelf();
    bind(StructureControlTypeRepository).toSelf();
    bind(StructureInspectionRepository).toSelf();
    bind(StructureRepository).toSelf();
    bind(UserCustomerRepository).toSelf();
    bind(UserFullRepository).toSelf();
    bind(UserRepository).toSelf();
    bind(WatershedRepository).toSelf();
    bind(InspectionDetailsReport).toSelf();
    bind(SwmpRepository).toSelf();
    bind(BmpRepository).toSelf();
    bind(MgRepository).toSelf();
    bind(SwmpActivityRepository).toSelf();
    bind(AnnualReportRepository).toSelf();
    bind(CustomerInspectionEmailRepository).toSelf();
  });
}
