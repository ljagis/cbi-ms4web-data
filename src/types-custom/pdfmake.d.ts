// import pdfmake = require('pdfmake');

declare module 'pdfmake' {
  class PdfPrinter {
    constructor(fontDescriptors: any);

    createPdfKitDocument(docDefinition: any, options?: any): any;
  }
  export = PdfPrinter;
}
