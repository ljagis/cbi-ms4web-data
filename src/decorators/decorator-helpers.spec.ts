import * as chai from 'chai';
import { getSqlFields2, getJsonAttribute } from '../decorators';
import { ConstructionSite, Community, Customer } from '../models';

describe(`getTableName`, () => {
  it(`should return getTableName`, () => {
    chai.expect(ConstructionSite.getTableName()).equals('ConstructionSites');
  });
});

describe(`getSqlField`, () => {
  it(`should get Construction Site's SQL Field Name`, () => {
    const actual = ConstructionSite.getSqlField<ConstructionSite>(
      cs => cs.name
    );
    chai.expect(actual).equals('Name');
  });

  it(`should get Construction Site's SQL Table and Field Name`, () => {
    const actual = ConstructionSite.getSqlField<ConstructionSite>(
      cs => cs.name,
      true
    );
    chai.expect(actual).equals('ConstructionSites.Name');
  });

  it(`should get Construction Site's SQL Table and Field Name as property`, () => {
    const expected = ConstructionSite.getSqlField<ConstructionSite>(
      cs => cs.name,
      true,
      true
    );
    chai.expect(expected).equals('ConstructionSites.Name as name');
  });
});

describe(`getSqlField2`, () => {
  it(`should get Construction Site's SQL Field Names`, () => {
    const actual = getSqlFields2<ConstructionSite>(ConstructionSite);
    chai.expect(actual.indexOf('Name')).to.be.greaterThan(-1);
  });

  it(`should get Construction Site's SQL Field Names + Table`, () => {
    const actual = getSqlFields2<ConstructionSite>(ConstructionSite, {
      includeTable: true,
    });
    chai.expect(actual.indexOf('ConstructionSites.Name')).to.be.greaterThan(-1);
  });

  it(`should get Construction Site's SQL Field Names as property`, () => {
    const actual = getSqlFields2<ConstructionSite>(ConstructionSite, {
      asProperty: true,
    });
    chai.expect(actual.indexOf('Name as name')).to.be.greaterThan(-1);
  });

  it(`should get Community internal field`, () => {
    const actual = getSqlFields2<Community>(Community, {
      includeInternalField: true,
    });
    chai.expect(actual.indexOf('OriginalId')).to.be.greaterThan(-1);
  });

  it(`should NOT get Community internal field`, () => {
    const actual = getSqlFields2<Community>(Community);
    chai.expect(actual.indexOf('OriginalId')).to.eq(-1);
  });
});

describe('jsonAttribute', () => {
  it('should get json attribute', async () => {
    const isFeaturesJson = getJsonAttribute(Customer, 'features');
    const isIdJson = getJsonAttribute(Customer, 'id');

    chai.expect(isFeaturesJson).true;
    chai.expect(isIdJson).not.ok;
  });
});
