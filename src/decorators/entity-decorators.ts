import 'reflect-metadata';

export const jsonSymbol = Symbol('json');

/**
 * [Class Decorator] Define the table name
 */
export function tableName(name: string) {
  return (target: any) => Reflect.defineMetadata('tableName', name, target);
}

/**
 * [Property Decorator] Define the sql ID field
 */
export function idField() {
  return (target: any, property: string): any =>
    Reflect.defineMetadata('idField', property, target);
}

/**
 * [Property Decorator] Define the sql GlobalID field
 */
export function globalIdField() {
  return (target, property) => {
    return Reflect.defineMetadata('globalIdField', property, target);
  };
}

/**
 * [Property Decorator] Define the sql field name
 * @param fieldName SQL field name
 */
export function sqlField(sqlFieldName: string) {
  return (target: any, property: string, index?: number): any => {
    // store as `sqlField:property` on the prototype
    const val = sqlFieldName || property;
    Reflect.defineMetadata(`sqlField:${property}`, val, target);
    Reflect.defineMetadata('sqlField', val, target, property);
  };
}

/**
 * [Property Decorator] Mark this field as internal and it will not be selected for output
 */
export function internalField() {
  return (target, property) => {
    return Reflect.defineMetadata('internalField', property, target, property);
  };
}

/**
 * [Property Decorator] Marks this field as a JSON serializable field in SQL
 */
export function jsonAttribute() {
  return (target, property) => {
    return Reflect.defineMetadata(jsonSymbol, true, target, property);
  };
}
