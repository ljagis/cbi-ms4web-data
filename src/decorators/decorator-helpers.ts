import 'reflect-metadata';
import { propertyNameFor } from '../shared';
import { jsonSymbol } from './entity-decorators';

/**
 * Helpers for getting decorator metadata
 */

/**
 * Gets the SQL field name of an property
 *
 * @template T
 * @param {{ new (): T}} entity
 * @param {string} property the property on the Entity class
 * @param {boolean} [includeTable]
 * @returns {string}
 *
 */
export function getSqlField<T>(
  entity: { new (...args) },
  propertyOrLambda: keyof T | ((args?: T) => any) | string,
  includeTable?: boolean,
  asProperty?: boolean
): string {
  let property: string;
  if (propertyOrLambda instanceof Function) {
    property = propertyNameFor<T>(propertyOrLambda);
  } else {
    property = propertyOrLambda as string;
  }
  let sqlField = Reflect.getMetadata('sqlField', entity.prototype, property);
  sqlField = includeTable ? `${getTableName(entity)}.${sqlField}` : sqlField;
  return asProperty ? `${sqlField} as ${property}` : sqlField;
}

/**
 * Gets the table name from a class
 * @return {T} [description]
 */
export function getTableName<T>(entity: { new (...args): T }): string {
  return Reflect.getMetadata('tableName', entity);
}

/**
 * Gets the id field of Entity
 *
 * @template T
 * @param {{ new (): T}} entity
 * @param {boolean} [includeTable]
 * @returns {string}
 *
 */
export function getIdField<T>(entity: { new (...args): T }): string {
  return Reflect.getMetadata('idField', entity.prototype);
}

export function getGlobalIdField<T>(
  entity: { new (...args): T },
  includeTable?: boolean
): string {
  let globalIdField =
    Reflect.getMetadata('globalIdField', entity.prototype) || 'GlobalId';
  return includeTable
    ? `${getTableName(entity)}.${globalIdField}`
    : globalIdField;
}

/**
 * Gets SQL fields based on JS object properties
 *
 * @template T
 * @param {{ new (): T}} entity
 * @param {string[]} [properties]
 * @param {boolean} [includeTable] - include table name e.g. `ConstructionSites.Name`
 * @param {boolean} [asProperty] - alias field to property e.g. `Name as name`
 * @returns {string[]}
 *
 */
export function getSqlFields<T>(
  entity: { new (...args): T },
  properties?: string[],
  includeTable?: boolean,
  asProperty?: boolean
): string[] {
  let map = getSqlMapping(entity);
  let tableName = getTableName(entity);
  properties = properties || Object.keys(map);
  return properties.filter(p => !hasInternalField(entity, p)).map(p => {
    let select: string;
    if (includeTable) {
      select = `${tableName}.${map[p]}`;
    } else {
      select = `${map[p]}`;
    }
    if (asProperty) {
      select = `${select} as ${p}`;
    }
    return select;
  });
}

export function getSqlFields2<T>(
  entity: { new (...args): T },
  options: {
    properties?: string[];
    includeTable?: boolean;
    asProperty?: boolean;
    includeInternalField?: boolean;
  } = {}
): string[] {
  let map = getSqlMapping(entity);
  let tableName = getTableName(entity);
  const properties = options.properties || Object.keys(map);
  return properties
    .filter(p => options.includeInternalField || !hasInternalField(entity, p))
    .map(p => {
      let select: string;
      if (options.includeTable) {
        select = `${tableName}.${map[p]}`;
      } else {
        select = `${map[p]}`;
      }
      if (options.asProperty) {
        select = `${select} as ${p}`;
      }
      return select;
    });
}

/**
 * Returns an object {field:sqlField}
 *
 * @template T
 * @param {{ new (): T}} entity
 * @returns
 *
 */
export function getSqlMapping<T>(entity: {
  new (...args): T;
}): { [property: string]: string } {
  let decoratorKeys = Reflect.getMetadataKeys(entity.prototype);
  let mapping = decoratorKeys
    .filter((k: string) => k.startsWith('sqlField:'))
    .reduce((memo, sqlField) => {
      let property = sqlField.split(':')[1];
      memo[property] = Reflect.getMetadata(sqlField, entity.prototype);
      return memo;
    }, {});
  return mapping;
}

/**
 * Returns `true` if object has a sqlField decorator
 *
 * @template T
 * @param {{ new (): T}} entity
 * @param {string} property
 * @returns {boolean}
 *
 */
export function hasSqlField<T>(
  entity: { new (): T },
  property: string
): boolean {
  return !!Reflect.getMetadata('sqlField', entity.prototype, property);
}

export function hasInternalField<T>(
  entity: { new (): T },
  property: string
): boolean {
  return !!Reflect.getMetadata('internalField', entity.prototype, property);
}

export function hasIdField<T>(entity: { new (): T }): boolean {
  return !!Reflect.getMetadata('idField', entity.prototype);
}

export function hasGlobalIdField<T>(entity: { new (): T }): boolean {
  return !!Reflect.getMetadata('globalIdField', entity.prototype);
}

export function getJsonAttribute<T>(
  target: any,
  propertyKey: keyof T
): boolean {
  return !!Reflect.getMetadata(
    jsonSymbol,
    target.prototype,
    propertyKey as string
  );
}
