import * as Bluebird from 'bluebird';
import { AssetQueryOptions } from '../models';
import * as Knex from 'knex';

export interface FetchAsset<TAsset = any> {
  fetch(
    customerId: string,
    queryOptions?: AssetQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<TAsset[]>;

  fetchById(
    id: number,
    customerId: string,
    queryOptions?: AssetQueryOptions
  ): Bluebird<TAsset>;
}
