import * as Bluebird from 'bluebird';
import * as express from 'express';

import {
  APP_PATH,
  ClientError,
  NotFoundError,
  Role,
  badRequest,
  okay,
  forbidden,
} from '../shared';
import {
  BmpControlMeasure,
  Customer,
  CustomerResponse,
  UserCustomer,
} from '../models';
import {
  BmpControlMeasureRepository,
  ConstructionSiteRepository,
  CustomFormTemplateRepository,
  CustomerRepository,
  FacilityRepository,
  OutfallRepository,
  StructureRepository,
  UserCustomerRepository,
  UserRepository,
} from '../data';
import {
  controller,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Client as OktaClient } from '@okta/okta-sdk-nodejs';
import { authenticationRequired } from 'express-stormpath';
import { superRequired } from '../middleware';

@controller(`${APP_PATH}/api`)
export class CustomerController extends BaseController {
  constructor(
    private _bmpControlMeasureRepo: BmpControlMeasureRepository,
    private _cftRepo: CustomFormTemplateRepository,
    private _customerRepo: CustomerRepository,
    private _oktaClient: OktaClient,
    private _userRepo: UserRepository,
    private _ucRepo: UserCustomerRepository,
    private _csRepo: ConstructionSiteRepository,
    private _facRepo: FacilityRepository,
    private _outfallRepo: OutfallRepository,
    private _strucRepo: StructureRepository
  ) {
    super();
  }

  @httpGet('/customer', authenticationRequired)
  async customer(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const customer = await this._customerRepo.fetchById(customerId);
    const customerResponse = Object.assign(
      new CustomerResponse(),
      customer
    ) as CustomerResponse;
    if (customer.logoThumbnailName) {
      customerResponse.logoThumbnailLocation = `${rootUrl}/api/staticfiles/${
        customer.id
      }/${customer.logoThumbnailName}`;
    }
    okay(res, customerResponse);
  }

  @httpPut('/customer', authenticationRequired)
  async updateCustomer(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const { id, ...customerWithoutId } = req.body as Customer;
    const customer = await this._customerRepo.fetchById(customerId);
    if (!customer) {
      throw new NotFoundError(`${customerId} not found`);
    }
    await this._customerRepo.update(customerWithoutId, customerId);
    okay(res);
  }

  @httpPut('/customers/:id', authenticationRequired)
  async update(req: express.Request, res: express.Response) {
    const customerId: string = req.params.id;
    const user = await this._userRepo.fetchByEmail(req.user.email);
    const { id, ...customerWithoutId } = req.body as Customer;
    const customer = await this._customerRepo.fetchById(customerId);
    if (!customer) {
      throw new NotFoundError(`${customerId} not found`);
    }
    const customers = await this._customerRepo.fetchByUserId(user.id);
    if (!customers.find(f => f.id === customerId)) {
      throw new NotFoundError(`${customerId} not found`);
    }
    const returnedCustomer = await this._customerRepo.update(
      customerWithoutId,
      customerId
    );
    okay(res, returnedCustomer);
  }

  @httpGet('/customers', authenticationRequired)
  async index(req: express.Request, res: express.Response) {
    // no need to check req.user since `authenticationRequired` handles that
    const user = await this._userRepo.fetchByEmail(req.user.email);
    const customers = await this._customerRepo.fetchByUserId(user.id);
    const customersResponses = customers.map(c => {
      const customerResponse = Object.assign(
        new CustomerResponse(),
        c
      ) as CustomerResponse;
      if (c.logoThumbnailName) {
        customerResponse.logoThumbnailLocation = `${this.getAbsoluteAppRoot(
          req
        )}/api/staticfiles/${c.id}/${c.logoThumbnailName}`;
      }
      return customerResponse;
    });

    okay(res, customersResponses);
  }

  @httpPost('/customers', authenticationRequired, superRequired)
  async createCustomer(req: express.Request, res: express.Response) {
    const newCustomer = req.body as Customer;

    const existingCustomer = await this._customerRepo.fetchById(req.body.id);
    if (existingCustomer) {
      return badRequest(res, { message: `${req.body.id} exists.` });
    }
    const user = await this._userRepo.fetchByEmail(req.user.email);
    if (!user) {
      throw Error(`User not found`);
    }

    const createdCustomer = await this._customerRepo.create(newCustomer);
    await this._cftRepo.createDefaultFormTemplates(createdCustomer.id);

    // by default add current user to group.
    await this._ucRepo.addUserToCustomer(
      user.id,
      createdCustomer.id,
      Role.super
    );

    // Add Admins
    await this._addAdminToCustomerIfNotExists(
      'msimeon@ljaengineering.com',
      createdCustomer.id
    );
    await this._addAdminToCustomerIfNotExists(
      'tgarmon@ljaengineering.com',
      createdCustomer.id
    );
    await this._addAdminToCustomerIfNotExists(
      'anguyen@ljaengineering.com',
      createdCustomer.id
    );

    // create default BMP Control Measures
    await Bluebird.each(
      this._getDefaultControlMeasures(),
      async controlMeasure => {
        await this.validate(controlMeasure);
        await this._bmpControlMeasureRepo.insert(
          controlMeasure,
          newCustomer.id
        );
      }
    );

    // create sample assets
    await this._csRepo.createSampleConstructionSite(createdCustomer.id);
    await this._facRepo.createSampleFacility(createdCustomer.id);
    await this._outfallRepo.createSampleOutfall(createdCustomer.id);
    await this._strucRepo.createSampleStructure(createdCustomer.id);

    return this.created('', createdCustomer);
  }

  @httpGet(
    '/customers/:id/availableToAddUsers',
    authenticationRequired,
    superRequired
  )
  async availableToAddUsers(req: express.Request, res: express.Response) {
    const customerId = req.params.id;
    const users = await this._userRepo.fetchAvailableToAddByCustomer(
      customerId
    );
    okay(res, users);
  }

  @httpGet('/customers/:id/users', authenticationRequired)
  async customerUsers(req: express.Request, res: express.Response) {
    const customerId = req.params.id;
    const users = await this._userRepo.fetchByCustomer(customerId, {
      excludeSuperRole: req.query.excludeSuperRole === 'true',
    });
    okay(res, users);
  }

  /**
   * Adds user to group
   * url: /api/customers/:id/users
   * POST data: { userId: 1}
   */
  @httpPost('/customers/:id/users', authenticationRequired, superRequired)
  async addUserToGroup(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = req.params.id;
    const userCustomer = req.body as UserCustomer;
    userCustomer.customerId = customerId;
    if (!userCustomer.userId) {
      throw new ClientError(`userId is required.`);
    }
    if (!userCustomer.role) {
      throw new ClientError(`role is required.`);
    }
    const customer = await this._customerRepo.fetchById(customerId);
    if (!customer) {
      throw new ClientError(`Customer not found`);
    }
    const user = await this._userRepo.fetchById(userCustomer.userId);
    if (!user) {
      throw new ClientError(`User not found`);
    }
    await this._ucRepo.addUserToCustomer(
      userCustomer.userId,
      userCustomer.customerId,
      userCustomer.role
    );
    okay(res);
  }

  @httpDelete(
    '/customers/:id/users/:userId([0-9]+)',
    authenticationRequired,
    superRequired
  )
  async removeUserFromGroup(req: express.Request, res: express.Response) {
    const customerId = req.params.id;
    const userId = req.params.userId;

    const customerUsers = await this._ucRepo.fetchByUserId(userId);

    if (customerUsers.length > 1) {
      return forbidden(res);
    }

    // get user from db
    const user = await this._userRepo.fetchById(userId);
    if (!user) {
      throw new ClientError(`User id ${userId} not found in database`);
    }

    // get okta user
    const oktaUser = await this._oktaClient.getUser(user.email);

    await this._userRepo.removeUserFromCustomer(userId, customerId);

    // remove `currentCustomer` from customData
    if (oktaUser && oktaUser.profile.currentCustomer === customerId) {
      oktaUser.profile.currentCustomer = null;
      await oktaUser.update();
    }

    okay(res);
  }

  private _getDefaultControlMeasures(): BmpControlMeasure[] {
    const cm1 = new BmpControlMeasure();
    cm1.name = 'Public Education and Outreach';
    const cm2 = new BmpControlMeasure();
    cm2.name = 'Public Participation/ Involvement';
    const cm3 = new BmpControlMeasure();
    cm3.name = 'Illicit Discharge Detection and Elimination';
    const cm4 = new BmpControlMeasure();
    cm4.name = 'Construction Site Runoff Control';
    const cm5 = new BmpControlMeasure();
    cm5.name = 'Post-Construction Runoff Control';
    const cm6 = new BmpControlMeasure();
    cm6.name = 'Pollution Prevention/Good Housekeeping';
    return [cm1, cm2, cm3, cm4, cm5, cm6];
  }

  private async _addAdminToCustomerIfNotExists(
    email: string,
    customerId: string
  ) {
    const admin = await this._userRepo.fetchByEmail(email);
    if (admin) {
      const uc = await this._ucRepo.fetchByUserIdCustomerId(
        admin.id,
        customerId
      );
      if (uc) {
        return; // exists, do nothing
      }
      await this._ucRepo.addUserToCustomer(admin.id, customerId, Role.super);
    }
  }
}
