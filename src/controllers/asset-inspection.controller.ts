import * as _ from 'lodash';
import * as express from 'express';
import * as fs from 'fs';
import * as moment from 'moment';
import * as os from 'os';
import * as sendgrid from '@sendgrid/mail';

import {
  ConstructionSite,
  ConstructionSiteInspection,
  Customer,
  EmailEntity,
  Facility,
  FacilityInspection,
  FileWithLinks,
  Inspection,
  Outfall,
  OutfallInspection,
  QueryOptions,
  Structure,
  StructureInspection,
} from '../models';
import {
  ConstructionSiteInspectionRepository,
  ConstructionSiteRepository,
  ContactRepository,
  CustomFieldRepository,
  EmailRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  FileRepository,
  InspectionTypeRepository,
  OutfallInspectionRepository,
  OutfallRepository,
  StructureInspectionRepository,
  StructureRepository,
  UserRepository,
  CustomerInspectionEmailRepository,
  ComplianceStatusRepository,
  UserFullRepository,
} from '../data';
import {
  Env,
  NotFoundError,
  ClientError,
  createClassFromObject,
  created,
  fileToFileLink,
  notFound,
  okay,
  forbidden,
  filterComplianceStatus,
  Role,
} from '../shared';
import { inject, unmanaged } from 'inversify';

import { BaseController } from './base.controller';
import { FetchAsset } from '../interfaces';
import { InspectionDetailsReport } from '../reports';
import { getTempPath } from '../shared/path-utils';
import { isFileNameValid } from '../shared/file-name-utils';

// type Asset = ConstructionSite | Facility | Outfall | Structure;
type TypeOfAsset =
  | typeof ConstructionSite
  | typeof Facility
  | typeof Outfall
  | typeof Structure;
// type InspectionType = ConstructionSiteInspection | FacilityInspection | OutfallInspection | StructureInspection;
type TypeOfInspection =
  | typeof ConstructionSiteInspection
  | typeof FacilityInspection
  | typeof OutfallInspection
  | typeof StructureInspection;

export abstract class AssetInspectionController extends BaseController {
  env = process.env as Env;

  @inject(ConstructionSiteInspectionRepository)
  protected _constructionSiteInspectionRepo: ConstructionSiteInspectionRepository;

  @inject(FacilityInspectionRepository)
  protected _facilityInspectionRepo: FacilityInspectionRepository;

  @inject(OutfallInspectionRepository)
  protected _outfallInspectionRepo: OutfallInspectionRepository;

  @inject(StructureInspectionRepository)
  protected _structureInspectionRepo: StructureInspectionRepository;

  @inject(ConstructionSiteRepository)
  protected _constructionSiteRepo: ConstructionSiteRepository;

  @inject(FacilityRepository) protected _facilityRepo: FacilityRepository;

  @inject(OutfallRepository) protected _outfallRepo: OutfallRepository;

  @inject(StructureRepository) protected _structureRepo: StructureRepository;

  @inject(FileRepository) protected _fileRepo: FileRepository;

  @inject(EmailRepository) protected _emailRepo: EmailRepository;

  @inject(InspectionTypeRepository)
  protected _inspectionTypeRepo: InspectionTypeRepository;

  @inject(UserRepository) protected _userRepo: UserRepository;

  @inject(UserFullRepository) protected _userFullRepo: UserFullRepository;

  @inject(ContactRepository) protected _contactRepo: ContactRepository;

  @inject(CustomFieldRepository)
  protected _customFieldRepo: CustomFieldRepository;

  @inject(InspectionDetailsReport)
  protected _inspectionDetailsReport: InspectionDetailsReport;

  @inject(CustomerInspectionEmailRepository)
  protected _customerInspectionEmailRepo: CustomerInspectionEmailRepository;

  @inject(ComplianceStatusRepository)
  protected _complianceStatusRepo: ComplianceStatusRepository;

  /**
   * Creates an instance of AssetInspectionController.
   *
   * @param {string} _assetType - One of the values in `EntityTypes`
   *
   */
  constructor(
    @unmanaged() protected TAsset: TypeOfAsset,
    @unmanaged() protected TInspection: TypeOfInspection
  ) {
    super();
  }

  /**
   * Get inspections for an asset
   *
   * `GET api/{asset}/:constructionSiteId/inspections`
   *
   * @param {express.Request} req
   * ```
   * request.query properties:
   *  limit?: {number}
   *  offset?: {number}
   *  orderByField?: {string} field to order by
   *  orderByDirection?: {string} order direction
   * ```
   */
  async index(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const assetId = this._getAssetId(req.params);
    const queryOptions: QueryOptions = {
      limit: Number(req.query.limit),
      offset: Number(req.query.offset),
      orderByField: req.query.orderByField,
      orderByDirection: req.query.orderByDirection,
    };

    const inspections = await this._getInspectionRepo().fetch(
      customerId,
      assetId,
      queryOptions
    );

    okay(res, inspections);
  }

  async single(req: express.Request, res: express.Response) {
    const assetId = this._getAssetId(req.params);
    const id = Number(req.params.id); // Id of inspection
    const customerId = await this.getCustomerId(req);
    const inspection = await this._getInspectionRepo().fetchById(
      id,
      assetId,
      customerId
    );

    if (inspection) {
      okay(res, inspection);
    } else {
      notFound(res);
    }
  }

  async details(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const { assetKey } = this._getAssetInspectionType();
    const inspectionEmail = await this._customerInspectionEmailRepo.fetch(
      customerId,
      _.capitalize(assetKey)
    );
    const details = await this._getDetails(req);
    details['customEmailSubject'] = inspectionEmail
      ? inspectionEmail.subject
      : undefined;
    details['customEmailContent'] = inspectionEmail
      ? inspectionEmail.content
      : undefined;
    okay(res, details);
  }

  async pdf(req: express.Request, res: express.Response) {
    const customer: Customer = req.principal.selectedCustomer;
    const details = await this._getDetails(req);
    const complianceStatuses = await this._getComplianceStatuses(customer.id);
    const pdfDoc = await this._inspectionDetailsReport.generate(
      this.TAsset.name,
      details,
      customer,
      complianceStatuses
    );
    const tempPath = getTempPath();
    const assetKey =
      this.TAsset.name.charAt(0).toLowerCase() + this.TAsset.name.slice(1);
    const siteName = details[assetKey].name || details[assetKey].location || '';
    let formattedDate = '';
    const { inspectionDate, scheduledInspectionDate } = details[
      assetKey + 'Inspection'
    ];

    if (inspectionDate || scheduledInspectionDate) {
      let m = moment(new Date(inspectionDate || scheduledInspectionDate));
      if (customer.timezone) {
        m.tz(customer.timezone);
      }
      if (m.isValid) {
        formattedDate = m.format('MM-DD-YYYY');
      }
    }

    let outFile = isFileNameValid(siteName)
      ? `${siteName} Inspection`
      : `Inspection`;

    outFile = outFile + (formattedDate ? ` ${formattedDate}.pdf` : '.pdf');

    const finalOutFile = `${tempPath}/${outFile}`;

    const writePdf = async () => {
      return new Promise((resolve, reject) => {
        pdfDoc.on('error', err => {
          reject(err);
        });
        pdfDoc
          .pipe(fs.createWriteStream(finalOutFile))
          .on('finish', () => resolve());
        pdfDoc.end();
      });
    };

    await writePdf();

    return this.ok({
      fileName: outFile,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', outFile].join('/'),
    });
  }

  async multiPdf(req: express.Request, res: express.Response) {
    const customer: Customer = req.principal.selectedCustomer;
    const customerId = await this.getCustomerId(req);
    const assetId = this._getAssetId(req.params);
    const inspectionDetails = [];

    const inspections = await this._getInspectionRepo().fetch(
      customerId,
      assetId
    );

    const complianceStatuses = await this._getComplianceStatuses(customer.id);

    for (let i = 0; i < inspections.length; i++) {
      const details = await this._getDetails(req, inspections[i].id);
      inspectionDetails.push(details);
    }

    const pdfDoc = await this._inspectionDetailsReport.multiGenerate(
      this.TAsset.name,
      inspectionDetails,
      customer,
      complianceStatuses
    );
    const tempPath = getTempPath();
    const assetKey =
      this.TAsset.name.charAt(0).toLowerCase() + this.TAsset.name.slice(1);
    const siteName =
      inspectionDetails[0][assetKey].name ||
      inspectionDetails[0][assetKey].location ||
      '';
    const outFile = isFileNameValid(siteName)
      ? `${siteName} Inspections.pdf`
      : `Inspections.pdf`;
    const finalOutFile = `${tempPath}/${outFile}`;

    const writePdf = async () => {
      return new Promise((resolve, reject) => {
        pdfDoc.on('error', err => {
          reject(err);
        });
        pdfDoc
          .pipe(fs.createWriteStream(finalOutFile))
          .on('finish', () => resolve());
        pdfDoc.end();
      });
    };

    await writePdf();

    return this.ok({
      fileName: outFile,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', outFile].join('/'),
    });
  }

  async email(req: express.Request, res: express.Response) {
    const inspectionId = Number(req.params.id);
    const customerId = await this.getCustomerId(req);
    const customer: Customer = req.principal.selectedCustomer;
    const details = await this._getDetails(req);
    const complianceStatuses = await this._getComplianceStatuses(customer.id);
    const pdfDoc = await this._inspectionDetailsReport.generate(
      this.TAsset.name,
      details,
      customer,
      complianceStatuses
    );
    const buffer = await this._writeEmailPdf(pdfDoc);
    const sendGridEmail = this._formatSendGridEmail(
      req,
      details,
      customerId,
      buffer
    );
    sendgrid.setApiKey(this.env.SENDGRID_API_KEY);
    let sendGridResponse;
    try {
      [sendGridResponse] = await sendgrid.send(<any>sendGridEmail, false);
    } catch (error) {
      const sendGridErrors = _.get(error, 'response.body.errors');
      const errorResponse = sendGridErrors
        ? sendGridErrors.map(error => error.message).join(' ')
        : 'Error sending email';
      throw Error(errorResponse);
    }

    if (
      sendGridResponse.statusCode !== 202 &&
      sendGridResponse.statusCode !== 204
    ) {
      throw Error('Error sending email');
    }
    const emailRecord = await this._formatEmailRecord(req);
    const emailLog = await this._emailRepo.insert(
      this.TInspection,
      inspectionId,
      customerId,
      emailRecord
    );

    created(res, emailLog);
  }

  /**
   * Creates an inspection for an asset
   *
   * `POST /api/constructionsites/:constructionSiteId/inspections`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  async create(req: express.Request, res: express.Response) {
    let customerId = await this.getCustomerId(req);
    let inspection = createClassFromObject<Inspection>(
      this.TInspection,
      req.body
    );
    let assetId = this._getAssetId(req.params);
    // passed when a follow-up inspection is created
    const parentInspectionId = Number(req.query.parentInspectionId);

    if (this.TInspection === ConstructionSiteInspection) {
      (<ConstructionSiteInspection>inspection).constructionSiteId = assetId;
    } else if (this.TInspection === FacilityInspection) {
      (<FacilityInspection>inspection).facilityId = assetId;
    } else if (this.TInspection === OutfallInspection) {
      (<OutfallInspection>inspection).outfallId = assetId;
    } else if (this.TInspection === StructureInspection) {
      (<StructureInspection>inspection).structureId = assetId;
    } else {
      throw Error('Inspection not supported');
    }

    await this.validate(inspection);

    const asset = await (this._getAssetRepo() as FetchAsset<any>).fetchById(
      assetId,
      customerId
    );
    if (!asset) {
      throw new NotFoundError(`Asset Id ${assetId} not found`);
    }

    const insertedInspection = await this._getInspectionRepo().insert(
      inspection as any,
      customerId
    );
    await this._updateAssetLatestInspection(asset, customerId);

    // If a follow up inspection was created (scheduledInspection) update the parent inspection with a followupInspecitonId
    if (parentInspectionId) {
      const parentInspection = await this._getInspectionRepo().fetchById(
        parentInspectionId,
        assetId,
        customerId
      );

      if (!parentInspection) {
        throw new NotFoundError(`Inspection ${parentInspectionId} not found`);
      }

      let inspectionToUpdate = {
        ...new this.TInspection(),
        ...parentInspection,
        followUpInspectionId: insertedInspection.id,
      };
      await this.validate(inspectionToUpdate);
      await this._getInspectionRepo().update(
        parentInspectionId,
        inspectionToUpdate,
        customerId
      );
    }
    created(res, insertedInspection);
  }

  async update(req: express.Request, res: express.Response) {
    let customerId = await this.getCustomerId(req);
    let assetId = this._getAssetId(req.params);
    let inspectionId = Number(req.params.id);

    const user = await this._userFullRepo.fetchByEmailAndCustomer(
      req.user.email,
      customerId
    );
    const inspection = await this._getInspectionRepo().fetchById(
      inspectionId,
      assetId,
      customerId
    );

    if (!inspection) {
      throw new NotFoundError(`Inspection ${inspectionId} not found`);
    }

    if (
      user.role !== Role.super &&
      user.role !== Role.admin &&
      inspection.inspectorId !== user.id
    ) {
      return forbidden(res, {
        message: 'Only inspection creator can update the inspection',
      });
    }

    let inspectionToUpdate = Object.assign(
      new this.TInspection(),
      inspection,
      req.body
    );
    await this.validate(inspectionToUpdate);

    await this._getInspectionRepo().update(inspectionId, req.body, customerId);

    const asset = await (this._getAssetRepo() as FetchAsset<any>).fetchById(
      assetId,
      customerId
    );
    await this._updateAssetLatestInspection(asset, customerId);

    okay(res);
  }

  async del(req: express.Request, res: express.Response) {
    let assetId = this._getAssetId(req.params);
    const id = Number(req.params.id); // Id of inspection
    let customerId = await this.getCustomerId(req);

    await this._getInspectionRepo().deleteInspection(id, assetId, customerId);
    const asset = await (this._getAssetRepo() as FetchAsset<any>).fetchById(
      assetId,
      customerId
    );
    await this._updateAssetLatestInspection(asset, customerId);

    okay(res);
  }

  private async _getComplianceStatuses(customerId: string) {
    const complianceStatuses = await this._complianceStatusRepo.fetch();

    return complianceStatuses.filter(filterComplianceStatus(customerId));
  }

  private async _updateAssetLatestInspection(
    originalAsset: Structure | ConstructionSite | Facility | Outfall,
    customerId
  ) {
    const latestInspection = await this._getInspectionRepo().getLatestInspection(
      originalAsset.id,
      customerId
    );

    const updatedAsset = Object.assign({}, originalAsset, {
      latestInspectionId: latestInspection ? latestInspection.id : null,
      latestInspectionDate: latestInspection
        ? latestInspection.inspectionDate
        : null,
      complianceStatus: latestInspection
        ? latestInspection.complianceStatus
        : null,
    });

    await this._getAssetRepo().updateAsset(
      originalAsset.id,
      updatedAsset,
      customerId
    );
  }

  private _getAssetInspectionType() {
    let assetKey: string;
    let inspectionKey: string;

    if (this.TInspection === ConstructionSiteInspection) {
      inspectionKey = 'constructionSiteInspection';
      assetKey = 'constructionSite';
    } else if (this.TInspection === FacilityInspection) {
      inspectionKey = 'facilityInspection';
      assetKey = 'facility';
    } else if (this.TInspection === OutfallInspection) {
      inspectionKey = 'outfallInspection';
      assetKey = 'outfall';
    } else if (this.TInspection === StructureInspection) {
      inspectionKey = 'structureInspection';
      assetKey = 'structure';
    } else {
      throw Error('Inspection not supported');
    }

    return { assetKey, inspectionKey };
  }

  private async _getDetails(req: express.Request, curInspectionId?: number) {
    let customerId = await this.getCustomerId(req);
    let rootUrl = this.getAbsoluteAppRoot(req);
    let inspectionId = Number(req.params.id || curInspectionId);
    let assetId = this._getAssetId(req.params);
    let details: any = {};

    const { assetKey, inspectionKey } = this._getAssetInspectionType();
    const inspection = await this._getInspectionRepo().fetchById(
      inspectionId,
      assetId,
      customerId
    );
    if (!inspection) {
      throw new NotFoundError(`Inspection ${inspectionId} not found.`);
    }

    const previousInspection = await this._getInspectionRepo().getPreviousInspection(
      assetId,
      customerId,
      inspection.inspectionDate
    );

    details[inspectionKey] = inspection;
    details[assetKey] = await (this._getAssetRepo() as FetchAsset<
      any
    >).fetchById(assetId, customerId);

    details[assetKey]['previousInspectionDate'] = previousInspection
      ? previousInspection.inspectionDate
      : null;

    // followUpInspection
    if (inspection.followUpInspectionId > 0) {
      details.followUpInspection = await this._getInspectionRepo().fetchById(
        inspection.followUpInspectionId,
        assetId,
        customerId
      );
    } else {
      details.followUpInspection = null;
    }

    // parentInspection
    const parentInspections = await this._getInspectionRepo().fetchParentInspections(
      inspectionId,
      assetId,
      customerId
    );
    details.parentInspection = parentInspections[0] || null;

    // NOTE: Only CS, Facilities, and Structures have contacts
    if (
      this.TInspection === ConstructionSiteInspection ||
      this.TInspection === FacilityInspection ||
      this.TInspection === StructureInspection
    ) {
      details.contacts = await this._contactRepo.fetchContactsForEntity(
        this.TAsset,
        assetId,
        customerId,
        inspection.inspectionDate
      );
    }

    details.inspectors = await this._userRepo.fetch(customerId);
    const files = await this._fileRepo.fetch(
      this.TInspection,
      inspectionId,
      customerId
    );
    details.files = files.map<FileWithLinks>(file =>
      fileToFileLink(file, customerId, rootUrl)
    );

    details.emails = await this._emailRepo.fetch(
      this.TInspection,
      inspectionId,
      customerId
    );

    details.customFields = await this._customFieldRepo.getCustomFieldValuesForInspection(
      this.TAsset,
      this.TInspection,
      assetId,
      inspectionId,
      customerId
    );
    return details;
  }

  private async _writeEmailPdf(pdfDoc: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const chunks = [];
      pdfDoc.on('data', chunk => {
        chunks.push(chunk);
      });

      pdfDoc.on('error', err => {
        reject(err);
      });

      pdfDoc.on('end', async () => {
        resolve(Buffer.concat(chunks));
      });

      pdfDoc.end();
    });
  }

  private async _formatEmailRecord(req: express.Request) {
    const email = new EmailEntity();
    email.globalIdFk = req.params.id;
    email.customerId = await this.getCustomerId(req);
    (email.entityType = this.TInspection.name),
      (email.emailTo = Array.isArray(req.body.emailTo)
        ? req.body.emailTo.join('|')
        : req.body.emailTo);
    email.emailCC = Array.isArray(req.body.emailCC)
      ? req.body.emailCC.join('|')
      : req.body.emailCC;
    email.subject = req.body.subject;
    email.body = req.body.content;
    email.addedDate = new Date();
    email.addedBy = req.principal.details.username;

    return email;
  }

  private _formatSendGridEmail(
    req: express.Request,
    details: any,
    customerId: string,
    buffer: Buffer
  ) {
    const assetKey =
      this.TAsset.name.charAt(0).toLowerCase() + this.TAsset.name.slice(1);
    const siteName = details[assetKey].name || details[assetKey].location || '';
    const siteDate = details[assetKey + 'Inspection'].inspectionDate;
    const formattedDate = moment(new Date(siteDate)).format('MM-DD-YYYY');
    const fileName = isFileNameValid(siteName)
      ? `${siteName} Inspection ${formattedDate}.pdf`
      : `Inspection ${formattedDate}.pdf`;

    const checkForMultipleEmails = emails => {
      if (Array.isArray(emails)) {
        return emails.reduce(
          (array, email) => [...array, { email: email }],
          []
        );
      } else {
        return [{ email: emails }];
      }
    };
    // ([{email: 'exampleOne@mail.com'}, {email: 'exampleTwo@email.com'}], 'email')
    const emailTo = _.uniqBy(checkForMultipleEmails(req.body.emailTo), 'email');
    const emailCC = req.body.emailCC
      ? _.uniqBy(checkForMultipleEmails(req.body.emailCC), 'email')
      : '';
    const subject = req.body.subject;
    const content = [
      req.body.content,
      '----------', // tslint:disable-next-line
      'This email has been generated from the MS4web Stormwater compliance application. Please send any responses to the email address in the CC field.',
    ].join(`${os.EOL}${os.EOL}`);

    if (!Array.isArray(req.files)) {
      throw new ClientError(
        'Request files is not an array.  Please check your request parameters.'
      );
    }

    const files: Express.Multer.File[] = req.files;

    const toBase64 = (url: string, fileNameStr: string) => {
      try {
        const bitmap = fs.readFileSync(url);
        return new Buffer(bitmap).toString('base64');
      } catch {
        throw new Error(`Unable to read inspection file ${fileNameStr}`);
      }
    };

    let filesToAttach = details.files || [];
    // filter out image files, they are on the inspection pdf
    filesToAttach = filesToAttach.filter(file =>
      file.mimeType.indexOf('image')
    );
    const inspectionAttachments = filesToAttach.reduce((array: any, file) => {
      const url = `${this.env.FILE_STORAGE_PATH}/${customerId}/${
        file.fileName
      }`;
      const fileData = toBase64(url, file.fileName);
      return [
        ...array,
        {
          content: fileData,
          filename: file.originalFileName,
          type: file.mimeType,
        },
      ];
    }, []);

    const userAttachments = files.reduce((array: any, file) => {
      return [
        ...array,
        {
          content: file.buffer.toString('base64'),
          filename: file.originalname,
          type: file.mimetype,
        },
      ];
    }, []);

    const attachments = [
      {
        content: buffer.toString('base64'),
        filename: fileName,
        type: 'application/pdf',
      },
      ...userAttachments,
      ...inspectionAttachments,
    ];

    const sendGridEmail = {
      to: emailTo,
      cc: emailCC,
      subject: subject,
      from: this.env.MAIL_FROM,
      text: content,
      attachments,
    };
    return sendGridEmail;
  }

  private _getInspectionRepo() {
    if (this.TInspection === ConstructionSiteInspection) {
      return this._constructionSiteInspectionRepo;
    } else if (this.TInspection === FacilityInspection) {
      return this._facilityInspectionRepo;
    } else if (this.TInspection === OutfallInspection) {
      return this._outfallInspectionRepo;
    } else if (this.TInspection === StructureInspection) {
      return this._structureInspectionRepo;
    } else {
      throw Error('Inspection not supported');
    }
  }

  private _getAssetRepo() {
    if (this.TAsset === ConstructionSite) {
      return this._constructionSiteRepo;
    } else if (this.TAsset === Facility) {
      return this._facilityRepo;
    } else if (this.TAsset === Outfall) {
      return this._outfallRepo;
    } else if (this.TAsset === Structure) {
      return this._structureRepo;
    } else {
      throw Error('Asset not supported');
    }
  }

  private _getAssetId(params) {
    if (this.TInspection === ConstructionSiteInspection) {
      return Number(params.constructionSiteId);
    } else if (this.TInspection === FacilityInspection) {
      return Number(params.facilityId);
    } else if (this.TInspection === OutfallInspection) {
      return Number(params.outfallId);
    } else if (this.TInspection === StructureInspection) {
      return Number(params.structureId);
    } else {
      throw Error('Inspection not supported');
    }
  }
}
