import {
  controller,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';
import { APP_PATH } from '../shared/apppath';
import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import * as express from 'express';
import {
  CustomFormTemplateRepository,
  InspectionTypeRepository,
  CustomerRepository,
} from '../data';
import {
  okay,
  createClassFromObject,
  validateModel,
  created,
  NotFoundError,
  ClientError,
} from '../shared/index';
import { adminRequired } from '../middleware';
import {
  CustomFormTemplate,
  CustomFormTemplateWithCustomFields,
} from '../models/index';
import { LIMIT_MAX_ACTIVE_CUSTOM_FORM_TEMPLATES } from '../shared/plans';

const CONTROLLER_BASE_ROUTE = 'customformtemplates';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class CustomFormTemplateController extends BaseController {
  constructor(
    private _repo: CustomFormTemplateRepository,
    private _itRepo: InspectionTypeRepository,
    private _customerRepo: CustomerRepository
  ) {
    super();
  }
  @httpGet('/', authenticationRequired)
  async index(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const customFormTemplates = await this._repo.fetch(customerId);
    okay(res, customFormTemplates);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const id = +req.params.id;
    const customFormTemplateWithCustomFields = await this._repo.fetchById(
      customerId,
      id
    );
    okay(res, customFormTemplateWithCustomFields);
  }

  @httpPost('/', authenticationRequired, adminRequired)
  async create(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);

    const customFormTemplate = req.body as CustomFormTemplateWithCustomFields;

    const cft = createClassFromObject(CustomFormTemplate, customFormTemplate);
    await validateModel(cft);

    const customer = await this._customerRepo.fetchById(customerId);

    if (!customer.unlimitedCustomFormTemplates) {
      const count = await this._repo.getCount(
        customerId,
        customFormTemplate.entityType
      );
      if (count >= LIMIT_MAX_ACTIVE_CUSTOM_FORM_TEMPLATES) {
        // tslint:disable-next-line
        const message = `You have reached the maximum number of templates per asset (${LIMIT_MAX_ACTIVE_CUSTOM_FORM_TEMPLATES}) allowed on your plan.  Please contact support to upgrade.`;
        this.methodNotAllowed(res, message);
        return;
      }
    }

    const returnedCft = await this._repo.insertWithCustomFields(
      customFormTemplate,
      customerId
    );

    created(res, returnedCft);
  }

  @httpPut('/:id([0-9]+)/archive', authenticationRequired, adminRequired)
  async archive(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = +req.params.id;
    const existingCustomFormTemplate = await this._repo.fetchById(
      customerId,
      id
    );

    if (!existingCustomFormTemplate) {
      throw new NotFoundError();
    }

    // note: system custom form template can be archived, see Jira: MS4-473

    // find any potential inspection types this template uses
    const inspectionTypesWithCustomFormTemplate = await this._itRepo.fetchByCftId(
      customerId,
      id
    );
    const inspectionTypesNotArchived = inspectionTypesWithCustomFormTemplate.filter(
      it => !it.isArchived
    );

    if (inspectionTypesNotArchived.length !== 0) {
      const inspectionTypeNames = inspectionTypesNotArchived.map(it => it.name);
      throw new ClientError(
        `There are Inspection Types tied to this form:  ${inspectionTypeNames.join(
          ', '
        )}`
      );
    }

    await this._repo.archive(customerId, id);

    const archivedCustomFormTemplate = await this._repo.fetchById(
      customerId,
      id
    );
    return this.ok(archivedCustomFormTemplate);
  }
}
