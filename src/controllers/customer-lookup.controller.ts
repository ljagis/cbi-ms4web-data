import * as express from 'express';
import { BaseController } from './base.controller';
import {
  ContactRepository,
  CommunityRepository,
  CustomerLookupRepository,
  ProjectRepository,
  ReceivingWaterRepository,
  WatershedRepository,
} from '../data';
import {
  Contact,
  Community,
  Entity,
  Project,
  ReceivingWater,
  Watershed,
  QueryOptions,
} from '../models';
import { okay, created, notFound, NotFoundError } from '../shared';

import { inject } from 'inversify';

import { createClassFromObject } from '../shared';

export class CustomerLookupController extends BaseController {
  @inject(ContactRepository) protected _contactRepo: ContactRepository;

  @inject(CommunityRepository) protected _communityRepo: CommunityRepository;

  @inject(ProjectRepository) protected _projectRepo: ProjectRepository;

  @inject(ReceivingWaterRepository)
  protected _receivingWaterRepo: ReceivingWaterRepository;

  @inject(WatershedRepository) protected _wsRepo: WatershedRepository;

  constructor(protected LookupClass: { new (...args): Entity }) {
    super();
  }

  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    let entities: Entity[];
    const queryOptions: QueryOptions = {
      limit: Number(req.query.limit),
      offset: Number(req.query.offset),
      orderByField: req.query.orderByField,
      orderByDirection: req.query.orderByDirection,
    };
    if (this.LookupClass === Contact) {
      entities = await this._contactRepo.fetch(customerId, queryOptions);
    } else if (this.LookupClass === Community) {
      entities = await this._communityRepo.fetch(customerId, queryOptions);
    } else if (this.LookupClass === Project) {
      entities = await this._projectRepo.fetch(customerId, queryOptions);
    } else if (this.LookupClass === ReceivingWater) {
      entities = await this._receivingWaterRepo.fetch(customerId, queryOptions);
    } else if (this.LookupClass === Watershed) {
      entities = await this._wsRepo.fetch(customerId, queryOptions);
    } else {
      throw Error('Not implemented');
    }
    okay(res, entities);
  }

  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    let entity: Entity;
    if (this.LookupClass === Contact) {
      entity = await this._contactRepo.fetchById(id, customerId);
    } else if (this.LookupClass === Community) {
      entity = await this._communityRepo.fetchById(id, customerId);
    } else if (this.LookupClass === Project) {
      entity = await this._projectRepo.fetchById(id, customerId);
    } else if (this.LookupClass === ReceivingWater) {
      entity = await this._receivingWaterRepo.fetchById(id, customerId);
    } else if (this.LookupClass === Watershed) {
      entity = await this._wsRepo.fetchById(id, customerId);
    } else {
      throw Error('Not implemented');
    }

    if (entity) {
      okay(res, entity);
    } else {
      notFound(res);
    }
  }

  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    let entity: Entity;
    if (this.LookupClass === Contact) {
      const contact = createClassFromObject<Contact>(Contact, req.body);
      await this.validate(contact);
      entity = await this._contactRepo.insert(contact, currentCustomer);
    } else if (this.LookupClass === Community) {
      const commumity = createClassFromObject<Community>(Community, req.body);
      await this.validate(commumity);
      entity = await this._communityRepo.insert(commumity, currentCustomer);
    } else if (this.LookupClass === Project) {
      const project = createClassFromObject<Project>(Project, req.body);
      await this.validate(project);
      entity = await this._projectRepo.insert(project, currentCustomer);
    } else if (this.LookupClass === ReceivingWater) {
      const rw = createClassFromObject<ReceivingWater>(
        ReceivingWater,
        req.body
      );
      await this.validate(rw);
      entity = await this._receivingWaterRepo.insert(rw, currentCustomer);
    } else if (this.LookupClass === Watershed) {
      const ws = createClassFromObject<Watershed>(Watershed, req.body);
      await this.validate(ws);
      entity = await this._wsRepo.insert(ws, currentCustomer);
    } else {
      throw Error('Not implemented');
    }

    created(res, entity);
  }

  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    let lookupRepo: CustomerLookupRepository<any>;
    if (this.LookupClass === Contact) {
      lookupRepo = this._contactRepo;
    } else if (this.LookupClass === Community) {
      lookupRepo = this._communityRepo;
    } else if (this.LookupClass === Project) {
      lookupRepo = this._projectRepo;
    } else if (this.LookupClass === ReceivingWater) {
      lookupRepo = this._receivingWaterRepo;
    } else if (this.LookupClass === Watershed) {
      lookupRepo = this._wsRepo;
    } else {
      throw Error('Not implemented');
    }

    const c = lookupRepo.fetchById(id, customerId);
    if (!c) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    Object.assign(c, req.body);
    await this.validate(c);
    const entity = await lookupRepo.update(id, req.body, customerId);
    okay(res, entity);
  }

  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    const customerId = await this.getCustomerId(req);

    let lookupRepo: CustomerLookupRepository<any>;
    if (this.LookupClass === Contact) {
      lookupRepo = this._contactRepo;
    } else if (this.LookupClass === Community) {
      lookupRepo = this._communityRepo;
    } else if (this.LookupClass === Project) {
      lookupRepo = this._projectRepo;
    } else if (this.LookupClass === ReceivingWater) {
      lookupRepo = this._receivingWaterRepo;
    } else if (this.LookupClass === Watershed) {
      lookupRepo = this._wsRepo;
    } else {
      throw Error('Not implemented');
    }

    const l = await lookupRepo.fetchById(id, customerId);
    if (!l) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    await lookupRepo.del(id, customerId);
    okay(res, undefined);
  }
}
