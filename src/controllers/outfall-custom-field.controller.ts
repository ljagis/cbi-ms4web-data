import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityCustomFieldController } from './entity-custom-field.controller';

const CONTROLLER_ROUTE = 'outfalls';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class OutfallCustomFieldController extends EntityCustomFieldController {
  constructor() {
    super(EntityTypes.Outfall);
  }

  @httpGet('/:id([0-9]+)/customfields', authenticationRequired)
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpPut(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(req, res, next) {
    await super.update(req, res, next);
  }
}
