import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import { APP_PATH } from '../shared';
import * as express from 'express';
import { Project } from './../models';
import { CustomerLookupController } from './customer-lookup.controller';

import { adminRequired } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'projects';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class ProjectController extends CustomerLookupController {
  constructor() {
    super(Project);
  }

  @httpGet('/', authenticationRequired)
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.index.apply(this, arguments);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  indexAt() {
    return super.indexAt.apply(this, arguments);
  }

  @httpPost('/', authenticationRequired, adminRequired)
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.create.apply(this, arguments);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired, adminRequired)
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.update.apply(this, arguments);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  del(req: express.Request, res: express.Response, next: express.NextFunction) {
    return super.del.apply(this, arguments);
  }
}
