import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as moment from 'moment';
import * as models from '../models';
import * as shared from '../shared';
import * as Promise from 'bluebird';

chai.use(ChaiHttp);

let setupAssets = () => {
  let cs: Partial<models.ConstructionSite> = {
    trackingId: 'TEST 1',
    name: 'Test Construction Site',
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    projectId: null,
    projectType: models.projectTypes.commercial,
    permitStatus: models.permitStatus.approved,
    projectArea: 2.3,
    disturbedArea: 3.3,
    startDate: new Date(2016, 11, 1),
    estimatedCompletionDate: new Date(2017, 11, 1),
    latestInspectionDate: new Date(2016, 11, 1),
    completionDate: new Date(2017, 11, 2),
    projectStatus: models.projectStatus.active,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    npdes: true,
    usace404: false,
    state401: true,
    usacenwp: false,
    additionalInformation: 'Additional info...',
    complianceStatus: 'Compliant',
    lat: 33.003305,
    lng: -96.983596,
  };
  let cs2: Partial<models.ConstructionSite> = {
    trackingId: 'TEST 2',
    name: 'Test Construction Site 2',
    physicalAddress1: '123 Test 2',
    physicalAddress2: 'SUITE 2',
    physicalCity: 'Dallas',
    physicalState: 'CA',
    physicalZip: '77043',
    projectId: null,
    projectType: models.projectTypes.municipal,
    permitStatus: models.permitStatus.expired,
    projectArea: 2.4,
    disturbedArea: 3.4,
    startDate: new Date(2016, 11, 2),
    estimatedCompletionDate: new Date(2017, 11, 2),
    latestInspectionDate: new Date(2016, 11, 2),
    completionDate: new Date(2017, 11, 3),
    projectStatus: models.projectStatus.completed,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    npdes: false,
    usace404: true,
    state401: false,
    usacenwp: true,
    additionalInformation: 'Additional info 2...',
    complianceStatus: 'Verbal Warning',
    lat: 34.003305,
    lng: -95.983596,
  };

  let fac1: Partial<models.Facility> = {
    dateAdded: new Date(2016, 11, 1),
    name: 'City Hall',
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    complianceStatus: 'Compliant',
    lat: 33.047787,
    lng: -96.995385,
    additionalInformation: 'extra info',
    sector: 'City Facility',
    sicCode: 'SIC code',
    isHighRisk: true,
    inspectionFrequencyId: null,
    yearBuilt: 1999,
    squareFootage: 1.1,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 3),
  };
  let fac2: Partial<models.Facility> = {
    dateAdded: new Date(2016, 11, 2),
    name: 'City Hall 2',
    physicalAddress1: '123 Test 2',
    physicalAddress2: 'SUITE 12',
    physicalCity: 'Houston2',
    physicalState: 'CA',
    physicalZip: '77043',
    complianceStatus: 'No Status',
    lat: 32.047787,
    lng: -95.995385,
    additionalInformation: 'extra info 2',
    sector: 'Private Industrial Facility',
    sicCode: 'SIC code 2',
    isHighRisk: false,
    inspectionFrequencyId: null,
    yearBuilt: 2000,
    squareFootage: 2.22,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 4),
  };
  let outfall1: Partial<models.Outfall> = {
    dateAdded: new Date(2016, 11, 1),
    location: 'Raldon LK Park',
    nearAddress: 'Near Raldon LK Park',
    trackingId: '555',
    complianceStatus: 'Not Screened',
    lat: 33.032482,
    lng: -97.003106,
    condition: models.outfallConditions[0],
    material: 'Concrete',
    obstruction: models.obstructionSeverities[0],
    outfallType: 'Pipe',
    outfallSizeIn: 1.1,
    outfallWidthIn: 2.2,
    outfallHeightIn: 3.3,
    additionalInformation: 'add info',
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 4),
  };
  let outfall2: Partial<models.Outfall> = {
    dateAdded: new Date(2016, 11, 2),
    location: 'Raldon LK Park 2',
    nearAddress: 'Near Raldon LK Park 2',
    trackingId: '555-2',
    complianceStatus: 'Not Screened',
    lat: 34.032482,
    lng: -98.003106,
    condition: models.outfallConditions[1],
    material: 'Plastic',
    obstruction: models.obstructionSeverities[1],
    outfallType: 'Box',
    outfallSizeIn: 1.11,
    outfallWidthIn: 2.22,
    outfallHeightIn: 3.33,
    additionalInformation: 'add info 2',
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 3),
  };
  let structure1: Partial<models.Structure> = {
    dateAdded: new Date(2016, 11, 1),
    name: 'City of Lewisville',
    controlType: 'Regional Detention',
    trackingId: '111',
    projectId: null,
    projectType: models.projectTypes.commercial,
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    permitStatus: models.permitStatus.approved,
    inspectionFrequencyId: null,
    maintenanceAgreement: 'maint aggrement',
    complianceStatus: 'Compliant',
    lat: 33.057063,
    lng: -96.991065,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'This is a bunch of extra info',
    latestInspectionDate: new Date(2016, 11, 4),
  };
  let structure2: Partial<models.Structure> = {
    dateAdded: new Date(2016, 11, 2),
    name: 'City of Lewisville 2',
    controlType: 'Detention Pond',
    trackingId: '1112',
    projectId: null,
    projectType: models.projectTypes.municipal,
    physicalAddress1: '123 Test 2',
    physicalAddress2: 'SUITE 2',
    physicalCity: 'Houston2',
    physicalState: 'CA',
    physicalZip: '77043',
    permitStatus: models.permitStatus.expired,
    inspectionFrequencyId: null,
    maintenanceAgreement: 'maint aggrement 2',
    complianceStatus: 'No Status',
    lat: 32.057063,
    lng: -94.991065,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'This is a bunch of extra info 2',
    latestInspectionDate: new Date(2016, 11, 4),
  };
  return [
    {
      name: shared.EntityTypes.ConstructionSite,
      route: 'constructionsites',
      postData: cs,
      putData: cs2,
    },
    {
      name: shared.EntityTypes.Facility,
      route: 'facilities',
      postData: fac1,
      putData: fac2,
    },
    {
      name: shared.EntityTypes.Outfall,
      route: 'outfalls',
      postData: outfall1,
      putData: outfall2,
    },
    {
      name: shared.EntityTypes.Structure,
      route: 'structures',
      postData: structure1,
      putData: structure2,
    },
  ];
};

let assetInfos = setupAssets();

/**
 * GET index
 */
assetInfos.forEach(asset => {
  describe(`GET all ${asset.name}`, () => {
    it(`should GET ${asset.name}`, () => {
      return chai
        .request(config.baseUrl)
        .get(`/api/${asset.route}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
          chai.expect(res).to.be.json;
          chai.assert(Array.isArray(res.body));
        });
    });
  });
});

/**
 * CRUD tests
 */
assetInfos.forEach(assetInfo => {
  let createdId: number;

  describe(`CRUD ${assetInfo.name}`, () => {
    it(`should POST ${assetInfo.name}`, () => {
      return chai
        .request(config.baseUrl)
        .post(`/api/${assetInfo.route}`)
        .set('Cookie', config.cookie)
        .send(assetInfo.postData)
        .then(res => {
          createdId = +res.body.id;
          chai.expect(res).to.have.status(201);
          chai.expect(res).to.be.json;
          chai.expect(createdId).to.be.greaterThan(0);
        });
    });

    it(`should GET ${assetInfo.name} to verify created data`, () => {
      return chai
        .request(config.baseUrl)
        .get(`/api/${assetInfo.route}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
          chai.expect(res).to.be.json;

          Object.keys(assetInfo.postData).forEach(k => {
            if (assetInfo.postData[k] instanceof Date) {
              chai.assert(
                moment(assetInfo.postData[k]).isSame(moment(res.body[k]))
              );
            } else {
              chai.expect(res.body[k]).equals(assetInfo.postData[k]);
            }
          });
        });
    });

    it(`should PUT ${assetInfo.name}`, () => {
      return chai
        .request(config.baseUrl)
        .put(`/api/${assetInfo.route}/${createdId}`)
        .set('Cookie', config.cookie)
        .send(assetInfo.putData)
        .then(res => {
          chai.expect(res).to.have.status(200);
          chai.expect(res).to.be.json;
        });
    });

    it(`should GET ${assetInfo.name} to verify modified data`, () => {
      return chai
        .request(config.baseUrl)
        .get(`/api/${assetInfo.route}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
          chai.expect(res).to.be.json;

          Object.keys(assetInfo.putData).forEach(k => {
            if (assetInfo.putData[k] instanceof Date) {
              chai.assert(
                moment(assetInfo.putData[k]).isSame(moment(res.body[k]))
              );
            } else {
              chai.expect(res.body[k]).equals(assetInfo.putData[k]);
            }
          });
        });
    });

    // Details route
    it(`should check for ${assetInfo.route}/details/:id route`, () => {
      return chai
        .request(config.baseUrl)
        .get(`/api/${assetInfo.route}/details/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
          chai.expect(res).to.be.json;

          // individual entity
          if (assetInfo.name === shared.EntityTypes.CitizenReport) {
            chai.expect(res.body.citizenReport).to.not.be.null;
          }
          if (assetInfo.name === shared.EntityTypes.ConstructionSite) {
            chai.expect(res.body.constructionSite).to.not.be.null;
          }
          if (assetInfo.name === shared.EntityTypes.Facility) {
            chai.expect(res.body.facility).to.not.be.null;
          }
          if (assetInfo.name === shared.EntityTypes.IllicitDischarge) {
            chai.expect(res.body.illicitDischarge).to.not.be.null;
          }
          if (assetInfo.name === shared.EntityTypes.Outfall) {
            chai.expect(res.body.outfall).to.not.be.null;
          }
          if (assetInfo.name === shared.EntityTypes.Structure) {
            chai.expect(res.body.structure).to.not.be.null;
          }

          // inspections
          if (
            [
              shared.EntityTypes.ConstructionSite,
              shared.EntityTypes.Facility,
              shared.EntityTypes.Outfall,
              shared.EntityTypes.Structure,
            ].indexOf(assetInfo.name) > -1
          ) {
            chai.expect(res.body.inspections).to.be.instanceOf(Array);
          }

          // projects for CS and Structures
          if (
            [
              shared.EntityTypes.ConstructionSite,
              shared.EntityTypes.Structure,
            ].indexOf(assetInfo.name) > -1
          ) {
            chai.expect(res.body.projects).to.have.length.greaterThan(0);
          }

          // contacts
          if (
            [
              shared.EntityTypes.ConstructionSite,
              shared.EntityTypes.Facility,
              shared.EntityTypes.Structure,
            ].indexOf(assetInfo.name) > -1
          ) {
            chai.expect(res.body.contacts).to.be.instanceOf(Array);
          }

          // control types
          if (assetInfo.name === shared.EntityTypes.Structure) {
            chai.expect(res.body.controlTypes).to.have.length.greaterThan(0);
          }
          chai.expect(res.body.customFields).to.be.instanceOf(Array);
          chai.expect(res.body.files).to.be.instanceOf(Array);
          chai.expect(res.body.communities).to.have.length.greaterThan(0);
          chai.expect(res.body.receivingWaters).to.have.length.greaterThan(0);
          chai.expect(res.body.watersheds).to.have.length.greaterThan(0);
        });
    });

    it(`should DELETE ${assetInfo.name}`, () => {
      if (createdId > 0) {
        return chai
          .request(config.baseUrl)
          .del(`/api/${assetInfo.route}/${createdId}`)
          .set('Cookie', config.cookie)
          .then(res => {
            chai.expect(res).to.have.status(200);
          });
      }
      return Promise.resolve();
    });
  });
});
