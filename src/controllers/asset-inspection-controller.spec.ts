import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as moment from 'moment';
import * as models from '../models';
import * as shared from '../shared';
import { container } from '../inversify.config';
import {
  CustomFormTemplateRepository,
  InspectionTypeRepository,
} from '../data';
import { CustomFormTemplate, InspectionType } from '../models';
chai.use(ChaiHttp);

interface AssetInfo {
  name: string;
  baseRoute: string;
  assetId: number;
  postData: any;
  putData: any;
}
let createdId: number;
let inspectionInfos: AssetInfo[];
let formTemplates: CustomFormTemplate[];
let inspectionTypes: InspectionType[];

let setupAssets = () => {
  let csInspection: Partial<models.ConstructionSiteInspection> = {
    constructionSiteId: 4007,
    isSiteActive: true,
    isPlanOnSite: true,
    isSitePermitted: true,
    arePlanRecordsCurrent: true,
    areErosionControlsAcceptable: true,
    areStructuralControlsAcceptable: true,
    areTrackingControlsAcceptable: true,
    areOutfallVelocityControlsAcceptable: true,
    isWasteManagementAcceptable: true,
    isMaintenanceOfControlsAcceptable: true,
    areLocalControlsAcceptable: true,
    areStabilizationControlsAcceptable: true,
    areNonStormWaterControlsAcceptable: true,
    additionalInformation: 'inspection comments',
    dateResolved: new Date(2016, 10, 23),
    inspectionDate: new Date(2016, 10, 19),
    scheduledInspectionDate: new Date(2016, 10, 22),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,

    followUpInspectionId: null,

    // weather
    weatherCondition: 'Cloudy',
    weatherTemperatureF: 96.7,
    weatherPrecipitationIn: 1.1,
    windDirection: 'N',
    windSpeedMph: 2.2,
    weatherStationId: 'KHOU',
    weatherDateTime: new Date(2016, 10, 20),
  };
  let csInspection2: Partial<models.ConstructionSiteInspection> = {
    constructionSiteId: 4007,
    isSiteActive: false,
    isPlanOnSite: false,
    isSitePermitted: false,
    arePlanRecordsCurrent: false,
    areErosionControlsAcceptable: false,
    areStructuralControlsAcceptable: false,
    areTrackingControlsAcceptable: false,
    areOutfallVelocityControlsAcceptable: false,
    isWasteManagementAcceptable: false,
    isMaintenanceOfControlsAcceptable: false,
    areLocalControlsAcceptable: false,
    areStabilizationControlsAcceptable: false,
    areNonStormWaterControlsAcceptable: false,
    additionalInformation: 'inspection comments 2',
    dateResolved: new Date(2016, 10, 24),
    inspectionDate: new Date(2016, 10, 20),
    scheduledInspectionDate: new Date(2016, 10, 23),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,

    followUpInspectionId: null,

    // weather
    weatherCondition: 'Rainy',
    weatherTemperatureF: 96.8,
    weatherPrecipitationIn: 1.2,
    windDirection: 'S',
    windSpeedMph: 2.3,
    weatherStationId: 'KHOU2',
    weatherDateTime: new Date(2016, 10, 21),
  };

  let outfallInspection: Partial<models.OutfallInspection> = {
    daysSinceLastRain: 2,
    dischargeDescription: 'discharge desc',
    dryOrWetWeatherInspection: 'Dry',
    color: 'Normal',
    clarity: 'Clear',
    odor: 'Sewer',
    foam: 'Light',
    sheen: 'Moderate',
    sustainedSolids: 'Light',
    setSolids: 'Moderate',
    floatingSolids: 'Heavy',
    pH: 1.1,
    temperatureF: 99.2,
    domgl: 2,
    turbidity: 'turb',
    cond: 'cond',
    doSaturation: 2.1,
    flowrateGpm: 1.4,
    copper: 1.3,
    phenols: 2.3,
    ammonia: 3.2,
    detergents: 4.3,
    tpo4: 3.6,
    cl2: 1.2,
    bod: 1.1,
    cod: 4.3,
    tss: 23.4,
    no3: 11.2,
    fecalColiform: 5.5,
    ecoli: 1.23,
    additionalInformation: 'insp comment',

    inspectionDate: new Date(2016, 10, 20),
    scheduledInspectionDate: new Date(2016, 10, 23),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Rainy',
    weatherTemperatureF: 96.8,
    weatherPrecipitationIn: 1.2,
    windDirection: 'S',
    windSpeedMph: 2.3,
    weatherStationId: 'KHOU2',
    weatherDateTime: new Date(2016, 10, 21),
  };
  let outfallInspection2: Partial<models.OutfallInspection> = {
    daysSinceLastRain: 3,
    dischargeDescription: 'discharge desc 2',
    dryOrWetWeatherInspection: 'Wet',
    color: 'Brown',
    clarity: 'Murky',
    odor: 'Sulphur',
    foam: 'Moderate',
    sheen: 'Light',
    sustainedSolids: 'Heavy',
    setSolids: 'Light',
    floatingSolids: 'Moderate',
    pH: 1.12,
    temperatureF: 99.22,
    domgl: 22,
    turbidity: 'turb 2',
    cond: 'cond 2',
    doSaturation: 2.12,
    flowrateGpm: 1.42,
    copper: 1.32,
    phenols: 2.32,
    ammonia: 3.22,
    detergents: 4.32,
    tpo4: 3.62,
    cl2: 1.22,
    bod: 1.12,
    cod: 4.32,
    tss: 23.42,
    no3: 11.22,
    fecalColiform: 5.52,
    ecoli: 1.24,
    additionalInformation: 'insp comment 2',

    inspectionDate: new Date(2016, 10, 21),
    scheduledInspectionDate: new Date(2016, 10, 24),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Cloudy',
    weatherTemperatureF: 96.82,
    weatherPrecipitationIn: 1.22,
    windDirection: 'N',
    windSpeedMph: 2.2,
    weatherStationId: 'KHOU1',
    weatherDateTime: new Date(2016, 10, 22),
  };
  let facilityInspection: Partial<models.FacilityInspection> = {
    isFacilityActive: true,
    isFacilityPermitted: true,
    isSwppOnSite: true,
    areRecordsCurrent: true,
    areSamplingDataEvaluationAcceptable: true,
    isBmpAcceptable: true,
    isGoodHouseKeeping: true,
    isErosionPresent: true,
    areWashoutsPresent: true,
    isDownstreamErosionPresent: true,
    areFloatablesPresent: true,
    areIllicitDischargesPresent: true,
    areNonStormWaterDischargePresent: true,
    isReturnInspectionRequired: true,

    dateResolved: new Date(2016, 10, 22),

    additionalInformation: 'inspectionComments',

    inspectionDate: new Date(2016, 10, 20),
    scheduledInspectionDate: new Date(2016, 10, 23),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Rainy',
    weatherTemperatureF: 96.8,
    weatherPrecipitationIn: 1.2,
    windDirection: 'S',
    windSpeedMph: 2.3,
    weatherStationId: 'KHOU2',
    weatherDateTime: new Date(2016, 10, 21),
  };
  let facilityInspection2: Partial<models.FacilityInspection> = {
    isFacilityActive: false,
    isFacilityPermitted: false,
    isSwppOnSite: false,
    areRecordsCurrent: false,
    areSamplingDataEvaluationAcceptable: false,
    isBmpAcceptable: false,
    isGoodHouseKeeping: false,
    isErosionPresent: false,
    areWashoutsPresent: false,
    isDownstreamErosionPresent: false,
    areFloatablesPresent: false,
    areIllicitDischargesPresent: false,
    areNonStormWaterDischargePresent: false,
    isReturnInspectionRequired: false,

    dateResolved: new Date(2016, 10, 23),

    additionalInformation: 'inspectionComments 2',

    inspectionDate: new Date(2016, 10, 21),
    scheduledInspectionDate: new Date(2016, 10, 24),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Cloudy',
    weatherTemperatureF: 96.9,
    weatherPrecipitationIn: 1.3,
    windDirection: 'SE',
    windSpeedMph: 2.4,
    weatherStationId: 'KHOU1',
    weatherDateTime: new Date(2016, 10, 22),
  };
  let structureInspection: Partial<models.StructureInspection> = {
    isControlActive: true,
    isBuiltWithinSpecification: true,
    isDepthOfSedimentAcceptable: true,
    requiresMaintenance: true,
    requiresRepairs: true,
    isStandingWaterPresent: true,
    areWashoutsPresent: true,
    floatablesRemovalRequired: true,
    isOutletClogged: true,
    isStructuralDamage: true,
    isErosionPresent: true,
    isDownstreamErosionPresent: true,
    isIllicitDischargePresent: true,
    isReturnInspectionRecommended: true,

    dateResolved: new Date(2016, 10, 22),

    additionalInformation: 'inspectionComments',

    inspectionDate: new Date(2016, 10, 20),
    scheduledInspectionDate: new Date(2016, 10, 23),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Rainy',
    weatherTemperatureF: 96.8,
    weatherPrecipitationIn: 1.2,
    windDirection: 'S',
    windSpeedMph: 2.3,
    weatherStationId: 'KHOU2',
    weatherDateTime: new Date(2016, 10, 21),
  };
  let structureInspection2: Partial<models.StructureInspection> = {
    isControlActive: false,
    isBuiltWithinSpecification: false,
    isDepthOfSedimentAcceptable: false,
    requiresMaintenance: false,
    requiresRepairs: false,
    isStandingWaterPresent: false,
    areWashoutsPresent: false,
    floatablesRemovalRequired: false,
    isOutletClogged: false,
    isStructuralDamage: false,
    isErosionPresent: false,
    isDownstreamErosionPresent: false,
    isIllicitDischargePresent: false,
    isReturnInspectionRecommended: false,

    dateResolved: new Date(2016, 10, 23),

    additionalInformation: 'inspectionComments 2',

    inspectionDate: new Date(2016, 10, 21),
    scheduledInspectionDate: new Date(2016, 10, 24),
    complianceStatus: 'Compliant',
    inspectorId: 1,
    inspectorId2: 1,
    followUpInspectionId: null,

    // weather
    weatherCondition: 'Cloudy',
    weatherTemperatureF: 96.82,
    weatherPrecipitationIn: 1.22,
    windDirection: 'SE',
    windSpeedMph: 2.33,
    weatherStationId: 'KHOU',
    weatherDateTime: new Date(2016, 10, 22),
  };
  inspectionInfos = [
    {
      name: shared.EntityTypes.ConstructionSiteInspection,
      baseRoute: 'constructionsites',
      assetId: 4007,
      postData: csInspection,
      putData: csInspection2,
    },
    {
      name: shared.EntityTypes.OutfallInspection,
      baseRoute: 'outfalls',
      assetId: 3000,
      postData: outfallInspection,
      putData: outfallInspection2,
    },
    {
      name: shared.EntityTypes.FacilityInspection,
      baseRoute: 'facilities',
      assetId: 3000,
      postData: facilityInspection,
      putData: facilityInspection2,
    },
    {
      name: shared.EntityTypes.StructureInspection,
      baseRoute: 'structures',
      assetId: 3000,
      postData: structureInspection,
      putData: structureInspection2,
    },
  ];
};
setupAssets();

inspectionInfos.forEach(inspectionInfo => {
  describe(`CRUD ${inspectionInfo.name}`, async () => {
    const cftRepo = container.get(CustomFormTemplateRepository);
    const itRepo = container.get(InspectionTypeRepository);

    before(async () => {
      formTemplates = await cftRepo.fetch(config.customerId);
      inspectionTypes = await itRepo.fetch(config.customerId);
      inspectionInfo.postData.inspectionTypeId = inspectionTypes[0].id;
      inspectionInfo.postData.customFormTemplateId = formTemplates[0].id;
    });
    it(`should POST ${inspectionInfo.name}`, async () => {
      const res = await chai
        .request(config.baseUrl)
        .post(
          `/api/${inspectionInfo.baseRoute}/${
            inspectionInfo.assetId
          }/inspections`
        )
        .set('Cookie', config.cookie)
        .send(inspectionInfo.postData);
      createdId = +res.body.id;
      chai.expect(res).to.have.status(201);
      chai.expect(res).to.be.json;
      chai.expect(createdId).to.be.greaterThan(0);
    });

    it(`should GET ${inspectionInfo.name} to verify created data`, async () => {
      let url = `/api/${inspectionInfo.baseRoute}/${
        inspectionInfo.assetId
      }/inspections/${createdId}`;
      const res = await chai
        .request(config.baseUrl)
        .get(url)
        .set('Cookie', config.cookie);
      chai.expect(res).to.have.status(200);
      chai.expect(res).to.be.json;

      Object.keys(inspectionInfo.postData).forEach(k => {
        if (inspectionInfo.postData[k] instanceof Date) {
          chai.assert(
            moment(inspectionInfo.postData[k]).isSame(moment(res.body[k]))
          );
        } else {
          chai.expect(res.body[k]).equals(inspectionInfo.postData[k]);
        }
      });
    });

    // Details route
    it(`should check for details route`, async () => {
      let url = `/api/${inspectionInfo.baseRoute}/${
        inspectionInfo.assetId
      }/inspections/details/${createdId}`;
      const res = await chai
        .request(config.baseUrl)
        .get(url)
        .set('Cookie', config.cookie);
      chai.expect(res).to.have.status(200);
      chai.expect(res).to.be.json;

      // individual entity
      if (inspectionInfo.name === shared.EntityTypes.ConstructionSite) {
        chai.expect(res.body.constructionSite).to.not.be.null;
        chai.expect(res.body.constructionSiteInspection).to.not.be.null;
        chai.expect(res.body.inspectionTypes).to.have.length.greaterThan(0);
      }
      if (inspectionInfo.name === shared.EntityTypes.Facility) {
        chai.expect(res.body.facility).to.not.be.null;
        chai.expect(res.body.facilityInspection).to.not.be.null;
      }
      if (inspectionInfo.name === shared.EntityTypes.Outfall) {
        chai.expect(res.body.outfall).to.not.be.null;
        chai.expect(res.body.outfallInspection).to.not.be.null;
      }
      if (inspectionInfo.name === shared.EntityTypes.Structure) {
        chai.expect(res.body.structure).to.not.be.null;
        chai.expect(res.body.structureInspection).to.not.be.null;
      }

      // contacts
      if (
        [
          shared.EntityTypes.ConstructionSiteInspection,
          shared.EntityTypes.FacilityInspection,
          shared.EntityTypes.StructureInspection,
        ].indexOf(inspectionInfo.name) > -1
      ) {
        chai.expect(res.body.contacts).to.have.length.greaterThan(0);
      }

      chai.expect(res.body.inspectors).to.have.length.greaterThan(0);
      chai.expect(res.body.customFields).to.have.length.greaterThan(0);
      chai.expect(res.body.files).to.be.instanceof(Array);
      chai.expect(res.body).to.have.property('followUpInspection');
      chai.expect(res.body).to.have.property('parentInspection');
    });

    it(`should PUT ${inspectionInfo.name}`, async () => {
      let url = `/api/${inspectionInfo.baseRoute}/${
        inspectionInfo.assetId
      }/inspections/${createdId}`;
      const res = await chai
        .request(config.baseUrl)
        .put(url)
        .set('Cookie', config.cookie)
        .send(inspectionInfo.putData);
      chai.expect(res).to.have.status(200);
      chai.expect(res).to.be.json;
    });

    it(`should GET ${
      inspectionInfo.name
    } to verify modified data`, async () => {
      let url = `/api/${inspectionInfo.baseRoute}/${
        inspectionInfo.assetId
      }/inspections/${createdId}`;
      const res = await chai
        .request(config.baseUrl)
        .get(url)
        .set('Cookie', config.cookie);
      chai.expect(res).to.have.status(200);
      chai.expect(res).to.be.json;

      Object.keys(inspectionInfo.putData).forEach(k => {
        if (inspectionInfo.putData[k] instanceof Date) {
          chai.assert(
            moment(inspectionInfo.putData[k]).isSame(moment(res.body[k]))
          );
        } else {
          chai.expect(res.body[k]).equals(inspectionInfo.putData[k]);
        }
      });
    });

    it(`should DELETE ${inspectionInfo.name}`, async () => {
      if (createdId > 0) {
        let url = `/api/${inspectionInfo.baseRoute}/${
          inspectionInfo.assetId
        }/inspections/${createdId}`;
        const res = await chai
          .request(config.baseUrl)
          .del(url)
          .set('Cookie', config.cookie);
        chai.expect(res).to.have.status(200);
      }
    });
  });
});
