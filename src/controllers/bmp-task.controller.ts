import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  created,
  createClassFromObject,
  notFound,
  NotFoundError,
} from '../shared';
import * as express from 'express';
import { BaseController } from './base.controller';
import { BmpTask, QueryOptions } from '../models';
import { BmpTaskRepository } from '../data';
import { inject } from 'inversify';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'bmptasks';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpTaskController extends BaseController {
  @inject(BmpTaskRepository) protected _bmpTaskRepo: BmpTaskRepository;

  constructor() {
    super();
  }

  @httpGet('/', authenticationRequired, enforceFeaturePermission('bmp', 'view'))
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const queryOptions: QueryOptions = {
      limit: Number(req.query.limit),
      offset: Number(req.query.offset),
      orderByField: req.query.orderByField,
      orderByDirection: req.query.orderByDirection,
    };
    const items = await this._bmpTaskRepo.fetch(customerId, queryOptions);
    okay(res, items);
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const task = createClassFromObject<BmpTask>(BmpTask, req.body);
    await this.validate(task);
    const c = await this._bmpTaskRepo.insert(task, currentCustomer);
    created(res, c);
  }

  @httpGet(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'view')
  )
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const item = await this._bmpTaskRepo.fetchById(id, customerId);
    if (item) {
      okay(res, item);
    } else {
      notFound(res);
    }
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);

    const c = await this._bmpTaskRepo.fetchById(id, customerId);
    if (!c) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    Object.assign(c, req.body);
    await this.validate(c);
    await this._bmpTaskRepo.update(id, req.body, customerId);
    okay(res, undefined);
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    const customerId = await this.getCustomerId(req);
    const l = await this._bmpTaskRepo.fetchById(id, customerId);
    if (!l) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    await this._bmpTaskRepo.del(id, customerId);
    okay(res, undefined);
  }
}
