import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import * as express from 'express';
import { CustomerLookupController } from './customer-lookup.controller';

import { Watershed } from '../models';
import { APP_PATH } from '../shared';
import { adminRequired } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'watersheds';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class WatershedController extends CustomerLookupController {
  constructor() {
    super(Watershed);
  }

  @httpGet('/', authenticationRequired)
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.index.apply(this, arguments);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  indexAt() {
    return super.indexAt.apply(this, arguments);
  }

  @httpPost('/', authenticationRequired, adminRequired)
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.create.apply(this, arguments);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired, adminRequired)
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.update.apply(this, arguments);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  del(req: express.Request, res: express.Response, next: express.NextFunction) {
    return super.del.apply(this, arguments);
  }
}
