import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityCustomFieldController } from './entity-custom-field.controller';

const CONTROLLER_ROUTE = 'facilities';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class FacilityCustomFieldController extends EntityCustomFieldController {
  constructor() {
    super(EntityTypes.Facility);
  }

  @httpGet('/:id([0-9]+)/customfields', authenticationRequired)
  async index(req, res, next) {
    return super.index(req, res, next);
  }

  @httpPut(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(req, res, next) {
    return await super.update(req, res, next);
  }
}
