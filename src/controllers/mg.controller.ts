import { MgRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, okay, createClassFromObject } from '../shared';
import {
  controller,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { MG } from '../models';

const CONTROLLER_ROUTE = 'mg';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class MgController extends BaseController {
  constructor(private _mgRepository: MgRepository) {
    super();
  }

  @httpPost('/', authenticationRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const mg = createClassFromObject(MG, req.body);
    const createdMg = await this._mgRepository.create(mg);

    okay(res, createdMg);
  }

  @httpPut('/', authenticationRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const mg = createClassFromObject(MG, req.body);
    await this._mgRepository.update(mg);

    okay(res);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    await this._mgRepository.delete(id);

    okay(res);
  }
}
