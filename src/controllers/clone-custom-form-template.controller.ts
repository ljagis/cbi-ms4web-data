import { controller, httpPost } from 'inversify-express-utils';
import { omit } from 'lodash';
import { APP_PATH } from '../shared/apppath';
import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import * as express from 'express';
import {
  CustomFormTemplateRepository,
  InspectionTypeRepository,
} from '../data';
import { createClassFromObject, okay } from '../shared/index';
import { CustomFormTemplate } from '../models/index';

const CONTROLLER_BASE_ROUTE = 'clonecustomformtemplates';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class CloneCustomFormTemplateController extends BaseController {
  constructor(
    private _cftRepo: CustomFormTemplateRepository,
    private _itRepo: InspectionTypeRepository
  ) {
    super();
  }
  @httpPost('/', authenticationRequired)
  async clone(req: express.Request, res: express.Response) {
    const { sourceCustomer, targetCustomer } = req.body;

    const inspectionTypes = await this._itRepo.fetch(sourceCustomer);
    const customFormTemplates = await this._cftRepo.fetch(sourceCustomer);
    const customFormTemplatesWithCustomFields = await Promise.all(
      customFormTemplates.map(cft =>
        this._cftRepo.fetchById(sourceCustomer, cft.id)
      )
    );

    const returnedCfts = await Promise.all(
      customFormTemplatesWithCustomFields.map(cft => {
        const newCft = createClassFromObject(CustomFormTemplate, cft);

        return this._cftRepo.insertWithCustomFields(
          omit(
            {
              ...newCft,
              customFields: cft.customFields,
            },
            'id'
          ),
          targetCustomer
        );
      })
    );

    const customTemplateIdMap = customFormTemplatesWithCustomFields.reduce(
      (acc, cft, i) => ({ ...acc, [cft.id]: returnedCfts[i].id }),
      {}
    );

    const returnedInspectionTypes = await Promise.all(
      inspectionTypes.map(it =>
        this._itRepo.insert(targetCustomer, {
          ...it,
          customFormTemplateId: customTemplateIdMap[it.customFormTemplateId],
        })
      )
    );

    okay(res, {
      inspectionTypes: returnedInspectionTypes,
      customFormTemplates: returnedCfts,
    });
  }
}
