import { config } from '../test/helper';
import { container } from '../inversify.config';
import { BmpDetail, BmpControlMeasure } from '../models';
import { BmpDetailRepository, BmpControlMeasureRepository } from '../data';
import * as chai from 'chai';
import * as moment from 'moment';

const controllerRoute = 'bmpdetails';
const customerId = 'LEWISVILLE';

describe(`BMP Detail Controller`, async () => {
  const detailRepo = container.get(BmpDetailRepository);
  const cmRepo = container.get(BmpControlMeasureRepository);

  let createdId: number;
  let controlMeasures: BmpControlMeasure[];
  const bmpDetailName = `We will clean up stuff ${new Date().getTime()}`;

  before(async () => {
    // gets all control measures
    controlMeasures = await cmRepo.fetch(customerId);
  });

  after(async () => {
    if (createdId) {
      await detailRepo.delete(config.customerId, createdId, { force: true });
    }
  });

  it(`should POST`, async () => {
    const expectedDueDate = moment.utc('2017-09-01').toDate();
    const expectedCompletionDate = moment.utc('2017-09-02').toDate();
    const expectedDescription = 'description';
    const cm = controlMeasures[0];
    const postData: Partial<BmpDetail> = {
      controlMeasureId: cm.id,
      name: bmpDetailName,
      description: expectedDescription,
      dueDate: expectedDueDate,
      completionDate: expectedCompletionDate,
    };
    const response = await chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postData);

    const returnedData = response.body as BmpDetail;
    createdId = +response.body.id;
    chai.expect(response).to.have.status(201);
    chai.expect(response).to.be.json;

    chai.expect(returnedData.id).greaterThan(0);
    chai.expect(returnedData.name).eq(bmpDetailName);
    chai.expect(new Date(returnedData.dueDate)).eql(expectedDueDate);
    chai
      .expect(new Date(returnedData.completionDate))
      .eql(expectedCompletionDate);
  });

  it(`should GET all`, async () => {
    const response = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie);

    chai.expect(response).to.have.status(200);
    chai.expect(response).to.be.json;

    const dataType = (response.body as BmpDetail[]).find(
      t => t.id === createdId
    );
    chai.expect(dataType).ok;
    chai.expect(dataType.name).eq(bmpDetailName);
  });

  it(`should PUT`, async () => {
    const expectedDetail: Partial<BmpDetail> = {
      name: bmpDetailName + 'updated',
      dueDate: new Date(2018, 1, 1),
      completionDate: new Date(2019, 2, 2),
    };
    const resp = await chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(expectedDetail);

    const returnedData: BmpDetail = resp.body;

    chai.expect(resp).json;
    chai.expect(returnedData.name).eq(expectedDetail.name);
    chai.expect(new Date(returnedData.dueDate)).eql(expectedDetail.dueDate);
    chai
      .expect(new Date(returnedData.completionDate))
      .eql(expectedDetail.completionDate);
  });

  it(`should DELETE`, async () => {
    const resp = await chai
      .request(config.baseUrl)
      .del(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    const returnedDetail = await detailRepo.fetchById(customerId, createdId);
    chai.expect(resp).json;
    chai.expect(resp).status(200);
    chai.expect(returnedDetail).not.ok;
  });
});
