import * as express from 'express';

import {
  ComplianceStatus,
  InspectionFrequency,
  OutfallMaterial,
  OutfallType,
  Sector,
  State,
  obstructionSeverities,
  outfallConditions,
  permitStatus,
  projectStatus,
  projectTypes,
} from '../models';
import {
  ComplianceStatusRepository,
  CountryRepository,
  InspectionFrequencyRepository,
  OutfallMaterialRepository,
  OutfallTypeRepository,
  SectorRepository,
  StateRepository,
} from '../data';
import { controller, httpGet } from 'inversify-express-utils';

import { APP_PATH, filterComplianceStatus } from '../shared';
import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import { roleFeaturePermissions } from '../shared/roles';

@controller(`${APP_PATH}/api/metadata`, authenticationRequired)
export class MetadataController extends BaseController {
  private _usStates: State[];
  private _outfallMaterials: OutfallMaterial[];
  private _outfallTypes: OutfallType[];
  private _sectors: Sector[];
  private _inspectionFrequencies: InspectionFrequency[];
  private _complianceStatus: ComplianceStatus[];

  constructor(
    private _statesRepo: StateRepository,
    private _outfallMaterialRepo: OutfallMaterialRepository,
    private _outfallTypeRepo: OutfallTypeRepository,
    private _sectorRepo: SectorRepository,
    private _inspFreqRepo: InspectionFrequencyRepository,
    private _complianceStatusRepo: ComplianceStatusRepository,
    private _countryRepo: CountryRepository
  ) {
    super();
  }

  @httpGet('/')
  async index(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);

    // Note: I realize this controller is not a singleton
    if (this._usStates === undefined) {
      this._usStates = await this._statesRepo.fetch();
    }
    if (this._outfallMaterials === undefined) {
      this._outfallMaterials = await this._outfallMaterialRepo.fetch();
    }
    if (this._outfallTypes === undefined) {
      this._outfallTypes = await this._outfallTypeRepo.fetch();
    }
    if (this._sectors === undefined) {
      this._sectors = await this._sectorRepo.fetch();
    }
    if (this._inspectionFrequencies === undefined) {
      this._inspectionFrequencies = await this._inspFreqRepo.fetch();
    }
    if (this._complianceStatus === undefined) {
      this._complianceStatus = await this._complianceStatusRepo.fetch();
    }
    const countries = await this._countryRepo.fetch();
    const allStates = await this._statesRepo.fetchAllStates();

    const metadata = {
      complianceStatus: this._complianceStatus.filter(
        filterComplianceStatus(customerId)
      ),
      inspectionFrequencies: this._inspectionFrequencies,
      permitStatus,
      obstructionSeverities,
      outfallConditions,
      outfallMaterials: this._outfallMaterials,
      outfallTypes: this._outfallTypes,
      projectStatus,
      projectTypes,
      sectors: this._sectors,
      states: this._usStates,
      allStates,
      countries,
      roleFeaturePermissions,
    };
    return this.ok(metadata);
  }
}
