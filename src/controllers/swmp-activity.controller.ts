import {
  SwmpRepository,
  FileRepository,
  BmpRepository,
  MgRepository,
  SwmpActivityRepository,
  AnnualReportRepository,
  CustomerRepository,
  UserFullRepository,
} from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, okay, createClassFromObject, Role } from '../shared';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { groupBy } from 'lodash';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { SWMP, BMP, MG, SwmpActivity } from '../models';
import { fileToFileLink } from '../shared';

const CONTROLLER_ROUTE = 'swmpactivities';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class SwmpActivityController extends BaseController {
  constructor(
    private _swmpActivityRepository: SwmpActivityRepository,
    private _fileRepository: FileRepository,
    private _swmpRepository: SwmpRepository,
    private _bmpRepository: BmpRepository,
    private _mgRepository: MgRepository,
    private _annualReportRepository: AnnualReportRepository,
    private _customerRepository: CustomerRepository,
    private _userFullRepository: UserFullRepository
  ) {
    super();
  }

  @httpGet('/:id([0-9]+)/:reportId([0-9]+)', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const swmpId = Number(req.params.id);
    const reportId = Number(req.params.reportId);
    const customerId = await this.getCustomerId(req);
    const customer = await this._customerRepository.fetchById(customerId);
    const userCustomer = await this._userFullRepository.fetchByEmailAndCustomer(
      req.user.email,
      customerId
    );
    let swmpActivities = [];
    let mg: MG[] = [];
    let bmp: BMP[] = [];
    const swmp: SWMP = await this._swmpRepository.fetchByIdAndCustomer(
      swmpId,
      customerId
    );

    if (swmp) {
      swmpActivities = await this._swmpActivityRepository.fetchBySwmpIdAndCustomer(
        swmpId,
        customerId
      );

      bmp = await this._bmpRepository.fetchBySwmpId(swmpId);

      const sanitizedMeasurableGoals = (
        measurableGoals: MG[],
        bstMgtPracs: BMP
      ) => {
        return (measurableGoals || []).map(m => ({
          ...m,
          yearsImplemented: (m.yearsImplemented as string)
            .split(',')
            .map(p => parseInt(p, 10)),
          bmp: {
            id: bstMgtPracs.id,
            number: bstMgtPracs.number,
            title: bstMgtPracs.title,
            minimumControlMeasures: (bstMgtPracs.minimumControlMeasures as string).split(
              ','
            ),
          },
        }));
      };

      const mgs = await this._mgRepository.fetchByBmpIds(bmp.map(b => b.id));
      const groupedMgs = groupBy(mgs, 'bmpId');

      for (let i = 0; i < bmp.length; i++) {
        const assignedMgs = sanitizedMeasurableGoals(
          groupedMgs[bmp[i].id],
          bmp[i]
        );
        bmp[i].measurableGoals = assignedMgs;
        bmp[i].minimumControlMeasures = (bmp[i]
          .minimumControlMeasures as string).split(',');
        mg.push(...assignedMgs);
      }

      if (reportId) {
        const annualReports = await this._annualReportRepository.fetchByIdAndCustomer(
          reportId,
          customerId
        );
        const reportingYearOffset = annualReports.year - swmp.year + 1;

        bmp = bmp.filter(b => b.startYear <= reportingYearOffset);
        mg = mg.filter(m =>
          (m.yearsImplemented as number[]).includes(reportingYearOffset)
        );
      }
    }

    okay(res, {
      swmpActivities,
      customer: {
        id: customer.id,
        stateAbbr: customer.stateAbbr,
        fullName: req.user.fullName,
        annualReport: customer.features.swmpAnnualReport,
        admin:
          userCustomer.role === Role.super || userCustomer.role === Role.admin,
      },
      swmp,
      bmp,
      mg,
    });
  }

  @httpGet('/:id([0-9]+)/attachments', authenticationRequired)
  async attachments(req: Request, res: Response, next: NextFunction) {
    const swmpId = Number(req.params.id);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const customerId = await this.getCustomerId(req);

    const activities = await this._swmpActivityRepository.fetchBySwmpIdAndCustomer(
      swmpId,
      customerId
    );

    const fileRes = await Promise.all(
      activities.map(a =>
        this._fileRepository.fetch(SwmpActivity, a.id, customerId)
      )
    );

    const attachments = activities.reduce((acc, activity, index) => {
      const files = fileRes[index];
      const fileLinks = files.map(f => fileToFileLink(f, customerId, rootUrl));

      acc[activity.id] = fileLinks;

      return acc;
    }, {});

    okay(res, attachments);
  }

  @httpPost('/', authenticationRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmpActivity = createClassFromObject(SwmpActivity, req.body);
    swmpActivity.customerId = customerId;

    const newSwmp: SwmpActivity = await this._swmpActivityRepository.create(
      swmpActivity
    );

    okay(res, newSwmp.id);
  }

  @httpPut('/', authenticationRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmpActivity = createClassFromObject(SwmpActivity, req.body);

    await this._swmpActivityRepository.update(swmpActivity, customerId);

    okay(res, swmpActivity.id);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    await this._swmpActivityRepository.delete(id);

    okay(res);
  }
}
