import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { EntityTypes } from '../shared';

import { EntityCustomFieldController } from './entity-custom-field.controller';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_ROUTE = 'illicitdischarges';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class IllicitDischargeCustomFieldController extends EntityCustomFieldController {
  constructor() {
    super(EntityTypes.IllicitDischarge);
  }

  @httpGet(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpPut(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit')
  )
  async update(req, res, next) {
    await super.update(req, res, next);
  }
}
