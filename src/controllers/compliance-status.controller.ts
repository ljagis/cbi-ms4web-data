import { ComplianceStatusRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, badRequest, filterComplianceStatus, okay } from '../shared';
import {
  controller,
  httpPost,
  httpDelete,
  httpGet,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { ComplianceStatus } from '../models';
import { superRequired } from '../middleware';

const CONTROLLER_ROUTE = 'compliancestatus';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class ComplianceStatusController extends BaseController {
  constructor(private _cpsRepo: ComplianceStatusRepository) {
    super();
  }

  @httpGet('/', authenticationRequired, superRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const complianceStatuses = await this._cpsRepo.fetch();
    okay(res, complianceStatuses);
  }

  @httpPost('/', authenticationRequired, superRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const { name, entityType, level } = req.body;
    const complianceStatus: ComplianceStatus = {
      name,
      entityType,
      level,
      customerIds: '[]',
      excludeCustomerIds: '[]',
    };

    const createdComplianceStatus = await this._cpsRepo.create(
      complianceStatus
    );

    okay(res, createdComplianceStatus);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, superRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    await this._cpsRepo.delete(id);

    okay(res);
  }

  @httpGet('/customer/:id', authenticationRequired, superRequired)
  async customerComplianceStatus(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const customerId = req.params.id;
    const complianceStatuses = await this._cpsRepo.fetch();
    return complianceStatuses.filter(filterComplianceStatus(customerId));
  }

  @httpPost('/customer/:id', authenticationRequired, superRequired)
  async addCustomerComplianceStatus(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const customerId = req.params.id;
    const { name, entityType, level } = req.body;

    const complianceStatus = await this._cpsRepo.fetchByAttr({
      name,
      entityType,
      level,
    });

    if (!complianceStatus) {
      const newComplianceStatus: ComplianceStatus = {
        name,
        entityType,
        level,
        customerIds: `["${customerId}"]`,
        excludeCustomerIds: '[]',
      };

      const createdComplianceStatus = await this._cpsRepo.create(
        newComplianceStatus
      );

      return okay(res, createdComplianceStatus);
    }

    const customerIds: string[] = JSON.parse(complianceStatus.customerIds);
    const excludeCustomerIds: string[] = JSON.parse(
      complianceStatus.excludeCustomerIds
    );

    if (
      (!customerIds.length && !excludeCustomerIds.includes(customerId)) ||
      (customerIds.length && customerIds.includes(customerId))
    ) {
      return badRequest(res, { message: 'Cannot add duplicated status' });
    } else if (!customerIds.length && excludeCustomerIds.includes(customerId)) {
      const newExcludeCustomerIds = excludeCustomerIds.filter(
        c => c !== customerId
      );

      const newComplianceStatus: ComplianceStatus = {
        ...complianceStatus,
        excludeCustomerIds: JSON.stringify(newExcludeCustomerIds),
      };

      await this._cpsRepo.update(newComplianceStatus);

      return okay(res, newComplianceStatus);
    }

    customerIds.push(customerId);

    const newCS: ComplianceStatus = {
      ...complianceStatus,
      customerIds: JSON.stringify(customerIds),
    };

    await this._cpsRepo.update(newCS);

    okay(res, newCS);
  }

  @httpDelete(
    '/customer/:customerId/:id([0-9]+)',
    authenticationRequired,
    superRequired
  )
  async removeCustomerComplianceStatus(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const customerId = req.params.customerId;
    const complianceStatusId = req.params.id;

    const complianceStatus: ComplianceStatus = await this._cpsRepo.fetchSingle(
      complianceStatusId
    );

    let customerIds: string[] = JSON.parse(complianceStatus.customerIds);

    if (!customerIds.length) {
      const excludeCustomerIds: string[] = JSON.parse(
        complianceStatus.excludeCustomerIds
      );

      excludeCustomerIds.push(customerId);

      await this._cpsRepo.update({
        ...complianceStatus,
        excludeCustomerIds: JSON.stringify(excludeCustomerIds),
      });

      return okay(res);
    }

    const isExistingCustomer = customerIds.some(c => c === customerId);

    if (isExistingCustomer) {
      if (customerIds.length > 1) {
        customerIds = customerIds.filter(c => c !== customerId);

        await this._cpsRepo.update({
          ...complianceStatus,
          customerIds: JSON.stringify(customerIds),
        });
      } else {
        await this._cpsRepo.delete(complianceStatus.id);
      }
    }

    okay(res);
  }
}
