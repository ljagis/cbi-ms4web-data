import { config } from '../test/helper';
import { container } from '../inversify.config';
import { CustomFormTemplateWithCustomFields } from '../models';
import { CustomFormTemplateRepository } from '../data';
import * as chai from 'chai';

const customerId = config.customerId;

describe(`Custom Form Template Controller tests`, async () => {
  const baseRoute = `customformtemplates`;
  const time = new Date().getTime();

  const cftRepo = container.get(CustomFormTemplateRepository);
  let createdId: number;

  const postData: Partial<CustomFormTemplateWithCustomFields> = {
    entityType: 'ConstructionSiteInspection',
    name: 'CFT Test' + time,
    customFields: [
      {
        customFieldType: 'Customer',
        stateAbbr: null,
        entityType: 'ConstructionSiteInspection',
        dataType: 'number',
        inputType: 'text',
        fieldLabel: 'number Resolved',
        fieldOptions: null,
        fieldOrder: 3,
        sectionName: 'section 2',
        defaultValue: null,
      },
      {
        customFieldType: 'Customer',
        stateAbbr: null,
        entityType: 'ConstructionSiteInspection',
        dataType: 'Date',
        inputType: 'date',
        fieldLabel: 'Date Resolved',
        fieldOptions: null,
        fieldOrder: 15,
        sectionName: 'section 1',
        defaultValue: null,
      },
    ],
  };

  it(`should POST`, async () => {
    const response = await chai
      .request(config.baseUrl)
      .post(`/api/${baseRoute}`)
      .set('Cookie', config.cookie)
      .send(postData);

    const actualCft = response.body as CustomFormTemplateWithCustomFields;

    createdId = actualCft.id;

    checkKeys(actualCft);
    compareCustomField(postData, actualCft);

    chai.expect(actualCft.id).greaterThan(0);

    chai.expect(actualCft.customFields).to.be.an('array');
    chai.expect(actualCft.customFields.length).to.equal(2);

    chai.expect(actualCft.entityType).to.equal(postData.entityType);
    chai.expect(actualCft.name).to.equal(postData.name);

    chai.expect(response).to.have.status(201);
    chai.expect(response).to.be.json;
  });

  it(`should GET a single Custom Form Template`, async () => {
    const response = await chai
      .request(config.baseUrl)
      .get(`/api/${baseRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    const actualCft = response.body as CustomFormTemplateWithCustomFields;

    checkKeys(actualCft);
    compareCustomField(postData, actualCft);

    chai.expect(actualCft.id).to.equal(createdId);
    chai
      .expect(actualCft.customFields[0].customFormTemplateId)
      .to.equal(createdId);

    chai.expect(actualCft.customFields).to.be.an('array');
    chai.expect(actualCft.customFields.length).to.equal(2);

    chai.expect(response).to.have.status(200);
    chai.expect(response).to.be.json;
  });

  function compareCustomField(expected, actual) {
    chai
      .expect(actual.customFields[0].customFieldType)
      .to.equal(expected.customFields[0].customFieldType);
    chai
      .expect(actual.customFields[0].customFormTemplateId)
      .to.equal(actual.id);
    chai
      .expect(actual.customFields[0].dataType)
      .to.equal(expected.customFields[0].dataType);
    chai
      .expect(actual.customFields[0].entityType)
      .to.equal(expected.customFields[0].entityType);
    chai
      .expect(actual.customFields[0].fieldLabel)
      .to.equal(expected.customFields[0].fieldLabel);
    chai
      .expect(actual.customFields[0].fieldOptions)
      .to.equal(expected.customFields[0].fieldOptions);
    chai
      .expect(actual.customFields[0].fieldOrder)
      .to.equal(expected.customFields[0].fieldOrder);
    chai
      .expect(actual.customFields[0].inputType)
      .to.equal(expected.customFields[0].inputType);
    chai
      .expect(actual.customFields[0].sectionName)
      .to.equal(expected.customFields[0].sectionName);
    chai
      .expect(actual.customFields[0].stateAbbr)
      .to.equal(expected.customFields[0].stateAbbr);
  }

  function checkKeys(actual) {
    chai
      .expect(actual)
      .to.have.keys('customFields', 'entityType', 'id', 'name', 'isSystem');
    chai
      .expect(actual.customFields[0])
      .to.have.keys(
        'customFieldType',
        'customFormTemplateId',
        'dataType',
        'entityType',
        'fieldLabel',
        'fieldOptions',
        'fieldOrder',
        'id',
        'inputType',
        'sectionName',
        'stateAbbr',
        'defaultValue'
      );
    chai.expect(actual.customFields[0]).to.not.have.property('value');
  }

  after(async () => {
    if (createdId) {
      await cftRepo.delete(customerId, createdId, { force: true });
    }
  });
});
