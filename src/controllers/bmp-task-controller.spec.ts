import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as moment from 'moment';
import * as models from '../models';
import * as Promise from 'bluebird';
import * as _ from 'lodash';

chai.use(ChaiHttp);

let controllerRoute = 'bmptasks';
let postTask: Partial<models.BmpTask> = {
  controlMeasureId: 1,
  name: 'Flyer',
};
let putTask: Partial<models.BmpTask> = {
  controlMeasureId: null,
  name: 'Mailing',
};
let createdId: number;

describe(`CRUD BMP Tasks`, () => {
  it(`should POST BMP Task`, () => {
    return chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postTask)
      .then(res => {
        createdId = +res.body.id;
        chai.expect(res).to.have.status(201);
        chai.expect(res).to.be.json;
        chai.expect(createdId).to.be.greaterThan(0);
      });
  });

  it(`should GET BMP Task to verify created data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(postTask).forEach(k => {
          if (postTask[k] instanceof Date) {
            chai.assert(moment(postTask[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(postTask[k]);
          }
        });
      });
  });

  it(`should GET BMP Task index`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
        chai.expect(res.body).to.be.instanceof(Array);
        chai.expect(res.body).to.have.length.above(0);
        let cm = _.find(res.body, {
          id: createdId,
        });
        let actual = _.extend(
          {
            id: createdId,
          },
          postTask
        );
        chai.expect(cm).to.deep.equal(actual);
      });
  });

  it(`should PUT BMP Task`, () => {
    return chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(putTask)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });

  it(`should GET BMP Task to verify modified data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(putTask).forEach(k => {
          if (putTask[k] instanceof Date) {
            chai.assert(moment(putTask[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(putTask[k]);
          }
        });
      });
  });

  it(`should DELETE BMP Task`, () => {
    if (createdId > 0) {
      return chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
        });
    }
    return Promise.resolve();
  });
});
