import * as FormData from 'form-data';

import { APP_PATH, okay } from '../shared';
import { Request, Response } from 'express';
import { controller, httpGet } from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import axios from 'axios';

let token: { token: string; expires: number };

const {
  NOLA_GIS_SERVER,
  NOLA_SERVICE_USER,
  NOLA_SERVICE_PASSWORD,
  NOLA_SERVICE_REFERER,
} = process.env;

/**
 * SWBNO does not have an internal ArcGIS Enterprise installation.
 * LJA is hosting their map service. Service also requires a token to access.
 * Special custom proxy just for SWBNO to access their service without the client knowing the server credentials.
 * Map server for SWBNO should be mapped to https://stormwater.ms4web.com/v1/api/swbno
 */
@controller(`${APP_PATH}/api/swbno/*`)
export class SWBNOProxyController extends BaseController {
  @httpGet('/', authenticationRequired)
  async current(req: Request, res: Response) {
    const extendedPath = req.originalUrl.match(/\/api\/swbno(.*)/)[1];

    const formData = new FormData();
    formData.append('username', NOLA_SERVICE_USER);
    formData.append('password', NOLA_SERVICE_PASSWORD);
    formData.append('client', 'referer');
    formData.append('referer', NOLA_SERVICE_REFERER);
    formData.append('expiration', 60);
    formData.append('f', 'json');

    const expireCheck = new Date().getTime() - 600000; // current time - 10 min
    if (!token || token.expires < expireCheck) {
      const authResponse = await axios({
        method: 'post',
        url: `${NOLA_GIS_SERVER}/portal/sharing/rest/generateToken`,
        data: formData,
        headers: formData.getHeaders(),
      });
      token = authResponse.data;
    }

    const mapServerResponse = await axios({
      method: 'get',
      url: `${NOLA_GIS_SERVER}/arcgis/rest/services/CBI/CBI_SWBNO/MapServer${extendedPath}&token=${
        token.token
      }`,
    });

    okay(res, mapServerResponse.data);
  }
}
