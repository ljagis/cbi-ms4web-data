import { Request } from 'express';
import * as multer from 'multer';
import * as fs from 'fs';
import * as uuid from 'uuid';
import * as path from 'path';
import { getCustomerId } from '../../shared/get-customer-id';

let storage = multer.diskStorage({
  destination(req: Request, file, cb) {
    getCustomerId(req).then(customerId => {
      let customerPath = `${process.env.FILE_STORAGE_PATH}/${customerId}`;
      if (!fs.existsSync(customerPath)) {
        fs.mkdirSync(customerPath);
      }
      cb(null, customerPath);
    });
  },
  filename(req, file, cb) {
    let unique = uuid.v4();
    let extension = path.extname(file.originalname);
    cb(null, `${unique}${extension}`);
  },
});
export const defaultMulterInstance = multer({
  storage: storage,
  fileFilter(req, file, cb) {
    let validExtensions = [
      // images
      'jpg',
      'jpeg',
      'png',
      'gif',
      'ico',

      // documents
      'pdf',
      'doc',
      'docx',
      'ppt',
      'pptx',
      'pps',
      'ppsx',
      'odt',
      'xls',
      'xlsx',
      'psd',

      // audio
      'mp3',
      'm4a',
      'ogg',
      'wav',

      // video
      'mp4',
      'm4v',
      'mov',
      'wmv',
      'avi',
      'mpg',
      'ogv',
      '3gp',
      '3g2',
    ];
    let ext = path
      .extname(file.originalname)
      .replace('.', '')
      .toLowerCase();
    let isValid = validExtensions.indexOf(ext) >= 0;
    if (isValid) {
      cb(null, true);
    } else {
      cb(new Error(`File format ${ext} is not acceptable`), false);
    }
  },
});
