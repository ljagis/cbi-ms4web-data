import * as multer from 'multer';

export const memoryMulterInstance = multer({
  storage: multer.memoryStorage(),
});
