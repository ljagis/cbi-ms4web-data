import {
  CustomerRepository,
  UserRepository,
  UserFullRepository,
  UserCustomerRepository,
} from '../data';
import { authenticationRequired } from 'express-stormpath';
import {
  APP_PATH,
  okay,
  created,
  forbidden,
  NotFoundError,
  plans,
  ClientError,
} from '../shared';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { User, Principal } from '../models';
import { AccountData } from 'stormpath';
import * as Bluebird from 'bluebird';
import { adminRequired } from '../middleware';

const CONTROLLER_ROUTE = 'users';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class UserController extends BaseController {
  constructor(
    private _customerRepository: CustomerRepository,
    private _userRepo: UserRepository,
    private _userFullRepo: UserFullRepository,
    private _ucRepo: UserCustomerRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    let users = await this._userFullRepo.fetchByCustomer(customerId);
    okay(res, users);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const user = await this._userFullRepo.fetchByIdAndCustomer(id, customerId);
    okay(res, user);
  }

  @httpPost('/', authenticationRequired, adminRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    // Check user count before user creation
    if (req.body.status === 'ENABLED') {
      const canEnableUser = await this._canEnableUser(
        req.principal,
        customerId
      );
      if (!canEnableUser) {
        return forbidden(res, {
          message: 'You have too many active users for your current plan',
        });
      }
    }

    const accountData = req.body as AccountData;
    const { role } = req.body;

    // check for existing user
    const existingUser = await this._userRepo.fetchByEmail(accountData.email);
    if (existingUser) {
      throw new ClientError(`${accountData.email} already exists`);
    }

    accountData.username = accountData.email;
    accountData.customData = {};
    const user = Object.assign(new User(), accountData) as User;

    await this.validate(user);
    const insertedUser = await this._userRepo.insert(user);

    await this._ucRepo.addUserToCustomer(insertedUser.id, customerId, role);

    created(res, insertedUser);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired, adminRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const { role } = req.body;
    const dbUser = await this._userFullRepo.fetchByIdAndCustomer(
      id,
      customerId
    );
    // Check if user can be changed
    if (dbUser.status !== req.body.status && req.body.status === 'ENABLED') {
      const canEnableUser = await this._canEnableUser(
        req.principal,
        customerId
      );
      if (!canEnableUser) {
        return forbidden(res, {
          message: 'You have too many active users for your current plan',
        });
      }
    }

    if (!dbUser) {
      throw new NotFoundError(`User ${id} not found.`);
    }
    const updatedUser = Object.assign({}, dbUser, req.body);
    await this._userRepo.update(id, updatedUser, customerId);
    if (role !== dbUser.role) {
      await this._ucRepo.updateRole(dbUser.id, customerId, role);
    }
    return okay(res);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = Number(req.params.id);
    await this._userRepo.del(id);
    okay(res);
  }

  async _canEnableUser(currentUser: Principal, customerId) {
    if (
      currentUser &&
      currentUser.details &&
      currentUser.details.role === 'super'
    ) {
      return true;
    }
    const [users, currentCustomer] = await Bluebird.all([
      this._userFullRepo.fetchByCustomer(customerId),
      this._customerRepository.fetchById(customerId),
    ]);

    const enabledUsersCount = users.filter(
      u => u.status === 'ENABLED' && u.role !== 'super'
    ).length;
    if (
      plans[currentCustomer.plan].maxActiveUsers &&
      enabledUsersCount >= plans[currentCustomer.plan].maxActiveUsers
    ) {
      return false;
    }
    return true;
  }
}
