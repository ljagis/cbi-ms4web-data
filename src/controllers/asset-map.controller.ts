import { Request, Response, NextFunction } from 'express';
import { BaseController } from './base.controller';
import { inject } from 'inversify';
import { controller, httpGet } from 'inversify-express-utils';
import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';
import {
  CitizenReportRepository,
  ConstructionSiteRepository,
  FacilityRepository,
  IllicitDischargeRepository,
  OutfallRepository,
  StructureRepository,
} from '../data';
import {
  Principal,
  ConstructionSiteQueryOptions,
  OutfallQueryOptions,
  FacilityQueryOptions,
  StructureQueryOptions,
  IllicitDischargeQueryOptions,
  CitizenReportQueryOptions,
} from '../models';

@controller(`${APP_PATH}/api/assetmap`)
export class AssetMapController extends BaseController {
  @inject(CitizenReportRepository) private _crRepo: CitizenReportRepository;

  @inject(ConstructionSiteRepository)
  private _csRepo: ConstructionSiteRepository;

  @inject(FacilityRepository) private _facilityRepo: FacilityRepository;

  @inject(IllicitDischargeRepository)
  private _illicitRepo: IllicitDischargeRepository;

  @inject(OutfallRepository) private _outfallRepo: OutfallRepository;

  @inject(StructureRepository) private _structureRepo: StructureRepository;

  constructor() {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    const csQueryOptions: ConstructionSiteQueryOptions = {
      includeLastInspector: true,
    };
    const outfallQueryOptions: OutfallQueryOptions = {
      includeLastInspector: true,
    };
    const facilityQueryOptions: FacilityQueryOptions = {
      includeLastInspector: true,
    };
    const structureQueryOptions: StructureQueryOptions = {
      includeLastInspector: true,
    };
    const idQueryOptions: IllicitDischargeQueryOptions = {};
    const crQueryOptions: CitizenReportQueryOptions = {};
    if (await principal.isContractor()) {
      csQueryOptions.contractorEmail = principal.details.email;
      outfallQueryOptions.contractorEmail = principal.details.email;
      facilityQueryOptions.contractorEmail = principal.details.email;
      structureQueryOptions.contractorEmail = principal.details.email;
      idQueryOptions.contractorEmail = principal.details.email;
      crQueryOptions.contractorEmail = principal.details.email;
    }

    const citizenReports = await this._crRepo.fetch(customerId, crQueryOptions);
    const constructionSites = await this._csRepo.fetch(
      customerId,
      csQueryOptions
    );
    const facilities = await this._facilityRepo.fetch(
      customerId,
      facilityQueryOptions
    );
    const illicitDischarges = await this._illicitRepo.fetch(
      customerId,
      idQueryOptions
    );
    const outfalls = await this._outfallRepo.fetch(
      customerId,
      outfallQueryOptions
    );
    const structures = await this._structureRepo.fetch(
      customerId,
      structureQueryOptions
    );
    return this.ok({
      citizenReports,
      constructionSites,
      facilities,
      illicitDischarges,
      outfalls,
      structures,
    });
  }
}
