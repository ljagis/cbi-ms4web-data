import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as moment from 'moment';
import * as models from '../models';
import * as Promise from 'bluebird';

chai.use(ChaiHttp);

let controllerRoute = 'bmpactivities';
let postBmp: models.BmpActivity = {
  dateAdded: new Date(2016, 10, 1),
  comments: 'Added links to several YouTube videos',
  completedDate: new Date(2016, 10, 2),
  description: 'Description',
  dueDate: new Date(2016, 10, 3),
  isCompleted: false,
  title: 'Storm Water Webpage Update',
};
let putBmp: models.BmpActivity = {
  dateAdded: new Date(2016, 11, 1),
  comments: 'Added links to several YouTube videos 2',
  completedDate: new Date(2016, 11, 2),
  description: 'Description 2',
  dueDate: new Date(2016, 11, 3),
  isCompleted: true,
  title: 'Storm Water Webpage Update 2',
};
let createdId: number;

describe(`GET all BMPs`, () => {
  it(`should GET Bmps`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
        chai.expect(res.body).to.be.instanceof(Array);
        chai.expect(res.body).to.have.length.above(0);
      });
  });
});

describe(`CRUD Bmp`, () => {
  it(`should POST Bmp`, () => {
    return chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postBmp)
      .then(res => {
        createdId = +res.body.id;
        chai.expect(res).to.have.status(201);
        chai.expect(res).to.be.json;
        chai.expect(createdId).to.be.greaterThan(0);
      });
  });

  it(`should GET Bmp to verify created data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(postBmp).forEach(k => {
          if (postBmp[k] instanceof Date) {
            chai.assert(moment(postBmp[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(postBmp[k]);
          }
        });
      });
  });

  it(`should PUT Bmp`, () => {
    return chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(putBmp)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });

  it(`should GET Bmp to verify modified data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(putBmp).forEach(k => {
          if (putBmp[k] instanceof Date) {
            chai.assert(moment(putBmp[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(putBmp[k]);
          }
        });
      });
  });

  it(`should DELETE Bmp`, () => {
    if (createdId > 0) {
      return chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
        });
    }
    return Promise.resolve();
  });
});
