import { APP_PATH } from '../shared';
import { enforceFeaturePermission } from '../middleware';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { EntityTypes } from '../shared';

import { EntityCustomFieldController } from './entity-custom-field.controller';

const CONTROLLER_ROUTE = 'citizenreports';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class CitizenReportCustomFieldController extends EntityCustomFieldController {
  constructor() {
    super(EntityTypes.CitizenReport);
  }

  @httpGet('/:id([0-9]+)/customfields', authenticationRequired)
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpPut(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit')
  )
  async update(req, res, next) {
    await super.update(req, res, next);
  }
}
