import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  createClassFromObject,
  validateModel,
  NotFoundError,
  created,
  ClientError,
  notFound,
} from '../shared';
import { BmpDataTypeRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { enforceFeaturePermission } from '../middleware';
import { getCustomerId } from '../shared';
import * as express from 'express';
import { BmpDataType } from '../models';

const CONTROLLER_BASE_ROUTE = 'bmpdatatypes';
@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpDataTypeController {
  constructor(private _dataTypeRepo: BmpDataTypeRepository) {}

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    const dataTypes = await this._dataTypeRepo.fetch(customerId);
    okay(res, dataTypes);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = +req.params.id;
    const dataType = await this._dataTypeRepo.fetchById(customerId, id);
    if (dataType) {
      okay(res, dataType);
    } else {
      notFound(res);
    }
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);

    const dataType = createClassFromObject<BmpDataType>(BmpDataType, req.body);
    await validateModel(dataType);

    const existingDts = await this._dataTypeRepo.fetch(customerId, {
      name: dataType.name,
    });
    if (existingDts.length) {
      throw new ClientError(`${dataType.name} already exists`);
    }
    const returnedDataType = await this._dataTypeRepo.insert(
      customerId,
      dataType
    );
    created(res, returnedDataType);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = Number(req.params.id);
    let dataType = await this._dataTypeRepo.fetchById(customerId, id);
    if (!dataType) {
      throw new NotFoundError(`Bmp Data Type not found`);
    }
    const updatedDataType = Object.assign(
      new BmpDataType(),
      dataType,
      req.body
    );
    await validateModel(updatedDataType);
    await this._dataTypeRepo.update(customerId, id, req.body);

    dataType = await this._dataTypeRepo.fetchById(customerId, id);
    okay(res, dataType);
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    let customerId = await getCustomerId(req);
    await this._dataTypeRepo.delete(customerId, id);
    okay(res);
  }
}
