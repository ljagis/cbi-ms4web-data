import { NotFoundError } from '../shared';
import { CustomField } from '../models';
import { CustomFieldRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, createClassFromObject, okay, created } from '../shared';
import { enforceFeaturePermission } from '../middleware';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';

const CONTROLLER_ROUTE = 'customfields';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class CustomFieldController extends BaseController {
  constructor(private _customFieldRepo: CustomFieldRepository) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    const cfs = await this._customFieldRepo.fetch(customerId);
    okay(res, cfs);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);
    const cf = await this._customFieldRepo.fetchById(id, customerId);
    okay(res, cf);
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('customFields', 'edit')
  )
  async create(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let customField = createClassFromObject<CustomField>(CustomField, req.body);
    const cf = await this._customFieldRepo.insert(customField, customerId);
    created(res, cf);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('customFields', 'edit')
  )
  async update(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);
    let customField = req.body as CustomField;
    const cf = await this._customFieldRepo.fetchById(id, customerId);
    if (!cf) {
      throw new NotFoundError(`Custom Field ${id} not found`);
    }
    Object.assign(cf, customField);
    await this.validate(cf);
    await this._customFieldRepo.update(id, customField, customerId);
    okay(res, undefined);
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('customFields', 'delete')
  )
  async del(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);
    const cf = this._customFieldRepo.fetchById(id, customerId);
    if (!cf) {
      throw new NotFoundError(`Custom Field ${id} not found`);
    }
    await this._customFieldRepo.del(id, customerId);
    okay(res, undefined);
  }
}
