import { UserRepository, CustomerRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, okay, ClientError } from '../shared';
import { controller, httpGet, httpPut } from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response } from 'express';
import { CustomerResponse } from '../models';
import * as Bluebird from 'bluebird';
const CONTROLLER_ROUTE = 'currentuser';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class CurrentUserController extends BaseController {
  constructor(
    private _userRepo: UserRepository,
    private _customerRepo: CustomerRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response) {
    const rootUrl = this.getAbsoluteAppRoot(req);
    // no need to check req.user since `authenticationRequired` handles that
    const user = await this._userRepo.fetchByEmail(req.user.email);
    const customers = await this._customerRepo.fetchByUserId(user.id as number);
    const customersResponses = customers.map(c => {
      const customerResponse = Object.assign(
        new CustomerResponse(),
        c
      ) as CustomerResponse;
      if (c.logoThumbnailName) {
        customerResponse.logoThumbnailLocation = `${rootUrl}/api/staticfiles/${
          c.id
        }/${c.logoThumbnailName}`;
      }
      return customerResponse;
    });
    okay(res, {
      user,
      customers: customersResponses,
    });
  }

  /**
   * Updates the `currentCustomer` field to the new currentCustomer
   * data: { "currentCustomer": "LJA" }
   */
  @httpPut('/setcustomer', authenticationRequired)
  async setCustomer(req: Request, res: Response) {
    let currentCustomer = req.body.currentCustomer;
    if (!currentCustomer) {
      throw new ClientError('currentCustomer not specified');
    }
    const user = await this._userRepo.fetchByEmail(req.user.email);
    const customers = await this._customerRepo.fetchByUserId(user.id);
    let selectedCustomer = customers.find(c => c.id === currentCustomer);

    if (!selectedCustomer) {
      throw new ClientError(`${currentCustomer} not found`);
    }
    req.user.customData.currentCustomer = selectedCustomer.id;
    const saveAsync = Bluebird.promisify(req.user.save, { context: req.user });
    await saveAsync();
    okay(res);
  }
}
