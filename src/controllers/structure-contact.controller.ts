import { authenticationRequired } from 'express-stormpath';
import { APP_PATH } from './../shared';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { Structure } from '../models';

import { EntityContactController } from './entity-contact.controller';
import { enforceFeaturePermission } from '../middleware';

@controller(`${APP_PATH}/api/structures`)
export class StructureContactController extends EntityContactController {
  constructor() {
    super(Structure);
  }

  @httpGet('/:entityId([0-9]+)/contacts/:id', authenticationRequired)
  single() {
    return super.single.apply(this, arguments);
  }

  @httpGet('/:entityId([0-9]+)/contacts', authenticationRequired)
  index() {
    return super.index.apply(this, arguments);
  }

  @httpPost(
    '/:entityId([0-9]+)/contacts',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  create() {
    return super.create.apply(this, arguments);
  }

  @httpPut(
    '/:entityId([0-9]+)/contacts/:contactId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }

  @httpDelete(
    '/:entityId([0-9]+)/contacts/:contactId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  del() {
    return super.del.apply(this, arguments);
  }
}
