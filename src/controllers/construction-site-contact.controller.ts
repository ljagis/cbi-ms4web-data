import { authenticationRequired } from 'express-stormpath';
import { APP_PATH } from '../shared';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { ConstructionSite } from '../models';

import { EntityContactController } from './entity-contact.controller';
import { enforceFeaturePermission } from '../middleware';

@controller(`${APP_PATH}/api/constructionsites`)
export class ConstructionSiteContactController extends EntityContactController {
  constructor() {
    super(ConstructionSite);
  }

  /**
   * GETs a single contact associated with a construction site
   * params:
   * entityId - construction site id
   * id - contact id
   * @returns
   *
   * @memberOf ConstructionSiteContactController
   */
  @httpGet('/:entityId([0-9]+)/contacts/:id', authenticationRequired)
  single() {
    return super.single.apply(this, arguments);
  }

  /**
   * GETs contacts associated with a construction site
   * params:
   * entityId - construction site id
   *
   */
  @httpGet('/:entityId([0-9]+)/contacts', authenticationRequired)
  index() {
    return super.index.apply(this, arguments);
  }

  /**
   * POSTs contact id to associate with a construction site
   * POSTs body contains object with id of contact.
   * example:
   * POST { "id": 1}
   */
  @httpPost(
    '/:entityId([0-9]+)/contacts',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  create() {
    return super.create.apply(this, arguments);
  }

  /**
   * PUTs contact id to associate with a construction site
   * PUTs body contains object with id of contact.
   * example:
   * PUT { "contactId": 1, "isPrimary": false }
   */
  @httpPut(
    '/:entityId([0-9]+)/contacts/:contactId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }

  /**
   * DELETEs removes contact from a construction site
   * params:
   * entityId - construction site id
   * id - contact id
   */
  @httpDelete(
    '/:entityId([0-9]+)/contacts/:contactId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  del() {
    return super.del.apply(this, arguments);
  }
}
