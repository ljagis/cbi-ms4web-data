import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { ConstructionSite, ConstructionSiteInspection } from '../models';
import { InspectionCustomFieldController } from './inspection-custom-field.controller';
import { enforceFeaturePermission } from '../middleware';

const ASSET_ROUTE = 'constructionsites';

@controller(`${APP_PATH}/api/${ASSET_ROUTE}`)
export class ConstructionSiteInspectionCustomFieldController extends InspectionCustomFieldController {
  constructor() {
    super(ConstructionSite, ConstructionSiteInspection);
  }

  @httpGet(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired
  )
  async index(req, res, next) {
    return await super.index(req, res, next);
  }

  @httpPut(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  async update(req, res, next) {
    return await super.update(req, res, next);
  }
}
