import { authenticationRequired } from 'express-stormpath';
import { controller, httpPost } from 'inversify-express-utils';
import * as express from 'express';
import * as sendgrid from '@sendgrid/mail';
import * as _ from 'lodash';
import * as os from 'os';
import * as fs from 'fs';
import * as moment from 'moment';

import {
  APP_PATH,
  ClientError,
  created,
  Env,
  fileToFileLink,
  filterComplianceStatus,
  NotFoundError,
} from '../shared';
import { BaseController } from './base.controller';
import { IllicitDischargeReport, InspectionDetailsReport } from '../reports';
import { FetchAsset } from '../interfaces';
import { isFileNameValid } from '../shared/file-name-utils';
import { memoryMulterInstance } from './helpers';
import {
  CommunityRepository,
  ComplianceStatusRepository,
  ConstructionSiteInspectionRepository,
  ConstructionSiteRepository,
  ContactRepository,
  CustomFieldRepository,
  EmailRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  FileRepository,
  IllicitDischargeRepository,
  OutfallInspectionRepository,
  OutfallRepository,
  ReceivingWaterRepository,
  StructureInspectionRepository,
  StructureRepository,
  UserRepository,
  WatershedRepository,
} from '../data';
import {
  ConstructionSite,
  ConstructionSiteInspection,
  Customer,
  Facility,
  FacilityInspection,
  FileEntity,
  FileWithLinks,
  IllicitDischarge,
  IllicitDischargeQueryOptions,
  Outfall,
  OutfallInspection,
  Principal,
  Structure,
  StructureInspection,
} from '../models';

type AssetType =
  | 'citizenreports'
  | 'illicitdischarges'
  | 'facilities'
  | 'constructionsites'
  | 'outfalls'
  | 'structures';

type TypeOfAsset =
  | typeof ConstructionSite
  | typeof Facility
  | typeof Outfall
  | typeof Structure;

type TypeOfInspection =
  | typeof ConstructionSiteInspection
  | typeof FacilityInspection
  | typeof OutfallInspection
  | typeof StructureInspection;

const CONTROLLER_BASE_ROUTE = 'assetdetails';

const assetTypeMap = {
  constructionsites: 'ConstructionSite',
  facilities: 'Facility',
  outfalls: 'Outfall',
  structures: 'Structure',
};

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class AssetDetailController extends BaseController {
  env = process.env as Env;

  constructor(
    private _constructionSiteRepo: ConstructionSiteRepository,
    private _facilityRepo: FacilityRepository,
    private _outfallRepo: OutfallRepository,
    private _structureRepo: StructureRepository,
    private _constructionSiteInspectionRepo: ConstructionSiteInspectionRepository,
    private _facilityInspectionRepo: FacilityInspectionRepository,
    private _outfallInspectionRepo: OutfallInspectionRepository,
    private _structureInspectionRepo: StructureInspectionRepository,
    private _complianceStatusRepo: ComplianceStatusRepository,
    private _inspectionDetailsReport: InspectionDetailsReport,
    private _contactRepo: ContactRepository,
    private _userRepo: UserRepository,
    private _fileRepo: FileRepository,
    private _emailRepo: EmailRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _illicitRepo: IllicitDischargeRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _illicitDischargeReport: IllicitDischargeReport
  ) {
    super();
  }

  @httpPost(
    '/:assetType/:id([0-9]+)/email',
    authenticationRequired,
    memoryMulterInstance.array('attachments', 10)
  )
  async email(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customer: Customer = req.principal.selectedCustomer;
    const customerId = await this.getCustomerId(req);
    const assetType: AssetType = req.params.assetType;
    const assetId = req.params.id;
    let pdfDoc;
    let assetDetails;

    if (assetType === 'illicitdischarges') {
      const details = await this._getDetails(req);
      const investigatorId = details.illicitDischarge.investigatorId;

      let investigatorName = null;

      if (investigatorId) {
        const investigator = await this._userRepo.fetchById(investigatorId);
        investigatorName = investigator ? investigator.fullName : null;
      }

      pdfDoc = await this._illicitDischargeReport.generate(
        customer,
        details,
        investigatorName
      );

      assetDetails = details;
    } else {
      const inspectionDetails = [];

      const inspections = await this._getInspectionRepo(assetType).fetch(
        customerId,
        assetId
      );

      const complianceStatuses = await this._getComplianceStatuses(customer.id);

      for (let i = 0; i < inspections.length; i++) {
        const details = await this._getDetails(req, inspections[i].id);
        inspectionDetails.push(details);
      }

      pdfDoc = await this._inspectionDetailsReport.multiGenerate(
        assetTypeMap[assetType],
        inspectionDetails,
        customer,
        complianceStatuses
      );

      assetDetails = inspectionDetails;
    }

    const buffer = await this._writeEmailPdf(pdfDoc);
    const sendGridEmail = this._formatSendGridEmail(
      req,
      assetDetails,
      customerId,
      buffer
    );
    sendgrid.setApiKey(this.env.SENDGRID_API_KEY);
    let sendGridResponse;
    try {
      [sendGridResponse] = await sendgrid.send(<any>sendGridEmail, false);
    } catch (error) {
      const sendGridErrors = _.get(error, 'response.body.errors');
      const errorResponse = sendGridErrors
        ? sendGridErrors.map(error => error.message).join(' ')
        : 'Error sending email';
      throw Error(errorResponse);
    }

    if (
      sendGridResponse.statusCode !== 202 &&
      sendGridResponse.statusCode !== 204
    ) {
      throw Error('Error sending email');
    }

    created(res);
  }

  private async _getDetails(req: express.Request, inspectionId?: number) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = req.params.id;
    const assetType: AssetType = req.params.assetType;

    if (assetType === 'illicitdischarges') {
      const queryOptions: IllicitDischargeQueryOptions = {};
      const principal = this.httpContext.user as Principal;
      if (await principal.isContractor()) {
        queryOptions.contractorEmail = principal.details.email;
      }
      const illicitDischarge = await this._illicitRepo.fetchById(
        assetId,
        customerId,
        queryOptions
      );
      if (!illicitDischarge) {
        throw new NotFoundError();
      }

      // all has lookups
      const communities = await this._communityRepo.fetch(customerId);
      const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
      const watersheds = await this._watershedRepo.fetch(customerId);
      const customFields = await this._customFieldRepo.getCustomFieldValuesForInvestigation(
        IllicitDischarge,
        assetId,
        customerId
      );
      const illicitDischargeFiles = await this._fileRepo
        .fetch(IllicitDischarge, assetId, customerId)
        .map<FileEntity, FileWithLinks>(file =>
          fileToFileLink(file, customerId, rootUrl)
        );

      return {
        illicitDischarge,
        communities,
        receivingWaters,
        watersheds,
        customFields,
        files: illicitDischargeFiles,
      };
    }

    let asset: TypeOfAsset;
    let assetInspection: TypeOfInspection;
    const details: any = {};

    const { assetKey, inspectionKey } = this._getAssetInspectionType(assetType);
    const inspection = await this._getInspectionRepo(assetType).fetchById(
      inspectionId,
      assetId,
      customerId
    );

    if (!inspection) {
      throw new NotFoundError(`Inspection ${inspectionId} not found.`);
    }

    const previousInspection = await this._getInspectionRepo(
      assetType
    ).getPreviousInspection(assetId, customerId, inspection.inspectionDate);

    details[inspectionKey] = inspection;
    details[assetKey] = await (this._getAssetRepo(assetType) as FetchAsset<
      any
    >).fetchById(assetId, customerId);

    details[assetKey]['previousInspectionDate'] = previousInspection
      ? previousInspection.inspectionDate
      : null;

    // followUpInspection
    if (inspection.followUpInspectionId > 0) {
      details.followUpInspection = await this._getInspectionRepo(
        assetType
      ).fetchById(inspection.followUpInspectionId, assetId, customerId);
    } else {
      details.followUpInspection = null;
    }

    // parentInspection
    const parentInspections = await this._getInspectionRepo(
      assetType
    ).fetchParentInspections(inspectionId, assetId, customerId);
    details.parentInspection = parentInspections[0] || null;

    if (assetType === 'constructionsites') {
      asset = ConstructionSite;
      assetInspection = ConstructionSiteInspection;
    } else if (assetType === 'facilities') {
      asset = Facility;
      assetInspection = FacilityInspection;
    } else if (assetType === 'structures') {
      asset = Structure;
      assetInspection = StructureInspection;
    } else if (assetType === 'outfalls') {
      asset = Outfall;
      assetInspection = OutfallInspection;
    }

    // NOTE: Only CS, Facilities, and Structures have contacts
    if (
      assetType === 'constructionsites' ||
      assetType === 'facilities' ||
      assetType === 'structures'
    ) {
      details.contacts = await this._contactRepo.fetchContactsForEntity(
        asset,
        assetId,
        customerId,
        inspection.inspectionDate
      );
    }

    details.inspectors = await this._userRepo.fetch(customerId);
    const files = await this._fileRepo.fetch(
      assetInspection,
      inspectionId,
      customerId
    );
    details.files = files.map<FileWithLinks>(file =>
      fileToFileLink(file, customerId, rootUrl)
    );

    details.emails = await this._emailRepo.fetch(
      assetInspection,
      inspectionId,
      customerId
    );

    details.customFields = await this._customFieldRepo.getCustomFieldValuesForInspection(
      asset,
      assetInspection,
      assetId,
      inspectionId,
      customerId
    );

    return details;
  }

  private async _writeEmailPdf(pdfDoc: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const chunks = [];
      pdfDoc.on('data', chunk => {
        chunks.push(chunk);
      });

      pdfDoc.on('error', err => {
        reject(err);
      });

      pdfDoc.on('end', async () => {
        resolve(Buffer.concat(chunks));
      });

      pdfDoc.end();
    });
  }

  private async _getComplianceStatuses(customerId: string) {
    const complianceStatuses = await this._complianceStatusRepo.fetch();
    return complianceStatuses.filter(filterComplianceStatus(customerId));
  }

  private _getAssetInspectionType(assetType: AssetType) {
    let assetKey: string;
    let inspectionKey: string;

    switch (assetType) {
      case 'constructionsites':
        inspectionKey = 'constructionSiteInspection';
        assetKey = 'constructionSite';
        break;

      case 'facilities':
        inspectionKey = 'facilityInspection';
        assetKey = 'facility';
        break;

      case 'outfalls':
        inspectionKey = 'outfallInspection';
        assetKey = 'outfall';
        break;

      case 'structures':
        inspectionKey = 'structureInspection';
        assetKey = 'structure';
        break;
    }

    return { assetKey, inspectionKey };
  }

  private _getAssetRepo(assetType: AssetType) {
    switch (assetType) {
      case 'constructionsites':
        return this._constructionSiteRepo;

      case 'facilities':
        return this._facilityRepo;

      case 'outfalls':
        return this._outfallRepo;

      case 'structures':
        return this._structureRepo;
    }
  }

  private _getInspectionRepo(assetType: AssetType) {
    switch (assetType) {
      case 'constructionsites':
        return this._constructionSiteInspectionRepo;

      case 'facilities':
        return this._facilityInspectionRepo;

      case 'outfalls':
        return this._outfallInspectionRepo;

      case 'structures':
        return this._structureInspectionRepo;
    }
  }

  private _formatSendGridEmail(
    req: express.Request,
    details: any,
    customerId: string,
    buffer: Buffer
  ) {
    const attachments = [];
    const assetType: AssetType = req.params.assetType;

    if (assetType === 'illicitdischarges') {
      const { dateReported } = details.illicitDischarge;
      const formattedDate = moment(new Date(dateReported)).format('MM-DD-YYYY');
      const fileName = `Illicit Discharge Report ${formattedDate}.pdf`;

      attachments.push({
        content: buffer.toString('base64'),
        filename: fileName,
        type: 'application/pdf',
      });
    } else if (details.length) {
      const { assetKey } = this._getAssetInspectionType(assetType);
      const siteName =
        details[0][assetKey].name || details[0][assetKey].location || '';
      const siteDate = details[0][assetKey + 'Inspection'].inspectionDate;
      const formattedDate = moment(new Date(siteDate)).format('MM-DD-YYYY');
      const fileName = isFileNameValid(siteName)
        ? `${siteName} Inspection ${formattedDate}.pdf`
        : `Inspection ${formattedDate}.pdf`;

      attachments.push({
        content: buffer.toString('base64'),
        filename: fileName,
        type: 'application/pdf',
      });
    }

    const checkForMultipleEmails = emails => {
      if (Array.isArray(emails)) {
        return emails.reduce(
          (array, email) => [...array, { email: email }],
          []
        );
      } else {
        return [{ email: emails }];
      }
    };
    // ([{email: 'exampleOne@mail.com'}, {email: 'exampleTwo@email.com'}], 'email')
    const emailTo = _.uniqBy(checkForMultipleEmails(req.body.emailTo), 'email');
    const emailCC = req.body.emailCC
      ? _.uniqBy(checkForMultipleEmails(req.body.emailCC), 'email')
      : '';
    const subject = req.body.subject;
    const content = [
      req.body.content,
      '----------', // tslint:disable-next-line
      'This email has been generated from the MS4web Stormwater compliance application. Please send any responses to the email address in the CC field.',
    ].join(`${os.EOL}${os.EOL}`);

    if (!Array.isArray(req.files)) {
      throw new ClientError(
        'Request files is not an array.  Please check your request parameters.'
      );
    }

    const files: Express.Multer.File[] = req.files;

    const toBase64 = (url: string, fileNameStr: string) => {
      try {
        const bitmap = fs.readFileSync(url);
        return new Buffer(bitmap).toString('base64');
      } catch {
        throw new Error(`Unable to read inspection file ${fileNameStr}`);
      }
    };

    let filesToAttach;

    if (assetType === 'illicitdischarges') {
      filesToAttach = details.files || [];
    } else {
      filesToAttach = details.reduce(
        (acc, detail) => [...acc, ...detail.files],
        []
      );
    }

    // filter out image files, they are on the inspection pdf
    filesToAttach = filesToAttach.filter(file =>
      file.mimeType.indexOf('image')
    );

    const assetAttachments = filesToAttach.reduce((array: any, file) => {
      const url = `${this.env.FILE_STORAGE_PATH}/${customerId}/${
        file.fileName
      }`;
      const fileData = toBase64(url, file.fileName);
      return [
        ...array,
        {
          content: fileData,
          filename: file.originalFileName,
          type: file.mimeType,
        },
      ];
    }, []);

    const userAttachments = files.reduce((array: any, file) => {
      return [
        ...array,
        {
          content: file.buffer.toString('base64'),
          filename: file.originalname,
          type: file.mimetype,
        },
      ];
    }, []);

    attachments.push(...userAttachments);
    attachments.push(...assetAttachments);

    const sendGridEmail = {
      to: emailTo,
      cc: emailCC,
      subject: subject,
      from: this.env.MAIL_FROM,
      text: content,
      attachments,
    };

    return sendGridEmail;
  }
}
