import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  createClassFromObject,
  validateModel,
  NotFoundError,
  created,
  notFound,
} from '../shared';
import { BmpActivityLogRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { enforceFeaturePermission } from '../middleware';
import { getCustomerId } from '../shared';
import * as express from 'express';
import { BmpDetail, BmpActivityLog } from '../models';

const CONTROLLER_BASE_ROUTE = 'bmpdetails';
@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpActivityLogController {
  constructor(private _repo: BmpActivityLogRepository) {}

  @httpGet(
    '/:detailId([0-9]+)/activitylogs',
    authenticationRequired,
    enforceFeaturePermission('activityLogs', 'view')
  )
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    const detailId = +req.params.detailId;
    const activityLogs = await this._repo.fetchByDetail(customerId, detailId);
    okay(res, activityLogs);
  }

  @httpGet(
    '/:detailId([0-9]+)/activitylogs/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('activityLogs', 'view')
  )
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = +req.params.id;
    const log = await this._repo.fetchById(customerId, id);
    if (log) {
      okay(res, log);
    } else {
      notFound(res);
    }
  }

  @httpPost(
    '/:detailId([0-9]+)/activitylogs',
    authenticationRequired,
    enforceFeaturePermission('activityLogs', 'edit')
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    const activityLog = createClassFromObject<BmpActivityLog>(
      BmpActivityLog,
      req.body
    );
    const detailId = +req.params.detailId;
    activityLog.bmpDetailId = detailId; // ensure bmpDetailId
    await validateModel(activityLog);
    const returnedLog = await this._repo.insert(customerId, activityLog);
    created(res, returnedLog);
  }

  @httpPut(
    '/:detailId([0-9]+)/activitylogs/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('activityLogs', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = Number(req.params.id);
    let bmpDetail = await this._repo.fetchById(customerId, id);
    if (!bmpDetail) {
      throw new NotFoundError(`Bmp Detail not found`);
    }
    const updatedDetail = Object.assign(new BmpDetail(), bmpDetail, req.body);
    await validateModel(updatedDetail);
    await this._repo.update(customerId, id, req.body);
    bmpDetail = await this._repo.fetchById(customerId, id);
    okay(res, bmpDetail);
  }

  @httpDelete(
    '/:detailId([0-9]+)/activitylogs/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('activityLogs', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    let customerId = await getCustomerId(req);
    await this._repo.delete(customerId, id);
    okay(res);
  }
}
