import * as XLSX from 'xlsx';
import * as moment from 'moment';

import { APP_PATH, EntityTypes, streamToPromise } from '../shared';
import { Request, Response } from 'express';
import { controller, httpPost } from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Customer, FileEntityJoined } from '../models';
import { CustomerRepository, FileRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { superRequired } from '../middleware';

const SERVER_PROD_URL = 'https://stormwater.ms4web.com';

@controller(`${APP_PATH}/api/customer-export`)
export class CustomerExportController extends BaseController {
  constructor(
    private _customerRepo: CustomerRepository,
    private _fileRepo: FileRepository
  ) {
    super();
  }

  @httpPost('/', authenticationRequired, superRequired)
  async exportAll(req: Request, res: Response) {
    const customers = await this._customerRepo.fetch();
    const exportModels = customers.map(customerToExportModel);
    const worksheet = XLSX.utils.json_to_sheet(exportModels);
    const stream = XLSX.stream.to_csv(worksheet, {});
    res.setHeader('Content-type', 'text/csv');
    const fileName = `all_customers_export_${moment().format(
      'YYYY_MM_DD'
    )}.csv`;
    res.setHeader('Content-disposition', `attachment;filename=${fileName}`);
    stream.pipe(res);
    await streamToPromise(stream);
  }

  @httpPost('/files', authenticationRequired)
  async exportAllFiles(req: Request, res: Response) {
    const customerId = await this.getCustomerId(req);
    const files = await this._fileRepo.fetchAll(customerId, { limit: 50000 });
    const exportModels = files.map(file => ({
      Name: file.originalFileName,
      'File Name': file.fileName,
      'File Type': file.mimeType,
      Size: `${(file.fileSize / 1048576).toFixed(3)} MB`,
      Date: moment(file.addedDate).format('MM/DD/YYYY, HH:mm'),
      'Asset Name': file.entityName,
      'Asset Type': file.entityType,
      Link: `${SERVER_PROD_URL}${getFileLink(file)}`,
    }));
    const worksheet = XLSX.utils.json_to_sheet(exportModels);
    const stream = XLSX.stream.to_csv(worksheet, {});
    res.setHeader('Content-type', 'text/csv');
    const fileName = `all_customer_files_export_${moment().format(
      'YYYY_MM_DD'
    )}.csv`;
    res.setHeader('Content-disposition', `attachment;filename=${fileName}`);
    stream.pipe(res);
    await streamToPromise(stream);
  }
}

function customerToExportModel(customer: Customer): CustomerExportModel {
  const {
    id,
    fullName,
    countryCode,
    stateAbbr,
    mapServiceUrl,
    plan,
    unlimitedAssets,
    unlimitedCustomFormTemplates,
    dateAdded,
  } = customer;
  return {
    id,
    fullName,
    countryCode,
    stateAbbr,
    mapServiceUrl,
    plan,
    unlimitedAssets,
    unlimitedCustomFormTemplates,
    featureCalendar: customer.features.calendar,
    featureFileGallery: customer.features.fileGallery,
    featureBmps: customer.features.bmps,
    featureReports: customer.features.reports,
    featureConstructionSites: customer.features.constructionSites,
    featureOutfalls: customer.features.outfalls,
    featureStructuralControls: customer.features.structuralControls,
    featureFacilities: customer.features.facilities,
    featureIllicitDischarges: customer.features.illicitDischarges,
    featureCitizenReports: customer.features.illicitDischarges,
    featureExtraLayers: customer.features.extraLayers,
    featureCustomFormTemplates: customer.features.customFormTemplates,
    featureExporting: customer.features.exporting,
    featureCustomFields: customer.features.customFields,
    dateAdded,
  };
}

function getFileLink(file: FileEntityJoined) {
  switch (file.entityType) {
    case EntityTypes.BmpDetail:
      return `/bmp-details/${file.entityId}`;

    case EntityTypes.ConstructionSite:
      return `/constructionsites/${file.entityId}`;

    case EntityTypes.Facility:
      return `/facilities/${file.entityId}`;

    case EntityTypes.Outfall:
      return `/outfalls/${file.entityId}`;

    case EntityTypes.Structure:
      return `/structures/${file.entityId}`;

    case EntityTypes.CitizenReport:
      return `/citizenreports/${file.entityId}`;

    case EntityTypes.IllicitDischarge:
      return `/illicitdischarges/${file.entityId}`;

    case EntityTypes.ConstructionSiteInspection:
      return `/constructionsites/${file.entityId}/inspections/${
        file.inspectionId
      }`;

    case EntityTypes.FacilityInspection:
      return `/facilities/${file.entityId}/inspections/${file.inspectionId}`;

    case EntityTypes.StructureInspection:
      return `/structures/${file.entityId}/inspections/${file.inspectionId}`;

    case EntityTypes.OutfallInspection:
      return `/outfalls/${file.entityId}/inspections/${file.inspectionId}`;

    default:
      return '';
  }
}

interface CustomerExportModel
  extends Pick<
      Customer,
      | 'id'
      | 'fullName'
      | 'countryCode'
      | 'stateAbbr'
      | 'mapServiceUrl'
      | 'plan'
      | 'unlimitedCustomFormTemplates'
      | 'unlimitedAssets'
      | 'dateAdded'
    > {
  featureCalendar: boolean;
  featureFileGallery: boolean;
  featureBmps: boolean;
  featureReports: boolean;
  featureConstructionSites: boolean;
  featureOutfalls: boolean;
  featureStructuralControls: boolean;
  featureFacilities: boolean;
  featureIllicitDischarges: boolean;
  featureCitizenReports: boolean;
  featureExtraLayers: boolean;
  featureCustomFormTemplates: boolean;
  featureExporting: boolean;
  featureCustomFields: boolean;
}
