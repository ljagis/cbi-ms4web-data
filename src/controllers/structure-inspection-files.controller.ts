import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers/multer';

const CONTROLLER_ROUTE = 'structures';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class StructureInspectionFilesController extends EntityFileController {
  constructor() {
    super(EntityTypes.StructureInspection);
  }

  @httpPost(
    '/:structureId/inspections/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet(
    '/:structureId/inspections/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired
  )
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet(
    '/:structureId/inspections/:id([0-9]+)/files',
    authenticationRequired
  )
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:structureId/inspections/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
