import { APP_PATH } from '../shared';
import { enforceFeaturePermission } from '../middleware';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { Outfall, OutfallInspection } from '../models';
import { InspectionCustomFieldController } from './inspection-custom-field.controller';

const ASSET_ROUTE = 'outfalls';

@controller(`${APP_PATH}/api/${ASSET_ROUTE}`)
export class OutfallInspectionCustomFieldController extends InspectionCustomFieldController {
  constructor() {
    super(Outfall, OutfallInspection);
  }

  @httpGet(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired
  )
  index() {
    return super.index.apply(this, arguments);
  }

  @httpPut(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }
}
