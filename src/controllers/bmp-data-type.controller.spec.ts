import { config } from '../test/helper';
import { container } from '../inversify.config';
import { BmpDataType } from '../models';
import { BmpDataTypeRepository } from '../data';
import * as chai from 'chai';

const controllerRoute = 'bmpdatatypes';
const customerId = 'LEWISVILLE';

describe(`BMP Data Type`, async () => {
  const dataTypeRepo = container.get(BmpDataTypeRepository);
  const unique = new Date().getTime();
  let createdId: number;
  after(async () => {
    if (createdId) {
      await dataTypeRepo.delete(config.customerId, createdId, { force: true });
    }
  });

  it(`should POST Data Type`, async () => {
    const expectedName = `miles driven${unique}`;
    const postData: Partial<BmpDataType> = {
      name: expectedName,
    };
    const response = await chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postData);

    const returnedData = response.body as BmpDataType;
    createdId = +response.body.id;
    chai.expect(response).to.have.status(201);
    chai.expect(response).to.be.json;
    chai.expect(returnedData.id).to.be.greaterThan(0);
    chai.expect(returnedData.name).eq(expectedName);
  });

  it(`should fetch all`, async () => {
    const expectedName = `miles driven${unique}`;
    const response = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie);

    chai.expect(response).to.have.status(200);
    chai.expect(response).to.be.json;

    const dataType = (response.body as BmpDataType[]).find(
      t => t.id === createdId
    );
    chai.expect(dataType).ok;
    chai.expect(dataType.name).eq(expectedName);
  });

  it(`should PUT`, async () => {
    const expectedData: Partial<BmpDataType> = {
      name: `miles driven${unique}`,
    };
    const resp = await chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(expectedData);

    const returnedData = resp.body as BmpDataType;
    chai.expect(resp).json;
    chai.expect(resp).status(200);
    chai.expect(returnedData.name).eq(expectedData.name);
  });

  it(`should DELETE`, async () => {
    const resp = await chai
      .request(config.baseUrl)
      .del(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    const returnedDetail = await dataTypeRepo.fetchById(customerId, createdId);
    chai.expect(resp).json;
    chai.expect(resp).status(200);
    chai.expect(returnedDetail).not.ok;
  });
});
