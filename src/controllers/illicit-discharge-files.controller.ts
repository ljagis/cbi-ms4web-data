import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';

import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_ROUTE = 'illicitdischarges';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class IllicitDischargeFiles extends EntityFileController {
  constructor() {
    super(EntityTypes.IllicitDischarge);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
