import { APP_PATH, createClassFromObject, okay, Role } from '../shared';
import { BMP, MG, SWMP, SWMPStatus, SWMPAddOption } from '../models';
import {
  BmpRepository,
  CustomerRepository,
  MgRepository,
  SwmpRepository,
  UserFullRepository,
} from '../data';
import { NextFunction, Request, Response } from 'express';
import {
  controller,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';
import { difference, intersection, omit } from 'lodash';

import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import { adminRequired } from '../middleware';
import { SWMP_TEMPLATE } from '../shared/constants';

const CONTROLLER_ROUTE = 'swmp';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class SwmpController extends BaseController {
  constructor(
    private _swmpRepository: SwmpRepository,
    private _bmpRepository: BmpRepository,
    private _mgRepository: MgRepository,
    private _customerRepository: CustomerRepository,
    private _userFullRepository: UserFullRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmp = await this._swmpRepository.fetchByCustomer(customerId);
    const customer = await this._customerRepository.fetchById(customerId);
    const userCustomer = await this._userFullRepository.fetchByEmailAndCustomer(
      req.user.email,
      customerId
    );

    okay(res, {
      swmp,
      customer: {
        id: customer.id,
        stateAbbr: customer.stateAbbr,
        fullName: req.user.fullName,
        annualReport: customer.features.swmpAnnualReport,
        admin:
          userCustomer.role === Role.super || userCustomer.role === Role.admin,
      },
    });
  }

  @httpPost('/', authenticationRequired, adminRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const { addOption } = req.body;
    const latestSwmp = await this._swmpRepository.fetchLatestByCustomer(
      customerId
    );
    let swmp = createClassFromObject(SWMP, req.body);
    let newSwmp: SWMP;

    swmp.customerId = customerId;
    swmp.status = SWMPStatus.Draft;

    if (addOption === SWMPAddOption.Copy && latestSwmp) {
      swmp = {
        ...latestSwmp,
        year: swmp.year,
        status: swmp.status,
        updatedDate: null,
        planLength: 5,
      };

      newSwmp = await this._swmpRepository.create(swmp);
      const bmp = await this._bmpRepository.fetchBySwmpId(latestSwmp.id);

      for (let i = 0; i < bmp.length; i++) {
        const newBmp = await this._bmpRepository.create({
          ...bmp[i],
          startYear: 0,
          swmpId: newSwmp.id,
        });

        const assignedMgs = await this._mgRepository.fetchByBmpId(bmp[i].id);

        for (let j = 0; j < assignedMgs.length; j++) {
          const yearsImplementedArray = (assignedMgs[j]
            .yearsImplemented as string)
            .split(',')
            .filter(a => parseInt(a, 10) <= 5);

          if (!yearsImplementedArray.length) {
            continue;
          }

          await this._mgRepository.create({
            ...assignedMgs[j],
            yearsImplemented: yearsImplementedArray.join(','),
            bmpId: newBmp.id,
          });
        }
      }
    } else {
      newSwmp = await this._swmpRepository.create(swmp);

      if (addOption === SWMPAddOption.Example) {
        for (let i = 0; i < SWMP_TEMPLATE.bmps.length; i++) {
          const newBmp = await this._bmpRepository.create({
            ...omit(SWMP_TEMPLATE.bmps[i], 'mgs'),
            swmpId: newSwmp.id,
          });

          for (let j = 0; j < SWMP_TEMPLATE.bmps[i].mgs.length; j++) {
            await this._mgRepository.create({
              ...SWMP_TEMPLATE.bmps[i].mgs[j],
              bmpId: newBmp.id,
            });
          }
        }
      }
    }

    okay(res, newSwmp);
  }

  @httpPut('/', authenticationRequired, adminRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmp = createClassFromObject(SWMP, req.body);

    await this._swmpRepository.update(swmp, customerId);

    okay(res);
  }

  @httpPut('/activate', authenticationRequired, adminRequired)
  async activate(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const prevSwmps = await this._swmpRepository.fetchByCustomer(customerId);
    const prevActivatedSwmp = prevSwmps.find(
      s => s.status === SWMPStatus.Active
    );
    // update previous activated swmp status to previous
    if (prevActivatedSwmp) {
      prevActivatedSwmp.status = SWMPStatus.Previous;
      await this._swmpRepository.update(prevActivatedSwmp, customerId);
    }

    // set new activate swmp
    const swmp = createClassFromObject(SWMP, req.body);
    await this._swmpRepository.update(swmp, customerId);

    okay(res);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    const customerId = await this.getCustomerId(req);
    await this._swmpRepository.delete(id, customerId);

    okay(res);
  }

  @httpPut('/extend', authenticationRequired, adminRequired)
  async extend(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmp = createClassFromObject(SWMP, req.body);

    await this._swmpRepository.update(swmp, customerId);
    const bmps = await this._bmpRepository.fetchBySwmpId(swmp.id);

    for (let i = 0; i < bmps.length; i++) {
      const assignedMgs = await this._mgRepository.fetchByBmpId(bmps[i].id);

      for (let j = 0; j < assignedMgs.length; j++) {
        const yearsImplementedArray = (assignedMgs[j]
          .yearsImplemented as string).split(',');
        if (!yearsImplementedArray.includes((swmp.planLength - 1).toString())) {
          continue;
        }

        await this._mgRepository.update({
          ...assignedMgs[j],
          yearsImplemented: [
            ...yearsImplementedArray,
            swmp.planLength.toString(),
          ].join(','),
        });
      }
    }

    okay(res);
  }

  @httpPost('/updateEntities', authenticationRequired, adminRequired)
  async updateEntities(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const swmp = createClassFromObject(SWMP, req.body.swmp);
    const bmps: BMP[] = req.body.bstMgtPracs;
    const queryPromises: Promise<any>[] = [];

    // delete removed entities
    queryPromises.push(
      ...this.removeEntities(
        await this._bmpRepository.fetchBySwmpId(swmp.id),
        bmps
      )
    );

    // update swmp
    await this._swmpRepository.update(
      {
        ...swmp,
        updatedDate: new Date(),
      },
      customerId
    );

    // update BMPs and MGs
    for (let i = 0; i < bmps.length; i++) {
      let newBmp: BMP | null = null;
      // check if a bmp is a new bmp or not
      const isExistBmp = typeof bmps[i].id === 'number';

      if (!isExistBmp) {
        newBmp = await this._bmpRepository.create(bmps[i]);
      } else {
        queryPromises.push(
          ...this.removeEntities(
            await this._mgRepository.fetchByBmpId(bmps[i].id),
            bmps[i].measurableGoals,
            true
          )
        );

        queryPromises.push(
          this._bmpRepository.update({
            ...omit(bmps[i], 'measurableGoals'),
          })
        );
      }

      for (let j = 0; j < bmps[i].measurableGoals.length; j++) {
        if (!isExistBmp) {
          queryPromises.push(
            this._mgRepository.create({
              ...bmps[i].measurableGoals[j],
              bmpId: newBmp.id,
            })
          );
        } else {
          // check if a mg is a new mg or not
          if (typeof bmps[i].measurableGoals[j].id === 'number') {
            queryPromises.push(
              this._mgRepository.update(bmps[i].measurableGoals[j])
            );
          } else {
            queryPromises.push(
              this._mgRepository.create({
                ...bmps[i].measurableGoals[j],
                bmpId: bmps[i].id,
              })
            );
          }
        }
      }
    }

    await Promise.all(queryPromises);

    okay(res);
  }

  removeEntities(
    prevEntities: (BMP | MG)[],
    newEntities: (BMP | MG)[],
    isRemoveMg?: boolean
  ) {
    const prevEntityIds = prevEntities.map(e => e.id);
    const newEntityIds = newEntities.map(e => e.id);

    const removedEntityIds = difference(
      prevEntityIds,
      intersection(prevEntityIds, newEntityIds)
    );

    if (isRemoveMg) {
      return removedEntityIds.map(id => this._mgRepository.delete(id));
    } else {
      return removedEntityIds.map(id => this._bmpRepository.delete(id));
    }
  }
}
