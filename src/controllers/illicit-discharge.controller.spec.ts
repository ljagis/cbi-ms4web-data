import {
  CustomFormTemplate,
  InspectionType,
  IllicitDischarge,
} from '../models';
import { config } from '../test/helper';
import { container } from '../inversify.config';
import * as Knex from 'knex';
import * as chai from 'chai';
import {
  CustomFormTemplateRepository,
  InspectionTypeRepository,
} from '../data/index';
import * as moment from 'moment';

describe(`Illicit Discharges CRUD`, async () => {
  const controllerRoute = 'illicitdischarges';
  let illicit1: Partial<IllicitDischarge> = {
    dateAdded: new Date(2016, 11, 1),
    dateReported: new Date(2016, 10, 1),
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    phoneNumber: '111-111-1111',
    cellNumber: '222-222-2222',
    faxNumber: '333-333-3333',
    conversation: 'A conversation',
    dischargeDescription: 'Discharge desc',
    requestCorrectiveAction: 'Corrective action',
    dateEliminated: new Date(2016, 12, 1),
    investigatorId: null,
    investigator2Id: null,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'Add info',
    complianceStatus: 'Active',
    name: 'LISD - Huffines Middle School',
    lng: -97.031293,
    lat: 33.054002,
  };
  let illicit2: Partial<IllicitDischarge> = {
    dateAdded: new Date(2016, 11, 2),
    dateReported: new Date(2016, 10, 2),
    physicalAddress1: '123 Test 2',
    physicalAddress2: 'SUITE 2',
    physicalCity: 'Houston 2',
    physicalState: 'CA',
    physicalZip: '77043',
    phoneNumber: '111-111-1111a',
    cellNumber: '222-222-2222a',
    faxNumber: '333-333-3333a',
    conversation: 'A conversation 2',
    dischargeDescription: 'Discharge desc 2',
    requestCorrectiveAction: 'Corrective action 2',
    dateEliminated: new Date(2016, 12, 2),
    investigatorId: null,
    investigator2Id: null,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'Add info 2',
    complianceStatus: 'Active',
    name: 'LISD - Huffines Middle School 2',
    lng: -97.031299,
    lat: 33.054009,
  };
  let createdId: number;
  let formTemplates: CustomFormTemplate[];
  let inspectionTypes: InspectionType[];

  const cftRepo = container.get(CustomFormTemplateRepository);
  const itRepo = container.get(InspectionTypeRepository);
  const knex = container.get(Knex) as Knex;

  before(async () => {
    formTemplates = await cftRepo.fetch(config.customerId);
    inspectionTypes = await itRepo.fetch(config.customerId);
    illicit1.customFormTemplateId = formTemplates[0].id;
    illicit1.inspectionTypeId = inspectionTypes[0].id;
  });

  after(async () => {
    if (createdId) {
      await knex(IllicitDischarge.tableName)
        .del()
        .where(IllicitDischarge.sqlField(f => f.id), createdId);
    }
  });

  it(`should GET all`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
    chai.assert(Array.isArray(res.body));
  });

  it(`should POST Illicit Discharge`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(illicit1);
    createdId = +res.body.id;
    chai.expect(res).to.have.status(201);
    chai.expect(res).to.be.json;
    chai.expect(createdId).to.be.greaterThan(0);
  });

  it(`should GET Illicit Discharge to verify created data`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    Object.keys(illicit1).forEach(k => {
      if (illicit1[k] instanceof Date) {
        chai.assert(moment(illicit1[k]).isSame(moment(res.body[k])));
      } else {
        chai.expect(res.body[k]).equals(illicit1[k]);
      }
    });
  });

  it(`should PUT Illicit Discharge`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(illicit2);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
  });

  it(`should GET Illicit Discharge to verify modified data`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    Object.keys(illicit2).forEach(k => {
      if (illicit2[k] instanceof Date) {
        chai.assert(moment(illicit2[k]).isSame(moment(res.body[k])));
      } else {
        chai.expect(res.body[k]).equals(illicit2[k]);
      }
    });
  });

  it(`should check for illicitdischarges/details/:id route`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/details/${createdId}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    chai.expect(res.body.illicitDischarge).to.not.be.null;

    chai.expect(res.body.customFields).to.be.instanceOf(Array);
    chai.expect(res.body.files).to.be.instanceOf(Array);
    chai.expect(res.body.communities).to.have.length.greaterThan(0);
    chai.expect(res.body.receivingWaters).to.have.length.greaterThan(0);
    chai.expect(res.body.watersheds).to.have.length.greaterThan(0);
  });

  it(`should DELETE Illicit Discharge`, async () => {
    if (createdId > 0) {
      await chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie);
    }
  });
});
