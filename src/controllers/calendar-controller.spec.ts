import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';

chai.use(ChaiHttp);

let controllerRoute = 'calendar';

describe(`calendar`, () => {
  it(`should GET calendar`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .query({
        from: '2016-09-01',
        to: '2016-01-01',
      })
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });
});
