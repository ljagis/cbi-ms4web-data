import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import { CustomField } from '../models';
import { container } from '../inversify.config';
import { CustomFieldRepository } from '../data';
import { EntityTypes } from '../shared';

chai.use(ChaiHttp);

/** Deprecated - use CustomFormTemplate */
describe.skip('Construction Site Inspection Custom Field Controller', async () => {
  const cfRepo = container.get(CustomFieldRepository);
  const assetId = 4007;
  const inspectionId = 1193;
  const customerId = 'LEWISVILLE';
  const controllerRoute = `constructionsites/${assetId}/inspections/${inspectionId}/customfields`;
  const customFieldValue = 'TEST Custom Field Value';
  let createdCustomField: CustomField;

  before(async () => {
    const cf = _newCustomField(EntityTypes.ConstructionSiteInspection);

    createdCustomField = await cfRepo.insert(cf, customerId);
  });

  it('should PUT CS Inspection CF', async () => {
    const idValue = { id: createdCustomField.id, value: customFieldValue };
    const res = await chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send([idValue]);

    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
  });

  it(`should GET CS Inspection CFs`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie);

    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
    chai.expect(res.body).to.be.instanceof(Array);
    chai.expect(res.body).to.have.length.above(0);
    const foundCustomField = res.body.find(
      c => c.id === (createdCustomField.id as number)
    );
    chai.expect(foundCustomField).to.be.ok;
    chai.expect(foundCustomField.value).to.be.eq(customFieldValue);
  });

  // cleanup
  after(async () => {
    if (createdCustomField) {
      await cfRepo.del(createdCustomField.id as number, customerId, true);
    }
  });
});

function _newCustomField(entityType: string) {
  let cf = new CustomField();
  cf.customFieldType = 'Customer';
  cf.stateAbbr = 'TX';
  cf.entityType = entityType;
  cf.dataType = 'string';
  cf.inputType = 'text';
  cf.fieldLabel = 'Test Custom Field';
  cf.fieldOrder = 1;
  return cf;
}
