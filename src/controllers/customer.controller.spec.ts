import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';

chai.use(ChaiHttp);

let customerRoute = 'customer';

describe(`GET current Customer`, () => {
  it(`should GET Customer to verify created data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${customerRoute}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        chai.expect(res.body.id).equals('LEWISVILLE');
        chai.expect(res.body.fullName).equals('City of Lewisville');
        chai.expect(res.body.stateAbbr).equals('TX');
      });
  });
});

describe(`PUT current Customer`, () => {
  it(`should save current Customer`, () => {
    return chai
      .request(config.baseUrl)
      .put(`/api/${customerRoute}`)
      .set('Cookie', config.cookie)
      .send({
        mapServiceUrl: 'http://google.com',
      })
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });
});
