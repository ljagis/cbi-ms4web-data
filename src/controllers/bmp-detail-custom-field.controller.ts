import { BaseController } from './base.controller';
import { CustomFieldRepository } from '../data/custom-field.repository';
import { APP_PATH, okay } from '../shared';
import { controller, httpGet, httpPut } from 'inversify-express-utils';

import { authenticationRequired } from 'express-stormpath';
import * as express from 'express';
import { CustomFieldValueMap } from '../models';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'bmpdetails';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpDetailCustomFieldController extends BaseController {
  constructor(private _customFieldRepo: CustomFieldRepository) {
    super();
  }

  @httpGet(
    '/:bmpDetailId([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'view')
  )
  async index(req: express.Request, res: express.Response, next) {
    const bmpDetailId = Number(req.params.bmpDetailId);
    let customerId = await this.getCustomerId(req);
    const values = await this._customFieldRepo.getCustomFieldValuesForBmpDetail(
      customerId,
      bmpDetailId
    );

    okay(res, values);
  }

  @httpPut(
    '/:bmpDetailId([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async update(req, res, next) {
    let customerId = await this.getCustomerId(req);
    const bmpDetailId = Number(req.params.bmpDetailId);
    const data: CustomFieldValueMap[] = req.body;
    await this._customFieldRepo.updateCustomFieldsValuesForBmpDetail(
      bmpDetailId,
      data,
      customerId
    );

    okay(res);
  }
}
