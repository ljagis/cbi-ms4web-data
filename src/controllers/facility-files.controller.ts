import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { EntityFileController } from './entity-file.controller';
import { enforceFeaturePermission } from '../middleware';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'facilities';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class FacilityFilesController extends EntityFileController {
  constructor() {
    super(EntityTypes.Facility);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    return await super.create(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files/:fileId([0-9]+)', authenticationRequired)
  async indexAt(req, res, next) {
    return await super.indexAt(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files', authenticationRequired)
  async index(req, res, next) {
    return super.index(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req, res, next) {
    return super.del(req, res, next);
  }
}
