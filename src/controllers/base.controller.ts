import { Request, Response } from 'express';
import { inject, injectable } from 'inversify';
import { validate, ValidatorOptions } from 'class-validator';
import {
  getUsername,
  throwErrorOnValidationError,
  methodNotAllowed,
} from '../shared';
import * as Bluebird from 'bluebird';
import { getCustomerId } from '../shared';
import { BaseHttpController } from 'inversify-express-utils';

@injectable()
export abstract class BaseController extends BaseHttpController {
  @inject('applicationPath') protected _applicationPath: string;

  /**
   * Proxy method for class-validator.  If fails, throws error
   * See https://github.com/pleerock/class-validator
   * @param  {[type]}                     options [description]
   * @return {Bluebird<ValidationError[]>}         [description]
   */
  validate<T>(obj: T, validatorOptions?: ValidatorOptions): Bluebird<T> {
    return Bluebird.resolve(validate(obj, validatorOptions)) // converts to bluebird promise
      .tap(throwErrorOnValidationError)
      .return(obj) as Bluebird<T>;
  }

  /**
   * Gets the current customer.  If no customer returned then gets the first customer
   *
   */
  protected async getCustomerId(req: Request) {
    return await getCustomerId(req);
  }

  protected getUsername(req: Request) {
    return getUsername(req);
  }

  /**
   * Gets absolute url root
   * ex: http://localhost:9000 or http://gis.ljaengineering.com/ms4web
   *
   * @protected
   * @param {Request} req
   * @returns
   *
   * @memberOf BaseController
   */
  protected getAbsoluteAppRoot(req: Request) {
    return `https://${req.get('host')}${this._applicationPath}`;
  }

  protected getProtocol(req) {
    // this works when the incoming message is run by an https node server.
    if (req.connection.encrypted) {
      return 'https';
    }

    // iisnode
    if (
      req.headers['x-iisnode-https'] &&
      req.headers['x-iisnode-https'] === 'on'
    ) {
      return 'https';
    }

    // x-forwarded-proto is sent when running on heroku, nodejitsu, or using nginx reverse proxy.
    return req.headers['x-forwarded-proto'] || 'http';
  }

  protected methodNotAllowed(res: Response, message: string) {
    return methodNotAllowed(res, message);
  }
}
