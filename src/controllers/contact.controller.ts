import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import * as express from 'express';
import { CustomerLookupController } from './customer-lookup.controller';
import { Contact } from '../models';
import { APP_PATH } from '../shared';

const CONTROLLER_ROUTE = 'contacts';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class ContactController extends CustomerLookupController {
  constructor() {
    super(Contact);
  }

  @httpGet('/', authenticationRequired)
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.index.apply(this, arguments);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  indexAt() {
    return super.indexAt.apply(this, arguments);
  }

  @httpPost('/', authenticationRequired)
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.create.apply(this, arguments);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired)
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.update.apply(this, arguments);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired)
  del(req: express.Request, res: express.Response, next: express.NextFunction) {
    return super.del.apply(this, arguments);
  }
}
