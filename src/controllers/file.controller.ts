import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';
import { inject } from 'inversify';
import { controller, httpGet } from 'inversify-express-utils';
import { fileToFileLink, okay, notFound } from '../shared';
import { Request, Response, NextFunction } from 'express';
import { FileRepository } from '../data';
import { QueryOptions } from '../models';
import { BaseController } from './base.controller';

const CONTROLLER_ROUTE = 'files';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class FileController extends BaseController {
  @inject(FileRepository) protected _fileRepo: FileRepository;

  constructor() {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let rootUrl = this.getAbsoluteAppRoot(req);
    let queryOptions = <QueryOptions>{
      limit: Number(req.query.limit),
      offset: Number(req.query.offset),
      orderByField: req.query.orderByField,
      orderByDirection: req.query.orderByDirection,
    };

    const files = await this._fileRepo.fetchAll(customerId, queryOptions);
    let fileWithLinks = files.map(f => fileToFileLink(f, customerId, rootUrl));
    okay(res, fileWithLinks);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);
    let rootUrl = this.getAbsoluteAppRoot(req);
    const file = await this._fileRepo.fetchSingle(id, customerId);
    if (file) {
      let fileWithLinks = fileToFileLink(file, customerId, rootUrl);
      okay(res, fileWithLinks);
    } else {
      notFound(res);
    }
  }
}
