import { controller, httpGet } from 'inversify-express-utils';

import { authenticationRequired } from 'express-stormpath';
import { BaseController } from './base.controller';
import { Request, Response } from 'express';
import { APP_PATH } from '../shared';
import { appendUser } from '../middleware';

@controller(`${APP_PATH}/api`)
export class MeController extends BaseController {
  constructor() {
    super();
  }

  @httpGet('/me', authenticationRequired, appendUser)
  async me(req: Request, res: Response) {
    return this.ok(req.user);
  }
}
