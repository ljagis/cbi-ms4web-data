import * as express from 'express';
import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  EntityTypes,
  createClassFromObject,
  NotFoundError,
  fileToFileLink,
} from '../shared';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';

import {
  Outfall,
  InspectionQueryOptions,
  OutfallInspection,
  FileEntity,
  FileWithLinks,
  OutfallQueryOptions,
  Principal,
} from '../models';

import { OutfallDetailsResult } from '../models/asset-details-result';
import { BaseController } from './base.controller';
import {
  OutfallRepository,
  CommunityRepository,
  WatershedRepository,
  ReceivingWaterRepository,
  CustomFieldRepository,
  FileRepository,
  OutfallInspectionRepository,
} from '../data';

const BASE_ROUTE = `${APP_PATH}/api/outfalls`;

@controller(BASE_ROUTE)
export class OutfallController extends BaseController {
  constructor(
    private _outfallRepo: OutfallRepository,
    private _outfallInspectionRepo: OutfallInspectionRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _customFieldRepo: CustomFieldRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }

    const assets = await this._outfallRepo.fetch(currentCustomer, queryOptions);
    return this.ok(assets);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const asset = await this._outfallRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (asset) {
      return this.ok(asset);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet('/:id([0-9]+)/details', authenticationRequired)
  async details(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const inspectionQOpts: InspectionQueryOptions = {
      orderByRaw:
        `${OutfallInspection.sqlField(f => f.inspectionDate)} desc, ` +
        `${OutfallInspection.sqlField(f => f.scheduledInspectionDate)} desc`,
    };
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }

    const outfall = await this._outfallRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (!outfall) {
      throw new NotFoundError();
    }
    const inspections = await this._outfallInspectionRepo.fetch(
      customerId,
      assetId,
      inspectionQOpts
    );

    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
      Outfall,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(Outfall, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );
    const result: OutfallDetailsResult = {
      outfall,
      inspections,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
    };
    return this.ok(result);
  }

  /**
   * Creates a new Outfall.
   *
   * `POST /api/outfalls`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    customerAssetLimit(EntityTypes.Outfall)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(Outfall, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._outfallRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._outfallRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._outfallRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._outfallRepo.fetchById(
        assetId,
        customerId
      );
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const outfall = await this._outfallRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (outfall) {
      await this._outfallRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }

  @httpPost(
    '/resetComplianceStatus',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    customerAssetLimit(EntityTypes.Outfall)
  )
  async resetComplianceStatus(req: express.Request, res: express.Response) {
    const { status } = req.body;
    const currentCustomer = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    const queryOptions: OutfallQueryOptions = {};

    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }

    const assets = await this._outfallRepo.fetch(currentCustomer, queryOptions);

    for (let i = 0; i < assets.length; i++) {
      await this._outfallRepo.updateAsset(
        assets[i].id,
        {
          ...assets[i],
          complianceStatus: status,
        },
        currentCustomer
      );
    }

    return this.ok({ success: true });
  }
}
