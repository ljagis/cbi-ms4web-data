import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { APP_PATH, EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';
import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'bmpactivities';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class BmpActivityFileController extends EntityFileController {
  constructor() {
    super(EntityTypes.BmpActivity);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'view')
  )
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files', authenticationRequired)
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
