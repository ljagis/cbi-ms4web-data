import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpPost, httpDelete } from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'swmpactivities';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class SwmpActivityFilesController extends EntityFileController {
  constructor() {
    super(EntityTypes.SwmpActivity);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
