import { authenticationRequired } from 'express-stormpath';
import { controller, httpPut } from 'inversify-express-utils';
import * as express from 'express';
import { BaseController } from './base.controller';
import { Customer } from '../models';
import { CustomerRepository } from '../data';
import { APP_PATH, okay, ClientError, Defaults } from '../shared';
import { adminRequired } from '../middleware';
import * as multer from 'multer';
import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';

import { getCustomerId } from '../shared';

let storage = multer.diskStorage({
  destination(req: express.Request, file, cb) {
    getCustomerId(req).then(customerId => {
      let customerPath = `${process.env.FILE_STORAGE_PATH}/${customerId}`;
      if (!fs.existsSync(customerPath)) {
        fs.mkdirSync(customerPath);
      }
      cb(null, customerPath);
    });
  },
  filename(req, file, cb) {
    let extension = path.extname(file.originalname);
    cb(null, `logo${extension}`);
  },
});
const upload = multer({
  storage: storage,
  fileFilter(req, file, cb) {
    let validExtensions = [
      // images
      'jpg',
      'jpeg',
      'png',
      'gif',
    ];
    let ext = path.extname(file.originalname).replace('.', ''.toLowerCase());
    let isValid = validExtensions.indexOf(ext) >= 0;
    if (isValid) {
      cb(null, true);
    } else {
      cb(new ClientError(`File format ${ext} is not acceptable`), false);
    }
  },
});

@controller(`${APP_PATH}/api`)
export class LogoController extends BaseController {
  constructor(private _customerRepo: CustomerRepository) {
    super();
  }

  @httpPut(
    '/logo',
    authenticationRequired,
    adminRequired,
    upload.single('logo')
  )
  async logo(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let file = req.file;
    let fileExtension = path.extname(file.filename);
    let tnFilename = `logo_tn${fileExtension}`;
    let rootUrl = this.getAbsoluteAppRoot(req);
    await sharp(file.path)
      .resize(Defaults.LOGO_MAX_WIDTH, Defaults.LOGO_MAX_HEIGHT)
      .max()
      .toFile(`${file.destination}/${tnFilename}`);

    sharp.cache(false); // disable cache
    let customer = {
      logoFileName: file.filename,
      logoThumbnailName: tnFilename,
    } as Customer;
    await this._customerRepo.update(customer, customerId);
    okay(res, {
      logoFileName: file.filename,
      logoThumbnailName: tnFilename,
      logoThumbnailLocation: `${rootUrl}/api/staticfiles/${customerId}/${tnFilename}`,
    });
  }
}
