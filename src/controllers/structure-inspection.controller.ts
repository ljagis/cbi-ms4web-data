import { Structure, StructureInspection } from '../models';
import {
  controller,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';

import { APP_PATH } from '../shared';
import { AssetInspectionController } from './asset-inspection.controller';
import { authenticationRequired } from 'express-stormpath';
import { enforceFeaturePermission } from '../middleware';
import { memoryMulterInstance } from './helpers';

const CONTROLLER_BASE_ROUTE = 'structures';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class StructureInspectionController extends AssetInspectionController {
  constructor() {
    super(Structure, StructureInspection);
  }

  @httpGet('/:structureId([0-9]+)/inspections', authenticationRequired)
  index() {
    return super.index.apply(this, arguments);
  }

  @httpGet(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired
  )
  single() {
    return super.single.apply(this, arguments);
  }

  @httpGet(
    '/:structureId([0-9]+)/inspections/details/:id([0-9]+)',
    authenticationRequired
  )
  @httpGet(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)/details',
    authenticationRequired
  )
  details() {
    return super.details.apply(this, arguments);
  }

  @httpPut(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }

  @httpPost(
    '/:structureId([0-9]+)/inspections',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  create() {
    return super.create.apply(this, arguments);
  }

  @httpDelete(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'delete')
  )
  del() {
    return super.del.apply(this, arguments);
  }

  @httpPost(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)/pdf',
    authenticationRequired
  )
  pdf() {
    return super.pdf.apply(this, arguments);
  }

  @httpPost('/:structureId([0-9]+)/inspections/pdf', authenticationRequired)
  multiPdf() {
    return super.multiPdf.apply(this, arguments);
  }

  @httpPost(
    '/:structureId([0-9]+)/inspections/:id([0-9]+)/email',
    authenticationRequired,
    memoryMulterInstance.array('attachments', 10)
  )
  email() {
    return super.email.apply(this, arguments);
  }
}
