import * as Knex from 'knex';
import * as _ from 'lodash';
import * as data from '../data';
import * as dh from '../decorators';
import * as express from 'express';
import * as shared from '../shared';

import { AssetReport, ContactReport } from '../models/reports';
import {
  ComplianceStatus,
  ConstructionSite,
  ConstructionSiteInspection,
  Contact,
  EntityContact,
  Facility,
  FacilityInspection,
  Structure,
  StructureInspection,
  User,
} from '../models';
import { controller, httpGet } from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';
import { inject } from 'inversify';

import { filterComplianceStatus } from '../shared';

const CONTROLLER_BASE_ROUTE = 'contactreport';

interface AllModels {
  complianceStatus?: ComplianceStatus[];
  contact?: Contact;
  users?: User[];
  constructionSites?: ConstructionSite[];
  facilities?: Facility[];
  structures?: Structure[];
  constructionSiteInspections?: ConstructionSiteInspection[];
  facilityInspections?: FacilityInspection[];
  structureInspections?: StructureInspection[];
}

@controller(`${shared.APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class ContactReportController extends BaseController {
  @inject(Knex) private _knex: Knex;

  constructor(
    private _constructionSiteInspectionRepo: data.ConstructionSiteInspectionRepository,
    private _constructionSiteRepo: data.ConstructionSiteRepository,
    private _contactRepo: data.ContactRepository,
    private _facilityInspectionRepo: data.FacilityInspectionRepository,
    private _facilityRepo: data.FacilityRepository,
    private _structureInspectionRepo: data.StructureInspectionRepository,
    private _structureRepo: data.StructureRepository,
    private _userRepo: data.UserRepository,
    private _complianceStatusRepo: data.ComplianceStatusRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const contactId = +req.query.contactid;
    const customerId = await this.getCustomerId(req);
    if (!contactId) {
      throw new shared.ClientError(`contactid querystring is required`);
    }
    const contact = await this._contactRepo.fetchById(contactId, customerId);
    if (!contact) {
      throw new shared.NotFoundError(`Contact ${contactId} not found.`);
    }

    const complianceStatus = await this._complianceStatusRepo.fetch();

    const allModels: AllModels = {
      contact,
      users: await this._userRepo.fetch(customerId),
      complianceStatus: complianceStatus.filter(
        filterComplianceStatus(customerId)
      ),
    };

    const selectGlobalIdFkQb = this._knex(dh.getTableName(Contact))
      .select(
        dh.getSqlField<EntityContact>(EntityContact, e => e.globalIdFk, true)
      )
      .innerJoin(
        dh.getTableName(EntityContact),
        dh.getSqlField<Contact>(Contact, c => c.id, true),
        dh.getSqlField<EntityContact>(EntityContact, e => e.contactId, true)
      )
      .where(dh.getSqlField<Contact>(Contact, c => c.id, true), contactId);

    allModels.constructionSites = await this._constructionSiteRepo.fetch(
      customerId,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<ConstructionSite>(ConstructionSite, c => c.globalId),
          selectGlobalIdFkQb
        );
        return qb;
      }
    );
    allModels.facilities = await this._facilityRepo.fetch(
      customerId,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<Facility>(Facility, c => c.globalId),
          selectGlobalIdFkQb
        );
        return qb;
      }
    );
    allModels.structures = await this._structureRepo.fetch(
      customerId,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<Structure>(Structure, c => c.globalId),
          selectGlobalIdFkQb
        );
        return qb;
      }
    );

    const constructionSiteIds = allModels.constructionSites.map(cs => cs.id);
    const facilityIds = allModels.facilities.map(f => f.id);
    const structureIds = allModels.structures.map(s => s.id);
    allModels.constructionSiteInspections = await this._constructionSiteInspectionRepo.fetch(
      customerId,
      undefined,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<ConstructionSiteInspection>(
            ConstructionSiteInspection,
            i => i.constructionSiteId
          ),
          constructionSiteIds
        );
        return qb;
      }
    );
    allModels.facilityInspections = await this._facilityInspectionRepo.fetch(
      customerId,
      undefined,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<FacilityInspection>(
            FacilityInspection,
            i => i.facilityId
          ),
          facilityIds
        );
        return qb;
      }
    );
    allModels.structureInspections = await this._structureInspectionRepo.fetch(
      customerId,
      undefined,
      undefined,
      qb => {
        qb.whereIn(
          dh.getSqlField<StructureInspection>(
            StructureInspection,
            i => i.structureId
          ),
          structureIds
        );
        return qb;
      }
    );

    const csReports = allModels.constructionSites.map(cs => {
      return {
        _inspections: _.filter(
          allModels.constructionSiteInspections,
          i => i.constructionSiteId === cs.id
        ),
        _assetType: shared.EntityTypes.ConstructionSite,
        _name: cs.name,
        ...cs,
      } as AssetReport;
    });
    const facilityReports = allModels.facilities.map(fac => {
      return {
        _inspections: _.filter(
          allModels.facilityInspections,
          i => i.facilityId === fac.id
        ),
        _assetType: shared.EntityTypes.Facility,
        _name: fac.name,
        ...fac,
      } as AssetReport;
    });
    const structureReports = allModels.structures.map(struc => {
      return {
        _inspections: _.filter(
          allModels.structureInspections,
          i => i.structureId === struc.id
        ),
        _assetType: shared.EntityTypes.Structure,
        _name: struc.name,
        ...struc,
      } as AssetReport;
    });
    const contactReport: ContactReport = {
      contact: allModels.contact,
      complianceStatus: allModels.complianceStatus,
      assets: [...csReports, ...facilityReports, ...structureReports],
      users: allModels.users,
    };

    shared.okay(res, contactReport);
  }
}
