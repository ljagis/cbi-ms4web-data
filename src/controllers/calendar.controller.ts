import { getSqlField } from './../decorators';
import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { controller, httpGet } from 'inversify-express-utils';
import { APP_PATH, okay } from '../shared';
import { authenticationRequired } from 'express-stormpath';
import { inject } from 'inversify';
import * as _ from 'lodash';
import * as moment from 'moment';

import {
  CitizenReportRepository,
  ConstructionSiteRepository,
  FacilityRepository,
  IllicitDischargeRepository,
  OutfallRepository,
  StructureRepository,
  ConstructionSiteInspectionRepository,
  FacilityInspectionRepository,
  OutfallInspectionRepository,
  StructureInspectionRepository,
  BmpActivityRepository,
  BmpDetailRepository,
} from '../data';

import {
  AssetQueryOptions,
  CitizenReportQueryOptions,
  CalendarIndexResponse,
  ConstructionSite,
  ConstructionSiteInspection,
  Facility,
  FacilityInspection,
  IllicitDischargeQueryOptions,
  InspectionQueryOptions,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
  BmpDetail,
  Principal,
  ConstructionSiteQueryOptions,
  OutfallQueryOptions,
  FacilityQueryOptions,
  StructureQueryOptions,
} from '../models';
import { QueryBuilder } from 'knex';

@controller(`${APP_PATH}/api/calendar`)
export class CalendarController extends BaseController {
  @inject(CitizenReportRepository) protected _crRepo: CitizenReportRepository;

  @inject(ConstructionSiteRepository)
  protected _csRepo: ConstructionSiteRepository;

  @inject(ConstructionSiteInspectionRepository)
  protected _csInspectionRepo: ConstructionSiteInspectionRepository;

  @inject(FacilityRepository) protected _facilityRepo: FacilityRepository;

  @inject(FacilityInspectionRepository)
  protected _facilityInspectionRepo: FacilityInspectionRepository;

  @inject(IllicitDischargeRepository)
  protected _illicitRepo: IllicitDischargeRepository;

  @inject(OutfallRepository) protected _outfallRepo: OutfallRepository;

  @inject(OutfallInspectionRepository)
  protected _outfallInspectionRepo: OutfallInspectionRepository;

  @inject(StructureRepository) protected _structureRepo: StructureRepository;

  @inject(StructureInspectionRepository)
  protected _structureInspectionRepo: StructureInspectionRepository;

  @inject(BmpActivityRepository)
  protected _bmpActivityRepo: BmpActivityRepository;

  @inject(BmpDetailRepository) protected _bmpDetailRepo: BmpDetailRepository;

  constructor() {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    let mFrom = moment().startOf('month');
    let mTo = moment().endOf('month');
    if (req.query.from) {
      mFrom = moment(req.query.from, moment.ISO_8601).startOf('month');
    }
    if (req.query.to) {
      mTo = moment(req.query.to, moment.ISO_8601).endOf('month');
    }
    const fromDate = mFrom.toDate();
    const toDate = mTo.toDate();

    const assetQOpts: AssetQueryOptions = {
      addedFrom: fromDate,
      addedTo: toDate,
      orderByField: 'dateAdded',
    };
    const csQueryOptions: ConstructionSiteQueryOptions = {
      ...assetQOpts,
    };
    const outfallQueryOptions: OutfallQueryOptions = {
      ...assetQOpts,
    };
    const facilityQueryOptions: FacilityQueryOptions = {
      ...assetQOpts,
    };
    const structureQueryOptions: StructureQueryOptions = {
      ...assetQOpts,
    };
    const crQueryOptions: CitizenReportQueryOptions = {
      reportedFrom: fromDate,
      reportedTo: toDate,
      followUpFrom: fromDate,
      followUpTo: toDate,
      ...assetQOpts,
    };

    const idQueryOptions: IllicitDischargeQueryOptions = {
      reportedFrom: fromDate,
      reportedTo: toDate,
      followUpFrom: fromDate,
      followUpTo: toDate,
      ...assetQOpts,
    };
    if (await principal.isContractor()) {
      csQueryOptions.contractorEmail = principal.details.email;
      outfallQueryOptions.contractorEmail = principal.details.email;
      facilityQueryOptions.contractorEmail = principal.details.email;
      structureQueryOptions.contractorEmail = principal.details.email;
      idQueryOptions.contractorEmail = principal.details.email;
      crQueryOptions.contractorEmail = principal.details.email;
    }

    let inspectionQueryOptions: InspectionQueryOptions = {
      inspectionDateFrom: fromDate,
      inspectionDateTo: toDate,
      scheduledInspectionDateFrom: fromDate,
      scheduledInspectionDateTo: toDate,
    };

    let csiQOpts = Object.assign({}, inspectionQueryOptions, {
      outFields: [
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.id,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.createdDate,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.inspectionDate,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.scheduledInspectionDate,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.complianceStatus,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.inspectorId,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.inspectorId2,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.followUpInspectionId,
          true,
          true
        ),
        getSqlField<ConstructionSiteInspection>(
          ConstructionSiteInspection,
          f => f.constructionSiteId,
          true,
          true
        ),
        getSqlField<ConstructionSite>(
          ConstructionSite,
          f => f.name,
          true,
          true
        ),
      ],
    });

    let fiQOpts = Object.assign({}, inspectionQueryOptions, {
      outFields: [
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.id,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.createdDate,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.inspectionDate,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.scheduledInspectionDate,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.complianceStatus,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.inspectorId,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.inspectorId2,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.followUpInspectionId,
          true,
          true
        ),
        getSqlField<FacilityInspection>(
          FacilityInspection,
          f => f.facilityId,
          true,
          true
        ),
        getSqlField<Facility>(Facility, f => f.name, true, true),
      ],
    });

    let oiQOpts = Object.assign({}, inspectionQueryOptions, {
      outFields: [
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.id,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.createdDate,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.inspectionDate,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.scheduledInspectionDate,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.complianceStatus,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.inspectorId,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.inspectorId2,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.followUpInspectionId,
          true,
          true
        ),
        getSqlField<OutfallInspection>(
          OutfallInspection,
          f => f.outfallId,
          true,
          true
        ),
        getSqlField<Outfall>(Outfall, f => f.location, true, true),
      ],
    });

    let siQOpts = Object.assign({}, inspectionQueryOptions, {
      outFields: [
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.id,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.createdDate,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.inspectionDate,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.scheduledInspectionDate,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.complianceStatus,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.inspectorId,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.inspectorId2,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.followUpInspectionId,
          true,
          true
        ),
        getSqlField<StructureInspection>(
          StructureInspection,
          f => f.structureId,
          true,
          true
        ),
        getSqlField<Structure>(Structure, f => f.name, true, true),
      ],
    });
    const bmpDetails = await this._bmpDetailRepo.fetch(customerId, qb => {
      return qb.where(function(this: QueryBuilder) {
        this.whereBetween(BmpDetail.sqlField(f => f.dueDate), [
          fromDate,
          toDate,
        ]).orWhereBetween(BmpDetail.sqlField(f => f.completionDate), [
          fromDate,
          toDate,
        ]);
      });
    });
    const constructionSites = await this._csRepo.fetch(
      customerId,
      csQueryOptions
    );
    const facilities = await this._facilityRepo.fetch(
      customerId,
      facilityQueryOptions
    );
    const outfalls = await this._outfallRepo.fetch(
      customerId,
      outfallQueryOptions
    );
    const structures = await this._structureRepo.fetch(
      customerId,
      structureQueryOptions
    );
    const citizenReports = await this._crRepo.fetch(customerId, crQueryOptions);
    const illicitDischarges = await this._illicitRepo.fetch(
      customerId,
      idQueryOptions
    );
    const constructionSiteInspections = await this._csInspectionRepo.fetchWithoutMap(
      customerId,
      undefined,
      csiQOpts
    );
    const facilityInspections = await this._facilityInspectionRepo.fetchWithoutMap(
      customerId,
      undefined,
      fiQOpts
    );
    const outfallInspections = await this._outfallInspectionRepo.fetchWithoutMap(
      customerId,
      undefined,
      oiQOpts
    );
    const structureInspections = await this._structureInspectionRepo.fetchWithoutMap(
      customerId,
      undefined,
      siQOpts
    );

    let calendarIndexResponse: CalendarIndexResponse = {};
    let dateAddedGroupByIterator = asset =>
      moment.utc(asset.dateAdded).toISOString();
    let dateReportedGroupByIterator = asset =>
      moment.utc(asset.dateReported).toISOString();
    let followUpDateGroupByIterator = asset => {
      return moment.utc(asset.followUpDate).toISOString();
    };

    let dateDueGroupByIterator = asset =>
      moment.utc(asset.dueDate).toISOString();
    let dateCompletedGroupByIterator = asset =>
      moment.utc(asset.completionDate).toISOString();

    let inspectionOrScheduledInspectionDateGroupByIterator = inspection => {
      if (inspection.inspectionDate) {
        return moment.utc(inspection.inspectionDate).toISOString();
      }
      return moment.utc(inspection.scheduledInspectionDate).toISOString();
    };
    const reduceBmpDetails = (details: BmpDetail[]) => {
      _(details)
        .filter(d => d.dueDate)
        .groupBy(dateDueGroupByIterator)
        .each((values, dateKey) => {
          calendarIndexResponse[dateKey] = calendarIndexResponse[dateKey] || {};
          const newDetails = (
            calendarIndexResponse[dateKey].bmpDetails || []
          ).concat(values);
          calendarIndexResponse[dateKey].bmpDetails = _.uniqBy<BmpDetail>(
            newDetails,
            vals => vals.id
          );
        });
      _(details)
        .filter(d => d.completionDate)
        .groupBy(dateCompletedGroupByIterator)
        .each((values, dateKey) => {
          calendarIndexResponse[dateKey] = calendarIndexResponse[dateKey] || {};
          const newDetails = (
            calendarIndexResponse[dateKey].bmpDetails || []
          ).concat(values);
          calendarIndexResponse[dateKey].bmpDetails = _.uniqBy<BmpDetail>(
            newDetails,
            vals => vals.id
          );
        });
    };
    /**
     * Groups assets by date, then assetKey
     */
    let reduceAsset = (assets: any[], assetKey: string) => {
      _(assets)
        .groupBy(dateAddedGroupByIterator)
        .reduce((memo, value, key) => {
          memo[key] = memo[key] || {};
          memo[key][assetKey] = value;
          return memo;
        }, calendarIndexResponse);
    };

    let reduceInvestigationByDateReported = (
      investigations: any[],
      assetKey: string
    ) => {
      _(investigations)
        .groupBy(dateReportedGroupByIterator)
        .reduce((memo, value, key) => {
          memo[key] = memo[key] || {};
          memo[key][assetKey] = value;
          return memo;
        }, calendarIndexResponse);
    };

    let reduceInvestigationByFollowUpDate = (
      investigations: any[],
      assetKey: string
    ) => {
      _(investigations)
        .groupBy(followUpDateGroupByIterator)
        .reduce((memo, value, key) => {
          memo[key] = memo[key] || {};
          memo[key][assetKey] = value;
          return memo;
        }, calendarIndexResponse);
    };

    let reduceInspections = (inspections: any[], assetKey: string) => {
      _(inspections)
        .groupBy(inspectionOrScheduledInspectionDateGroupByIterator)
        .reduce((memo, value, key) => {
          memo[key] = memo[key] || {};
          memo[key][assetKey] = value;
          return memo;
        }, calendarIndexResponse);
    };

    reduceBmpDetails(bmpDetails);

    reduceAsset(constructionSites, 'constructionSites');
    reduceAsset(facilities, 'facilities');
    // reduceAsset(monitoringLocations, 'monitoringLocations');
    reduceAsset(outfalls, 'outfalls');
    reduceAsset(structures, 'structures');

    reduceInvestigationByDateReported(citizenReports, 'citizenReports');
    reduceInvestigationByDateReported(illicitDischarges, 'illicitDischarges');
    reduceInvestigationByFollowUpDate(citizenReports, 'citizenReports');
    reduceInvestigationByFollowUpDate(illicitDischarges, 'illicitDischarges');

    reduceInspections(
      constructionSiteInspections,
      'constructionSiteInspections'
    );
    reduceInspections(facilityInspections, 'facilityInspections');
    reduceInspections(outfallInspections, 'outfallInspections');
    reduceInspections(structureInspections, 'structureInspections');
    okay(res, calendarIndexResponse);
  }
}
