import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { Structure, StructureInspection } from '../models';
import { InspectionCustomFieldController } from './inspection-custom-field.controller';
import { enforceFeaturePermission } from '../middleware';

const ASSET_ROUTE = 'structures';

@controller(`${APP_PATH}/api/${ASSET_ROUTE}`)
export class StructureInspectionCustomFieldController extends InspectionCustomFieldController {
  constructor() {
    super(Structure, StructureInspection);
  }

  @httpGet(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired
  )
  index() {
    return super.index.apply(this, arguments);
  }

  @httpPut(
    '/:assetId([0-9]+)/inspections/:inspectionId([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }
}
