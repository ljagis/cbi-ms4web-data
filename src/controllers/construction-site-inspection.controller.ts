import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { APP_PATH } from '../shared';
import { ConstructionSite, ConstructionSiteInspection } from '../models';

import { AssetInspectionController } from './asset-inspection.controller';
import { enforceFeaturePermission } from '../middleware';
import { memoryMulterInstance } from './helpers';

const CONTROLLER_BASE_ROUTE = 'constructionsites';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class ConstructionSiteInspectionController extends AssetInspectionController {
  constructor() {
    super(ConstructionSite, ConstructionSiteInspection);
  }

  @httpGet('/:constructionSiteId([0-9]+)/inspections', authenticationRequired)
  index() {
    return super.index.apply(this, arguments);
  }

  @httpGet(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired
  )
  single() {
    return super.single.apply(this, arguments);
  }

  @httpGet(
    '/:constructionSiteId([0-9]+)/inspections/details/:id([0-9]+)',
    authenticationRequired
  )
  @httpGet(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)/details',
    authenticationRequired
  )
  details() {
    return super.details.apply(this, arguments);
  }

  @httpPut(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }

  @httpPost(
    '/:constructionSiteId([0-9]+)/inspections',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit')
  )
  create() {
    return super.create.apply(this, arguments);
  }

  @httpDelete(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'delete')
  )
  del() {
    return super.del.apply(this, arguments);
  }

  @httpPost(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)/pdf',
    authenticationRequired
  )
  pdf() {
    return super.pdf.apply(this, arguments);
  }

  @httpPost(
    '/:constructionSiteId([0-9]+)/inspections/pdf',
    authenticationRequired
  )
  multiPdf() {
    return super.multiPdf.apply(this, arguments);
  }

  @httpPost(
    '/:constructionSiteId([0-9]+)/inspections/:id([0-9]+)/email',
    authenticationRequired,
    memoryMulterInstance.array('attachments', 10)
  )
  email() {
    return super.email.apply(this, arguments);
  }
}
