import { Request, Response, NextFunction } from 'express';
import { inject, unmanaged } from 'inversify';
import { BaseController } from './base.controller';
import * as Bluebird from 'bluebird';

import { FileRepository } from '../data';
import {
  BmpActivity,
  CitizenReport,
  ConstructionSite,
  ConstructionSiteInspection,
  Entity,
  Facility,
  FacilityInspection,
  IllicitDischarge,
  MonitoringLocation,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
  FileEntity,
  BmpDetail,
  SwmpActivity,
} from '../models';
import * as path from 'path';
import {
  EntityTypes,
  okay,
  notFound,
  Defaults,
  ClientError,
  fileToFileLink,
} from '../shared';

import * as sharp from 'sharp';

export abstract class EntityFileController extends BaseController {
  @inject(FileRepository) protected _fileRepo: FileRepository;

  protected EntityWithFile: { new (...args) };
  protected entityMap: { [key: string]: { new (...args): Entity } } = {};

  constructor(@unmanaged() protected entityType: string) {
    super();
    this.entityMap[EntityTypes.BmpActivity] = BmpActivity;
    this.entityMap[EntityTypes.BmpDetail] = BmpDetail;
    this.entityMap[EntityTypes.ConstructionSite] = ConstructionSite;
    this.entityMap[
      EntityTypes.ConstructionSiteInspection
    ] = ConstructionSiteInspection;
    this.entityMap[EntityTypes.Facility] = Facility;
    this.entityMap[EntityTypes.FacilityInspection] = FacilityInspection;
    this.entityMap[EntityTypes.Outfall] = Outfall;
    this.entityMap[EntityTypes.OutfallInspection] = OutfallInspection;
    this.entityMap[EntityTypes.Structure] = Structure;
    this.entityMap[EntityTypes.StructureInspection] = StructureInspection;
    this.entityMap[EntityTypes.MonitoringLocation] = MonitoringLocation;
    this.entityMap[EntityTypes.CitizenReport] = CitizenReport;
    this.entityMap[EntityTypes.IllicitDischarge] = IllicitDischarge;
    this.entityMap[EntityTypes.SwmpActivity] = SwmpActivity;

    this.EntityWithFile = this.entityMap[this.entityType];
    if (!this.EntityWithFile) {
      throw Error(`${entityType} is not implemented`);
    }
  }

  async create(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const rootUrl = this.getAbsoluteAppRoot(req);

    if (!Array.isArray(req.files)) {
      throw new ClientError(
        'Request files is not an array.  Please check your request parameters.'
      );
    }

    // upload.array changes the signature of req.files to File[]
    const files: Express.Multer.File[] = req.files;

    const insertedFiles = await Bluebird.mapSeries(files, async multerFile => {
      const fileExtension = path.extname(multerFile.filename);
      const guid = path.basename(multerFile.filename, fileExtension);
      const file = new FileEntity();
      file.addedBy = this.getUsername(req);
      file.entityType = this.entityType;
      file.fileName = multerFile.filename;
      file.originalFileName = multerFile.originalname;
      file.fileSize = multerFile.size;
      file.globalId = guid;
      file.mimeType = multerFile.mimetype;

      if (multerFile.mimetype.startsWith('image')) {
        /**
         * Since we're overwriting the original image, we need to output the
         * rotated image to a buffer so we're not simultaneously reading
         * from/writing to the same location
         */
        const buffer = await sharp(multerFile.path)
          .rotate()
          .resize(Defaults.IMAGE_MAX_WIDTH, Defaults.IMAGE_MAX_HEIGHT)
          .max()
          .jpeg({
            quality: Defaults.IMAGE_QUALITY,
          })
          .toBuffer();
        await sharp(buffer).toFile(multerFile.path);

        // create thumbnail
        // TODO: apparently this doesn't support bitmaps
        const tnFilename = `${guid}_tn.jpg`;

        await sharp(multerFile.path)
          .rotate()
          .resize(Defaults.THUMBNAIL_MAX_WIDTH, Defaults.THUMBNAIL_MAX_HEIGHT)
          .max()
          .jpeg({
            quality: Defaults.THUMBNAIL_QUALITY,
          })
          .toFile(`${multerFile.destination}/${tnFilename}`);

        file.thumbnailName = tnFilename;
      }
      const insertedFile = await this._fileRepo.insert(
        this.EntityWithFile,
        assetId,
        customerId,
        file
      );
      return insertedFile;
    });

    sharp.cache(false); // disable cache;

    const fileLinks = insertedFiles.map(f =>
      fileToFileLink(f, customerId, rootUrl)
    );
    res.json(fileLinks);
  }

  async indexAt(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let fileId = Number(req.params.fileId);
    let rootUrl = this.getAbsoluteAppRoot(req);
    const file = await this._fileRepo.fetchSingle(fileId, customerId);
    if (file) {
      let fileWithLinks = fileToFileLink(file, customerId, rootUrl);
      okay(res, fileWithLinks);
    } else {
      notFound(res);
    }
  }

  async index(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let assetId = Number(req.params.id);
    let rootUrl = this.getAbsoluteAppRoot(req);
    const files = await this._fileRepo.fetch(
      this.EntityWithFile,
      assetId,
      customerId
    );

    const fileLinks = files.map(file =>
      fileToFileLink(file, customerId, rootUrl)
    );
    okay(res, fileLinks);
  }

  async del(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    let fileId = Number(req.params.fileId);
    await this._fileRepo.deleteFile(this.EntityWithFile, fileId, customerId);
    okay(res, undefined);
  }
}
