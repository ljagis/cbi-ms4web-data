import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  created,
  createClassFromObject,
  notFound,
  NotFoundError,
} from '../shared';
import * as express from 'express';
import { BaseController } from './base.controller';
import { BmpControlMeasure, QueryOptions } from '../models';
import { BmpControlMeasureRepository, BmpDetailRepository } from '../data';
import { inject } from 'inversify';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'bmpcontrolmeasures';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpControlMeasureController extends BaseController {
  @inject(BmpControlMeasureRepository)
  protected _cmRepo: BmpControlMeasureRepository;

  constructor(private _bmpDetailRepo: BmpDetailRepository) {
    super();
  }

  @httpGet('/', authenticationRequired, enforceFeaturePermission('bmp', 'view'))
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let queryOptions: QueryOptions = {
      limit: Number(req.query.limit),
      offset: Number(req.query.offset),
      orderByField: req.query.orderByField,
      orderByDirection: req.query.orderByDirection,
    };
    const items = await this._cmRepo.fetch(customerId, queryOptions);
    okay(res, items);
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let currentCustomer = await this.getCustomerId(req);
    let controlMeasure = createClassFromObject<BmpControlMeasure>(
      BmpControlMeasure,
      req.body
    );
    // const user = this.httpContext.user.details as Principal;
    // if (user.details && user.details.role) {
    //   roleFeaturePermissions[user.details.role].
    // }
    await this.validate(controlMeasure);
    const c = await this._cmRepo.insert(controlMeasure, currentCustomer);
    created(res, c);
  }

  @httpGet(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'view')
  )
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const item = await this._cmRepo.fetchById(id, customerId);
    if (item) {
      okay(res, item);
    } else {
      notFound(res);
    }
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async update(req: express.Request, res: express.Response) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);

    const existingCm = await this._cmRepo.fetchById(id, customerId);
    if (!existingCm) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    const updateCm = Object.assign(
      new BmpControlMeasure(),
      existingCm,
      req.body
    );
    await this.validate(updateCm);
    await this._cmRepo.update(id, req.body, customerId);

    const returnedCm = await this._cmRepo.fetchById(id, customerId);

    okay(res, returnedCm);
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'delete')
  )
  async del(req: express.Request, res: express.Response) {
    const id = Number(req.params.id);
    let customerId = await this.getCustomerId(req);
    const controlMeasure = await this._cmRepo.fetchById(id, customerId);
    if (!controlMeasure) {
      throw new NotFoundError(`Resource ${id} not found`);
    }

    const details = await this._bmpDetailRepo.fetch(customerId);
    const filteredDetails = details.filter(
      detail => detail.controlMeasureId === id
    );
    if (filteredDetails.length > 0) {
      throw Error(
        `${
          controlMeasure.name
        } cannot be deleted because there are BMP Details Associated with this Control Measure.`
      );
    }
    await this._cmRepo.del(id, customerId);
    okay(res);
  }
}
