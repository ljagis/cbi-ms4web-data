import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as moment from 'moment';
import * as models from '../models';
import * as Promise from 'bluebird';
import * as _ from 'lodash';

chai.use(ChaiHttp);

let controllerRoute = 'bmpcontrolmeasures';
let postCm: Partial<models.BmpControlMeasure> = {
  name: 'Public Education, Outreach and Involvement',
  sort: null,
};
let putCm: Partial<models.BmpControlMeasure> = {
  name: 'Public Education, Outreach and Involvement 2',
};
let createdId: number;

describe(`CRUD Control Measure`, () => {
  it(`should POST Control Measure`, () => {
    return chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postCm)
      .then(res => {
        createdId = +res.body.id;
        chai.expect(res).to.have.status(201);
        chai.expect(res).to.be.json;
        chai.expect(createdId).to.be.greaterThan(0);
      });
  });

  it(`should GET Control Measure to verify created data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(postCm).forEach(k => {
          if (postCm[k] instanceof Date) {
            chai.assert(moment(postCm[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(postCm[k]);
          }
        });
      });
  });

  it(`should GET Control Measure index`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
        chai.expect(res.body).to.be.instanceof(Array);
        chai.expect(res.body).to.have.length.above(0);
        let cm = _.find(res.body, {
          id: createdId,
        });
        let actual = _.extend(
          {
            id: createdId,
          },
          postCm
        );
        chai.expect(cm).to.deep.equal(actual);
      });
  });

  it(`should PUT Control Measure`, () => {
    return chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(putCm)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });

  it(`should GET Control Measure to verify modified data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(putCm).forEach(k => {
          if (putCm[k] instanceof Date) {
            chai.assert(moment(putCm[k]).isSame(moment(res.body[k])));
          } else {
            chai.expect(res.body[k]).equals(putCm[k]);
          }
        });
      });
  });

  it(`should DELETE Control Measure`, () => {
    if (createdId > 0) {
      return chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
        });
    }
    return Promise.resolve();
  });
});
