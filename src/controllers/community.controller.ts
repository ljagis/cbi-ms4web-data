import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { APP_PATH } from '../shared';
import * as express from 'express';
import { CustomerLookupController } from './customer-lookup.controller';
import { Community } from '../models';

const CONTROLLER_BASE_ROUTE = 'communities';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class CommunityController extends CustomerLookupController {
  constructor() {
    super(Community);
  }

  @httpGet('/', authenticationRequired)
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.index.apply(this, arguments);
  }

  @httpPost('/', authenticationRequired)
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.create.apply(this, arguments);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired)
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    return super.update.apply(this, arguments);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired)
  del(req: express.Request, res: express.Response, next: express.NextFunction) {
    return super.del.apply(this, arguments);
  }
}
