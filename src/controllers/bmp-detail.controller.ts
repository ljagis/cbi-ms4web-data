import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  createClassFromObject,
  validateModel,
  NotFoundError,
  created,
  notFound,
} from '../shared';
import {
  BmpDetailRepository,
  BmpActivityLogRepository,
  FileRepository,
} from '../data';
import { authenticationRequired } from 'express-stormpath';
import { getCustomerId } from '../shared';
import * as express from 'express';
import { BmpDetail } from '../models';
import { enforceFeaturePermission } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'bmpdetails';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class BmpDetailController {
  constructor(
    private _repo: BmpDetailRepository,
    private _activityRepo: BmpActivityLogRepository,
    private _fileRepo: FileRepository
  ) {}

  @httpGet('/', authenticationRequired, enforceFeaturePermission('bmp', 'view'))
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    const bmpDetails = await this._repo.fetch(customerId);
    okay(res, bmpDetails);
  }

  @httpGet(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'view')
  )
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = +req.params.id;
    const detail = await this._repo.fetchById(customerId, id);
    if (detail) {
      okay(res, detail);
    } else {
      notFound(res);
    }
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    const bmpDetail = createClassFromObject<BmpDetail>(BmpDetail, req.body);
    await validateModel(bmpDetail);
    const returnedDetail = await this._repo.insert(customerId, bmpDetail);
    created(res, returnedDetail);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await getCustomerId(req);
    let id = Number(req.params.id);
    let bmpDetail = await this._repo.fetchById(customerId, id);
    if (!bmpDetail) {
      throw new NotFoundError(`Bmp Detail not found`);
    }
    const updatedDetail = Object.assign(new BmpDetail(), bmpDetail, req.body);
    await validateModel(updatedDetail);
    await this._repo.update(customerId, id, req.body);
    bmpDetail = await this._repo.fetchById(customerId, id);
    okay(res, bmpDetail);
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('bmp', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    let customerId = await getCustomerId(req);
    const detail = await this._repo.fetchById(customerId, id);
    const activityLogs = await this._activityRepo.fetchByDetail(customerId, id);
    const files = await this._fileRepo.fetch(BmpDetail, id, customerId);
    if (activityLogs.length > 0 || files.length > 0) {
      throw Error(
        `${
          detail.name
        } cannot be deleted, make sure there are no Activity Logs or Files associated with this BMP Detail.`
      );
    }
    await this._repo.delete(customerId, id);
    okay(res);
  }
}
