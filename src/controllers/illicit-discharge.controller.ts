import * as express from 'express';
import * as fs from 'fs-extra';
import * as moment from 'moment';

import {
  APP_PATH,
  EntityTypes,
  NotFoundError,
  createClassFromObject,
  fileToFileLink,
} from '../shared';
import {
  CommunityRepository,
  ConstructionSiteRepository,
  ContactRepository,
  CustomFieldRepository,
  FileRepository,
  IllicitDischargeRepository,
  ReceivingWaterRepository,
  UserRepository,
  WatershedRepository,
} from '../data';
import {
  ConstructionSite,
  FileEntity,
  FileWithLinks,
  IllicitDischarge,
  IllicitDischargeQueryOptions,
  Principal,
} from '../models';
import {
  controller,
  httpDelete,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';

import { BaseController } from './base.controller';
import { IllicitDischargeReport } from '../reports/illicit-discharge-report';
import { authenticationRequired } from 'express-stormpath';
import { getTempPath } from '../shared/path-utils';

const BASE_ROUTE = `${APP_PATH}/api/illicitdischarges`;

@controller(BASE_ROUTE)
export class IllicitDischargeController extends BaseController {
  constructor(
    private _illicitRepo: IllicitDischargeRepository,
    private _csRepo: ConstructionSiteRepository,
    private _contactRepo: ContactRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _userRepo: UserRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _illicitDischargeReport: IllicitDischargeReport
  ) {
    super();
  }

  @httpGet(
    '/',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const queryOptions: IllicitDischargeQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const assets = await this._illicitRepo.fetch(currentCustomer, queryOptions);
    return this.ok(assets);
  }

  @httpGet(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: IllicitDischargeQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const asset = await this._illicitRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (asset) {
      return this.ok(asset);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet(
    '/:id([0-9]+)/details',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'view')
  )
  async details(req: express.Request) {
    const details = await this.getDetails(req);
    return this.ok(details);
  }

  @httpGet('/:id([0-9]+)/pdf', authenticationRequired)
  async pdf(req: express.Request, res: express.Response) {
    const { selectedCustomer } = req.principal;
    const details = await this.getDetails(req);
    const investigatorId = details.illicitDischarge.investigatorId;
    let investigatorName = null;

    if (investigatorId) {
      const investigator = await this._userRepo.fetchById(investigatorId);
      investigatorName = investigator ? investigator.fullName : null;
    }

    const dataStream = await this._illicitDischargeReport.generate(
      selectedCustomer,
      details,
      investigatorName
    );

    const tempPath = getTempPath();
    const { dateReported } = details.illicitDischarge;
    const formattedDate = moment(new Date(dateReported)).format('MM-DD-YYYY');
    const fileName = `Illicit Discharge Report ${formattedDate}.pdf`;
    const outputFilePath = `${tempPath}/${fileName}`;

    const writeFile = async (inputStream: any, filePath: string) =>
      new Promise((resolve, reject) => {
        const writeStream = fs.createWriteStream(filePath);
        // prettier-ignore
        inputStream
          .pipe(writeStream)
          .on('finish', resolve)
          .on('error', reject);
        inputStream.end();
      });

    await writeFile(dataStream, outputFilePath);

    return this.ok({
      fileName,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', fileName].join(
        '/'
      ),
    });
  }

  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit'),
    customerAssetLimit(EntityTypes.IllicitDischarge)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(IllicitDischarge, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._illicitRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: IllicitDischargeQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._illicitRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._illicitRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._illicitRepo.fetchById(
        assetId,
        customerId
      );
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'delete')
  )
  async del(req) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: IllicitDischargeQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const id = await this._illicitRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (id) {
      await this._illicitRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }

  private async getDetails(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const queryOptions: IllicitDischargeQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const illicitDischarge = await this._illicitRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (!illicitDischarge) {
      throw new NotFoundError();
    }

    let constructionSite = null;
    let contacts = [];

    if (illicitDischarge.constructionSiteId) {
      constructionSite = await this._csRepo.fetchById(
        illicitDischarge.constructionSiteId,
        customerId
      );
      contacts = await this._contactRepo.fetchContactsForEntity(
        ConstructionSite,
        illicitDischarge.constructionSiteId,
        customerId
      );
    }

    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForInvestigation(
      IllicitDischarge,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(IllicitDischarge, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );

    return {
      illicitDischarge,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
      constructionSite,
      contacts,
    };
  }
}
