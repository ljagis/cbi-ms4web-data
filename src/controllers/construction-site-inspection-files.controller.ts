import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'constructionsites';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class ConstructionSiteInspectionFilesController extends EntityFileController {
  constructor() {
    super(EntityTypes.ConstructionSiteInspection);
  }

  @httpPost(
    '/:constructionSiteId/inspections/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet(
    '/:constructionSiteId/inspections/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired
  )
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet(
    '/:constructionSiteId/inspections/:id([0-9]+)/files',
    authenticationRequired
  )
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:constructionSiteId/inspections/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('inspections', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
