import { CustomFieldValueMap, CustomFieldValue } from '../models';
import { Request, Response, NextFunction } from 'express';
import { inject, unmanaged } from 'inversify';
import { BaseController } from './base.controller';

import {
  CommunityRepository,
  ContactRepository,
  CitizenReportRepository,
  ConstructionSiteRepository,
  ConstructionSiteInspectionRepository,
  CustomFieldRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  IllicitDischargeRepository,
  MonitoringLocationRepository,
  OutfallRepository,
  OutfallInspectionRepository,
  StructureRepository,
  StructureInspectionRepository,
} from '../data';
import {
  CitizenReport,
  ConstructionSite,
  ConstructionSiteInspection,
  Entity,
  Facility,
  FacilityInspection,
  IllicitDischarge,
  MonitoringLocation,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
} from '../models';
import { EntityTypes, okay, NotFoundError } from '../shared';

export abstract class EntityCustomFieldController extends BaseController {
  @inject(CommunityRepository) protected _communityRepo: CommunityRepository;

  @inject(ContactRepository) protected _contactRepo: ContactRepository;

  @inject(CitizenReportRepository) protected _crRepo: CitizenReportRepository;

  @inject(ConstructionSiteRepository)
  protected _csRepo: ConstructionSiteRepository;

  @inject(ConstructionSiteInspectionRepository)
  protected _csInspectionRepo: ConstructionSiteInspectionRepository;

  @inject(FacilityInspectionRepository)
  protected _facilityInspectionRepo: FacilityInspectionRepository;

  @inject(FacilityRepository) protected _facilityRepo: FacilityRepository;

  @inject(IllicitDischargeRepository)
  protected _illicitRepo: IllicitDischargeRepository;

  @inject(MonitoringLocationRepository)
  protected _monitoringLocRepo: MonitoringLocationRepository;

  @inject(OutfallRepository) protected _outfallRepo: OutfallRepository;

  @inject(OutfallInspectionRepository)
  protected _outfallInspectionRepo: OutfallInspectionRepository;

  @inject(StructureRepository) protected _structureRepo: StructureRepository;

  @inject(StructureInspectionRepository)
  protected _structureInspectionRepo: StructureInspectionRepository;

  @inject(CustomFieldRepository)
  protected _customFieldRepo: CustomFieldRepository;

  protected EntityWithCustomField: { new (...args): Entity };
  protected entityMap: { [key: string]: { new (...args): Entity } } = {};

  constructor(@unmanaged() protected entityType: string) {
    super();
    this.entityMap[EntityTypes.ConstructionSite] = ConstructionSite;
    this.entityMap[
      EntityTypes.ConstructionSiteInspection
    ] = ConstructionSiteInspection;
    this.entityMap[EntityTypes.Facility] = Facility;
    this.entityMap[EntityTypes.FacilityInspection] = FacilityInspection;
    this.entityMap[EntityTypes.Outfall] = Outfall;
    this.entityMap[EntityTypes.OutfallInspection] = OutfallInspection;
    this.entityMap[EntityTypes.Structure] = Structure;
    this.entityMap[EntityTypes.StructureInspection] = StructureInspection;
    this.entityMap[EntityTypes.MonitoringLocation] = MonitoringLocation;
    this.entityMap[EntityTypes.CitizenReport] = CitizenReport;
    this.entityMap[EntityTypes.IllicitDischarge] = IllicitDischarge;

    this.EntityWithCustomField = this.entityMap[this.entityType];
    if (!this.EntityWithCustomField) {
      throw Error(`${entityType} custom field is not implemented`);
    }
  }

  async index(req: Request, res: Response, next: NextFunction) {
    const entityId = Number(req.params.id);
    let customerId = await this.getCustomerId(req);
    const entity = await this.fetchEntity(entityId, customerId);
    if (!entity) {
      throw new NotFoundError('Resource not found');
    }
    let customFields: CustomFieldValue[];
    if (
      this.EntityWithCustomField === CitizenReport ||
      this.EntityWithCustomField === IllicitDischarge
    ) {
      customFields = await this._customFieldRepo.getCustomFieldValuesForInvestigation(
        this.EntityWithCustomField as any,
        entityId,
        customerId
      );
    } else {
      customFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
        this.EntityWithCustomField,
        entityId,
        customerId
      );
    }
    okay(res, customFields);
  }

  async update(req: Request, res: Response, next: NextFunction) {
    let customerId = await this.getCustomerId(req);
    const entityId = Number(req.params.id);
    let data: CustomFieldValueMap[] = req.body;

    const entity = await this.fetchEntity(entityId, customerId);
    if (!entity) {
      throw new NotFoundError('Resource not found');
    }
    if (
      this.EntityWithCustomField === CitizenReport ||
      this.EntityWithCustomField === IllicitDischarge
    ) {
      await this._customFieldRepo.updateCustomFieldValuesForInvestigation(
        this.EntityWithCustomField as any,
        entityId,
        data,
        customerId
      );
    } else {
      await this._customFieldRepo.updateCustomFieldsForEntity(
        this.EntityWithCustomField,
        entityId,
        data,
        customerId
      );
    }
    okay(res);
  }

  async fetchEntity(entityId: number, customerId: string) {
    switch (this.entityType) {
      case EntityTypes.CitizenReport:
        return await this._crRepo.fetchById(entityId, customerId);
      case EntityTypes.ConstructionSite:
        return await this._csRepo.fetchById(entityId, customerId);
      case EntityTypes.Facility:
        return await this._facilityRepo.fetchById(entityId, customerId);
      case EntityTypes.IllicitDischarge:
        return await this._illicitRepo.fetchById(entityId, customerId);
      // case EntityTypes.MonitoringLocation:
      //   return await this._monitoringLocRepo.fetchById(entityId, customerId);
      case EntityTypes.Outfall:
        return await this._outfallRepo.fetchById(entityId, customerId);
      case EntityTypes.Structure:
        return await this._structureRepo.fetchById(entityId, customerId);
      default:
        throw Error('Not implemented');
    }
  }
}
