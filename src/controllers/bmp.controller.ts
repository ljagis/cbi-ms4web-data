import { BmpRepository } from '../data';
import { authenticationRequired } from 'express-stormpath';
import { APP_PATH, okay, createClassFromObject } from '../shared';
import {
  controller,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { Request, Response, NextFunction } from 'express';
import { BMP } from '../models';

const CONTROLLER_ROUTE = 'bmp';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class BmpController extends BaseController {
  constructor(private _bmpRepository: BmpRepository) {
    super();
  }

  @httpPost('/', authenticationRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const bmp = createClassFromObject(BMP, req.body);
    const createdBmp = await this._bmpRepository.create(bmp);

    okay(res, createdBmp);
  }

  @httpPut('/', authenticationRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const bmp = createClassFromObject(BMP, req.body);
    await this._bmpRepository.update(bmp);

    okay(res);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired)
  async del(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    await this._bmpRepository.delete(id);

    okay(res);
  }
}
