import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { EntityFileController } from './entity-file.controller';
import { enforceFeaturePermission } from '../middleware';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'structures';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class StructureFilesController extends EntityFileController {
  constructor() {
    super(EntityTypes.Structure);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files/:fileId([0-9]+)', authenticationRequired)
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files', authenticationRequired)
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
