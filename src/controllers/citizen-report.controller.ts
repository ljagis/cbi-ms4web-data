import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import {
  APP_PATH,
  EntityTypes,
  createClassFromObject,
  NotFoundError,
  fileToFileLink,
} from '../shared';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';

import * as express from 'express';
import {
  CitizenReport,
  CitizenReportDetailsResult,
  FileEntity,
  FileWithLinks,
  CitizenReportQueryOptions,
  Principal,
  ConstructionSite,
} from '../models';

import { BaseController } from './base.controller';
import {
  CommunityRepository,
  ReceivingWaterRepository,
  WatershedRepository,
  FileRepository,
  CustomFieldRepository,
  CitizenReportRepository,
  ConstructionSiteRepository,
  ContactRepository,
} from '../data';

const BASE_ROUTE = `${APP_PATH}/api/citizenreports`;

@controller(BASE_ROUTE)
export class CitizenReportController extends BaseController {
  constructor(
    private _crRepo: CitizenReportRepository,
    private _csRepo: ConstructionSiteRepository,
    private _contactRepo: ContactRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _customFieldRepo: CustomFieldRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const queryOptions: CitizenReportQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const assets = await this._crRepo.fetch(currentCustomer, queryOptions);
    return this.ok(assets);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: CitizenReportQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const asset = await this._crRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (asset) {
      return this.ok(asset);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet('/:id([0-9]+)/details', authenticationRequired)
  async details(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const queryOptions: CitizenReportQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const citizenReport = await this._crRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );

    if (!citizenReport) {
      throw new NotFoundError();
    }

    let constructionSite = null;
    let contacts = [];

    if (citizenReport.constructionSiteId) {
      constructionSite = await this._csRepo.fetchById(
        citizenReport.constructionSiteId,
        customerId
      );
      contacts = await this._contactRepo.fetchContactsForEntity(
        ConstructionSite,
        citizenReport.constructionSiteId,
        customerId
      );
    }

    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForInvestigation(
      CitizenReport,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(CitizenReport, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );
    const result: CitizenReportDetailsResult = {
      citizenReport,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
      constructionSite,
      contacts,
    };
    return this.ok(result);
  }

  /**
   * Creates a new Citizen Report.
   *
   * `POST /api/citizenreport`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit'),
    customerAssetLimit(EntityTypes.CitizenReport)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(CitizenReport, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._crRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: CitizenReportQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._crRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._crRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._crRepo.fetchById(assetId, customerId);
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const queryOptions: CitizenReportQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const asset = await this._crRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (asset) {
      await this._crRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }
}
