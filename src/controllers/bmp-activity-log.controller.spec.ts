import { config } from '../test/helper';
import { container } from '../inversify.config';
import { BmpDetail, BmpActivityLog, BmpControlMeasure } from '../models';
import { BmpActivityLogRepository } from '../data';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
chai.use(ChaiHttp);
import * as moment from 'moment';
import {
  createTempBmpDetail,
  createTestMCM,
  deleteMcm,
  deleteBmpDetail,
} from '../test/utils/bmp';

const customerId = config.customerId;

describe(`BMP Activity Log Controller`, async () => {
  const activityLogRepo = container.get(BmpActivityLogRepository);
  let baseRoute: string;
  // const detailRepo = kernel.get(BmpDetailRepository);

  let createdId: number;
  let testMcm: BmpControlMeasure;
  let testBmp: BmpDetail;

  before(async () => {
    // gets all control measures
    // controlMeasures = await cmRepo.fetch(customerId);
    testMcm = await createTestMCM();

    testBmp = await createTempBmpDetail(testMcm.id);

    baseRoute = `bmpdetails/${testBmp.id}/activitylogs`;
  });

  after(async () => {
    if (createdId) {
      await activityLogRepo.delete(config.customerId, createdId, {
        force: true,
      });
    }
    if (testBmp && testBmp.id) {
      await deleteBmpDetail(testBmp.id);
    }
    if (testMcm && testMcm.id) {
      await deleteMcm(testMcm.id);
    }
  });

  it(`should POST`, async () => {
    const expectedActivityDate = moment.utc('2017-09-01').toDate();
    const expectedDataType = 'Miles driven';
    const expectedQuantity = 5;
    const expectedComments = 'Some comment';
    const postData: Partial<BmpActivityLog> = {
      bmpDetailId: testBmp.id,
      activityDate: expectedActivityDate,
      dataType: expectedDataType,
      quantity: expectedQuantity,
      comments: expectedComments,
    };
    const response = await chai
      .request(config.baseUrl)
      .post(`/api/${baseRoute}`)
      .set('Cookie', config.cookie)
      .send(postData);

    const returnedLog = response.body as BmpActivityLog;
    createdId = +response.body.id;
    chai.expect(response).to.have.status(201);
    chai.expect(response).to.be.json;

    chai.expect(returnedLog.id).greaterThan(0);
    chai.expect(returnedLog.dateAdded).ok;
    chai
      .expect(moment(returnedLog.activityDate).toDate())
      .eql(expectedActivityDate);
    chai.expect(returnedLog.dataType).eq(expectedDataType);
    chai.expect(returnedLog.quantity).eq(expectedQuantity);
    chai.expect(returnedLog.comments).eq(expectedComments);
  });

  it(`should GET all`, async () => {
    const response = await chai
      .request(config.baseUrl)
      .get(`/api/${baseRoute}`)
      .set('Cookie', config.cookie);

    chai.expect(response).to.have.status(200);
    chai.expect(response).to.be.json;

    const dataType = (response.body as BmpActivityLog[]).find(
      log => log.id === createdId
    );
    chai.expect(dataType).ok;
  });

  it(`should PUT`, async () => {
    const expectedActivityDate = moment.utc('2017-09-02').toDate();
    const expectedDataType = 'Miles driven 2';
    const expectedQuantity = 7;
    const expectedComments = 'Some comment 2';
    const expectedLog: Partial<BmpActivityLog> = {
      activityDate: expectedActivityDate,
      dataType: expectedDataType,
      quantity: expectedQuantity,
      comments: expectedComments,
    };
    const resp = await chai
      .request(config.baseUrl)
      .put(`/api/${baseRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(expectedLog);

    const returnedLog: BmpActivityLog = resp.body;

    chai.expect(resp).json;
    chai
      .expect(moment(returnedLog.activityDate).toDate())
      .eql(expectedLog.activityDate);
    chai.expect(returnedLog.dataType).eq(expectedLog.dataType);
    chai.expect(returnedLog.quantity).eq(expectedLog.quantity);
    chai.expect(returnedLog.comments).eq(expectedLog.comments);
  });

  it(`should DELETE`, async () => {
    const resp = await chai
      .request(config.baseUrl)
      .del(`/api/${baseRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    const returnedDetail = await activityLogRepo.fetchById(
      customerId,
      createdId
    );
    chai.expect(resp).json;
    chai.expect(resp).status(200);
    chai.expect(returnedDetail).not.ok;
  });
});
