import { controller, httpPost } from 'inversify-express-utils';
import { BaseController } from './base.controller';
import { APP_PATH, ClientError } from '../shared';

import * as fs from 'fs';
import {
  BmpActivitySummaryReport,
  InspectionSummaryReport,
  SwmpAnnualReport,
} from '../reports';
import { authenticationRequired } from 'express-stormpath';
import { customerFeatureRequired } from '../middleware';
import { getTempPath } from '../shared/path-utils';
import * as uuid from 'uuid';
import * as moment from 'moment';
import { Request } from 'express';
import { Customer, AnnualReport } from '../models';
import { AnnualReportRepository } from '../data';

@controller(`${APP_PATH}/api/reports`)
export class ReportsController extends BaseController {
  constructor(
    private _annualReportRepository: AnnualReportRepository,
    private _bmpActivitySummaryRpt: BmpActivitySummaryReport,
    private _inspSummaryRpt: InspectionSummaryReport,
    private _swmpAnnualRpt: SwmpAnnualReport
  ) {
    super();
  }

  writePdf = async (pdfDoc: any, finalOutFile: string) => {
    return new Promise((resolve, reject) => {
      pdfDoc.on('error', err => {
        reject(err);
      });
      pdfDoc
        .pipe(fs.createWriteStream(finalOutFile))
        .on('finish', () => resolve());
      pdfDoc.end();
    });
  };

  @httpPost(
    '/bmp-activity-summary',
    authenticationRequired,
    customerFeatureRequired('reports')
  )
  async bmpActivitySummary(req: Request) {
    const { fromDate, toDate } = req.body;

    if (!moment(fromDate).isValid() || !moment(toDate).isValid()) {
      throw new ClientError('Invalid input');
    }

    const pdfDoc = await this._bmpActivitySummaryRpt.generate(
      req.principal.selectedCustomer,
      {
        activityFrom: fromDate,
        activityTo: toDate,
      }
    );
    const tempPath = getTempPath();
    const outFile = `bmp-activity-summary-${uuid.v1()}.pdf`;
    const finalOutFile = `${tempPath}/${outFile}`;

    await this.writePdf(pdfDoc, finalOutFile);

    return this.ok({
      fileName: outFile,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', outFile].join('/'),
    });
  }

  @httpPost(
    '/inspection-summary',
    authenticationRequired,
    customerFeatureRequired('reports')
  )
  async inspectionSummaryReport(req: Request) {
    const { fromDate, toDate, entityType } = req.body;
    if (
      !moment(fromDate).isValid() ||
      !moment(toDate).isValid() ||
      !entityType
    ) {
      throw new ClientError('Invalid input');
    }
    const customer: Customer = req.principal.selectedCustomer;

    const pdfDoc = await this._inspSummaryRpt.generate(customer, {
      from: fromDate,
      to: toDate,
      entityType,
    });

    const tempPath = getTempPath();
    const outFile = `${
      customer.id
    }-${entityType}-inspection-summary-${uuid.v1()}.pdf`;
    const finalOutFile = `${tempPath}/${outFile}`;

    await this.writePdf(pdfDoc, finalOutFile);

    return this.ok({
      fileName: outFile,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', outFile].join('/'),
    });
  }

  @httpPost(
    '/swmp-annual',
    authenticationRequired,
    customerFeatureRequired('reports')
  )
  async swmpAnnualReport(req: Request) {
    const customer: Customer = req.principal.selectedCustomer;
    const id = req.body.id;

    const report: AnnualReport = await this._annualReportRepository.fetchByIdAndCustomer(
      id,
      customer.id
    );

    const pdfDoc = await this._swmpAnnualRpt.generate(customer, id);

    const tempPath = getTempPath();
    const outFile = `${report.year} Annual Report.pdf`;
    const finalOutFile = `${tempPath}/${outFile}`;

    await this.writePdf(pdfDoc, finalOutFile);

    return this.ok({
      fileName: outFile,
      fileUrl: [this.getAbsoluteAppRoot(req), 'public/temp', outFile].join('/'),
    });
  }
}
