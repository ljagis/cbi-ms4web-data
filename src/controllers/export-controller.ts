import * as Bluebird from 'bluebird';
import * as JSZip from 'jszip';
import * as XLSX from 'xlsx';
import * as _ from 'lodash';
import * as express from 'express';
import * as fs from 'fs-extra';
import * as path from 'path';
import * as shared from '../shared';
import * as uuid from 'uuid';
import * as moment from 'moment';

import {
  AnnualReportRepository,
  BmpActivityLogRepository,
  BmpControlMeasureRepository,
  BmpDetailRepository,
  BmpRepository,
  CitizenReportRepository,
  CommunityRepository,
  ConstructionSiteInspectionRepository,
  ConstructionSiteRepository,
  ContactRepository,
  CustomerRepository,
  CustomFieldRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  IllicitDischargeRepository,
  InspectionTypeRepository,
  MgRepository,
  OutfallInspectionRepository,
  OutfallRepository,
  ProjectRepository,
  ReceivingWaterRepository,
  StructureInspectionRepository,
  StructureRepository,
  SwmpActivityRepository,
  SwmpRepository,
  WatershedRepository,
} from '../data';
import { controller, httpGet, httpPost } from 'inversify-express-utils';

import { APP_PATH } from '../shared/apppath';
import { BaseController } from './base.controller';
import { CustomField } from '../models';
import { authenticationRequired } from 'express-stormpath';

interface WorkBookWithName {
  workbook: XLSX.WorkBook;
  workbookName: string;
}

enum InspectionSheetName {
  ContructionSiteInspection = 'Construction Site Inspections',
  FacilityInspection = 'Facility Inspections',
  StructuralControlInspection = 'Structural Control Inspections',
  OutfallInspection = 'Outfall Inspections',
  CitizenReports = 'Citizen Reports',
  IllicitDischarges = 'Illicit Discharges',
}

// memoize pure function for performance
const columnForLabel = _.memoize(_columnForLabel);
const TEMP_SUFFIX_NAME = '.placeholder'; // temporary suffix name for exports that are processing

@controller(`${APP_PATH}/api/export`)
export class ExportController extends BaseController {
  constructor(
    private _bmpActLogRepo: BmpActivityLogRepository,
    private _bmpCMRepo: BmpControlMeasureRepository,
    private _bmpDetailRepo: BmpDetailRepository,
    private _communityRepo: CommunityRepository,
    private _contactRepo: ContactRepository,
    private _crRepo: CitizenReportRepository,
    private _csRepo: ConstructionSiteRepository,
    private _csInspRepo: ConstructionSiteInspectionRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _facilityRepo: FacilityRepository,
    private _facilityInspRepo: FacilityInspectionRepository,
    private _idRepo: IllicitDischargeRepository,
    private _inspTypeRepo: InspectionTypeRepository,
    private _outfallRepo: OutfallRepository,
    private _outfallInspRepo: OutfallInspectionRepository,
    private _projectRepo: ProjectRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _structureRepo: StructureRepository,
    private _structureInspRepo: StructureInspectionRepository,
    private _wsRepo: WatershedRepository,
    private _swmpRepo: SwmpRepository,
    private _swmpActivityRepo: SwmpActivityRepository,
    private _swmpAnnualReportRepo: AnnualReportRepository,
    private _bmpRepo: BmpRepository,
    private _mgRepo: MgRepository,
    private _customerRepository: CustomerRepository
  ) {
    super();
  }

  /**
   * Lists all exported files of the customer
   * @param req
   */
  @httpGet('/files', authenticationRequired)
  async files(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const tempCustomerPath = await getTempCustomerPath(customerId);
    const files = await fs.readdir(tempCustomerPath);
    const exportFiles = await Bluebird.reduce(
      files,
      async (memo, fileName) => {
        const fullPath = path.join(tempCustomerPath, fileName);
        const stat = await fs.stat(fullPath);
        // skip if directory or hiden file
        if (stat.isDirectory() || fileName.startsWith('.')) {
          return memo;
        }
        const isProcessing = fileName.endsWith(TEMP_SUFFIX_NAME);
        const link = !isProcessing
          ? getPublicExportLink(
              fileName,
              this.getAbsoluteAppRoot(req),
              customerId
            )
          : null;
        memo.push({
          name: fileName,
          isProcessing,
          size: stat.size,
          createdDate: stat.birthtime,
          link,
        });
        return memo;
      },
      [] as ExportFileInfo[]
    );
    return this.ok(exportFiles);
  }

  /**
   * initiates an export and immediately returns the temporary file.  The file
   * will be ready and the client will refresh the /files route for the files exported
   * @param req
   */
  @httpPost('/inspections', authenticationRequired)
  async inspections(req: express.Request) {
    req.setTimeout(500000, null);
    const customerId = await this.getCustomerId(req);
    const unique = uuid.v1(); // time-based uuid
    const outputFileName = `inspections_${unique}.zip`;
    const tempCustomerPath = await getTempCustomerPath(customerId);

    // create a temp placeholder file to let the client knows it's processing
    const tempCustomerFile = path.join(
      tempCustomerPath,
      `${outputFileName}${TEMP_SUFFIX_NAME}`
    );
    await fs.createFile(tempCustomerFile);

    // initiate the export, but don't wait for it to finish.
    this._writeInspectionsZip(
      customerId,
      outputFileName,
      tempCustomerPath
    ).then(async () => {
      await fs.unlink(tempCustomerFile); // remove temp placeholder file after processing is done
    });

    return this.ok({
      fileName: outputFileName,
      fileLink: getPublicExportLink(
        outputFileName,
        this.getAbsoluteAppRoot(req),
        customerId
      ),
    });
  }

  /**
   * export swmp data
   * @param req
   */
  @httpPost('/swmp', authenticationRequired)
  async swmp(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const unique = uuid.v1(); // time-based uuid
    const outputFileName = `swmp_${unique}.xlsx`;
    const tempCustomerPath = await getTempCustomerPath(customerId);

    // create a temp placeholder file to let the client knows it's processing
    const tempCustomerFile = path.join(
      tempCustomerPath,
      `${outputFileName}${TEMP_SUFFIX_NAME}`
    );
    await fs.createFile(tempCustomerFile);

    // initiate the export, but don't wait for it to finish
    this._writeSwmpZip(customerId, outputFileName, tempCustomerPath).then(
      async () => {
        await fs.unlink(tempCustomerFile); // remove temp placeholder file after processing is done
      }
    );

    return this.ok({
      fileName: outputFileName,
      fileLink: getPublicExportLink(
        outputFileName,
        this.getAbsoluteAppRoot(req),
        customerId
      ),
    });
  }

  /**
   * export all other data
   * @param req
   */
  @httpPost('/', authenticationRequired)
  async index(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const unique = uuid.v1(); // time-based uuid
    const outputFileName = `all_data_${unique}.xlsx`;
    const tempCustomerPath = await getTempCustomerPath(customerId);

    // create a temp placeholder file to let the client knows it's processing
    const tempCustomerFile = path.join(
      tempCustomerPath,
      `${outputFileName}${TEMP_SUFFIX_NAME}`
    );
    await fs.createFile(tempCustomerFile);

    // initiate the export, but don't wait for it to finish
    this._writeAllOthersZip(customerId, outputFileName, tempCustomerPath).then(
      async () => {
        await fs.unlink(tempCustomerFile); // remove temp placeholder file after processing is done
      }
    );

    return this.ok({
      fileName: outputFileName,
      fileLink: getPublicExportLink(
        outputFileName,
        this.getAbsoluteAppRoot(req),
        customerId
      ),
    });
  }

  private async _writeAllOthersZip(
    customerId: string,
    outputFileName: string,
    outputFolderName: string
  ) {
    const workbook = {
      Sheets: {},
      SheetNames: [],
    } as XLSX.WorkBook;

    const allCustomFields = await this._customFieldRepo.fetch(customerId);
    const bmpDetailCustomFields = allCustomFields.filter(
      f => f.entityType === 'BmpDetail'
    );
    const csCustomFields = allCustomFields.filter(
      f => f.entityType === 'ConstructionSite'
    );
    const facCustomFields = allCustomFields.filter(
      f => f.entityType === 'Facility'
    );
    const outfallCustomFields = allCustomFields.filter(
      f => f.entityType === 'Outfall'
    );
    const structureCustomFields = allCustomFields.filter(
      f => f.entityType === 'Structure'
    );

    // ConstructionSite
    const csSheetName = 'Construction Sites';
    const constructionSites = await this._csRepo.exportData(customerId);

    // ConstructionSite CF
    const csCfValues = await this._csRepo.exportCustomFields(customerId);
    const csCfGrouped = _.groupBy(csCfValues, 'constructionSiteId');

    createWsWithCfColumns(
      csSheetName,
      constructionSites,
      csCustomFields,
      csCfGrouped
    );

    // Facility
    const facSheetName = 'Facilities';
    const facilities = await this._facilityRepo.exportData(customerId);

    // Facility CF
    const facCfValues = await this._facilityRepo.exportCustomFields(customerId);
    const facCfGrouped = _.groupBy(facCfValues, 'facilityId');

    createWsWithCfColumns(
      facSheetName,
      facilities,
      facCustomFields,
      facCfGrouped
    );

    // Outfall
    const outfallSheetName = 'Outfalls';
    const outfalls = await this._outfallRepo.exportData(customerId);

    // Outfall CF
    const outfallCfValues = await this._outfallRepo.exportCustomFields(
      customerId
    );
    const outfallCfGrouped = _.groupBy(outfallCfValues, 'outfallId');
    createWsWithCfColumns(
      outfallSheetName,
      outfalls,
      outfallCustomFields,
      outfallCfGrouped
    );

    // Structure
    const structureSheetName = 'Structures';
    const structures = await this._structureRepo.exportData(customerId);

    // Structure CF
    const structureCfValues = await this._structureRepo.exportCustomFields(
      customerId
    );
    const structureCfGrouped = _.groupBy(structureCfValues, 'structureId');

    createWsWithCfColumns(
      structureSheetName,
      structures,
      structureCustomFields,
      structureCfGrouped
    );

    // BmpControlMeasure
    const bmpCMSheetName = 'Bmp MCMs';
    const controlMeasures = await this._bmpCMRepo.exportData(customerId);
    const cmsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(controlMeasures)
    );
    addSheetToWorkbook(bmpCMSheetName, cmsSheet, workbook);

    // BmpDetails
    const bmpDetailSheetName = 'Bmp Details';
    const bmpDetails = await this._bmpDetailRepo.exportData(customerId);
    // BmpDetail Custom Fields
    const bmpDetailCustomFieldValues = await this._bmpDetailRepo.exportCustomFields(
      customerId
    );
    const bmpDetailCustomFieldGrouped = _.groupBy(
      bmpDetailCustomFieldValues,
      'bmpDetailId'
    );

    createWsWithCfColumns(
      bmpDetailSheetName,
      bmpDetails,
      bmpDetailCustomFields,
      bmpDetailCustomFieldGrouped
    );

    // BmpActivityLogs
    const bmpActivityLogSheetName = 'Bmp Activity Logs';
    const bmpActLogs = await this._bmpActLogRepo.exportData(customerId);
    const bmpActLogSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(bmpActLogs)
    );
    addSheetToWorkbook(bmpActivityLogSheetName, bmpActLogSheet, workbook);

    // Contacts
    const contacts = await this._contactRepo.fetch(customerId, {});
    const contactsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(contacts))
    );
    addSheetToWorkbook('Contacts', contactsSheet, workbook);

    // Asset Contacts
    const entityContacts = await this._contactRepo.exportEntityContacts(
      customerId
    );
    const entityContactsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(entityContacts))
    );
    addSheetToWorkbook('Asset Contacts', entityContactsSheet, workbook);

    // Community
    const communities = await this._communityRepo.fetch(customerId);
    const communitiesSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(communities))
    );
    addSheetToWorkbook('Community', communitiesSheet, workbook);

    // ReceivingWater
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const receivingWatersSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(receivingWaters))
    );
    addSheetToWorkbook('Receiving Waters', receivingWatersSheet, workbook);

    // Watershed
    const watersheds = await this._wsRepo.fetch(customerId);
    const watershedsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(watersheds))
    );
    addSheetToWorkbook('Watersheds', watershedsSheet, workbook);

    // Project
    const projects = await this._projectRepo.fetch(customerId);
    const projectsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(projects))
    );
    addSheetToWorkbook('Projects', projectsSheet, workbook);

    // CS Inspection Types
    const inspTypes = await this._inspTypeRepo.fetch(customerId);
    const inspTypesSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(inspTypes))
    );
    addSheetToWorkbook('CS Inspection Types', inspTypesSheet, workbook);

    await this._addSwmpToWorkbook(customerId, workbook);

    XLSX.writeFile(workbook, path.join(outputFolderName, outputFileName));

    function createWsWithCfColumns(
      entitySheetName: string,
      entities: any[],
      entityCfs: CustomField[],
      entityCfGrouped: _.Dictionary<any[]>
    ) {
      const entityWithCfs = entities.map(entity => {
        entityCfs.forEach(f => (entity[columnForLabel(f.fieldLabel)] = null));

        const customFieldValues = entityCfGrouped[entity.id];

        if (customFieldValues) {
          customFieldValues.forEach(cfv => {
            entity[columnForLabel(cfv.fieldLabel)] = cfv.value;
          });
        }
        return entity;
      });

      const entitySheet = XLSX.utils.aoa_to_sheet(
        shared.entitiesToAoA(entityWithCfs)
      );
      addSheetToWorkbook(entitySheetName, entitySheet, workbook);
    }
  }

  private async _writeInspectionsZip(
    customerId: string,
    fileName: string,
    filePath: string
  ) {
    let workbooks = [] as WorkBookWithName[];
    const fieldToGroupBy = 'customFormTemplateId';
    // ConstructionSiteInspections
    const allCsInsp = await this._csInspRepo.exportData(customerId);
    const allCsInspGrouped = _.groupBy(allCsInsp, fieldToGroupBy);
    const allCsInspCfs = sanitizeSheetName(
      await this._csInspRepo.exportCustomFields(customerId)
    );
    const allCsInspCfsGrouped = _.groupBy(allCsInspCfs, fieldToGroupBy);
    createWrkBksWithCfColumns(
      InspectionSheetName.ContructionSiteInspection,
      allCsInspGrouped,
      allCsInspCfsGrouped,
      'constructionSiteInspectionId',
      workbooks
    );

    // FacilityInspections
    const allFacInsp = await this._facilityInspRepo.exportData(customerId);
    const allFacInspGrouped = _.groupBy(allFacInsp, fieldToGroupBy);
    const allFacInspCfs = sanitizeSheetName(
      await this._facilityInspRepo.exportCustomFields(customerId)
    );
    const allFacInspCfsGrouped = _.groupBy(allFacInspCfs, fieldToGroupBy);
    createWrkBksWithCfColumns(
      InspectionSheetName.FacilityInspection,
      allFacInspGrouped,
      allFacInspCfsGrouped,
      'facilityInspectionId',
      workbooks
    );

    // StructureInspections
    const allStrucInsp = await this._structureInspRepo.exportData(customerId);
    const allStrucInspGrouped = _.groupBy(allStrucInsp, fieldToGroupBy);
    const allStrucInspCfs = sanitizeSheetName(
      await this._structureInspRepo.exportCustomFields(customerId)
    );
    const allStrucInspCfsGrouped = _.groupBy(allStrucInspCfs, fieldToGroupBy);
    createWrkBksWithCfColumns(
      InspectionSheetName.StructuralControlInspection,
      allStrucInspGrouped,
      allStrucInspCfsGrouped,
      'structureInspectionId',
      workbooks
    );

    // OutfallInspections
    const allOutInsp = await this._outfallInspRepo.exportData(customerId);
    const allOutInspGrouped = _.groupBy(allOutInsp, fieldToGroupBy);
    const allOutInspCfs = sanitizeSheetName(
      await this._outfallInspRepo.exportCustomFields(customerId)
    );
    const allOutInspCfsGrouped = _.groupBy(allOutInspCfs, fieldToGroupBy);
    createWrkBksWithCfColumns(
      InspectionSheetName.OutfallInspection,
      allOutInspGrouped,
      allOutInspCfsGrouped,
      'outfallInspectionId',
      workbooks
    );

    // CitizenReports
    const allCitizenReports = await this._crRepo.exportData(customerId);
    const allCitizenReportsGrouped = _.groupBy(
      allCitizenReports,
      fieldToGroupBy
    );
    const allCitizenReportsCfs = sanitizeSheetName(
      await this._crRepo.exportCustomFields(customerId)
    );
    const allCitizenReportsCfsGrouped = _.groupBy(
      allCitizenReportsCfs,
      fieldToGroupBy
    );
    createWrkBksWithCfColumns(
      InspectionSheetName.CitizenReports,
      allCitizenReportsGrouped,
      allCitizenReportsCfsGrouped,
      'citizenReportId',
      workbooks
    );

    // IllicitDischarges
    const allId = await this._idRepo.exportData(customerId);
    const allIdGrouped = _.groupBy(allId, fieldToGroupBy);
    const allIdCfs = sanitizeSheetName(
      await this._idRepo.exportCustomFields(customerId)
    );
    const allIdCfsGrouped = _.groupBy(allIdCfs, fieldToGroupBy);
    createWrkBksWithCfColumns(
      InspectionSheetName.IllicitDischarges,
      allIdGrouped,
      allIdCfsGrouped,
      'illicitDischargeId',
      workbooks
    );

    const zip = new JSZip();
    const zipFolder = zip.folder(fileName);

    for (let i = 0; i < workbooks.length; i++) {
      const tempWrite = XLSX.write(workbooks[i].workbook, {
        type: 'base64',
        compression: true,
      });
      const tempOutFileName = `${workbooks[i].workbookName}.xlsx`;

      zipFolder.file(tempOutFileName, tempWrite, { base64: true });
    }

    const zipBuffer = await zip.generateAsync({
      type: 'nodebuffer',
      streamFiles: true,
    });
    const outputPath = path.join(filePath, fileName);
    await fs.writeFile(outputPath, zipBuffer);

    function sanitizeSheetName(originInspCfs: any[]) {
      const regex = /\\|\/|:|\?|\*|\[|]/g;
      const inspCfs = originInspCfs.map(cf => ({
        ...cf,
        name: cf.name
          .replace(regex, '')
          .replace(/\s+/g, ' ')
          .slice(0, 25),
      }));

      const inspCfsByName = _.groupBy(inspCfs, 'name');
      const inspCfsNameCount = Object.keys(inspCfsByName).reduce(
        (acc, field) => ((acc[field] = 0), acc),
        {}
      );

      return inspCfs.map(cf => {
        if (inspCfsByName[cf.name].length === 1) {
          return cf;
        }

        return {
          ...cf,
          name: `${cf.name}(${++inspCfsNameCount[cf.name]})`,
        };
      });
    }
  }

  private async _writeSwmpZip(
    customerId: string,
    outputFileName: string,
    outputFolderName: string
  ) {
    const workbook = {
      Sheets: {},
      SheetNames: [],
    } as XLSX.WorkBook;

    await this._addSwmpToWorkbook(customerId, workbook);

    XLSX.writeFile(workbook, path.join(outputFolderName, outputFileName));
  }

  private async _addSwmpToWorkbook(
    customerId: string,
    workbook: XLSX.WorkBook
  ) {
    const customer = await this._customerRepository.fetchById(customerId);

    if (!customer.features.swmp) {
      return addSheetToWorkbook(
        'SWMP',
        XLSX.utils.aoa_to_sheet([['Customer does not support SWMP feature']]),
        workbook
      );
    }

    // SWMP
    const swmps = await this._swmpRepo.fetchByCustomer(customerId);
    const swmpsSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(swmps))
    );
    addSheetToWorkbook('SWMP', swmpsSheet, workbook);

    // BMP
    const bmps = [];

    for (let i = 0; i < swmps.length; i++) {
      const bmpsBySwmp = await this._bmpRepo.fetchBySwmpId(swmps[i].id);
      bmps.push(...bmpsBySwmp);
    }

    const bmpsSheet = XLSX.utils.aoa_to_sheet(shared.entitiesToAoA(bmps));
    addSheetToWorkbook('BMP', bmpsSheet, workbook);

    // MG
    const mgs = [];

    for (let i = 0; i < bmps.length; i++) {
      const mgsByBmp = await this._mgRepo.fetchByBmpId(bmps[i].id);
      mgs.push(...mgsByBmp);
    }

    const mgsSheet = XLSX.utils.aoa_to_sheet(shared.entitiesToAoA(mgs));
    addSheetToWorkbook('MG', mgsSheet, workbook);

    // SWMP Activity
    const swmpActivities = [];

    for (let i = 0; i < swmps.length; i++) {
      const activitiesBySwmp = await this._swmpActivityRepo.fetchBySwmpIdAndCustomer(
        swmps[i].id,
        customerId,
        true
      );
      swmpActivities.push(...activitiesBySwmp);
    }

    const swmpActivitiesSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(removeCustomerId(swmpActivities))
    );
    addSheetToWorkbook('SWMP Activity', swmpActivitiesSheet, workbook);

    if (customer.features.swmpAnnualReport) {
      // SWMP Annual Report
      const swmpAnnualReports = [];

      for (let i = 0; i < swmps.length; i++) {
        const swmpAnnualReport = await this._swmpAnnualReportRepo.fetchBySwmpAndCustomer(
          swmps[i].id,
          swmps[i].year,
          customerId
        );
        swmpAnnualReport && swmpAnnualReports.push(swmpAnnualReport);
      }

      const swmpAnnualReportsSheet = XLSX.utils.aoa_to_sheet(
        shared.entitiesToAoA(removeCustomerId(swmpAnnualReports))
      );
      addSheetToWorkbook(
        'SWMP Annual Report',
        swmpAnnualReportsSheet,
        workbook
      );
    }
  }
}

function addSheetToWorkbook(
  sheetName: string,
  sheet: XLSX.WorkSheet,
  workbook: XLSX.WorkBook
) {
  workbook.SheetNames.push(sheetName);
  workbook.Sheets[sheetName] = sheet;
  return workbook;
}

function removeCustomerId(list) {
  return list.map(i => {
    delete i.customerId;
    delete i.originalId;
    return i;
  });
}

function _columnForLabel(label: string) {
  return `CF: ${label}`;
}

async function getTempCustomerPath(customerId: string) {
  const root = path.dirname(process.argv[1]);
  const tempPath = path.join(root, 'public/temp_export', customerId);
  await fs.ensureDir(tempPath);
  return tempPath;
}

/**
 * returns the public url of the file
 * @param absoluteRootUrl
 * @param customerId
 */
function getPublicExportLink(
  file: string,
  absoluteRootUrl: string,
  customerId: string
) {
  return [absoluteRootUrl, 'public/temp_export', customerId, file].join('/');
}

/**
 * @param {_.Dictionary<any[]>} entityInspectionsGrouped - Dictionary of Inspections grouped by cftIds.
 * @param {_.Dictionary<any[]>} entityInspectionsCfsGrouped - Dictionary of CustomFieldValues grouped by cftIds.
 * @param {string} inspectionGroupKeyName - The entityInspectionId to group CustomFieldValues
 */
function createWrkBksWithCfColumns(
  inspectionSheetName: InspectionSheetName,
  entityInspectionsGrouped: _.Dictionary<any[]>,
  entityInspectionsCfsGrouped: _.Dictionary<any[]>,
  inspectionGroupKeyName: string,
  workbooks: WorkBookWithName[]
) {
  if (!Object.keys(entityInspectionsGrouped).length) {
    return;
  }

  const workbook = {
    Sheets: {},
    SheetNames: [],
  } as XLSX.WorkBook;

  Object.keys(entityInspectionsGrouped).forEach(cftId => {
    const inspectionCfGroup = entityInspectionsCfsGrouped[cftId];
    if (!inspectionCfGroup) {
      return;
    }
    // initialize custom field object with blank values
    const emptyCFObject = inspectionCfGroup.reduce((obj, cf) => {
      obj[columnForLabel(cf.fieldLabel)] = null;
      return obj;
    }, {});
    const inspectionCfGrouped = _.groupBy(
      inspectionCfGroup,
      inspectionGroupKeyName
    );
    const inspectionsWithCfs = entityInspectionsGrouped[cftId].map(
      inspection => {
        const customFieldValues = inspectionCfGrouped[inspection.id];
        Object.assign(inspection, emptyCFObject);
        if (customFieldValues) {
          customFieldValues.forEach(
            cfv => (inspection[columnForLabel(cfv.fieldLabel)] = cfv.value)
          );
        }
        switch (inspectionSheetName) {
          case InspectionSheetName.ContructionSiteInspection:
            inspection.id = `C-${inspection.id}`;
            break;
          case InspectionSheetName.FacilityInspection:
            inspection.id = `F-${inspection.id}`;
            break;
          case InspectionSheetName.StructuralControlInspection:
            inspection.id = `S-${inspection.id}`;
            break;
          case InspectionSheetName.OutfallInspection:
            inspection.id = `O-${inspection.id}`;
            break;
          case InspectionSheetName.CitizenReports:
            inspection.id = `CZ-${inspection.id}`;
            break;
          case InspectionSheetName.IllicitDischarges:
            inspection.id = `I-${inspection.id}`;
        }
        return inspection;
      }
    );

    if (
      inspectionSheetName !== InspectionSheetName.CitizenReports &&
      inspectionSheetName !== InspectionSheetName.IllicitDischarges
    ) {
      const inspectionTime = inspectionsWithCfs.reduce(
        (acc, inspection) =>
          inspection.timeIn && inspection.timeOut
            ? {
                seconds:
                  acc.seconds +
                  moment(inspection.timeOut).diff(inspection.timeIn, 'seconds'),
                count: acc.count + 1,
              }
            : acc,
        { seconds: 0, count: 0 }
      );

      const averageInspectionMinutes = inspectionTime.count
        ? Math.round(inspectionTime.seconds / inspectionTime.count / 60)
        : 0;

      inspectionsWithCfs.push([]);
      inspectionsWithCfs.push([
        'Average Inspection Time',
        `${averageInspectionMinutes} minute${
          averageInspectionMinutes === 1 ? '' : 's'
        } (${inspectionTime.count} inspection${
          inspectionTime.count === 1 ? '' : 's'
        })`,
      ]);
    }

    const inspectionSheet = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA(inspectionsWithCfs)
    );
    // sheet names cannot be longer than 31 characters
    const sheetName = inspectionCfGroup[0].name.slice(0, 30);
    addSheetToWorkbook(sheetName, inspectionSheet, workbook);
  });
  workbooks.push({ workbook, workbookName: inspectionSheetName });
}

interface ExportFileInfo {
  isProcessing: boolean;
  name: string;
  size: number; // in bytes
  createdDate: Date;
  link: string;
}
