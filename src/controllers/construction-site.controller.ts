import {
  APP_PATH,
  EntityTypes,
  createClassFromObject,
  fileToFileLink,
} from '../shared';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';
import * as express from 'express';
import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
  httpPut,
} from 'inversify-express-utils';
import { authenticationRequired } from 'express-stormpath';

import {
  ConstructionSite,
  Principal,
  InspectionQueryOptions,
  ConstructionSiteInspection,
  FileEntity,
  FileWithLinks,
  ConstructionSiteDetailsResult,
  ConstructionSiteQueryOptions,
} from '../models';
import { BaseController } from './base.controller';
import {
  ConstructionSiteRepository,
  ConstructionSiteInspectionRepository,
  ProjectRepository,
  CommunityRepository,
  ContactRepository,
  ReceivingWaterRepository,
  WatershedRepository,
  FileRepository,
  CustomFieldRepository,
  CitizenReportRepository,
  IllicitDischargeRepository,
} from '../data';

const BASE_ROUTE = `${APP_PATH}/api/constructionsites`;

@controller(BASE_ROUTE)
export class ConstructionSiteController extends BaseController {
  constructor(
    private _csRepo: ConstructionSiteRepository,
    private _csInspectionRepo: ConstructionSiteInspectionRepository,
    private _projectRepo: ProjectRepository,
    private _communityRepo: CommunityRepository,
    private _contactRepo: ContactRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _crRepo: CitizenReportRepository,
    private _illicitRepo: IllicitDischargeRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req, res, next) {
    const currentCustomer = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    let sites: ConstructionSite[] = [];
    const queryOptions: ConstructionSiteQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    if (req.query.active === 'true') {
      queryOptions.active = true;
      queryOptions.orderByField = 'name';
      queryOptions.orderByDirection = 'asc';
    }
    sites = await this._csRepo.fetch(currentCustomer, queryOptions);
    return this.ok(sites);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: ConstructionSiteQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const site = await this._csRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (site) {
      return this.ok(site);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet('/:id([0-9]+)/details', authenticationRequired)
  async details(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const inspectionQOpts: InspectionQueryOptions = {
      orderByRaw:
        `${ConstructionSiteInspection.sqlField(f => f.inspectionDate)} desc, ` +
        `${ConstructionSiteInspection.sqlField(
          f => f.scheduledInspectionDate
        )} desc`,
    };
    const principal = this.httpContext.user as Principal;
    const queryOptions: ConstructionSiteQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const constructionSite = await this._csRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (!constructionSite) {
      return this.notFound();
    }
    const inspections = await this._csInspectionRepo.fetch(
      customerId,
      assetId,
      inspectionQOpts
    );
    const citizenReports = await this._crRepo.fetch(customerId, {
      constructionSiteId: assetId,
    });
    const illicitDischarges = await this._illicitRepo.fetch(customerId, {
      constructionSiteId: assetId,
    });
    const projects = await this._projectRepo.fetch(customerId);
    const contacts = await this._contactRepo.fetchContactsForEntity(
      ConstructionSite,
      assetId,
      customerId
    );
    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
      ConstructionSite,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(ConstructionSite, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );
    const result: ConstructionSiteDetailsResult = {
      constructionSite,
      inspections,
      projects,
      contacts,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
      citizenReports,
      illicitDischarges,
    };
    return this.ok(result);
  }

  /**
   * Creates a new Construction Site.
   *
   * `POST /api/constructionsites`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    customerAssetLimit(EntityTypes.ConstructionSite)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(ConstructionSite, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._csRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: ConstructionSiteQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._csRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._csRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._csRepo.fetchById(assetId, customerId);
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: ConstructionSiteQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail;
      principal.details.email;
    }
    const site = await this._csRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (site) {
      await this._csRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }
}
