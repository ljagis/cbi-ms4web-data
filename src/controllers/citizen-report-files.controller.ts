import { APP_PATH } from '../shared';
import { enforceFeaturePermission } from '../middleware';
import { authenticationRequired } from 'express-stormpath';

import {
  controller,
  httpGet,
  httpPost,
  httpDelete,
} from 'inversify-express-utils';
import { EntityTypes } from '../shared';

import { EntityFileController } from './entity-file.controller';
import { defaultMulterInstance } from './helpers';

const CONTROLLER_ROUTE = 'citizenreports';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class CitizenReportFiles extends EntityFileController {
  constructor() {
    super(EntityTypes.CitizenReport);
  }

  @httpPost(
    '/:id([0-9]+)/files',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'edit'),
    defaultMulterInstance.array('attachments', 10)
  )
  async create(req, res, next) {
    await super.create(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files/:fileId([0-9]+)', authenticationRequired)
  async indexAt(req, res, next) {
    await super.indexAt(req, res, next);
  }

  @httpGet('/:id([0-9]+)/files', authenticationRequired)
  async index(req, res, next) {
    await super.index(req, res, next);
  }

  @httpDelete(
    '/:id([0-9]+)/files/:fileId([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('investigations', 'delete')
  )
  async del(req, res, next) {
    await super.del(req, res, next);
  }
}
