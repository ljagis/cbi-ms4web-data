import * as express from 'express';
import { BaseController } from './base.controller';
import { ContactRepository } from '../data';
import { okay, ClientError } from '../shared';
import { inject, unmanaged } from 'inversify';

export abstract class EntityContactController extends BaseController {
  @inject(ContactRepository) protected _contactRepo: ContactRepository;

  constructor(@unmanaged() protected EntityWithContacts: { new (...args) }) {
    super();
  }

  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let entityId = Number(req.params.entityId);

    const contacts = await this._contactRepo.fetchContactsForEntity(
      this.EntityWithContacts,
      entityId,
      customerId
    );
    okay(res, contacts);
  }

  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let entityId = Number(req.params.entityId);
    let contactId = Number(req.params.id);

    const contacts = await this._contactRepo.fetchContactForEntity(
      this.EntityWithContacts,
      contactId,
      entityId,
      customerId
    );
    okay(res, contacts);
  }

  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);

    const entityId = Number(req.params.entityId);
    const contactId = req.body.contactId;
    const isPrimary = req.body.isPrimary;

    if (!contactId) {
      throw new ClientError('Contact id is required');
    }

    const id = await this._contactRepo.insertContactForEntity(
      this.EntityWithContacts,
      entityId,
      contactId,
      customerId,
      undefined,
      isPrimary
    );
    okay(res, {
      id: id,
      contactId: contactId,
    });
  }

  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const entityId = Number(req.params.entityId);
    const contactId = Number(req.params.contactId);
    const isPrimary = req.body.isPrimary;

    if (!contactId) {
      throw new ClientError('Contact id is required');
    }

    const result = await this._contactRepo.updateEntityContactPrimary(
      this.EntityWithContacts,
      entityId,
      contactId,
      customerId,
      isPrimary
    );

    okay(res, result);
  }

  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let contactId = Number(req.params.contactId);
    let entityId = Number(req.params.entityId);
    await this._contactRepo.deleteAssetContact(
      this.EntityWithContacts,
      entityId,
      contactId,
      customerId
    );
    okay(res, undefined);
  }
}
