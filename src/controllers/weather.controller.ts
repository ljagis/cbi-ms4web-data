import { controller, httpGet } from 'inversify-express-utils';

import { APP_PATH } from '../shared';
import { BaseController } from './base.controller';
import { WeatherKitService } from '../services/weather-kit.service';
import { Request } from 'express';
import { authenticationRequired } from 'express-stormpath';

const CONTROLLER_BASE_ROUTE = 'weather';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class WeatherController extends BaseController {
  constructor(private _wkSvc: WeatherKitService) {
    super();
  }

  /**
   * GET api/weather/current
   */
  @httpGet('/current', authenticationRequired)
  async current(req: Request) {
    const { lat, lng } = req.query;
    try {
      const resp = await this._wkSvc.forecastSimple(+lat, +lng);
      return this.ok(resp);
    } catch (error) {
      this.badRequest(error);
    }
  }
}
