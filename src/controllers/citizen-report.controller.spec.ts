import { CitizenReport, CustomFormTemplate, InspectionType } from '../models';
import { config } from '../test/helper';
import { container } from '../inversify.config';
import * as Knex from 'knex';
import * as chai from 'chai';
import {
  CustomFormTemplateRepository,
  InspectionTypeRepository,
} from '../data/index';
import * as moment from 'moment';

describe(`Citizen Reports CRUD`, async () => {
  const controllerRoute = 'citizenreports';
  const cr: Partial<CitizenReport> = {
    citizenName: 'Alex Nguyen',
    dateReported: new Date(2016, 11, 1),
    complianceStatus: 'Active',
    lat: 33.056299,
    lng: -96.99825,
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    phoneNumber: '111-111-1111',
    cellNumber: '222-222-2222',
    faxNumber: '333-333-3333',
    report: 'Some sample report',
    responseDate: new Date(2016, 12, 1),
    response: 'Sample response',
    investigatorId: null,
    investigator2Id: null,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'additional info',
  };

  const cr2: Partial<CitizenReport> = {
    citizenName: 'Alex Nguyen 2',
    dateReported: new Date(2016, 11, 2),
    complianceStatus: 'Resolved',
    lat: 34.056299,
    lng: -94.99825,
    physicalAddress1: '123 Test 2',
    physicalAddress2: 'SUITE 12',
    physicalCity: 'Houston2',
    physicalState: 'CA',
    physicalZip: '77043',
    phoneNumber: '111-111-1111x',
    cellNumber: '222-222-2222x',
    faxNumber: '333-333-3333x',
    report: 'Some sample report 2',
    responseDate: new Date(2016, 12, 2),
    response: 'Sample response 2',
    investigatorId: null,
    investigator2Id: null,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'additional info 2',
  };
  let createdId: number;
  let formTemplates: CustomFormTemplate[];
  let inspectionTypes: InspectionType[];

  const cftRepo = container.get(CustomFormTemplateRepository);
  const itRepo = container.get(InspectionTypeRepository);
  const knex = container.get(Knex) as Knex;

  before(async () => {
    formTemplates = await cftRepo.fetch(config.customerId);
    inspectionTypes = await itRepo.fetch(config.customerId);
    cr.customFormTemplateId = formTemplates[0].id;
    cr.inspectionTypeId = inspectionTypes[0].id;
  });

  after(async () => {
    if (createdId) {
      await knex(CitizenReport.tableName)
        .del()
        .where(CitizenReport.sqlField(f => f.id), createdId);
    }
  });

  it(`should GET all`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
    chai.assert(Array.isArray(res.body));
  });

  it(`should POST Citizen Report`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(cr);
    createdId = +res.body.id;
    chai.expect(res).to.have.status(201);
    chai.expect(res).to.be.json;
    chai.expect(createdId).to.be.greaterThan(0);
  });

  it(`should GET Citizen Report to verify created data`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    Object.keys(cr).forEach(k => {
      if (cr[k] instanceof Date) {
        chai.assert(moment(cr[k]).isSame(moment(res.body[k])));
      } else {
        chai.expect(res.body[k]).equals(cr[k]);
      }
    });
  });

  it(`should PUT Citizen Report`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(cr2);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;
  });

  it(`should GET Citizen Report to verify modified data`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    Object.keys(cr2).forEach(k => {
      if (cr2[k] instanceof Date) {
        chai.assert(moment(cr2[k]).isSame(moment(res.body[k])));
      } else {
        chai.expect(res.body[k]).equals(cr2[k]);
      }
    });
  });

  it(`should check for citizenreports/details/:id route`, async () => {
    const res = await chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/details/${createdId}`)
      .set('Cookie', config.cookie);
    chai.expect(res).to.have.status(200);
    chai.expect(res).to.be.json;

    chai.expect(res.body.citizenReport).to.not.be.null;

    chai.expect(res.body.customFields).to.be.instanceOf(Array);
    chai.expect(res.body.files).to.be.instanceOf(Array);
    chai.expect(res.body.communities).to.have.length.greaterThan(0);
    chai.expect(res.body.receivingWaters).to.have.length.greaterThan(0);
    chai.expect(res.body.watersheds).to.have.length.greaterThan(0);
  });

  it(`should DELETE Citizen Report`, async () => {
    if (createdId > 0) {
      await chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie);
    }
  });
});
