import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  EntityTypes,
  createClassFromObject,
  NotFoundError,
  fileToFileLink,
} from '../shared';
import * as express from 'express';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';

import {
  Structure,
  Principal,
  InspectionQueryOptions,
  StructureInspection,
  FileEntity,
  FileWithLinks,
  StructureQueryOptions,
} from '../models';

import { StructureDetailsResult } from '../models/asset-details-result';
import { BaseController } from './base.controller';
import {
  StructureRepository,
  StructureInspectionRepository,
  CommunityRepository,
  ReceivingWaterRepository,
  WatershedRepository,
  FileRepository,
  CustomFieldRepository,
  ContactRepository,
  ProjectRepository,
  StructureControlTypeRepository,
} from '../data';

const BASE_ROUTE = `${APP_PATH}/api/structures`;

@controller(BASE_ROUTE)
export class StructureController extends BaseController {
  constructor(
    private _structureRepo: StructureRepository,
    private _structureInspectionRepo: StructureInspectionRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _contactRepo: ContactRepository,
    private _projectRepo: ProjectRepository,
    private _structureControlTypeRepo: StructureControlTypeRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    const queryOptions: StructureQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const structures = await this._structureRepo.fetch(
      currentCustomer,
      queryOptions
    );
    return this.ok(structures);
  }

  /**
   * Get Structure by `id`
   *
   * `GET /api/structures/:id`
   *
   * @param {express.Request} req
   * ```
   * request.params:
   *   {number} id
   * ```
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  @httpGet('/:id([0-9]+)', authenticationRequired)
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: StructureQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const structure = await this._structureRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (structure) {
      return this.ok(structure);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet('/:id([0-9]+)/details', authenticationRequired)
  async details(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const inspectionQOpts: InspectionQueryOptions = {
      orderByRaw:
        `${StructureInspection.sqlField(f => f.inspectionDate)} desc, ` +
        `${StructureInspection.sqlField(f => f.scheduledInspectionDate)} desc`,
    };
    const queryOptions: StructureQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const structure = await this._structureRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (!structure) {
      throw new NotFoundError();
    }
    const inspections = await this._structureInspectionRepo.fetch(
      customerId,
      assetId,
      inspectionQOpts
    );
    const projects = await this._projectRepo.fetch(customerId);
    const contacts = await this._contactRepo.fetchContactsForEntity(
      Structure,
      assetId,
      customerId
    );
    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
      Structure,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(Structure, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );
    const controlTypes = await this._structureControlTypeRepo.fetch(customerId);
    const result: StructureDetailsResult = {
      structure,
      inspections,
      projects,
      contacts,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
      controlTypes,
    };
    return this.ok(result);
  }

  /**
   * Creates a new Structure.
   *
   * `POST /api/structures`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    customerAssetLimit(EntityTypes.Structure)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(Structure, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._structureRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: StructureQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._structureRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._structureRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._structureRepo.fetchById(
        assetId,
        customerId
      );
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: StructureQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const structure = await this._structureRepo.fetchById(assetId, customerId);
    if (structure) {
      await this._structureRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }
}
