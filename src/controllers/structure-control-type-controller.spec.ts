import 'mocha';
import * as chai from 'chai';
import ChaiHttp = require('chai-http');
import { config } from '../test/helper';
import * as models from '../models';
import * as Promise from 'bluebird';

chai.use(ChaiHttp);

let controllerRoute = 'structurecontroltypes';
let postControlType: models.StructureControlType = {
  name: 'Detention Pond',
};
let putControlType: models.StructureControlType = {
  name: 'Detention Pond 2',
};
let createdId: number;

describe(`GET all Structure Control Types`, () => {
  it(`should GET Structure Control Types`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
        chai.expect(res.body).to.be.instanceof(Array);
        chai.expect(res.body).to.have.length.above(0);
      });
  });
});

describe(`CRUD StructureControlType`, () => {
  it(`should POST StructureControlType`, () => {
    return chai
      .request(config.baseUrl)
      .post(`/api/${controllerRoute}`)
      .set('Cookie', config.cookie)
      .send(postControlType)
      .then(res => {
        createdId = +res.body.id;
        chai.expect(res).to.have.status(201);
        chai.expect(res).to.be.json;
        chai.expect(createdId).to.be.greaterThan(0);
      });
  });

  it(`should GET Bmp to verify created data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(postControlType).forEach(k => {
          chai.expect(res.body[k]).equals(postControlType[k]);
        });
      });
  });

  it(`should PUT StructureControlType`, () => {
    return chai
      .request(config.baseUrl)
      .put(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(putControlType)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;
      });
  });

  it(`should GET StructureControlType to verify modified data`, () => {
    return chai
      .request(config.baseUrl)
      .get(`/api/${controllerRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .then(res => {
        chai.expect(res).to.have.status(200);
        chai.expect(res).to.be.json;

        Object.keys(putControlType).forEach(k => {
          chai.expect(res.body[k]).equals(putControlType[k]);
        });
      });
  });

  it(`should DELETE Bmp`, () => {
    if (createdId > 0) {
      return chai
        .request(config.baseUrl)
        .del(`/api/${controllerRoute}/${createdId}`)
        .set('Cookie', config.cookie)
        .then(res => {
          chai.expect(res).to.have.status(200);
        });
    }
    return Promise.resolve();
  });
});
