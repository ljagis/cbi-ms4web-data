import * as express from 'express';
import * as moment from 'moment';
import { groupBy, omit, sum, uniq } from 'lodash';
import * as XLSX from 'xlsx';
import * as fs from 'fs-extra';
import * as path from 'path';
import { BaseController } from './base.controller';
import { controller, httpGet, httpPost } from 'inversify-express-utils';
import * as shared from '../shared';
import * as data from '../data';
import { AssetReport } from '../models/reports';
import { authenticationRequired } from 'express-stormpath';
import {
  ComplianceStatus,
  User,
  Inspection,
  ConstructionSiteQueryOptions,
  Principal,
  OutfallQueryOptions,
  FacilityQueryOptions,
  StructureQueryOptions,
} from '../models';
import { EntityTypes, okay, filterComplianceStatus } from '../shared';

const CONTROLLER_BASE_ROUTE = 'assetsummaryreport';

interface StaffReport {
  date: string;
  data: {
    constructionSite: { [key: string]: number };
    outfall: { [key: string]: number };
    structure: { [key: string]: number };
    facility: { [key: string]: number };
    illicitDischarge: { [key: string]: number };
    citizenReport: { [key: string]: number };
    total: number;
  };
}

interface AssetSummaryResponse {
  complianceStatus: ComplianceStatus[];
  users: User[];
  assets: AssetReport[];
  staffReports: {
    completed: StaffReport[];
    scheduled: StaffReport[];
  };
  inspections: Inspection[];
}

const STAFF = 'staff';

@controller(`${shared.APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class AssetSummaryReportController extends BaseController {
  constructor(
    private _constructionSiteRepo: data.ConstructionSiteRepository,
    private _facilityRepo: data.FacilityRepository,
    private _outfallRepo: data.OutfallRepository,
    private _structureRepo: data.StructureRepository,
    private _constructionSiteInspectionRepo: data.ConstructionSiteInspectionRepository,
    private _facilityInspectionRepo: data.FacilityInspectionRepository,
    private _outfallInspectionRepo: data.OutfallInspectionRepository,
    private _structureInspectionRepo: data.StructureInspectionRepository,
    private _userRepo: data.UserRepository,
    private _complianceStatusRepo: data.ComplianceStatusRepository,
    private _illicitDischargeRepo: data.IllicitDischargeRepository,
    private _citizenReportRepo: data.CitizenReportRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: express.Request, res: express.Response) {
    const assetType: string = req.query.assettype;
    const fromDate = req.query.fromDate;
    const toDate = req.query.toDate;

    const customerId = await this.getCustomerId(req);

    if (!assetType) {
      throw new shared.ClientError('assettype is required');
    }
    const csQueryOptions: ConstructionSiteQueryOptions = {};
    const outfallQueryOptions: OutfallQueryOptions = {};
    const facilityQueryOptions: FacilityQueryOptions = {};
    const structureQueryOptions: StructureQueryOptions = {};
    const principal = this.httpContext.user as Principal;
    if (await principal.isContractor()) {
      csQueryOptions.contractorEmail = principal.details.email;
      outfallQueryOptions.contractorEmail = principal.details.email;
      facilityQueryOptions.contractorEmail = principal.details.email;
      structureQueryOptions.contractorEmail = principal.details.email;
    }

    const complianceStatus = await this._complianceStatusRepo.fetch();

    const assetResponse: AssetSummaryResponse = {
      complianceStatus: complianceStatus.filter(
        filterComplianceStatus(customerId)
      ),
      users: await this._userRepo.fetch(customerId),
      assets: undefined,
      staffReports: undefined,
      inspections: undefined,
    };

    /**
     * 1. get inspections
     * 2. get assets
     * 3. merge inspections to asset
     */

    switch (assetType.toLowerCase()) {
      case EntityTypes.ConstructionSite.toLowerCase():
        const allInspections = await this._constructionSiteInspectionRepo.fetch(
          customerId,
          undefined,
          { inspectionDateFrom: fromDate, inspectionDateTo: toDate }
        );

        const constructionSiteIds = uniq(
          allInspections.map(inspection => inspection.constructionSiteId)
        );
        const csInspectionsKeyed = groupBy(
          allInspections,
          'constructionSiteId'
        );

        const cSites = await this._constructionSiteRepo.fetch(customerId, {
          ids: constructionSiteIds,
          ...csQueryOptions,
        });

        assetResponse.assets = cSites.map(cs => {
          const ar: AssetReport = {
            _inspections: csInspectionsKeyed[cs.id],
            _assetType: EntityTypes.ConstructionSite,
            _name: cs.name,
            ...cs,
          };
          return ar;
        });

        break;
      case EntityTypes.Facility.toLowerCase():
        const facInspections = await this._facilityInspectionRepo.fetch(
          customerId,
          undefined,
          { inspectionDateFrom: fromDate, inspectionDateTo: toDate }
        );

        const facIds = uniq(
          facInspections.map(inspection => inspection.facilityId)
        );
        const facInspectionsKeyed = groupBy(facInspections, 'facilityId');

        const facs = await this._facilityRepo.fetch(customerId, {
          ids: facIds,
          ...facilityQueryOptions,
        });

        assetResponse.assets = facs.map(facility => {
          const ar: AssetReport = {
            _inspections: facInspectionsKeyed[facility.id],
            _assetType: EntityTypes.Facility,
            _name: facility.name,
            ...facility,
          };
          return ar;
        });
        break;
      case EntityTypes.Outfall.toLowerCase():
        const outfallInspections = await this._outfallInspectionRepo.fetch(
          customerId,
          undefined,
          { inspectionDateFrom: fromDate, inspectionDateTo: toDate }
        );

        const outfallIds = uniq(
          outfallInspections.map(inspection => inspection.outfallId)
        );
        const outfallInspectionsKeyed = groupBy(
          outfallInspections,
          'outfallId'
        );

        const outfalls = await this._outfallRepo.fetch(customerId, {
          ids: outfallIds,
          ...outfallQueryOptions,
        });

        assetResponse.assets = outfalls.map(outfall => {
          const ar: AssetReport = {
            _inspections: outfallInspectionsKeyed[outfall.id],
            _assetType: EntityTypes.Outfall,
            _name: outfall.location,
            ...outfall,
          };
          return ar;
        });
        break;
      case EntityTypes.Structure.toLowerCase():
        const strucInspections = await this._structureInspectionRepo.fetch(
          customerId,
          undefined,
          { inspectionDateFrom: fromDate, inspectionDateTo: toDate }
        );

        const structureIds = uniq(
          strucInspections.map(inspection => inspection.structureId)
        );
        const structureInspectionsKeyed = groupBy(
          strucInspections,
          'structureId'
        );

        const strucs = await this._structureRepo.fetch(customerId, {
          ids: structureIds,
          ...structureQueryOptions,
        });

        assetResponse.assets = strucs.map(structure => {
          let ar = {
            _inspections: structureInspectionsKeyed[structure.id],
            _assetType: EntityTypes.Structure,
            _name: structure.name,
            ...structure,
          };
          return ar;
        });
        break;

      case STAFF:
        // Completed assets
        const completedAssets = await Promise.all([
          this._constructionSiteInspectionRepo.fetch(customerId, null, {
            inspectionDateFrom: fromDate,
            inspectionDateTo: toDate,
          }),
          this._outfallInspectionRepo.fetch(customerId, null, {
            inspectionDateFrom: fromDate,
            inspectionDateTo: toDate,
          }),
          this._structureInspectionRepo.fetch(customerId, null, {
            inspectionDateFrom: fromDate,
            inspectionDateTo: toDate,
          }),
          this._facilityInspectionRepo.fetch(customerId, null, {
            inspectionDateFrom: fromDate,
            inspectionDateTo: toDate,
          }),
          this._illicitDischargeRepo.fetch(customerId, {
            reportedFrom: fromDate,
            reportedTo: toDate,
          }),
          this._citizenReportRepo.fetch(customerId, {
            reportedFrom: fromDate,
            reportedTo: toDate,
          }),
        ]);

        const completedPreStaffAssets = completedAssets.map((asset, index) =>
          this._formatStaffAsset(
            asset,
            index <= 3 ? 'inspectionDate' : 'dateReported', // For citizen report and illicit discharge
            index <= 3 ? 'inspectorId' : 'investigatorId',
            index <= 3 ? 'inspectorId2' : 'investigator2Id'
          )
        );
        const completedStaffReport = this._getStaffReport(
          completedPreStaffAssets
        );

        // Future assets
        const futureAssets = await Promise.all([
          this._constructionSiteInspectionRepo.fetch(customerId, null, {
            scheduledInspectionDateFrom: fromDate,
            scheduledInspectionDateTo: toDate,
            staffInspection: true,
          }),
          this._outfallInspectionRepo.fetch(customerId, null, {
            scheduledInspectionDateFrom: fromDate,
            scheduledInspectionDateTo: toDate,
            staffInspection: true,
          }),
          this._structureInspectionRepo.fetch(customerId, null, {
            scheduledInspectionDateFrom: fromDate,
            scheduledInspectionDateTo: toDate,
            staffInspection: true,
          }),
          this._facilityInspectionRepo.fetch(customerId, null, {
            scheduledInspectionDateFrom: fromDate,
            scheduledInspectionDateTo: toDate,
            staffInspection: true,
          }),
        ]);

        const scheduledPreStaffAssets = futureAssets.map(asset =>
          this._formatStaffAsset(
            asset,
            'scheduledInspectionDate',
            'inspectorId',
            'inspectorId2'
          )
        );
        const scheduledStaffReport = this._getStaffReport(
          scheduledPreStaffAssets,
          true
        );

        assetResponse.staffReports = {
          completed: completedStaffReport,
          scheduled: scheduledStaffReport,
        };
        break;
      default:
        throw Error(`${assetType} is not implemented`);
    }

    okay(res, assetResponse);
  }

  @httpPost('/staff-export', authenticationRequired)
  async staffExport(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const { users, staffReports, dateRange } = req.body as {
      users: User[];
      staffReports: { completed: StaffReport[]; scheduled: StaffReport[] };
      dateRange: string;
    };

    const workbook = {
      Sheets: {},
      SheetNames: [],
    } as XLSX.WorkBook;

    const outputFileName = `staff_summary_${dateRange}.xlsx`;
    const tempCustomerPath = await getTempCustomerPath(customerId);

    function makeSheetData(
      reportData: StaffReport['data'],
      userName: string,
      key: string | number
    ) {
      const payload = {
        Users: userName,
        'Construction Inspections':
          (reportData.constructionSite && reportData.constructionSite[key]) ||
          0,
        'Outfall Screenings':
          (reportData.outfall && reportData.outfall[key]) || 0,
        'Structural Control Inspections':
          (reportData.structure && reportData.structure[key]) || 0,
        'Facility Inspections':
          (reportData.facility && reportData.facility[key]) || 0,
        'IDDE Investigations':
          (reportData.illicitDischarge && reportData.illicitDischarge[key]) ||
          0,
        'Citizen Reports':
          (reportData.citizenReport && reportData.citizenReport[key]) || 0,
      };

      return {
        payload,
        isValid: Object.values(omit(payload, 'Users')).some(p => Boolean(p)),
      };
    }

    function makeSheet(reports: StaffReport[]) {
      let totalCount = 0;

      reports.forEach(report => {
        totalCount += report.data.total;

        const userSheetData = users.reduce((acc, user) => {
          const { payload, isValid } = makeSheetData(
            report.data,
            user.fullName,
            user.id
          );

          if (isValid) {
            acc.push(payload);
          }

          return acc;
        }, []);

        const extraSheetData = ['Unassigned', 'Total'].reduce((acc, key) => {
          const { payload, isValid } = makeSheetData(
            report.data,
            key,
            key.toLowerCase()
          );

          if (isValid) {
            acc.push(payload);
          }

          return acc;
        }, []);

        const sheet = XLSX.utils.aoa_to_sheet(
          shared.entitiesToAoA([...userSheetData, ...extraSheetData])
        );

        addSheetToWorkbook(report.date, sheet, workbook);
      });

      return totalCount;
    }

    const completedCount = makeSheet(staffReports.completed);
    const scheduledCount = makeSheet(staffReports.scheduled);

    const sheetData = XLSX.utils.aoa_to_sheet(
      shared.entitiesToAoA([
        { Completed: completedCount, Scheduled: scheduledCount },
      ])
    );

    addSheetToWorkbook('Total', sheetData, workbook);

    XLSX.writeFile(workbook, path.join(tempCustomerPath, outputFileName));

    okay(res, {
      fileName: outputFileName,
      fileUrl: getPublicExportLink(
        outputFileName,
        this.getAbsoluteAppRoot(req),
        customerId
      ),
    });
  }

  private _formatStaffAsset(
    assets: any[],
    dateField: string,
    userField: string,
    userField2: string
  ) {
    const groupedByDateAsset = groupBy(assets, asset =>
      moment(asset[dateField]).format('MMMM YYYY')
    );

    return Object.keys(groupedByDateAsset).reduce((acc, date) => {
      const asset = groupedByDateAsset[date].filter(a => Boolean(a[userField]));
      const groupedByUserAsset = groupBy(asset, userField);

      acc[date] = {};
      acc[date]['unassigned'] = groupedByDateAsset[date].filter(
        a => !a[userField] && !a[userField2]
      ).length;

      Object.keys(groupedByUserAsset).forEach(userId => {
        acc[date][userId] = groupedByUserAsset[userId].length;
      });

      acc[date]['total'] = sum(Object.values(acc[date]));

      return acc;
    }, {});
  }

  private _getStaffReport(assets: any[], scheduled?: boolean): StaffReport[] {
    const assetDates = assets
      .reduce<string[]>(
        (acc, asset) => uniq([...acc, ...Object.keys(asset)]),
        []
      )
      .sort((a, b) => (new Date(a) > new Date(b) ? 1 : -1));

    return assetDates.reduce((acc, date) => {
      const reportData = {
        constructionSite: assets[0][date],
        outfall: assets[1][date],
        structure: assets[2][date],
        facility: assets[3][date],
        illicitDischarge: assets[4] ? assets[4][date] : undefined,
        citizenReport: assets[5] ? assets[5][date] : undefined,
      };

      const total = Object.values(reportData)
        .filter(Boolean)
        .reduce<number>((t, asset) => (t += asset.total), 0);

      acc.push({
        date: `${date}${scheduled ? ' Scheduled' : ''}`,
        data: { ...reportData, total },
      });

      return acc;
    }, []);
  }
}

function addSheetToWorkbook(
  sheetName: string,
  sheet: XLSX.WorkSheet,
  workbook: XLSX.WorkBook
) {
  workbook.SheetNames.push(sheetName);
  workbook.Sheets[sheetName] = sheet;
  return workbook;
}

async function getTempCustomerPath(customerId: string) {
  const root = path.dirname(process.argv[1]);
  const tempPath = path.join(root, 'public/temp_export', customerId);
  await fs.ensureDir(tempPath);
  return tempPath;
}

function getPublicExportLink(
  file: string,
  absoluteRootUrl: string,
  customerId: string
) {
  return [absoluteRootUrl, 'public/temp_export', customerId, file].join('/');
}
