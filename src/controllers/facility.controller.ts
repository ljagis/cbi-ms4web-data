import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  EntityTypes,
  createClassFromObject,
  NotFoundError,
  fileToFileLink,
} from '../shared';
import { customerAssetLimit, enforceFeaturePermission } from '../middleware';
import * as express from 'express';
import {
  Facility,
  Principal,
  InspectionQueryOptions,
  FacilityInspection,
  FileEntity,
  FileWithLinks,
  FacilityQueryOptions,
} from '../models';
import { FacilityDetailsResult } from '../models/asset-details-result';
import { BaseController } from './base.controller';
import {
  CommunityRepository,
  ReceivingWaterRepository,
  WatershedRepository,
  FileRepository,
  CustomFieldRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  ContactRepository,
} from '../data';

const BASE_ROUTE = `${APP_PATH}/api/facilities`;

@controller(BASE_ROUTE)
export class FacilityController extends BaseController {
  constructor(
    private _facilityInspectionRepo: FacilityInspectionRepository,
    private _facilityRepo: FacilityRepository,
    private _communityRepo: CommunityRepository,
    private _receivingWaterRepo: ReceivingWaterRepository,
    private _watershedRepo: WatershedRepository,
    private _fileRepo: FileRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _contactRepo: ContactRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const currentCustomer = await this.getCustomerId(req);
    const principal = this.httpContext.user as Principal;
    const queryOptions: FacilityQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const facilities = await this._facilityRepo.fetch(
      currentCustomer,
      queryOptions
    );
    return this.ok(facilities);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async single(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: FacilityQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const facility = await this._facilityRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (facility) {
      return this.ok(facility);
    }
    return this.notFound();
  }

  @httpGet('/details/:id([0-9]+)', authenticationRequired)
  @httpGet('/:id([0-9]+)/details', authenticationRequired)
  async details(req: express.Request) {
    const customerId = await this.getCustomerId(req);
    const rootUrl = this.getAbsoluteAppRoot(req);
    const assetId = Number(req.params.id);
    const inspectionQOpts: InspectionQueryOptions = {
      orderByRaw:
        `${FacilityInspection.sqlField(f => f.inspectionDate)} desc, ` +
        `${FacilityInspection.sqlField(f => f.scheduledInspectionDate)} desc`,
    };
    const principal = this.httpContext.user as Principal;
    const queryOptions: FacilityQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const facility = await this._facilityRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (!facility) {
      throw new NotFoundError();
    }
    const inspections = await this._facilityInspectionRepo.fetch(
      customerId,
      assetId,
      inspectionQOpts
    );
    const contacts = await this._contactRepo.fetchContactsForEntity(
      Facility,
      assetId,
      customerId
    );
    // all has lookups
    const communities = await this._communityRepo.fetch(customerId);
    const receivingWaters = await this._receivingWaterRepo.fetch(customerId);
    const watersheds = await this._watershedRepo.fetch(customerId);
    const customFields = await this._customFieldRepo.getCustomFieldValuesForEntity(
      Facility,
      assetId,
      customerId
    );
    const files = await this._fileRepo
      .fetch(Facility, assetId, customerId)
      .map<FileEntity, FileWithLinks>(file =>
        fileToFileLink(file, customerId, rootUrl)
      );
    const result: FacilityDetailsResult = {
      facility,
      inspections,
      contacts,
      communities,
      receivingWaters,
      watersheds,
      customFields,
      files,
    };
    return this.ok(result);
  }

  /**
   * Creates a new Facility.
   *
   * `POST /api/facilities`
   *
   * @param {express.Request} req POST with header `Content-Type: application/json`
   * @param {express.Response} res Returns a response with `id` of newly created object
   * @param {express.NextFunction} next
   */
  @httpPost(
    '/',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit'),
    customerAssetLimit(EntityTypes.Facility)
  )
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const asset = createClassFromObject(Facility, req.body);
    asset.customerId = customerId; // ensure customer id
    await this.validate(asset);
    const createdAsset = await this._facilityRepo.insert(asset, customerId);
    return this.created(`${BASE_ROUTE}/${createdAsset.id}`, createdAsset);
  }

  @httpPut(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: FacilityQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const dbAsset = await this._facilityRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (dbAsset) {
      await this._facilityRepo.updateAsset(assetId, req.body, customerId);
      const updatedAsset = await this._facilityRepo.fetchById(
        assetId,
        customerId
      );
      return this.ok(updatedAsset);
    }
    return this.notFound();
  }

  @httpDelete(
    '/:id([0-9]+)',
    authenticationRequired,
    enforceFeaturePermission('assets', 'delete')
  )
  async del(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const assetId = Number(req.params.id);
    const principal = this.httpContext.user as Principal;
    const queryOptions: FacilityQueryOptions = {};
    if (await principal.isContractor()) {
      queryOptions.contractorEmail = principal.details.email;
    }
    const facility = await this._facilityRepo.fetchById(
      assetId,
      customerId,
      queryOptions
    );
    if (facility) {
      await this._facilityRepo.deleteAsset(assetId, customerId);
      return this.ok();
    }
    return this.notFound();
  }
}
