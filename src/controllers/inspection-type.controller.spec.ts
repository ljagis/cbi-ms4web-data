import { config } from '../test/helper';
import { container } from '../inversify.config';
import { InspectionType } from '../models';
import { InspectionTypeRepository } from '../data';
import * as chai from 'chai';
import * as Knex from 'knex';

const baseRoute = `inspectiontypes`;
const customerId = config.customerId;

describe(`Inspection Type Controller`, async () => {
  const itRepo = container.get(InspectionTypeRepository);
  const knex = container.get<Knex>(Knex);

  let createdId: number;

  before(async () => {
    // setup
  });

  after(async () => {
    if (createdId) {
      await knex(InspectionType.getTableName())
        .del()
        .where(InspectionType.sqlField(f => f.id), createdId);
    }
  });

  it(`should POST`, async () => {
    const postData: Partial<InspectionType> = {
      name: 'test inspection type',
      // TODO templateId
    };
    const response = await chai
      .request(config.baseUrl)
      .post(`/api/${baseRoute}`)
      .set('Cookie', config.cookie)
      .send(postData);

    const returnedData = response.body as InspectionType;
    createdId = +response.body.id;
    chai.expect(response).to.have.status(201);
    chai.expect(response).to.be.json;

    chai.expect(returnedData.id).greaterThan(0);
    chai.expect(returnedData.name).eq(postData.name);
  });

  it(`should GET all`, async () => {
    const response = await chai
      .request(config.baseUrl)
      .get(`/api/${baseRoute}`)
      .set('Cookie', config.cookie);

    chai.expect(response).to.have.status(200);
    chai.expect(response).to.be.json;

    const dataType = (response.body as InspectionType[]).find(
      log => log.id === createdId
    );
    chai.expect(dataType).ok;
  });

  it(`should PUT`, async () => {
    const expectedLog: Partial<InspectionType> = {
      name: 'test inspection type updated',
    };
    const resp = await chai
      .request(config.baseUrl)
      .put(`/api/${baseRoute}/${createdId}`)
      .set('Cookie', config.cookie)
      .send(expectedLog);

    const returnedItem: InspectionType = resp.body;

    chai.expect(resp).json;
    chai.expect(returnedItem.name).eq(expectedLog.name);
  });

  it(`should DELETE`, async () => {
    const resp = await chai
      .request(config.baseUrl)
      .del(`/api/${baseRoute}/${createdId}`)
      .set('Cookie', config.cookie);

    const returnedDetail = await itRepo.fetchById(customerId, createdId);
    chai.expect(resp).json;
    chai.expect(resp).status(200);
    chai.expect(returnedDetail).not.ok;
  });
});
