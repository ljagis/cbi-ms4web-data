import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  created,
  createClassFromObject,
  NotFoundError,
} from '../shared';
import * as express from 'express';
import { StructureControlType } from './../models';
import { BaseController } from './base.controller';

import { StructureControlTypeRepository } from '../data';
import { adminRequired } from '../middleware';

const CONTROLLER_BASE_ROUTE = 'structurecontroltypes';

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class StructureControlTypesController extends BaseController {
  constructor(
    private _structureControlTypeRepo: StructureControlTypeRepository
  ) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const controlTypes = await this._structureControlTypeRepo.fetch(customerId);
    okay(res, controlTypes);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const controlType = await this._structureControlTypeRepo.fetchById(
      id,
      customerId
    );
    okay(res, controlType);
  }

  @httpPost('/', authenticationRequired, adminRequired)
  async create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let controlType = createClassFromObject<StructureControlType>(
      StructureControlType,
      req.body
    );
    await this.validate(controlType);
    controlType = await this._structureControlTypeRepo.insert(
      controlType,
      customerId
    );

    created(res, controlType);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired, adminRequired)
  async update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let customerId = await this.getCustomerId(req);
    let id = Number(req.params.id);

    let controlType = await this._structureControlTypeRepo.fetchById(
      id,
      customerId
    );
    if (!controlType) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    Object.assign(controlType, req.body);
    await this.validate(controlType);
    await this._structureControlTypeRepo.update(id, controlType, customerId);
    okay(res, undefined);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    const customerId = await this.getCustomerId(req);
    const controlType = await this._structureControlTypeRepo.fetchById(
      id,
      customerId
    );
    if (!controlType) {
      throw new NotFoundError(`Resource ${id} not found`);
    }
    await this._structureControlTypeRepo.del(id, customerId);
    okay(res, undefined);
  }
}
