import { APP_PATH } from '../shared';
import { authenticationRequired } from 'express-stormpath';

import { controller, httpGet, httpPut } from 'inversify-express-utils';
import { EntityTypes } from '../shared';
import { enforceFeaturePermission } from '../middleware';

import { EntityCustomFieldController } from './entity-custom-field.controller';

const CONTROLLER_ROUTE = 'constructionsites';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class ConstructionSiteCustomFieldController extends EntityCustomFieldController {
  constructor() {
    super(EntityTypes.ConstructionSite);
  }

  @httpGet('/:id([0-9]+)/customfields', authenticationRequired)
  index() {
    return super.index.apply(this, arguments);
  }

  @httpPut(
    '/:id([0-9]+)/customfields',
    authenticationRequired,
    enforceFeaturePermission('assets', 'edit')
  )
  update() {
    return super.update.apply(this, arguments);
  }
}
