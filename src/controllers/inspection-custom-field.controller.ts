import { CustomFieldValueMap } from '../models';
import { Request, Response, NextFunction } from 'express';
import { inject, unmanaged } from 'inversify';
import { BaseController } from './base.controller';

import {
  ConstructionSiteRepository,
  ConstructionSiteInspectionRepository,
  CustomFieldRepository,
  FacilityInspectionRepository,
  FacilityRepository,
  OutfallRepository,
  OutfallInspectionRepository,
  StructureRepository,
  StructureInspectionRepository,
} from '../data';
import {
  ConstructionSite,
  ConstructionSiteInspection,
  Facility,
  FacilityInspection,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
} from '../models';
import { okay, NotFoundError } from '../shared';

type TypeOfAsset =
  | typeof ConstructionSite
  | typeof Facility
  | typeof Outfall
  | typeof Structure;
type TypeOfInspection =
  | typeof ConstructionSiteInspection
  | typeof FacilityInspection
  | typeof OutfallInspection
  | typeof StructureInspection;

export abstract class InspectionCustomFieldController extends BaseController {
  @inject(ConstructionSiteRepository)
  protected _csRepo: ConstructionSiteRepository;

  @inject(ConstructionSiteInspectionRepository)
  protected _csInspectionRepo: ConstructionSiteInspectionRepository;

  @inject(FacilityInspectionRepository)
  protected _facilityInspectionRepo: FacilityInspectionRepository;

  @inject(FacilityRepository) protected _facilityRepo: FacilityRepository;

  @inject(OutfallRepository) protected _outfallRepo: OutfallRepository;

  @inject(OutfallInspectionRepository)
  protected _outfallInspectionRepo: OutfallInspectionRepository;

  @inject(StructureRepository) protected _structureRepo: StructureRepository;

  @inject(StructureInspectionRepository)
  protected _structureInspectionRepo: StructureInspectionRepository;

  @inject(CustomFieldRepository)
  protected _customFieldRepo: CustomFieldRepository;

  constructor(
    @unmanaged() protected TAsset: TypeOfAsset,
    @unmanaged() protected TInspection: TypeOfInspection
  ) {
    super();
  }

  async index(req: Request, res: Response, next: NextFunction) {
    const assetId = Number(req.params.assetId);
    const inspectionId = Number(req.params.inspectionId);
    const customerId = await this.getCustomerId(req);

    const inspection = await this._getInspectionRepo().fetchById(
      inspectionId,
      assetId,
      customerId
    );
    if (!inspection) {
      throw new NotFoundError('Resource not found');
    }

    const cfvs = await this._customFieldRepo.getCustomFieldValuesForInspection(
      this.TAsset,
      this.TInspection,
      assetId,
      inspectionId,
      customerId
    );

    okay(res, cfvs);
  }

  async update(req: Request, res: Response, next: NextFunction) {
    const assetId = Number(req.params.assetId);
    const inspectionId = Number(req.params.inspectionId);
    const customerId = await this.getCustomerId(req);
    const data: CustomFieldValueMap[] = req.body;

    const inspection = await this._getInspectionRepo().fetchById(
      inspectionId,
      assetId,
      customerId
    );
    if (!inspection) {
      throw new NotFoundError('Resource not found');
    }

    await this._customFieldRepo.updateCustomFieldValuesForInspection(
      this.TInspection,
      inspectionId,
      data,
      customerId
    );

    okay(res);
  }

  _getInspectionRepo() {
    if (this.TInspection === ConstructionSiteInspection) {
      return this._csInspectionRepo;
    } else if (this.TInspection === FacilityInspection) {
      return this._facilityInspectionRepo;
    } else if (this.TInspection === OutfallInspection) {
      return this._outfallInspectionRepo;
    } else if (this.TInspection === StructureInspection) {
      return this._structureInspectionRepo;
    } else {
      throw Error('Not implemented');
    }
  }
}
