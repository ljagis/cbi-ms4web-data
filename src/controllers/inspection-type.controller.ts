import { authenticationRequired } from 'express-stormpath';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
} from 'inversify-express-utils';
import {
  APP_PATH,
  okay,
  notFound,
  createClassFromObject,
  NotFoundError,
  created,
} from '../shared';
import * as express from 'express';
import { InspectionType } from './../models';

import { adminRequired } from '../middleware';
import { BaseController } from './base.controller';
import { InspectionTypeRepository } from '../data';

const CONTROLLER_BASE_ROUTE = 'inspectiontypes';

// TODO: add tests

@controller(`${APP_PATH}/api/${CONTROLLER_BASE_ROUTE}`)
export class InspectionTypeController extends BaseController {
  constructor(private _inspTypeRepo: InspectionTypeRepository) {
    super();
  }

  @httpGet('/', authenticationRequired)
  async index(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const inspectionTypes = await this._inspTypeRepo.fetch(customerId);
    okay(res, inspectionTypes);
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async indexAt(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const id = +req.params.id;
    const inspectionType = await this._inspTypeRepo.fetchById(customerId, id);
    if (inspectionType) {
      okay(res, inspectionType);
    } else {
      notFound(res);
    }
  }

  @httpPost('/', authenticationRequired, adminRequired)
  async create(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const inspectionType = createClassFromObject(InspectionType, req.body);
    await this.validate(inspectionType);
    const createdItem = await this._inspTypeRepo.insert(
      customerId,
      inspectionType
    );
    created(res, createdItem);
  }

  @httpPut('/:id([0-9]+)', authenticationRequired, adminRequired)
  async update(req: express.Request, res: express.Response) {
    const customerId = await this.getCustomerId(req);
    const id = Number(req.params.id);
    const it = await this._inspTypeRepo.fetchById(customerId, id);
    if (!it) {
      throw new NotFoundError();
    }
    const itToUpdate = await this.validate(Object.assign(it, req.body));
    await this.validate(itToUpdate);
    const updated = await this._inspTypeRepo.update(customerId, id, req.body);
    okay(res, updated);
  }

  @httpPut('/:id([0-9]+)/archive', authenticationRequired, adminRequired)
  async archive(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const customerId = await this.getCustomerId(req);
    const id = +req.params.id;
    const existingIt = await this._inspTypeRepo.fetchById(customerId, id);

    if (!existingIt) {
      throw new NotFoundError();
    }

    const inspectionTypesWithExistingItEntityType = await this._inspTypeRepo.fetchByEntityType(
      customerId,
      existingIt.entityType
    );

    if (inspectionTypesWithExistingItEntityType.length === 1) {
      throw Error(
        `${
          existingIt.id
        } cannot be archived. At least 1 unarchived InspectionType must be available per entityType.`
      );
    }

    await this._inspTypeRepo.archive(customerId, id);

    const archivedInspectionType = await this._inspTypeRepo.fetchById(
      customerId,
      id
    );
    okay(res, archivedInspectionType);
  }

  @httpDelete('/:id([0-9]+)', authenticationRequired, adminRequired)
  async del(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const id = Number(req.params.id);
    const customerId = await this.getCustomerId(req);
    const it = await this._inspTypeRepo.fetchById(customerId, id);
    if (!it) {
      throw new NotFoundError();
    }
    await this._inspTypeRepo.delete(customerId, id);
    okay(res);
  }
}
