import { APP_PATH, createClassFromObject, okay } from '../shared';
import { AnnualReport, initialAnnualReportData } from '../models';
import { AnnualReportRepository, SwmpRepository } from '../data';
import { NextFunction, Request, Response } from 'express';
import {
  controller,
  httpGet,
  httpPost,
  httpPut,
} from 'inversify-express-utils';

import { BaseController } from './base.controller';
import { authenticationRequired } from 'express-stormpath';

const CONTROLLER_ROUTE = 'annualreport';

@controller(`${APP_PATH}/api/${CONTROLLER_ROUTE}`)
export class AnnualReportController extends BaseController {
  constructor(
    private _annualReportRepository: AnnualReportRepository,
    private _swmpRepository: SwmpRepository
  ) {
    super();
  }

  @httpGet('/:id([0-9]+)', authenticationRequired)
  async index(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const id = req.params.id;
    const annualReports = await this._annualReportRepository.fetchByIdAndCustomer(
      id,
      customerId
    );

    okay(res, {
      ...annualReports,
      report: JSON.parse(annualReports.report),
    });
  }

  // get all report statuses assigned to a swmp
  @httpGet('/status/:id([0-9]+)', authenticationRequired)
  async status(req: Request, res: Response, next: NextFunction) {
    const status: any[] = [];
    const swmpId = req.params.id;
    const customerId = await this.getCustomerId(req);
    const swmp = await this._swmpRepository.fetchByIdAndCustomer(
      swmpId,
      customerId
    );
    for (let i = swmp.year; i < swmp.year + swmp.planLength; i++) {
      const report = await this._annualReportRepository.fetchBySwmpAndCustomer(
        swmp.id,
        i,
        customerId
      );

      status.push({
        id: report ? report.id : null,
        year: i,
      });
    }

    okay(res, status);
  }

  @httpPost('/', authenticationRequired)
  async create(req: Request, res: Response, next: NextFunction) {
    const customerId = await this.getCustomerId(req);
    const annualReport = createClassFromObject(AnnualReport, req.body);
    const swmp = await this._swmpRepository.fetchByIdAndCustomer(
      annualReport.swmpId,
      customerId
    );
    let recentAnnualReport: AnnualReport = null;

    for (let i = annualReport.year - 1; i >= swmp.year; i--) {
      const annualReportByYear = await this._annualReportRepository.fetchBySwmpAndCustomer(
        annualReport.swmpId,
        i,
        customerId
      );

      if (annualReportByYear) {
        recentAnnualReport = annualReportByYear;
        break;
      }
    }
    annualReport.contactName = swmp.contactName;
    annualReport.phone = swmp.phone;
    annualReport.emailAddress = swmp.emailAddress;
    annualReport.mailingAddress = swmp.mailingAddress;
    annualReport.customerId = customerId;
    annualReport.report = recentAnnualReport
      ? recentAnnualReport.report
      : JSON.stringify(initialAnnualReportData);

    const createdAnnualReport = await this._annualReportRepository.create(
      annualReport
    );

    okay(res, createdAnnualReport);
  }

  @httpPut('/', authenticationRequired)
  async update(req: Request, res: Response, next: NextFunction) {
    const id = req.body.id;
    const report = JSON.stringify(req.body.report);

    const updatedAnnualReport = await this._annualReportRepository.update(
      id,
      report
    );

    okay(res, updatedAnnualReport);
  }

  @httpPut('/contact', authenticationRequired)
  async updateContact(req: Request, res: Response, next: NextFunction) {
    try {
      const { id, contactName, phone, emailAddress, mailingAddress } = req.body;
      const reportData: Partial<AnnualReport> = {
        contactName,
        phone,
        emailAddress,
        mailingAddress,
      };
      const updatedAnnualReport = await this._annualReportRepository.updateContactInfo(
        id,
        reportData
      );
      okay(res, updatedAnnualReport);
    } catch (error) {
      next(error);
    }
  }
}
