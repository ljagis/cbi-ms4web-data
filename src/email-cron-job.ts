const CronJob = require('cron').CronJob;

import * as Knex from 'knex';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as sendgrid from '@sendgrid/mail';

import {
  ConstructionSiteInspectionRepository,
  FacilityInspectionRepository,
  OutfallInspectionRepository,
  StructureInspectionRepository,
} from './data';
import { inject, injectable } from 'inversify';

import { Env } from './shared';

export interface DatabaseInspection {
  customerId: string;
  inspectionId: number;
  siteId: number;
  scheduledInspectionDate: Date | string;
  name: string;
  inspectionDate: null;
  inspectorId: number;
  inspectorId2?: number | null;
  email: string[];
  givenName: string[];
  surname: string[];
  userId: number;
  role: string;
  dailyInspectorEmail: boolean;
  mondayInspectorEmail: boolean;
  mondayAdminEmail: boolean;
}

export interface CollectedInspections {
  [key: string]: {
    info: EmailContactInfo;
    inspections: Inspections;
  };
}

export interface Inspections {
  [key: string]: EmailInspectionInfo;
}

export interface EmailInspectionInfo {
  customerId: string;
  inspectionId: number;
  siteId: number;
  scheduledInspectionDate: string;
  name: string;
  type: string;
  site: string;
  inspector?: string[];
  inspector2?: string[];
}

export interface EmailContactInfo {
  givenName: string;
  surname: string;
  email: string;
  role: string;
}

@injectable()
export class EmailCronJobRepository {
  env = process.env as Env;
  @inject(Knex) protected knex: Knex;
  @inject(ConstructionSiteInspectionRepository)
  private _constructionRepo: ConstructionSiteInspectionRepository;
  @inject(OutfallInspectionRepository)
  private _outfallRepo: OutfallInspectionRepository;
  @inject(StructureInspectionRepository)
  private _structureRepo: StructureInspectionRepository;
  @inject(FacilityInspectionRepository)
  private _facilityRepo: FacilityInspectionRepository;

  start() {
    const sendEmailReports = async (isMondayReport: boolean) => {
      const inspectionsToReport = await this.retrieveReports(isMondayReport);
      this.sortInspectionsForEmail(inspectionsToReport);
    };

    // seconds, minutes, hours, day of month, months, day of week (0-6, Sun-Sat)
    new CronJob(
      '0 0 2 * * 1-5',
      async function() {
        const dayOfTheWeek = moment()
          .utc()
          .day();
        const isMondayReport = dayOfTheWeek === 1 ? true : false;

        sendEmailReports(isMondayReport);
      },
      null,
      true,
      'America/Los_Angeles'
    );
  }

  organizeInspections = (
    incoming: DatabaseInspection[],
    prior: CollectedInspections | {},
    site: string,
    type: string,
    isMonday: boolean
  ) => {
    return incoming.reduce((obj, inspection) => {
      const {
        customerId,
        email,
        inspectionId,
        siteId,
        scheduledInspectionDate,
        inspectorId2,
        givenName,
        surname,
        name,
        dailyInspectorEmail,
        mondayInspectorEmail,
        mondayAdminEmail,
      } = inspection;
      if (!Array.isArray(email) || email.length < 3) {
        return obj;
      }

      email.forEach((contact, i) => {
        if (contact === null) {
          return;
        }
        // i 0-1 (inspector), i = 2 (admin)
        const marker = i === 2 ? 'ADMIN' : 'INSP';

        if (i === 2 && (!isMonday || !mondayAdminEmail)) {
          return;
        }

        if (i < 2 && !isMonday && !dailyInspectorEmail) {
          return;
        }
        /*
        **inspector Email logic for Mondays**
        dailyInspectorEmail | mondayInspectorEmail  | Result
        ------------------------------------------------------
              true          |        true           | show all
              false         |        false          | show none
              true          |        false          | show past due only
              false         |        true           | show all
        */
        if (
          i < 2 &&
          isMonday &&
          !dailyInspectorEmail &&
          !mondayInspectorEmail
        ) {
          return;
        }
        if (i < 2 && !mondayInspectorEmail && type === 'upcoming') {
          return;
        }
        if (obj[`${contact}${marker}`]) {
          if (
            obj[`${contact}${marker}`]['inspections'][
              `${inspectionId}${siteId}${scheduledInspectionDate}`
            ]
          ) {
            return; // duplicate inspection
          }
          // add to existing contact's tally
          obj[`${contact}${marker}`]['inspections'][
            `${inspectionId}${siteId}${scheduledInspectionDate}`
          ] = {
            customerId,
            inspectionId,
            siteId,
            scheduledInspectionDate: moment(scheduledInspectionDate).format(
              'L'
            ),
            name,
            site,
            type,
            ...(i === 2 && { inspector: [email[0], givenName[0], surname[0]] }),
            ...(i === 2 &&
              inspectorId2 !== null && {
                inspector2: [email[1], givenName[1], surname[1]],
              }),
          };
          return;
        }
        // new contact
        obj[`${contact}${marker}`] = {
          info: {
            givenName: givenName[i],
            surname: surname[i],
            email: contact,
            role: marker,
          },
          inspections: {
            [`${inspectionId}${siteId}${scheduledInspectionDate}`]: {
              customerId,
              inspectionId,
              siteId,
              scheduledInspectionDate: moment(scheduledInspectionDate).format(
                'L'
              ),
              name,
              site,
              type,
              ...(i === 2 && {
                inspector: [email[0], givenName[0], surname[0]],
              }),
              ...(i === 2 &&
                inspectorId2 !== null && {
                  inspector2: [email[1], givenName[1], surname[1]],
                }),
            },
          },
        };
        return;
      });
      return obj;
    }, prior);
  };

  async retrieveReports(isMonday: boolean) {
    const constructionPastDue = await this._constructionRepo.pastDueInspectionsReport();
    const outfallPastDue = await this._outfallRepo.pastDueInspectionsReport();
    const structurePastDue = await this._structureRepo.pastDueInspectionsReport();
    const facilityPastDue = await this._facilityRepo.pastDueInspectionsReport();

    const iteration1 = this.organizeInspections(
      constructionPastDue,
      {},
      'Construction Site',
      'pastdue',
      isMonday
    );
    const iteration2 = this.organizeInspections(
      outfallPastDue,
      iteration1,
      'Outfall',
      'pastdue',
      isMonday
    );
    const iteration3 = this.organizeInspections(
      structurePastDue,
      iteration2,
      'Structure',
      'pastdue',
      isMonday
    );
    const iteration4 = this.organizeInspections(
      facilityPastDue,
      iteration3,
      'Facility',
      'pastdue',
      isMonday
    );
    let iteration8;

    if (isMonday) {
      const constructionUpcoming = await this._constructionRepo.upcomingInspectionsReport();
      const outfallUpcoming = await this._outfallRepo.upcomingInspectionsReport();
      const structureUpcoming = await this._structureRepo.upcomingInspectionsReport();
      const facilityUpcoming = await this._facilityRepo.upcomingInspectionsReport();

      const iteration5 = this.organizeInspections(
        constructionUpcoming,
        iteration4,
        'Construction Site',
        'upcoming',
        isMonday
      );
      const iteration6 = this.organizeInspections(
        outfallUpcoming,
        iteration5,
        'Outfall',
        'upcoming',
        isMonday
      );
      const iteration7 = this.organizeInspections(
        structureUpcoming,
        iteration6,
        'Structure',
        'upcoming',
        isMonday
      );
      iteration8 = this.organizeInspections(
        facilityUpcoming,
        iteration7,
        'Facility',
        'upcoming',
        isMonday
      );
    }
    return isMonday ? iteration8 : iteration4;
  }

  sortInspectionsForEmail = async (
    collectedInspections: CollectedInspections | {}
  ) => {
    for (let person in collectedInspections) {
      if (
        collectedInspections[person].inspections &&
        collectedInspections[person].info
      ) {
        let upcoming = [];
        let pastdue = [];

        const sortByDate = (inspections: Inspections) => {
          return _.sortBy(inspections, [
            function(inspection) {
              const splitDate = inspection.scheduledInspectionDate.split('/');
              const numberDate = [splitDate[2], splitDate[0], splitDate[1]];
              const sortableDate = numberDate.join('');
              return sortableDate;
            },
          ]);
        };

        const sorted = sortByDate(collectedInspections[person].inspections);
        sorted.forEach(inspection => {
          const {
            customerId,
            inspectionId,
            siteId,
            scheduledInspectionDate,
            inspector,
            inspector2,
            name,
            site,
            type,
          } = inspection;
          const inspectionString = `${site} "${name}" in ${customerId} ${
            type === 'pastdue' ? 'was' : 'is'
          } scheduled to be inspected on ${scheduledInspectionDate}`;

          const inspectorsString =
            collectedInspections[person].info.role === 'ADMIN'
              ? `${
                  inspector
                    ? ` by ${inspector[1]} ${inspector[2]} (${inspector[0]})`
                    : ''
                }${
                  inspector2
                    ? `and ${inspector2[1]} ${inspector2[2]} (${inspector2[0]})`
                    : ''
                }`
              : '';

          const inspectionLocation = () => {
            switch (site) {
              case 'Construction Site':
                return 'constructionsites';
              case 'Outfall':
                return 'outfalls';
              case 'Structure':
                return 'structures';
              case 'Facility':
                return 'facilities';
              default:
                return 'error';
            }
          };

          const linkString = `. <a href="${
            this.env.BROWSER_BASE_URL
          }/${inspectionLocation()}/${siteId}/inspections/${inspectionId}">Inspection Link</a>`;
          const string = inspectionString.concat(inspectorsString, linkString);
          type === 'pastdue' ? pastdue.push(string) : upcoming.push(string);
        });

        pastdue.length && pastdue.unshift('Past Due Inspections:');
        upcoming.length &&
          upcoming.unshift(`This Week's Upcoming Inspections:`);
        await this.sendEmail(
          collectedInspections[person].info,
          upcoming,
          pastdue
        );
        // TODO Save sent information for records
      }
    }
  };

  sendEmail = async (
    info: EmailContactInfo,
    upcoming: string[],
    pastdue: string[]
  ) => {
    const to = `${info.email}`;
    const subject = `MS4web Stormwater ${
      info.role === 'ADMIN' ? 'Admin' : 'Inspector'
    } Report`;
    let content = [
      `Good morning ${info.givenName} ${info.surname}.`,
      'This email has been generated from the MS4web Stormwater compliance application.',
      'To view the inspections from the link provided, make sure you are logged in to MS4web under the correct location.',
      '----------',
    ];
    pastdue.length && content.push(...pastdue);
    pastdue.length && upcoming.length && content.push('----------');
    upcoming.length && content.push(...upcoming);
    const html = content.join('<br><br>');
    const sendGridEmail = { to, subject, from: this.env.MAIL_FROM, html };
    sendgrid.setApiKey(this.env.SENDGRID_API_KEY);
    let sendGridResponse;
    try {
      [sendGridResponse] = await sendgrid.send(<any>sendGridEmail, false);
    } catch (error) {
      const sendGridErrors = _.get(error, 'response.body.errors');
      const errorResponse = sendGridErrors
        ? sendGridErrors.map(error => error.messsage).join(' ')
        : 'Error sending email';
      throw Error(errorResponse);
    }

    if (
      sendGridResponse.statusCode !== 202 &&
      sendGridResponse.statusCode !== 204
    ) {
      return 'Error sending';
    }
    return 'sucess';
  };
}
