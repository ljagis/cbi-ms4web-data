import { Request, Response, NextFunction } from 'express';
import {
  CustomerRepository,
  AssetRepository,
  CitizenReportRepository,
  ConstructionSiteRepository,
  FacilityRepository,
  IllicitDischargeRepository,
  MonitoringLocationRepository,
  OutfallRepository,
  StructureRepository,
} from '../data';
import { container } from '../inversify.config';
import {
  forbidden,
  EntityTypes,
  methodNotAllowed,
  getCustomerId,
} from '../shared';

import { Entity } from '../models';
import { LIMIT_MAX_ACTIVE_RECORDS_PER_ASSET } from '../shared/plans';

export function premiumRequired(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const _customerRepo = container.get(CustomerRepository);
  getCustomerId(req)
    .then(customerId => {
      return _customerRepo.fetchById(customerId);
    })
    .then(customer => {
      if (customer.plan !== 'premium') {
        return forbidden(res, {
          message: 'Your current plan does not permit this request',
        });
      }
      return next();
    });
}

export function customerAssetLimit(assetType: string) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const customerId = await getCustomerId(req);
    const customerRepo = container.get<CustomerRepository>(CustomerRepository);
    // see if limit exists
    const customer = await customerRepo.fetchById(customerId);
    let assetRepo: AssetRepository<Entity>;
    if (!customer.unlimitedAssets) {
      if (assetType === EntityTypes.CitizenReport) {
        assetRepo = container.get(CitizenReportRepository);
      } else if (assetType === EntityTypes.ConstructionSite) {
        assetRepo = container.get(ConstructionSiteRepository);
      } else if (assetType === EntityTypes.Facility) {
        assetRepo = container.get(FacilityRepository);
      } else if (assetType === EntityTypes.IllicitDischarge) {
        assetRepo = container.get(IllicitDischargeRepository);
      } else if (assetType === EntityTypes.MonitoringLocation) {
        assetRepo = container.get(MonitoringLocationRepository);
      } else if (assetType === EntityTypes.Outfall) {
        assetRepo = container.get(OutfallRepository);
      } else if (assetType === EntityTypes.Structure) {
        assetRepo = container.get(StructureRepository);
      } else {
        throw new Error('Not implemented');
      }

      const count = await assetRepo.getCount(customerId);
      if (count >= LIMIT_MAX_ACTIVE_RECORDS_PER_ASSET) {
        // tslint:disable-next-line
        const message = `You have reached the maximum number of records(${LIMIT_MAX_ACTIVE_RECORDS_PER_ASSET}) allowed on your plan.  Please contact support to upgrade.`;
        methodNotAllowed(res, message);
        return;
      }
    }
    next();
  };
}
