import * as httpMocks from 'node-mocks-http';
import * as sinon from 'sinon';
import { expect } from 'chai';
import { HttpStatusCodes, Role } from '../shared';
import { enforceFeaturePermission } from './enforce-feature-permission.middleware';
import { Principal, UserFull } from '../models';

describe(`roles middleware tests`, () => {
  it(`should enforceFeaturePermission`, async () => {
    const principal = new Principal({
      email: 'anguyen@lja.com',
      role: 'super',
    } as UserFull);

    const requestMock = httpMocks.createRequest({
      principal: principal,
    });
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    expect(nextFake.called).false;
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );

    expect(nextFake.called).true;
  });

  it(`should not let readonly edit inspections`, async () => {
    const principal = new Principal({
      email: 'anguyen@lja.com',
      role: 'readonly',
    } as UserFull);
    const requestMock = httpMocks.createRequest({
      principal,
    });
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
    expect(nextFake.called).false;
  });
});

describe(`enforceFeaturePermission tests for role: admin`, () => {
  const principal = new Principal({
    email: 'anguyen@lja.com',
    role: 'admin',
  } as UserFull);
  const requestMock = httpMocks.createRequest({
    principal: principal,
  });

  it(`should edit inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should edit bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should edit activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should edit investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should edit assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });
});

describe(`enforceFeaturePermission tests for role: inspector`, () => {
  const principal = new Principal({
    email: 'anguyen@lja.com',
    role: 'inspector',
  } as UserFull);
  const requestMock = httpMocks.createRequest({
    principal: principal,
  });

  it(`should edit inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should edit bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
  });

  it(`should delete bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
  });

  it(`should edit activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
  });

  it(`should edit investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
  });

  it(`should edit assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
  });
});

describe(`enforceFeaturePermission tests for role: readonly`, () => {
  const principal = new Principal({
    email: 'anguyen@lja.com',
    role: 'readonly',
  } as UserFull);
  const requestMock = httpMocks.createRequest({
    principal: principal,
  });

  it(`should edit inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });
});

describe(`enforceFeaturePermission tests for role: inspector-no-delete`, () => {
  const principal = new Principal({
    email: 'anguyen@lja.com',
    role: Role['inspector-no-delete'],
  } as UserFull);
  const requestMock = httpMocks.createRequest({
    principal: principal,
  });

  it(`should edit inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete inspections`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('inspections', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete bmp`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('bmp', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete activityLogs`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('activityLogs', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).true;
  });

  it(`should delete investigations`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('investigations', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should edit assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'edit')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });

  it(`should delete assets`, async () => {
    const responseMock = httpMocks.createResponse();
    const nextFake = sinon.stub();
    await enforceFeaturePermission('assets', 'delete')(
      requestMock,
      responseMock,
      nextFake
    );
    expect(nextFake.called).false;
    expect(responseMock.statusCode).eq(HttpStatusCodes.Forbidden);
  });
});
