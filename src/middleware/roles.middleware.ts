import { Request, Response, NextFunction } from 'express';
import { UserFullRepository } from '../data';
import { container } from '../inversify.config';
import { forbidden } from '../shared/httpresponses';
import { getCustomerId } from '../shared';

/**
 * Adds user from db to req
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function appendUser(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const _userRepo = container.get(UserFullRepository);
  const customerId = await getCustomerId(req);
  const user = await _userRepo.fetchByEmailAndCustomer(
    req.user.email,
    customerId
  );
  if (!user) {
    throw Error(`User ${req.user.email} not found`);
  }
  req.user = Object.assign({}, user, req.user, {
    id: user.id, // use database id
  });
  next();
}

/**
 * Adds user from db and check to see if user is admin or greater
 */
export async function adminRequired(req, res, next) {
  const validRoles = {
    super: true,
    admin: true,
  };
  const _userRepo = container.get(UserFullRepository);
  const customerId = await getCustomerId(req);

  const user = await _userRepo.fetchByEmailAndCustomer(
    req.user.email,
    customerId
  );

  if (validRoles[user.role]) {
    return next();
  }
  return forbidden(res, {
    message: 'You do not have permission to visit this route.',
  });
}

/**
 * Adds user from db and check to see if user is super
 */
export async function superRequired(req, res, next) {
  const _userRepo = container.get(UserFullRepository);
  const customerId = await getCustomerId(req);

  const user = await _userRepo.fetchByEmailAndCustomer(
    req.user.email,
    customerId
  );
  req.user = Object.assign(req.user, user);
  if (user.role === 'super') {
    return next();
  }
  forbidden(res, {
    message: 'You do not have permission to visit this route.',
  });
}
