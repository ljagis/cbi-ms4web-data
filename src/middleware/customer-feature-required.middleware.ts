import { Request, Response, NextFunction } from 'express';
// import { forbidden } from '../shared';
import { CustomerFeaturesUnion } from '../models';

export function customerFeatureRequired(feature: CustomerFeaturesUnion) {
  return async (req: Request, res: Response, next: NextFunction) => {
    // TODO: implement this. Currently it only prevents on the client side
    next();
    return;
    // return forbidden(res, {
    //   message: `You do not have permission to execute this action. Feature: ${feature}`,
    // });
  };
}
