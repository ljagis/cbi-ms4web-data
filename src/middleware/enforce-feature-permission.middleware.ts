import { Request, Response, NextFunction } from 'express';
import { FeaturePermissionUnion, PermissionUnion } from '../shared/roles';
import { forbidden } from '../shared';

export function enforceFeaturePermission(
  feature: FeaturePermissionUnion,
  permission: PermissionUnion
) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const principal = req.principal;
    if (
      principal &&
      (await principal.hasFeaturePermission(feature, permission))
    ) {
      next();
      return;
    }
    return forbidden(res, {
      message: `You do not have permission to execute this action. Feature: ${feature}, ${permission}`,
    });
  };
}
