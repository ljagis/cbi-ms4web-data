import { Request, Response, NextFunction } from 'express';
import { notFound } from '../shared/httpresponses';
import { getCustomerId } from '../shared';
/**
 * Validates if the user has permissions to go to the `/api/files` route
 *
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
export async function validateFilesRouteMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const customerIdFromUrl: string = req.url.split('/')[1];
  const customerId = await getCustomerId(req);
  if (req.user.role === 'super') {
    next();
  }
  if (
    customerId &&
    customerIdFromUrl &&
    customerId.toLowerCase() === customerIdFromUrl.toLowerCase()
  ) {
    next();
  } else {
    notFound(res);
  }
}
