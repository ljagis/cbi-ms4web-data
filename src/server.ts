import 'tslib';
import 'reflect-metadata';

// required for class-validator
import 'core-js/shim';
import * as express from 'express';
import * as path from 'path';

import * as stormpath from 'express-stormpath';

import * as Bluebird from 'bluebird';
global.Promise = Bluebird; // sets all Promises to be bluebird

import { container } from './inversify.config';

import { Env, APP_PATH, expressErrorHandler } from './shared';

// Middlewares
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

import { InversifyExpressServer } from 'inversify-express-utils';
let mssql = require('mssql');
mssql.Promise = Bluebird; // use Bluebird as promise instead of Promise

import * as sharp from 'sharp';
import { validateFilesRouteMiddleware } from './middleware';
import { CustomAuthProvider } from './services';
import { PMISCronJobRepository } from './pmis-cron-job';
// disable sharp caching - will not lock `libvips` dll files
sharp.cache(false);
const pmisJobRepo = container.get(PMISCronJobRepository);

export class Server {
  env: Env;
  app: express.Express;
  appPath;

  /*
   * initializes instance of a server
   */
  static initialize() {
    return new Server().startListening();
  }

  constructor() {
    this.env = process.env as Env;
    this.appPath = APP_PATH;
    this.app = express();

    this.useStormpath();

    new InversifyExpressServer(
      container,
      null,
      null,
      this.app,
      CustomAuthProvider
    )
      .setConfig(app => {
        // middlewares
        this._applyMiddlewares(this.app);
      })
      .setErrorConfig(app => {
        app.use(expressErrorHandler);
      })
      .build();
  }

  _applyMiddlewares(app: express.Express) {
    app.use(cors());
    app.use(compression());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
    app.use(cookieParser('spatial'));
    app.use(morgan('combined'));

    // for serving files
    app.use(
      `${this.appPath}/api/staticfiles`,
      stormpath.authenticationRequired,
      validateFilesRouteMiddleware,
      express.static(this.env.FILE_STORAGE_PATH)
    );

    // for /public
    app.use(
      `${this.appPath}/public`,
      express.static(path.join(__dirname, 'public'))
    );
  }

  /**
   * Ensures that Stormpath and mssql connnection pools are ready before express listens
   */
  startListening() {
    // tslint:disable-next-line:no-console
    console.log(`Initializing express...`);
    this._stormpathReady().then(() => {
      this.app.listen(this.env.PORT, () => {
        pmisJobRepo.start();
        // tslint:disable-next-line:no-console
        console.log(
          `Express listening on ${this.env.PORT} in '${this.app.get('env') ||
            'unknown'}' mode`
        );
      });
    });
    return this;
  }

  /**
   * See https://github.com/stormpath/express-stormpath/blob/master/lib/config.yml
   *
   * @returns {Server}
   */
  useStormpath(): Server {
    this.app.use(
      stormpath.init(this.app, {
        // debug: 'debug',
        expand: {
          groups: true,
          customData: true,
        },
        web: {
          refreshTokenCookie: {
            maxAge: 604800000 * 4 * 12, // 1 year
          },
          accessTokenCookie: {
            path: `${this.appPath}/`,
          },
          login: {
            uri: `${this.appPath}/login`,
            nextUri: `${this.env.BROWSER_BASE_URL}`,
            view: __dirname + '/views/login.jade',
          },
          logout: {
            enabled: true,
            uri: `${this.appPath}/logout`,
            nextUri: `${this.appPath}/login`,
          },
          forgotPassword: {
            enabled: true,
            uri: `${this.appPath}/forgot`,
            nextUri: `${this.appPath}/login?status=forgot`,
            view: __dirname + '/views/forgot-password.jade',
          },
          changePassword: {
            enabled: true,
            // autoLogin: true, // As of 4.0, this doesn't work
            uri: `${this.appPath}/change`,
            nextUri: `${this.appPath}/login?status=reset`,
            view: __dirname + '/views/change-password.jade',
            errorUri: `${this.appPath}/forgot?status=invalid_sptoken`,
          },
          me: {
            uri: `${this.appPath}/me`,
            expand: {
              groups: true,
              customData: true,
            },
          },
          register: {
            enabled: false,
          },
          // Note: uncomment this if you roll your own views
          // produces: ['application/json']
        },
      })
    );

    // ensures `user` is on the request object
    this.app.use(stormpath.getUser);
    return this;
  }

  // returns promise when stormpath is ready
  private _stormpathReady(): Bluebird<{}> {
    return new Bluebird(resolve =>
      (<any>this.app).on('stormpath.ready', resolve)
    );
  }
}

Server.initialize();
