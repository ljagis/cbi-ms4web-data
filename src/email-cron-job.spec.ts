import 'mocha';

import * as chai from 'chai';

import { EmailCronJobRepository } from './email-cron-job';
import { container } from './inversify.config';

describe.only('Email Cron Job Tests', () => {
  const cjRepo = container.get(EmailCronJobRepository);

  describe(`organizeInspections`, () => {
    it(`should return given object when passed an empty array`, async () => {
      const mockData = { mockInfo: 'test' };
      const inspections = cjRepo.organizeInspections(
        [],
        mockData,
        'Construction',
        'upcoming',
        true
      );
      chai.expect(inspections).to.be.equal(mockData);
    });
    it(`should return an object with keys for every person (3) if all settings are true`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: 999,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            'mfoxwell@lja.com',
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', 'Matt', 'Ty'],
          surname: ['Mattoon', 'Foxwell', 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'mfoxwell@lja.comINSP': {
          info: {
            givenName: 'Matt',
            surname: 'Foxwell',
            email: 'mfoxwell@lja.com',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
              inspector2: ['mfoxwell@lja.com', 'Matt', 'Foxwell'],
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should not add inspector2 key/value pair if inspectorId2 is null`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            'mfoxwell@lja.com',
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', 'Matt', 'Ty'],
          surname: ['Mattoon', 'Foxwell', 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'mfoxwell@lja.comINSP': {
          info: {
            givenName: 'Matt',
            surname: 'Foxwell',
            email: 'mfoxwell@lja.com',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
          },
        },
      };
      // no inspector2 added
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true
      );

      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });

    it(`should filter out admins if it's not Monday, admins should never recieve an email not on Monday`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            'mfoxwell@lja.com',
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', 'Matt', 'Ty'],
          surname: ['Mattoon', 'Foxwell', 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        // no ADMIN added
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'mfoxwell@lja.comINSP': {
          info: {
            givenName: 'Matt',
            surname: 'Foxwell',
            email: 'mfoxwell@lja.com',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        false // isMonday
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should filter out admins if mondayAdminEmail is false even if it's Monday`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            'mfoxwell@lja.com',
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', 'Matt', 'Ty'],
          surname: ['Mattoon', 'Foxwell', 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: false,
        },
      ];
      const expectedResult = {
        // no ADMIN added
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'mfoxwell@lja.comINSP': {
          info: {
            givenName: 'Matt',
            surname: 'Foxwell',
            email: 'mfoxwell@lja.com',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true // isMonday
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should filter out inspector emails if dailyInspectorEmail is false AND
    it's not Monday, admins will be filtered by default because it's not
    Monday`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            'mfoxwell@lja.com',
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', 'Matt', 'Ty'],
          surname: ['Mattoon', 'Foxwell', 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: false,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
        {
          inspectionId: 58543,
          siteId: 16525,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-09T05:00:00.000Z',
          inspectorId: 143,
          inspectorId2: null,
          name: '749-751-753 COMPASS LOOP',
          customerId: 'ND_BISMARCK',
          email: [
            'swieser@bismarcknd.gov',
            null,
            'thalstengard@bismarcknd.gov',
          ],
          givenName: ['Scott', null, 'Terry'],
          surname: ['Wieser', null, 'Halstengard'],
          userId: 142,
          role: 'admin',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        // no dmattoon@cityofdubuque.org added
        'swieser@bismarcknd.govINSP': {
          info: {
            givenName: 'Scott',
            surname: 'Wieser',
            email: 'swieser@bismarcknd.gov',
            role: 'INSP',
          },
          inspections: {
            '58543165252019-10-09T05:00:00.000Z': {
              customerId: 'ND_BISMARCK',
              inspectionId: 58543,
              siteId: 16525,
              scheduledInspectionDate: '10/09/2019',
              name: '749-751-753 COMPASS LOOP',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        false
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should filter out inspector emails if dailyInspectorEmail is false AND
    mondayInspectorEmail is false, even if it's Monday`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: false,
          mondayInspectorEmail: false,
          mondayAdminEmail: true,
        },
        {
          inspectionId: 58543,
          siteId: 16525,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-09T05:00:00.000Z',
          inspectorId: 143,
          inspectorId2: null,
          name: '749-751-753 COMPASS LOOP',
          customerId: 'ND_BISMARCK',
          email: [
            'swieser@bismarcknd.gov',
            null,
            'thalstengard@bismarcknd.gov',
          ],
          givenName: ['Scott', null, 'Terry'],
          surname: ['Wieser', null, 'Halstengard'],
          userId: 142,
          role: 'admin',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        // no dmattoon@cityofdubuque.org added
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
          },
        },
        'swieser@bismarcknd.govINSP': {
          info: {
            givenName: 'Scott',
            surname: 'Wieser',
            email: 'swieser@bismarcknd.gov',
            role: 'INSP',
          },
          inspections: {
            '58543165252019-10-09T05:00:00.000Z': {
              customerId: 'ND_BISMARCK',
              inspectionId: 58543,
              siteId: 16525,
              scheduledInspectionDate: '10/09/2019',
              name: '749-751-753 COMPASS LOOP',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'thalstengard@bismarcknd.govADMIN': {
          info: {
            givenName: 'Terry',
            surname: 'Halstengard',
            email: 'thalstengard@bismarcknd.gov',
            role: 'ADMIN',
          },
          inspections: {
            '58543165252019-10-09T05:00:00.000Z': {
              customerId: 'ND_BISMARCK',
              inspectionId: 58543,
              siteId: 16525,
              scheduledInspectionDate: '10/09/2019',
              name: '749-751-753 COMPASS LOOP',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['swieser@bismarcknd.gov', 'Scott', 'Wieser'],
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true // isMonday
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should filter out "upcoming" emails if mondayInspectorEmail is false,
    note: "upcoming" emails are only passed in on Mondays through logic outside
    organizeInspections`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: false,
          mondayAdminEmail: true,
        },
        {
          inspectionId: 58543,
          siteId: 16525,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-09T05:00:00.000Z',
          inspectorId: 143,
          inspectorId2: null,
          name: '749-751-753 COMPASS LOOP',
          customerId: 'ND_BISMARCK',
          email: [
            'swieser@bismarcknd.gov',
            null,
            'thalstengard@bismarcknd.gov',
          ],
          givenName: ['Scott', null, 'Terry'],
          surname: ['Wieser', null, 'Halstengard'],
          userId: 142,
          role: 'admin',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        // no dmattoon@cityofdubuque.org added
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
          },
        },
        'swieser@bismarcknd.govINSP': {
          info: {
            givenName: 'Scott',
            surname: 'Wieser',
            email: 'swieser@bismarcknd.gov',
            role: 'INSP',
          },
          inspections: {
            '58543165252019-10-09T05:00:00.000Z': {
              customerId: 'ND_BISMARCK',
              inspectionId: 58543,
              siteId: 16525,
              scheduledInspectionDate: '10/09/2019',
              name: '749-751-753 COMPASS LOOP',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'thalstengard@bismarcknd.govADMIN': {
          info: {
            givenName: 'Terry',
            surname: 'Halstengard',
            email: 'thalstengard@bismarcknd.gov',
            role: 'ADMIN',
          },
          inspections: {
            '58543165252019-10-09T05:00:00.000Z': {
              customerId: 'ND_BISMARCK',
              inspectionId: 58543,
              siteId: 16525,
              scheduledInspectionDate: '10/09/2019',
              name: '749-751-753 COMPASS LOOP',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['swieser@bismarcknd.gov', 'Scott', 'Wieser'],
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should filter out duplicate inspections`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
    it(`should add to the tally of that persons email if an object with their key already exists`, async () => {
      const mockData = [
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-10-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
        {
          inspectionId: 55905,
          siteId: 19219,
          inspectionDate: null,
          scheduledInspectionDate: '2019-11-01T05:00:00.000Z',
          inspectorId: 214,
          inspectorId2: null,
          name: 'Veterans Pond',
          customerId: 'IA_DUBUQUE',
          email: [
            'dmattoon@cityofdubuque.org',
            null,
            'tgarmon@ljaengineering.com',
          ],
          givenName: ['Dean', null, 'Ty'],
          surname: ['Mattoon', null, 'Garmon'],
          userId: 7,
          role: 'super',
          dailyInspectorEmail: true,
          mondayInspectorEmail: true,
          mondayAdminEmail: true,
        },
      ];
      const expectedResult = {
        // different dates
        'dmattoon@cityofdubuque.orgINSP': {
          info: {
            givenName: 'Dean',
            surname: 'Mattoon',
            email: 'dmattoon@cityofdubuque.org',
            role: 'INSP',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
            '55905192192019-11-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '11/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
            },
          },
        },
        'tgarmon@ljaengineering.comADMIN': {
          info: {
            givenName: 'Ty',
            surname: 'Garmon',
            email: 'tgarmon@ljaengineering.com',
            role: 'ADMIN',
          },
          inspections: {
            '55905192192019-10-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '10/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
            '55905192192019-11-01T05:00:00.000Z': {
              customerId: 'IA_DUBUQUE',
              inspectionId: 55905,
              siteId: 19219,
              scheduledInspectionDate: '11/01/2019',
              name: 'Veterans Pond',
              site: 'Construction',
              type: 'upcoming',
              inspector: ['dmattoon@cityofdubuque.org', 'Dean', 'Mattoon'],
            },
          },
        },
      };
      const inspections = cjRepo.organizeInspections(
        mockData,
        {},
        'Construction',
        'upcoming',
        true
      );
      chai.expect(inspections).to.be.deep.equal(expectedResult);
    });
  });
});
