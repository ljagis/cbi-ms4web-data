import { CustomerLookupRepository } from './customer-lookup.repository';
import { StructureControlType } from '../models';
import { injectable } from 'inversify';

@injectable()
export class StructureControlTypeRepository extends CustomerLookupRepository<
  StructureControlType
> {
  constructor() {
    super(StructureControlType);
  }
}
