import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { ContactRepository } from './contact.repository';

describe('ContactRepository Tests #ContactRepository', () => {
  const repo = container.get(ContactRepository);
  const customerId = 'LEWISVILLE';

  describe(`exportEntityContacts tests #ContactRepository`, () => {
    it(`should export entity contacts`, async () => {
      const contacts = await repo.exportEntityContacts(customerId);
      const contact = contacts[0];

      chai.expect(contacts).to.be.instanceOf(Array);
      chai.expect(contacts).to.have.length.greaterThan(0);

      chai.expect(contact).to.have.property('contactId');
      chai.expect(contact).to.have.property('entityType');
      chai.expect(contact).to.have.property('constructionSiteId');
      chai.expect(contact).to.have.property('structureId');
      chai.expect(contact).to.have.property('contactType');
      chai.expect(contact).to.have.property('name');
      chai.expect(contact).to.have.property('company');
      chai.expect(contact).to.have.property('email');
      chai.expect(contact).to.have.property('phone');
      chai.expect(contact).to.have.property('mobilePhone');
      chai.expect(contact).to.have.property('fax');
      chai.expect(contact).to.have.property('mailingAddressLine1');
      chai.expect(contact).to.have.property('mailingAddressLine2');
      chai.expect(contact).to.have.property('mailingCity');
      chai.expect(contact).to.have.property('mailingState');
      chai.expect(contact).to.have.property('mailingZip');
    });
  });
});
