import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { StructureRepository } from './structure.repository';
import * as Knex from 'knex';

describe('Structure Repository Tests', () => {
  const repo = container.get(StructureRepository);
  const knex: Knex = container.get(Knex);
  const customerId = 'LEWISVILLE';

  describe(`exportData tests #StructureRepository`, () => {
    it(`should export data`, async () => {
      const structures = await repo.exportData(customerId);

      const structure = structures[0];

      chai.expect(structures).to.be.instanceOf(Array);
      chai.expect(structures).to.have.length.greaterThan(0);

      // TODO: test for properties exported.
      chai.expect(structure).to.have.property('id');
      chai.expect(structure).to.have.property('name');
      chai.expect(structure).to.have.property('inspectionFrequencyName');
    });
  });

  describe(`exportCustomFields #StructureRepository`, () => {
    it(`should get custom field values`, async () => {
      const cfvs = await repo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('structureId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });

  describe(`fetch by contractorEmail`, async () => {
    it(`should fetch by contractorEmail`, async () => {
      const expectedSql = `
      SELECT
  count (*) as count
FROM
  Facilities cs INNER JOIN EntityContacts ec ON cs.GlobalId = ec.GlobalIdFk INNER JOIN Contacts c ON c.Id = ec.ContactId
WHERE
  cs.IsDeleted=0
  AND
  c.IsDeleted=0
  AND
  c.Email = 'alexng99+contractor@gmail.com'`;
      const expected: any[] = await knex.raw(expectedSql);

      const contractorEmail = 'alexng99+contractor@gmail.com';
      const structures = await repo.fetch(customerId, { contractorEmail });
      chai.expect(structures).length.greaterThan(0);
      chai.expect(structures.length).eq(expected[0].count);
    });
  });
});
