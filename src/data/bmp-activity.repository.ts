import { Repository } from './repository';
import {
  QueryOptions,
  BmpActivity,
  FileEntity,
  BmpControlMeasure,
  BmpTask,
} from '../models';
import { injectable } from 'inversify';
import { getTableName, getSqlField, getSqlFields } from '../decorators';
import { EntityTypes } from '../shared';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

@injectable()
export class BmpActivityRepository extends Repository<BmpActivity> {
  constructor() {
    super(BmpActivity);
  }

  fetch(
    customer: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpActivity[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSql, queryOptions.orderByDirection);
    }

    queryBuilder
      .where(BmpActivity.getSqlField<BmpActivity>(a => a.customerId), customer)
      .where(BmpActivity.getSqlField<BmpActivity>(a => a.isDeleted), 0);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map<BmpActivity, BmpActivity>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  fetchById(
    id: number,
    customer: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpActivity> {
    let queryBuilder = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id)
      .where(BmpActivity.getSqlField<BmpActivity>(a => a.customerId), customer)
      .where(BmpActivity.getSqlField<BmpActivity>(a => a.isDeleted), 0);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder
      .map<BmpActivity, BmpActivity>(l => this.mapObjectToEntityClass(l))
      .get<BmpActivity>(0);
  }

  insert(bmp: BmpActivity, customerId: string): Bluebird<BmpActivity> {
    bmp.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(bmp);
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return queryBuilder
      .map(e => this.mapObjectToEntityClass(e))
      .get<BmpActivity>(0);
  }

  update(id: number, bmp: BmpActivity, customerId: string): Bluebird<void> {
    const data = this.mapObjectToSql(bmp);
    const qb = this.knex(this.tableName)
      .update(data)
      .where(getSqlField<BmpActivity>(this.entity, c => c.id), id)
      .where(
        getSqlField<BmpActivity>(this.entity, c => c.customerId),
        customerId
      );
    return qb;
  }

  del(id: number, customerId: string): Bluebird<any> {
    let trans = this.knex.transaction(trx => {
      // del related files
      let getGlobalIdQb = this.knex(this.tableName)
        .where(this.getSqlField(b => b.id), id)
        .where(this.getSqlField(b => b.customerId), customerId)
        .select(this.getSqlField(b => b.globalId));
      let delRelatedFilesQb = this.knex(getTableName(FileEntity))
        .update(getSqlField<FileEntity>(FileEntity, f => f.isDeleted), 1)
        .where(
          getSqlField<FileEntity>(FileEntity, f => f.entityType),
          EntityTypes.BmpActivity
        )
        .whereIn(
          getSqlField<FileEntity>(FileEntity, f => f.globalIdFk),
          getGlobalIdQb
        );

      // del bmp
      let delBmpQb = this.knex(this.tableName)
        .update(this.getSqlField(b => b.isDeleted), 1)
        .where(this.getSqlField(c => c.id), id)
        .where(this.getSqlField(c => c.customerId), customerId);

      return delRelatedFilesQb
        .transacting(trx)
        .return(delBmpQb.transacting(trx));
    });

    return trans;
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${BmpControlMeasure.sqlField(c => c.id, true)} as controlMeasureId`,
      `${BmpControlMeasure.sqlField(c => c.name, true)} as controlMeasureName`,
      `${BmpTask.sqlField(c => c.id, true)} as taskId`,
      `${BmpTask.sqlField(c => c.name, true)} as taskName`,

      BmpActivity.sqlField(a => a.id, true, true),
      BmpActivity.sqlField(a => a.dateAdded, true, true),
      BmpActivity.sqlField(a => a.completedDate, true, true),
      BmpActivity.sqlField(a => a.dueDate, true, true),
      BmpActivity.sqlField(a => a.comments, true, true),
      BmpActivity.sqlField(a => a.description, true, true),
      BmpActivity.sqlField(a => a.isCompleted, true, true),
      BmpActivity.sqlField(a => a.title, true, true),
    ];
    return await this.knex(BmpActivity.getTableName())
      .leftOuterJoin(
        BmpTask.getTableName(),
        BmpTask.sqlField(p => p.id, true),
        BmpActivity.sqlField(c => c.taskId, true)
      )
      .leftOuterJoin(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(p => p.id, true),
        BmpTask.sqlField(c => c.controlMeasureId, true)
      )
      .where(BmpActivity.sqlField(c => c.customerId, true), customerId)
      .where(BmpActivity.sqlField(c => c.isDeleted, true), 0)
      .where(BmpTask.sqlField(c => c.customerId, true), customerId)
      .where(BmpTask.sqlField(c => c.isDeleted, true), 0)
      .where(BmpControlMeasure.sqlField(c => c.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }
}
