import { injectable, inject } from 'inversify';
import * as Knex from 'knex';
import {
  BmpDataType,
  Creatable,
  Updateable,
  CrudDeletable,
  DeleteOptions,
} from '../models';
import { getSqlFields2 } from '../decorators';
import { objectToSqlData } from '../shared/object-to-sql';
import * as Bluebird from 'bluebird';
import { fetchEntities } from './shared';

interface BmpDataTypeQueryOptions {
  name?: string;
}

@injectable()
export class BmpDataTypeRepository
  implements Creatable<BmpDataType>, Updateable<BmpDataType>, CrudDeletable {
  constructor(@inject(Knex) protected knex: Knex) {}

  async insert(
    customerId: string,
    dataType: Partial<BmpDataType>
  ): Bluebird<BmpDataType> {
    dataType.customerId = customerId; // ensure customer gets populated
    const data = objectToSqlData(BmpDataType, dataType, this.knex);
    const returnFields = getSqlFields2(BmpDataType, { asProperty: true });
    const qb = this.knex(BmpDataType.getTableName())
      .insert(data)
      .returning(returnFields)
      .get<BmpDataType>(0);
    return qb;
  }

  async fetch(
    customerId: string,
    queryOptions: BmpDataTypeQueryOptions = {}
  ): Bluebird<BmpDataType[]> {
    const qb = fetchEntities(BmpDataType, this.knex)
      .where(BmpDataType.sqlField(d => d.customerId), customerId)
      .where(BmpDataType.sqlField(d => d.isDeleted), 0);

    if (queryOptions.name) {
      qb.where(BmpDataType.sqlField(f => f.name, true), queryOptions.name);
    }
    return qb;
  }

  async fetchById(customerId: string, id: number): Bluebird<BmpDataType> {
    return fetchEntities(BmpDataType, this.knex)
      .where(BmpDataType.sqlField(d => d.id), id)
      .where(BmpDataType.sqlField(d => d.customerId), customerId)
      .where(BmpDataType.sqlField(d => d.isDeleted), 0)
      .get<BmpDataType>(0);
  }

  async update(
    customerId: string,
    id: number,
    dataType: Partial<BmpDataType>
  ): Bluebird<BmpDataType> {
    const data = objectToSqlData(BmpDataType, dataType, this.knex);
    const returnFields = getSqlFields2(BmpDataType, { asProperty: true });
    const qb = this.knex(BmpDataType.getTableName())
      .update(data)
      .returning(returnFields)
      .where(BmpDataType.sqlField(d => d.customerId), customerId)
      .where(BmpDataType.sqlField(d => d.id), id);
    return qb;
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<number> {
    const qb = this.knex(BmpDataType.getTableName())
      .where(BmpDataType.sqlField(f => f.customerId), customerId)
      .where(BmpDataType.sqlField(f => f.id), id);
    if (options && options.force) {
      qb.del();
    } else {
      qb.update(BmpDataType.sqlField(f => f.isDeleted), 1);
    }
    return qb;
  }
}
