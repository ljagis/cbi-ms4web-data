import 'mocha';
import { expect } from 'chai';

import { container } from '../inversify.config';
import { InspectionTypeRepository } from './inspection-type.repository';

import { config } from '../test/helper';
import { InspectionType } from '../models/inspection-type';

describe(`Inspection Type Repository tests`, async () => {
  let inspectionType: InspectionType;
  const time = new Date().getTime();
  const repo = container.get(InspectionTypeRepository);

  it(`should insert inspection type`, async () => {
    const expectedInspectionType = {
      name: 'Test inspection type' + time,
    } as InspectionType;
    inspectionType = await repo.insert(
      config.customerId,
      expectedInspectionType
    );

    expect(inspectionType.id).greaterThan(0);
    expect(inspectionType.name).eq(expectedInspectionType.name);
  });

  it(`should get inspection types`, async () => {
    const its = await repo.fetch(config.customerId);

    const it = its.find(i => i.name === 'Test inspection type' + time);

    expect(its).length.greaterThan(0);
    expect(it).ok;
  });

  it(`should update`, async () => {
    const expectedInspectionType = {
      name: 'Test inspection type Updated' + time,
    } as InspectionType;

    const actual = await repo.update(
      config.customerId,
      inspectionType.id,
      expectedInspectionType
    );

    expect(actual.name).eq(expectedInspectionType.name);
  });

  after(async () => {
    if (inspectionType && inspectionType.id) {
      await repo.delete(config.customerId, inspectionType.id, { force: true });
    }
  });
});
