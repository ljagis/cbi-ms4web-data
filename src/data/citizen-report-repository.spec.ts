import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { CitizenReportRepository } from './citizen-report.repository';

describe('Citizen Report Repository Tests', () => {
  const repo = container.get(CitizenReportRepository);

  describe(`exportData tests #CitizenReportRepository`, () => {
    it(`should export data`, async () => {
      const customerId = 'LEWISVILLE';
      const crs = await repo.exportData(customerId);

      const cr = crs[0];

      chai.expect(crs).to.be.instanceOf(Array);
      chai.expect(crs).to.have.length.greaterThan(0);

      chai.expect(cr).to.have.property('id');
      chai.expect(cr).to.have.property('dateAdded');

      chai.expect(cr).to.have.property('citizenName');
      chai.expect(cr).to.have.property('dateReported');
      chai.expect(cr).to.have.property('complianceStatus');
      chai.expect(cr).to.have.property('lat');
      chai.expect(cr).to.have.property('lng');

      chai.expect(cr).to.have.property('physicalAddress1');
      chai.expect(cr).to.have.property('physicalAddress2');
      chai.expect(cr).to.have.property('physicalCity');
      chai.expect(cr).to.have.property('physicalState');
      chai.expect(cr).to.have.property('physicalZip');

      chai.expect(cr).to.have.property('investigatorFirstName');
      chai.expect(cr).to.have.property('investigatorLastName');

      chai.expect(cr).to.have.property('communityName');
      chai.expect(cr).to.have.property('watershedName');
      chai.expect(cr).to.have.property('subWatershedName');
      chai.expect(cr).to.have.property('receivingWaterName');
    });
  });

  // TODO: test
  describe.skip(`exportCustomFields #CitizenReportRepository`, () => {
    it(`should get custom field values`, async () => {
      const cfvs = await repo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('citizenReportId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });
});
