import 'reflect-metadata';

import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  CrudDeletable,
  CustomField,
  CustomFormTemplate,
  CustomFormTemplateWithCustomFields,
  DeleteOptions,
  InspectionType,
} from '../models';
import { inject, injectable } from 'inversify';

import { CustomFieldRepository } from './custom-field.repository';
import { EntityTypes } from '../shared/entity-types';
import { Repository } from './repository';
import { getSqlFields2 } from '../decorators/decorator-helpers';
import { objectToSqlData } from '../shared/object-to-sql';
import { validateModel } from '../shared/validate';

const batchInsertChunkSize = 100;

@injectable()
export class CustomFormTemplateRepository extends Repository<CustomFormTemplate>
  implements CrudDeletable {
  constructor(
    @inject(CustomFieldRepository) private _cfRepo: CustomFieldRepository
  ) {
    super(CustomFormTemplate);
  }

  async fetch(customerId: string): Bluebird<CustomFormTemplate[]> {
    return await this.knex(this.tableName)
      .select(this.defaultSelect)
      .where({
        [CustomFormTemplate.sqlField(t => t.customerId)]: customerId,
        [CustomFormTemplate.sqlField(t => t.isDeleted)]: 0,
        [CustomFormTemplate.sqlField(t => t.isArchived)]: 0,
      });
  }

  async fetchById(
    customerId: string,
    id: number
  ): Bluebird<CustomFormTemplateWithCustomFields> {
    const cft = await this.knex(this.tableName)
      .select(this.defaultSelect)
      .where({
        [CustomFormTemplate.sqlField(t => t.customerId)]: customerId,
        [CustomFormTemplate.sqlField(t => t.isDeleted)]: 0,
        [CustomFormTemplate.sqlField(t => t.id)]: id,
      })
      .get<CustomFormTemplateWithCustomFields>(0);

    const customFields: CustomField[] = await this.knex(
      CustomField.getTableName()
    )
      .select(getSqlFields2(CustomField, { asProperty: true }))
      .where({
        [CustomField.sqlField(f => f.isDeleted)]: 0,
        [CustomField.sqlField(f => f.customerId)]: customerId,
        [CustomField.sqlField(f => f.customFormTemplateId)]: cft.id,
      })
      .orderBy(CustomField.sqlField(f => f.fieldOrder));

    cft.customFields = customFields;

    return cft;
  }

  async insert(
    cfTemplate: Partial<CustomFormTemplate>,
    customerId: string
  ): Bluebird<CustomFormTemplate> {
    return await this._insertTemplate(cfTemplate, customerId)
      .map(e => this.mapObjectToEntityClass(e))
      .get<CustomFormTemplate>(0);
  }

  async insertWithCustomFields(
    cfTemplate: Partial<CustomFormTemplateWithCustomFields>,
    customerId: string
  ): Bluebird<CustomFormTemplateWithCustomFields> {
    let insertedCft: CustomFormTemplateWithCustomFields = await this.knex.transaction(
      async trx => {
        insertedCft = await this._insertTemplate(cfTemplate, customerId)
          .transacting(trx)
          .get<CustomFormTemplateWithCustomFields>(0);
        insertedCft.customFields = [];

        // check customFields
        if (cfTemplate.customFields && cfTemplate.customFields.length) {
          await Bluebird.each(cfTemplate.customFields, async cf => {
            cf.customFormTemplateId = insertedCft.id;
            await this.validate(
              Object.assign<CustomField, any>(new CustomField(), cf)
            );
            const insertCf = await this._cfRepo
              .insertQueryBuilder(cf, customerId)
              .transacting(trx)
              .get<CustomField>(0);
            insertedCft.customFields.push(insertCf);
          });
        }
        return insertedCft;
      }
    );

    return insertedCft;
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<void> {
    const isForceDel = options && options.force;
    await this.knex.transaction(async trx => {
      await this._deleteFormTemplateCustomFields(
        customerId,
        id,
        isForceDel
      ).transacting(trx);
      await this._deleteCustomFieldTemplate(
        customerId,
        id,
        isForceDel
      ).transacting(trx);
    });
  }

  async archive(customerId: string, id: number): Bluebird<void> {
    await this.knex(this.tableName)
      .where(this.getSqlField(cft => cft.id), id)
      .where(this.getSqlField(cft => cft.customerId), customerId)
      .update(this.getSqlField(cft => cft.isArchived), 1);
  }

  async getCount(customerId: string, entityType: string) {
    const count = await this.knex(this.tableName)
      .where({
        [this.getSqlField(f => f.isDeleted, true)]: 0,
        [this.getSqlField(f => f.isArchived, true)]: 0,
        [this.getSqlField(f => f.customerId, true)]: customerId,
        [this.getSqlField(f => f.entityType, true)]: entityType,
      })
      .count()
      .get(0) // gets first item from rows
      .get<number>(''); // gets '' property
    return count;
  }

  /**
   * Creates default custom form template for each inspection / investigation
   *
   * @param {string} customerId
   */
  async createDefaultFormTemplates(
    customerId: string,
    trans?: Knex.Transaction
  ) {
    const createForms = async (trx: Knex.Transaction) => {
      await this._createDefaultCSInspectionFormTemplate(customerId, trx);
      await this._createDefaultFacilityInspectionFormTemplate(customerId, trx);
      await this._createDefaultOutfallInspectionFormTemplate(customerId, trx);
      await this._createDefaultStructureInspectionFormTemplate(customerId, trx);
      await this._createDefaultCitizenReportFormTemplate(customerId, trx);
      await this._createDefaultIllicitDischargeFormTemplate(customerId, trx);
      await this._createFollowUpCorrectiveActionForm(customerId, trx);
    };

    if (trans) {
      await createForms(trans);
    } else {
      await this.knex.transaction(async trx => createForms(trx));
    }
  }

  private _insertTemplate(
    cfTemplate: Partial<CustomFormTemplateWithCustomFields>,
    customerId: string
  ) {
    cfTemplate.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(cfTemplate);
    const returnFields = this.getSelectFields();
    return this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
  }

  private _deleteFormTemplateCustomFields(
    customerId: string,
    customFieldTemplateId: number,
    isForceDel = false
  ) {
    const deleteCustomFieldsQb = this.knex(CustomField.getTableName()).where({
      [CustomField.sqlField(f => f.customerId)]: customerId,
      [CustomField.sqlField(
        f => f.customFormTemplateId
      )]: customFieldTemplateId,
    });
    if (isForceDel) {
      deleteCustomFieldsQb.del();
    } else {
      deleteCustomFieldsQb.update(CustomField.sqlField(f => f.isDeleted), 1);
    }
    return deleteCustomFieldsQb;
  }

  private _deleteCustomFieldTemplate(
    customerId: string,
    cftId: number,
    isForceDel = false
  ) {
    const delCft = this.knex(CustomFormTemplate.getTableName()).where({
      [CustomFormTemplate.sqlField(f => f.customerId)]: customerId,
      [CustomFormTemplate.sqlField(f => f.id)]: cftId,
    });
    if (isForceDel) {
      delCft.del();
    } else {
      delCft.update(CustomFormTemplate.sqlField(f => f.isDeleted), 1);
    }
    return delCft;
  }

  private async _createDefaultCSInspectionFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.ConstructionSiteInspection;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Construction Site Inspection',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Construction Site Inspection`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Date Resolved',
        dataType: 'Date',
        inputType: 'date',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Site Active',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Site Permitted',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Erosion Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Local Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Has Plan On Site',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Non-stormwater Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Outfall Velocity Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Stabilization Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Structural Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Tracking Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Maintenance of Controls',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Acceptable Waste Management',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Has Current Plan Records',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  private async _createDefaultFacilityInspectionFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.FacilityInspection;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Facility Inspection',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Facility Inspection`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Date Resolved',
        dataType: 'Date',
        inputType: 'date',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Facility Active',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Facility Permitted',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Erosion Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Downstream Erosion Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Floatables Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Illicit Discharges Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Non-Stormwater Disharges Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Washouts Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Records Current',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Sampling Data Evaluation Acceptable',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is BMP Acceptable',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Good House Keeping',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Return Inspection Required',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is SWPP On Site',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  private async _createDefaultOutfallInspectionFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.OutfallInspection;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Outfall Inspection',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Outfall Inspection`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Dry or Wet Weather',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Dry|Wet',
        defaultValue: 'Dry',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Days Since Last Rain',
        dataType: 'number',
        inputType: 'text',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Color',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Clear|Light Gray|Brown|Tan|Dark Grey',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Clarity',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Clear|Transparent|Murky|Opaque',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Odor',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Sewer|Sulfur|Chemical|Rotten Eggs|Unknown',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Foam',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Light|Medium|Heavy',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Sheen',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Light|Medium|Heavy',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Suspended Solids',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Light|Medium|Heavy',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Settled Solids',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Light|Medium|Heavy',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Floating Solids',
        dataType: 'string',
        inputType: 'select',
        fieldOptions: 'Light|Medium|Heavy',
        sectionName: '1. Visual Observations',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'PH',
        dataType: 'string',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Temperature(F)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'DO (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Turbidity (NTU)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Cond (mOhms)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'DO (%Sat)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Flowrate (GPM)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Copper (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Phenols (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Ammonia (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Detergents (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'T.PO4 (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Cl2 (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'BOD (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'COD (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'TSS (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'NO3 (mg/L)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Fecal Coliform (col/100mL)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'E. Coli (col/100mL)',
        dataType: 'number',
        inputType: 'text',
        sectionName: '2. Test Results',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Discharge Description',
        dataType: 'string',
        inputType: 'textarea',
        sectionName: '3. Discharge Description',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  private async _createDefaultStructureInspectionFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.StructureInspection;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Structure Inspection',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Structure Inspection`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Date Resolved',
        dataType: 'Date',
        inputType: 'date',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Control Active',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Are Washouts Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Removal of Floatables Required',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Built within Specification',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Depth of Sediment Acceptable',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'true',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Downstream Erosion Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Erosion Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Illict Discharge Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Outlet Clogged',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Return Inspection Recommended',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Is Standing Water Present',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Has Structural Damage',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Requires Maintenance',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Requires Repairs',
        dataType: 'boolean',
        inputType: 'checkbox',
        defaultValue: 'false',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  private async _createDefaultIllicitDischargeFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.IllicitDischarge;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Illicit Discharge',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Illicit Discharge`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Date Eliminated',
        dataType: 'Date',
        inputType: 'date',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Phone Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Cell Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Fax Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Conversation',
        dataType: 'string',
        inputType: 'textarea',
        sectionName: '2. Conversation',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Corrective Action',
        dataType: 'string',
        inputType: 'textarea',
        sectionName: '3. Corrective Action',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  private async _createDefaultCitizenReportFormTemplate(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;
    const entityType = EntityTypes.CitizenReport;
    const cft = {
      entityType: entityType,
      customerId: customerId,
      isSystem: true,
      name: 'Default Citizen Report',
    } as Partial<CustomFormTemplate>;

    const cftId = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, cft, this.knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));

    const inspectionType = {
      customerId: customerId,
      name: `Default Citizen Report`,
      customFormTemplateId: cftId,
      entityType: cft.entityType,
    } as Partial<InspectionType>;

    await this.knex(InspectionType.tableName)
      .insert(objectToSqlData(InspectionType, inspectionType, this.knex))
      .transacting(trx)
      .returning(InspectionType.sqlField(f => f.id));
    let fieldOrder = 0;
    const rowsToInsert = [
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Phone Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Cell Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Fax Number',
        dataType: 'string',
        inputType: 'text',
        sectionName: '1. Contact Information',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),

      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Response Date',
        dataType: 'Date',
        inputType: 'date',
        sectionName: '2. Response',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
      await this._createCustomFieldSqlData(knex, {
        fieldLabel: 'Response',
        dataType: 'string',
        inputType: 'textarea',
        sectionName: '2. Response',
        fieldOrder: ++fieldOrder,
        customerId: customerId,
        customFormTemplateId: cftId,
        entityType: entityType,
      }),
    ];

    await knex
      .batchInsert(CustomField.tableName, rowsToInsert, batchInsertChunkSize)
      .transacting(trx);
  }

  /**
   * creates a 'Follow Up / Corrective Action Form' to CS, Facility, Structure inspections
   * https://ljagis.atlassian.net/browse/MS4-489
   * @param customerId
   * @param trx
   */
  private async _createFollowUpCorrectiveActionForm(
    customerId: string,
    trx: Knex.Transaction
  ) {
    const knex = this.knex;

    // Construction Site inspections
    const csForm = createFormForAsset(EntityTypes.ConstructionSiteInspection);
    const [csFormId] = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, csForm, knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));
    const csFieldsData = createFields(
      csFormId,
      EntityTypes.ConstructionSiteInspection
    ).map(field => objectToSqlData(CustomField, field, knex));

    // Facility inspections
    const facForm = createFormForAsset(EntityTypes.FacilityInspection);
    const [facFormId] = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, facForm, knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));
    const facFieldsData = createFields(
      facFormId,
      EntityTypes.FacilityInspection
    ).map(field => objectToSqlData(CustomField, field, knex));

    // Structure inspections
    const structureForm = createFormForAsset(EntityTypes.StructureInspection);
    const [structureFormId] = await this.knex(CustomFormTemplate.tableName)
      .transacting(trx)
      .insert(objectToSqlData(CustomFormTemplate, structureForm, knex))
      .returning(CustomFormTemplate.sqlField(f => f.id));
    const structureFieldsData = createFields(
      structureFormId,
      EntityTypes.StructureInspection
    ).map(field => objectToSqlData(CustomField, field, knex));

    await knex
      .batchInsert(
        CustomField.tableName,
        [...csFieldsData, ...facFieldsData, ...structureFieldsData],
        batchInsertChunkSize
      )
      .transacting(trx);

    function createFormForAsset(entityType: string) {
      return {
        entityType,
        customerId: customerId,
        name: 'Sample Follow Up / Corrective Action Form',
      } as CustomFormTemplate;
    }

    function createFields(
      customFormTemplateId: number,
      entityType: string
    ): CustomField[] {
      let fieldOrder = 0;
      return [
        {
          fieldLabel:
            'Have the deficiencies from the previous inspection been corrected?',
          dataType: 'boolean',
          inputType: 'checkbox',
          sectionName: null,
          sectionSubtitle: null,
          fieldOrder: ++fieldOrder,
          customerId: customerId,
          customFormTemplateId,
          entityType,
          customFieldType: 'Customer',
          maxLengthEnabled: 0,
          maxLength: 150,
        },
        {
          fieldLabel: 'If no, explain:',
          dataType: 'string',
          inputType: 'textarea',
          sectionName: null,
          sectionSubtitle: null,
          fieldOrder: ++fieldOrder,
          customerId: customerId,
          customFormTemplateId,
          entityType,
          customFieldType: 'Customer',
          maxLengthEnabled: 0,
          maxLength: 150,
        },
        {
          fieldLabel:
            // tslint:disable-next-line:max-line-length
            'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.',
          dataType: 'section',
          inputType: 'section',
          sectionName:
            // tslint:disable-next-line:max-line-length
            'If all deficiencies have been corrected start your next inspection.  If not schedule your next follow up inspection and follow your enforcement escalation procedures.',
          sectionSubtitle: null,
          fieldOrder: ++fieldOrder,
          customerId: customerId,
          customFormTemplateId,
          entityType,
          customFieldType: 'Customer',
          maxLengthEnabled: 0,
          maxLength: 150,
        },
      ];
    }
  }

  private async _createCustomFieldSqlData(
    knex: Knex,
    cf: Partial<CustomField>
  ) {
    const customField = Object.assign(new CustomField(), cf);
    await validateModel(customField);
    return objectToSqlData(CustomField, customField, knex);
  }
}
