import * as Bluebird from 'bluebird';
import * as moment from 'moment';

import {
  CustomField,
  CustomFieldValue,
  CustomFormTemplate,
  Facility,
  FacilityInspection,
  InspectionType,
  User,
} from './../models';

import { AssetInspectionRepository } from './asset-inspection.repository';
import { EntityTypes } from '../shared/entity-types';
import { injectable } from 'inversify';

@injectable()
export class FacilityInspectionRepository extends AssetInspectionRepository<
  Facility,
  FacilityInspection
> {
  constructor() {
    super(FacilityInspection);
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      FacilityInspection.sqlField(c => c.id, true, true),
      FacilityInspection.sqlField(c => c.createdDate, true, true),
      FacilityInspection.sqlField(c => c.facilityId, true, true),
      `${Facility.sqlField(c => c.name, true)} as facilityName`,
      `${Facility.sqlField(c => c.trackingId, true)} as facilityTrackingId`,
      FacilityInspection.sqlField(c => c.inspectionDate, true, true),
      FacilityInspection.sqlField(c => c.scheduledInspectionDate, true, true),
      FacilityInspection.sqlField(c => c.complianceStatus, true, true),
      FacilityInspection.sqlField(c => c.inspectorId, true, true),
      `inspector.${User.sqlField(u => u.givenName)} as inspectorFirstName`,
      `inspector.${User.sqlField(u => u.surname)} as inspectorLastName`,
      FacilityInspection.sqlField(c => c.followUpInspectionId, true, true),
      FacilityInspection.sqlField(c => c.additionalInformation, true, true),

      // weather
      FacilityInspection.sqlField(c => c.weatherCondition, true, true),
      FacilityInspection.sqlField(c => c.weatherTemperatureF, true, true),
      FacilityInspection.sqlField(c => c.weatherPrecipitationIn, true, true),
      FacilityInspection.sqlField(c => c.weatherLast72, true, true),
      FacilityInspection.sqlField(c => c.weatherLast24, true, true),
      FacilityInspection.sqlField(c => c.windDirection, true, true),
      FacilityInspection.sqlField(c => c.windSpeedMph, true, true),
      FacilityInspection.sqlField(c => c.weatherStationId, true, true),
      FacilityInspection.sqlField(c => c.weatherDateTime, true, true),
      FacilityInspection.sqlField(c => c.timeIn, true, true),
      FacilityInspection.sqlField(c => c.timeOut, true, true),

      FacilityInspection.sqlField(c => c.customFormTemplateId, true, true),
      FacilityInspection.sqlField(c => c.inspectionTypeId, true, true),
      `${InspectionType.sqlField(i => i.name, true)} as inspectionType`,
    ];
    return await this.fetchBaseQueryBuilder(customerId)
      .leftOuterJoin(
        `${User.getTableName()} as inspector`,
        `inspector.${User.sqlField(u => u.id)}`,
        FacilityInspection.sqlField(i => i.inspectorId, true)
      )
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${FacilityInspection.sqlField(c => c.id, true)} as facilityInspectionId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
      FacilityInspection.sqlField(c => c.customFormTemplateId, true, true),
      CustomFormTemplate.sqlField(ct => ct.name, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        FacilityInspection.getTableName(),
        FacilityInspection.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        Facility.getTableName(),
        Facility.sqlField(cs => cs.id, true),
        FacilityInspection.sqlField(i => i.facilityId, true)
      )
      .join(
        CustomFormTemplate.getTableName(),
        CustomFormTemplate.sqlField(ct => ct.id, true),
        FacilityInspection.sqlField(i => i.customFormTemplateId, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.FacilityInspection
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(FacilityInspection.sqlField(f => f.isDeleted, true), 0)
      .where(Facility.sqlField(f => f.isDeleted, true), 0)
      .where(Facility.sqlField(f => f.customerId, true), customerId)
      .orderBy(FacilityInspection.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true))
      .timeout(60000);
    return await qb;
  }

  /*
    Table structure for pastDueInspectionsReport and upcomingInspectionsReport.
    Lines denote how tables are joined, *'s are selected keys.
<<<<<<< HEAD
=======

>>>>>>> feat: SWMP, BMP, MG APIs
          FacilityInspections           Facilities     Users     UserCustomers   Customers
                *Id                      *Name
          *ConstructionId ---------------- Id      _-------------------------------- Id
        ScheduledInspectionDate        *CustomerId --------------- CustomerId     *DailyInspectorEmail
            *InspectionDate                             Id ------- *UserId       *MondayInspectorEmail
                                                      *Email        *Role         *MondayAdminEmail
                                                      *GivenName
                                                      *Surname
            *InspectorId ------------------------------ Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
            *InspectorId2 ----------------------------- Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
      */

  async pastDueInspectionsReport() {
    const start = '2019-9-22';
    const end = moment()
      .utc()
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'FacilityInspections.Id as inspectionId',
        'FacilityInspections.FacilityId as siteId',
        'FacilityInspections.InspectionDate as inspectionDate',
        'FacilityInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'FacilityInspections.InspectorId as inspectorId',
        'FacilityInspections.InspectorId2 as inspectorId2',
        'Facilities.Name as name',
        'Facilities.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('FacilityInspections.IsDeleted', '=', 'false')
      .where('Facilities.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Facilities', {
        'FacilityInspections.FacilityId': 'Facilities.Id',
      })
      .leftJoin('Users', { 'FacilityInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'FacilityInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Facilities.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Facilities.CustomerId': 'Customers.Id' });
  }

  async upcomingInspectionsReport() {
    const start = moment()
      .utc()
      .format('YYYY-MM-DD');
    const end = moment()
      .utc()
      .add(7, 'days')
      .format('YYYY-MM-DD');
    return await this.knex(this.tableName)
      .select([
        'FacilityInspections.Id as inspectionId',
        'FacilityInspections.FacilityId as siteId',
        'FacilityInspections.InspectionDate as inspectionDate',
        'FacilityInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'FacilityInspections.InspectorId as inspectorId',
        'FacilityInspections.InspectorId2 as inspectorId2',
        'Facilities.Name as name',
        'Facilities.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('FacilityInspections.IsDeleted', '=', 'false')
      .where('Facilities.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Facilities', {
        'FacilityInspections.FacilityId': 'Facilities.Id',
      })
      .leftJoin('Users', { 'FacilityInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'FacilityInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Facilities.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Facilities.CustomerId': 'Customers.Id' });
  }
}
