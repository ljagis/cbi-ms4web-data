import 'mocha';

import * as moment from 'moment';

import { ConstructionSiteInspectionRepository } from './construction-site-inspection.repository';
import { container } from '../inversify.config';
import { expect } from 'chai';

describe.only('Construction Site Inspection Repository Tests #ConstructionSiteInspectionRepository', () => {
  const repo = container.get(ConstructionSiteInspectionRepository);
  const customerId = 'LEWISVILLE';

  describe.skip(`exportData tests #ConstructionSiteInspectionRepository`, () => {
    it(`should export data`, async () => {
      const inspections = await repo.exportData(customerId);

      const inspection = inspections[0];

      expect(inspections).to.be.instanceOf(Array);
      expect(inspections).to.have.length.greaterThan(0);

      // TODO: test for properties exported.
      expect(inspection).to.have.property('constructionSiteId');
      expect(inspection).to.have.property('constructionSiteName');
    });
  });

  describe.skip(`exportCustomFields tests #ConstructionSiteInspectionRepository`, () => {
    // TODO: update tests
    it(`should export data`, async () => {
      const cfvs = await repo.exportCustomFields(customerId);

      const cfv = cfvs[0];

      expect(cfvs).to.be.instanceOf(Array);
      expect(cfvs).to.have.length.greaterThan(0);

      expect(cfv).to.have.property('constructionSiteInspectionId');
      expect(cfv).to.have.property('fieldLabel');
      expect(cfv).to.have.property('value');
    });
  });

  describe(`pastDueInspectionsReport`, () => {
    it(`should have the proper structure`, async () => {
      const inspections = await repo.pastDueInspectionsReport();
      expect(inspections).to.be.instanceOf(Array);
      inspections.forEach(inspection => {
        expect(inspection).to.have.property('inspectionId');
        expect(inspection).to.have.property('siteId');
        expect(inspection).to.have.property('inspectionDate');
        expect(inspection).to.have.property('scheduledInspectionDate');
        expect(inspection).to.have.property('inspectorId');
        expect(inspection).to.have.property('inspectorId2');
        expect(inspection).to.have.property('name');
        expect(inspection).to.have.property('customerId');
        expect(inspection).to.have.property('email');
        expect(inspection).to.have.property('givenName');
        expect(inspection).to.have.property('surname');
        expect(inspection).to.have.property('userId');
        expect(inspection).to.have.property('role');
        expect(inspection).to.have.property('dailyInspectorEmail');
        expect(inspection).to.have.property('mondayInspectorEmail');
        expect(inspection).to.have.property('mondayAdminEmail');

        expect(inspection['inspectionId']).to.be.a('number');
        expect(inspection['siteId']).to.be.a('number');
        expect(inspection['inspectionDate']).to.equal(null);
        expect(inspection['scheduledInspectionDate']).to.be.a('Date');

        expect(inspection['scheduledInspectionDate']).to.satisfy(function(
          scheduledInspectionDate
        ) {
          if (
            moment(scheduledInspectionDate).format('L') < moment().format('L')
          ) {
            return true;
          }
          return false;
        });

        expect(inspection['inspectorId']).to.be.a('number');
        expect(inspection['name']).to.be.a('string');
        expect(inspection['customerId']).to.be.a('string');

        expect(inspection['userId']).to.be.a('number');
        expect(inspection['role']).to.be.a('string');
        expect(inspection['dailyInspectorEmail']).to.be.a('boolean');
        expect(inspection['mondayInspectorEmail']).to.be.a('boolean');
        expect(inspection['mondayAdminEmail']).to.be.a('boolean');

        expect(inspection['email']).to.be.an('array');
        expect(inspection['givenName']).to.be.an('array');
        expect(inspection['surname']).to.be.an('array');

        expect(inspection['email']).to.have.lengthOf(3);
        expect(inspection['givenName']).to.have.lengthOf(3);
        expect(inspection['surname']).to.have.lengthOf(3);

        expect(inspection).to.satisfy(function(insp) {
          if (
            insp['inspectorId2'] === null &&
            insp['email'][1] === null &&
            insp['givenName'][1] === null &&
            insp['surname'][1] === null
          ) {
            return true;
          }
          if (
            typeof insp['inspectorId2'] === 'number' &&
            typeof insp['email'][1] === 'string' &&
            typeof insp['givenName'][1] === 'string' &&
            typeof insp['surname'][1] === 'string'
          ) {
            return true;
          }
          return false;
        });
      });
    });
  });
  describe(`upcomingInspectionsReport`, () => {
    it(`should have the proper structure`, async () => {
      const inspections = await repo.upcomingInspectionsReport();
      expect(inspections).to.be.instanceOf(Array);
      inspections.forEach(inspection => {
        expect(inspection).to.have.property('inspectionId');
        expect(inspection).to.have.property('siteId');
        expect(inspection).to.have.property('inspectionDate');
        expect(inspection).to.have.property('scheduledInspectionDate');
        expect(inspection).to.have.property('inspectorId');
        expect(inspection).to.have.property('inspectorId2');
        expect(inspection).to.have.property('name');
        expect(inspection).to.have.property('customerId');
        expect(inspection).to.have.property('email');
        expect(inspection).to.have.property('givenName');
        expect(inspection).to.have.property('surname');
        expect(inspection).to.have.property('userId');
        expect(inspection).to.have.property('role');
        expect(inspection).to.have.property('dailyInspectorEmail');
        expect(inspection).to.have.property('mondayInspectorEmail');
        expect(inspection).to.have.property('mondayAdminEmail');

        expect(inspection['inspectionId']).to.be.a('number');
        expect(inspection['siteId']).to.be.a('number');
        expect(inspection['inspectionDate']).to.equal(null);
        expect(inspection['scheduledInspectionDate']).to.be.a('Date');

        expect(inspection['scheduledInspectionDate']).to.satisfy(function(
          scheduledInspectionDate
        ) {
          if (
            moment(scheduledInspectionDate).format('L') >= moment().format('L')
          ) {
            return true;
          }
          return false;
        });

        expect(inspection['inspectorId']).to.be.a('number');
        expect(inspection['name']).to.be.a('string');
        expect(inspection['customerId']).to.be.a('string');

        expect(inspection['userId']).to.be.a('number');
        expect(inspection['role']).to.be.a('string');
        expect(inspection['dailyInspectorEmail']).to.be.a('boolean');
        expect(inspection['mondayInspectorEmail']).to.be.a('boolean');
        expect(inspection['mondayAdminEmail']).to.be.a('boolean');

        expect(inspection['email']).to.be.an('array');
        expect(inspection['givenName']).to.be.an('array');
        expect(inspection['surname']).to.be.an('array');

        expect(inspection['email']).to.have.lengthOf(3);
        expect(inspection['givenName']).to.have.lengthOf(3);
        expect(inspection['surname']).to.have.lengthOf(3);

        expect(inspection).to.satisfy(function(insp) {
          if (
            insp['inspectorId2'] === null &&
            insp['email'][1] === null &&
            insp['givenName'][1] === null &&
            insp['surname'][1] === null
          ) {
            return true;
          }
          if (
            typeof insp['inspectorId2'] === 'number' &&
            typeof insp['email'][1] === 'string' &&
            typeof insp['givenName'][1] === 'string' &&
            typeof insp['surname'][1] === 'string'
          ) {
            return true;
          }
          return false;
        });
      });
    });
  });
});
