import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  Community,
  ConstructionSite,
  ConstructionSiteQueryOptions,
  Contact,
  CustomField,
  CustomFieldValue,
  EntityContact,
  Project,
  ReceivingWater,
  Watershed,
  projectStatus,
} from '../models';

import { AssetRepository } from './asset.repository';
import { Defaults } from '../shared';
import { EntityTypes } from '../shared/entity-types';
import { FetchAsset } from '../interfaces';
import { getSqlFields2 } from '../decorators';
import { injectable } from 'inversify';

@injectable()
export class ConstructionSiteRepository extends AssetRepository<
  ConstructionSite
> implements FetchAsset<ConstructionSite> {
  constructor() {
    super(ConstructionSite);
  }

  async fetch(
    customerId: string,
    queryOptions?: ConstructionSiteQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<ConstructionSite[]> {
    return await this.getFetchQueryBuilder(
      customerId,
      queryOptions,
      queryBuilderOverrides
    ).map(e => this.mapObjectToEntityClass(e));
  }

  async fetchById(
    id: number,
    customerId: string,
    queryOptions?: ConstructionSiteQueryOptions
  ): Bluebird<ConstructionSite> {
    const queryBuilder = this.getFetchQueryBuilder(
      customerId,
      queryOptions
    ).where(ConstructionSite.sqlField(f => f.id, true), id);
    return await queryBuilder
      .map<ConstructionSite, ConstructionSite>(asset =>
        this.mapObjectToEntityClass(asset)
      )
      .get<ConstructionSite>(0); // first
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      ConstructionSite.sqlField(c => c.id, true, true),
      ConstructionSite.sqlField(c => c.dateAdded, true, true),
      ConstructionSite.sqlField(c => c.trackingId, true, true),
      ConstructionSite.sqlField(c => c.name, true, true),
      ConstructionSite.sqlField(c => c.physicalAddress1, true, true),
      ConstructionSite.sqlField(c => c.physicalAddress2, true, true),
      ConstructionSite.sqlField(c => c.physicalCity, true, true),
      ConstructionSite.sqlField(c => c.physicalState, true, true),
      ConstructionSite.sqlField(c => c.physicalZip, true, true),
      ConstructionSite.sqlField(c => c.projectId, true, true),
      `${Project.sqlField(p => p.name, true)} as projectName`,
      ConstructionSite.sqlField(c => c.projectType, true, true),
      ConstructionSite.sqlField(c => c.permitStatus, true, true),
      ConstructionSite.sqlField(c => c.projectArea, true, true),
      ConstructionSite.sqlField(c => c.disturbedArea, true, true),
      ConstructionSite.sqlField(c => c.startDate, true, true),
      ConstructionSite.sqlField(c => c.estimatedCompletionDate, true, true),
      ConstructionSite.sqlField(c => c.latestInspectionDate, true, true),
      ConstructionSite.sqlField(c => c.completionDate, true, true),
      ConstructionSite.sqlField(c => c.projectStatus, true, true),
      ConstructionSite.sqlField(c => c.communityId, true, true),
      `${Community.sqlField(c => c.name, true)} as communityName`,
      ConstructionSite.sqlField(c => c.watershedId, true, true),
      `${Watershed.sqlField(c => c.name, true)} as watershedName`,
      ConstructionSite.sqlField(c => c.subwatershedId, true, true),
      `SubWatershed.${Watershed.sqlField(c => c.name)} as subWatershedName`,
      ConstructionSite.sqlField(c => c.receivingWatersId, true, true),
      `${ReceivingWater.sqlField(c => c.name, true)} as receivingWaterName`,
      ConstructionSite.sqlField(c => c.npdes, true, true),
      ConstructionSite.sqlField(c => c.usace404, true, true),
      ConstructionSite.sqlField(c => c.state401, true, true),
      ConstructionSite.sqlField(c => c.usacenwp, true, true),
      ConstructionSite.sqlField(c => c.additionalInformation, true, true),
      ConstructionSite.sqlField(c => c.complianceStatus, true, true),
      ConstructionSite.sqlField(c => c.lat, true, true),
      ConstructionSite.sqlField(c => c.lng, true, true),
    ];
    return await this.knex(this.tableName)
      .leftOuterJoin(
        Project.getTableName(),
        Project.sqlField(p => p.id, true),
        ConstructionSite.sqlField(c => c.projectId, true)
      )
      .leftOuterJoin(
        Community.getTableName(),
        Community.sqlField(p => p.id, true),
        ConstructionSite.sqlField(c => c.communityId, true)
      )
      .leftOuterJoin(
        Watershed.getTableName(),
        Watershed.sqlField(p => p.id, true),
        ConstructionSite.sqlField(c => c.watershedId, true)
      )
      .leftOuterJoin(
        `${Watershed.getTableName()} as SubWatershed`,
        `SubWatershed.${Watershed.sqlField(p => p.id)}`,
        ConstructionSite.sqlField(c => c.subwatershedId, true)
      )
      .leftOuterJoin(
        ReceivingWater.getTableName(),
        ReceivingWater.sqlField(p => p.id, true),
        ConstructionSite.sqlField(c => c.receivingWatersId, true)
      )
      .where(ConstructionSite.sqlField(c => c.customerId, true), customerId)
      .where(ConstructionSite.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${ConstructionSite.sqlField(c => c.id, true)} as constructionSiteId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        ConstructionSite.getTableName(),
        ConstructionSite.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.ConstructionSite
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(ConstructionSite.sqlField(f => f.isDeleted, true), 0)
      .orderBy(ConstructionSite.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true));
    return await qb;
  }

  async createSampleConstructionSite(
    customerId: string
  ): Bluebird<ConstructionSite> {
    const sampleCS: ConstructionSite = {
      ...new ConstructionSite(),
      customerId,
      name: 'Sample Construction Site',
      complianceStatus: 'Compliant',
      lat: 29.757213, // Minute Maid Park
      lng: -95.354992,
    };
    const data = this.objectToSqlData(ConstructionSite, sampleCS, this.knex);
    const returnFields = getSqlFields2(ConstructionSite, { asProperty: true });
    const returnedData = await this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return returnedData;
  }

  /**
   * PRIVATES
   */

  private getFetchQueryBuilder(
    customerId: string,
    queryOptions?: ConstructionSiteQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;

    let queryBuilder = this.knex(this.tableName)
      .select(
        !queryOptions.includeLastInspector
          ? this.defaultSelect
          : [
              ...this.defaultSelect,
              'LastInspection.InspectorId AS inspectorId',
              'LastInspection.InspectorId2 AS inspectorId2',
            ]
      )
      .limit(limit)
      .offset(queryOptions.offset);

    // construction site specific
    if (queryOptions.projectStatus) {
      queryBuilder.where(
        ConstructionSite.sqlField(cs => cs.projectStatus, true),
        projectStatus.active
      );
    }

    if (queryOptions.ids && queryOptions.ids.length) {
      queryBuilder.whereIn(
        ConstructionSite.sqlField(cs => cs.id, true),
        queryOptions.ids
      );
    }

    if (queryOptions.contractorEmail) {
      queryBuilder = this._appendContractorEmail(
        queryBuilder,
        queryOptions.contractorEmail
      );
    }

    if (queryOptions.includeLastInspector) {
      queryBuilder = this._appendLastInspector(queryBuilder);
    }

    if (queryOptions.addedFrom || queryOptions.addedTo) {
      queryBuilder.where(function() {
        // added from, to
        if (queryOptions.addedFrom || queryOptions.addedTo) {
          this.orWhere(function() {
            if (queryOptions.addedFrom) {
              this.where('DateAdded', '>=', queryOptions.addedFrom);
            }
            if (queryOptions.addedTo) {
              this.where('DateAdded', '<=', queryOptions.addedTo);
            }
          });
        }
      });
    }

    if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
      queryBuilder.where(function() {
        if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
          this.orWhere(function() {
            if (queryOptions.inspectedFrom) {
              this.where(
                'LatestInspectionDate',
                '>=',
                queryOptions.inspectedFrom
              );
            }
            if (queryOptions.inspectedTo) {
              this.where(
                'LatestInspectionDate',
                '<=',
                queryOptions.inspectedTo
              );
            }
          });
        }
      });
    }

    if (queryOptions.completeAfter) {
      queryBuilder.where(function() {
        this.where(
          'completionDate',
          '>=',
          queryOptions.completeAfter
        ).orWhereNull('completionDate');
      });
    }

    if (queryOptions.active) {
      queryBuilder.whereNull('completionDate');
    }

    if (queryOptions.orderByRaw) {
      queryBuilder.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSqlFull, queryOptions.orderByDirection);
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder
      .where(this.getSqlField(e => e.customerId, true), customerId)
      .where(this.getSqlField(e => e.isDeleted, true), 0);
    return queryBuilder;
  }

  private _appendContractorEmail(qb: Knex.QueryBuilder, email: string) {
    return qb
      .join(
        EntityContact.tableName,
        EntityContact.sqlField(f => f.globalIdFk, true),
        ConstructionSite.sqlField(f => f.globalId, true)
      )
      .join(
        Contact.tableName,
        Contact.sqlField(f => f.id, true),
        EntityContact.sqlField(f => f.contactId, true)
      )
      .where({
        [Contact.sqlField(f => f.email, true)]: email,
        [Contact.sqlField(f => f.isDeleted, true)]: 0,
      });
  }

  private _appendLastInspector(qb: Knex.QueryBuilder) {
    return qb.joinRaw(`
        LEFT JOIN
          (
            SELECT MIN(CSI1.Id) AS Id, CSI1.ConstructionSiteId
            FROM ConstructionSiteInspections CSI1
            JOIN
              (
                SELECT ConstructionSiteId, MAX(InspectionDate) AS latestDate
                FROM ConstructionSiteInspections
                GROUP BY ConstructionSiteId
              ) CSI2
              ON CSI1.ConstructionSiteId = CSI2.ConstructionSiteId
                AND CSI1.InspectionDate = CSI2.latestDate
            GROUP BY CSI1.ConstructionSiteId
          ) LID
          ON LID.ConstructionSiteId = ConstructionSites.Id
        LEFT JOIN ConstructionSiteInspections LastInspection
          ON LastInspection.Id = LID.Id
      `);
  }
}
