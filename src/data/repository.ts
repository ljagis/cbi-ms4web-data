import * as Knex from 'knex';
import {
  getTableName,
  getIdField,
  getSqlFields,
  getSqlMapping,
  getSqlField,
} from '../decorators';
import * as Bluebird from 'bluebird';
import { Entity } from '../models';
import { validate, ValidatorOptions } from 'class-validator';
import { throwErrorOnValidationError, objectToSqlData } from '../shared';
import { inject, injectable, unmanaged } from 'inversify';

@injectable()
export abstract class Repository<T extends Entity> {
  protected entity: any;

  protected tableName: string;
  /**
   * `id` field in typescript
   */
  protected idField: string;
  /**
   * Id field in SQL
   */
  protected idFieldSql: string;
  protected idFieldSqlFull: string;

  protected defaultSelect: string[];

  @inject(Knex) protected knex: Knex;

  constructor(@unmanaged() entity: any) {
    this.entity = entity;
    this.tableName = getTableName(entity);
    this.idField = getIdField(this.entity);
    this.idFieldSql = getSqlField(this.entity, this.idField);
    this.idFieldSqlFull = getSqlField(this.entity, this.idField, true);
    this.defaultSelect = this.getSelectFields(undefined, true);
  }

  /**
   * Proxy method for class-validator.  If fails, throws error
   * See https://github.com/pleerock/class-validator
   * @param  {[type]}                     options [description]
   * @return {Bluebird<ValidationError[]>}         [description]
   */
  validate(obj: T, validatorOptions?: ValidatorOptions): Bluebird<T> {
    return Bluebird.resolve(validate(obj, validatorOptions)) // converts to bluebird promise
      .tap(throwErrorOnValidationError)
      .return(obj) as Bluebird<T>;
  }

  /**
   * Creates a new class of type T from obj
   *
   * @returns {T}
   *
   */
  protected mapObjectToEntityClass(obj, ...args): T {
    return this.mapObjectToClass<T>(this.entity, obj, ...args);
  }

  /**
   * Creates a new class of type TClass from obj
   *
   * @protected
   * @template TClass
   * @param {{ new (...args)}} TheClass
   * @param {any} obj
   * @param {any} args
   * @returns {TClass}
   *
   * @memberOf Repository
   */
  protected mapObjectToClass<TClass>(
    TheClass: { new (...args) },
    obj,
    ...args
  ): TClass {
    return Object.assign(new TheClass(...args), obj);
  }

  /**
   * Helper function to translate to SQL
   */
  protected getSqlField(
    propertyOrFunction: keyof T | ((args?: T) => any),
    includeTable?: boolean
  ): string {
    return getSqlField<T>(this.entity, propertyOrFunction, includeTable);
  }

  /**
   * Helper function to translate to SQL
   */
  protected getSelectFields(
    properties?: string[],
    includeTable?: boolean
  ): string[] {
    return getSqlFields(this.entity, properties, includeTable, true);
  }

  /**
   * Generic method to map object to SQL for inserts.
   * Converts boolean to bit in SQL
   *
   * @protected
   * @param {any} entity
   * @returns
   *
   * @memberOf Repository
   */
  protected mapObjectToSql(entity) {
    return this.objectToSqlData(this.entity, entity);
  }

  /**
   * Removes extra properties from `obj` that are not part of the sql mapping class `T`
   *
   * @protected
   * @template T
   * @param {{ new (...args): T}} TheEntity
   * @param {any} obj
   * @returns
   *
   * @memberOf Repository
   */
  protected removeExtraPropertiesFromEntity(
    TheEntity: { new (...args): T },
    obj
  ) {
    let mappings = getSqlMapping(TheEntity);
    Object.keys(obj).forEach(prop => {
      if (mappings[prop] === undefined) {
        delete mappings[prop];
      }
    });
    return obj;
  }

  /**
   * object to sql representation
   *
   * @param {{ new ()}} EntityClass
   * @param {any} obj
   * @param options
   * @returns
   *
   */
  public objectToSqlData(EntityClass: { new () }, obj, options?) {
    return objectToSqlData(EntityClass, obj, this.knex, options);
  }
}
