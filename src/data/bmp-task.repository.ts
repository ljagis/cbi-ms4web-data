import { Repository } from './repository';
import { QueryOptions, BmpTask, BmpControlMeasure } from '../models';
import { injectable } from 'inversify';
import { getSqlField, getSqlFields } from '../decorators';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

@injectable()
export class BmpTaskRepository extends Repository<BmpTask> {
  constructor() {
    super(BmpTask);
  }

  fetch(
    customer: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpTask[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSql, queryOptions.orderByDirection);
    }
    queryBuilder
      .where(BmpTask.getSqlField<BmpTask>(c => c.customerId), customer)
      .where(BmpTask.getSqlField<BmpTask>(t => t.isDeleted), 0);

    if (typeof queryBuilderOverrides === 'function') {
      queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map<BmpTask, BmpTask>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  fetchById(
    id: number,
    customer: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpTask> {
    let queryBuilder = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id)
      .where(BmpTask.getSqlField<BmpTask>(c => c.customerId), customer)
      .where(BmpTask.getSqlField<BmpTask>(t => t.isDeleted), 0);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder
      .map<BmpTask, BmpTask>(l => this.mapObjectToEntityClass(l))
      .get<BmpTask>(0);
  }

  insert(task: BmpTask, customerId: string): Bluebird<BmpTask> {
    task.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(task);
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return queryBuilder
      .map(e => this.mapObjectToEntityClass(e))
      .get<BmpTask>(0);
  }

  update(id: number, task: BmpTask, customerId: string): Bluebird<void> {
    const data = this.mapObjectToSql(task);
    const qb = this.knex(this.tableName)
      .update(data)
      .where(getSqlField<BmpTask>(this.entity, c => c.id), id)
      .where(getSqlField<BmpTask>(this.entity, c => c.customerId), customerId);
    return qb;
  }

  del(id: number, customerId: string): Bluebird<any> {
    let trans = this.knex.transaction(trx => {
      // del cm
      let delTaskQb = this.knex(this.tableName)
        .update(this.getSqlField(b => b.isDeleted), 1)
        .where(this.getSqlField(c => c.id), id)
        .where(this.getSqlField(c => c.customerId), customerId);

      return delTaskQb.transacting(trx);
    });

    return trans;
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      BmpTask.sqlField(a => a.id, true, true),
      BmpTask.sqlField(a => a.name, true, true),
      BmpTask.sqlField(a => a.controlMeasureId, true, true),
      `${BmpControlMeasure.sqlField(
        cm => cm.name,
        true
      )} as controlMeasureName`,
    ];
    return await this.knex(BmpTask.getTableName())
      .leftOuterJoin(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(p => p.id, true),
        BmpTask.sqlField(c => c.controlMeasureId, true)
      )
      .where(BmpTask.sqlField(c => c.customerId, true), customerId)
      .where(BmpTask.sqlField(c => c.isDeleted, true), 0)
      .where(BmpControlMeasure.sqlField(c => c.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }
}
