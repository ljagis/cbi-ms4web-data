import { Repository } from './repository';
import { BMP, MG } from '../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';
import { getTableName } from '../decorators';

@injectable()
export class BmpRepository extends Repository<BMP> {
  constructor() {
    super(BMP);
  }

  async create(bmp: Partial<BMP>) {
    const returnFields = this.getSelectFields();
    const createdBmp = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(bmp))
      .returning(returnFields)
      .get<BMP>(0);

    return createdBmp;
  }

  async fetchBySwmpId(swmpId: number): Bluebird<BMP[]> {
    return await this.knex(this.tableName)
      .where(BMP.sqlField(b => b.swmpId), swmpId)
      .select(this.defaultSelect)
      .orderByRaw(
        `len(${BMP.sqlField(b => b.number)}), ${BMP.sqlField(b => b.number)}`
      );
  }

  async update(bmp: Partial<BMP>) {
    const qb = this.knex(this.tableName)
      .update(this.mapObjectToSql(bmp))
      .where(BMP.sqlField(b => b.id), bmp.id);

    return qb;
  }

  async delete(id: number) {
    const trans = this.knex.transaction(trx => {
      const delMg = this.knex(getTableName(MG))
        .where(MG.sqlField(m => m.bmpId), id)
        .delete();

      const delBmp = this.knex(this.tableName)
        .where(BMP.sqlField(b => b.id), id)
        .delete();

      return delMg.transacting(trx).return(delBmp.transacting(trx));
    });

    return trans;
  }
}
