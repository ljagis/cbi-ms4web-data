import * as Bluebird from 'bluebird';
import * as moment from 'moment';

import {
  CustomField,
  CustomFieldValue,
  CustomFormTemplate,
  InspectionType,
  Structure,
  StructureInspection,
  User,
} from '../models';

import { AssetInspectionRepository } from './asset-inspection.repository';
import { EntityTypes } from '../shared/entity-types';
import { injectable } from 'inversify';

@injectable()
export class StructureInspectionRepository extends AssetInspectionRepository<
  Structure,
  StructureInspection
> {
  constructor() {
    super(StructureInspection);
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      StructureInspection.sqlField(c => c.id, true, true),
      StructureInspection.sqlField(c => c.createdDate, true, true),
      StructureInspection.sqlField(c => c.structureId, true, true),
      `${Structure.sqlField(c => c.name, true)} as structureName`,
      `${Structure.sqlField(c => c.trackingId)} as structureTrackingId`,
      StructureInspection.sqlField(c => c.inspectionDate, true, true),
      StructureInspection.sqlField(c => c.scheduledInspectionDate, true, true),
      StructureInspection.sqlField(c => c.complianceStatus, true, true),
      StructureInspection.sqlField(c => c.inspectorId, true, true),
      `inspector.${User.sqlField(u => u.givenName)} as inspectorFirstName`,
      `inspector.${User.sqlField(u => u.surname)} as inspectorLastName`,
      StructureInspection.sqlField(c => c.followUpInspectionId, true, true),
      StructureInspection.sqlField(c => c.additionalInformation, true, true),
      // weather
      StructureInspection.sqlField(c => c.weatherCondition, true, true),
      StructureInspection.sqlField(c => c.weatherTemperatureF, true, true),
      StructureInspection.sqlField(c => c.weatherPrecipitationIn, true, true),
      StructureInspection.sqlField(c => c.weatherLast72, true, true),
      StructureInspection.sqlField(c => c.weatherLast24, true, true),
      StructureInspection.sqlField(c => c.windDirection, true, true),
      StructureInspection.sqlField(c => c.windSpeedMph, true, true),
      StructureInspection.sqlField(c => c.weatherStationId, true, true),
      StructureInspection.sqlField(c => c.weatherDateTime, true, true),
      StructureInspection.sqlField(c => c.timeIn, true, true),
      StructureInspection.sqlField(c => c.timeOut, true, true),

      StructureInspection.sqlField(c => c.customFormTemplateId, true, true),
      StructureInspection.sqlField(c => c.inspectionTypeId, true, true),
      `${InspectionType.sqlField(i => i.name, true)} as inspectionType`,
    ];

    return await this.fetchBaseQueryBuilder(customerId)
      .leftOuterJoin(
        `${User.getTableName()} as inspector`,
        `inspector.${User.sqlField(u => u.id)}`,
        StructureInspection.sqlField(i => i.inspectorId, true)
      )
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${StructureInspection.sqlField(
        c => c.id,
        true
      )} as structureInspectionId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
      StructureInspection.sqlField(c => c.customFormTemplateId, true, true),
      CustomFormTemplate.sqlField(ct => ct.name, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        StructureInspection.getTableName(),
        StructureInspection.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        Structure.getTableName(),
        Structure.sqlField(cs => cs.id, true),
        StructureInspection.sqlField(i => i.structureId, true)
      )
      .join(
        CustomFormTemplate.getTableName(),
        CustomFormTemplate.sqlField(ct => ct.id, true),
        StructureInspection.sqlField(i => i.customFormTemplateId, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.StructureInspection
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(StructureInspection.sqlField(f => f.isDeleted, true), 0)
      .where(Structure.sqlField(f => f.isDeleted, true), 0)
      .where(Structure.sqlField(f => f.customerId, true), customerId)
      .orderBy(StructureInspection.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true))
      .timeout(60000);
    return await qb;
  }

  /*
    Table structure for pastDueInspectionsReport and upcomingInspectionsReport.
    Lines denote how tables are joined, *'s are selected keys.
<<<<<<< HEAD
=======

>>>>>>> feat: SWMP, BMP, MG APIs
          StructureInspections          Structures     Users      UserCustomers      Customers
                *Id                      *Name
          *ConstructionId ---------------- Id      _-------------------------------- Id
        ScheduledInspectionDate        *CustomerId --------------- CustomerId     *DailyInspectorEmail
            *InspectionDate                             Id ------- *UserId       *MondayInspectorEmail
                                                      *Email        *Role         *MondayAdminEmail
                                                      *GivenName
                                                      *Surname
            *InspectorId ------------------------------ Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
            *InspectorId2 ----------------------------- Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
      */

  async pastDueInspectionsReport() {
    const start = '2019-9-22';
    const end = moment()
      .utc()
      .format('YYYY-MM-DD');
    return await this.knex(this.tableName)
      .select([
        'StructureInspections.Id as inspectionId',
        'StructureInspections.StructureId as siteId',
        'StructureInspections.InspectionDate as inspectionDate',
        'StructureInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'StructureInspections.InspectorId as inspectorId',
        'StructureInspections.InspectorId2 as inspectorId2',
        'Structures.Name as name',
        'Structures.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('StructureInspections.IsDeleted', '=', 'false')
      .where('Structures.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Structures', {
        'StructureInspections.StructureId': 'Structures.Id',
      })
      .leftJoin('Users', { 'StructureInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'StructureInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Structures.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Structures.CustomerId': 'Customers.Id' });
  }

  async upcomingInspectionsReport() {
    const start = moment()
      .utc()
      .format('YYYY-MM-DD');
    const end = moment()
      .utc()
      .add(7, 'days')
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'StructureInspections.Id as inspectionId',
        'StructureInspections.StructureId as siteId',
        'StructureInspections.InspectionDate as inspectionDate',
        'StructureInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'StructureInspections.InspectorId as inspectorId',
        'StructureInspections.InspectorId2 as inspectorId2',
        'Structures.Name as name',
        'Structures.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('StructureInspections.IsDeleted', '=', 'false')
      .where('Structures.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Structures', {
        'StructureInspections.StructureId': 'Structures.Id',
      })
      .leftJoin('Users', { 'StructureInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'StructureInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Structures.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Structures.CustomerId': 'Customers.Id' });
  }
}
