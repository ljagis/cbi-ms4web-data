import {
  InspectionType,
  Creatable,
  Fetchable,
  Updateable,
  DeleteOptions,
} from '../models';
import { injectable } from 'inversify';
import { Repository } from './repository';
import * as Bluebird from 'bluebird';
import { QueryBuilder } from 'knex';
import { getSqlFields2 } from '../decorators';
import { objectToSqlData } from '../shared/object-to-sql';
import { CrudDeletable } from '../models/crud';

@injectable()
export class InspectionTypeRepository extends Repository<InspectionType>
  implements Creatable<InspectionType>,
    Fetchable<InspectionType>,
    Updateable<InspectionType>,
    CrudDeletable {
  constructor() {
    super(InspectionType);
  }

  async fetch(
    customerId: string,
    queryBuilderOverrides?: (queryBuilder: QueryBuilder) => QueryBuilder
  ): Bluebird<InspectionType[]> {
    const qb = this.getInspectionTypes(customerId)
      .where(InspectionType.sqlField(f => f.isDeleted), 0)
      .where(InspectionType.sqlField(f => f.isArchived), 0);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return await qb;
  }

  async fetchById(
    customerId: string,
    id: number,
    queryBuilderOverrides?: (queryBuilder: QueryBuilder) => QueryBuilder
  ): Bluebird<InspectionType> {
    const qb = this.getInspectionTypes(customerId).where({
      [InspectionType.sqlField(f => f.id)]: id,
      [InspectionType.sqlField(f => f.isDeleted)]: 0,
    });

    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return await qb.get<InspectionType>(0);
  }

  async fetchByCftId(
    customerId: string,
    customFormTemplateId: number,
    queryBuilderOverrides?: (queryBuilder: QueryBuilder) => QueryBuilder
  ): Bluebird<InspectionType[]> {
    const qb = this.getInspectionTypes(customerId)
      .where(
        InspectionType.sqlField(f => f.customFormTemplateId),
        customFormTemplateId
      )
      .where(InspectionType.sqlField(f => f.isDeleted), 0);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return await qb;
  }

  async fetchByEntityType(
    customerId: string,
    entityType: string,
    queryBuilderOverrides?: (QueryBuilder: QueryBuilder) => QueryBuilder
  ): Bluebird<InspectionType[]> {
    const qb = this.getInspectionTypes(customerId)
      .where(InspectionType.sqlField(f => f.entityType), entityType)
      .where(InspectionType.sqlField(f => f.isDeleted), 0);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return await qb;
  }

  async update(
    customerId: string,
    id: number,
    data: Partial<InspectionType>
  ): Bluebird<InspectionType> {
    const sqlData = objectToSqlData(InspectionType, data, this.knex);
    const qb = this.knex(InspectionType.getTableName())
      .update(sqlData)
      .where(InspectionType.sqlField(f => f.id), id)
      .returning(getSqlFields2(InspectionType, { asProperty: true }))
      .get<InspectionType>(0);
    return await qb;
  }

  async insert(
    customerId: string,
    data: Partial<InspectionType>
  ): Bluebird<InspectionType> {
    data.customerId = customerId; // ensure customerId
    const sqlData = objectToSqlData(InspectionType, data, this.knex);
    const qb = this.knex(InspectionType.getTableName())
      .insert(sqlData)
      .returning(getSqlFields2(InspectionType, { asProperty: true }))
      .get<InspectionType>(0);
    return await qb;
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<number> {
    const qb = this.knex(InspectionType.getTableName()).where({
      [InspectionType.sqlField(f => f.id)]: id,
      [InspectionType.sqlField(f => f.customerId)]: customerId,
    });

    if (options && options.force) {
      qb.del();
    } else {
      qb.update(InspectionType.sqlField(f => f.isDeleted), 1);
    }
    return await qb;
  }

  async archive(customerId: string, id: number): Bluebird<void> {
    await this.knex(this.tableName)
      .where(this.getSqlField(it => it.id), id)
      .where(this.getSqlField(it => it.customerId), customerId)
      .update(this.getSqlField(it => it.isArchived), 1);
  }

  private getInspectionTypes(customerId: string) {
    const selectFields = getSqlFields2(InspectionType, {
      includeTable: true,
      asProperty: true,
    });
    return this.knex(InspectionType.getTableName())
      .select(selectFields)
      .where(InspectionType.sqlField(f => f.customerId), customerId);
  }
}
