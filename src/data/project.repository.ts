import { CustomerLookupRepository } from './customer-lookup.repository';
import { Project } from '../models';
import { injectable } from 'inversify';

@injectable()
export class ProjectRepository extends CustomerLookupRepository<Project> {
  constructor() {
    super(Project);
  }
}
