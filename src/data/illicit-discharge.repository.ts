import {
  IllicitDischarge,
  User,
  Community,
  Watershed,
  ReceivingWater,
  CustomField,
  CustomFieldValue,
  CustomFormTemplate,
  InspectionType,
  IllicitDischargeQueryOptions,
  EntityContact,
  Contact,
} from '../models';
import { AssetRepository } from './asset.repository';
import * as Bluebird from 'bluebird';
import { injectable } from 'inversify';
import { EntityTypes } from '../shared/entity-types';
import * as Knex from 'knex';
import { FetchAsset } from '../interfaces';
import { Defaults } from '../shared';

@injectable()
export class IllicitDischargeRepository extends AssetRepository<
  IllicitDischarge
> implements FetchAsset<IllicitDischarge> {
  constructor() {
    super(IllicitDischarge);
  }

  async fetch(
    customerId: string,
    queryOptions?: IllicitDischargeQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<IllicitDischarge[]> {
    return await this.getFetchQueryBuilder(
      customerId,
      queryOptions,
      queryBuilderOverrides
    ).map(e => this.mapObjectToEntityClass(e));
  }

  async fetchById(
    id: number,
    customerId: string,
    queryOptions?: IllicitDischargeQueryOptions
  ): Bluebird<IllicitDischarge> {
    const queryBuilder = this.getFetchQueryBuilder(
      customerId,
      queryOptions
    ).where(IllicitDischarge.sqlField(f => f.id, true), id);
    return await queryBuilder
      .map<IllicitDischarge, IllicitDischarge>(asset =>
        this.mapObjectToEntityClass(asset)
      )
      .get<IllicitDischarge>(0); // first
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      IllicitDischarge.sqlField(c => c.id, true, true),
      IllicitDischarge.sqlField(c => c.name, true, true),
      IllicitDischarge.sqlField(c => c.lat, true, true),
      IllicitDischarge.sqlField(c => c.lng, true, true),
      IllicitDischarge.sqlField(c => c.complianceStatus, true, true),
      IllicitDischarge.sqlField(c => c.dateAdded, true, true),
      IllicitDischarge.sqlField(c => c.dateReported, true, true),
      IllicitDischarge.sqlField(c => c.followUpDate, true, true),

      // Physical Location
      IllicitDischarge.sqlField(c => c.physicalAddress1, true, true),
      IllicitDischarge.sqlField(c => c.physicalAddress2, true, true),
      IllicitDischarge.sqlField(c => c.physicalCity, true, true),
      IllicitDischarge.sqlField(c => c.physicalState, true, true),
      IllicitDischarge.sqlField(c => c.physicalZip, true, true),

      IllicitDischarge.sqlField(c => c.dischargeDescription, true, true),

      // Investigator
      IllicitDischarge.sqlField(c => c.investigatorId, true, true),
      `${User.sqlField(u => u.givenName)} as investigatorFirstName`,
      `${User.sqlField(u => u.surname)} as investigatorLastName`,

      IllicitDischarge.sqlField(c => c.communityId, true, true),
      `${Community.sqlField(c => c.name, true)} as communityName`,
      IllicitDischarge.sqlField(c => c.watershedId, true, true),
      `${Watershed.sqlField(c => c.name, true)} as watershedName`,
      IllicitDischarge.sqlField(c => c.subwatershedId, true, true),
      `SubWatershed.${Watershed.sqlField(c => c.name)} as subWatershedName`,
      IllicitDischarge.sqlField(c => c.receivingWatersId, true, true),
      `${ReceivingWater.sqlField(c => c.name, true)} as receivingWaterName`,
      IllicitDischarge.sqlField(c => c.additionalInformation, true, true),

      IllicitDischarge.sqlField(c => c.customFormTemplateId, true, true),
      IllicitDischarge.sqlField(c => c.inspectionTypeId, true, true),
      `${InspectionType.sqlField(i => i.name, true)} as inspectionType`,
    ];
    return await this.knex(this.tableName)
      .leftOuterJoin(
        `${User.getTableName()}`,
        `${User.sqlField(u => u.id, true)}`,
        IllicitDischarge.sqlField(i => i.investigatorId, true)
      )
      .leftOuterJoin(
        Community.getTableName(),
        Community.sqlField(p => p.id, true),
        IllicitDischarge.sqlField(c => c.communityId, true)
      )
      .leftOuterJoin(
        Watershed.getTableName(),
        Watershed.sqlField(p => p.id, true),
        IllicitDischarge.sqlField(c => c.watershedId, true)
      )
      .leftOuterJoin(
        `${Watershed.getTableName()} as SubWatershed`,
        `SubWatershed.${Watershed.sqlField(p => p.id)}`,
        IllicitDischarge.sqlField(c => c.subwatershedId, true)
      )
      .leftOuterJoin(
        ReceivingWater.getTableName(),
        ReceivingWater.sqlField(p => p.id, true),
        IllicitDischarge.sqlField(c => c.receivingWatersId, true)
      )
      .join(
        InspectionType.getTableName(),
        InspectionType.sqlField(i => i.id, true),
        IllicitDischarge.sqlField(c => c.inspectionTypeId, true)
      )
      .where(IllicitDischarge.sqlField(c => c.customerId, true), customerId)
      .where(IllicitDischarge.sqlField(c => c.isDeleted, true), 0)
      .orderBy(IllicitDischarge.sqlField(c => c.id, true))
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${IllicitDischarge.sqlField(c => c.id, true)} as illicitDischargeId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
      IllicitDischarge.sqlField(c => c.customFormTemplateId, true, true),
      CustomFormTemplate.sqlField(ct => ct.name, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        IllicitDischarge.getTableName(),
        IllicitDischarge.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        CustomFormTemplate.getTableName(),
        CustomFormTemplate.sqlField(ct => ct.id, true),
        IllicitDischarge.sqlField(id => id.customFormTemplateId, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.IllicitDischarge
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(IllicitDischarge.sqlField(f => f.isDeleted, true), 0)
      .where(IllicitDischarge.sqlField(f => f.customerId, true), customerId)
      .orderBy(IllicitDischarge.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true))
      .timeout(60000);
    return await qb;
  }

  private getFetchQueryBuilder(
    customerId: string,
    queryOptions?: IllicitDischargeQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;

    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .limit(limit)
      .offset(queryOptions.offset);

    // selects InspectionType.name as inspectionType
    const inspectionTypeIdField = IllicitDischarge.sqlField(
      f => f.inspectionTypeId,
      true
    );
    queryBuilder.select(
      `${InspectionType.sqlField(f => f.name, true)} as inspectionType`
    );
    queryBuilder.join(
      InspectionType.tableName,
      InspectionType.sqlField(f => f.id, true),
      inspectionTypeIdField
    );

    if (queryOptions.contractorEmail) {
      queryBuilder = this._appendContractorEmail(
        queryBuilder,
        queryOptions.contractorEmail
      );
    }

    if (queryOptions.constructionSiteId) {
      queryBuilder = queryBuilder.where(
        this.getSqlField(e => e.constructionSiteId, true),
        queryOptions.constructionSiteId
      );
    }

    if (queryOptions.constructionSiteIds) {
      queryBuilder = queryBuilder.whereIn(
        this.getSqlField(e => e.constructionSiteId, true),
        queryOptions.constructionSiteIds
      );
    }

    if (
      queryOptions.addedFrom ||
      queryOptions.addedTo ||
      queryOptions.reportedFrom ||
      queryOptions.reportedTo ||
      queryOptions.followUpFrom ||
      queryOptions.followUpTo
    ) {
      queryBuilder.where(function() {
        // added from, to
        if (queryOptions.addedFrom || queryOptions.addedTo) {
          this.orWhere(function() {
            if (queryOptions.addedFrom) {
              this.where('DateAdded', '>=', queryOptions.addedFrom);
            }
            if (queryOptions.addedTo) {
              this.where('DateAdded', '<=', queryOptions.addedTo);
            }
          });
        }

        // reported from, to
        if (queryOptions.reportedFrom || queryOptions.reportedTo) {
          this.orWhere(function() {
            if (queryOptions.reportedFrom) {
              this.where('DateReported', '>=', queryOptions.reportedFrom);
            }
            if (queryOptions.reportedTo) {
              this.where('DateReported', '<=', queryOptions.reportedTo);
            }
          });
        }

        // followup from, to
        if (queryOptions.followUpFrom || queryOptions.followUpTo) {
          this.orWhere(function() {
            if (queryOptions.followUpFrom) {
              this.where('FollowUpDate', '>=', queryOptions.followUpFrom);
            }
            if (queryOptions.followUpTo) {
              this.where('FollowUpDate', '<=', queryOptions.followUpTo);
            }
          });
        }
      });
    }

    if (queryOptions.orderByRaw) {
      queryBuilder.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSqlFull, queryOptions.orderByDirection);
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder
      .where(this.getSqlField(e => e.customerId, true), customerId)
      .where(this.getSqlField(e => e.isDeleted, true), 0);
    return queryBuilder;
  }

  private _appendContractorEmail(qb: Knex.QueryBuilder, email: string) {
    return qb
      .join(
        EntityContact.tableName,
        EntityContact.sqlField(f => f.globalIdFk, true),
        IllicitDischarge.sqlField(f => f.globalId, true)
      )
      .join(
        Contact.tableName,
        Contact.sqlField(f => f.id, true),
        EntityContact.sqlField(f => f.contactId, true)
      )
      .where({
        [Contact.sqlField(f => f.email, true)]: email,
        [Contact.sqlField(f => f.isDeleted, true)]: 0,
      });
  }
}
