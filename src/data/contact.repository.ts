import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  ConstructionSite,
  Contact,
  Entity,
  EntityContact,
  Facility,
  MonitoringLocation,
  Outfall,
  QueryOptions,
  Structure,
} from '../models';
import { getSqlField, getSqlFields2, getTableName } from '../decorators';

import { CustomerLookupRepository } from './customer-lookup.repository';
import { EntityTypes } from '../shared';
import { injectable } from 'inversify';

import moment = require('moment');

@injectable()
export class ContactRepository extends CustomerLookupRepository<Contact> {
  constructor() {
    super(Contact);
  }

  /**
   * Gets contacts based on customer
   *
   * @param {string} customer
   * @param {QueryOptions} queryOptions
   * @param {((qb: Knex.QueryBuilder) => Knex.QueryBuilder)} [queryBuilderOverrides]
   * @returns {Bluebird<Contact[]>}
   *
   */
  fetch(
    customer: string,
    queryOptions: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<Contact[]> {
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder = queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder = queryBuilder.offset(queryOptions.offset);
    }
    if (queryOptions.orderByField) {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder = queryBuilder.orderBy(
        this.idFieldSql,
        queryOptions.orderByDirection
      );
    }

    queryBuilder.where(this.getSqlField(c => c.customerId), customer);

    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder.where(this.getSqlField(c => c.isDeleted), 0);

    return queryBuilder.map(c => this.mapObjectToEntityClass(c));
  }

  /**
   * Gets a contact by `id`
   *
   * @param {number} id
   * @param {string} customerId
   * @returns {Bluebird<Contact>}
   *
   * @memberOf ContactRepository
   */
  fetchById(id: number, customerId: string): Bluebird<Contact> {
    const qb = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id)
      .where(this.getSqlField(c => c.isDeleted), 0)
      .where(this.getSqlField(c => c.customerId), customerId);
    return qb.map(c => this.mapObjectToEntityClass(c)).get<Contact>(0);
  }

  /**
   * Updates a contact.  Supports partial updates.
   *
   * @param {Contact} contact
   * @param {string} customer
   * @returns
   *
   * @memberOf ContactRepository
   */
  update(id: number, contact: Contact, customerId: string): Bluebird<Contact> {
    const data = this.mapObjectToSql(contact);
    const returnFields = getSqlFields2(Contact, { asProperty: true });
    const qb = this.knex(this.tableName)
      .update(data)
      .where(getSqlField<Contact>(Contact, c => c.isDeleted), 0)
      .where(getSqlField<Contact>(Contact, c => c.id), id)
      .where(getSqlField<Contact>(Contact, c => c.customerId), customerId)
      .returning(returnFields)
      .get<Contact>(0);
    return qb;
  }

  /**
   * Delete contact by `id`
   *
   * @param {number} id
   * @param {string} customer
   * @returns
   *
   */
  del(id: number, customerId: string) {
    let qb = this.knex(this.tableName)
      .update(getSqlField<Contact>(Contact, c => c.isDeleted), 1)
      .where(getSqlField<Contact>(Contact, c => c.id), id)
      .where(getSqlField<Contact>(Contact, c => c.customerId), customerId);
    return qb;
  }

  fetchContactForEntity(
    EntityWithContact: { new (...args): Entity },
    contactId: number,
    assetId: number,
    customerId: string
  ): Bluebird<Contact> {
    let queryBuilder = this._fetchContactsForEntityQueryBuilder(
      EntityWithContact,
      assetId,
      customerId
    ).where(getSqlField<Contact>(Contact, c => c.id), contactId);

    return queryBuilder
      .map<any, Contact>(c => this.mapObjectToEntityClass(c))
      .get<Contact>(0);
  }

  /**
   * Fetches contacts for an entity
   *
   * @param {{ new (...args): Entity }} EntityWithContact - CitizenReport, ConstructionSite, etc...
   * @param {number} assetId - the `id` of the asset
   * @param {string} customer - customerId
   * @param {Date} inspectionDate - If it's an inspetion with an inspection date
   * @returns {Bluebird<Contact[]>}
   */
  fetchContactsForEntity(
    EntityWithContact: { new (...args): Entity },
    assetId: number,
    customerId: string,
    inspectionDate?: Date
  ): Bluebird<Contact[]> {
    let queryBuilder = this._fetchContactsForEntityQueryBuilder(
      EntityWithContact,
      assetId,
      customerId,
      inspectionDate
    );
    return queryBuilder.map<any, Contact>(c => this.mapObjectToEntityClass(c));
  }

  /**
   * Builds a QueryBuilder to get contacts for entity
   *
   * @param {{ new (...args): Entity }} EntityWithContact
   * @param {number} assetId
   * @param {string} customerId
   * @param {Date} inspectionDate - If it's an inspetion with an inspection date
   * @returns {Knex.QueryBuilder}
   *
   */
  _fetchContactsForEntityQueryBuilder(
    EntityWithContact: { new (...args): Entity },
    assetId: number,
    customerId: string,
    inspectionDate?: Date
  ): Knex.QueryBuilder {
    const entityContactTableName = getTableName(EntityContact); // 'EntityContacts'
    const entityContactGlobalIdFkSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      ec => ec.globalIdFk,
      true
    ); // 'EntityContacts.GlobalIdFk'
    const entityContactContactIdSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      c => c.contactId,
      true
    ); // 'EntityContacts.ContactId'
    const entityDateRemoved = getSqlField<EntityContact>(
      EntityContact,
      c => c.dateRemoved,
      true
    ); // 'EntityContacts.DateRemoved'
    const entityDateAdded = getSqlField<EntityContact>(
      EntityContact,
      c => c.dateAdded,
      true
    ); // 'EntityContacts.DateAdded'
    const entityContactIsPrimary = getSqlField<EntityContact>(
      EntityContact,
      c => c.isPrimary,
      true,
      true
    ); // 'EntityContacts.IsPrimary'

    const entityTableName = getTableName(EntityWithContact);
    let entityIdFieldFull: string;
    let entityGlobalIdSqlFieldFull: string;
    let entityCustomerIdSqlFieldFull: string;

    if (EntityWithContact === ConstructionSite) {
      entityIdFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.id,
        true
      ); // ConstructionSites.Id
      entityGlobalIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.globalId,
        true
      ); // ConstructionSites.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.customerId,
        true
      ); // ConstructionSites.CustomerId
    } else if (EntityWithContact === Facility) {
      entityIdFieldFull = getSqlField<Facility>(Facility, a => a.id, true); // Facilitys.Id
      entityGlobalIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.globalId,
        true
      ); // Facilitys.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.customerId,
        true
      ); // Facilitys.CustomerId
    } else if (EntityWithContact === MonitoringLocation) {
      entityIdFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.id,
        true
      ); // MonitoringLocations.Id
      entityGlobalIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.globalId,
        true
      ); // MonitoringLocations.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.customerId,
        true
      ); // MonitoringLocations.CustomerId
    } else if (EntityWithContact === Outfall) {
      entityIdFieldFull = getSqlField<Outfall>(Outfall, a => a.id, true); // Outfalls.Id
      entityGlobalIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.globalId,
        true
      ); // Outfalls.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.customerId,
        true
      ); // Outfalls.CustomerId
    } else if (EntityWithContact === Structure) {
      entityIdFieldFull = getSqlField<Structure>(Structure, a => a.id, true); // Structures.Id
      entityGlobalIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.globalId,
        true
      ); // Structures.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.customerId,
        true
      ); // Structures.CustomerId
    } else {
      throw Error('This entity does not support contacts');
    }
    let subqueryBuilder: Knex.QueryBuilder;
    if (inspectionDate) {
      subqueryBuilder = this.knex(entityContactTableName)
        .innerJoin(
          entityTableName,
          entityContactGlobalIdFkSqlFieldFull,
          entityGlobalIdSqlFieldFull
        )
        .where(function() {
          this.whereNull(entityDateAdded).orWhere(
            entityDateAdded,
            '<',
            inspectionDate
          );
        })
        .where(function() {
          this.whereNull(entityDateRemoved).orWhere(
            entityDateRemoved,
            '>',
            inspectionDate
          );
        })
        .where(entityIdFieldFull, assetId)
        .where(entityCustomerIdSqlFieldFull, customerId);
    } else {
      subqueryBuilder = this.knex(entityContactTableName)
        .innerJoin(
          entityTableName,
          entityContactGlobalIdFkSqlFieldFull,
          entityGlobalIdSqlFieldFull
        )
        .whereNull(entityDateRemoved)
        .where(entityIdFieldFull, assetId)
        .where(entityCustomerIdSqlFieldFull, customerId);
    }

    return subqueryBuilder
      .join(
        this.tableName,
        this.getSqlField(c => c.id, true),
        entityContactContactIdSqlFieldFull
      )
      .select([...this.defaultSelect, entityContactIsPrimary])
      .orderBy(this.getSqlField(c => c.name));
  }

  async insertContactForEntity(
    EntityWithContact: { new (...args): Entity },
    entityId: number,
    contactId: number,
    customerId: string,
    trx?: Knex.Transaction,
    isPrimary?: boolean
  ): Bluebird<number> {
    const entityContactTableName = getTableName(EntityContact); // 'EntityContacts'
    const entityContactGlobalIdFkSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.globalIdFk,
      true
    ); // 'EntityContacts.GlobalIdFk'
    const entityContactContactIdSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.contactId,
      true
    ); // 'EntityContacts.ContactId'
    const returnEntityContactIdField = getSqlField<EntityContact>(
      EntityContact,
      e => e.id,
      false,
      true
    );
    const entityContactEntityTypeSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.entityType
    );
    const entityTableName = getTableName(EntityWithContact); // ConstructionSites, etc...
    let entityIdFieldFull: string;
    let entityGlobalIdSqlFieldFull: string;
    let entityCustomerIdSqlFieldFull: string;
    let dataToInsert = {};

    if (EntityWithContact === ConstructionSite) {
      entityIdFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.id,
        true
      ); // ConstructionSites.Id
      entityGlobalIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.globalId,
        true
      ); // ConstructionSites.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.customerId,
        true
      ); // ConstructionSites.CustomerId
      dataToInsert[entityContactEntityTypeSqlFieldFull] =
        EntityTypes.ConstructionSite;
    } else if (EntityWithContact === Facility) {
      entityIdFieldFull = getSqlField<Facility>(Facility, a => a.id, true); // Facilitys.Id
      entityGlobalIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.globalId,
        true
      ); // Facilitys.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.customerId,
        true
      ); // Facilitys.CustomerId
      dataToInsert[entityContactEntityTypeSqlFieldFull] = EntityTypes.Facility;
    } else if (EntityWithContact === MonitoringLocation) {
      entityIdFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.id,
        true
      ); // MonitoringLocations.Id
      entityGlobalIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.globalId,
        true
      ); // MonitoringLocations.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.customerId,
        true
      ); // MonitoringLocations.CustomerId
      dataToInsert[entityContactEntityTypeSqlFieldFull] =
        EntityTypes.MonitoringLocation;
    } else if (EntityWithContact === Outfall) {
      entityIdFieldFull = getSqlField<Outfall>(Outfall, a => a.id, true); // Outfalls.Id
      entityGlobalIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.globalId,
        true
      ); // Outfalls.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.customerId,
        true
      ); // Outfalls.CustomerId
      dataToInsert[entityContactEntityTypeSqlFieldFull] = EntityTypes.Outfall;
    } else if (EntityWithContact === Structure) {
      entityIdFieldFull = getSqlField<Structure>(Structure, a => a.id, true); // Structures.Id
      entityGlobalIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.globalId,
        true
      ); // Structures.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.customerId,
        true
      ); // Structures.CustomerId
      dataToInsert[entityContactEntityTypeSqlFieldFull] = EntityTypes.Structure;
    } else {
      throw 'Asset contact not implemented';
    }

    dataToInsert[entityContactContactIdSqlFieldFull] = contactId;
    dataToInsert[
      getSqlField<EntityContact>(EntityContact, e => e.isPrimary, true)
    ] = isPrimary;
    dataToInsert[
      getSqlField<EntityContact>(EntityContact, e => e.dateAdded, true)
    ] = moment().toDate();
    dataToInsert[
      getSqlField<EntityContact>(EntityContact, e => e.dateRemoved, true)
    ] = null;
    dataToInsert[
      getSqlField<EntityContact>(EntityContact, e => e.isDeleted, true)
    ] = 0;

    dataToInsert[entityContactGlobalIdFkSqlFieldFull] = this.knex(
      entityTableName
    )
      .select(entityGlobalIdSqlFieldFull)
      .where(entityIdFieldFull, entityId)
      .where(entityCustomerIdSqlFieldFull, customerId);
    const qb = this.knex(entityContactTableName)
      .insert(dataToInsert)
      .returning(returnEntityContactIdField);
    if (trx) {
      qb.transacting(trx);
    }
    return qb.then(rs => rs[0]);
  }

  async updateEntityContactPrimary(
    EntityWithContact: { new (...args): Entity },
    entityId: number,
    contactId: number,
    customerId: string,
    isPrimary: boolean
  ): Bluebird<number> {
    const entityContactTableName = getTableName(EntityContact); // 'EntityContacts'
    const entityContactGlobalIdFkSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.globalIdFk,
      true
    ); // 'EntityContacts.GlobalIdFk'
    const entityContactContactIdSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.contactId,
      true
    ); // 'EntityContacts.ContactId'
    const returnEntityContactIdField = getSqlField<EntityContact>(
      EntityContact,
      e => e.id,
      false,
      true
    );

    const entityTableName = getTableName(EntityWithContact); // ConstructionSites, etc...
    let entityIdFieldFull: string;
    let entityGlobalIdSqlFieldFull: string;
    let entityCustomerIdSqlFieldFull: string;

    if (EntityWithContact === ConstructionSite) {
      entityIdFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.id,
        true
      ); // ConstructionSites.Id
      entityGlobalIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.globalId,
        true
      ); // ConstructionSites.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.customerId,
        true
      ); // ConstructionSites.CustomerId
    } else if (EntityWithContact === Facility) {
      entityIdFieldFull = getSqlField<Facility>(Facility, a => a.id, true); // Facilitys.Id
      entityGlobalIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.globalId,
        true
      ); // Facilitys.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.customerId,
        true
      ); // Facilitys.CustomerId
    } else if (EntityWithContact === Structure) {
      entityIdFieldFull = getSqlField<Structure>(Structure, a => a.id, true); // Structures.Id
      entityGlobalIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.globalId,
        true
      ); // Structures.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.customerId,
        true
      ); // Structures.CustomerId
    } else {
      throw 'Asset contact not implemented';
    }

    const entityContactGlobalId = await this.knex(entityTableName)
      .select(entityGlobalIdSqlFieldFull)
      .where(entityIdFieldFull, entityId)
      .where(entityCustomerIdSqlFieldFull, customerId)
      .first();

    if (!entityContactGlobalId) {
      throw 'Asset not found';
    }

    return this.knex(entityContactTableName)
      .where(
        entityContactGlobalIdFkSqlFieldFull,
        entityContactGlobalId.GlobalId
      )
      .where(entityContactContactIdSqlFieldFull, contactId)
      .update({ isPrimary })
      .returning(returnEntityContactIdField);
  }

  deleteAssetContact(
    EntityWithContact: { new (...args): Entity },
    entityId: number,
    contactId: number,
    customerId: string
  ) {
    const entityContactTableName = getTableName(EntityContact); // 'EntityContacts'
    const entityContactGlobalIdFkSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.globalIdFk,
      true
    ); // 'EntityContacts.GlobalIdFk'
    const entityContactContactIdSqlFieldFull = getSqlField<EntityContact>(
      EntityContact,
      e => e.contactId,
      true
    ); // 'EntityContacts.ContactId'
    const entityTableName = getTableName(EntityWithContact); // ConstructionSites, etc...
    let entityIdFieldFull: string;
    let entityGlobalIdSqlFieldFull: string;
    let entityCustomerIdSqlFieldFull: string;

    if (EntityWithContact === ConstructionSite) {
      entityIdFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.id,
        true
      ); // ConstructionSites.Id
      entityGlobalIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.globalId,
        true
      ); // ConstructionSites.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<ConstructionSite>(
        ConstructionSite,
        a => a.customerId,
        true
      ); // ConstructionSites.CustomerId
    } else if (EntityWithContact === Facility) {
      entityIdFieldFull = getSqlField<Facility>(Facility, a => a.id, true); // Facilitys.Id
      entityGlobalIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.globalId,
        true
      ); // Facilitys.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Facility>(
        Facility,
        a => a.customerId,
        true
      ); // Facilitys.CustomerId
    } else if (EntityWithContact === MonitoringLocation) {
      entityIdFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.id,
        true
      ); // MonitoringLocations.Id
      entityGlobalIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.globalId,
        true
      ); // MonitoringLocations.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<MonitoringLocation>(
        MonitoringLocation,
        a => a.customerId,
        true
      ); // MonitoringLocations.CustomerId
    } else if (EntityWithContact === Outfall) {
      entityIdFieldFull = getSqlField<Outfall>(Outfall, a => a.id, true); // Outfalls.Id
      entityGlobalIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.globalId,
        true
      ); // Outfalls.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Outfall>(
        Outfall,
        a => a.customerId,
        true
      ); // Outfalls.CustomerId
    } else if (EntityWithContact === Structure) {
      entityIdFieldFull = getSqlField<Structure>(Structure, a => a.id, true); // Structures.Id
      entityGlobalIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.globalId,
        true
      ); // Structures.GlobalId
      entityCustomerIdSqlFieldFull = getSqlField<Structure>(
        Structure,
        a => a.customerId,
        true
      ); // Structures.CustomerId
    } else {
      throw 'Asset contact not implemented';
    }
    const qb = this.knex(entityContactTableName)
      .update(getSqlField<EntityContact>(EntityContact, c => c.isDeleted), 1)
      .update(
        getSqlField<EntityContact>(EntityContact, c => c.dateRemoved),
        moment().toDate()
      )
      .where(entityContactContactIdSqlFieldFull, contactId)
      .whereIn(
        entityContactGlobalIdFkSqlFieldFull,
        this.knex(entityTableName)
          .select(entityGlobalIdSqlFieldFull)
          .where(entityIdFieldFull, entityId)
          .where(entityCustomerIdSqlFieldFull, customerId)
      );

    return qb;
  }

  /**
   * Exports joined list of entity contacts + assets for export sheet
   * @param customerId
   */
  async exportEntityContacts(customerId: string): Bluebird<any[]> {
    const exportFields = [
      EntityContact.sqlField(c => c.contactId, true, true),
      EntityContact.sqlField(c => c.entityType, true, true),
      `${ConstructionSite.sqlField(c => c.id, true)} as constructionSiteId`,
      `${Facility.sqlField(c => c.id, true)} as facilityId`,
      `${Structure.sqlField(c => c.id, true)} as structureId`,

      Contact.sqlField(c => c.contactType, true, true),
      Contact.sqlField(c => c.name, true, true),
      Contact.sqlField(c => c.company, true, true),
      Contact.sqlField(c => c.email, true, true),
      Contact.sqlField(c => c.phone, true, true),
      Contact.sqlField(c => c.mobilePhone, true, true),
      Contact.sqlField(c => c.fax, true, true),
      Contact.sqlField(c => c.mailingAddressLine1, true, true),
      Contact.sqlField(c => c.mailingAddressLine2, true, true),
      Contact.sqlField(c => c.mailingCity, true, true),
      Contact.sqlField(c => c.mailingState, true, true),
      Contact.sqlField(c => c.mailingZip, true, true),
    ];

    const qb = this.knex(EntityContact.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        Contact.getTableName(),
        Contact.sqlField(v => v.id, true),
        EntityContact.sqlField(c => c.contactId, true)
      )
      .leftOuterJoin(
        ConstructionSite.getTableName(),
        ConstructionSite.sqlField(v => v.globalId, true),
        EntityContact.sqlField(c => c.globalIdFk, true)
      )
      .leftOuterJoin(
        Facility.getTableName(),
        Facility.sqlField(v => v.globalId, true),
        EntityContact.sqlField(c => c.globalIdFk, true)
      )
      .leftOuterJoin(
        Structure.getTableName(),
        Structure.sqlField(v => v.globalId, true),
        EntityContact.sqlField(c => c.globalIdFk, true)
      )
      .where(EntityContact.sqlField(f => f.isDeleted, true), 0)
      .where(Contact.sqlField(f => f.isDeleted, true), 0)
      .where(Contact.sqlField(f => f.customerId, true), customerId)
      .where(function(this: Knex.QueryBuilder) {
        this.orWhere(ConstructionSite.sqlField(c => c.isDeleted, true), 0)
          .orWhere(Facility.sqlField(c => c.isDeleted, true), 0)
          .orWhere(Structure.sqlField(c => c.isDeleted, true), 0);
      })
      .orderBy(EntityContact.sqlField(c => c.entityType, true));
    return await qb;
  }
}
