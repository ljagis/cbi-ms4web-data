import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpActivityRepository } from './bmp-activity.repository';

describe('BMP Activity Repository Tests', () => {
  const bmpActRepo = container.get(BmpActivityRepository);

  describe(`exportData tests`, () => {
    it(`should export data`, async () => {
      const customerId = 'LEWISVILLE';
      const activities = await bmpActRepo.exportData(customerId);

      const activity = activities[0];

      chai.expect(activities).to.be.instanceOf(Array);
      chai.expect(activities).to.have.length.greaterThan(0);

      chai.expect(activity).to.have.property('controlMeasureId');
      chai.expect(activity).to.have.property('controlMeasureName');

      chai.expect(activity).to.have.property('taskId');
      chai.expect(activity).to.have.property('taskName');
      chai.expect(activity).to.have.property('dateAdded');
      chai.expect(activity).to.have.property('completedDate');
      chai.expect(activity).to.have.property('dueDate');
      chai.expect(activity).to.have.property('comments');
      chai.expect(activity).to.have.property('description');
      chai.expect(activity).to.have.property('isCompleted');
      chai.expect(activity).to.have.property('title');
    });
  });
});
