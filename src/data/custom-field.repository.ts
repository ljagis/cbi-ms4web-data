import { injectable } from 'inversify';
import { Repository } from './repository';
import {
  Customer,
  CustomField,
  CustomFieldValue,
  CustomFieldValueMap,
  Entity,
  ConstructionSite,
  ConstructionSiteInspection,
  Facility,
  FacilityInspection,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
  HasCustomer,
  BmpDetail,
  BmpControlMeasure,
  IllicitDischarge,
  CitizenReport,
} from '../models';
import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import {
  getTableName,
  getSqlField,
  getSqlFields,
  getSqlFields2,
} from '../decorators';
import { EntityTypes } from '../shared/entity-types';

type TypeOfAssetWithInspection =
  | typeof ConstructionSite
  | typeof Facility
  | typeof Outfall
  | typeof Structure;
// type AssetWithInspection = ConstructionSite | Facility | Outfall | Structure;
type TypeOfInspection =
  | typeof ConstructionSiteInspection
  | typeof FacilityInspection
  | typeof OutfallInspection
  | typeof StructureInspection;
// type AssetInspection = ConstructionSiteInspection | FacilityInspection | OutfallInspection | StructureInspection;
type TypeOfInvestigation = typeof IllicitDischarge | typeof CitizenReport;

@injectable()
export class CustomFieldRepository extends Repository<CustomField> {
  constructor() {
    super(CustomField);
  }

  fetch(customerId: string): Bluebird<CustomField[]> {
    let queryBuilder = this.getfetchQueryBuilder(customerId);
    return queryBuilder.map(o => this.mapObjectToEntityClass(o));
  }

  fetchById(id: number, customerId: string): Bluebird<CustomField> {
    let queryBuilder = this.getfetchQueryBuilder(customerId).where(
      this.idFieldSql,
      id
    );
    return queryBuilder
      .map(o => this.mapObjectToEntityClass(o))
      .get<CustomField>(0);
  }

  async fetchByFormTemplateId(
    customerId: string,
    templateId: number
  ): Bluebird<CustomField[]> {
    return this.getfetchQueryBuilder(customerId).where(
      CustomField.sqlField(f => f.customFormTemplateId, true),
      templateId
    );
  }

  async insert(
    customField: CustomField,
    customerId: string
  ): Bluebird<CustomField> {
    // Ensure `customerId`
    customField.customerId = customerId;
    await this.validate(customField);
    return this.insertQueryBuilder(customField, customerId)
      .map(e => this.mapObjectToEntityClass(e))
      .get<CustomField>(0);
  }

  insertQueryBuilder(customField: Partial<CustomField>, customerId: string) {
    customField.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(customField);
    const returnFields = this.getSelectFields(undefined, false);
    const qb = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return qb;
  }

  /**
   * "deletes" a Custom Field by setting `isDeleted` to true
   *
   * @param {number} id
   * @param {string} customerId
   * @returns {Bluebird<number>} number of affected rows
   *
   * @memberOf CustomFieldRepository
   */
  del(id: number, customerId: string, hardDelete?: boolean): Bluebird<number> {
    let delQb = this.knex(this.tableName);
    if (hardDelete) {
      delQb.delete();
    } else {
      delQb.update(this.getSqlField(f => f.isDeleted), 1);
    }
    delQb
      .where(this.idFieldSql, id)
      .where(this.getSqlField(f => f.customerId), customerId);
    return delQb;
  }

  update(
    id: number,
    customField: Partial<CustomField>,
    customerId: string
  ): Bluebird<number> {
    customField.customerId = customerId; // explicitly set customerId
    let data = this.mapObjectToSql(customField);
    if (Object.keys(data).length) {
      let qb = this.knex(this.tableName)
        .update(data)
        .where(this.idFieldSql, id);
      return qb;
    } else {
      return Bluebird.resolve(null);
    }
  }

  getfetchQueryBuilder(customerId: string): Knex.QueryBuilder {
    return this.knex(this.tableName)
      .select(this.defaultSelect)
      .where(this.getSqlField(f => f.isDeleted), 0)
      .where(this.getSqlField(f => f.customerId), customerId);
  }

  /**
   * Gets custom fields for entity and joins with the values
   *
   * @param {{ new (...args)}} EntityClass
   * @param {number} entityId
   * @param {string} customerId
   * @returns
   *
   * @memberOf CustomFieldRepository
   */
  getCustomFieldValuesForEntity(
    EntityClass: { new (...args) },
    entityId: number,
    customerId: string
  ): Bluebird<CustomFieldValue[]> {
    let entityType = (<any>EntityClass).name;
    let entityTableName = getTableName(EntityClass);
    let entityGlobalId = getSqlField(EntityClass, 'globalId', true);
    let entityIdSF = getSqlField<Entity>(EntityClass, e => e.id, true);
    let entityCustomerIdSF = getSqlField(EntityClass, 'customerId', true);
    let cfCustomerIdSF = getSqlField<CustomField>(
      CustomField,
      cf => cf.customerId,
      true
    );
    let cfEntityTypeSF = getSqlField<CustomField>(
      CustomField,
      cf => cf.entityType,
      true
    );
    let cfStateAbbrSF = getSqlField<CustomField>(
      CustomField,
      cf => cf.stateAbbr,
      true
    );
    let cfvTableName = getTableName(CustomFieldValue);
    let cfvGlobalIdFk = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.globalIdFk,
      true
    );
    let cfvCustomFieldId = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.customFieldId,
      true
    );
    let cfId = getSqlField<CustomField>(CustomField, cf => cf.id, true);

    let subQb = this.knex(cfvTableName)
      .join(entityTableName, entityGlobalId, cfvGlobalIdFk)
      .join(
        CustomField.getTableName(),
        CustomField.getSqlField<CustomField>(c => c.id, true),
        CustomFieldValue.getSqlField<CustomFieldValue>(
          v => v.customFieldId,
          true
        )
      )
      .where(entityIdSF, entityId)
      .where(entityCustomerIdSF, customerId)
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId, true),
        customerId
      )
      .select(`${cfvTableName}.*`)
      .as(cfvTableName);

    // all fields from CustomField + value from CustomFieldValue
    let selectFields = [
      ...getSqlFields(CustomField, undefined, true, true),
      getSqlField<CustomFieldValue>(CustomFieldValue, e => e.value, true, true),
    ];
    let stateAbbrQb = this.knex(Customer.getTableName())
      .select(getSqlField<Customer>(Customer, f => f.stateAbbr))
      .where(getSqlField<Customer>(Customer, f => f.id), customerId);
    let qb = this.knex(this.tableName)
      .leftOuterJoin(subQb, cfvCustomFieldId, cfId)
      .where(getSqlField<CustomField>(CustomField, f => f.isDeleted, true), 0)
      .where(cfEntityTypeSF, entityType)
      .where(cfCustomerIdSF, customerId)
      .orWhere(function(this: Knex.QueryBuilder) {
        this.where(
          CustomField.getSqlField<CustomField>(c => c.customFieldType, true),
          'State'
        ).where(cfStateAbbrSF, '=', stateAbbrQb);
      })
      .select(selectFields);

    /**
     * SELECT [CustomFields].[CustomFieldType] AS [customFieldType]
  ,[CustomFields].[StateAbbr] AS [stateAbbr]
  ,[CustomFields].[EntityType] AS [entityType]
  ,[CustomFields].[DataType] AS [dataType]
  ,[CustomFields].[InputType] AS [inputType]
  ,[CustomFields].[FieldLabel] AS [fieldLabel]
  ,[CustomFields].[FieldOptions] AS [fieldOptions]
  ,[CustomFields].[FieldOrder] AS [fieldOrder]
  ,[CustomFields].[Id] AS [id]
  ,[CustomFieldValues].[Value] AS [value]
FROM [CustomFields]
LEFT JOIN (
  SELECT [CustomFieldValues].*
  FROM [CustomFieldValues]
  INNER JOIN [CitizenReports] ON [CitizenReports].[GlobalId] = [CustomFieldValues].[GlobalIdFK]
  INNER JOIN [CustomFields] ON [CustomFields].[Id] = [CustomFieldValues].[CustomFieldId]
  WHERE [CitizenReports].[Id] = 272
    AND [CitizenReports].[CustomerId] = 'LEWISVILLE-IMPORT'
    AND [CustomFields].[CustomerId] = 'LEWISVILLE-IMPORT'
  ) AS [CustomFieldValues] ON [CustomFieldValues].[CustomFieldId] = [CustomFields].[Id]
WHERE [CustomFields].[EntityType] = 'CitizenReport'
  AND [CustomFields].[CustomerId] = 'LEWISVILLE-IMPORT'
  OR (
    [CustomFields].[CustomFieldType] = 'State'
    AND [CustomFields].[StateAbbr] = (
      SELECT [StateAbbr]
      FROM [Customers]
      WHERE [Id] = 'LEWISVILLE-IMPORT'
      )
    )
  AND [IsDeleted] = 0
     */
    return qb;
  }

  async getCustomFieldValuesForBmpDetail(
    customerId: string,
    bmpDetailId: number
  ): Bluebird<any[]> {
    const detailCustomFields = await this.getCustomFieldsForEntity(
      BmpDetail,
      customerId
    );

    const cfvFields = getSqlFields2(CustomFieldValue, {
      includeTable: true,
      asProperty: true,
    });
    const customFieldValues: CustomFieldValue[] = await this.knex(
      CustomFieldValue.getTableName()
    )
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where({
        [BmpControlMeasure.sqlField(f => f.isDeleted, true)]: 0,
        [BmpControlMeasure.sqlField(f => f.customerId, true)]: customerId,
        [BmpDetail.sqlField(f => f.id, true)]: bmpDetailId,
        [BmpDetail.sqlField(f => f.isDeleted, true)]: 0,
      })
      .select(cfvFields);

    const cfvGroupedByCustomFieldId = customFieldValues.reduce(
      (memo, v) => {
        memo[v.customFieldId] = v;
        return memo;
      },
      {} as { [id: string]: CustomFieldValue }
    );

    return detailCustomFields.map(cf => {
      const newValue: any = Object.assign({}, cf);
      if (cfvGroupedByCustomFieldId[cf.id]) {
        newValue.value = cfvGroupedByCustomFieldId[cf.id].value;
      } else {
        newValue.value = null;
      }
      return newValue;
    });
  }

  async updateCustomFieldsValuesForBmpDetail(
    bmpDetailId: number,
    customFieldsMap: CustomFieldValueMap[],
    customerId: string
  ): Bluebird<void> {
    const detailCustomFields = await this.getCustomFieldsForEntity(
      BmpDetail,
      customerId
    );
    const cfvFields = getSqlFields2(CustomFieldValue, {
      includeTable: true,
      asProperty: true,
    });
    // get custom field values
    const customFieldValues: CustomFieldValue[] = await this.knex(
      CustomFieldValue.getTableName()
    )
      .join(
        CustomField.getTableName(),
        CustomField.sqlField(f => f.id, true),
        CustomFieldValue.sqlField(f => f.customFieldId, true)
      )
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where({
        [CustomField.sqlField(f => f.entityType, true)]: EntityTypes.BmpDetail,
        [CustomField.sqlField(f => f.isDeleted, true)]: 0,
        [BmpDetail.sqlField(f => f.isDeleted, true)]: 0,
        [BmpDetail.sqlField(f => f.id, true)]: bmpDetailId,
        [BmpControlMeasure.sqlField(f => f.isDeleted, true)]: 0,
        [BmpControlMeasure.sqlField(f => f.customerId, true)]: customerId,
      })
      .select(cfvFields);

    await this.knex.transaction(async trx => {
      await Bluebird.each(customFieldsMap, async cfMap => {
        const customField = detailCustomFields.find(cf => cf.id === cfMap.id);
        if (!customField) {
          return; // custom field does not exist, skip
        }
        // find custom field value
        const customFieldValue = customFieldValues.find(
          v => v.customFieldId === cfMap.id
        );

        if (customFieldValue) {
          // update
          await trx(CustomFieldValue.getTableName())
            .update({
              [CustomFieldValue.sqlField(f => f.value)]: cfMap.value,
            })
            .where({
              [CustomFieldValue.sqlField(f => f.id)]: customFieldValue.id,
              [CustomFieldValue.sqlField(f => f.globalIdFk)]: this.knex(
                BmpDetail.getTableName()
              )
                .where(BmpDetail.sqlField(f => f.id), bmpDetailId)
                .select(BmpDetail.sqlField(f => f.globalId)),
            })
            .whereIn(CustomFieldValue.sqlField(f => f.customFieldId), function(
              this: Knex.QueryBuilder
            ) {
              this.from(CustomField.getTableName())
                .where({
                  [CustomField.sqlField(f => f.isDeleted)]: 0,
                  [CustomField.sqlField(f => f.customerId)]: customerId,
                })
                .select(CustomField.sqlField(f => f.id));
            })
            .transacting(trx);
        } else {
          // insert
          await trx(CustomFieldValue.getTableName()).insert({
            [CustomFieldValue.sqlField(f => f.customFieldId)]: cfMap.id,
            [CustomFieldValue.sqlField(f => f.globalIdFk)]: this.knex(
              BmpDetail.getTableName()
            )
              .where(BmpDetail.sqlField(f => f.id), bmpDetailId)
              .select(BmpDetail.sqlField(f => f.globalId)),
            [CustomFieldValue.sqlField(f => f.value)]: cfMap.value,
          });
        }
      });
    });
  }

  async getCustomFieldValuesForInvestigation(
    investigationClass: TypeOfInvestigation,
    investigationId: number,
    customerId: string
  ) {
    const investigation = await getInvestigation(
      investigationId,
      customerId,
      investigationClass,
      this.knex
    );
    const knex = this.knex;
    const cfvs = await this.knex(CustomField.tableName)
      .select(
        ...getSqlFields2(CustomField, { includeTable: true, asProperty: true }),
        CustomFieldValue.sqlField(e => e.value, true, true)
      )
      .leftJoin(CustomFieldValue.tableName, function() {
        this.on(
          CustomFieldValue.sqlField(f => f.customFieldId, true),
          CustomField.sqlField(f => f.id, true)
        ).andOn(
          CustomFieldValue.sqlField(f => f.globalIdFk, true),
          knex.raw('?', investigation.globalId)
        );
      })
      .where({
        [CustomField.sqlField(f => f.isDeleted, true)]: 0,
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.customFormTemplateId,
          true
        )]: investigation.customFormTemplateId,
      })
      .orderBy(CustomField.sqlField(f => f.fieldOrder, true));
    return cfvs;
  }

  async getCustomFieldValuesForInspection(
    Asset: TypeOfAssetWithInspection,
    AInspection: TypeOfInspection,
    assetId: number,
    inspectionId: number,
    customerId: string
  ): Bluebird<CustomFieldValue[]> {
    const knex = this.knex;

    let inspection = await getInspection(
      inspectionId,
      customerId,
      AInspection,
      this.knex
    );

    const customFieldValues = await this.knex(CustomField.tableName)
      .select(
        ...getSqlFields2(CustomField, { includeTable: true, asProperty: true }),
        CustomFieldValue.sqlField(e => e.value, true, true)
      )
      .leftOuterJoin(CustomFieldValue.tableName, function() {
        this.on(
          CustomFieldValue.sqlField(f => f.customFieldId, true),
          CustomField.sqlField(f => f.id, true)
        ).andOn(
          CustomFieldValue.sqlField(f => f.globalIdFk, true),
          knex.raw('?', inspection.globalId)
        );
      })
      .where({
        [CustomField.sqlField(f => f.isDeleted, true)]: 0,
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.customFormTemplateId,
          true
        )]: inspection.customFormTemplateId,
      })
      .orderBy(CustomField.sqlField(f => f.fieldOrder, true));

    return customFieldValues;
  }

  /**
   * Gets custom fields for for an entity type
   *
   * @param {{ new (...args)}} EntityClass
   * @param {string} customerId
   * @returns {Bluebird<CustomField[]>}
   *
   */
  getCustomFieldsForEntity(
    EntityClass: { new (...args): Entity },
    customerId: string
  ): Bluebird<CustomField[]> {
    /*
    SELECT *
    FROM CustomFields
    WHERE (
        CustomFields.customerid = 'lewisville'
        OR CustomFields.stateabbr IN (
          SELECT stateabbr
          FROM customers
          WHERE id = 'lewisville'
          )
        )
      AND CustomFields.EntityType = 'ConstructionSite'
      AND CustomFields.IsDeleted = 0
    */
    let entityType = (<any>EntityClass).name;
    let cfCustomerIdSF = getSqlField<CustomField>(
      CustomField,
      cf => cf.customerId,
      true
    );
    let cfEntityTypeSql = getSqlField<CustomField>(
      CustomField,
      cf => cf.entityType,
      true
    );
    let cfStateAbbrSF = getSqlField<CustomField>(
      CustomField,
      cf => cf.stateAbbr,
      true
    );
    let stateAbbrQb = this.knex(getTableName(Customer))
      .select(getSqlField<Customer>(Customer, f => f.stateAbbr))
      .where(getSqlField<Customer>(Customer, f => f.id), customerId);

    let qb = this.knex(this.tableName)
      .where(cfEntityTypeSql, entityType)
      .where(function() {
        this.where(cfCustomerIdSF, customerId).orWhereIn(
          cfStateAbbrSF,
          stateAbbrQb
        );
      })
      .where(getSqlField<CustomField>(CustomField, f => f.isDeleted), 0)
      .select(this.defaultSelect);
    return qb.map<any, CustomField>(o => this.mapObjectToEntityClass(o));
  }

  /**
   *
   * @param {typeof Entity} EntityClass
   * @param {number} entityId
   * @param {[{id: number, value: any}]} customFieldsMap
   * @param {any} customerId
   *
   */
  updateCustomFieldsForEntity(
    EntityClass: any,
    entityId: number,
    customFieldsMap: CustomFieldValueMap[],
    customerId: string,
    trx?: Knex.Transaction
  ): Bluebird<void> {
    let cfIdField = getSqlField<CustomField>(CustomField, cf => cf.id, true);
    let cfvIdField = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.id,
      true
    );
    let cfvCustomFieldIdField = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.customFieldId,
      true
    );
    let cfvGlobalIdFkField = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.globalIdFk,
      true
    );
    let cfvValueField = getSqlField<CustomFieldValue>(
      CustomFieldValue,
      cfv => cfv.value,
      true
    );
    let cfvTableName = getTableName(CustomFieldValue);
    let entityGlobalIdField = getSqlField(EntityClass, 'globalId', true);
    let entityIdField = getSqlField<Entity>(EntityClass, e => e.id, true);
    let entityTableName = getTableName(EntityClass);

    let combinedQueries = customFieldsMap.map(map => {
      let checkCfvExistsQb = this.knex(this.tableName)
        .join(cfvTableName, cfvCustomFieldIdField, cfIdField)
        .join(entityTableName, entityGlobalIdField, cfvGlobalIdFkField)
        .where(cfIdField, map.id)
        .where(entityIdField, entityId)
        .where(
          getSqlField<HasCustomer>(EntityClass, c => c.customerId, true),
          customerId
        )
        .select(cfvIdField);
      let insertData = {};
      insertData[cfvCustomFieldIdField] = map.id;
      insertData[cfvGlobalIdFkField] = this.knex(entityTableName)
        .select(entityGlobalIdField)
        .where(entityIdField, entityId);
      insertData[cfvValueField] = map.value;
      let insertQb = this.knex(cfvTableName).insert(insertData);
      let updateQb = this.knex(cfvTableName)
        .update(cfvValueField, map.value)
        .whereIn(cfvIdField, checkCfvExistsQb);
      let ifNotExistsRaw = this.knex
        .raw(checkCfvExistsQb.toQuery())
        .wrap('IF NOT EXISTS (', ')');
      let combinedQuery = `${ifNotExistsRaw.toQuery()} ${insertQb.toQuery()} ELSE ${updateQb.toQuery()}`;

      /**
       * Example:
       * IF NOT EXISTS (
    SELECT [CustomFieldValues].[Id]
    FROM [CustomFields]
    INNER JOIN [CustomFieldValues] ON [CustomFieldValues].[CustomFieldId] = [CustomFields].[Id]
    INNER JOIN [ConstructionSites] ON [ConstructionSites].[GlobalId] = [CustomFieldValues].[GlobalIdFK]
    WHERE [CustomFields].[Id] = 1
      AND [ConstructionSites].[Id] = 4007
      AND [ConstructionSites].[CustomerId] = 'LEWISVILLE'
    )
  INSERT INTO [CustomFieldValues] (
    [CustomFieldValues].[CustomFieldId]
    ,[CustomFieldValues].[GlobalIdFK]
    ,[CustomFieldValues].[Value]
    )
  VALUES (
    1
    ,(
      SELECT [ConstructionSites].[GlobalId]
      FROM [ConstructionSites]
      WHERE [ConstructionSites].[Id] = 4007
      )
    ,'jjj'
    )
ELSE
  UPDATE [CustomFieldValues]
  SET [CustomFieldValues].[Value] = 'jjj'
  WHERE [CustomFieldValues].[Id] IN (
      SELECT [CustomFieldValues].[Id]
      FROM [CustomFields]
      INNER JOIN [CustomFieldValues] ON [CustomFieldValues].[CustomFieldId] = [CustomFields].[Id]
      INNER JOIN [ConstructionSites] ON [ConstructionSites].[GlobalId] = [CustomFieldValues].[GlobalIdFK]
      WHERE [CustomFields].[Id] = 1
        AND [ConstructionSites].[Id] = 4007
        AND [ConstructionSites].[CustomerId] = 'LEWISVILLE'
      );

SELECT @@rowcount
       */
      return combinedQuery;
    });

    const transScope = (newTrx: Knex.Transaction) =>
      newTrx.raw(combinedQueries.join(';'));

    if (trx) {
      return transScope(trx);
    } else {
      return this.knex.transaction(transScope);
    }
  }

  async updateCustomFieldValuesForInspection(
    inspectionClass: TypeOfInspection,
    entityId: number,
    customFieldsMap: CustomFieldValueMap[],
    customerId: string
  ): Bluebird<number> {
    const inspection = await getInspection(
      entityId,
      customerId,
      inspectionClass,
      this.knex
    );
    let rowsAffected = 0;

    await this.knex.transaction(async trx => {
      // const inspection = await getInspection(entityId, customerId, inspectionClass, this.knex);
      const customFields: CustomField[] = await this.knex(CustomField.tableName)
        .where({
          [CustomField.sqlField(f => f.isDeleted, true)]: 0,
          [CustomField.sqlField(f => f.customFieldType, true)]: 'Customer',
          [CustomField.sqlField(f => f.customerId, true)]: customerId,
          [CustomField.sqlField(
            f => f.customFormTemplateId,
            true
          )]: inspection.customFormTemplateId,
        })
        .select(getSqlFields2(CustomField, { asProperty: true }));
      const cfValues: CustomFieldValue[] = await this.knex(
        CustomFieldValue.tableName
      )
        .where({
          [CustomFieldValue.sqlField(f => f.globalIdFk)]: inspection.globalId,
        })
        .select(getSqlFields2(CustomFieldValue, { asProperty: true }));

      await Bluebird.each(customFieldsMap, async cfMap => {
        const cf = customFields.find(f => f.id === cfMap.id);
        if (!cf) {
          return; // not part of CF, break
        }
        const cfv = cfValues.find(f => f.customFieldId === cfMap.id);
        if (cfv) {
          rowsAffected += await this.knex(CustomFieldValue.tableName)
            .update(CustomFieldValue.sqlField(f => f.value), cfMap.value)
            .where(CustomFieldValue.sqlField(f => f.id), cfv.id)
            .transacting(trx);
        } else {
          await this.knex(CustomFieldValue.tableName)
            .insert({
              [CustomFieldValue.sqlField(
                f => f.globalIdFk
              )]: inspection.globalId,
              [CustomFieldValue.sqlField(f => f.customFieldId)]: cfMap.id,
              [CustomFieldValue.sqlField(f => f.value)]: cfMap.value,
            })
            .transacting(trx);
          rowsAffected++;
        }
      });
    });

    return rowsAffected;
  }

  async updateCustomFieldValuesForInvestigation(
    investigationClass: TypeOfInvestigation,
    entityId: number,
    customFieldsMap: CustomFieldValueMap[],
    customerId: string
  ): Bluebird<number> {
    const investigation = await getInvestigation(
      entityId,
      customerId,
      investigationClass,
      this.knex
    );
    let rowsAffected = 0;

    await this.knex.transaction(async trx => {
      const customFields: CustomField[] = await this.knex(CustomField.tableName)
        .where({
          [CustomField.sqlField(f => f.isDeleted, true)]: 0,
          [CustomField.sqlField(f => f.customFieldType, true)]: 'Customer',
          [CustomField.sqlField(f => f.customerId, true)]: customerId,
          [CustomField.sqlField(
            f => f.customFormTemplateId,
            true
          )]: investigation.customFormTemplateId,
        })
        .select(getSqlFields2(CustomField, { asProperty: true }));
      const cfValues: CustomFieldValue[] = await this.knex(
        CustomFieldValue.tableName
      )
        .where({
          [CustomFieldValue.sqlField(
            f => f.globalIdFk
          )]: investigation.globalId,
        })
        .select(getSqlFields2(CustomFieldValue, { asProperty: true }));

      await Bluebird.each(customFieldsMap, async cfMap => {
        const cf = customFields.find(f => f.id === cfMap.id);
        if (!cf) {
          return; // not part of CF, break
        }
        const cfv = cfValues.find(f => f.customFieldId === cfMap.id);
        if (cfv) {
          rowsAffected += await this.knex(CustomFieldValue.tableName)
            .update(CustomFieldValue.sqlField(f => f.value), cfMap.value)
            .where(CustomFieldValue.sqlField(f => f.id), cfv.id)
            .transacting(trx);
        } else {
          await this.knex(CustomFieldValue.tableName)
            .insert({
              [CustomFieldValue.sqlField(
                f => f.globalIdFk
              )]: investigation.globalId,
              [CustomFieldValue.sqlField(f => f.customFieldId)]: cfMap.id,
              [CustomFieldValue.sqlField(f => f.value)]: cfMap.value,
            })
            .transacting(trx);
          rowsAffected++;
        }
      });
    });

    return rowsAffected;
  }
}

/** Gets inspection depending on the type */
async function getInspection(
  inspectionId: number,
  customerId: string,
  inspectionClass: TypeOfInspection,
  knex: Knex
) {
  if (inspectionClass === ConstructionSiteInspection) {
    return knex(ConstructionSiteInspection.tableName)
      .select(
        getSqlFields2(ConstructionSiteInspection, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .join(
        ConstructionSite.tableName,
        ConstructionSite.sqlField(f => f.id, true),
        ConstructionSiteInspection.sqlField(f => f.constructionSiteId, true)
      )
      .where({
        [ConstructionSiteInspection.sqlField(f => f.isDeleted, true)]: 0,
        [ConstructionSiteInspection.sqlField(f => f.id, true)]: inspectionId,
        [ConstructionSite.sqlField(f => f.isDeleted, true)]: 0,
        [ConstructionSite.sqlField(f => f.customerId, true)]: customerId,
      })
      .get<ConstructionSiteInspection>(0);
  } else if (inspectionClass === FacilityInspection) {
    return knex(FacilityInspection.tableName)
      .select(
        getSqlFields2(FacilityInspection, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .join(
        Facility.tableName,
        Facility.sqlField(f => f.id, true),
        FacilityInspection.sqlField(f => f.facilityId, true)
      )
      .where({
        [FacilityInspection.sqlField(f => f.isDeleted, true)]: 0,
        [FacilityInspection.sqlField(f => f.id, true)]: inspectionId,
        [Facility.sqlField(f => f.isDeleted, true)]: 0,
        [Facility.sqlField(f => f.customerId, true)]: customerId,
      })
      .get<FacilityInspection>(0);
  } else if (inspectionClass === OutfallInspection) {
    return knex(OutfallInspection.tableName)
      .select(
        getSqlFields2(OutfallInspection, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .join(
        Outfall.tableName,
        Outfall.sqlField(f => f.id, true),
        OutfallInspection.sqlField(f => f.outfallId, true)
      )
      .where({
        [OutfallInspection.sqlField(f => f.isDeleted, true)]: 0,
        [OutfallInspection.sqlField(f => f.id, true)]: inspectionId,
        [Outfall.sqlField(f => f.isDeleted, true)]: 0,
        [Outfall.sqlField(f => f.customerId, true)]: customerId,
      })
      .get<OutfallInspection>(0);
  } else if (inspectionClass === StructureInspection) {
    return knex(StructureInspection.tableName)
      .select(
        getSqlFields2(StructureInspection, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .join(
        Structure.tableName,
        Structure.sqlField(f => f.id, true),
        StructureInspection.sqlField(f => f.structureId, true)
      )
      .where({
        [StructureInspection.sqlField(f => f.isDeleted, true)]: 0,
        [StructureInspection.sqlField(f => f.id, true)]: inspectionId,
        [Structure.sqlField(f => f.isDeleted, true)]: 0,
        [Structure.sqlField(f => f.customerId, true)]: customerId,
      })
      .get<StructureInspection>(0);
  } else {
    throw Error('Unsupported inspection');
  }
}

async function getInvestigation(
  id: number,
  customerId: string,
  investigationClass: TypeOfInvestigation,
  knex: Knex
) {
  if (investigationClass === CitizenReport) {
    return knex(CitizenReport.tableName)
      .select(
        getSqlFields2(CitizenReport, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .where({
        [CitizenReport.sqlField(f => f.id, true)]: id,
        [CitizenReport.sqlField(f => f.customerId, true)]: customerId,
        [CitizenReport.sqlField(f => f.isDeleted, true)]: 0,
      })
      .get<CitizenReport>(0);
  } else if (investigationClass === IllicitDischarge) {
    return knex(IllicitDischarge.tableName)
      .select(
        getSqlFields2(IllicitDischarge, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .where({
        [IllicitDischarge.sqlField(f => f.id, true)]: id,
        [IllicitDischarge.sqlField(f => f.customerId, true)]: customerId,
        [IllicitDischarge.sqlField(f => f.isDeleted, true)]: 0,
      })
      .get<IllicitDischarge>(0);
  }
}
