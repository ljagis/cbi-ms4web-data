import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import {
  CustomFormTemplate,
  CustomFormTemplateWithCustomFields,
} from '../models';
import * as Knex from 'knex';
import { CustomFormTemplateRepository } from './custom-form-template.repository';
import { config } from '../test/helper';
import { EntityTypes } from '../shared/index';

describe('Custom Form Template Repository tests', () => {
  const templateRepo = container.get(CustomFormTemplateRepository);
  const knex = container.get<Knex>(Knex);
  const customerId = config.customerId;
  const time = new Date().getTime();

  describe(`CRUD test`, async () => {
    // TODO: complete tests
    let createdTemplate: CustomFormTemplate;

    it('should create', async () => {
      const template = new CustomFormTemplate();
      template.entityType = 'ConstructionSiteInspection';
      template.name = 'Test form 2016';
      const actualTemplate = (createdTemplate = await templateRepo.insert(
        template,
        customerId
      ));
      chai.expect(actualTemplate).is.not.null;
      chai.expect(actualTemplate.entityType).to.be.eq(template.entityType);
      chai.expect(actualTemplate.name).to.be.eq(template.name);
      chai.expect(actualTemplate.id).to.be.greaterThan(0);
    });

    it('should fetch', async () => {
      const expectedTemplates: any[] = await knex(
        CustomFormTemplate.getTableName()
      ).where({
        [CustomFormTemplate.sqlField(f => f.customerId)]: customerId,
        [CustomFormTemplate.sqlField(f => f.isArchived)]: 0,
        [CustomFormTemplate.sqlField(f => f.isDeleted)]: 0,
      });
      const actualTemplates = await templateRepo.fetch(customerId);
      chai.expect(actualTemplates).to.have.length.greaterThan(0);
      chai.expect(actualTemplates).to.have.length(expectedTemplates.length);
    });

    // cleanup
    after(async () => {
      if (createdTemplate && createdTemplate.id > 0) {
        await knex(CustomFormTemplate.getTableName())
          .del()
          .where('Id', createdTemplate.id);
      }
    });
  });

  describe(`with CustomFields`, async () => {
    let savedTemplate: CustomFormTemplateWithCustomFields;

    it('should create with customFields', async () => {
      const expectedTemplate = new CustomFormTemplateWithCustomFields();
      expectedTemplate.entityType = 'ConstructionSiteInspection';
      expectedTemplate.name = 'Test form 2016 ' + time;

      expectedTemplate.customFields = [
        {
          sectionName: 'section 1',
          customFieldType: 'Customer',
          entityType: EntityTypes.ConstructionSiteInspection,
          dataType: 'string',
          inputType: 'text',
          fieldLabel: 'Test CF ' + time,
        },
      ];

      const actualTemplate = (savedTemplate = await templateRepo.insertWithCustomFields(
        expectedTemplate,
        customerId
      ));

      chai.expect(actualTemplate).not.null;
      chai.expect(actualTemplate.entityType).eq(expectedTemplate.entityType);
      chai.expect(actualTemplate.name).eq(expectedTemplate.name);
      chai.expect(actualTemplate.id).greaterThan(0);

      chai.expect(actualTemplate.customFields).length.greaterThan(0);
      const actualCf = actualTemplate.customFields[0];
      const expectedCF = expectedTemplate.customFields[0];
      chai
        .expect(actualCf.customFormTemplateId)
        .eq(expectedCF.customFormTemplateId);
      chai.expect(actualCf.sectionName).eq(expectedCF.sectionName);
      chai.expect(actualCf.customFieldType).eq(expectedCF.customFieldType);
      chai.expect(actualCf.entityType).eq(expectedCF.entityType);
      chai.expect(actualCf.dataType).eq(expectedCF.dataType);
      chai.expect(actualCf.inputType).eq(expectedCF.inputType);
      chai.expect(actualCf.fieldLabel).eq(expectedCF.fieldLabel);
    });

    after(async () => {
      if (savedTemplate && savedTemplate.id) {
        await templateRepo.delete(customerId, savedTemplate.id, {
          force: true,
        });
      }
    });
  });
});
