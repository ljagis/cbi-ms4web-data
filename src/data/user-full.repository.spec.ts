import { expect } from 'chai';
import { container } from '../inversify.config';
import { UserFullRepository } from './user-full.repository';

describe('UserFullRepository Tests', async () => {
  const repo = container.get(UserFullRepository);

  it(`should get fetchByEmailAndCustomer`, async () => {
    const user = await repo.fetchByEmailAndCustomer(
      'anguyen@lja.com',
      'LEWISVILLE'
    );
    expect(user).ok;
    expect(user.fullName).length.greaterThan(0);
    expect(user.role).eq('super');
  });

  it(`should get fetchByCustomer`, async () => {
    const users = await repo.fetchByCustomer('LEWISVILLE');
    expect(users).ok;
    expect(users.length).greaterThan(0);
    expect(users.every(u => u.role.length > 0));
  });
});
