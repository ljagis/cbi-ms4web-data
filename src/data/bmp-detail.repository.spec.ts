import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpDetailRepository } from './bmp-detail.repository';
import { BmpDetail, BmpControlMeasure } from '../models';
import { BmpControlMeasureRepository } from './bmp-control-measure.repository';
import * as moment from 'moment';

const customerId = 'LEWISVILLE';

describe('BMP Detail Repo', async () => {
  const bmpDetailRepo = container.get(BmpDetailRepository);
  const cmRepo = container.get(BmpControlMeasureRepository);

  let createdId: number;
  let controlMeasures: BmpControlMeasure[];
  const bmpDetailName = `We will clean up stuff ${new Date().getTime()}`;

  before(async () => {
    // gets all control measures
    controlMeasures = await cmRepo.fetch(customerId);
  });

  // Cleanup
  after(async () => {
    if (createdId > 0) {
      await bmpDetailRepo.delete(customerId, createdId, { force: true });
    }
  });

  describe(`create`, async () => {
    it(`should create BMP detail`, async () => {
      const expectedDueDate = moment.utc('2017-09-01').toDate();
      const expectedCompletionDate = moment.utc('2017-09-02').toDate();
      const expectedDescription = 'description';
      const cm = controlMeasures[0];
      const bmpDetail: Partial<BmpDetail> = {
        controlMeasureId: cm.id,
        name: bmpDetailName,
        description: expectedDescription,
        dueDate: expectedDueDate,
        completionDate: expectedCompletionDate,
      };

      const returnedData = await bmpDetailRepo.insert(customerId, bmpDetail);
      createdId = returnedData.id;
      chai.expect(returnedData.id).greaterThan(0);
      chai.expect(returnedData.name).eq(bmpDetailName);
      chai.expect(returnedData.dueDate).eql(expectedDueDate);
      chai.expect(returnedData.completionDate).eql(expectedCompletionDate);
    });
  });

  describe(`fetch `, async () => {
    it(`should fetch all`, async () => {
      const bmpDetails = await bmpDetailRepo.fetch(customerId);

      const dataType = bmpDetails.find(t => t.name === bmpDetailName);

      chai.expect(bmpDetails).to.be.instanceOf(Array);
      chai.expect(bmpDetails).to.have.length.greaterThan(0);
      chai.expect(dataType.name).eq(bmpDetailName);
    });

    it(`should fetch by id`, async () => {
      chai.expect(createdId).greaterThan(0);

      const bmpDetail = await bmpDetailRepo.fetchById(customerId, createdId);

      chai.expect(bmpDetail).not.undefined;
      chai.expect(bmpDetail.name).eq(bmpDetailName);
    });
  });

  describe(`update `, async () => {
    it(`should update`, async () => {
      chai.expect(createdId).greaterThan(0);
      const expectedDetail: Partial<BmpDetail> = {
        name: bmpDetailName + 'updated',
        dueDate: new Date(2018, 1, 1),
        completionDate: new Date(2019, 2, 2),
      };
      await bmpDetailRepo.update(customerId, createdId, expectedDetail);

      const bmpDetail = await bmpDetailRepo.fetchById(customerId, createdId);

      chai.expect(bmpDetail.name).eq(expectedDetail.name);
      chai.expect(bmpDetail.dueDate).eql(expectedDetail.dueDate);
      chai.expect(bmpDetail.completionDate).eql(expectedDetail.completionDate);
    });
  });

  describe(`soft delete`, async () => {
    it(`should soft delete`, async () => {
      chai.expect(createdId).greaterThan(0);
      await bmpDetailRepo.delete(customerId, createdId);

      const dataType = await bmpDetailRepo.fetchById(customerId, createdId);
      chai.expect(dataType).not.ok;
    });
  });
});
