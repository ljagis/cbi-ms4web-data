import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { CustomerRepository } from './customer.repository';

describe('Tests #CustomerRepository', async () => {
  const repo = container.get(CustomerRepository);
  const stamp = Date.now();
  const customerId = `TEST_${stamp}`;

  describe('create customer', async () => {
    it('should create customer', async () => {
      const createdCustomer = await repo.create({
        id: customerId,
        fullName: 'Customer for unit tests',
        stateAbbr: 'TX',
        features: {
          calendar: true,
          fileGallery: true,
          bmps: true,
          swmp: true,
          reports: true,
          constructionSites: true,
          outfalls: true,
          structuralControls: true,
          facilities: true,
          illicitDischarges: true,
          citizenReports: true,
          extraLayers: true,
          customFormTemplates: true,
          exporting: true,
          customFields: true,
          swmpAnnualReport: false,
        },
      });

      chai.expect(createdCustomer).ok;
      chai.expect(createdCustomer.features.calendar).true;
    });
  });

  describe(`fetch tests`, async () => {
    it(`should fetch by id`, async () => {
      const customer = await repo.fetchById(customerId);
      chai.expect(customer).ok;
      chai.expect(customer.features).ok;
    });
  });

  // cleanup
  after(async () => {
    describe('cleanup', async () => {
      it('should clean up', async () => {
        const rowsAffected = await repo.delete(customerId, 0);
        chai.expect(rowsAffected).eq(1);
      });
    });
  });
});
