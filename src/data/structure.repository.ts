import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  Community,
  Contact,
  CustomField,
  CustomFieldValue,
  EntityContact,
  InspectionFrequency,
  Project,
  ReceivingWater,
  Structure,
  StructureQueryOptions,
  Watershed,
} from '../models';

import { AssetRepository } from './asset.repository';
import { Defaults } from '../shared';
import { EntityTypes } from '../shared/entity-types';
import { FetchAsset } from '../interfaces';
import { getSqlFields2 } from '../decorators';
import { injectable } from 'inversify';

@injectable()
export class StructureRepository extends AssetRepository<Structure>
  implements FetchAsset<Structure> {
  constructor() {
    super(Structure);
  }

  async fetch(
    customerId: string,
    queryOptions?: StructureQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<Structure[]> {
    return await this.getFetchQueryBuilder(
      customerId,
      queryOptions,
      queryBuilderOverrides
    ).map(e => this.mapObjectToEntityClass(e));
  }

  async fetchById(
    id: number,
    customerId: string,
    queryOptions?: StructureQueryOptions
  ): Bluebird<Structure> {
    const queryBuilder = this.getFetchQueryBuilder(
      customerId,
      queryOptions
    ).where(Structure.sqlField(f => f.id, true), id);
    return await queryBuilder
      .map<Structure, Structure>(asset => this.mapObjectToEntityClass(asset))
      .get<Structure>(0); // first
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      Structure.sqlField(c => c.id, true, true),
      Structure.sqlField(c => c.dateAdded, true, true),
      Structure.sqlField(c => c.name, true, true),
      Structure.sqlField(c => c.controlType, true, true),
      Structure.sqlField(c => c.trackingId, true, true),
      Structure.sqlField(c => c.complianceStatus, true, true),
      Structure.sqlField(c => c.projectId, true, true),
      `${Project.sqlField(p => p.name, true)} as projectName`,
      Structure.sqlField(c => c.projectType, true, true),
      Structure.sqlField(c => c.physicalAddress1, true, true),
      Structure.sqlField(c => c.physicalAddress2, true, true),
      Structure.sqlField(c => c.physicalCity, true, true),
      Structure.sqlField(c => c.physicalState, true, true),
      Structure.sqlField(c => c.physicalZip, true, true),
      Structure.sqlField(c => c.permitStatus, true, true),
      Structure.sqlField(c => c.inspectionFrequencyId, true, true),
      `${InspectionFrequency.sqlField(
        p => p.name,
        true
      )} as inspectionFrequencyName`,
      Structure.sqlField(c => c.maintenanceAgreement, true, true),
      Structure.sqlField(c => c.lat, true, true),
      Structure.sqlField(c => c.lng, true, true),
      Structure.sqlField(c => c.additionalInformation, true, true),
      Structure.sqlField(c => c.latestInspectionDate, true, true),
      Structure.sqlField(c => c.communityId, true, true),
      `${Community.sqlField(c => c.name, true)} as communityName`,
      Structure.sqlField(c => c.watershedId, true, true),
      `${Watershed.sqlField(c => c.name, true)} as watershedName`,
      Structure.sqlField(c => c.subwatershedId, true, true),
      `SubWatershed.${Watershed.sqlField(c => c.name)} as subWatershedName`,
      Structure.sqlField(c => c.receivingWatersId, true, true),
      `${ReceivingWater.sqlField(c => c.name, true)} as receivingWaterName`,
    ];
    return await this.knex(this.tableName)
      .leftOuterJoin(
        InspectionFrequency.getTableName(),
        InspectionFrequency.sqlField(f => f.id, true),
        Structure.sqlField(s => s.inspectionFrequencyId, true)
      )
      .leftOuterJoin(
        Project.getTableName(),
        Project.sqlField(p => p.id, true),
        Structure.sqlField(c => c.projectId, true)
      )
      .leftOuterJoin(
        Community.getTableName(),
        Community.sqlField(p => p.id, true),
        Structure.sqlField(c => c.communityId, true)
      )
      .leftOuterJoin(
        Watershed.getTableName(),
        Watershed.sqlField(p => p.id, true),
        Structure.sqlField(c => c.watershedId, true)
      )
      .leftOuterJoin(
        `${Watershed.getTableName()} as SubWatershed`,
        `SubWatershed.${Watershed.sqlField(p => p.id)}`,
        Structure.sqlField(c => c.subwatershedId, true)
      )
      .leftOuterJoin(
        ReceivingWater.getTableName(),
        ReceivingWater.sqlField(p => p.id, true),
        Structure.sqlField(c => c.receivingWatersId, true)
      )
      .where(Structure.sqlField(c => c.customerId, true), customerId)
      .where(Structure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${Structure.sqlField(c => c.id, true)} as structureId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        Structure.getTableName(),
        Structure.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.Structure
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(Structure.sqlField(f => f.isDeleted, true), 0)
      .where(Structure.sqlField(f => f.customerId, true), customerId)
      .orderBy(Structure.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true));
    return await qb;
  }

  async createSampleStructure(customerId: string): Bluebird<Structure> {
    const sampleAsset: Structure = {
      ...new Structure(),
      customerId,
      name: 'Sample Structural Control',
      complianceStatus: 'Compliant',
      lat: 29.766115, // Near U of H
      lng: -95.361096,
    };
    const data = this.objectToSqlData(Structure, sampleAsset, this.knex);
    const returnFields = getSqlFields2(Structure, { asProperty: true });
    const returnedData = await this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return returnedData;
  }

  /**
   * PRIVATES ---
   */

  private getFetchQueryBuilder(
    customerId: string,
    queryOptions?: StructureQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;

    let queryBuilder = this.knex(this.tableName)
      .select(
        !queryOptions.includeLastInspector
          ? this.defaultSelect
          : [
              ...this.defaultSelect,
              'LastInspection.InspectorId AS inspectorId',
              'LastInspection.InspectorId2 AS inspectorId2',
            ]
      )
      .limit(limit)
      .offset(queryOptions.offset);

    if (queryOptions.ids && queryOptions.ids.length) {
      queryBuilder.whereIn(
        Structure.sqlField(s => s.id, true),
        queryOptions.ids
      );
    }

    if (queryOptions.contractorEmail) {
      queryBuilder = this._appendContractorEmail(
        queryBuilder,
        queryOptions.contractorEmail
      );
    }

    if (queryOptions.includeLastInspector) {
      queryBuilder = this._appendLastInspector(queryBuilder);
    }

    if (queryOptions.addedFrom || queryOptions.addedTo) {
      queryBuilder.where(function() {
        // added from, to
        if (queryOptions.addedFrom || queryOptions.addedTo) {
          this.orWhere(function() {
            if (queryOptions.addedFrom) {
              this.where('DateAdded', '>=', queryOptions.addedFrom);
            }
            if (queryOptions.addedTo) {
              this.where('DateAdded', '<=', queryOptions.addedTo);
            }
          });
        }
      });
    }

    if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
      queryBuilder.where(function() {
        if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
          this.orWhere(function() {
            if (queryOptions.inspectedFrom) {
              this.where(
                'LatestInspectionDate',
                '>=',
                queryOptions.inspectedFrom
              );
            }
            if (queryOptions.inspectedTo) {
              this.where(
                'LatestInspectionDate',
                '<=',
                queryOptions.inspectedTo
              );
            }
          });
        }
      });
    }

    if (queryOptions.orderByRaw) {
      queryBuilder.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSqlFull, queryOptions.orderByDirection);
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder
      .where(this.getSqlField(e => e.customerId, true), customerId)
      .where(this.getSqlField(e => e.isDeleted, true), 0);
    return queryBuilder;
  }

  private _appendContractorEmail(qb: Knex.QueryBuilder, email: string) {
    return qb
      .join(
        EntityContact.tableName,
        EntityContact.sqlField(f => f.globalIdFk, true),
        Structure.sqlField(f => f.globalId, true)
      )
      .join(
        Contact.tableName,
        Contact.sqlField(f => f.id, true),
        EntityContact.sqlField(f => f.contactId, true)
      )
      .where({
        [Contact.sqlField(f => f.email, true)]: email,
        [Contact.sqlField(f => f.isDeleted, true)]: 0,
      });
  }

  private _appendLastInspector(qb: Knex.QueryBuilder) {
    return qb.joinRaw(`
        LEFT JOIN
          (
            SELECT MIN(SI1.Id) AS Id, SI1.StructureId
            FROM StructureInspections SI1
            JOIN
              (
                SELECT StructureId, MAX(InspectionDate) AS latestDate
                FROM StructureInspections
                GROUP BY StructureId
              ) SI2
              ON SI1.StructureId = SI2.StructureId
                AND SI1.InspectionDate = SI2.latestDate
            GROUP BY SI1.StructureId
          ) LID
          ON LID.StructureId = Structures.Id
        LEFT JOIN StructureInspections LastInspection
          ON LastInspection.Id = LID.Id
      `);
  }
}
