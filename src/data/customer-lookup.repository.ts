import * as Knex from 'knex';
import { Repository } from './repository';
import {
  HasCustomer,
  BmpActivity,
  Contact,
  Community,
  Project,
  ReceivingWater,
  StructureControlType,
  Watershed,
  Entity,
  QueryOptions,
} from '../models';
import { injectable, unmanaged } from 'inversify';
import { getSqlField, getSqlFields, getSqlFields2 } from '../decorators';
import * as Bluebird from 'bluebird';

type Lookup =
  | BmpActivity
  | Contact
  | Community
  | Project
  | ReceivingWater
  | StructureControlType
  | Watershed;

@injectable()
export abstract class CustomerLookupRepository<
  T extends Entity & HasCustomer
> extends Repository<T> {
  constructor(@unmanaged() entity: any) {
    super(entity);
  }

  /**
   * * Generic method to get a lookups of type T
   *
   * @param {string} customer
   * @param {QueryOptions} queryOptions
   * @param {((qb: Knex.QueryBuilder) => Knex.QueryBuilder)} [queryBuilderOverrides] additional queryBuilders
   * @returns {Bluebird<T[]>}
   *
   * @memberOf LookupRepository
   */
  fetch(
    customer: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<T[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder = queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder = queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder = queryBuilder.orderBy(
        this.idFieldSql,
        queryOptions.orderByDirection
      );
    }
    queryBuilder = queryBuilder.where(
      this.getSqlField(t => t.customerId),
      customer
    );
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map<T, T>(l => this.mapObjectToEntityClass(l));
  }

  fetchById(
    id: number,
    customer: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<T> {
    let queryBuilder = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id)
      .where(this.getSqlField(t => t.customerId), customer);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder
      .map<T, T>(l => this.mapObjectToEntityClass(l))
      .get<T>(0);
  }

  insert(lookup: T, customerId: string): Bluebird<T> {
    lookup.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(lookup);
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return queryBuilder.map(e => this.mapObjectToEntityClass(e)).get<T>(0);
  }

  update(id: number, lookup: T, customerId: string): Bluebird<T> {
    const data = this.mapObjectToSql(lookup);
    const returnFields = getSqlFields2(this.entity, { asProperty: true });
    const qb = this.knex(this.tableName)
      .update(data)
      .where(getSqlField<Lookup>(this.entity, c => c.id), id)
      .where(getSqlField<Lookup>(this.entity, c => c.customerId), customerId)
      .returning(returnFields)
      .get<T>(0);
    return qb;
  }

  del(id: number, customerId: string) {
    let qb = this.knex(this.tableName)
      .del()
      .where(getSqlField<Lookup>(this.entity, c => c.id), id)
      .where(getSqlField<Lookup>(this.entity, c => c.customerId), customerId);
    return qb;
  }
}
