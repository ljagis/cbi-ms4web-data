import { CustomerLookupRepository } from './customer-lookup.repository';
import { Watershed, WatershedQueryOptions } from '../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';
@injectable()
export class WatershedRepository extends CustomerLookupRepository<Watershed> {
  constructor() {
    super(Watershed);
  }

  fetch(
    customer: string,
    queryOptions?: WatershedQueryOptions
  ): Bluebird<Watershed[]> {
    queryOptions = queryOptions || {};
    return super.fetch(customer, queryOptions, qb => {
      if (
        queryOptions.isSubWatershed === true ||
        queryOptions.isSubWatershed === false
      ) {
        qb = qb.where(
          this.getSqlField('isSubWatershed'),
          queryOptions.isSubWatershed ? 1 : 0
        );
      }
      return qb;
    });
  }
}
