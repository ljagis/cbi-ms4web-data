import { Repository } from './repository';
import {
  Entity,
  Facility,
  FacilityInspection,
  ConstructionSite,
  ConstructionSiteInspection,
  ConstructionSiteInspectionQueryOptions,
  HasCustomer,
  Outfall,
  OutfallInspection,
  Structure,
  StructureInspection,
  Inspection,
  InspectionQueryOptions,
  InspectionType,
  CustomFieldValueMap,
} from '../models';

import * as Knex from 'knex';

import { unmanaged, inject } from 'inversify';
import { createClassFromObject, Defaults } from '../shared';
import * as Bluebird from 'bluebird';
import {
  getSqlField,
  getGlobalIdField,
  getTableName,
  getSqlFields,
  getIdField,
} from '../decorators';
import { CustomFieldRepository } from './custom-field.repository';

type AssetInspection =
  | ConstructionSiteInspection
  | FacilityInspection
  | OutfallInspection
  | StructureInspection;

/**
 * Base class for Asset Inspection
 *
 * @export
 * @class AssetInspectionRepository
 * @extends {Repository<T>}
 * @template T
 */
export class AssetInspectionRepository<
  TAsset extends Entity & HasCustomer,
  TInspection extends Inspection
> extends Repository<TInspection> {
  protected globalIdField: string;
  protected globalIdFieldSql: string;

  protected Asset: any;

  private _assetIdFieldFull: string; // eg: ConstructionSite.Id
  private _assetInspectionAssetFKFieldFull: string; // e.g. ConstructionSiteInspections.ConstructionSiteId
  private _assetCustomerIdFieldFull: string; // e.g. ConstructionSite.CustomerId

  @inject(CustomFieldRepository) private _cfRepo: CustomFieldRepository;

  constructor(@unmanaged() entity: any) {
    super(entity);

    this.globalIdField = getGlobalIdField(this.entity);
    this.globalIdFieldSql = getSqlField(this.entity, this.globalIdField);

    this.defaultSelect = [
      ...this.defaultSelect,
      `${InspectionType.sqlField(f => f.name, true)} as inspectionType`,
    ];

    if (this.entity.name === ConstructionSiteInspection.name) {
      this.Asset = ConstructionSite;
      this._assetInspectionAssetFKFieldFull = ConstructionSiteInspection.sqlField(
        i => i.constructionSiteId,
        true
      );
    } else if (this.entity.name === FacilityInspection.name) {
      this.Asset = Facility;
      this._assetInspectionAssetFKFieldFull = FacilityInspection.sqlField(
        i => i.facilityId,
        true
      );
    } else if (this.entity.name === OutfallInspection.name) {
      this.Asset = Outfall;
      this._assetInspectionAssetFKFieldFull = OutfallInspection.sqlField(
        i => i.outfallId,
        true
      );
    } else if (this.entity.name === StructureInspection.name) {
      this.Asset = Structure;
      this._assetInspectionAssetFKFieldFull = StructureInspection.sqlField(
        i => i.structureId,
        true
      );
    } else {
      throw Error('Invalid Asset Inspection type');
    }

    this._assetIdFieldFull = getSqlField(
      this.Asset,
      getIdField(this.Asset),
      true
    ); // ConstructionSite.Id
    this._assetCustomerIdFieldFull = getSqlField<TAsset>(
      this.Asset,
      a => a.customerId,
      true
    );
  }

  fetch(
    customerId: string,
    assetId?: number,
    queryOptions?: InspectionQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<TInspection[]> {
    queryOptions = queryOptions || {};

    let queryBuilder = this.fetchBaseQueryBuilder(
      customerId,
      assetId,
      queryOptions
    ).select(queryOptions.outFields || this.defaultSelect);

    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map(i =>
      createClassFromObject<TInspection>(this.entity as any, i)
    );
  }

  /**
   * Fetches inspections without mapping to inspection class (for joins)
   *
   * @param {string} customerId
   * @param {number} [assetId]
   * @param {InspectionQueryOptions} [queryOptions]
   * @param {((qb: Knex.QueryBuilder) => Knex.QueryBuilder)} [queryBuilderOverrides]
   * @returns {Knex.QueryBuilder}
   *
   */
  fetchWithoutMap(
    customerId: string,
    assetId?: number,
    queryOptions?: InspectionQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    let queryBuilder = this.fetchBaseQueryBuilder(
      customerId,
      assetId,
      queryOptions
    ).select(queryOptions.outFields || this.defaultSelect);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder;
  }

  fetchCount(
    customerId: string,
    assetId?: number,
    queryOptions?: InspectionQueryOptions
  ): Bluebird<number> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.fetchBaseQueryBuilder(
      customerId,
      assetId,
      queryOptions
    ).count('* as total');
    return queryBuilder.then(rs => rs[0]).get<number>('total');
  }

  fetchById(
    id: number,
    assetId: number,
    customerId: string
  ): Bluebird<TInspection> {
    let queryBuilder = this.fetchBaseQueryBuilder(customerId, assetId)
      .select(this.defaultSelect)
      .where(this.idFieldSqlFull, id);

    return queryBuilder
      .map(i => this.mapObjectToEntityClass(i))
      .get<TInspection>(0);
  }

  fetchParentInspections(
    id: number,
    assetId: number,
    customerId: string
  ): Bluebird<TInspection[]> {
    return this.fetchBaseQueryBuilder(customerId, assetId)
      .select(this.defaultSelect)
      .where(Inspection.sqlField(i => i.followUpInspectionId), id)
      .map(i => this.mapObjectToEntityClass(i));
  }

  async getLatestInspection(assetId: number, customerId: string) {
    return this.knex(this.tableName)
      .limit(1)
      .join(
        getTableName(this.Asset),
        this._assetInspectionAssetFKFieldFull,
        '=',
        this._assetIdFieldFull
      )
      .where(this._assetCustomerIdFieldFull, customerId)
      .where(getSqlField<Inspection>(this.entity, a => a.isDeleted, true), 0)
      .whereNot(
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true),
        null
      )
      .where(this._assetIdFieldFull, assetId)
      .orderBy(
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true),
        'desc'
      )
      .select([
        getSqlField<Inspection>(this.entity, a => a.id, true, true),
        getSqlField<Inspection>(
          this.entity,
          a => a.complianceStatus,
          true,
          true
        ),
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true, true),
      ])
      .get<Pick<TInspection, 'id' | 'complianceStatus' | 'inspectionDate'>>(0);
  }

  async getPreviousInspection(
    assetId: number,
    customerId: string,
    inspectionDate: Date
  ) {
    return this.knex(this.tableName)
      .limit(1)
      .join(
        getTableName(this.Asset),
        this._assetInspectionAssetFKFieldFull,
        '=',
        this._assetIdFieldFull
      )
      .where(this._assetCustomerIdFieldFull, customerId)
      .where(getSqlField<Inspection>(this.entity, a => a.isDeleted, true), 0)
      .where(this._assetIdFieldFull, assetId)
      .where(
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true),
        '<',
        inspectionDate
      )
      .orderBy(
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true),
        'desc'
      )
      .select([
        getSqlField<Inspection>(this.entity, a => a.inspectionDate, true, true),
      ])
      .get<Pick<TInspection, 'inspectionDate'>>(0);
  }

  async insert(
    assetInspection: AssetInspection,
    customerId: string
  ): Bluebird<TInspection> {
    const data = this.mapObjectToSql(assetInspection);
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    const inspection = await queryBuilder
      .map(i => this.mapObjectToEntityClass(i))
      .get<TInspection>(0);

    const cfs = await this._cfRepo.fetchByFormTemplateId(
      customerId,
      inspection.customFormTemplateId
    );
    const cfMap = cfs.map(
      cf => ({ id: cf.id, value: cf.defaultValue } as CustomFieldValueMap)
    );
    await this._cfRepo.updateCustomFieldValuesForInspection(
      this.entity as any,
      inspection.id,
      cfMap,
      customerId
    );

    return inspection;
  }

  update(id: number, inspection: any, customerId: string): Bluebird<void> {
    const data = this.mapObjectToSql(inspection);
    let selectAssetIdSubQb = this.knex(getTableName(this.Asset))
      .select(getSqlField<Entity>(this.Asset, a => a.id))
      .where(getSqlField<TAsset>(this.Asset, a => a.customerId), customerId);
    const qb = this.knex(this.tableName)
      .update(data)
      .whereIn(this._assetInspectionAssetFKFieldFull, selectAssetIdSubQb)
      .where(this.idField, id);
    return qb;
  }

  /**
   * "Deletes" an inspection by setting inspection's `isDeleted` to true
   * @param {number} id Inspection Id
   * @param {number} assetId Asset Id
   * @param {string} customerId
   *
   */
  deleteInspection(
    id: number,
    assetId: number,
    customerId: string
  ): Bluebird<void> {
    return this.deleteByIdQueryBuilder(id, assetId, customerId);
  }

  /**
   * Returns the query builder for delete deleting an individual inspection id
   *
   * @param {number} id
   * @param {number} assetId
   * @param {string} customerId
   *
   */
  deleteByIdQueryBuilder(
    id: number,
    assetId: number,
    customerId: string
  ): Knex.QueryBuilder {
    return this.deleteInspectionsQueryBuilder(assetId, customerId).where(
      this.idFieldSql,
      id
    );
  }

  /**
   * Returns a QueryBuilder that deletes inspections based on asset id
   *
   * @param {number} assetId
   * @param {string} customerId
   * @returns {knex.QueryBuilder}
   *
   * @memberOf AssetInspectionRepository
   */
  deleteInspectionsQueryBuilder(
    assetId: number,
    customerId: string
  ): Knex.QueryBuilder {
    let whereInSubquery = this.knex(getTableName(this.Asset))
      .select(this._assetIdFieldFull)
      .where(this._assetIdFieldFull, assetId)
      .where(this._assetCustomerIdFieldFull, customerId);
    return this.knex(this.tableName)
      .update(getSqlField<Inspection>(this.entity, a => a.isDeleted, true), 1)
      .whereIn(this._assetInspectionAssetFKFieldFull, whereInSubquery);
  }

  /**
   * Default fetch query builder that gets inspections that are not deleted by customer
   *
   * @param {string} customerId
   * @param {number} [assetId]
   * @param {InspectionQueryOptions} [queryOptions]
   */
  fetchBaseQueryBuilder(
    customerId: string,
    assetId?: number,
    queryOptions?: InspectionQueryOptions
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;
    const inspectionTypeIdFull = getSqlField<Inspection>(
      this.entity,
      f => f.inspectionTypeId,
      true
    );
    let baseQb = this.knex(this.tableName)
      .join(
        getTableName(this.Asset),
        this._assetInspectionAssetFKFieldFull,
        '=',
        this._assetIdFieldFull
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(f => f.id, true),
        inspectionTypeIdFull
      )
      .where(this._assetCustomerIdFieldFull, customerId)
      .where(getSqlField<Inspection>(this.entity, a => a.isDeleted, true), 0);

    if (
      (<ConstructionSiteInspectionQueryOptions>queryOptions)
        .constructionSiteProjectStatus
    ) {
      let projectStatus = (<ConstructionSiteInspectionQueryOptions>queryOptions)
        .constructionSiteProjectStatus;
      baseQb.where(
        getSqlField<ConstructionSite>(ConstructionSite, cs => cs.projectStatus),
        projectStatus
      );
    }

    if (assetId) {
      baseQb.where(this._assetIdFieldFull, assetId);
    }

    const entityClass = this.entity;

    if (
      queryOptions.inspectionDateFrom ||
      queryOptions.inspectionDateTo ||
      queryOptions.scheduledInspectionDateFrom ||
      queryOptions.scheduledInspectionDateTo
    ) {
      baseQb.where(function() {
        // inspection date
        if (queryOptions.inspectionDateFrom || queryOptions.inspectionDateTo) {
          this.orWhere(function() {
            if (queryOptions.inspectionDateFrom) {
              this.where(
                getSqlField<Inspection>(
                  entityClass,
                  i => i.inspectionDate,
                  true
                ),
                '>=',
                queryOptions.inspectionDateFrom
              );
            }
            if (queryOptions.inspectionDateTo) {
              this.where(
                getSqlField<Inspection>(
                  entityClass,
                  i => i.inspectionDate,
                  true
                ),
                '<=',
                queryOptions.inspectionDateTo
              );
            }
          });
        }
        // scheduled inspection date
        if (
          queryOptions.scheduledInspectionDateFrom ||
          queryOptions.scheduledInspectionDateTo
        ) {
          this.orWhere(function() {
            if (queryOptions.scheduledInspectionDateFrom) {
              this.where(
                getSqlField<Inspection>(
                  entityClass,
                  i => i.scheduledInspectionDate,
                  true
                ),
                '>=',
                queryOptions.scheduledInspectionDateFrom
              );
            }
            if (queryOptions.scheduledInspectionDateTo) {
              this.where(
                getSqlField<Inspection>(
                  entityClass,
                  i => i.scheduledInspectionDate,
                  true
                ),
                '<=',
                queryOptions.scheduledInspectionDateTo
              );
            }
            if (queryOptions.staffInspection) {
              this.whereNull(
                getSqlField<Inspection>(
                  entityClass,
                  i => i.inspectionDate,
                  true
                )
              );
            }
          });
        }
        // --
      });
    }

    if (queryOptions.limit) {
      baseQb.limit(limit);
    }
    if (queryOptions.offset) {
      baseQb.offset(queryOptions.offset);
    }

    if (queryOptions.orderByRaw) {
      baseQb.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      baseQb.orderBy(
        this.getSqlField(queryOptions.orderByField as any, true),
        queryOptions.orderByDirection
      );
    } else {
      baseQb.orderBy(this.idFieldSql, queryOptions.orderByDirection);
    }

    return baseQb;
  }
}
