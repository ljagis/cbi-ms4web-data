import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpTaskRepository } from './bmp-task.repository';

describe('BMP Task Repository Tests', () => {
  const bmpTaskRepo = container.get(BmpTaskRepository);

  describe(`exportData tests #BmpTaskRepository #exportData`, () => {
    it(`should export data`, async () => {
      const customerId = 'LEWISVILLE';
      const tasks = await bmpTaskRepo.exportData(customerId);

      const task = tasks[0];

      chai.expect(tasks).to.be.instanceOf(Array);
      chai.expect(tasks).to.have.length.greaterThan(0);

      chai.expect(task).to.have.property('id');
      chai.expect(task).to.have.property('name');
      chai.expect(task).to.have.property('controlMeasureId');
      chai.expect(task).to.have.property('controlMeasureName');
    });
  });
});
