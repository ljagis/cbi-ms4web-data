import { Repository } from './repository';
import { MG } from '../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';

@injectable()
export class MgRepository extends Repository<MG> {
  constructor() {
    super(MG);
  }

  async create(mg: Partial<MG>) {
    const returnFields = this.getSelectFields();
    const createdMg = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(mg))
      .returning(returnFields)
      .get<MG>(0);

    return createdMg;
  }

  async fetchByBmpId(bmpId: number): Bluebird<MG[]> {
    return await this.knex(this.tableName)
      .where(MG.sqlField(m => m.bmpId), bmpId)
      .select(this.defaultSelect)
      .orderBy(MG.sqlField(m => m.number));
  }

  async fetchByBmpIds(bmpIds: number[]): Bluebird<MG[]> {
    return await this.knex(this.tableName)
      .whereIn(MG.sqlField(m => m.bmpId), bmpIds)
      .select(this.defaultSelect)
      .orderBy(MG.sqlField(m => m.number));
  }

  async update(mg: Partial<MG>) {
    const qb = this.knex(this.tableName)
      .update(this.mapObjectToSql(mg))
      .where(MG.sqlField(m => m.id), mg.id);

    return qb;
  }

  async delete(id: number) {
    const qb = this.knex(this.tableName)
      .where(MG.sqlField(m => m.id), id)
      .delete();

    return qb;
  }
}
