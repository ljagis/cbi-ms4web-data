import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  Community,
  Contact,
  CustomField,
  CustomFieldValue,
  EntityContact,
  Outfall,
  OutfallQueryOptions,
  ReceivingWater,
  Watershed,
} from '../models';

import { AssetRepository } from './asset.repository';
import { Defaults } from '../shared';
import { EntityTypes } from '../shared/entity-types';
import { FetchAsset } from '../interfaces';
import { getSqlFields2 } from '../decorators';
import { injectable } from 'inversify';

@injectable()
export class OutfallRepository extends AssetRepository<Outfall>
  implements FetchAsset<Outfall> {
  constructor() {
    super(Outfall);
  }

  async fetch(
    customerId: string,
    queryOptions?: OutfallQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<Outfall[]> {
    return await this.getFetchQueryBuilder(
      customerId,
      queryOptions,
      queryBuilderOverrides
    ).map(e => this.mapObjectToEntityClass(e));
  }

  async fetchById(
    id: number,
    customerId: string,
    queryOptions?: OutfallQueryOptions
  ): Bluebird<Outfall> {
    const queryBuilder = this.getFetchQueryBuilder(
      customerId,
      queryOptions
    ).where(Outfall.sqlField(f => f.id, true), id);
    return await queryBuilder
      .map<Outfall, Outfall>(asset => this.mapObjectToEntityClass(asset))
      .get<Outfall>(0); // first
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      Outfall.sqlField(c => c.id, true, true),
      Outfall.sqlField(c => c.dateAdded, true, true),
      Outfall.sqlField(c => c.location, true, true),

      Outfall.sqlField(c => c.nearAddress, true, true),
      Outfall.sqlField(c => c.trackingId, true, true),
      Outfall.sqlField(c => c.complianceStatus, true, true),

      Outfall.sqlField(c => c.lat, true, true),
      Outfall.sqlField(c => c.lng, true, true),

      Outfall.sqlField(c => c.condition, true, true),
      Outfall.sqlField(c => c.material, true, true),
      Outfall.sqlField(c => c.obstruction, true, true),
      Outfall.sqlField(c => c.outfallType, true, true),

      Outfall.sqlField(c => c.outfallSizeIn, true, true),
      Outfall.sqlField(c => c.outfallWidthIn, true, true),
      Outfall.sqlField(c => c.outfallHeightIn, true, true),

      Outfall.sqlField(c => c.additionalInformation, true, true),

      Outfall.sqlField(c => c.latestInspectionDate, true, true),

      Outfall.sqlField(c => c.communityId, true, true),
      `${Community.sqlField(c => c.name, true)} as communityName`,
      Outfall.sqlField(c => c.watershedId, true, true),
      `${Watershed.sqlField(c => c.name, true)} as watershedName`,
      Outfall.sqlField(c => c.subwatershedId, true, true),
      `SubWatershed.${Watershed.sqlField(c => c.name)} as subWatershedName`,
      Outfall.sqlField(c => c.receivingWatersId, true, true),
      `${ReceivingWater.sqlField(c => c.name, true)} as receivingWaterName`,
    ];
    return await this.knex(this.tableName)
      .leftOuterJoin(
        Community.getTableName(),
        Community.sqlField(p => p.id, true),
        Outfall.sqlField(c => c.communityId, true)
      )
      .leftOuterJoin(
        Watershed.getTableName(),
        Watershed.sqlField(p => p.id, true),
        Outfall.sqlField(c => c.watershedId, true)
      )
      .leftOuterJoin(
        `${Watershed.getTableName()} as SubWatershed`,
        `SubWatershed.${Watershed.sqlField(p => p.id)}`,
        Outfall.sqlField(c => c.subwatershedId, true)
      )
      .leftOuterJoin(
        ReceivingWater.getTableName(),
        ReceivingWater.sqlField(p => p.id, true),
        Outfall.sqlField(c => c.receivingWatersId, true)
      )
      .where(Outfall.sqlField(c => c.customerId, true), customerId)
      .where(Outfall.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${Outfall.sqlField(c => c.id, true)} as outfallId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        Outfall.getTableName(),
        Outfall.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(CustomField.sqlField(f => f.entityType, true), EntityTypes.Outfall)
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(Outfall.sqlField(f => f.isDeleted, true), 0)
      .where(Outfall.sqlField(f => f.customerId, true), customerId)
      .orderBy(Outfall.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true));
    return await qb;
  }

  private getFetchQueryBuilder(
    customerId: string,
    queryOptions?: OutfallQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;

    let queryBuilder = this.knex(this.tableName)
      .select(
        !queryOptions.includeLastInspector
          ? this.defaultSelect
          : [
              ...this.defaultSelect,
              'LastInspection.InspectorId AS inspectorId',
              'LastInspection.InspectorId2 AS inspectorId2',
            ]
      )
      .limit(limit)
      .offset(queryOptions.offset);

    if (queryOptions.ids && queryOptions.ids.length) {
      queryBuilder.whereIn(Outfall.sqlField(o => o.id, true), queryOptions.ids);
    }

    if (queryOptions.contractorEmail) {
      queryBuilder = this._appendContractorEmail(
        queryBuilder,
        queryOptions.contractorEmail
      );
    }

    if (queryOptions.includeLastInspector) {
      queryBuilder = this._appendLastInspector(queryBuilder);
    }

    if (queryOptions.addedFrom || queryOptions.addedTo) {
      queryBuilder.where(function() {
        // added from, to
        if (queryOptions.addedFrom || queryOptions.addedTo) {
          this.orWhere(function() {
            if (queryOptions.addedFrom) {
              this.where('DateAdded', '>=', queryOptions.addedFrom);
            }
            if (queryOptions.addedTo) {
              this.where('DateAdded', '<=', queryOptions.addedTo);
            }
          });
        }
      });
    }

    if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
      queryBuilder.where(function() {
        if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
          this.orWhere(function() {
            if (queryOptions.inspectedFrom) {
              this.where(
                'LatestInspectionDate',
                '>=',
                queryOptions.inspectedFrom
              );
            }
            if (queryOptions.inspectedTo) {
              this.where(
                'LatestInspectionDate',
                '<=',
                queryOptions.inspectedTo
              );
            }
          });
        }
      });
    }

    if (queryOptions.orderByRaw) {
      queryBuilder.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSqlFull, queryOptions.orderByDirection);
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder
      .where(this.getSqlField(e => e.customerId, true), customerId)
      .where(this.getSqlField(e => e.isDeleted, true), 0);
    return queryBuilder;
  }

  async createSampleOutfall(customerId: string): Bluebird<Outfall> {
    const sampleAsset: Outfall = {
      ...new Outfall(),
      customerId,
      location: 'Sample Outfall',
      complianceStatus: 'Compliant',
      lat: 29.760138,
      lng: -95.369371, // Houston City Hall
    };
    const data = this.objectToSqlData(Outfall, sampleAsset, this.knex);
    const returnFields = getSqlFields2(Outfall, { asProperty: true });
    const returnedData = await this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return returnedData;
  }

  /**
   * Privates --------------
   */

  private _appendContractorEmail(qb: Knex.QueryBuilder, email: string) {
    return qb
      .join(
        EntityContact.tableName,
        EntityContact.sqlField(f => f.globalIdFk, true),
        Outfall.sqlField(f => f.globalId, true)
      )
      .join(
        Contact.tableName,
        Contact.sqlField(f => f.id, true),
        EntityContact.sqlField(f => f.contactId, true)
      )
      .where({
        [Contact.sqlField(f => f.email, true)]: email,
        [Contact.sqlField(f => f.isDeleted, true)]: 0,
      });
  }

  private _appendLastInspector(qb: Knex.QueryBuilder) {
    return qb.joinRaw(`
        LEFT JOIN
          (
            SELECT MIN(OFI1.Id) AS Id, OFI1.OutfallId
            FROM OutfallInspections OFI1
            JOIN
              (
                SELECT OutfallId, MAX(InspectionDate) AS latestDate
                FROM OutfallInspections
                GROUP BY OutfallId
              ) OFI2
              ON OFI1.OutfallId = OFI2.OutfallId
                AND OFI1.InspectionDate = OFI2.latestDate
            GROUP BY OFI1.OutfallId
          ) LID
          ON LID.OutfallId = Outfalls.Id
        LEFT JOIN OutfallInspections LastInspection
          ON LastInspection.Id = LID.Id
      `);
  }
}
