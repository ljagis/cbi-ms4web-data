import { injectable, inject } from 'inversify';
import * as Knex from 'knex';
import {
  BmpActivityLog,
  BmpDetail,
  QueryBuilderOverride,
  Creatable,
  Updateable,
  CrudDeletable,
  BmpControlMeasure,
  DeleteOptions,
} from '../models';
import { getSqlFields2 } from '../decorators';
import { objectToSqlData } from '../shared/object-to-sql';
import * as Bluebird from 'bluebird';
import { fetchEntities } from './shared';

interface BmpActivityLogQueryOptions {
  bmpDetailId?: number;
  activityFrom?: string;
  activityTo?: string;
}

@injectable()
export class BmpActivityLogRepository
  implements Creatable<BmpActivityLog>,
    Updateable<BmpActivityLog>,
    CrudDeletable {
  constructor(@inject(Knex) protected knex: Knex) {}

  async insert(
    customerId: string,
    dataType: Partial<BmpActivityLog>
  ): Bluebird<BmpActivityLog> {
    const data = objectToSqlData(BmpActivityLog, dataType, this.knex);
    const returnFields = getSqlFields2(BmpActivityLog, { asProperty: true });
    const qb = this.knex(BmpActivityLog.getTableName())
      .insert(data)
      .returning(returnFields);

    _buildWhereInQuery(customerId, qb);

    return qb.get<BmpActivityLog>(0);
  }

  async fetch(
    customerId: string,
    queryOptions: BmpActivityLogQueryOptions = {}
  ): Bluebird<BmpActivityLog[]> {
    const qb = fetchEntities(BmpActivityLog, this.knex)
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(f => f.id, true),
        BmpActivityLog.sqlField(f => f.bmpDetailId, true)
      )
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where(BmpControlMeasure.sqlField(d => d.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(d => d.isDeleted, true), 0)
      .where(BmpDetail.sqlField(d => d.isDeleted, true), 0)
      .where(BmpActivityLog.sqlField(d => d.isDeleted, true), 0);

    if (queryOptions.bmpDetailId) {
      qb.where(
        BmpActivityLog.sqlField(f => f.bmpDetailId, true),
        queryOptions.bmpDetailId
      );
    }

    if (queryOptions.activityFrom) {
      qb.where(
        BmpActivityLog.sqlField(f => f.activityDate, true),
        '>=',
        queryOptions.activityFrom
      );
    }
    if (queryOptions.activityTo) {
      qb.where(
        BmpActivityLog.sqlField(f => f.activityDate, true),
        '<=',
        queryOptions.activityTo
      );
    }

    return qb;
  }

  async fetchByDetail(
    customerId: string,
    id: number
  ): Bluebird<BmpActivityLog[]> {
    const queryBuilder = this.fetch(customerId, {
      bmpDetailId: id,
    });
    return queryBuilder;
  }

  async fetchById(
    customerId: string,
    id: number,
    queryBuilderOverrides?: QueryBuilderOverride
  ): Bluebird<BmpActivityLog> {
    const qb = fetchEntities(BmpActivityLog, this.knex)
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(f => f.id, true),
        BmpActivityLog.sqlField(f => f.bmpDetailId, true)
      )
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where(BmpControlMeasure.sqlField(d => d.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(d => d.isDeleted, true), 0)
      .where(BmpDetail.sqlField(d => d.isDeleted, true), 0)
      .where(BmpActivityLog.sqlField(d => d.isDeleted, true), 0)
      .where(BmpActivityLog.sqlField(f => f.id, true), id);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return qb.get<BmpActivityLog>(0);
  }

  async update(
    customerId: string,
    id: number,
    dataType: Partial<BmpActivityLog>
  ): Bluebird<BmpActivityLog> {
    const data = objectToSqlData(BmpActivityLog, dataType, this.knex);
    const returnFields = getSqlFields2(BmpActivityLog, { asProperty: true });
    const qb = this.knex(BmpActivityLog.getTableName())
      .update(data)
      .where(BmpActivityLog.sqlField(d => d.id, true), id)
      .returning(returnFields);
    _buildWhereInQuery(customerId, qb);
    return qb;
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<number> {
    const qb = this.knex(BmpActivityLog.getTableName()).where(
      BmpActivityLog.sqlField(f => f.id),
      id
    );
    _buildWhereInQuery(customerId, qb);
    if (options && options.force) {
      qb.del();
    } else {
      qb.update(BmpActivityLog.sqlField(f => f.isDeleted), 1);
    }

    return qb;
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${BmpControlMeasure.sqlField(c => c.id, true)} as controlMeasureId`,
      `${BmpControlMeasure.sqlField(c => c.name, true)} as controlMeasureName`,
      `${BmpDetail.sqlField(c => c.id, true)} as bmpDetailId`,
      `${BmpDetail.sqlField(c => c.name, true)} as bmpDetailName`,

      BmpActivityLog.sqlField(a => a.id, true, true),
      BmpActivityLog.sqlField(a => a.dateAdded, true, true),
      BmpActivityLog.sqlField(a => a.activityDate, true, true),
      BmpActivityLog.sqlField(a => a.dataType, true, true),
      BmpActivityLog.sqlField(a => a.quantity, true, true),
      BmpActivityLog.sqlField(a => a.comments, true, true),
    ];
    return await this.knex(BmpActivityLog.getTableName())
      .leftOuterJoin(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(p => p.id, true),
        BmpActivityLog.sqlField(c => c.bmpDetailId, true)
      )
      .leftOuterJoin(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(p => p.id, true),
        BmpDetail.sqlField(c => c.controlMeasureId, true)
      )
      .where(BmpActivityLog.sqlField(c => c.isDeleted, true), 0)
      .where(BmpDetail.sqlField(c => c.isDeleted, true), 0)
      .where(BmpControlMeasure.sqlField(c => c.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }
}

/**
 * builds whereIn query to join with customerId
 *
 * @param {string} customerId
 * @param {Knex.QueryBuilder} qb
 */
function _buildWhereInQuery(customerId: string, qb: Knex.QueryBuilder) {
  qb.whereIn(BmpActivityLog.sqlField(f => f.bmpDetailId, true), function(
    this: Knex
  ) {
    this.from(BmpControlMeasure.getTableName())
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(f => f.id, true),
        BmpActivityLog.sqlField(f => f.bmpDetailId, true)
      )
      .select(BmpDetail.sqlField(f => f.id, true))
      .where(BmpControlMeasure.sqlField(f => f.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(f => f.isDeleted, true), 0)
      .where(BmpDetail.sqlField(f => f.isDeleted, true), 0);
  });
}
