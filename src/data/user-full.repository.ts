import { Repository } from './repository';
import { UserCustomer, QueryOptions, UserFull } from '../models';
import { injectable } from 'inversify';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

@injectable()
export class UserFullRepository extends Repository<UserFull> {
  constructor() {
    super(UserFull);
    this.defaultSelect = [
      ...this.defaultSelect,
      UserCustomer.sqlField(uc => uc.role, true, true),
    ];
  }

  async fetchByCustomer(customerId: string): Bluebird<UserFull[]> {
    const queryBuilder = this._fetchQueryBuilder().where(
      UserCustomer.sqlField(uc => uc.customerId, true),
      customerId
    );
    return await queryBuilder.map(u => this.mapObjectToEntityClass(u));
  }

  async fetchByEmailAndCustomer(
    email: string,
    customerId: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<UserFull> {
    const queryBuilder = this._fetchQueryBuilder(null, queryBuilderOverrides)
      .where(this.getSqlField(u => u.email, true), email)
      .where(UserCustomer.sqlField(uc => uc.customerId, true), customerId);

    return await queryBuilder
      .map(u => this.mapObjectToEntityClass(u))
      .get<UserFull>(0);
  }

  async fetchByIdAndCustomer(id: number, customerId: string) {
    const queryBuilder = this._fetchQueryBuilder()
      .where(this.getSqlField(u => u.id, true), id)
      .where(UserCustomer.sqlField(uc => uc.customerId, true), customerId);

    return await queryBuilder
      .map(u => this.mapObjectToEntityClass(u))
      .get<UserFull>(0);
  }

  protected mapObjectToEntityClass(obj, ...args): UserFull {
    let user = super.mapObjectToEntityClass(obj, ...args);
    user.fullName = user.getFullName();
    return user;
  }

  private _fetchQueryBuilder(
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ) {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName)
      .join(
        UserCustomer.tableName,
        UserFull.sqlField(u => u.id, true),
        UserCustomer.sqlField(uc => uc.userId, true)
      )
      .select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder = queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder = queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(u => u.surname),
        queryOptions.orderByDirection
      );
    }

    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder;
  }
}
