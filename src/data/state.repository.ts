import * as Bluebird from 'bluebird';

import { Repository } from './repository';
import { State } from '../models';
import { injectable } from 'inversify';
@injectable()
export class StateRepository extends Repository<State> {
  constructor() {
    super(State);
  }

  /**
   * Only fetch US states
   */
  fetch(): Bluebird<State[]> {
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .where(State.sqlField(f => f.countryCode), 'US')
      .orderBy(this.getSqlField(s => s.name), 'asc');
    return queryBuilder;
  }

  /**
   * Fetches all states of countries
   */
  fetchAllStates(): Bluebird<State[]> {
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .orderBy(this.getSqlField(s => s.name), 'asc');
    return queryBuilder;
  }
}
