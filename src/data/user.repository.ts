import { Repository } from './repository';
import {
  UserCustomer,
  QueryOptions,
  User,
  stormpathToOktaUser,
  oktaToStormpathUser,
  OKTA_USER_STATUSES,
} from '../models';
import { injectable, inject } from 'inversify';
import { getSqlFields, getTableName } from '../decorators';
import { NotFoundError, Env } from '../shared';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';
import { Client } from '@okta/okta-sdk-nodejs';
import { AccountData } from 'stormpath';
import * as uuid from 'uuid';

@injectable()
export class UserRepository extends Repository<User> {
  env: Env = process.env as Env;

  constructor(@inject(Client) private _oktaClient: Client) {
    super(User);
  }

  async fetch(
    customerId: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<User[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this._fetchQueryBuilder(
      queryOptions,
      queryBuilderOverrides
    );
    const selectUserIdQb = this.knex(UserCustomer.getTableName())
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .select(UserCustomer.getSqlField<UserCustomer>(u => u.userId));
    queryBuilder.whereIn(User.getSqlField<User>(u => u.id), selectUserIdQb);
    return await queryBuilder.map<User, User>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  async fetchByCustomer(
    customerId: string,
    queryOptions?: QueryOptions & { excludeSuperRole?: boolean },
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<User[]> {
    const queryBuilder = this._fetchQueryBuilder(
      queryOptions,
      queryBuilderOverrides
    );
    let selectUserIdQb = this.knex(UserCustomer.getTableName())
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .select(UserCustomer.getSqlField<UserCustomer>(u => u.userId));

    if (queryOptions && queryOptions.excludeSuperRole) {
      selectUserIdQb = selectUserIdQb.andWhereNot(
        UserCustomer.getSqlField<UserCustomer>(u => u.role),
        'super'
      );
    }

    queryBuilder.whereIn(User.getSqlField<User>(u => u.id), selectUserIdQb);
    return await queryBuilder.map<User, User>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  async fetchAvailableToAddByCustomer(
    customerId: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<User[]> {
    const queryBuilder = this._fetchQueryBuilder(
      queryOptions,
      queryBuilderOverrides
    );
    const selectUserIdQb = this.knex(UserCustomer.getTableName())
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .select(UserCustomer.getSqlField<UserCustomer>(u => u.userId));
    queryBuilder.whereNotIn(User.getSqlField<User>(u => u.id), selectUserIdQb);
    return await queryBuilder.map<User, User>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  async fetchById(
    id: number,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<User> {
    let queryBuilder = this._fetchQueryBuilder(undefined, qb =>
      qb.where(this.idFieldSql, id)
    );
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return await queryBuilder
      .map<User, User>(l => this.mapObjectToEntityClass(l))
      .get<User>(0);
  }

  async fetchByIdAndCustomer(
    id: number,
    customerId: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ) {
    const queryBuilder = this._fetchQueryBuilder({}, queryBuilderOverrides);
    const selectUserIdQb = this.knex(UserCustomer.getTableName())
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(UserCustomer.sqlField(u => u.userId), id)
      .select(UserCustomer.getSqlField<UserCustomer>(u => u.userId));
    queryBuilder.whereIn(User.getSqlField<User>(u => u.id), selectUserIdQb);
    return await queryBuilder
      .map<User, User>(l => this.mapObjectToEntityClass(l))
      .get<User>(0);
  }

  async fetchByEmail(
    email: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<User> {
    let queryBuilder = this._fetchQueryBuilder(undefined, qb =>
      qb.where(this.getSqlField(u => u.email), email)
    );
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return await queryBuilder
      .map<User, User>(l => this.mapObjectToEntityClass(l))
      .get<User>(0);
  }

  async insert(accountData: Partial<User>): Bluebird<User> {
    const okUser = await this.createOktaUser(accountData).catch(e => {
      throw e;
    });
    accountData.oktaId = okUser.id;
    const data = Object.assign({}, accountData, oktaToStormpathUser(okUser));
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(data))
      .returning(returnFields)
      .map<User, User>(u => this.mapObjectToEntityClass(u))
      .get<User>(0);
    return queryBuilder;
  }

  /**
   * Removes user from customer/group
   *
   * @param {number} userId
   * @param {string} customerId
   * @returns {Bluebird<number>} number of rows affected
   *
   * @memberOf UserRepository
   */
  async removeUserFromCustomer(
    userId: number,
    customerId: string
  ): Bluebird<number> {
    return await this.knex(getTableName(UserCustomer))
      .del()
      .limit(1)
      .where(UserCustomer.getSqlField<UserCustomer>(f => f.userId), userId)
      .where(
        UserCustomer.getSqlField<UserCustomer>(f => f.customerId),
        customerId
      );
  }

  async update(id: number, user: User, customerId: string): Bluebird<void> {
    const data = this.mapObjectToSql(user);
    const qb = this.knex(this.tableName)
      .update(data)
      .where(this.idFieldSql, id);

    const oldUser = await this.fetchById(id);
    const oktaUser = await this._oktaClient.getUser(oldUser.email);

    await this.knex.transaction(async trx => {
      await qb.transacting(trx);
      const tempUser = stormpathToOktaUser(user as any);
      oktaUser.profile.firstName = tempUser.profile.firstName;
      oktaUser.profile.lastName = tempUser.profile.lastName;
      oktaUser.profile.email = tempUser.profile.email;
      oktaUser.profile.login = tempUser.profile.email;
      oktaUser.status = tempUser.status;

      if (oldUser.status !== user.status) {
        switch (user.status) {
          case 'DISABLED':
            await oktaUser.suspend();
            break;
          case 'ENABLED':
            if (oktaUser.status === OKTA_USER_STATUSES.SUSPENDED) {
              await oktaUser.unsuspend();
            }
            break;
        }
      }

      await oktaUser.update();
    });
  }

  protected mapObjectToEntityClass(obj, ...args): User {
    let user = super.mapObjectToEntityClass(obj, ...args);
    user.fullName = user.getFullName();
    return user;
  }

  /**
   * Completely deletes a user from database
   *
   * @param {number} id
   */
  async del(id: number): Bluebird<any> {
    let delQb = this.knex(this.tableName)
      .delete()
      .where(this.getSqlField(u => u.id), id);
    const user = await this.fetchById(id);

    if (!user) {
      throw new NotFoundError(`User ${id} not found`);
    }
    await this.knex.transaction(trx => {
      return delQb.transacting(trx).then(async () => {
        const oktaUser = await this._oktaClient.getUser(user.email);
        await oktaUser.suspend();
      });
    });
  }

  async createOktaUser(accountData: AccountData) {
    // Activation flow
    // User is created with no password and status "Pending user action"
    // User does not receive an activation email
    // Admin password reset is triggered
    // User receives email to reset their password (Password Reset by Admin template)
    // Password Reset by Admin template should be created to look like a welcome email
    // User clicks the link, resets their password, is signed in, and status becomes "Active"
    const stormpathMigrationRecoveryAnswer = uuid.v4();
    const payload = {
      profile: {
        firstName: accountData.givenName,
        lastName: accountData.surname,
        email: accountData.email,
        login: accountData.email,
        stormpathMigrationRecoveryAnswer: stormpathMigrationRecoveryAnswer,
        emailVerificationToken: uuid.v4(),
      },
      credentials: {
        // NOTE Do not include a password to trigger the correct user activation flow
        recovery_question: {
          question: 'stormpathMigrationRecoveryAnswer',
          answer: stormpathMigrationRecoveryAnswer,
        },
      },
      groupIds: [this.env.OKTA_GROUP_ID],
    };

    // Create a user. See https://developer.okta.com/docs/reference/api/users/#create-user
    const okUser = await this._oktaClient.http.postJson(
      `${this._oktaClient.baseUrl}/api/v1/users?activate=false`,
      {
        body: payload,
      }
    );

    // Activate user. See https://developer.okta.com/docs/reference/api/users/#activate-user
    await this._oktaClient.http.postJson(
      `${this._oktaClient.baseUrl}/api/v1/users/${
        okUser.id
      }/lifecycle/activate?sendEmail=false`,
      {}
    );

    // Reset password. See https://developer.okta.com/docs/reference/api/users/#reset-password
    await this._oktaClient.http.postJson(
      `${this._oktaClient.baseUrl}/api/v1/users/${
        okUser.id
      }/lifecycle/reset_password?sendEmail=true`,
      {}
    );

    return okUser;
  }

  private _fetchQueryBuilder(
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ) {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder = queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder = queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(u => u.givenName),
        queryOptions.orderByDirection
      );
    }

    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder;
  }
}
