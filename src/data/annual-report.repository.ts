import { Repository } from './repository';
import { AnnualReport } from '../models';
import { injectable } from 'inversify';
import { UserCustomer } from '../models';

@injectable()
export class AnnualReportRepository extends Repository<AnnualReport> {
  constructor() {
    super(AnnualReport);
  }

  async create(annualReport: Partial<AnnualReport>) {
    const returnFields = this.getSelectFields();
    const createdAnnualReport: AnnualReport = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(annualReport))
      .returning(returnFields)
      .get<AnnualReport>(0);

    return createdAnnualReport;
  }

  async update(id: number, report: string) {
    const qb = this.knex(this.tableName)
      .update(AnnualReport.sqlField(r => r.report), report)
      .where(AnnualReport.sqlField(r => r.id), id);

    return qb;
  }

  async updateContactInfo(id: number, contactData: Partial<AnnualReport>) {
    const qb = this.knex(this.tableName)
      .update({
        [AnnualReport.sqlField(r => r.contactName)]: contactData.contactName,
        [AnnualReport.sqlField(r => r.phone)]: contactData.phone,
        [AnnualReport.sqlField(r => r.emailAddress)]: contactData.emailAddress,
        [AnnualReport.sqlField(
          r => r.mailingAddress
        )]: contactData.mailingAddress,
      })
      .where(AnnualReport.sqlField(r => r.id), id);

    return qb;
  }

  async fetchByIdAndCustomer(id: number, customerId: string) {
    return await this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(AnnualReport.getSqlField<AnnualReport>(r => r.id), id)
      .select(this.defaultSelect)
      .get<AnnualReport>(0);
  }

  async fetchBySwmpAndCustomer(
    swmpId: number,
    year: number,
    customerId: string
  ) {
    return await this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(AnnualReport.getSqlField<AnnualReport>(r => r.swmpId), swmpId)
      .where(AnnualReport.getSqlField<AnnualReport>(r => r.year), year)
      .select(this.defaultSelect)
      .get<AnnualReport>(0);
  }
}
