import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { FacilityRepository } from './facility.repository';
import * as Knex from 'knex';

describe('Facility Repository Tests', () => {
  const repo = container.get(FacilityRepository);
  const knex: Knex = container.get(Knex);
  const customerId = 'LEWISVILLE';

  describe(`exportData tests #FacilityRepository`, () => {
    it(`should export data`, async () => {
      const facilities = await repo.exportData(customerId);

      const facility = facilities[0];

      chai.expect(facilities).to.be.instanceOf(Array);
      chai.expect(facilities).to.have.length.greaterThan(0);

      // TODO: test for properties exported.
      chai.expect(facility).to.have.property('id');
      chai.expect(facility).to.have.property('name');
      chai.expect(facility).to.have.property('inspectionFrequencyName');
    });
  });

  describe(`exportCustomFields #FacilityRepository`, () => {
    it(`should get custom field values`, async () => {
      const cfvs = await repo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('facilityId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });

  describe(`fetch by contractorEmail`, async () => {
    it(`should fetch by contractorEmail`, async () => {
      const expectedSql = `
      SELECT
  count (*) as count
FROM
  Facilities cs INNER JOIN EntityContacts ec ON cs.GlobalId = ec.GlobalIdFk INNER JOIN Contacts c ON c.Id = ec.ContactId
WHERE
  cs.IsDeleted=0
  AND
  c.IsDeleted=0
  AND
  c.Email = 'alexng99+contractor@gmail.com'`;
      const expected: any[] = await knex.raw(expectedSql);
      const contractorEmail = 'alexng99+contractor@gmail.com';
      const assets = await repo.fetch(customerId, { contractorEmail });
      chai.expect(assets).length.greaterThan(0);
      chai.expect(assets.length).eq(expected[0].count);
    });
  });
});
