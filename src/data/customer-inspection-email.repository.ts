import { Repository } from './repository';
import { CustomerInspectionEmail } from '../models';
import { injectable } from 'inversify';
import { UserCustomer } from '../models';

@injectable()
export class CustomerInspectionEmailRepository extends Repository<
  CustomerInspectionEmail
> {
  constructor() {
    super(CustomerInspectionEmail);
  }

  fetch(customerId: string, inspectionType: string) {
    return this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(
        CustomerInspectionEmail.sqlField(c => c.inspectionType),
        inspectionType
      )
      .select(this.defaultSelect)
      .get<CustomerInspectionEmail>(0);
  }
}
