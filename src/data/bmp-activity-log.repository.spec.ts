import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpDetailRepository } from './bmp-detail.repository';
import { BmpDetail, BmpControlMeasure, BmpActivityLog } from '../models';
import * as Knex from 'knex';
import { BmpControlMeasureRepository } from './bmp-control-measure.repository';
import * as moment from 'moment';
import { BmpActivityLogRepository } from './bmp-activity-log.repository';

const customerId = 'LEWISVILLE';

describe('BMP Activity Log Repo', async () => {
  const bmpDetailRepo = container.get(BmpDetailRepository);
  const bmpActivityLogRepo = container.get(BmpActivityLogRepository);
  const cmRepo = container.get(BmpControlMeasureRepository);
  const knex = container.get<Knex>(Knex);

  let createdActivityLogId: number;
  let controlMeasures: BmpControlMeasure[];
  let controlMeasure: BmpControlMeasure;
  let bmpDetail: BmpDetail;
  const bmpDetailName = `We will clean up stuff`;
  const expectedActivityDate = moment.utc('2017-09-01').toDate();
  const expectedDataType = 'Miles Driven';
  const expectedQty = 12;
  const expectComments = 'comments abc';

  before(async () => {
    // gets all control measures
    controlMeasures = await cmRepo.fetch(customerId);
    controlMeasure = controlMeasures[0];
    // add a temp test cm
    bmpDetail = await bmpDetailRepo.insert(customerId, {
      name: bmpDetailName,
      controlMeasureId: controlMeasure.id,
    });
  });

  // Cleanup
  after(async () => {
    if (createdActivityLogId > 0) {
      await bmpActivityLogRepo.delete(customerId, createdActivityLogId, {
        force: true,
      });
    }
    if (bmpDetail && bmpDetail.id > 0) {
      await knex(BmpDetail.getTableName())
        .delete()
        .where(BmpDetail.sqlField(f => f.id), bmpDetail.id);
    }
  });

  describe(`create`, async () => {
    it(`should create Activity Log`, async () => {
      const bmpLog: Partial<BmpActivityLog> = {
        bmpDetailId: bmpDetail.id,
        activityDate: expectedActivityDate,
        dataType: expectedDataType,
        quantity: expectedQty,
        comments: expectComments,
      };

      const returnedLog = await bmpActivityLogRepo.insert(customerId, bmpLog);

      createdActivityLogId = returnedLog.id;
      chai.expect(returnedLog.id).greaterThan(0);
      chai.expect(returnedLog.bmpDetailId).eq(bmpDetail.id);
      chai.expect(returnedLog.activityDate).eql(expectedActivityDate);
      chai.expect(returnedLog.dataType).eq(expectedDataType);
      chai.expect(returnedLog.quantity).eql(expectedQty);
      chai.expect(returnedLog.comments).eql(expectComments);
    });
  });

  describe(`fetch `, async () => {
    // TODO: test `fetchByDetail`
    it(`should fetch all`, async () => {
      const logs = await bmpActivityLogRepo.fetch(customerId, {
        bmpDetailId: bmpDetail.id,
      });

      const returnedLog = logs.find(t => t.id === createdActivityLogId);

      chai.expect(logs).to.be.instanceOf(Array);
      chai.expect(logs).to.have.length.greaterThan(0);
      chai.expect(returnedLog.id).greaterThan(0);
      chai.expect(returnedLog.bmpDetailId).eq(bmpDetail.id);
      chai.expect(returnedLog.activityDate).eql(expectedActivityDate);
      chai.expect(returnedLog.dataType).eq(expectedDataType);
      chai.expect(returnedLog.quantity).eql(expectedQty);
      chai.expect(returnedLog.comments).eql(expectComments);
    });

    it(`should fetch by id`, async () => {
      chai.expect(createdActivityLogId).greaterThan(0);

      const returnedLog = await bmpActivityLogRepo.fetchById(
        customerId,
        createdActivityLogId
      );

      chai.expect(returnedLog).not.undefined;
      chai.expect(returnedLog.id).greaterThan(0);
      chai.expect(returnedLog.bmpDetailId).eq(bmpDetail.id);
      chai.expect(returnedLog.activityDate).eql(expectedActivityDate);
      chai.expect(returnedLog.dataType).eq(expectedDataType);
      chai.expect(returnedLog.quantity).eql(expectedQty);
      chai.expect(returnedLog.comments).eql(expectComments);
    });
  });

  describe(`fetch by from and to date `, async () => {
    let returnedLog: BmpActivityLog;
    before(async () => {
      const bmpLog: Partial<BmpActivityLog> = {
        bmpDetailId: bmpDetail.id,
        activityDate: moment('2018-02-02').toDate(),
        dataType: expectedDataType,
        quantity: expectedQty,
        comments: expectComments,
      };
      returnedLog = await bmpActivityLogRepo.insert(customerId, bmpLog);
    });

    after(async () => {
      if (returnedLog) {
        await bmpActivityLogRepo.delete(customerId, returnedLog.id, {
          force: true,
        });
      }
    });

    it(`should fetch by fromDate`, async () => {
      const logs = await bmpActivityLogRepo.fetch(customerId, {
        activityFrom: moment('2018-02-02')
          .add(-1, 'days')
          .toISOString(),
      });
      const log = logs.find(l => l.id === returnedLog.id);

      chai.expect(log).ok;
    });

    it(`should not fetch by fromDate`, async () => {
      const logs = await bmpActivityLogRepo.fetch(customerId, {
        activityFrom: moment('2018-02-02')
          .add(1, 'days')
          .toISOString(),
      });
      const log = logs.find(l => l.id === returnedLog.id);

      chai.expect(log).not.ok;
    });

    it(`should fetch by toDate`, async () => {
      const logs = await bmpActivityLogRepo.fetch(customerId, {
        activityTo: moment('2018-02-02')
          .add(1, 'days')
          .toISOString(),
      });
      const log = logs.find(l => l.id === returnedLog.id);

      chai.expect(log).ok;
    });

    it(`should not fetch by toDate`, async () => {
      const logs = await bmpActivityLogRepo.fetch(customerId, {
        activityTo: moment('2018-02-02')
          .add(-1, 'days')
          .toISOString(),
      });
      const log = logs.find(l => l.id === returnedLog.id);

      chai.expect(log).not.ok;
    });
  });

  describe(`update `, async () => {
    it(`should update`, async () => {
      chai.expect(createdActivityLogId).greaterThan(0);
      const expectedActivityDate2 = moment.utc('2017-09-05').toDate();
      const expectedDataType2 = 'Miles Driven 2';
      const expectedQty2 = 13;
      const expectComments2 = 'comments abc 123';
      await bmpActivityLogRepo.update(customerId, createdActivityLogId, {
        activityDate: expectedActivityDate2,
        dataType: expectedDataType2,
        quantity: expectedQty2,
        comments: expectComments2,
      });

      const returnedLog = await bmpActivityLogRepo.fetchById(
        customerId,
        createdActivityLogId
      );

      chai.expect(returnedLog).ok;
      chai.expect(returnedLog.id).greaterThan(0);
      chai.expect(returnedLog.bmpDetailId).eq(bmpDetail.id);
      chai.expect(returnedLog.activityDate).eql(expectedActivityDate2);
      chai.expect(returnedLog.dataType).eq(expectedDataType2);
      chai.expect(returnedLog.quantity).eql(expectedQty2);
      chai.expect(returnedLog.comments).eql(expectComments2);
    });
  });

  describe(`soft delete`, async () => {
    it(`should soft delete`, async () => {
      chai.expect(createdActivityLogId).greaterThan(0);
      await bmpActivityLogRepo.delete(customerId, createdActivityLogId);

      const returnedLog = await bmpActivityLogRepo.fetchById(
        customerId,
        createdActivityLogId
      );
      chai.expect(returnedLog).not.ok;
    });
  });
});
