import { Repository } from './repository';
import { SWMP } from '../models';
import { injectable } from 'inversify';
import { UserCustomer } from '../models';
import * as Bluebird from 'bluebird';

@injectable()
export class SwmpRepository extends Repository<SWMP> {
  constructor() {
    super(SWMP);
  }

  async create(swmp: Partial<SWMP>) {
    const returnFields = this.getSelectFields();
    const createdSwmp: SWMP = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(swmp))
      .returning(returnFields)
      .get<SWMP>(0);

    return createdSwmp;
  }

  async update(swmp: Partial<SWMP>, customerId: string) {
    const qb = this.knex(this.tableName)
      .update(this.mapObjectToSql(swmp))
      .where(SWMP.sqlField(s => s.customerId), customerId)
      .where(SWMP.sqlField(s => s.id), swmp.id);

    return qb;
  }

  async delete(id: number, customerId: string) {
    const qb = this.knex(this.tableName)
      .update(SWMP.sqlField(s => s.isDeleted), true)
      .where(SWMP.sqlField(s => s.customerId), customerId)
      .where(SWMP.sqlField(s => s.id), id);

    return qb;
  }

  async fetchByCustomer(customerId: string): Bluebird<SWMP[]> {
    return await this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(SWMP.sqlField(s => s.isDeleted), false)
      .select(this.defaultSelect)
      .orderBy(SWMP.sqlField(s => s.year), 'desc');
  }

  async fetchByIdAndCustomer(swmpId: number, customerId: string) {
    return await this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(SWMP.sqlField(s => s.isDeleted), false)
      .where(SWMP.getSqlField<SWMP>(s => s.id), swmpId)
      .select(this.defaultSelect)
      .get<SWMP>(0);
  }

  async fetchLatestByCustomer(customerId: string) {
    return await this.knex(this.tableName)
      .where(
        UserCustomer.getSqlField<UserCustomer>(u => u.customerId),
        customerId
      )
      .where(SWMP.sqlField(s => s.isDeleted), false)
      .orderBy(SWMP.sqlField(s => s.id), 'desc')
      .limit(1)
      .select(this.defaultSelect)
      .get<SWMP>(0);
  }
}
