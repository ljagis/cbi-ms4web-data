import * as Knex from 'knex';
import { Repository } from '../repository';
import {
  Entity,
  QueryOptions,
  OutfallMaterial,
  OutfallType,
  Sector,
  InspectionFrequency,
} from '../../models';
import { injectable, unmanaged } from 'inversify';
import * as Bluebird from 'bluebird';

@injectable()
export abstract class CommonLookupRepository<
  T extends Entity
> extends Repository<T> {
  constructor(@unmanaged() entity: { new (...args): T }) {
    super(entity);
  }

  /**
   * * Generic method to get a lookups of type T
   *
   * @param {QueryOptions} queryOptions
   * @param {((qb: Knex.QueryBuilder) => Knex.QueryBuilder)} [queryBuilderOverrides] additional queryBuilders
   * @returns {Bluebird<T[]>}
   *
   * @memberOf LookupRepository
   */
  fetch(
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<T[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder = queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder = queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder = queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map(o => this.mapObjectToEntityClass(o));
  }

  fetchById(
    id: number,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<T> {
    let queryBuilder = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id);
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder.map(o => this.mapObjectToEntityClass(o)).get<T>(0);
  }
}

@injectable()
export class OutfallMaterialRepository extends CommonLookupRepository<
  OutfallMaterial
> {
  constructor() {
    super(OutfallMaterial);
  }
}

@injectable()
export class OutfallTypeRepository extends CommonLookupRepository<OutfallType> {
  constructor() {
    super(OutfallType);
  }
}

@injectable()
export class SectorRepository extends CommonLookupRepository<Sector> {
  constructor() {
    super(Sector);
  }
}

@injectable()
export class InspectionFrequencyRepository extends CommonLookupRepository<
  InspectionFrequency
> {
  constructor() {
    super(InspectionFrequency);
  }
}
