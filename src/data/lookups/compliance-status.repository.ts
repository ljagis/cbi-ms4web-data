import * as Knex from 'knex';
import { Repository } from '../repository';
import { ComplianceStatus, QueryOptions } from '../../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';

@injectable()
export class ComplianceStatusRepository extends Repository<ComplianceStatus> {
  constructor() {
    super(ComplianceStatus);
  }

  fetch(
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<ComplianceStatus[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderByRaw(
        `${this.getSqlField(c => c.entityType)}, ${this.getSqlField(
          c => c.level
        )}`
      );
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map(o => this.mapObjectToEntityClass(o));
  }

  fetchSingle(id: number) {
    return this.knex(this.tableName)
      .select(this.defaultSelect)
      .where(this.getSqlField(c => c.id), id)
      .get<ComplianceStatus>(0);
  }

  fetchByAttr({ name, entityType, level }: Partial<ComplianceStatus>) {
    return this.knex(this.tableName)
      .select(this.defaultSelect)
      .where({
        [this.getSqlField(c => c.name)]: name,
        [this.getSqlField(c => c.entityType)]: entityType,
        [this.getSqlField(c => c.level)]: level,
      })
      .get<ComplianceStatus>(0);
  }

  update(complianceStatus: ComplianceStatus) {
    return this.knex(this.tableName)
      .update(this.mapObjectToSql(complianceStatus))
      .where(this.getSqlField(c => c.id), complianceStatus.id);
  }

  create(complianceStatus: ComplianceStatus) {
    return this.knex(this.tableName)
      .insert(this.mapObjectToSql(complianceStatus))
      .returning(this.getSelectFields())
      .get<ComplianceStatus>(0);
  }

  delete(id: number) {
    return this.knex(this.tableName)
      .where(this.getSqlField(c => c.id), id)
      .delete();
  }
}
