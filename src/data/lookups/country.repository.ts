import { injectable } from 'inversify';
import { Repository } from '../repository';
import { Country } from '../../models';
import * as Bluebird from 'bluebird';

@injectable()
export class CountryRepository extends Repository<Country> {
  constructor() {
    super(Country);
  }

  async fetch(): Bluebird<Country[]> {
    const countries = await this.knex(this.tableName)
      .select(this.defaultSelect)
      .orderBy(Country.sqlField(c => c.name));
    return countries;
  }
}
