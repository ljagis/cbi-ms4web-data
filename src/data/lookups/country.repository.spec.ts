import 'mocha';

import * as Knex from 'knex';
import * as sinon from 'sinon';

import { Container } from 'inversify';
import { CountryRepository } from './country.repository';
import { expect } from 'chai';

describe('Country Repository Tests', async () => {
  describe(`fetch`, async () => {
    it(`should fetch`, async () => {
      const container = new Container();
      const mockKnex: Knex = {
        select: () => {},
        orderBy: () => {},
      } as any;
      const selectStub = sinon.stub(mockKnex, 'select').returnsThis();
      const orderStub = sinon.stub(mockKnex, 'orderBy').returnsThis();
      const knexStub = sinon.stub().returns(mockKnex);
      container.bind(Knex).toConstantValue(knexStub);
      container.bind(CountryRepository).toSelf();

      const repo = container.get(CountryRepository);
      await repo.fetch();

      expect(knexStub.withArgs('Countries').calledOnce).true;
      expect(orderStub.withArgs('Name').calledOnce).true;
      expect(selectStub.calledOnce).true;
    });
  });
});
