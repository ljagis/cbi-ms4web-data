import { Repository } from './repository';
import { UserCustomer } from '../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';
import { getSqlFields2 } from '../decorators';

@injectable()
export class UserCustomerRepository extends Repository<UserCustomer> {
  constructor() {
    super(UserCustomer);
  }

  async fetchByCustomer(customerId: string): Bluebird<UserCustomer[]> {
    const queryBuilder = this._fetchQueryBuilder().where(
      UserCustomer.sqlField(uc => uc.customerId, true),
      customerId
    );
    return await queryBuilder.map(u => this.mapObjectToEntityClass(u));
  }

  async fetchByUserId(userId: number): Bluebird<UserCustomer[]> {
    const queryBuilder = this._fetchQueryBuilder().where(
      UserCustomer.sqlField(uc => uc.userId, true),
      userId
    );
    return await queryBuilder.map(u => this.mapObjectToEntityClass(u));
  }

  async fetchByUserIdCustomerId(
    userId: number,
    customerId: string
  ): Bluebird<UserCustomer> {
    const queryBuilder = this._fetchQueryBuilder().where({
      [UserCustomer.sqlField(uc => uc.userId, true)]: userId,
      [UserCustomer.sqlField(uc => uc.customerId, true)]: customerId,
    });
    const ucs = await queryBuilder.map(u => this.mapObjectToEntityClass(u));
    return ucs[0];
  }

  async updateRole(userId: number, customerId: string, role: string) {
    const data = this.mapObjectToSql(<UserCustomer>{
      userId,
      customerId,
      role,
    });
    return this.knex(UserCustomer.tableName)
      .update(data)
      .where({
        [UserCustomer.sqlField(f => f.userId)]: userId,
        [UserCustomer.sqlField(f => f.customerId)]: customerId,
      });
  }

  async addUserToCustomer(
    userId: number,
    customerId: string,
    role: string
  ): Bluebird<UserCustomer> {
    const userCustomer = new UserCustomer();
    userCustomer.customerId = customerId;
    userCustomer.userId = userId;
    userCustomer.role = role;
    const data = this.mapObjectToSql(userCustomer);
    const returnFields = getSqlFields2(UserCustomer, { asProperty: true });
    const insertedUserCustomer = await this.knex(UserCustomer.getTableName())
      .insert(data)
      .returning(returnFields)
      .map<UserCustomer, UserCustomer>(uc => this.mapObjectToEntityClass(uc))
      .get<UserCustomer>(0);
    return insertedUserCustomer;
  }

  private _fetchQueryBuilder() {
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    return queryBuilder;
  }
}
