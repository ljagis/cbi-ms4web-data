import { Repository } from './repository';
import {
  Customer,
  UserCustomer,
  CrudDeletable,
  DeleteOptions,
  PropsOf,
} from '../models';
import * as dh from '../decorators';
import * as Bluebird from 'bluebird';

export class CustomerRepository extends Repository<Customer>
  implements CrudDeletable {
  constructor() {
    super(Customer);
  }

  async fetch(): Bluebird<Customer[]> {
    return await this.knex(this.tableName)
      .select(this.defaultSelect)
      .map(c => this._parseCustomer(c));
  }

  async fetchById(id: string): Bluebird<Customer> {
    const customer = await this.knex(this.tableName)
      .where(this.idFieldSql, id)
      .first(this.defaultSelect);
    return this._parseCustomer(customer);
  }

  async update(customer: Partial<Customer>, id: string): Bluebird<Customer> {
    const data = this.mapObjectToSql(customer);
    const returnFields = dh.getSqlFields(Customer, undefined, false, true);
    const updatedCustomer = await this.knex(this.tableName)
      .update(data)
      .where(this.idFieldSql, id)
      .returning(returnFields)
      .get(0);
    return this._parseCustomer(updatedCustomer);
  }

  async fetchByUserId(userId: number): Bluebird<Customer[]> {
    const selectCustomerIdQb = this.knex(UserCustomer.getTableName())
      .select(UserCustomer.getSqlField<UserCustomer>(uc => uc.customerId))
      .where(UserCustomer.getSqlField<UserCustomer>(uc => uc.userId), userId);
    const customers = await this.knex(this.tableName)
      .whereIn(this.getSqlField(c => c.id), selectCustomerIdQb)
      .select(this.defaultSelect)
      .map<Customer, Customer>(c => this._parseCustomer(c));
    return customers;
  }

  async create(customer: Partial<Customer>): Bluebird<Customer> {
    const data = this.mapObjectToSql(customer);

    // customer.id is not an Identity, have to set it here.
    data[this.getSqlField(c => c.id)] = customer.id;

    const returnFields = this.getSelectFields();
    const createdCustomer = await this.knex(Customer.getTableName())
      .insert(data)
      .returning(returnFields)
      .get(0);
    return this._parseCustomer(createdCustomer);
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<number> {
    const rowsAffected = await this.knex
      .table(this.tableName)
      .delete()
      .where({
        [this.getSqlField(c => c.id)]: customerId,
      });
    return rowsAffected;
  }

  /** parses the object to be consumed by the client */
  private _parseCustomer(customer: PropsOf<Customer>): Customer {
    if (!customer) {
      return customer;
    }
    return {
      ...customer,
      features: JSON.parse(customer.features),
      hiddenFields: JSON.parse(customer.hiddenFields),
    };
  }
}
