import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import {
  CustomField,
  ConstructionSite,
  ConstructionSiteInspection,
  CustomFieldValueMap,
  BmpDetail,
  CustomFieldValue,
} from '../models';
import { EntityTypes } from '../shared';
import { CustomFieldRepository } from './custom-field.repository';
import { BmpDetailRepository } from './bmp-detail.repository';
import * as Knex from 'knex';
import { getSqlFields2 } from '../decorators/index';

const knex = container.get(Knex) as Knex;

describe('Custom Field Repository tests', async () => {
  const cfRepo = container.get(CustomFieldRepository);
  const customerId = 'LEWISVILLE';
  let createdCustomField: CustomField;

  it('should fetch', async () => {
    const customFields = await cfRepo.fetch(customerId);
    chai.expect(customFields).to.have.length.above(0);
    chai.expect(customFields[0]).to.haveOwnProperty('customFieldType');
  });

  it('should CREATE a custom field', async () => {
    const cf = _newCustomField();

    createdCustomField = await cfRepo.insert(cf, customerId);

    chai
      .expect(createdCustomField.customFieldType)
      .to.be.equal(cf.customFieldType);
    chai.expect(createdCustomField.stateAbbr).to.be.equal(cf.stateAbbr);
    chai.expect(createdCustomField.entityType).to.be.equal(cf.entityType);
    chai.expect(createdCustomField.dataType).to.be.equal(cf.dataType);
    chai.expect(createdCustomField.inputType).to.be.equal(cf.inputType);
    chai.expect(createdCustomField.fieldLabel).to.be.equal(cf.fieldLabel);
    chai.expect(createdCustomField.fieldOrder).to.be.equal(cf.fieldOrder);
  });

  it('should UPDATE a custom field', async () => {
    // assumed CREATE is done
    const cfId = createdCustomField.id as number;

    const updateCf: Partial<CustomField> = {
      customFieldType: 'State',
      stateAbbr: 'NY',
      entityType: EntityTypes.Facility,
      dataType: 'boolean',
      inputType: 'checkbox',
      fieldLabel: 'Label 2',
      fieldOptions: 'Option1|Option2',
      fieldOrder: 2,
    };

    await cfRepo.update(cfId, updateCf, customerId);
    const updatedCf = await cfRepo.fetchById(cfId, customerId);

    chai
      .expect(updatedCf.customFieldType)
      .to.be.equal(updateCf.customFieldType);
    chai.expect(updatedCf.stateAbbr).to.be.equal(updateCf.stateAbbr);
    chai.expect(updatedCf.entityType).to.be.equal(updateCf.entityType);
    chai.expect(updatedCf.dataType).to.be.equal(updateCf.dataType);
    chai.expect(updatedCf.inputType).to.be.equal(updateCf.inputType);
    chai.expect(updatedCf.fieldLabel).to.be.equal(updateCf.fieldLabel);
    chai.expect(updatedCf.fieldOrder).to.be.equal(updateCf.fieldOrder);
  });

  it('should soft DELETE custom field', async () => {
    const cfId = createdCustomField.id as number;
    const rows = await cfRepo.del(createdCustomField.id as number, customerId);
    chai.expect(rows).to.be.eq(1);
    const fetchedCf = await cfRepo.fetchById(cfId, customerId);
    chai.expect(fetchedCf).to.be.not.ok; // falsy
  });

  // cleanup
  after(async () => {
    if (createdCustomField) {
      await cfRepo.del(createdCustomField.id as number, customerId, true);
    }
  });
});

describe('Custom Field Repository - ConstructionSite value tests', async () => {
  const cfRepo = container.get(CustomFieldRepository);
  const customerId = 'LEWISVILLE';
  let createdCustomField: CustomField;
  const cf = _newCustomField();

  before(async () => {
    createdCustomField = await cfRepo.insert(cf, customerId);
  });

  it('should get custom field values', async () => {
    const customFields = await cfRepo.getCustomFieldValuesForEntity(
      ConstructionSite,
      4007,
      customerId
    );
    // assets
    chai.expect(customFields).length.greaterThan(0);
    const foundCustomField = customFields.find(
      c => c.id === (createdCustomField.id as number)
    );
    chai.expect(foundCustomField).to.be.ok;
  });

  it('shoud set custom field values for ConstructionSite', async () => {
    const value = 'some value';
    let cfv = new CustomFieldValueMap(createdCustomField.id as number, value);

    await cfRepo.updateCustomFieldsForEntity(
      ConstructionSite,
      4007,
      [cfv],
      customerId
    );

    const customFields = await cfRepo.getCustomFieldValuesForEntity(
      ConstructionSite,
      4007,
      customerId
    );

    const foundCustomField = customFields.find(
      c => c.id === (createdCustomField.id as number)
    );

    chai.expect(foundCustomField).to.be.ok;
    chai.expect(foundCustomField.value).to.be.eq(value);
  });

  after(async () => {
    if (createdCustomField) {
      await cfRepo.del(createdCustomField.id as number, customerId, true);
    }
  });
});

describe('Custom Field Repository - Construction Site Inspection value tests', async () => {
  const cfRepo = container.get(CustomFieldRepository);
  const customerId = 'LEWISVILLE';
  let createdCustomField: CustomField;
  const cf = _newInspectionCustomField();
  cf.entityType = EntityTypes.ConstructionSiteInspection; // set entity to CS Inspection

  const exampleAsset = 4007;
  const exampleInspection = 1193;

  before(async () => {
    const inspection = await knex(ConstructionSiteInspection.tableName)
      .where({
        [ConstructionSiteInspection.sqlField(f => f.id)]: exampleInspection,
      })
      .select(getSqlFields2(ConstructionSiteInspection, { asProperty: true }))
      .get<ConstructionSiteInspection>(0);
    cf.customFormTemplateId = inspection.customFormTemplateId;
    createdCustomField = await cfRepo.insert(cf, customerId);
  });

  it('should get custom field values', async () => {
    const inspection = await knex(ConstructionSiteInspection.tableName)
      .select(
        getSqlFields2(ConstructionSiteInspection, {
          asProperty: true,
          includeTable: true,
          includeInternalField: true,
        })
      )
      .join(
        ConstructionSite.tableName,
        ConstructionSite.sqlField(f => f.id, true),
        ConstructionSiteInspection.sqlField(f => f.constructionSiteId, true)
      )
      .where({
        [ConstructionSiteInspection.sqlField(f => f.isDeleted, true)]: 0,
        [ConstructionSiteInspection.sqlField(
          f => f.id,
          true
        )]: exampleInspection,
        [ConstructionSite.sqlField(f => f.isDeleted, true)]: 0,
        [ConstructionSite.sqlField(f => f.customerId, true)]: customerId,
      })
      .get<ConstructionSiteInspection>(0);

    const expectedCustomFieldValues: CustomFieldValue[] = await knex(
      CustomField.tableName
    )
      .select(
        ...getSqlFields2(CustomField, { includeTable: true, asProperty: true }),
        CustomFieldValue.sqlField(e => e.value, true, true)
      )
      .leftOuterJoin(CustomFieldValue.tableName, function() {
        this.on(
          CustomFieldValue.sqlField(f => f.customFieldId, true),
          CustomField.sqlField(f => f.id, true)
        ).andOn(
          CustomFieldValue.sqlField(f => f.globalIdFk, true),
          knex.raw('?', inspection.globalId)
        );
      })
      .where({
        [CustomField.sqlField(f => f.isDeleted, true)]: 0,
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.customFormTemplateId,
          true
        )]: inspection.customFormTemplateId,
      })
      .orderBy(CustomField.sqlField(f => f.fieldOrder, true));

    const actualCustomFieldValues = await cfRepo.getCustomFieldValuesForInspection(
      ConstructionSite,
      ConstructionSiteInspection,
      exampleAsset,
      exampleInspection,
      customerId
    );

    // assets
    chai.expect(actualCustomFieldValues.length).greaterThan(0);
    chai
      .expect(actualCustomFieldValues.length)
      .eq(expectedCustomFieldValues.length);

    expectedCustomFieldValues.forEach((ev, i) => {
      chai
        .expect(actualCustomFieldValues[i].customFieldId)
        .eq(ev.customFieldId);
      chai.expect(actualCustomFieldValues[i].value).eq(ev.value);
    });
  });

  it('shoud set custom field values for ConstructionSiteInspection', async () => {
    const value = 'some value 2';

    let cfv = new CustomFieldValueMap(createdCustomField.id as number, value);

    const rowsAffected1 = await cfRepo.updateCustomFieldValuesForInspection(
      ConstructionSiteInspection,
      exampleInspection,
      [cfv],
      customerId
    );

    const customFields = await cfRepo.getCustomFieldValuesForInspection(
      ConstructionSite,
      ConstructionSiteInspection,
      exampleAsset,
      exampleInspection,
      customerId
    );

    const foundCustomField = customFields.find(
      c => c.id === (createdCustomField.id as number)
    );

    chai.expect(foundCustomField).to.be.ok;
    chai.expect(foundCustomField.value).to.be.eq(value);
    chai.expect(rowsAffected1).eq(1);

    // Do a SQL update
    foundCustomField.value = 'some custom value 3';
    const rowsAffected2 = await cfRepo.updateCustomFieldValuesForInspection(
      ConstructionSiteInspection,
      exampleInspection,
      [
        {
          id: foundCustomField.id,
          value: foundCustomField.value,
        },
      ],
      customerId
    );

    const customFields2 = await cfRepo.getCustomFieldValuesForInspection(
      ConstructionSite,
      ConstructionSiteInspection,
      exampleAsset,
      exampleInspection,
      customerId
    );
    const foundCustomField2 = customFields2.find(
      c => c.id === (createdCustomField.id as number)
    );
    chai.expect(foundCustomField2).to.be.ok;
    chai.expect(foundCustomField2.value).to.be.eq('some custom value 3');
    chai.expect(rowsAffected2).eq(1);
  });

  // clean up
  after(async () => {
    if (createdCustomField) {
      await cfRepo.del(createdCustomField.id as number, customerId, true);
    }
  });
});

describe('Custom Field Repository - BmpDetail value tests', async () => {
  const cfRepo = container.get(CustomFieldRepository);
  const bmpDetailRepo = container.get(BmpDetailRepository);
  const customerId = 'LEWISVILLE';
  let createdCustomField: CustomField;
  let bmpDetail: BmpDetail;
  const cf = _newBmpDetailCustomField();

  before(async () => {
    createdCustomField = await cfRepo.insert(cf, customerId);
    const bmpDetails = await bmpDetailRepo.fetch(customerId);
    bmpDetail = bmpDetails[0];
  });

  it.skip('should get custom field values', async () => {
    const customFields = await cfRepo.getCustomFieldValuesForBmpDetail(
      customerId,
      bmpDetail.id
    );
    // assets
    chai.expect(customFields).length.greaterThan(0);
    const foundCustomField = customFields.find(
      c => c.id === (createdCustomField.id as number)
    );
    chai.expect(foundCustomField).to.be.ok;
  });

  it('should set custom field values for BmpDetail', async () => {
    const value = 'some value' + new Date().getTime();
    let cfv = new CustomFieldValueMap(createdCustomField.id as number, value);

    await cfRepo.updateCustomFieldsValuesForBmpDetail(
      bmpDetail.id,
      [cfv],
      customerId
    );

    const customFields = await cfRepo.getCustomFieldValuesForBmpDetail(
      customerId,
      bmpDetail.id
    );

    const foundCustomField = customFields.find(
      c => c.id === (createdCustomField.id as number)
    );

    chai.expect(foundCustomField).to.be.ok;
    chai.expect(foundCustomField.value).to.be.eq(value);
  });

  after(async () => {
    if (createdCustomField) {
      await cfRepo.del(createdCustomField.id as number, customerId, true);
    }
  });
});

function _newCustomField() {
  let cf = new CustomField();
  cf.customFieldType = 'Customer';
  cf.stateAbbr = 'TX';
  cf.entityType = EntityTypes.ConstructionSite;
  cf.dataType = 'string';
  cf.inputType = 'text';
  cf.fieldLabel = 'Test Custom Field';
  cf.fieldOrder = 1;
  return cf;
}

function _newInspectionCustomField() {
  let cf = new CustomField();
  cf.customFieldType = 'Customer';
  cf.stateAbbr = 'TX';
  cf.entityType = EntityTypes.ConstructionSiteInspection;
  cf.dataType = 'string';
  cf.inputType = 'text';
  cf.fieldLabel = 'Test Custom Field';
  cf.fieldOrder = 1;
  return cf;
}

function _newBmpDetailCustomField() {
  let cf = new CustomField();
  cf.customFieldType = 'Customer';
  cf.stateAbbr = 'TX';
  cf.entityType = EntityTypes.BmpDetail;
  cf.dataType = 'string';
  cf.inputType = 'text';
  cf.fieldLabel = 'Test Custom Field';
  cf.fieldOrder = 1;
  return cf;
}
