import * as Bluebird from 'bluebird';
import * as moment from 'moment';

import {
  ConstructionSite,
  ConstructionSiteInspection,
  CustomField,
  CustomFieldValue,
  CustomFormTemplate,
  InspectionType,
  User,
} from './../models';

import { AssetInspectionRepository } from './asset-inspection.repository';
import { EntityTypes } from '../shared/entity-types';
import { injectable } from 'inversify';

@injectable()
export class ConstructionSiteInspectionRepository extends AssetInspectionRepository<
  ConstructionSite,
  ConstructionSiteInspection
> {
  constructor() {
    super(ConstructionSiteInspection);
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      ConstructionSiteInspection.sqlField(c => c.id, true, true),
      ConstructionSiteInspection.sqlField(c => c.createdDate, true, true),
      ConstructionSiteInspection.sqlField(
        c => c.constructionSiteId,
        true,
        true
      ),
      `${ConstructionSite.sqlField(c => c.name, true)} as constructionSiteName`,
      `${ConstructionSite.sqlField(
        c => c.trackingId,
        true
      )} as constructionSiteTrackingId`,
      ConstructionSiteInspection.sqlField(c => c.inspectionDate, true, true),
      ConstructionSiteInspection.sqlField(
        c => c.scheduledInspectionDate,
        true,
        true
      ),
      ConstructionSiteInspection.sqlField(c => c.complianceStatus, true, true),
      ConstructionSiteInspection.sqlField(c => c.inspectorId, true, true),
      `inspector.${User.sqlField(u => u.givenName)} as inspectorFirstName`,
      `inspector.${User.sqlField(u => u.surname)} as inspectorLastName`,
      ConstructionSiteInspection.sqlField(
        c => c.followUpInspectionId,
        true,
        true
      ),
      ConstructionSiteInspection.sqlField(
        c => c.additionalInformation,
        true,
        true
      ),

      // weather
      ConstructionSiteInspection.sqlField(c => c.weatherCondition, true, true),
      ConstructionSiteInspection.sqlField(
        c => c.weatherTemperatureF,
        true,
        true
      ),
      ConstructionSiteInspection.sqlField(
        c => c.weatherPrecipitationIn,
        true,
        true
      ),
      ConstructionSiteInspection.sqlField(c => c.weatherLast72, true, true),
      ConstructionSiteInspection.sqlField(c => c.weatherLast24, true, true),
      ConstructionSiteInspection.sqlField(c => c.windDirection, true, true),
      ConstructionSiteInspection.sqlField(c => c.windSpeedMph, true, true),
      ConstructionSiteInspection.sqlField(c => c.weatherStationId, true, true),
      ConstructionSiteInspection.sqlField(c => c.weatherDateTime, true, true),
      ConstructionSiteInspection.sqlField(c => c.timeIn, true, true),
      ConstructionSiteInspection.sqlField(c => c.timeOut, true, true),

      ConstructionSiteInspection.sqlField(
        c => c.customFormTemplateId,
        true,
        true
      ),
      ConstructionSiteInspection.sqlField(c => c.inspectionTypeId, true, true),
      `${InspectionType.sqlField(i => i.name, true)} as inspectionType`,
    ];

    return await this.fetchBaseQueryBuilder(customerId)
      .leftOuterJoin(
        `${User.getTableName()} as inspector`,
        `inspector.${User.sqlField(u => u.id)}`,
        ConstructionSiteInspection.sqlField(i => i.inspectorId, true)
      )
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${ConstructionSiteInspection.sqlField(
        c => c.id,
        true
      )} as constructionSiteInspectionId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
      ConstructionSiteInspection.sqlField(
        c => c.customFormTemplateId,
        true,
        true
      ),
      CustomFormTemplate.sqlField(ct => ct.name, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        ConstructionSiteInspection.getTableName(),
        ConstructionSiteInspection.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        ConstructionSite.getTableName(),
        ConstructionSite.sqlField(cs => cs.id, true),
        ConstructionSiteInspection.sqlField(i => i.constructionSiteId, true)
      )
      .join(
        CustomFormTemplate.getTableName(),
        CustomFormTemplate.sqlField(ct => ct.id, true),
        ConstructionSiteInspection.sqlField(i => i.customFormTemplateId, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.ConstructionSiteInspection
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(ConstructionSiteInspection.sqlField(f => f.isDeleted, true), 0)
      .where(ConstructionSite.sqlField(f => f.isDeleted, true), 0)
      .where(ConstructionSite.sqlField(f => f.customerId, true), customerId)
      .orderBy(ConstructionSiteInspection.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true))
      .timeout(60000);
    return await qb;
  }

  /*
    Table structure for pastDueInspectionsReport and upcomingInspectionsReport.
    Lines denote how tables are joined, *'s are selected keys.
<<<<<<< HEAD
=======

>>>>>>> feat: SWMP, BMP, MG APIs
      ConstructionSiteInspections   ConstructionSites   Users     UserCustomers   Customers
                *Id                      *Name
          *ConstructionId ---------------- Id      _-------------------------------- Id
        ScheduledInspectionDate        *CustomerId --------------- CustomerId     *DailyInspectorEmail
            *InspectionDate                             Id ------- *UserId       *MondayInspectorEmail
                                                      *Email        *Role         *MondayAdminEmail
                                                      *GivenName
                                                      *Surname
            *InspectorId ------------------------------ Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
            *InspectorId2 ----------------------------- Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
      */

  async pastDueInspectionsReport() {
    const start = '2019-9-22';
    const end = moment()
      .utc()
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'ConstructionSiteInspections.Id as inspectionId',
        'ConstructionSiteInspections.ConstructionSiteId as siteId',
        'ConstructionSiteInspections.InspectionDate as inspectionDate',
        'ConstructionSiteInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'ConstructionSiteInspections.InspectorId as inspectorId',
        'ConstructionSiteInspections.InspectorId2 as inspectorId2',
        'ConstructionSites.Name as name',
        'ConstructionSites.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('ConstructionSiteInspections.IsDeleted', '=', 'false')
      .where('ConstructionSites.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('ConstructionSites', {
        'ConstructionSiteInspections.ConstructionSiteId':
          'ConstructionSites.Id',
      })
      .leftJoin('Users', {
        'ConstructionSiteInspections.InspectorId': 'Users.Id',
      })
      .leftJoin('Users as Users2', {
        'ConstructionSiteInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'ConstructionSites.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', {
        'ConstructionSites.CustomerId': 'Customers.Id',
      });
  }

  async upcomingInspectionsReport() {
    const start = moment()
      .utc()
      .format('YYYY-MM-DD');
    const end = moment()
      .utc()
      .add(7, 'days')
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'ConstructionSiteInspections.Id as inspectionId',
        'ConstructionSiteInspections.ConstructionSiteId as siteId',
        'ConstructionSiteInspections.InspectionDate as inspectionDate',
        'ConstructionSiteInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'ConstructionSiteInspections.InspectorId as inspectorId',
        'ConstructionSiteInspections.InspectorId2 as inspectorId2',
        'ConstructionSites.Name as name',
        'ConstructionSites.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('ConstructionSiteInspections.IsDeleted', '=', 'false')
      .where('ConstructionSites.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('ConstructionSites', {
        'ConstructionSiteInspections.ConstructionSiteId':
          'ConstructionSites.Id',
      })
      .leftJoin('Users', {
        'ConstructionSiteInspections.InspectorId': 'Users.Id',
      })
      .leftJoin('Users as Users2', {
        'ConstructionSiteInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'ConstructionSites.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', {
        'ConstructionSites.CustomerId': 'Customers.Id',
      });
  }
}
