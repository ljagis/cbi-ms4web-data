import { Repository } from './repository';
import { QueryOptions, BmpControlMeasure } from '../models';
import { injectable } from 'inversify';
import { getSqlField, getSqlFields } from '../decorators';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

@injectable()
export class BmpControlMeasureRepository extends Repository<BmpControlMeasure> {
  constructor() {
    super(BmpControlMeasure);
  }

  fetch(
    customer: string,
    queryOptions?: QueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpControlMeasure[]> {
    queryOptions = queryOptions || {};
    let queryBuilder = this.knex(this.tableName).select(this.defaultSelect);
    if (queryOptions.limit > 0) {
      queryBuilder.limit(queryOptions.limit);
    }
    if (queryBuilder.offset) {
      queryBuilder.offset(queryOptions.offset);
    }

    if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSql, queryOptions.orderByDirection);
    }
    queryBuilder
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.customerId),
        customer
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.isDeleted),
        0
      );
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilderOverrides(queryBuilder);
    }

    return queryBuilder.map<BmpControlMeasure, BmpControlMeasure>(l =>
      this.mapObjectToEntityClass(l)
    );
  }

  fetchById(
    id: number,
    customer: string,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<BmpControlMeasure> {
    let queryBuilder = this.knex
      .select(this.defaultSelect)
      .from(this.tableName)
      .where(this.idFieldSql, id)
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.customerId),
        customer
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.isDeleted),
        0
      );
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    return queryBuilder
      .map<BmpControlMeasure, BmpControlMeasure>(l =>
        this.mapObjectToEntityClass(l)
      )
      .get<BmpControlMeasure>(0);
  }

  insert(
    controlMeasure: Partial<BmpControlMeasure>,
    customerId: string
  ): Bluebird<BmpControlMeasure> {
    controlMeasure.customerId = customerId; // ensure customerId
    const data = this.mapObjectToSql(controlMeasure);
    let returnFields = getSqlFields(this.entity, undefined, false, true);
    const queryBuilder = this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return queryBuilder
      .map(e => this.mapObjectToEntityClass(e))
      .get<BmpControlMeasure>(0);
  }

  update(
    id: number,
    bmp: BmpControlMeasure,
    customerId: string
  ): Bluebird<void> {
    const data = this.mapObjectToSql(bmp);
    const qb = this.knex(this.tableName)
      .update(data)
      .where(getSqlField<BmpControlMeasure>(this.entity, c => c.id), id)
      .where(
        getSqlField<BmpControlMeasure>(this.entity, c => c.customerId),
        customerId
      );
    return qb;
  }

  del(id: number, customerId: string): Bluebird<any> {
    let trans = this.knex.transaction(trx => {
      let qb = this.knex(this.tableName)
        .update(this.getSqlField(b => b.isDeleted), 1)
        .where(this.getSqlField(c => c.id), id)
        .where(this.getSqlField(c => c.customerId), customerId);
      return qb.transacting(trx);
    });

    return trans;
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      BmpControlMeasure.sqlField(a => a.id, true, true),
      BmpControlMeasure.sqlField(a => a.name, true, true),
      BmpControlMeasure.sqlField(a => a.sort, true, true),
    ];
    return this.knex(BmpControlMeasure.getTableName())
      .where(BmpControlMeasure.sqlField(c => c.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }
}
