import { CustomerLookupRepository } from './customer-lookup.repository';
import { ReceivingWater } from '../models';
import { injectable } from 'inversify';

@injectable()
export class ReceivingWaterRepository extends CustomerLookupRepository<
  ReceivingWater
> {
  constructor() {
    super(ReceivingWater);
  }
}
