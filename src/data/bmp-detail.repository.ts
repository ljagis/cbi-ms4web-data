//#region imports
import { injectable, inject } from 'inversify';
import * as Knex from 'knex';
import {
  BmpDetail,
  QueryBuilderOverride,
  Creatable,
  Fetchable,
  Updateable,
  CrudDeletable,
  BmpControlMeasure,
  DeleteOptions,
  CustomField,
  CustomFieldValue,
} from '../models';
import { getSqlFields2 } from '../decorators';
import { objectToSqlData } from '../shared/object-to-sql';
import * as Bluebird from 'bluebird';
import { fetchEntities } from './shared';
import { EntityTypes } from '../shared/index';
//#endregion

@injectable()
export class BmpDetailRepository
  implements Creatable<BmpDetail>,
    Fetchable<BmpDetail>,
    Updateable<BmpDetail>,
    CrudDeletable {
  constructor(@inject(Knex) protected knex: Knex) {}

  async insert(
    customerId: string,
    detail: Partial<BmpDetail>
  ): Bluebird<BmpDetail> {
    const data = objectToSqlData(BmpDetail, detail, this.knex);
    const returnFields = getSqlFields2(BmpDetail, { asProperty: true });
    const qb = this.knex(BmpDetail.getTableName())
      .insert(data)
      .whereIn(BmpDetail.sqlField(f => f.controlMeasureId, true), function(
        this: Knex
      ) {
        this.from(BmpControlMeasure.getTableName())
          .select(BmpControlMeasure.sqlField(f => f.id, true))
          .where(
            BmpControlMeasure.sqlField(f => f.customerId, true),
            customerId
          )
          .where(BmpControlMeasure.sqlField(f => f.isDeleted, true), 0);
      })
      .returning(returnFields)
      .get<BmpDetail>(0);
    return qb;
  }

  async fetch(
    customerId: string,
    queryBuilderOverrides?: QueryBuilderOverride
  ): Bluebird<BmpDetail[]> {
    const qb = fetchEntities(BmpDetail, this.knex)
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where(BmpControlMeasure.sqlField(d => d.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(d => d.isDeleted, true), 0)
      .where(BmpDetail.sqlField(d => d.isDeleted, true), 0);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return qb;
  }

  async fetchById(
    customerId: string,
    id: number,
    queryBuilderOverrides?: QueryBuilderOverride
  ): Bluebird<BmpDetail> {
    const qb = fetchEntities(BmpDetail, this.knex)
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(f => f.id, true),
        BmpDetail.sqlField(f => f.controlMeasureId, true)
      )
      .where(BmpDetail.sqlField(d => d.id, true), id)
      .where(BmpControlMeasure.sqlField(d => d.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(d => d.isDeleted, true), 0)
      .where(BmpDetail.sqlField(d => d.isDeleted, true), 0);
    if (queryBuilderOverrides) {
      queryBuilderOverrides(qb);
    }
    return qb.get<BmpDetail>(0);
  }

  async update(
    customerId: string,
    id: number,
    dataType: Partial<BmpDetail>
  ): Bluebird<BmpDetail> {
    const data = objectToSqlData(BmpDetail, dataType, this.knex);
    const returnFields = getSqlFields2(BmpDetail, { asProperty: true });
    const qb = this.knex(BmpDetail.getTableName())
      .update(data)
      .whereIn(BmpDetail.sqlField(f => f.controlMeasureId, true), function(
        this: Knex
      ) {
        this.from(BmpControlMeasure.getTableName())
          .select(BmpControlMeasure.sqlField(f => f.id, true))
          .where(
            BmpControlMeasure.sqlField(f => f.customerId, true),
            customerId
          )
          .where(BmpControlMeasure.sqlField(f => f.isDeleted, true), 0);
      })
      .where(BmpDetail.sqlField(d => d.id, true), id)
      .returning(returnFields);
    return qb;
  }

  async delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<number> {
    const qb = this.knex(BmpDetail.getTableName());
    if (options && options.force) {
      qb.del();
    } else {
      qb.update(BmpDetail.sqlField(f => f.isDeleted), 1);
    }
    qb
      .whereIn(BmpDetail.sqlField(f => f.controlMeasureId, true), function(
        this: Knex
      ) {
        this.from(BmpControlMeasure.getTableName())
          .select(BmpControlMeasure.sqlField(f => f.id, true))
          .where(
            BmpControlMeasure.sqlField(f => f.customerId, true),
            customerId
          )
          .where(BmpControlMeasure.sqlField(f => f.isDeleted, true), 0);
      })
      .where(BmpDetail.sqlField(f => f.id), id);
    return qb;
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      BmpDetail.sqlField(a => a.id, true, true),
      BmpDetail.sqlField(a => a.name, true, true),
      BmpDetail.sqlField(a => a.description, true, true),
      BmpDetail.sqlField(a => a.dateAdded, true, true),
      BmpDetail.sqlField(a => a.dueDate, true, true),
      BmpDetail.sqlField(a => a.completionDate, true, true),
      BmpDetail.sqlField(a => a.controlMeasureId, true, true),
      `${BmpControlMeasure.sqlField(
        cm => cm.name,
        true
      )} as controlMeasureName`,
    ];
    return await this.knex(BmpDetail.getTableName())
      .leftOuterJoin(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(p => p.id, true),
        BmpDetail.sqlField(c => c.controlMeasureId, true)
      )
      .where(BmpDetail.sqlField(c => c.isDeleted, true), 0)
      .where(BmpControlMeasure.sqlField(c => c.customerId, true), customerId)
      .where(BmpControlMeasure.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${BmpDetail.sqlField(c => c.id, true)} as bmpDetailId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        BmpDetail.getTableName(),
        BmpDetail.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        BmpControlMeasure.getTableName(),
        BmpControlMeasure.sqlField(cs => cs.id, true),
        BmpDetail.sqlField(v => v.controlMeasureId, true)
      )
      .where({
        [CustomField.sqlField(f => f.isDeleted, true)]: 0,
        [CustomField.sqlField(f => f.entityType, true)]: EntityTypes.BmpDetail,
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [BmpDetail.sqlField(f => f.isDeleted, true)]: 0,
        [BmpControlMeasure.sqlField(f => f.isDeleted, true)]: 0,
      })
      .orderBy(BmpDetail.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true));
    return qb;
  }
}
