import { MonitoringLocation } from '../models';
import { AssetRepository } from './asset.repository';

import { injectable } from 'inversify';

@injectable()
export class MonitoringLocationRepository extends AssetRepository<
  MonitoringLocation
> {
  constructor() {
    super(MonitoringLocation);
  }
}
