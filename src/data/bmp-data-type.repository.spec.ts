import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpDataTypeRepository } from './bmp-data-type.repository';
import { BmpDataType } from '../models';
// import * as Knex from 'knex';

const customerId = 'LEWISVILLE';

describe('BMP Data Type', async () => {
  const dataTypeRepo = container.get(BmpDataTypeRepository);
  // const knex = kernel.get<Knex>(Knex);

  let createdId: number;
  const dataTypeName = `Miles Driven ${new Date().getTime()}`;

  // Cleanup
  after(async () => {
    if (createdId > 0) {
      await dataTypeRepo.delete(customerId, createdId, { force: true });
    }
  });

  describe(`create`, async () => {
    it(`should create data`, async () => {
      const dataType: Partial<BmpDataType> = {
        name: dataTypeName,
      };

      const data = await dataTypeRepo.insert(customerId, dataType);
      createdId = data.id;
      // chai.expect(data.customerId).eq(customerId);
      chai.expect(data.id).greaterThan(0);
      chai.expect(data.name).eq(dataTypeName);
    });
  });

  describe(`fetch `, async () => {
    it(`should fetch all`, async () => {
      const dataTypes = await dataTypeRepo.fetch(customerId);

      const dataType = dataTypes.find(t => t.name === dataTypeName);

      chai.expect(dataTypes).to.be.instanceOf(Array);
      chai.expect(dataTypes).to.have.length.greaterThan(0);
      chai.expect(dataType.name).eq(dataTypeName);
    });

    it(`should fetch by id`, async () => {
      chai.expect(createdId).greaterThan(0);

      const dataType = await dataTypeRepo.fetchById(customerId, createdId);

      chai.expect(dataType).not.undefined;
      chai.expect(dataType.name).eq(dataTypeName);
    });
  });

  describe(`update `, async () => {
    it(`should update`, async () => {
      chai.expect(createdId).greaterThan(0);
      const newDataTypename = dataTypeName + 'updated';
      await dataTypeRepo.update(customerId, createdId, {
        name: newDataTypename,
      });

      const dataTypes = await dataTypeRepo.fetch(customerId);

      const dataType = dataTypes.find(t => t.name === newDataTypename);

      chai.expect(dataTypes).to.be.instanceOf(Array);
      chai.expect(dataTypes).to.have.length.greaterThan(0);
      chai.expect(dataType.name).eq(newDataTypename);
    });
  });

  describe(`soft delete`, async () => {
    it(`should soft delete`, async () => {
      chai.expect(createdId).greaterThan(0);
      await dataTypeRepo.delete(customerId, createdId);

      const dataType = await dataTypeRepo.fetchById(customerId, createdId);
      chai.expect(dataType).not.ok;
    });
  });
});
