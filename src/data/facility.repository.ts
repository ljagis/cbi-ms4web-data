import * as Bluebird from 'bluebird';
import * as Knex from 'knex';

import {
  Community,
  Contact,
  CustomField,
  CustomFieldValue,
  EntityContact,
  Facility,
  FacilityQueryOptions,
  InspectionFrequency,
  ReceivingWater,
  Watershed,
} from '../models';

import { AssetRepository } from './asset.repository';
import { Defaults } from '../shared';
import { EntityTypes } from '../shared/entity-types';
import { FetchAsset } from '../interfaces';
import { getSqlFields2 } from '../decorators';
import { injectable } from 'inversify';

@injectable()
export class FacilityRepository extends AssetRepository<Facility>
  implements FetchAsset<Facility> {
  constructor() {
    super(Facility);
  }

  async fetch(
    customerId: string,
    queryOptions?: FacilityQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Bluebird<Facility[]> {
    return await this.getFetchQueryBuilder(
      customerId,
      queryOptions,
      queryBuilderOverrides
    ).map(e => this.mapObjectToEntityClass(e));
  }

  async fetchById(
    id: number,
    customerId: string,
    queryOptions?: FacilityQueryOptions
  ): Bluebird<Facility> {
    const queryBuilder = this.getFetchQueryBuilder(
      customerId,
      queryOptions
    ).where(Facility.sqlField(f => f.id, true), id);
    return await queryBuilder
      .map<Facility, Facility>(asset => this.mapObjectToEntityClass(asset))
      .get<Facility>(0); // first
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      Facility.sqlField(c => c.id, true, true),
      Facility.sqlField(c => c.dateAdded, true, true),
      Facility.sqlField(c => c.trackingId, true, true),
      Facility.sqlField(c => c.name, true, true),
      Facility.sqlField(c => c.physicalAddress1, true, true),
      Facility.sqlField(c => c.physicalAddress2, true, true),
      Facility.sqlField(c => c.physicalCity, true, true),
      Facility.sqlField(c => c.physicalState, true, true),
      Facility.sqlField(c => c.physicalZip, true, true),

      Facility.sqlField(c => c.complianceStatus, true, true),
      Facility.sqlField(c => c.sector, true, true),
      Facility.sqlField(c => c.sicCode, true, true),
      Facility.sqlField(c => c.isHighRisk, true, true),
      Facility.sqlField(c => c.inspectionFrequencyId, true, true),
      `${InspectionFrequency.sqlField(
        p => p.name,
        true
      )} as inspectionFrequencyName`,
      Facility.sqlField(c => c.yearBuilt, true, true),
      Facility.sqlField(c => c.squareFootage, true, true),

      Facility.sqlField(c => c.additionalInformation, true, true),

      Facility.sqlField(c => c.lat, true, true),
      Facility.sqlField(c => c.lng, true, true),

      Facility.sqlField(c => c.communityId, true, true),
      `${Community.sqlField(c => c.name, true)} as communityName`,
      Facility.sqlField(c => c.watershedId, true, true),
      `${Watershed.sqlField(c => c.name, true)} as watershedName`,
      Facility.sqlField(c => c.subwatershedId, true, true),
      `SubWatershed.${Watershed.sqlField(c => c.name)} as subWatershedName`,
      Facility.sqlField(c => c.receivingWatersId, true, true),
      `${ReceivingWater.sqlField(c => c.name, true)} as receivingWaterName`,

      Facility.sqlField(c => c.latestInspectionDate, true, true),
    ];
    return await this.knex(this.tableName)
      .leftOuterJoin(
        InspectionFrequency.getTableName(),
        InspectionFrequency.sqlField(p => p.id, true),
        Facility.sqlField(c => c.inspectionFrequencyId, true)
      )
      .leftOuterJoin(
        Community.getTableName(),
        Community.sqlField(p => p.id, true),
        Facility.sqlField(c => c.communityId, true)
      )
      .leftOuterJoin(
        Watershed.getTableName(),
        Watershed.sqlField(p => p.id, true),
        Facility.sqlField(c => c.watershedId, true)
      )
      .leftOuterJoin(
        `${Watershed.getTableName()} as SubWatershed`,
        `SubWatershed.${Watershed.sqlField(p => p.id)}`,
        Facility.sqlField(c => c.subwatershedId, true)
      )
      .leftOuterJoin(
        ReceivingWater.getTableName(),
        ReceivingWater.sqlField(p => p.id, true),
        Facility.sqlField(c => c.receivingWatersId, true)
      )
      .where(Facility.sqlField(c => c.customerId, true), customerId)
      .where(Facility.sqlField(c => c.isDeleted, true), 0)
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${Facility.sqlField(c => c.id, true)} as facilityId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        Facility.getTableName(),
        Facility.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.Facility
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(Facility.sqlField(f => f.isDeleted, true), 0)
      .where(Facility.sqlField(f => f.customerId, true), customerId)
      .orderBy(Facility.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true));
    return await qb;
  }

  async createSampleFacility(customerId: string): Bluebird<Facility> {
    const sampleAsset: Facility = {
      ...new Facility(),
      customerId,
      name: 'Sample Facility',
      complianceStatus: 'Compliant',
      lat: 29.760138,
      lng: -95.369371, // Houston City Hall
    };
    const data = this.objectToSqlData(Facility, sampleAsset, this.knex);
    const returnFields = getSqlFields2(Facility, { asProperty: true });
    const returnedData = await this.knex(this.tableName)
      .insert(data)
      .returning(returnFields);
    return returnedData;
  }

  /**
   * PRIVATES ---------
   */

  private getFetchQueryBuilder(
    customerId: string,
    queryOptions?: FacilityQueryOptions,
    queryBuilderOverrides?: ((qb: Knex.QueryBuilder) => Knex.QueryBuilder)
  ): Knex.QueryBuilder {
    queryOptions = queryOptions || {};
    const limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;

    let queryBuilder = this.knex(this.tableName)
      .select(
        !queryOptions.includeLastInspector
          ? this.defaultSelect
          : [
              ...this.defaultSelect,
              'LastInspection.InspectorId AS inspectorId',
              'LastInspection.InspectorId2 AS inspectorId2',
            ]
      )
      .limit(limit)
      .offset(queryOptions.offset);

    if (queryOptions.ids && queryOptions.ids.length) {
      queryBuilder.whereIn(
        Facility.sqlField(f => f.id, true),
        queryOptions.ids
      );
    }

    if (queryOptions.contractorEmail) {
      queryBuilder = this._appendContractorEmail(
        queryBuilder,
        queryOptions.contractorEmail
      );
    }

    if (queryOptions.includeLastInspector) {
      queryBuilder = this._appendLastInspector(queryBuilder);
    }

    if (queryOptions.addedFrom || queryOptions.addedTo) {
      queryBuilder.where(function() {
        // added from, to
        if (queryOptions.addedFrom || queryOptions.addedTo) {
          this.orWhere(function() {
            if (queryOptions.addedFrom) {
              this.where('DateAdded', '>=', queryOptions.addedFrom);
            }
            if (queryOptions.addedTo) {
              this.where('DateAdded', '<=', queryOptions.addedTo);
            }
          });
        }
      });
    }

    if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
      queryBuilder.where(function() {
        if (queryOptions.inspectedFrom || queryOptions.inspectedTo) {
          this.orWhere(function() {
            if (queryOptions.inspectedFrom) {
              this.where(
                'LatestInspectionDate',
                '>=',
                queryOptions.inspectedFrom
              );
            }
            if (queryOptions.inspectedTo) {
              this.where(
                'LatestInspectionDate',
                '<=',
                queryOptions.inspectedTo
              );
            }
          });
        }
      });
    }

    if (queryOptions.orderByRaw) {
      queryBuilder.orderByRaw(queryOptions.orderByRaw);
    } else if (queryOptions.orderByField) {
      queryBuilder.orderBy(
        this.getSqlField(queryOptions.orderByField as any),
        queryOptions.orderByDirection
      );
    } else {
      queryBuilder.orderBy(this.idFieldSqlFull, queryOptions.orderByDirection);
    }
    if (typeof queryBuilderOverrides === 'function') {
      queryBuilder = queryBuilderOverrides(queryBuilder);
    }
    queryBuilder
      .where(this.getSqlField(e => e.customerId, true), customerId)
      .where(this.getSqlField(e => e.isDeleted, true), 0);
    return queryBuilder;
  }

  private _appendContractorEmail(qb: Knex.QueryBuilder, email: string) {
    return qb
      .join(
        EntityContact.tableName,
        EntityContact.sqlField(f => f.globalIdFk, true),
        Facility.sqlField(f => f.globalId, true)
      )
      .join(
        Contact.tableName,
        Contact.sqlField(f => f.id, true),
        EntityContact.sqlField(f => f.contactId, true)
      )
      .where({
        [Contact.sqlField(f => f.email, true)]: email,
        [Contact.sqlField(f => f.isDeleted, true)]: 0,
      });
  }

  private _appendLastInspector(qb: Knex.QueryBuilder) {
    return qb.joinRaw(`
        LEFT JOIN
          (
            SELECT MIN(FI1.Id) AS Id, FI1.FacilityId
            FROM FacilityInspections FI1
            JOIN
              (
                SELECT FacilityId, MAX(InspectionDate) AS latestDate
                FROM FacilityInspections
                GROUP BY FacilityId
              ) FI2
              ON FI1.FacilityId = FI2.FacilityId
                AND FI1.InspectionDate = FI2.latestDate
            GROUP BY FI1.FacilityId
          ) LID
          ON LID.FacilityId = Facilities.Id
        LEFT JOIN FacilityInspections LastInspection
          ON LastInspection.Id = LID.Id
      `);
  }
}
