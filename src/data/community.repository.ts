import { CustomerLookupRepository } from './customer-lookup.repository';
import { Community } from '../models';
import { injectable } from 'inversify';

@injectable()
export class CommunityRepository extends CustomerLookupRepository<Community> {
  constructor() {
    super(Community);
  }
}
