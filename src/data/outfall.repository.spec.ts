import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { OutfallRepository } from './outfall.repository';
import * as Knex from 'knex';

describe('Outfall Repository Tests', () => {
  const repo = container.get(OutfallRepository);
  const knex: Knex = container.get(Knex);
  const customerId = 'LEWISVILLE';

  describe(`exportData tests #OutfallRepository`, () => {
    it(`should export data`, async () => {
      const outfalls = await repo.exportData(customerId);

      const outfall = outfalls[0];

      chai.expect(outfalls).to.be.instanceOf(Array);
      chai.expect(outfalls).to.have.length.greaterThan(0);

      // TODO: test for properties exported.
      chai.expect(outfall).to.have.property('id');
      chai.expect(outfall).to.have.property('location');
      chai.expect(outfall).to.have.property('nearAddress');
    });
  });

  describe(`exportCustomFields #OutfallRepository`, () => {
    it(`should get custom field values`, async () => {
      const cfvs = await repo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('outfallId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });

  describe(`fetch by contractorEmail`, async () => {
    it(`should fetch by contractorEmail`, async () => {
      const expectedSql = `
      SELECT
  count (*) as count
FROM
  Outfalls cs INNER JOIN EntityContacts ec ON cs.GlobalId = ec.GlobalIdFk INNER JOIN Contacts c ON c.Id = ec.ContactId
WHERE
  cs.IsDeleted=0
  AND
  c.IsDeleted=0
  AND
  c.Email = 'alexng99+contractor@gmail.com'`;
      const expected: any[] = await knex.raw(expectedSql);
      const contractorEmail = 'alexng99+contractor@gmail.com';
      const sites = await repo.fetch(customerId, { contractorEmail });
      chai.expect(sites.length).eq(expected[0].count);
    });
  });
});
