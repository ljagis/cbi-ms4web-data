import * as Bluebird from 'bluebird';
import * as moment from 'moment';

import {
  CustomField,
  CustomFieldValue,
  CustomFormTemplate,
  InspectionType,
  Outfall,
  OutfallInspection,
  User,
} from './../models';

import { AssetInspectionRepository } from './asset-inspection.repository';
import { EntityTypes } from '../shared/entity-types';
import { injectable } from 'inversify';

@injectable()
export class OutfallInspectionRepository extends AssetInspectionRepository<
  Outfall,
  OutfallInspection
> {
  constructor() {
    super(OutfallInspection);
  }

  async exportData(customerId: string): Bluebird<any[]> {
    const exportFields = [
      OutfallInspection.sqlField(c => c.id, true, true),
      OutfallInspection.sqlField(c => c.outfallId, true, true),
      `${Outfall.sqlField(c => c.location, true)} as outfallLocation`,
      `${Outfall.sqlField(c => c.nearAddress, true)} as outfallNearAddresss`,
      `${Outfall.sqlField(c => c.trackingId, true)} as outfallTrackingId`,
      OutfallInspection.sqlField(c => c.createdDate, true, true),
      OutfallInspection.sqlField(c => c.inspectionDate, true, true),
      OutfallInspection.sqlField(c => c.scheduledInspectionDate, true, true),
      OutfallInspection.sqlField(c => c.complianceStatus, true, true),
      OutfallInspection.sqlField(c => c.inspectorId, true, true),
      `inspector.${User.sqlField(u => u.givenName)} as inspectorFirstName`,
      `inspector.${User.sqlField(u => u.surname)} as inspectorLastName`,
      OutfallInspection.sqlField(c => c.followUpInspectionId, true, true),
      OutfallInspection.sqlField(c => c.additionalInformation, true, true),

      // weather
      OutfallInspection.sqlField(c => c.weatherCondition, true, true),
      OutfallInspection.sqlField(c => c.weatherTemperatureF, true, true),
      OutfallInspection.sqlField(c => c.weatherPrecipitationIn, true, true),
      OutfallInspection.sqlField(c => c.weatherLast72, true, true),
      OutfallInspection.sqlField(c => c.weatherLast24, true, true),
      OutfallInspection.sqlField(c => c.windDirection, true, true),
      OutfallInspection.sqlField(c => c.windSpeedMph, true, true),
      OutfallInspection.sqlField(c => c.weatherStationId, true, true),
      OutfallInspection.sqlField(c => c.weatherDateTime, true, true),
      OutfallInspection.sqlField(c => c.timeIn, true, true),
      OutfallInspection.sqlField(c => c.timeOut, true, true),

      OutfallInspection.sqlField(c => c.customFormTemplateId, true, true),
      OutfallInspection.sqlField(c => c.inspectionTypeId, true, true),
      `${InspectionType.sqlField(i => i.name, true)} as inspectionType`,
    ];
    return await this.fetchBaseQueryBuilder(customerId)
      .leftOuterJoin(
        `${User.getTableName()} as inspector`,
        `inspector.${User.sqlField(u => u.id)}`,
        OutfallInspection.sqlField(i => i.inspectorId, true)
      )
      .select(exportFields);
  }

  async exportCustomFields(customerId: string): Bluebird<any[]> {
    const exportFields = [
      `${OutfallInspection.sqlField(c => c.id, true)} as outfallInspectionId`,
      CustomField.sqlField(c => c.fieldLabel, true, true),
      CustomFieldValue.sqlField(v => v.value, true, true),
      OutfallInspection.sqlField(c => c.customFormTemplateId, true, true),
      CustomFormTemplate.sqlField(ct => ct.name, true, true),
    ];

    // TODO: handle state fields export.
    const qb = this.knex(CustomField.getTableName())
      .select(exportFields)
      .leftOuterJoin(
        CustomFieldValue.getTableName(),
        CustomFieldValue.sqlField(v => v.customFieldId, true),
        CustomField.sqlField(c => c.id, true)
      )
      .join(
        OutfallInspection.getTableName(),
        OutfallInspection.sqlField(cs => cs.globalId, true),
        CustomFieldValue.sqlField(v => v.globalIdFk, true)
      )
      .join(
        Outfall.getTableName(),
        Outfall.sqlField(cs => cs.id, true),
        OutfallInspection.sqlField(i => i.outfallId, true)
      )
      .join(
        CustomFormTemplate.getTableName(),
        CustomFormTemplate.sqlField(ct => ct.id, true),
        OutfallInspection.sqlField(i => i.customFormTemplateId, true)
      )
      .where(CustomField.sqlField(f => f.isDeleted, true), 0)
      .where(
        CustomField.sqlField(f => f.entityType, true),
        EntityTypes.OutfallInspection
      )
      .where(CustomField.sqlField(f => f.customerId, true), customerId)
      .where(OutfallInspection.sqlField(f => f.isDeleted, true), 0)
      .where(Outfall.sqlField(f => f.isDeleted, true), 0)
      .where(Outfall.sqlField(f => f.customerId, true), customerId)
      .orderBy(OutfallInspection.sqlField(c => c.id, true))
      .orderBy(CustomField.sqlField(c => c.fieldOrder, true))
      .orderBy(CustomField.sqlField(c => c.fieldLabel, true))
      .timeout(60000);
    return await qb;
  }

  /*
    Table structure for pastDueInspectionsReport and upcomingInspectionsReport.
    Lines denote how tables are joined, *'s are selected keys.
<<<<<<< HEAD
=======

>>>>>>> feat: SWMP, BMP, MG APIs
          OutfallInspections              Outfalls      Users     UserCustomers   Customers
                *Id                      *Location
          *ConstructionId ---------------- Id      _-------------------------------- Id
        ScheduledInspectionDate        *CustomerId --------------- CustomerId     *DailyInspectorEmail
            *InspectionDate                             Id ------- *UserId       *MondayInspectorEmail
                                                      *Email        *Role         *MondayAdminEmail
                                                      *GivenName
                                                      *Surname
            *InspectorId ------------------------------ Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
            *InspectorId2 ----------------------------- Id
                                                      *Email
                                                      *GivenName
                                                      *Surname
      */

  async pastDueInspectionsReport() {
    const start = '2019-9-22';
    const end = moment()
      .utc()
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'OutfallInspections.Id as inspectionId',
        'OutfallInspections.OutfallId as siteId',
        'OutfallInspections.InspectionDate as inspectionDate',
        'OutfallInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'OutfallInspections.InspectorId as inspectorId',
        'OutfallInspections.InspectorId2 as inspectorId2',
        'Outfalls.Location as name',
        'Outfalls.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('OutfallInspections.IsDeleted', '=', 'false')
      .where('Outfalls.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Outfalls', {
        'OutfallInspections.OutfallId': 'Outfalls.Id',
      })
      .leftJoin('Users', { 'OutfallInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'OutfallInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Outfalls.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Outfalls.CustomerId': 'Customers.Id' });
  }

  async upcomingInspectionsReport() {
    const start = moment()
      .utc()
      .format('YYYY-MM-DD');
    const end = moment()
      .utc()
      .add(7, 'days')
      .format('YYYY-MM-DD');

    return await this.knex(this.tableName)
      .select([
        'OutfallInspections.Id as inspectionId',
        'OutfallInspections.OutfallId as siteId',
        'OutfallInspections.InspectionDate as inspectionDate',
        'OutfallInspections.ScheduledInspectionDate as scheduledInspectionDate',
        'OutfallInspections.InspectorId as inspectorId',
        'OutfallInspections.InspectorId2 as inspectorId2',
        'Outfalls.Location as name',
        'Outfalls.CustomerId as customerId',
        'Users.Email as email',
        'Users.GivenName as givenName',
        'Users.Surname as surname',
        'Users2.Email as email',
        'Users2.GivenName as givenName',
        'Users2.Surname as surname',
        'AdminInfo.Email as email',
        'AdminInfo.GivenName as givenName',
        'AdminInfo.Surname as surname',
        'UsersCustomers.UserId as userId',
        'UsersCustomers.Role as role',
        'Customers.DailyInspectorEmail as dailyInspectorEmail',
        'Customers.MondayInspectorEmail as mondayInspectorEmail',
        'Customers.MondayAdminEmail as mondayAdminEmail',
      ])
      // start >= whereBetween > end
      .whereBetween('ScheduledInspectionDate', [start, end])
      .whereNull('InspectionDate')
      .whereNot({ InspectorId: '1' })
      .where('OutfallInspections.IsDeleted', '=', 'false')
      .where('Outfalls.IsDeleted', '=', 'false')
      .whereIn('UsersCustomers.Role', ['super', 'admin'])
      .andWhereNot(function() {
        this.where('Customers.DailyInspectorEmail', '=', false);
        this.where('Customers.MondayInspectorEmail', '=', false);
        this.where('Customers.MondayAdminEmail', '=', false);
      })
      .leftJoin('Outfalls', {
        'OutfallInspections.OutfallId': 'Outfalls.Id',
      })
      .leftJoin('Users', { 'OutfallInspections.InspectorId': 'Users.Id' })
      .leftJoin('Users as Users2', {
        'OutfallInspections.InspectorId2': 'Users2.Id',
      })
      .leftJoin('UsersCustomers', {
        'Outfalls.CustomerId': 'UsersCustomers.CustomerId',
      })
      .leftJoin('Users as AdminInfo', {
        'UsersCustomers.Userid': 'AdminInfo.Id',
      })
      .leftJoin('Customers', { 'Outfalls.CustomerId': 'Customers.Id' });
  }
}
