import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { IllicitDischargeRepository } from './illicit-discharge.repository';

describe('Citizen Report Repository Tests', () => {
  const repo = container.get(IllicitDischargeRepository);

  describe(`exportData tests #IllicitDischargeRepository`, () => {
    it(`should export data`, async () => {
      const customerId = 'LEWISVILLE';
      const illicits = await repo.exportData(customerId);

      const illicit = illicits[0];

      chai.expect(illicits).to.be.instanceOf(Array);
      chai.expect(illicits).to.have.length.greaterThan(0);

      chai.expect(illicit).to.have.property('id');
      chai.expect(illicit).to.have.property('dateAdded');

      chai.expect(illicit).to.have.property('name');
      chai.expect(illicit).to.have.property('dateReported');
      chai.expect(illicit).to.have.property('complianceStatus');
      chai.expect(illicit).to.have.property('lat');
      chai.expect(illicit).to.have.property('lng');

      chai.expect(illicit).to.have.property('physicalAddress1');
      chai.expect(illicit).to.have.property('physicalAddress2');
      chai.expect(illicit).to.have.property('physicalCity');
      chai.expect(illicit).to.have.property('physicalState');
      chai.expect(illicit).to.have.property('physicalZip');

      chai.expect(illicit).to.have.property('investigatorFirstName');
      chai.expect(illicit).to.have.property('investigatorLastName');

      chai.expect(illicit).to.have.property('communityName');
      chai.expect(illicit).to.have.property('watershedName');
      chai.expect(illicit).to.have.property('subWatershedName');
      chai.expect(illicit).to.have.property('receivingWaterName');
    });
  });

  describe.skip(`exportCustomFields #IllicitDischargeRepository`, () => {
    // TODO test
    it(`should get custom field values`, async () => {
      const cfvs = await repo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('illicitDischargeId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });
});
