import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { ConstructionSiteRepository } from './construction-site.repository';
import * as Knex from 'knex';

describe('Construction Site Repository Tests', () => {
  const csRepo = container.get(ConstructionSiteRepository);
  const knex: Knex = container.get(Knex);
  const customerId = 'LEWISVILLE';

  describe(`exportData tests #ConstructionSiteRepository`, () => {
    it(`should export data`, async () => {
      const inspections = await csRepo.exportData(customerId);

      const inspection = inspections[0];

      chai.expect(inspections).to.be.instanceOf(Array);
      chai.expect(inspections).to.have.length.greaterThan(0);

      // TODO: test for properties exported.
      chai.expect(inspection).to.have.property('id');
      chai.expect(inspection).to.have.property('name');
    });
  });

  describe(`getCount tests #ConstructionSiteRepository`, async () => {
    it(`should get count`, async () => {
      const count = await csRepo.getCount(customerId);
      chai.expect(count).greaterThan(0);
    });
  });

  describe(`exportCustomFields`, () => {
    it(`should get custom field values`, async () => {
      const cfvs = await csRepo.exportCustomFields('LEWISVILLE');
      const cfv = cfvs[0];

      chai.expect(cfvs).to.be.instanceOf(Array);
      chai.expect(cfvs).to.have.length.greaterThan(0);

      chai.expect(cfv).to.have.property('constructionSiteId');
      chai.expect(cfv).to.have.property('fieldLabel');
      chai.expect(cfv).to.have.property('value');
    });
  });

  describe(`fetch by contractorEmail`, async () => {
    it(`should fetch by contractorEmail`, async () => {
      const expectedSql = `
      SELECT
  count (*) as count
FROM
  ConstructionSites cs INNER JOIN EntityContacts ec ON cs.GlobalId = ec.GlobalIdFk INNER JOIN Contacts c ON c.Id = ec.ContactId
WHERE
  cs.IsDeleted=0
  AND
  c.IsDeleted=0
  AND
  c.Email = 'alexng99+contractor@gmail.com'`;
      const expected: any[] = await knex.raw(expectedSql);
      const contractorEmail = 'alexng99+contractor@gmail.com';
      const sites = await csRepo.fetch(customerId, { contractorEmail });
      chai.expect(sites.length).greaterThan(0);
      chai.expect(sites.length).eq(expected[0].count);
    });
  });
});
