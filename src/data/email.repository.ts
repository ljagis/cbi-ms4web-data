import { getTableName, getSqlField } from './../decorators';
import { Repository } from './repository';
import { Entity, EmailEntity } from '../models';
import * as Knex from 'knex';
import * as Bluebird from 'bluebird';

export class EmailRepository extends Repository<EmailEntity> {
  constructor() {
    super(EmailEntity);
  }

  fetchSingle(id: number, customerId: string): Bluebird<EmailEntity> {
    let queryBuilder = this._getSingleQueryBuilder(id, customerId);
    return queryBuilder
      .map<any, EmailEntity>(f => this.mapObjectToEntityClass(f))
      .then(emailEntity => emailEntity.map(this._splitArrayValues))
      .get<EmailEntity>(0);
  }

  fetch(
    EntityWithEmail: { new (...args) },
    assetId: number,
    customerId: string
  ): Bluebird<EmailEntity[]> {
    let queryBuilder = this._getBaseFetchQueryBuilder(
      EntityWithEmail,
      assetId,
      customerId
    );
    return queryBuilder
      .map<any, EmailEntity>(f => this.mapObjectToEntityClass(f))
      .then(emailEntity => emailEntity.map(this._splitArrayValues));
  }

  insert(
    EntityWithEmail: { new (...args) },
    assetId: number,
    customerId: string,
    email: EmailEntity,
    trx?: Knex.Transaction
  ): Bluebird<EmailEntity> {
    let assetTableName = getTableName(EntityWithEmail);
    let assetIdSqlField = getSqlField(EntityWithEmail, 'id');
    let assetGlobalIdSqlField = getSqlField(EntityWithEmail, 'globalId');
    email.customerId = customerId; // ensure customerId is set

    let data = this.mapObjectToSql(email);
    let globalIdFkSqlFieldFull = this.getSqlField(f => f.globalIdFk, true);
    data[globalIdFkSqlFieldFull] = this.knex(assetTableName)
      .select(assetGlobalIdSqlField)
      .where(assetIdSqlField, assetId);
    let qb = this.knex(this.tableName)
      .insert(data)
      .returning(this.getSelectFields(undefined, false));
    if (trx) {
      qb.transacting(trx);
    }
    return qb
      .map(e => this.mapObjectToEntityClass(e))
      .then(emailEntity => emailEntity.map(this._splitArrayValues))
      .get<EmailEntity>(0);
  }

  /**
   * Gets the base QueryBuilder for retrieving an asset's email
   *
   * @param {{new (...args)}} Entity ConstructionSite, etc...
   * @param {number} entityId The entity id
   * @param {string} customerId
   * @returns {knex.QueryBuilder}
   *
   */
  _getBaseFetchQueryBuilder(
    EntityWithEmail: { new (...args) },
    entityId: number,
    customerId: string
  ): Knex.QueryBuilder {
    let assetTableName = getTableName(EntityWithEmail);
    let assetIdSqlField = getSqlField<Entity>(EntityWithEmail, e => e.id);
    let assetGlobalIdSqlField = getSqlField(EntityWithEmail, 'globalId');
    let globalIdFkSqlFieldFull = this.getSqlField(f => f.globalIdFk, true);

    // gets Entity Type from constructor name - for sql performance
    let entityType = (<any>EntityWithEmail).name;
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .orderBy(this.getSqlField(f => f.addedDate), 'desc')
      .where(this.getSqlField(f => f.customerId), customerId)
      .where(this.getSqlField(f => f.entityType), entityType)
      .whereIn(
        globalIdFkSqlFieldFull,
        this.knex(assetTableName)
          .select(assetGlobalIdSqlField)
          .where(assetIdSqlField, entityId)
      );
    return queryBuilder;
  }

  _getSingleQueryBuilder(id: number, customerId: string): Knex.QueryBuilder {
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .where(this.getSqlField(f => f.customerId), customerId)
      .where(this.getSqlField(f => f.id), id);
    return queryBuilder;
  }

  /**
   * This is needed for SQL Union to work on missing column names
   */
  _createNullColumn(columnName: string) {
    return this.knex.raw(`null as ${columnName}`);
  }

  _splitArrayValues(emailEntity): EmailEntity {
    const delimiter = '|';
    return Object.assign({}, emailEntity, {
      emailTo: emailEntity['emailTo'].split(delimiter),
      emailCC: emailEntity['emailCC'].split(delimiter),
    });
  }
}
