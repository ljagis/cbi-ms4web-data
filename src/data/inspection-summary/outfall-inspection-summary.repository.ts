import { injectable, inject } from 'inversify';
import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import {
  OutfallInspection as OFI,
  CustomFieldValue,
  CustomField,
  InspectionSummaryRecord,
  InspectionType,
  InspectionSummaryModel,
  Outfall,
} from '../../models';
import { EntityTypes } from '../../shared';

interface ReportOptions {
  from: string;
  to: string;
}

@injectable()
export class OutfallInspectionSummaryRepository {
  constructor(@inject(Knex) private knex: Knex) {}

  async getInspectionSummaryRecords(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryRecord[]> {
    const qb = this.knex(CustomFieldValue.tableName)
      .join(
        CustomField.tableName,
        CustomField.sqlField(f => f.id, true),
        CustomFieldValue.sqlField(v => v.customFieldId, true)
      )
      .join(
        OFI.tableName,
        OFI.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        Outfall.tableName,
        Outfall.sqlField(c => c.id, true),
        OFI.sqlField(i => i.outfallId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        OFI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.entityType,
          true
        )]: EntityTypes.OutfallInspection,
        [CustomField.sqlField(f => f.isDeleted, true)]: false,

        // Asset filters
        [Outfall.sqlField(c => c.customerId, true)]: customerId,
        [Outfall.sqlField(c => c.isDeleted, true)]: false,
        [OFI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(OFI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // from CustomFieldValue
        CustomFieldValue.sqlField(f => f.customFieldId, true, true),
        CustomFieldValue.sqlField(f => f.value, true, true),

        // From CustomField
        CustomField.sqlField(f => f.dataType, true, true),
        CustomField.sqlField(f => f.inputType, true, true),
        CustomField.sqlField(f => f.fieldOptions, true, true),
        CustomField.sqlField(f => f.fieldOrder, true, true),
        CustomField.sqlField(f => f.sectionName, true, true),
        CustomField.sqlField(f => f.fieldLabel, true, true),

        // From Inspection
        `${OFI.sqlField(f => f.id, true)} as inspectionId`,
        OFI.sqlField(f => f.inspectionTypeId, true, true),

        // From Asset
        `${Outfall.sqlField(f => f.id, true)} as assetId`,
        Outfall.sqlField(f => f.complianceStatus, true, true),

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      )
      .orderByRaw(
        [
          CustomField.sqlField(f => f.sectionName, true),
          CustomField.sqlField(f => f.fieldOrder, true),
        ].join(', ')
      );
    return await qb;
  }

  async getInspections(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryModel[]> {
    const qb = this.knex(OFI.tableName)
      .join(
        Outfall.tableName,
        Outfall.sqlField(c => c.id, true),
        OFI.sqlField(i => i.outfallId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        OFI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        // Asset filters
        [Outfall.sqlField(c => c.customerId, true)]: customerId,
        [Outfall.sqlField(c => c.isDeleted, true)]: false,
        [OFI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(OFI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // From Inspection
        OFI.sqlField(f => f.id, true, true),
        OFI.sqlField(f => f.inspectionTypeId, true, true),
        OFI.sqlField(f => f.complianceStatus, true, true),
        OFI.sqlField(f => f.timeIn, true, true),
        OFI.sqlField(f => f.timeOut, true, true),

        // From Asset
        `${Outfall.sqlField(f => f.id, true)} as assetId`,

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      );
    return await qb;
  }
}
