import { injectable, inject } from 'inversify';
import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import {
  FacilityInspection as FI,
  CustomFieldValue,
  CustomField,
  InspectionSummaryRecord,
  InspectionType,
  InspectionSummaryModel,
  Facility,
} from '../../models';
import { EntityTypes } from '../../shared';

interface ReportOptions {
  from: string;
  to: string;
}

@injectable()
export class FacilityInspectionSummaryRepository {
  constructor(@inject(Knex) private knex: Knex) {}

  async getInspectionSummaryRecords(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryRecord[]> {
    const qb = this.knex(CustomFieldValue.tableName)
      .join(
        CustomField.tableName,
        CustomField.sqlField(f => f.id, true),
        CustomFieldValue.sqlField(v => v.customFieldId, true)
      )
      .join(
        FI.tableName,
        FI.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        Facility.tableName,
        Facility.sqlField(c => c.id, true),
        FI.sqlField(i => i.facilityId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        FI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.entityType,
          true
        )]: EntityTypes.FacilityInspection,
        [CustomField.sqlField(f => f.isDeleted, true)]: false,

        // Asset filters
        [Facility.sqlField(c => c.customerId, true)]: customerId,
        [Facility.sqlField(c => c.isDeleted, true)]: false,
        [FI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(FI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // from CustomFieldValue
        CustomFieldValue.sqlField(f => f.customFieldId, true, true),
        CustomFieldValue.sqlField(f => f.value, true, true),

        // From CustomField
        CustomField.sqlField(f => f.dataType, true, true),
        CustomField.sqlField(f => f.inputType, true, true),
        CustomField.sqlField(f => f.fieldOptions, true, true),
        CustomField.sqlField(f => f.fieldOrder, true, true),
        CustomField.sqlField(f => f.sectionName, true, true),
        CustomField.sqlField(f => f.fieldLabel, true, true),

        // From Inspection
        `${FI.sqlField(f => f.id, true)} as inspectionId`,
        FI.sqlField(f => f.inspectionTypeId, true, true),

        // From Asset
        `${Facility.sqlField(f => f.id, true)} as assetId`,
        Facility.sqlField(f => f.complianceStatus, true, true),

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      )
      .orderByRaw(
        [
          CustomField.sqlField(f => f.sectionName, true),
          CustomField.sqlField(f => f.fieldOrder, true),
        ].join(', ')
      );
    return await qb;
  }

  async getInspections(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryModel[]> {
    const qb = this.knex(FI.tableName)
      .join(
        Facility.tableName,
        Facility.sqlField(c => c.id, true),
        FI.sqlField(i => i.facilityId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        FI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        // Asset filters
        [Facility.sqlField(c => c.customerId, true)]: customerId,
        [Facility.sqlField(c => c.isDeleted, true)]: false,
        [FI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(FI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // From Inspection
        FI.sqlField(f => f.id, true, true),
        FI.sqlField(f => f.inspectionTypeId, true, true),
        FI.sqlField(f => f.complianceStatus, true, true),
        FI.sqlField(f => f.timeIn, true, true),
        FI.sqlField(f => f.timeOut, true, true),

        // From Asset
        `${Facility.sqlField(f => f.id, true)} as assetId`,

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      );
    return await qb;
  }
}
