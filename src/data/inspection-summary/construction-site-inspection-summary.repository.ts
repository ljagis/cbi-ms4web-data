import { injectable, inject } from 'inversify';
import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import {
  ConstructionSiteInspection as CSI,
  ConstructionSite,
  CustomFieldValue,
  CustomField,
  InspectionSummaryRecord,
  InspectionType,
  InspectionSummaryModel,
} from '../../models';
import { EntityTypes } from '../../shared';

interface ReportOptions {
  from: string;
  to: string;
}

@injectable()
export class ConstructionSiteInspectionSummaryRepository {
  constructor(@inject(Knex) private knex: Knex) {}

  /**
   * Retrieves custom field values for inspections wihtin date range
   * @param customerId
   * @param options
   */
  async getInspectionSummaryRecords(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryRecord[]> {
    const qb = this.knex(CustomFieldValue.tableName)
      .join(
        CustomField.tableName,
        CustomField.sqlField(f => f.id, true),
        CustomFieldValue.sqlField(v => v.customFieldId, true)
      )
      .join(
        CSI.tableName,
        CSI.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        ConstructionSite.tableName,
        ConstructionSite.sqlField(c => c.id, true),
        CSI.sqlField(i => i.constructionSiteId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        CSI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.entityType,
          true
        )]: EntityTypes.ConstructionSiteInspection,
        [CustomField.sqlField(f => f.isDeleted, true)]: false,

        // Asset filters
        [ConstructionSite.sqlField(c => c.customerId, true)]: customerId,
        [ConstructionSite.sqlField(c => c.isDeleted, true)]: false,
        [CSI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(CSI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // from CustomFieldValue
        CustomFieldValue.sqlField(f => f.customFieldId, true, true),
        CustomFieldValue.sqlField(f => f.value, true, true),

        // From CustomField
        CustomField.sqlField(f => f.dataType, true, true),
        CustomField.sqlField(f => f.inputType, true, true),
        CustomField.sqlField(f => f.fieldOptions, true, true),
        CustomField.sqlField(f => f.fieldOrder, true, true),
        CustomField.sqlField(f => f.sectionName, true, true),
        CustomField.sqlField(f => f.fieldLabel, true, true),

        // From Inspection
        `${CSI.sqlField(f => f.id, true)} as inspectionId`,
        CSI.sqlField(f => f.inspectionTypeId, true, true),

        // From Asset
        `${ConstructionSite.sqlField(f => f.id, true)} as assetId`,
        ConstructionSite.sqlField(f => f.complianceStatus, true, true),

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      )
      .orderByRaw(
        [
          CustomField.sqlField(f => f.sectionName, true),
          CustomField.sqlField(f => f.fieldOrder, true),
        ].join(', ')
      );
    return await qb;
  }

  async getInspections(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryModel[]> {
    const qb = this.knex(CSI.tableName)
      .join(
        ConstructionSite.tableName,
        ConstructionSite.sqlField(c => c.id, true),
        CSI.sqlField(i => i.constructionSiteId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        CSI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        // Asset filters
        [ConstructionSite.sqlField(c => c.customerId, true)]: customerId,
        [ConstructionSite.sqlField(c => c.isDeleted, true)]: false,
        [CSI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(CSI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // From Inspection
        CSI.sqlField(f => f.id, true, true),
        CSI.sqlField(f => f.inspectionTypeId, true, true),
        CSI.sqlField(f => f.complianceStatus, true, true),
        CSI.sqlField(f => f.timeIn, true, true),
        CSI.sqlField(f => f.timeOut, true, true),

        // From Asset
        `${ConstructionSite.sqlField(f => f.id, true)} as assetId`,

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      );
    return await qb;
  }
}
