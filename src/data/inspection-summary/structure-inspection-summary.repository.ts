import { injectable, inject } from 'inversify';
import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import {
  StructureInspection as SCI,
  CustomFieldValue,
  CustomField,
  InspectionSummaryRecord,
  InspectionType,
  InspectionSummaryModel,
  Structure,
} from '../../models';
import { EntityTypes } from '../../shared';

interface ReportOptions {
  from: string;
  to: string;
}

@injectable()
export class StructureInspectionSummaryRepository {
  constructor(@inject(Knex) private knex: Knex) {}

  async getInspectionSummaryRecords(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryRecord[]> {
    const qb = this.knex(CustomFieldValue.tableName)
      .join(
        CustomField.tableName,
        CustomField.sqlField(f => f.id, true),
        CustomFieldValue.sqlField(v => v.customFieldId, true)
      )
      .join(
        SCI.tableName,
        SCI.sqlField(f => f.globalId, true),
        CustomFieldValue.sqlField(f => f.globalIdFk, true)
      )
      .join(
        Structure.tableName,
        Structure.sqlField(c => c.id, true),
        SCI.sqlField(i => i.structureId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        SCI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        [CustomField.sqlField(f => f.customerId, true)]: customerId,
        [CustomField.sqlField(
          f => f.entityType,
          true
        )]: EntityTypes.StructureInspection,
        [CustomField.sqlField(f => f.isDeleted, true)]: false,

        // Asset filters
        [Structure.sqlField(c => c.customerId, true)]: customerId,
        [Structure.sqlField(c => c.isDeleted, true)]: false,
        [SCI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(SCI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // from CustomFieldValue
        CustomFieldValue.sqlField(f => f.customFieldId, true, true),
        CustomFieldValue.sqlField(f => f.value, true, true),

        // From CustomField
        CustomField.sqlField(f => f.dataType, true, true),
        CustomField.sqlField(f => f.inputType, true, true),
        CustomField.sqlField(f => f.fieldOptions, true, true),
        CustomField.sqlField(f => f.fieldOrder, true, true),
        CustomField.sqlField(f => f.sectionName, true, true),
        CustomField.sqlField(f => f.fieldLabel, true, true),

        // From Inspection
        `${SCI.sqlField(f => f.id, true)} as inspectionId`,
        SCI.sqlField(f => f.inspectionTypeId, true, true),

        // From Asset
        `${Structure.sqlField(f => f.id, true)} as assetId`,
        Structure.sqlField(f => f.complianceStatus, true, true),

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      )
      .orderByRaw(
        [
          CustomField.sqlField(f => f.sectionName, true),
          CustomField.sqlField(f => f.fieldOrder, true),
        ].join(', ')
      );
    return await qb;
  }

  async getInspections(
    customerId: string,
    options: ReportOptions
  ): Bluebird<InspectionSummaryModel[]> {
    const qb = this.knex(SCI.tableName)
      .join(
        Structure.tableName,
        Structure.sqlField(c => c.id, true),
        SCI.sqlField(i => i.structureId, true)
      )
      .join(
        InspectionType.tableName,
        InspectionType.sqlField(c => c.id, true),
        SCI.sqlField(i => i.inspectionTypeId, true)
      )
      .where({
        // Asset filters
        [Structure.sqlField(c => c.customerId, true)]: customerId,
        [Structure.sqlField(c => c.isDeleted, true)]: false,
        [SCI.sqlField(c => c.isDeleted, true)]: false,
      })
      .whereBetween(SCI.sqlField(i => i.inspectionDate, true), [
        options.from,
        options.to,
      ])
      .select(
        // From Inspection
        SCI.sqlField(f => f.id, true, true),
        SCI.sqlField(f => f.inspectionTypeId, true, true),
        SCI.sqlField(f => f.complianceStatus, true, true),
        SCI.sqlField(f => f.timeIn, true, true),
        SCI.sqlField(f => f.timeOut, true, true),

        // From Asset
        `${Structure.sqlField(f => f.id, true)} as assetId`,

        // from InspectionType
        `${InspectionType.sqlField(f => f.name, true)} as inspectionTypeName`
      );
    return await qb;
  }
}
