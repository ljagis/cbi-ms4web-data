import { Repository } from './repository';
import { MG, SwmpActivity } from '../models';
import { injectable } from 'inversify';
import * as Bluebird from 'bluebird';

@injectable()
export class SwmpActivityRepository extends Repository<SwmpActivity> {
  constructor() {
    super(SwmpActivity);
  }

  async create(swmpActivity: Partial<SwmpActivity>) {
    const returnFields = this.getSelectFields();
    const createdSwmpActivity: SwmpActivity = await this.knex(this.tableName)
      .insert(this.mapObjectToSql(swmpActivity))
      .returning(returnFields)
      .get<SwmpActivity>(0);

    return createdSwmpActivity;
  }

  async update(swmpActivity: Partial<SwmpActivity>, customerId: string) {
    const qb = this.knex(this.tableName)
      .update(this.mapObjectToSql(swmpActivity))
      .where(SwmpActivity.sqlField(s => s.customerId), customerId)
      .where(SwmpActivity.sqlField(s => s.id), swmpActivity.id);

    return qb;
  }

  async fetchBySwmpIdAndCustomer(
    swmpId: number,
    customerId: string,
    joinMG?: boolean
  ): Bluebird<SwmpActivity[]> {
    let baseQuery = this.knex(this.tableName)
      .where(SwmpActivity.sqlField(s => s.customerId), customerId)
      .where(SwmpActivity.sqlField(s => s.swmpId), swmpId)
      .orderBy(SwmpActivity.sqlField(s => s.date), 'desc')
      .orderBy(SwmpActivity.sqlField(s => s.dateAdded), 'desc');

    if (joinMG) {
      return baseQuery
        .join(
          MG.getTableName(),
          MG.sqlField(m => m.id, true),
          SwmpActivity.sqlField(s => s.mgId, true)
        )
        .select([
          SwmpActivity.sqlField(s => s.id, true, true),
          SwmpActivity.sqlField(s => s.customerId, true, true),
          SwmpActivity.sqlField(s => s.swmpId, true, true),
          SwmpActivity.sqlField(s => s.mgId, true, true),
          SwmpActivity.sqlField(s => s.date, true, true),
          SwmpActivity.sqlField(s => s.quantity, true, true),
          SwmpActivity.sqlField(s => s.description, true, true),
          SwmpActivity.sqlField(s => s.dateAdded, true, true),
          `${MG.sqlField(m => m.units, true)} as units`,
          `${MG.sqlField(m => m.informationUsed, true)} as informationUsed`,
        ]);
    }

    return baseQuery.select(this.defaultSelect);
  }

  async delete(id: number) {
    const qb = this.knex(this.tableName)
      .where(SwmpActivity.sqlField(s => s.id), id)
      .delete();

    return qb;
  }
}
