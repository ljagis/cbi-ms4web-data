import { expect } from 'chai';
import { container } from '../inversify.config';
import { UserCustomerRepository } from './user-customer.repository';

describe('UserCustomerRepository Tests', async () => {
  const repo = container.get(UserCustomerRepository);
  const lewisville = 'LEWISVILLE';

  it(`should get fetchByUserId`, async () => {
    const anguyenUserId = 23; // hard coded

    const userCustomers = await repo.fetchByUserId(anguyenUserId);
    expect(userCustomers).ok;
    expect(userCustomers).length.greaterThan(0);
    expect(userCustomers.every(u => u.userId === anguyenUserId));
  });

  it(`should get fetchByCustomer`, async () => {
    const usersCustomers = await repo.fetchByCustomer(lewisville);
    expect(usersCustomers).ok;
    expect(usersCustomers.length).greaterThan(0);
    expect(usersCustomers.every(u => u.customerId === lewisville));
  });
});
