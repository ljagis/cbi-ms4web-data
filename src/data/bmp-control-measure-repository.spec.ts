import 'mocha';
import * as chai from 'chai';
import { container } from '../inversify.config';
import { BmpControlMeasureRepository } from './bmp-control-measure.repository';

describe('BMP Activity Repository Tests', () => {
  const bmpCMRepo = container.get(BmpControlMeasureRepository);

  describe(`exportData tests`, () => {
    it(`should export data`, async () => {
      const customerId = 'LEWISVILLE';
      const cms = await bmpCMRepo.exportData(customerId);

      const cm = cms[0];

      chai.expect(cms).to.be.instanceOf(Array);
      chai.expect(cms).to.have.length.greaterThan(0);

      chai.expect(cm).to.have.property('id');
      chai.expect(cm).to.have.property('name');
    });
  });
});
