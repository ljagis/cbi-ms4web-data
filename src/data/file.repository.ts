import { getTableName, getSqlField } from './../decorators';
import { Repository } from './repository';
import { Entity, FileEntity } from '../models';
import * as m from '../models';
import * as Knex from 'knex';
import { Defaults, propertyNameFor } from '../shared';
import * as Bluebird from 'bluebird';

export class FileRepository extends Repository<FileEntity> {
  constructor() {
    super(FileEntity);
  }

  fetchSingle(id: number, customerId: string): Bluebird<FileEntity> {
    let queryBuilder = this._getSingleQueryBuilder(id, customerId);
    return queryBuilder
      .map<any, FileEntity>(f => this.mapObjectToEntityClass(f))
      .get<FileEntity>(0);
  }

  /**
   * Fetches all files from all entities
   *
   * @param {string} customerId
   */
  fetchAll(
    customerId: string,
    queryOptions?: m.QueryOptions
  ): Bluebird<m.FileEntityJoined[]> {
    /**
     */
    queryOptions = queryOptions || {};
    let limit = queryOptions.limit || Defaults.QUERY_MAX_RECORD_COUNT;
    let orderBy =
      queryOptions.orderByField || this.getSqlField(f => f.addedDate);
    let orderByDir = queryOptions.orderByDirection || 'asc';
    let entityNameProp = propertyNameFor<m.FileEntityJoined>(a => a.entityName);
    let entityIdProp = propertyNameFor<m.FileEntityJoined>(a => a.entityId);
    let inspectionIdColumn = propertyNameFor<m.FileEntityJoined>(
      a => a.inspectionId
    );

    // Bmp Details
    let bmpQb = this.knex(getTableName(m.BmpDetail))
      .join(
        m.BmpControlMeasure.getTableName(),
        getSqlField<m.BmpDetail>(m.BmpDetail, f => f.controlMeasureId, true),
        getSqlField<m.BmpControlMeasure>(m.BmpControlMeasure, f => f.id, true)
      )
      .where(
        getSqlField<m.BmpControlMeasure>(
          m.BmpControlMeasure,
          f => f.customerId,
          true
        ),
        customerId
      )
      .where(getSqlField<m.BmpDetail>(m.BmpDetail, f => f.isDeleted, true), 0)
      .select(
        getSqlField<m.BmpDetail>(m.BmpDetail, f => f.globalId, true),
        getSqlField<m.BmpDetail>(m.BmpDetail, f => f.id, true),
        `${getSqlField<m.BmpDetail>(
          m.BmpDetail,
          f => f.name,
          true
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );

    // Construction Site
    let constructionSiteQb = this.knex(getTableName(m.ConstructionSite))
      .where(
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.customerId),
        customerId
      )
      .where(
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.isDeleted),
        0
      )
      .select(
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.globalId),
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.id),
        `${getSqlField<m.ConstructionSite>(
          m.ConstructionSite,
          f => f.name
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Facility
    let facilityQb = this.knex(getTableName(m.Facility))
      .where(getSqlField<m.Facility>(m.Facility, f => f.customerId), customerId)
      .where(getSqlField<m.Facility>(m.Facility, f => f.isDeleted), 0)
      .select(
        getSqlField<m.Facility>(m.Facility, f => f.globalId),
        getSqlField<m.Facility>(m.Facility, f => f.id),
        `${getSqlField<m.Facility>(
          m.Facility,
          f => f.name
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    let monitoringLocQb = this.knex(getTableName(m.MonitoringLocation))
      .where(
        getSqlField<m.MonitoringLocation>(
          m.MonitoringLocation,
          f => f.customerId
        ),
        customerId
      )
      .where(
        getSqlField<m.MonitoringLocation>(
          m.MonitoringLocation,
          f => f.isDeleted
        ),
        0
      )
      .select(
        getSqlField<m.MonitoringLocation>(
          m.MonitoringLocation,
          f => f.globalId
        ),
        getSqlField<m.MonitoringLocation>(m.MonitoringLocation, f => f.id),
        `${getSqlField<m.MonitoringLocation>(
          m.MonitoringLocation,
          f => f.name
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Outfall
    let outfallQb = this.knex(getTableName(m.Outfall))
      .where(getSqlField<m.Outfall>(m.Outfall, f => f.customerId), customerId)
      .where(getSqlField<m.Outfall>(m.Outfall, f => f.isDeleted), 0)
      .select(
        getSqlField<m.Outfall>(m.Outfall, f => f.globalId),
        getSqlField<m.Outfall>(m.Outfall, f => f.id),
        `${getSqlField<m.Outfall>(
          m.Outfall,
          f => f.location
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Structure
    let structureQb = this.knex(getTableName(m.Structure))
      .where(
        getSqlField<m.Structure>(m.Structure, f => f.customerId),
        customerId
      )
      .where(getSqlField<m.Structure>(m.Structure, f => f.isDeleted), 0)
      .select(
        getSqlField<m.Structure>(m.Structure, f => f.globalId),
        getSqlField<m.Structure>(m.Structure, f => f.id),
        `${getSqlField<m.Structure>(
          m.Structure,
          f => f.name
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Citizen Report
    let citizenReportQb = this.knex(getTableName(m.CitizenReport))
      .where(
        getSqlField<m.CitizenReport>(m.CitizenReport, f => f.customerId),
        customerId
      )
      .where(getSqlField<m.CitizenReport>(m.CitizenReport, f => f.isDeleted), 0)
      .select(
        getSqlField<m.CitizenReport>(m.CitizenReport, f => f.globalId),
        getSqlField<m.CitizenReport>(m.CitizenReport, f => f.id),
        `${getSqlField<m.CitizenReport>(
          m.CitizenReport,
          f => f.citizenName
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Illicit Discharge
    let illicitDischargeQb = this.knex(getTableName(m.IllicitDischarge))
      .where(
        getSqlField<m.IllicitDischarge>(m.IllicitDischarge, f => f.customerId),
        customerId
      )
      .where(
        getSqlField<m.IllicitDischarge>(m.IllicitDischarge, f => f.isDeleted),
        0
      )
      .select(
        getSqlField<m.IllicitDischarge>(m.IllicitDischarge, f => f.globalId),
        getSqlField<m.IllicitDischarge>(m.IllicitDischarge, f => f.id),
        `${getSqlField<m.IllicitDischarge>(
          m.IllicitDischarge,
          f => f.name
        )} as ${entityNameProp}`,
        this._createNullColumn(inspectionIdColumn)
      );
    // Construction Site Inspections
    let csInspQb = this.knex(getTableName(m.ConstructionSiteInspection))
      .join(
        m.ConstructionSite.getTableName(),
        getSqlField<m.ConstructionSiteInspection>(
          m.ConstructionSiteInspection,
          f => f.constructionSiteId,
          true
        ),
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.id, true)
      )
      .where(
        getSqlField<m.ConstructionSite>(
          m.ConstructionSite,
          f => f.customerId,
          true
        ),
        customerId
      )
      .where(
        getSqlField<m.ConstructionSiteInspection>(
          m.ConstructionSiteInspection,
          f => f.isDeleted,
          true
        ),
        0
      )
      .select(
        getSqlField<m.ConstructionSiteInspection>(
          m.ConstructionSiteInspection,
          f => f.globalId,
          true
        ),
        getSqlField<m.ConstructionSite>(m.ConstructionSite, f => f.id, true),
        `${getSqlField<m.ConstructionSite>(
          m.ConstructionSite,
          f => f.name,
          true
        )} as ${entityNameProp}`,
        `${getSqlField<m.ConstructionSiteInspection>(
          m.ConstructionSiteInspection,
          f => f.id,
          true
        )} as ${inspectionIdColumn}`
      );
    // Facility Inspections
    let facInspQb = this.knex(getTableName(m.FacilityInspection))
      .join(
        m.Facility.getTableName(),
        getSqlField<m.FacilityInspection>(
          m.FacilityInspection,
          f => f.facilityId,
          true
        ),
        getSqlField<m.Facility>(m.Facility, f => f.id, true)
      )
      .where(
        getSqlField<m.Facility>(m.Facility, f => f.customerId, true),
        customerId
      )
      .where(
        getSqlField<m.FacilityInspection>(
          m.FacilityInspection,
          f => f.isDeleted,
          true
        ),
        0
      )
      .select(
        getSqlField<m.FacilityInspection>(
          m.FacilityInspection,
          f => f.globalId,
          true
        ),
        getSqlField<m.Facility>(m.Facility, f => f.id, true),
        `${getSqlField<m.Facility>(
          m.Facility,
          f => f.name,
          true
        )} as ${entityNameProp}`,
        `${getSqlField<m.FacilityInspection>(
          m.FacilityInspection,
          f => f.id,
          true
        )} as ${inspectionIdColumn}`
      );

    // Structure Inspections
    let structureInspQb = this.knex(getTableName(m.StructureInspection))
      .join(
        m.Structure.getTableName(),
        getSqlField<m.StructureInspection>(
          m.StructureInspection,
          f => f.structureId,
          true
        ),
        getSqlField<m.Structure>(m.Structure, f => f.id, true)
      )
      .where(
        getSqlField<m.Structure>(m.Structure, f => f.customerId, true),
        customerId
      )
      .where(
        getSqlField<m.StructureInspection>(
          m.StructureInspection,
          f => f.isDeleted,
          true
        ),
        0
      )
      .select(
        getSqlField<m.StructureInspection>(
          m.StructureInspection,
          f => f.globalId,
          true
        ),
        getSqlField<m.Structure>(m.Structure, f => f.id, true),
        `${getSqlField<m.Structure>(
          m.Structure,
          f => f.name,
          true
        )} as ${entityNameProp}`,
        `${getSqlField<m.StructureInspection>(
          m.StructureInspection,
          f => f.id,
          true
        )} as ${inspectionIdColumn}`
      );

    // Outfall Inspections
    let outfallInspQb = this.knex(getTableName(m.OutfallInspection))
      .join(
        m.Outfall.getTableName(),
        getSqlField<m.OutfallInspection>(
          m.OutfallInspection,
          f => f.outfallId,
          true
        ),
        getSqlField<m.Outfall>(m.Outfall, f => f.id, true)
      )
      .where(
        getSqlField<m.Outfall>(m.Outfall, f => f.customerId, true),
        customerId
      )
      .where(
        getSqlField<m.OutfallInspection>(
          m.OutfallInspection,
          f => f.isDeleted,
          true
        ),
        0
      )
      .select(
        getSqlField<m.OutfallInspection>(
          m.OutfallInspection,
          f => f.globalId,
          true
        ),
        getSqlField<m.Outfall>(m.Outfall, f => f.id, true),
        `${getSqlField<m.Outfall>(
          m.Outfall,
          f => f.location,
          true
        )} as ${entityNameProp}`,
        `${getSqlField<m.OutfallInspection>(
          m.OutfallInspection,
          f => f.id,
          true
        )} as ${inspectionIdColumn}`
      );
    let commonEntities = this.knex
      .raw(
        [
          bmpQb.toQuery(),
          constructionSiteQb.toQuery(),
          facilityQb.toQuery(),
          monitoringLocQb.toQuery(),
          outfallQb.toQuery(),
          structureQb.toQuery(),
          citizenReportQb.toQuery(),
          illicitDischargeQb.toQuery(),
          csInspQb.toQuery(),
          facInspQb.toQuery(),
          structureInspQb.toQuery(),
          outfallInspQb.toQuery(),
        ].join(' UNION ')
      )
      .wrap(
        '(',
        `) as Entities on Entities.GlobalId = ${this.getSqlField(
          f => f.globalIdFk
        )}`
      );

    let fullQb = this.knex(this.tableName)
      .leftJoin(commonEntities)
      .select([
        ...this.getSelectFields(undefined, true),
        `Entities.Id as ${entityIdProp}`,
        `Entities.entityName as ${entityNameProp}`,
        `Entities.inspectionId as ${inspectionIdColumn}`,
      ])
      .where(this.getSqlField(f => f.customerId, true), customerId)
      .where(this.getSqlField(f => f.isDeleted, true), 0)
      .whereNot('Entities.GlobalId', null)
      .limit(limit)
      .orderBy(orderBy, orderByDir);
    if (queryOptions.offset) {
      fullQb.offset(queryOptions.offset);
    }

    return fullQb;
  }

  fetch(
    EntityWithFile: { new (...args) },
    assetId: number,
    customerId: string
  ): Bluebird<FileEntity[]> {
    let queryBuilder = this._getBaseFetchQueryBuilder(
      EntityWithFile,
      assetId,
      customerId
    );
    return queryBuilder.map<any, FileEntity>(f =>
      this.mapObjectToEntityClass(f)
    );
  }

  insert(
    EntityWithFile: { new (...args) },
    assetId: number,
    customerId: string,
    file: FileEntity,
    trx?: Knex.Transaction
  ): Bluebird<FileEntity> {
    let assetTableName = getTableName(EntityWithFile);
    let assetIdSqlField = getSqlField(EntityWithFile, 'id');
    let assetGlobalIdSqlField = getSqlField(EntityWithFile, 'globalId');
    file.customerId = customerId; // ensure customerId is set

    let data = this.mapObjectToSql(file);
    let globalIdFkSqlFieldFull = this.getSqlField(f => f.globalIdFk, true);
    data[globalIdFkSqlFieldFull] = this.knex(assetTableName)
      .select(assetGlobalIdSqlField)
      .where(assetIdSqlField, assetId);
    let qb = this.knex(this.tableName)
      .insert(data)
      .returning(this.getSelectFields(undefined, false));
    if (trx) {
      qb.transacting(trx);
    }
    return qb.map(e => this.mapObjectToEntityClass(e)).get<FileEntity>(0);
  }

  deleteFile(
    EntityWithFile: { new (...args) },
    fileId: number,
    customerId: string
  ): Bluebird<number> {
    let deleteBuilder = this.knex(this.tableName)
      .update(this.getSqlField(f => f.isDeleted), 1)
      .where(this.idFieldSql, fileId)
      .where(this.getSqlField(f => f.customerId), customerId);
    return deleteBuilder.then<number>(rs => rs);
  }

  /**
   * Gets the base QueryBuilder for retrieving an asset's file
   *
   * @param {{new (...args)}} Entity ConstructionSite, etc...
   * @param {number} entityId The entity id
   * @param {string} customerId
   * @returns {knex.QueryBuilder}
   *
   */
  _getBaseFetchQueryBuilder(
    EntityWithFile: { new (...args) },
    entityId: number,
    customerId: string
  ): Knex.QueryBuilder {
    let assetTableName = getTableName(EntityWithFile);
    let assetIdSqlField = getSqlField<Entity>(EntityWithFile, e => e.id);
    let assetGlobalIdSqlField = getSqlField(EntityWithFile, 'globalId');
    let globalIdFkSqlFieldFull = this.getSqlField(f => f.globalIdFk, true);

    // gets Entity Type from constructor name - for sql performance
    let entityType = (<any>EntityWithFile).name;
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .orderBy(this.getSqlField(f => f.addedDate), 'asc')
      .where(this.getSqlField(f => f.customerId), customerId)
      .where(this.getSqlField(f => f.isDeleted), 0)
      .where(this.getSqlField(f => f.entityType), entityType)
      .whereIn(
        globalIdFkSqlFieldFull,
        this.knex(assetTableName)
          .select(assetGlobalIdSqlField)
          .where(assetIdSqlField, entityId)
      );
    return queryBuilder;
  }

  _getSingleQueryBuilder(id: number, customerId: string): Knex.QueryBuilder {
    let queryBuilder = this.knex(this.tableName)
      .select(this.defaultSelect)
      .where(this.getSqlField(f => f.customerId), customerId)
      .where(this.getSqlField(f => f.isDeleted), 0)
      .where(this.getSqlField(f => f.id), id);
    return queryBuilder;
  }

  /**
   * This is needed for SQL Union to work on missing column names
   */
  _createNullColumn(columnName: string) {
    return this.knex.raw(`null as ${columnName}`);
  }
}
