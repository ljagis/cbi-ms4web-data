import * as Knex from 'knex';
import { getSqlFields2, getTableName } from '../../decorators';
import { Entity } from '../../models';

/**
 * Returns default Query Builder with default select fields
 * field format will be 'FieldName as fieldName'
 *
 * @export
 * @param {{ new (...args): Entity}} entityClass - the model class
 * @param {Knex} knex
 * @returns Knex.QueryBuilder
 */
export function fetchEntities(
  entityClass: { new (...args): Entity },
  knex: Knex
) {
  const selectFields = getSqlFields2(entityClass, {
    includeTable: true,
    asProperty: true,
  });
  const queryBuilder = knex(getTableName(entityClass)).select(selectFields);
  return queryBuilder;
}
