import * as models from '../../models';
import * as _ from 'lodash';
import { EntityTypes } from '../../shared';

let entityMap = {};
entityMap[EntityTypes.BmpActivity] = models.BmpActivity;
entityMap[EntityTypes.CitizenReport] = models.CitizenReport;
entityMap[EntityTypes.ConstructionSite] = models.ConstructionSite;
entityMap[EntityTypes.ConstructionSiteInspection] =
  models.ConstructionSiteInspection;
entityMap[EntityTypes.Facility] = models.Facility;
entityMap[EntityTypes.FacilityInspection] = models.FacilityInspection;
entityMap[EntityTypes.IllicitDischarge] = models.IllicitDischarge;
entityMap[EntityTypes.MonitoringLocation] = models.MonitoringLocation;
entityMap[EntityTypes.Outfall] = models.Outfall;
entityMap[EntityTypes.OutfallInspection] = models.OutfallInspection;
entityMap[EntityTypes.Structure] = models.Structure;
entityMap[EntityTypes.StructureInspection] = models.StructureInspection;

export function getEntityType(Entity: typeof models.Entity): string {
  return _.findKey(entityMap, value => value === Entity);
}
