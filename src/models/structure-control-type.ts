import { Entity } from './entity';
import { HasCustomer } from './interfaces';
import { tableName, idField, internalField, sqlField } from '../decorators';

@tableName('StructureControlTypes')
export class StructureControlType extends Entity implements HasCustomer {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId?: string = undefined;

  @sqlField('Name') name: string = undefined;
}
