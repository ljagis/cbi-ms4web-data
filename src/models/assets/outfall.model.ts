import {
  HasCustomer,
  HasCompliance,
  HasCommunity,
  HasReceivingWaters,
  HasWatersheds,
} from '../interfaces';
import { Asset } from './asset.model';
import { tableName, sqlField, internalField } from '../../decorators';

@tableName('Outfalls')
export class Outfall extends Asset
  implements HasCustomer,
    HasCompliance,
    HasCommunity,
    HasReceivingWaters,
    HasWatersheds {
  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('Location') location: string = undefined;

  @sqlField('NearAddress') nearAddress: string = undefined;

  @sqlField('TrackingId') trackingId: string = undefined;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('Geometry.STY') lat: number = undefined;

  @sqlField('Geometry.STX') lng: number = undefined;

  @sqlField('Condition') condition: string = undefined;

  @sqlField('Material') material: string = undefined;

  @sqlField('Obstruction') obstruction: string = undefined;

  @sqlField('OutfallType') outfallType: string = undefined;

  @sqlField('OutfallSizeIn') outfallSizeIn: number = undefined;

  @sqlField('OutfallWidthIn') outfallWidthIn: number = undefined;

  @sqlField('OutfallHeightIn') outfallHeightIn: number = undefined;

  @sqlField('AdditionalInformation') additionalInformation: string = undefined;

  @sqlField('CommunityId') communityId: number = undefined;

  @sqlField('WatershedId') watershedId: number = undefined;

  @sqlField('SubwatershedId') subwatershedId: number = undefined;

  @sqlField('ReceivingWatersId') receivingWatersId: number = undefined;

  @sqlField('LatestInspectionId') latestInspectionId: number = null;

  @sqlField('LatestInspectionDate') latestInspectionDate: Date = null;
}
