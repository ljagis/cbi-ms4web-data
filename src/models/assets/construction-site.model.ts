import { Asset } from './asset.model';
import { IsNotEmpty } from 'class-validator';
import {
  HasCustomer,
  HasCompliance,
  HasCommunity,
  HasReceivingWaters,
  HasWatersheds,
  HasPhysicalAddress,
  HasProject,
} from '../interfaces';
import { internalField } from '../../decorators';
import { tableName, sqlField } from '../../decorators';

@tableName('ConstructionSites')
export class ConstructionSite extends Asset
  implements HasCustomer,
    HasCompliance,
    HasCommunity,
    HasReceivingWaters,
    HasWatersheds,
    HasPhysicalAddress,
    HasProject {
  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('TrackingId') trackingId: string = null;

  /**
   * Site Name
   */
  @sqlField('Name')
  @IsNotEmpty()
  name: string = null;

  @sqlField('PhysicalAddress1') physicalAddress1: string = null;

  @sqlField('PhysicalAddress2') physicalAddress2: string = null;

  @sqlField('PhysicalCity') physicalCity: string = null;

  @sqlField('PhysicalState') physicalState: string = null;

  @sqlField('PhysicalZip') physicalZip: string = null;

  @sqlField('ProjectId') projectId: number = null;

  /**
   * Municipal, Commercial, Residential, or Industrial
   *
   * @type {string}
   */
  @sqlField('ProjectType') projectType: string = null;

  /**
   * Approved, Pending, Expired, or No Permit on File. No Permit on File should be a red flag and generate a follow up.
   *
   * @type {string}
   */
  @sqlField('PermitStatus') permitStatus: string = null;

  /**
   * Acres, 2 decimals
   *
   * @type {number}
   */
  @sqlField('ProjectArea') projectArea: number = null;

  /**
   * Acres, 2 decimals
   *
   * @type {number}
   */
  @sqlField('DisturbedArea') disturbedArea: number = null;

  @sqlField('StartDate') startDate: Date = null;

  @sqlField('EstimatedCompletionDate') estimatedCompletionDate: Date = null;

  @sqlField('LatestInspectionId') latestInspectionId: number = null;

  @sqlField('LatestInspectionDate') latestInspectionDate: Date = null;

  /**
   * Based on final inspection date when final inspection is collected
   *
   * @type {Date}
   */
  @sqlField('CompletionDate') completionDate: Date = null;

  /**
   * Active, Not Started, Suspended or Completed
   *
   * @type {string}
   */
  @sqlField('ProjectStatus') projectStatus: string = null;

  @sqlField('CommunityId') communityId: number = null;

  @sqlField('WatershedId') watershedId: number = null;

  @sqlField('SubwatershedId') subwatershedId: number = null;

  @sqlField('ReceivingWatersId') receivingWatersId: number = null;

  /**
   * National Pollution Discharge Elimination System Permit
   *
   * @type {boolean}
   */
  @sqlField('NPDES') npdes: boolean = null;

  /**
   * U.S. Army Corps of Engineers Section 404 Permit
   *
   * @type {boolean}
   */
  @sqlField('USACE404') usace404: boolean = null;

  /**
   * Clean Water Act, Section 401 Certification
   *
   * @type {boolean}
   */
  @sqlField('State401') state401: boolean = null;

  /**
   * U.S. Army Corps of Engineers Nationwide Permit
   *
   * @type {boolean}
   */
  @sqlField('USACENWP') usacenwp: boolean = null;

  @sqlField('AdditionalInformation') additionalInformation: string = null;

  /**
   * Based on last inspection but also directly editable. Color coded based on level of enforcement:
   * green = compliant, yellow = verbal enforcement, red = enforcement > verbal
   *
   * @type {string}
   */
  @sqlField('ComplianceStatus') complianceStatus: string = null;

  @sqlField('Geometry.STY') lat: number = null;

  @sqlField('Geometry.STX') lng: number = null;
}
