import {
  idField,
  internalField,
  globalIdField,
  sqlField,
} from '../../decorators';
import { IsNotEmpty } from 'class-validator';
import { Entity } from '../entity';

export class Asset extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @globalIdField()
  @internalField()
  @sqlField('GlobalId')
  globalId?: string = undefined;

  @sqlField('CustomerId')
  @internalField()
  @IsNotEmpty()
  customerId?: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;

  @sqlField('DateAdded') dateAdded?: Date = undefined;
}
