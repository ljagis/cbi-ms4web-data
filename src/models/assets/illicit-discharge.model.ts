import {
  HasCustomer,
  HasCompliance,
  HasCommunity,
  HasReceivingWaters,
  HasWatersheds,
  HasPhysicalAddress,
  HasDateReported,
} from '../interfaces';
import { tableName, sqlField, internalField } from '../../decorators';
import { Investigation } from './investigation';

@tableName('IllicitDischarges')
export class IllicitDischarge extends Investigation
  implements HasCustomer,
    HasCompliance,
    HasCommunity,
    HasReceivingWaters,
    HasWatersheds,
    HasPhysicalAddress,
    HasDateReported {
  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('DateReported') dateReported: Date = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('PhysicalAddress1') physicalAddress1: string = undefined;

  @sqlField('PhysicalAddress2') physicalAddress2: string = undefined;

  @sqlField('PhysicalCity') physicalCity: string = undefined;

  @sqlField('PhysicalState') physicalState: string = undefined;

  @sqlField('PhysicalZip') physicalZip: string = undefined;

  @sqlField('PhoneNumber') phoneNumber: string = undefined;

  @sqlField('CellNumber') cellNumber: string = undefined;

  @sqlField('FaxNumber') faxNumber: string = undefined;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('Geometry.STY') lat: number = undefined;

  @sqlField('Geometry.STX') lng: number = undefined;

  @sqlField('Conversation') conversation: string = undefined;

  @sqlField('DischargeDescription') dischargeDescription: string = undefined;

  @sqlField('RequestCorrectiveAction')
  requestCorrectiveAction: string = undefined;

  @sqlField('DateEliminated') dateEliminated: Date = undefined;

  @sqlField('InvestigatorId') investigatorId: number = undefined;

  @sqlField('Investigator2Id') investigator2Id: number = undefined;

  @sqlField('CommunityId') communityId: number = undefined;

  @sqlField('WatershedId') watershedId: number = undefined;

  @sqlField('SubwatershedId') subwatershedId: number = undefined;

  @sqlField('ReceivingWatersId') receivingWatersId: number = undefined;

  @sqlField('AdditionalInformation') additionalInformation: string = undefined;
}
