import { Asset } from './asset.model';
import {
  HasCustomer,
  HasCompliance,
  HasCommunity,
  HasReceivingWaters,
  HasWatersheds,
  HasPhysicalAddress,
} from '../interfaces';

import { tableName, sqlField, internalField } from '../../decorators';

@tableName('Facilities')
export class Facility extends Asset
  implements HasCustomer,
    HasCompliance,
    HasCommunity,
    HasReceivingWaters,
    HasWatersheds,
    HasPhysicalAddress {
  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('TrackingId') trackingId: string = null;

  @sqlField('Name') name: string = undefined;

  @sqlField('PhysicalAddress1') physicalAddress1: string = null;

  @sqlField('PhysicalAddress2') physicalAddress2: string = null;

  @sqlField('PhysicalCity') physicalCity: string = null;

  @sqlField('PhysicalState') physicalState: string = null;

  @sqlField('PhysicalZip') physicalZip: string = null;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('Geometry.STY') lat: number = undefined;

  @sqlField('Geometry.STX') lng: number = undefined;

  @sqlField('AdditionalInformation') additionalInformation: string = undefined;

  @sqlField('Sector') sector: string = undefined;

  @sqlField('SICCode') sicCode: string = undefined;

  @sqlField('IsHighRisk') isHighRisk: boolean = false;

  @sqlField('InspectionFrequencyId') inspectionFrequencyId: number = undefined;

  @sqlField('YearBuilt') yearBuilt: number = undefined;

  @sqlField('SquareFootage') squareFootage: number = undefined;

  @sqlField('CommunityId') communityId: number = undefined;

  @sqlField('WatershedId') watershedId: number = undefined;

  @sqlField('SubwatershedId') subwatershedId: number = undefined;

  @sqlField('ReceivingWatersId') receivingWatersId: number = undefined;

  @sqlField('LatestInspectionId') latestInspectionId: number = null;

  @sqlField('LatestInspectionDate') latestInspectionDate: Date = null;
}
