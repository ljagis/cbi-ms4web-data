import { Asset } from './asset.model';
import { HasCustomer, HasCompliance } from '../interfaces';
import { tableName, sqlField } from '../../decorators';

@tableName('MonitoringLocations')
export class MonitoringLocation extends Asset
  implements HasCustomer, HasCompliance {
  @sqlField('Name') name: string = undefined;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('Geometry.STY') lat: number = undefined;

  @sqlField('Geometry.STX') lng: number = undefined;
}
