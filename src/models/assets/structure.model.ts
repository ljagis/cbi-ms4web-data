import { Asset } from './asset.model';
import {
  HasCustomer,
  HasCompliance,
  HasCommunity,
  HasReceivingWaters,
  HasWatersheds,
  HasProject,
  HasPhysicalAddress,
} from '../interfaces';
import { tableName, sqlField, internalField } from '../../decorators';

@tableName('Structures')
export class Structure extends Asset
  implements HasCustomer,
    HasCompliance,
    HasCommunity,
    HasReceivingWaters,
    HasWatersheds,
    HasProject,
    HasPhysicalAddress,
    HasProject {
  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('ControlType') controlType: string = undefined;

  @sqlField('TrackingId') trackingId: string = undefined;

  @sqlField('ProjectId') projectId: number = undefined;

  @sqlField('ProjectType') projectType: string = undefined;

  @sqlField('PhysicalAddress1') physicalAddress1: string = null;

  @sqlField('PhysicalAddress2') physicalAddress2: string = null;

  @sqlField('PhysicalCity') physicalCity: string = null;

  @sqlField('PhysicalState') physicalState: string = null;

  @sqlField('PhysicalZip') physicalZip: string = null;

  @sqlField('PermitStatus') permitStatus: string = undefined;

  @sqlField('InspectionFrequencyId') inspectionFrequencyId: number = undefined;

  @sqlField('MaintenanceAgreement') maintenanceAgreement: string = undefined;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('Geometry.STY') lat: number = undefined;

  @sqlField('Geometry.STX') lng: number = undefined;

  @sqlField('CommunityId') communityId: number = undefined;

  @sqlField('WatershedId') watershedId: number = undefined;

  @sqlField('SubwatershedId') subwatershedId: number = undefined;

  @sqlField('ReceivingWatersId') receivingWatersId: number = undefined;

  @sqlField('AdditionalInformation') additionalInformation: string = null;

  @sqlField('LatestInspectionId') latestInspectionId: number = null;

  @sqlField('LatestInspectionDate') latestInspectionDate: Date = null;
}
