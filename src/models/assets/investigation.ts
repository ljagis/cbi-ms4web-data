import { Asset } from './asset.model';
import {
  HasCustomFormTemplate,
  HasInspectionType,
  HasFollowUpDate,
} from '../interfaces';
import { sqlField } from '../../decorators/entity-decorators';

export abstract class Investigation extends Asset
  implements HasCustomFormTemplate, HasInspectionType, HasFollowUpDate {
  @sqlField('CustomFormTemplateId') customFormTemplateId: number = undefined;

  @sqlField('InspectionTypeId') inspectionTypeId: number = undefined;

  @sqlField('ConstructionSiteId') constructionSiteId: number = undefined;

  @sqlField('FollowUpDate') followUpDate: Date = undefined;

  // From InspectionType table
  inspectionType: string = undefined;
}
