import { Lookup } from './lookup';
import { tableName } from '../../decorators';

@tableName('OutfallTypes')
export class OutfallType extends Lookup {}
