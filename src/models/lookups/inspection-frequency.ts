import { Lookup } from './lookup';
import { tableName, sqlField } from '../../decorators';

@tableName('InspectionFrequencies')
export class InspectionFrequency extends Lookup {
  @sqlField('Days') days: number = undefined;
}
