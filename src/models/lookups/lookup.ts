import { Entity } from '../entity';
import { idField, sqlField } from '../../decorators';

export abstract class Lookup extends Entity {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('Sort') sort: number = undefined;
}
