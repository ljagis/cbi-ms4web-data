import { Lookup } from './lookup';
import { tableName } from '../../decorators';

@tableName('OutfallMaterials')
export class OutfallMaterial extends Lookup {}
