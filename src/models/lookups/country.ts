import { tableName, sqlField } from '../../decorators';
import { Entity } from '../entity';

@tableName('Countries')
export class Country extends Entity {
  @sqlField('Name') name: number = undefined;
  @sqlField('Code') code: number = undefined;
}
