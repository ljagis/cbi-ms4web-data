import { Lookup } from './lookup';
import { tableName } from '../../decorators';

@tableName('Sectors')
export class Sector extends Lookup {}
