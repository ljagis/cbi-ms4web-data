import * as chai from 'chai';
import { ConstructionSite } from './assets/construction-site.model';

describe('Entity tests', () => {
  describe('sqlField tests', () => {
    it('should be DisturbedArea', () => {
      chai
        .expect(ConstructionSite.sqlField(c => c.disturbedArea))
        .to.eq('DisturbedArea');
    });

    it('should be ConstructionSites.DisturbedArea', () => {
      chai
        .expect(ConstructionSite.sqlField(c => c.disturbedArea, true))
        .to.eq('ConstructionSites.DisturbedArea');
    });

    it('should be ConstructionSites.DisturbedArea as disturbedArea', () => {
      chai
        .expect(ConstructionSite.sqlField(c => c.disturbedArea, true, true))
        .to.eq('ConstructionSites.DisturbedArea as disturbedArea');
    });
  });
});
