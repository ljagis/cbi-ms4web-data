import { Entity } from './entity';
import { HasCustomer, HasCustomFormTemplate, Deletable } from './interfaces';
import { tableName, idField, internalField, sqlField } from '../decorators';

@tableName('InspectionType')
export class InspectionType extends Entity
  implements HasCustomer, HasCustomFormTemplate, Deletable {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('EntityType') entityType: string = undefined;

  @sqlField('Sort') sort: number = undefined;

  @sqlField('CustomFormTemplateId') customFormTemplateId: number = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;

  @sqlField('IsArchived') isArchived: boolean = undefined;
}
