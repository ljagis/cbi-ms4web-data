import { FileEntity } from './file-entity';

export interface FileWithLinks extends FileEntity {
  resourceUrl: string;
  fileUrl: string;
  thumbnailUrl: string;
}
