import { globalIdField, idField, internalField, sqlField } from '../decorators';

import { Entity } from './entity';
import { HasCustomFormTemplate } from './interfaces';

export class Inspection extends Entity implements HasCustomFormTemplate {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @globalIdField()
  @internalField()
  @sqlField('GlobalId')
  globalId?: string = undefined;

  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('CreatedDate') createdDate?: Date = undefined;

  @sqlField('InspectionDate') inspectionDate: Date = undefined;

  @sqlField('TimeIn') timeIn: Date = undefined;

  @sqlField('TimeOut') timeOut: Date = undefined;

  @sqlField('ScheduledInspectionDate')
  scheduledInspectionDate: Date = undefined;

  @sqlField('ComplianceStatus') complianceStatus: string = undefined;

  @sqlField('InspectorId') inspectorId: number = undefined;

  @sqlField('InspectorId2') inspectorId2: number = undefined;

  @sqlField('FollowUpInspectionId') followUpInspectionId: number = undefined;

  @sqlField('CustomFormTemplateId') customFormTemplateId: number = undefined;

  @sqlField('InspectionTypeId') inspectionTypeId: number = undefined;

  // Joined from InspectionType
  inspectionType: string = undefined;

  // Weather specific
  @sqlField('WeatherCondition') weatherCondition: string = undefined;

  @sqlField('WeatherTemperatureF') weatherTemperatureF: number = undefined;

  @sqlField('WeatherPrecipitationIn')
  weatherPrecipitationIn: number = undefined;

  @sqlField('WeatherLast72') weatherLast72: number = undefined;

  @sqlField('WeatherLast24') weatherLast24: number = undefined;

  @sqlField('WindDirection') windDirection: string = undefined;

  @sqlField('WindSpeedMph') windSpeedMph: number = undefined;

  @sqlField('WeatherStationId') weatherStationId: string = undefined;

  @sqlField('WeatherDateTime') weatherDateTime: Date = undefined;

  // end Weather specific

  @sqlField('AdditionalInformation') additionalInformation: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;
}
