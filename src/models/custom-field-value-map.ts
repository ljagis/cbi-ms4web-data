export class CustomFieldValueMap {
  /** Id of the custom field */
  id: number;

  value: any;

  constructor(id: number, value) {
    this.id = id;
    this.value = value;
  }
}
