import { Entity } from './entity';

import { tableName, idField, internalField, sqlField } from '../decorators';

@tableName('ReceivingWaters')
export class ReceivingWater extends Entity {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('Name') name: string = undefined;
}
