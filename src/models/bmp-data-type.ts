import { Entity } from './entity';
import { Deletable } from './interfaces';

import { tableName, internalField, sqlField, idField } from '../decorators';

@tableName('BMPDataTypes')
export class BmpDataType extends Entity implements Deletable {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId?: string = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;
}
