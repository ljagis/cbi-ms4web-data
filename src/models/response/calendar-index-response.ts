import { StructureInspection } from '../structure-inspection.model';
import { OutfallInspection } from '../outfall-inspection.model';
import { FacilityInspection } from '../facility-inspection.model';
import { ConstructionSiteInspection } from '../construction-site-inspection.model';
import {
  CitizenReport,
  ConstructionSite,
  Facility,
  MonitoringLocation,
  Outfall,
  Structure,
  IllicitDischarge,
} from '../assets';
import { BmpDetail } from '../bmp-detail';

export interface CalendarIndexItem {
  bmpDetails?: BmpDetail[];
  citizenReports?: CitizenReport[];
  constructionSites?: ConstructionSite[];
  facilities?: Facility[];
  monitoringLocations?: MonitoringLocation[];
  outfalls?: Outfall[];
  structures?: Structure[];
  illicitDischarges?: IllicitDischarge[];
  constructionSiteInspections?: ConstructionSiteInspection[];
  facilityInspections?: FacilityInspection[];
  outfallInspections?: OutfallInspection[];
  structureInspections?: StructureInspection[];
}

export interface CalendarIndexResponse {
  [date: string]: CalendarIndexItem;
}
