import { sqlField, tableName } from '../decorators';

import { Entity } from './entity';

@tableName('States')
export class State extends Entity {
  @sqlField('Name') name: string = undefined;

  @sqlField('Abbr') abbr: string = undefined;

  @sqlField('CountryCode') countryCode: string = undefined;
}
