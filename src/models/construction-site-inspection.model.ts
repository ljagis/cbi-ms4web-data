import { InspectionWithActions } from './inspection-with-actions';

import { tableName, sqlField } from '../decorators';

import { IsPositive } from 'class-validator';

@tableName('ConstructionSiteInspections')
export class ConstructionSiteInspection extends InspectionWithActions {
  @sqlField('ConstructionSiteId')
  @IsPositive()
  constructionSiteId: number = undefined;

  @sqlField('IsSiteActive') isSiteActive: boolean = undefined;

  @sqlField('IsPlanOnSite') isPlanOnSite: boolean = undefined;

  @sqlField('IsSitePermitted') isSitePermitted: boolean = undefined;

  @sqlField('ArePlanRecordsCurrent') arePlanRecordsCurrent: boolean = undefined;

  @sqlField('AreErosionControlsAcceptable')
  areErosionControlsAcceptable: boolean = undefined;

  @sqlField('AreStructuralControlsAcceptable')
  areStructuralControlsAcceptable: boolean = undefined;

  @sqlField('IsWasteManagementAcceptable')
  isWasteManagementAcceptable: boolean = undefined;

  @sqlField('IsMaintenanceOfControlsAcceptable')
  isMaintenanceOfControlsAcceptable: boolean = undefined;

  @sqlField('AreLocalControlsAcceptable')
  areLocalControlsAcceptable: boolean = undefined;

  @sqlField('AreStabilizationControlsAcceptable')
  areStabilizationControlsAcceptable: boolean = undefined;

  @sqlField('AreTrackingControlsAcceptable')
  areTrackingControlsAcceptable: boolean = undefined;

  @sqlField('AreOutfallVelocityControlsAcceptable')
  areOutfallVelocityControlsAcceptable: boolean = undefined;

  @sqlField('AreNonStormWaterControlsAcceptable')
  areNonStormWaterControlsAcceptable: boolean = undefined;

  @sqlField('Geometry.STY') lat: number = null;

  @sqlField('Geometry.STX') lng: number = null;

  @sqlField('GeometryError') geometryError: number = null;
}
