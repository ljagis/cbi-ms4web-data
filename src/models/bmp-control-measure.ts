import { Entity } from './entity';
import { Deletable, Sortable } from './interfaces';

import { tableName, internalField, sqlField, idField } from '../decorators';
import * as validators from 'class-validator';

@tableName('BmpControlMeasures')
export class BmpControlMeasure extends Entity implements Deletable, Sortable {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId?: string = undefined;

  @sqlField('Name')
  @validators.IsNotEmpty()
  name: string = undefined;

  @sqlField('Sort') sort?: number = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;
}
