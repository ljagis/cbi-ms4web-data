import { idField, jsonAttribute, sqlField, tableName } from '../decorators';

import { Entity } from './entity';

@tableName('Customers')
export class Customer extends Entity {
  @idField()
  @sqlField('Id')
  id?: string = undefined;

  @sqlField('FullName') fullName?: string = undefined;

  @sqlField('CountryCode') countryCode?: string = undefined;

  @sqlField('AerialProvider') aerialProvider?: string = undefined;

  @sqlField('StateAbbr') stateAbbr?: string = undefined;

  @sqlField('MapServiceUrl') mapServiceUrl?: string = undefined;

  @sqlField('LogoFileName') logoFileName?: string = undefined;

  @sqlField('LogoThumbnailName') logoThumbnailName?: string = undefined;

  @sqlField('Plan') plan?: string = undefined;

  @sqlField('DefaultRoute') defaultRoute?: string = undefined;

  @sqlField('Timezone') timezone?: string = undefined;

  @sqlField('UnlimitedCustomFormTemplates')
  unlimitedCustomFormTemplates?: boolean = undefined;

  @sqlField('UnlimitedAssets') unlimitedAssets?: boolean = undefined;

  @sqlField('Features')
  @jsonAttribute()
  features?: CustomerFeatures = undefined;

  @sqlField('HiddenFields')
  @jsonAttribute()
  hiddenFields?: string[] = undefined;

  @sqlField('DateAdded') dateAdded?: Date = undefined;

  @sqlField('DailyInspectorEmail') dailyInspectorEmail?: boolean = undefined;

  @sqlField('MondayInspectorEmail') mondayInspectorEmail?: boolean = undefined;

  @sqlField('MondayAdminEmail') mondayAdminEmail?: boolean = undefined;

  @sqlField('SwmpEnabled') swmpEnabled?: boolean = undefined;
}

export interface CustomerFeatures {
  calendar: boolean;
  fileGallery: boolean;
  bmps: boolean;
  swmp: boolean;
  reports: boolean;
  constructionSites: boolean;
  outfalls: boolean;
  structuralControls: boolean;
  facilities: boolean;
  illicitDischarges: boolean;
  citizenReports: boolean;
  extraLayers: boolean;
  customFormTemplates: boolean;
  exporting: boolean;
  customFields: boolean;
  swmpAnnualReport: boolean;
}

export type CustomerFeaturesUnion = keyof CustomerFeatures;
