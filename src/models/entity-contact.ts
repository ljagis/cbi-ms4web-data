import { idField, internalField, sqlField, tableName } from '../decorators';

import { Deletable } from './interfaces';
import { Entity } from './entity';
import { IsNotEmpty } from 'class-validator';

@tableName('EntityContacts')
export class EntityContact extends Entity implements Deletable {
  // explicitly set to `undefined` for typescript to generate properties

  @idField()
  @sqlField('Id')
  id: number = undefined;

  @sqlField('GlobalIdFk')
  @internalField()
  globalIdFk: string = undefined;

  @sqlField('EntityType')
  @IsNotEmpty()
  entityType: string = undefined;

  @sqlField('ContactId')
  @IsNotEmpty()
  contactId: number = undefined;

  @sqlField('IsDeleted') isDeleted?: boolean = false;

  @sqlField('IsPrimary') isPrimary?: boolean = true;

  @sqlField('DateAdded') dateAdded: Date = undefined;

  @sqlField('DateRemoved') dateRemoved: Date = undefined;
}
