import { Entity } from './entity';
import {
  tableName,
  idField,
  globalIdField,
  internalField,
  sqlField,
} from '../decorators';

@tableName('Files')
export class FileEntity extends Entity {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @globalIdField()
  @internalField()
  @sqlField('GlobalId')
  globalId: string = undefined;

  @internalField()
  @sqlField('GlobalIdFK')
  globalIdFk: string = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('EntityType') entityType: string = undefined;

  @sqlField('OriginalFileName') originalFileName: string = undefined;

  @sqlField('FileSize') fileSize: number = undefined;

  @sqlField('FileName') fileName: string = undefined;

  @sqlField('ThumbnailName') thumbnailName: string = undefined;

  @sqlField('MIMEType') mimeType: string = undefined;

  @sqlField('AddedDate') addedDate: Date = undefined;

  @sqlField('AddedBy') addedBy: string = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted: boolean = undefined;
}

/**
 * File Entity + common fields from entityId and common entity name
 */
export interface FileEntityJoined extends FileEntity {
  entityId: number;
  entityName: string;
  inspectionId: number;
}
