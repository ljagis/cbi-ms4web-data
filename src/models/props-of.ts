/** Like Partial<T>, except each property is type `any` */
export type PropsOf<T> = { [P in keyof T]?: any };
