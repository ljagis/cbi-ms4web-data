import { Entity } from './entity';
import { Deletable } from './interfaces';

import { tableName, internalField, sqlField } from '../decorators';

@tableName('BmpActivities')
export class BmpActivity extends Entity implements Deletable {
  @sqlField('GlobalId')
  @internalField()
  globalId?: string = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId?: string = undefined;

  @sqlField('DateAdded') dateAdded: Date = undefined;

  @sqlField('Comments') comments: string = undefined;

  @sqlField('CompletedDate') completedDate: Date = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('DueDate') dueDate: Date = undefined;

  @sqlField('IsCompleted') isCompleted: boolean = false;

  @sqlField('Title') title: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;

  @sqlField('TaskId') taskId?: number = undefined;
}
