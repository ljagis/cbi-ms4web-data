/** Construction Site Project Types */
export const projectTypes = {
  commercial: 'Commercial Development',
  municipal: 'Municipal',
  residential: 'Residential Development',
  industrial: 'Industrial',
  government: 'Government',
  private: 'Private',
  medical: 'Medical',
  oilAndGas: 'Oil & Gas',
  subdivision: 'Subdivision',
  public: 'Public',
  cityProject: 'City Project',
  institutional: 'Institutional',
  multiFamily: 'Multi-Family Residential Lot',
  singleFamily: 'Single-Family Residential Lot',
};

/**
 * Permit status
 */
export const permitStatus = {
  active: 'Active',
  approved: 'Approved',
  pending: 'Pending',
  expired: 'Expired',
  noPermit: 'No Permit on File',
  terminated: 'Terminated',
  other: 'Other',
};

/**
 * Construction Site Project Status
 */
export const projectStatus = {
  active: 'Active',
  completed: 'Completed',
  notStarted: 'Not Started',
  suspended: 'Suspended',
};

export interface Compliance {
  name: string;
  level: string;
}

export const outfallConditions = [
  'Good',
  'Needs Repairs',
  'Washout',
  'Inaccessible',
];
export const obstructionSeverities = [
  'Not Obstructed',
  'Partially Obstructed',
  'Fully Obstructed',
];
export const accountStatus = {
  ENABLED: 'ENABLED',
  DISABLED: 'DISABLED',
};
