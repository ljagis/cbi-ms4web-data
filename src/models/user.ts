import { Entity } from './entity';
import * as _ from 'lodash';
import * as sp from 'stormpath';
import * as val from 'class-validator';
import { idField, tableName, sqlField } from '../decorators';

@tableName('Users')
export class User extends Entity implements sp.AccountData {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  // same as id
  @sqlField('Href') href?: string = undefined;

  // id in Okta
  @sqlField('OktaId') oktaId?: string = undefined;

  @sqlField('Username') username?: string = undefined;

  @sqlField('Email')
  @val.IsEmail()
  email?: string = undefined;

  @sqlField('GivenName')
  @val.IsNotEmpty()
  givenName?: string = undefined;

  @sqlField('MiddleName') middleName?: string = undefined;

  @sqlField('Surname')
  @val.IsNotEmpty()
  surname?: string = undefined;

  @sqlField('Status') status?: string = undefined;

  @sqlField('CreatedAt') createdAt?: Date = undefined;

  @sqlField('ModifiedAt') modifiedAt?: Date = undefined;

  customData?: any = undefined;

  fullName?: string = undefined;

  password?: string = undefined;

  // would love to use a property getter on this, but it doesn't serialize
  getFullName() {
    return _.compact([this.givenName, this.middleName, this.surname]).join(' ');
  }
}
