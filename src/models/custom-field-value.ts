import { Entity } from './entity';

import { tableName, internalField, sqlField, idField } from '../decorators';

@tableName('CustomFieldValues')
export class CustomFieldValue extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomFieldId') customFieldId: number = undefined;

  @sqlField('GlobalIdFK')
  @internalField()
  globalIdFk: string = undefined;

  @sqlField('Value') value: string = undefined;
}
