import * as Bluebird from 'bluebird';
import { QueryBuilderOverride } from './query-builder-override';

export interface DeleteOptions {
  force: boolean;
}

export interface Creatable<T> {
  insert(customerId: string, data: Partial<T>): Bluebird<T>;
}
export interface Fetchable<T> {
  fetch(
    customerId: string,
    queryBuilderOverrides?: QueryBuilderOverride
  ): Bluebird<T[]>;

  fetchById(
    customerId: string,
    id: number,
    queryBuilderOverrides?: QueryBuilderOverride
  ): Bluebird<T>;
}

export interface Updateable<T> {
  update(customerId: string, id: number, data: Partial<T>): Bluebird<T>;
}

export interface CrudDeletable {
  delete(
    customerId: string,
    id: number,
    options?: DeleteOptions
  ): Bluebird<void | number>;
}
