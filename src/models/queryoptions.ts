export interface QueryOptions {
  limit?: number;
  offset?: number;
  orderByField?: string;
  orderByDirection?: string;

  outFields?: string[];

  // if orderByRaw exist, will use this instead of orderByField
  orderByRaw?: string;
}

export interface WatershedQueryOptions extends QueryOptions {
  isSubWatershed?: boolean;
}
