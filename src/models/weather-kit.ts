export interface WeatherKitCurrentResponse {
  currentWeather: WeatherKitCurrent;
}

export interface WeatherKitForecastResponse {
  forecastDaily: WeatherKitForecast;
}

export interface WeatherKitData {
  name: string;
  metadata: {
    attributionURL: string;
    expireTime: string;
    latitude: number;
    longitude: number;
    readTime: string;
    reportedTime: string;
    units: string;
    version: number;
  };
}

export interface WeatherKitForecastDay {
  forecastStart: string;
  forecastEnd: string;
  conditionCode: string;
  maxUvIndex: number;
  moonPhase: string;
  moonrise: string;
  moonset: string;
  precipitationAmount: number;
  precipitationChance: number;
  precipitationType: string;
  snowfallAmount: number;
  solarMidnight: string;
  solarNoon: string;
  sunrise: string;
  sunriseCivil: string;
  sunriseNautical: string;
  sunriseAstronomical: string;
  sunset: string;
  sunsetCivil: string;
  sunsetNautical: string;
  sunsetAstronomical: string;
  temperatureMax: number;
  temperatureMin: number;
  daytimeForecast: {
    forecastStart: string;
    forecastEnd: string;
    cloudCover: number;
    conditionCode: string;
    humidity: number;
    precipitationAmount: number;
    precipitationChance: number;
    precipitationType: string;
    snowfallAmount: number;
    windDirection: number;
    windSpeed: number;
  };
  overnightForecast: {
    forecastStart: string;
    forecastEnd: string;
    cloudCover: number;
    conditionCode: string;
    humidity: number;
    precipitationAmount: number;
    precipitationChance: number;
    precipitationType: string;
    snowfallAmount: number;
    windDirection: number;
    windSpeed: number;
  };
}

export interface WeatherKitCurrent extends WeatherKitData {
  asOf: string;
  cloudCover: number;
  cloudCoverLowAltPct: number;
  cloudCoverMidAltPct: number;
  cloudCoverHighAltPct: number;
  conditionCode: string;
  daylight: boolean;
  humidity: number;
  precipitationIntensity: number;
  pressure: number;
  pressureTrend: string;
  temperature: number;
  temperatureApparent: number;
  temperatureDewPoint: number;
  uvIndex: number;
  visibility: number;
  windDirection: number;
  windGust: number;
  windSpeed: number;
}

export interface WeatherKitForecast extends WeatherKitData {
  days: WeatherKitForecastDay[];
}
