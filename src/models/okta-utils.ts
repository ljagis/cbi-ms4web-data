import { User } from '@okta/okta-sdk-nodejs';
import { Account } from 'stormpath';

/**
 * converts okta user to stormpath user
 */
export function oktaToStormpathUser(oktaUser: Partial<User>): Partial<Account> {
  const href =
    oktaUser && oktaUser._links && oktaUser._links.self && oktaUser._links.self
      ? oktaUser._links.self.href
      : null;
  return {
    createdAt: oktaUser.created,
    email: oktaUser.profile.email,
    givenName: oktaUser.profile.firstName,
    href: href,
    modifiedAt: oktaUser.lastUpdated,
    status: oktaToStormpathStatus(oktaUser.status),
    surname: oktaUser.profile.lastName,
    username: oktaUser.profile.login || oktaUser.profile.email,
  };
}

export function stormpathToOktaUser(spUser: Partial<Account>): Partial<User> {
  return {
    created: spUser.createdAt,
    profile: {
      email: spUser.email,
      firstName: spUser.givenName,
      lastName: spUser.surname,
      login: spUser.username,
      stormpathHref: spUser.href,
    },
    lastUpdated: spUser.modifiedAt,
    status: stormpathToOktaStatus(spUser.status),
  };
}

export function oktaToStormpathStatus(
  oktaStatus: string
): 'ENABLED' | 'DISABLED' {
  switch (oktaStatus) {
    case 'STAGED':
    case 'PROVISIONED':
    case 'ACTIVE':
    case 'RECOVERY':
    case 'LOCKED_OUT':
    case 'PASSWORD_EXPIRED':
      return 'ENABLED';
    case 'SUSPENDED':
    case 'DEPROVISIONED':
      return 'DISABLED';
  }
  return 'DISABLED';
}

export function stormpathToOktaStatus(stormpathStatus: string) {
  switch (stormpathStatus) {
    case 'ENABLED':
      return 'ACTIVE';
    case 'DISABLED':
      return 'SUSPENDED';
  }
  return 'SUSPENDED';
}
