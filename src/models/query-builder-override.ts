import * as Knex from 'knex';

export type QueryBuilderOverride = (
  queryBuilder: Knex.QueryBuilder
) => Knex.QueryBuilder;
