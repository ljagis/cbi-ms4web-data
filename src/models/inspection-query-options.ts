import { QueryOptions } from './queryoptions';

export interface InspectionQueryOptions extends QueryOptions {
  inspectionDateFrom?: Date;
  inspectionDateTo?: Date;
  scheduledInspectionDateFrom?: Date;
  scheduledInspectionDateTo?: Date;
  joinAssetTable?: boolean;
  staffInspection?: boolean;
}

export interface ConstructionSiteInspectionQueryOptions extends QueryOptions {
  constructionSiteProjectStatus?: string;
}

export interface StructureInspectionQueryOptions extends QueryOptions {}

export interface FacilityInspectionQueryOptions extends QueryOptions {}

export interface OutfallInspectionQueryOptions extends QueryOptions {}
