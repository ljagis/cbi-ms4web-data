import { Entity } from './entity';
import { IsNotEmpty, IsEmail } from 'class-validator';

import { idField, tableName, sqlField } from '../decorators';

@tableName('AnnualReport')
export class AnnualReport extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @IsNotEmpty()
  customerId: string = undefined;

  @sqlField('SwmpId')
  @IsNotEmpty()
  swmpId: number = undefined;

  @sqlField('Year')
  @IsNotEmpty()
  year: number = undefined;

  @sqlField('Report') report: string = undefined;

  @sqlField('ContactName') contactName: string = undefined;

  @sqlField('Phone') phone: string = undefined;

  @sqlField('EmailAddress')
  @IsEmail()
  emailAddress: string = undefined;

  @sqlField('MailingAddress') mailingAddress: string = undefined;
}
