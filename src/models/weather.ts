/**
 * This should be the generic interface for weather in case we change services again.
 */
export interface Weather {
  /**
   * timestamp of weather
   */
  time: Date;

  /**
   * Short description of weather 1-3 words
   */
  condition: string;

  /**
   * Temperature in Fahrenheit
   */
  temperatureF: number;

  /** Precipitation in inches */
  preciptiationIn: number;

  /** The direction that the wind is coming from in degrees, with true north at 0° and progressing clockwise */
  windDirection: number;

  /** The wind gust speed in miles per hour */
  windSpeedMph: number;

  /**
   * A machine-readable text summary of this data point, suitable for selecting an
   * icon for display. If defined, this property will have one of the following values:
   * clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day,
   * or partly-cloudy-night. (Developers should ensure that a sensible default is
   * defined, as additional values, such as hail, thunderstorm, or tornado, may be defined in the future.)
   */
  icon: string;

  /** Accumulation of rainfall from last 72 hours from when user clicks 'Get Weather' at requested location  */
  last72: number;

  /** Total rainfall over the past 24 hours */
  last24: number;
}
