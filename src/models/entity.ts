import { idField, sqlField, getTableName, getSqlField } from '../decorators';

export class Entity {
  @idField()
  @sqlField('Id')
  id?: number | string = undefined;

  static get tableName() {
    return getTableName(this);
  }

  static getTableName(): string {
    return getTableName(this);
  }

  static getSqlField<T extends Entity>(
    propertyOrLambda: keyof T | ((args?: T) => any),
    includeTable?: boolean,
    asProperty?: boolean
  ): string {
    return getSqlField(this, propertyOrLambda, includeTable, asProperty);
  }

  static sqlField<T extends Entity>(
    this: { new (...args): T },
    propertyOrLambda: keyof T | ((args?: T) => any),
    includeTable?: boolean,
    asProperty?: boolean
  ): string {
    return getSqlField(this, propertyOrLambda, includeTable, asProperty);
  }
}
