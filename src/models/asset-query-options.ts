import { QueryOptions } from './queryoptions';
import { InvestigationQueryOptions } from './investigation-query-options';

export interface AssetQueryOptions extends QueryOptions {
  addedFrom?: Date;
  addedTo?: Date;
  completeAfter?: Date;
  inspectedFrom?: Date;
  inspectedTo?: Date;
  ids?: number[];

  // this filter limits to only assets contractor is a contact of
  contractorEmail?: string;
  includeLastInspector?: boolean;
}

export interface ConstructionSiteQueryOptions extends AssetQueryOptions {
  name?: string;
  projectStatus?: string;
  active?: boolean;
}

export interface FacilityQueryOptions extends AssetQueryOptions {}

export interface MonitoringLocationQueryOptions extends AssetQueryOptions {}

export interface OutfallQueryOptions extends AssetQueryOptions {}

export interface StructureQueryOptions extends AssetQueryOptions {}

export interface IllicitDischargeQueryOptions
  extends InvestigationQueryOptions {
  ownerName?: string;
}

export interface CitizenReportQueryOptions extends InvestigationQueryOptions {
  citizenName?: string;
}
