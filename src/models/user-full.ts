import { User } from './user';

export class UserFull extends User {
  role?: string;
}
