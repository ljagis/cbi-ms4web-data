export interface DsForecastResponse {
  latitude: number;
  longitude: number;
  timezone: string;
  currently: DsCurrently;
  minutely: DsMinutely;
  hourly: DsHourly;
  daily: DsDaily;
  flags: DsFlags;
  offset: number;
}

export interface DsCurrently {
  time: number;
  summary: string;
  icon: string;
  nearestStormDistance?: number;
  nearestStormBearing?: number;
  precipIntensity: number;
  precipProbability: number;
  temperature: number;
  apparentTemperature: number;
  dewPoint: number;
  humidity: number;
  pressure: number;
  windSpeed: number;
  windGust: number;
  windBearing: number;
  cloudCover: number;
  uvIndex: number;
  visibility: number;
  ozone: number;
  precipType?: string;
}

export interface DsDaily {
  summary: string;
  icon: string;
  data: DsDailyDatum[];
}

export interface DsDailyDatum {
  time: number;
  summary: string;
  icon: string;
  sunriseTime: number;
  sunsetTime: number;
  moonPhase: number;
  precipIntensity: number;
  precipIntensityMax: number;
  precipIntensityMaxTime: number;
  precipProbability: number;
  precipType: string;
  temperatureHigh: number;
  temperatureHighTime: number;
  temperatureLow: number;
  temperatureLowTime: number;
  apparentTemperatureHigh: number;
  apparentTemperatureHighTime: number;
  apparentTemperatureLow: number;
  apparentTemperatureLowTime: number;
  dewPoint: number;
  humidity: number;
  pressure: number;
  windSpeed: number;
  windGust: number;
  windGustTime: number;
  windBearing: number;
  cloudCover: number;
  uvIndex: number;
  uvIndexTime: number;
  visibility: number;
  ozone: number;
  temperatureMin: number;
  temperatureMinTime: number;
  temperatureMax: number;
  temperatureMaxTime: number;
  apparentTemperatureMin: number;
  apparentTemperatureMinTime: number;
  apparentTemperatureMax: number;
  apparentTemperatureMaxTime: number;
}

export interface DsFlags {
  sources: string[];
  'nearest-station': number;
  units: string;
}

export interface DsHourly {
  summary: string;
  icon: string;
  data: DsCurrently[];
}

export interface DsMinutely {
  summary: string;
  icon: string;
  data: DsMinutelyDatum[];
}

export interface DsMinutelyDatum {
  time: number;
  precipIntensity: number;
  precipProbability: number;
}
