import { Entity } from './entity';
import { MG } from './mg';
import { IsNotEmpty } from 'class-validator';

import { idField, tableName, sqlField } from '../decorators';

@tableName('BMP')
export class BMP extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('SwmpId')
  @IsNotEmpty()
  swmpId: number = undefined;

  @sqlField('Number') number: string = undefined;

  @sqlField('Title') title: string = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('StartYear') startYear: number = undefined;

  @sqlField('MinimumControlMeasures')
  minimumControlMeasures: string | string[] = undefined;

  measurableGoals: MG[] = [];
}
