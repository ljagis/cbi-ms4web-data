import { AssetQueryOptions } from './asset-query-options';

export interface InvestigationQueryOptions extends AssetQueryOptions {
  reportedFrom?: Date;
  reportedTo?: Date;
  followUpFrom?: Date;
  followUpTo?: Date;
  constructionSiteId?: number;
  constructionSiteIds?: number[];
}
