import { Inspection } from './inspection';

import { tableName, sqlField } from '../decorators';

import { IsPositive } from 'class-validator';

@tableName('OutfallInspections')
export class OutfallInspection extends Inspection {
  @sqlField('OutfallId')
  @IsPositive()
  outfallId?: number = undefined;

  @sqlField('DaysSinceLastRain') daysSinceLastRain: number = undefined;

  @sqlField('DischargeDescription') dischargeDescription: string = undefined;

  @sqlField('DryOrWetWeatherInspection')
  dryOrWetWeatherInspection: string = undefined;

  @sqlField('Color') color: string = undefined;

  @sqlField('Clarity') clarity: string = undefined;

  @sqlField('Odor') odor: string = undefined;

  @sqlField('Foam') foam: string = undefined;

  @sqlField('Sheen') sheen: string = undefined;

  @sqlField('SustainedSolids') sustainedSolids: string = undefined;

  @sqlField('SetSolids') setSolids: string = undefined;

  @sqlField('FloatingSolids') floatingSolids: string = undefined;

  @sqlField('PH') pH: number = undefined;

  @sqlField('TemperatureF') temperatureF: number = undefined;

  @sqlField('DOmgl') domgl: number = undefined;

  @sqlField('Turbidity') turbidity: string = undefined;

  @sqlField('Cond') cond: string = undefined;

  @sqlField('DOSaturation') doSaturation: number = undefined;

  @sqlField('FlowrateGPM') flowrateGpm: number = undefined;

  @sqlField('Copper') copper: number = undefined;

  @sqlField('Phenols') phenols: number = undefined;

  @sqlField('Ammonia') ammonia: number = undefined;

  @sqlField('Detergents') detergents: number = undefined;

  @sqlField('TPO4') tpo4: number = undefined;

  @sqlField('CL2') cl2: number = undefined;

  @sqlField('BOD') bod: number = undefined;

  @sqlField('COD') cod: number = undefined;

  @sqlField('TSS') tss: number = undefined;

  @sqlField('NO3') no3: number = undefined;

  @sqlField('FecalColiform') fecalColiform: number = undefined;

  @sqlField('EColi') ecoli: number = undefined;

  @sqlField('Geometry.STY') lat: number = null;

  @sqlField('Geometry.STX') lng: number = null;

  @sqlField('GeometryError') geometryError: number = null;
}
