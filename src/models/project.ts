import { Entity } from './entity';

import { tableName, idField, internalField, sqlField } from '../decorators';

@tableName('Projects')
export class Project extends Entity {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('Name') name: string = undefined;
}
