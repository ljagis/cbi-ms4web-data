import { Inspection } from './inspection';
import { sqlField } from '../decorators';

export abstract class InspectionWithActions extends Inspection {
  @sqlField('DateResolved') dateResolved: Date = undefined;
}
