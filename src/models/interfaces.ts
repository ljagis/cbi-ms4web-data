export interface HasGlobalId {
  globalId?: string;
}

export interface HasCustomer {
  customerId?: string;
}

export interface HasCompliance {
  complianceStatus: string;
}

export interface HasProject {
  projectId: number;
  projectType: string;
}

export interface HasCommunity {
  communityId: number;
}

export interface HasWatersheds {
  watershedId: number;
  subwatershedId: number;
}

export interface HasProject {
  projectId: number;
}

export interface HasReceivingWaters {
  receivingWatersId: number;
}

export interface HasPhysicalAddress {
  physicalAddress1?: string;
  physicalAddress2?: string;
  physicalCity?: string;
  physicalState?: string;
  physicalZip?: string;
}

export interface HasDateReported {
  dateReported?: Date;
}

/**
 * The class that implements this interface will be able to "archive" by marking the `isDeleted` value as `true`
 */
export interface Deletable {
  isDeleted?: boolean;
}

export interface Sortable {
  sort?: number;
}

export interface HasCustomFormTemplate {
  customFormTemplateId: number;
}

export interface HasInspectionType {
  inspectionTypeId: number;
}

export interface HasFollowUpDate {
  followUpDate?: Date;
}
