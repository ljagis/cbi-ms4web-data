import { FileWithLinks } from './file-with-links';
import { Watershed } from './watershed';
import { CustomFieldValue } from './custom-field-value';
import { ReceivingWater } from './receivingwater';
import { Community } from './community';
import { Contact } from './contact';
import {
  ConstructionSite,
  Structure,
  Outfall,
  Facility,
  CitizenReport,
  IllicitDischarge,
} from './assets';
import { Project } from './project';
import { ConstructionSiteInspection } from './construction-site-inspection.model';
import { OutfallInspection } from './outfall-inspection.model';
import { FacilityInspection } from './facility-inspection.model';
import { StructureInspection } from './structure-inspection.model';
import { StructureControlType } from './structure-control-type';

export interface AssetDetailsResult {
  // citizenReport?: CitizenReport;
  // illicitDischarge?: IllicitDischarge;
  communities: Community[];
  receivingWaters: ReceivingWater[];
  watersheds: Watershed[];
  customFields: CustomFieldValue[];
  files: FileWithLinks[];
}

export interface ConstructionSiteDetailsResult extends AssetDetailsResult {
  constructionSite: ConstructionSite;
  projects: Project[];
  inspections: ConstructionSiteInspection[];
  contacts: Contact[];
  citizenReports: CitizenReport[];
  illicitDischarges: IllicitDischarge[];
}

export interface OutfallDetailsResult extends AssetDetailsResult {
  outfall: Outfall;
  inspections: OutfallInspection[];
}

export interface FacilityDetailsResult extends AssetDetailsResult {
  facility: Facility;
  inspections: FacilityInspection[];
  contacts: Contact[];
}

export interface StructureDetailsResult extends AssetDetailsResult {
  structure: Structure;
  inspections: StructureInspection[];
  projects: Project[];
  contacts: Contact[];
  controlTypes: StructureControlType[];
}

export interface CitizenReportDetailsResult extends AssetDetailsResult {
  citizenReport: CitizenReport;
  constructionSite?: ConstructionSite;
  contacts?: Contact[];
}
export interface IllicitDischargeDetailsResult extends AssetDetailsResult {
  illicitDischarge: IllicitDischarge;
  constructionSite?: ConstructionSite;
  contacts?: Contact[];
}
