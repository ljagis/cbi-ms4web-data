import { interfaces } from 'inversify-express-utils';
import { UserFull } from './user-full';
import { Role } from '../shared';
import {
  FeaturePermissionUnion,
  PermissionUnion,
  roleFeaturePermissions,
} from '../shared/roles';
import { Customer } from './customer';

export class Principal implements interfaces.Principal {
  details: UserFull;
  selectedCustomer?: Customer;

  constructor(details: any) {
    this.details = details;
  }
  async isAuthenticated(): Promise<boolean> {
    return await !!this.details;
  }
  async isResourceOwner(resourceId: any): Promise<boolean> {
    return await true;
  }
  async isInRole(role: string): Promise<boolean> {
    return await (this.details && this.details.role === role);
  }

  async isContractor(): Promise<boolean> {
    return await this.isInRole(Role.contractor);
  }

  async hasFeaturePermission(
    feature: FeaturePermissionUnion,
    permission: PermissionUnion
  ): Promise<boolean> {
    const role = this.details && this.details.role;
    if (role === Role.super) {
      return true;
    }
    const featurePermission = role && roleFeaturePermissions[role];
    return (
      featurePermission &&
      featurePermission[feature] &&
      featurePermission[feature][permission]
    );
  }
}
