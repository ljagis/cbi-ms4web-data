import { Entity } from './entity';
import { HasCustomer } from './interfaces';
import { tableName, internalField, sqlField } from '../decorators';

@tableName('Watersheds')
export class Watershed extends Entity implements HasCustomer {
  @internalField()
  @sqlField('CustomerId')
  customerId: string = undefined;

  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('IsSubWatershed') isSubWatershed: boolean = undefined;
}
