import { IsNotEmpty } from 'class-validator';
import { idField, sqlField, tableName } from '../decorators';

import { Entity } from './entity';

@tableName('CustomerInspectionEmail')
export class CustomerInspectionEmail extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @IsNotEmpty()
  customerId: string = undefined;

  @sqlField('InspectionType')
  @IsNotEmpty()
  inspectionType: string = undefined;

  @sqlField('Content') content: string = undefined;

  @sqlField('Subject') subject: string = undefined;
}
