import { Entity } from './entity';
import { IsNotEmpty } from 'class-validator';

import { idField, tableName, sqlField } from '../decorators';

@tableName('UsersCustomers')
export class UserCustomer extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('UserId')
  @IsNotEmpty()
  userId: number = undefined;

  @sqlField('CustomerId')
  @IsNotEmpty()
  customerId: string = undefined;

  @sqlField('Role')
  @IsNotEmpty()
  role: string = undefined;
}
