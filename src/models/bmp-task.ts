import { Entity } from './entity';
import { Deletable } from './interfaces';

import { tableName, internalField, sqlField } from '../decorators';

@tableName('BmpTasks')
export class BmpTask extends Entity implements Deletable {
  @sqlField('CustomerId')
  @internalField()
  customerId?: string = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('ControlMeasureId') controlMeasureId: number = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;

  @sqlField('OriginalId')
  @internalField()
  originalId: number = undefined;
}
