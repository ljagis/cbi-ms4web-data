import { Customer } from './customer';
export class CustomerResponse extends Customer {
  logoThumbnailLocation: string = undefined;
}
