import { Deletable, HasCustomFormTemplate, HasCustomer } from './interfaces';
import { IsIn, IsNotEmpty } from 'class-validator';
import { idField, internalField, sqlField, tableName } from '../decorators';

import { Entity } from './entity';
import { EntityTypes } from '../shared';

@tableName('CustomFields')
export class CustomField extends Entity
  implements Deletable, HasCustomer, HasCustomFormTemplate {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  /**
   * State | Customer
   *
   * @type {string}
   */
  @IsIn([null, 'State', 'Customer'])
  @sqlField('CustomFieldType')
  customFieldType: 'State' | 'Customer' = 'Customer';

  /**
   * If `customFieldType` exists, then this is the state abbreviation for that state
   * Example: TX
   *
   * @type {string}
   */
  @sqlField('StateAbbr') stateAbbr?: string = undefined;

  /**
   * See {EntityTypes} for valid values
   *
   * @type {string}
   */
  @sqlField('EntityType')
  @IsNotEmpty()
  @IsIn([
    EntityTypes.BmpDetail,
    EntityTypes.CitizenReport,
    EntityTypes.ConstructionSite,
    EntityTypes.ConstructionSiteInspection,
    EntityTypes.Facility,
    EntityTypes.FacilityInspection,
    EntityTypes.IllicitDischarge,
    EntityTypes.MonitoringLocation,
    EntityTypes.Outfall,
    EntityTypes.OutfallInspection,
    EntityTypes.Structure,
    EntityTypes.StructureInspection,
  ])
  entityType: string = undefined;

  /**
   * Value type.  Valid values are: `number`, `boolean`, `string`, `Date`
   *
   * @type {string}
   */
  @IsIn(['number', 'boolean', 'string', 'Date', 'section'])
  @sqlField('DataType')
  dataType: 'number' | 'boolean' | 'string' | 'Date' | 'section' = undefined;

  /**
   * UI input type.  Valid values are: 'checkbox', 'text', 'textarea', 'date', 'select'
   *
   * @type {string}
   */
  @IsIn([
    'checkbox',
    'text',
    'textarea',
    'date',
    'select',
    'section',
    'multi-select',
    'conditional-text',
  ])
  @sqlField('InputType')
  inputType:
    | 'checkbox'
    | 'text'
    | 'textarea'
    | 'date'
    | 'select'
    | 'section'
    | 'multi-select'
    | 'conditional-text' = undefined;

  @sqlField('FieldLabel')
  @IsNotEmpty()
  fieldLabel: string = undefined;

  @sqlField('FieldOptions') fieldOptions?: string = undefined;

  @sqlField('FieldOrder') fieldOrder: number = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;

  @sqlField('CustomFormTemplateId') customFormTemplateId: number;

  @sqlField('MaxLengthEnabled') maxLengthEnabled: number;

  @sqlField('MaxLength') maxLength: number;

  @sqlField('SectionName') sectionName: string;

  @sqlField('SectionSubtitle') sectionSubtitle: string;

  @sqlField('DefaultValue') defaultValue?: string = undefined;

  @sqlField('IsRequired') isRequired?: boolean = undefined;
}
