import { IsEmail, IsNotEmpty } from 'class-validator';
import { idField, internalField, sqlField, tableName } from '../decorators';

import { Entity } from './entity';

@tableName('SWMP')
export class SWMP extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @IsNotEmpty()
  customerId: string = undefined;

  @sqlField('Year') year: number = undefined;

  @sqlField('Number') number: string = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('OperatorLevel') operatorLevel: number = undefined;

  @sqlField('ContactName') contactName: string = undefined;

  @sqlField('Phone') phone: string = undefined;

  @sqlField('EmailAddress')
  @IsEmail()
  emailAddress: string = undefined;

  @sqlField('MailingAddress') mailingAddress: string = undefined;

  @sqlField('AnnualReportingYear') annualReportingYear: Date | null = undefined;

  @sqlField('Status') status: string = undefined;

  @sqlField('UpdatedDate') updatedDate: Date = undefined;

  @sqlField('PlanLength') planLength: number = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted: boolean = undefined;
}
