export interface InspectionSummaryRecord {
  // from CustomFieldValue
  customFieldId: number;
  value: any;

  // from CustomField
  dataType: 'number' | 'boolean' | 'string' | 'Date' | 'section';
  inputType: 'checkbox' | 'text' | 'textarea' | 'date' | 'select' | 'section';
  fieldOptions: string;
  fieldOrder: number;
  sectionName: string;
  fieldLabel: string;

  // from Inspection
  inspectionId: number;
  inspectionTypeId: number;

  // from InspectionType
  inspectionTypeName: string;

  // from Asset
  assetId: number;
  complianceStatus: string;
}

export interface InspectionSummaryModel {
  id: number;
  complianceStatus: string;
  assetId: number;
  timeIn: string | null;
  timeOut: string | null;

  inspectionTypeId: number;

  // from InspectionType
  inspectionTypeName: string;
}
