export enum SWMPStatus {
  Draft = 'Draft',
  Active = 'Active',
  Previous = 'Previous',
}
