import { Inspection } from '../inspection';
import { User } from '../user';
import { Contact } from '../contact';
import { ComplianceStatus } from '../compliance-status';

export interface AssetReport {
  _inspections?: Inspection[];
  _assetType?: string;
  _name: string;
  [prop: string]: any;
}

export interface AssetSummaryReport {
  assets: AssetReport[];
  users: User[];
  complianceStatus: ComplianceStatus[];
}

export interface ContactReport {
  assets: AssetReport[];
  complianceStatus: any;
  contact: Contact;
  users: User[];
}
