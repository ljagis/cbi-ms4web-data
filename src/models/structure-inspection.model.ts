import { InspectionWithActions } from './inspection-with-actions';

import { tableName, sqlField } from '../decorators';

import { IsPositive } from 'class-validator';

@tableName('StructureInspections')
export class StructureInspection extends InspectionWithActions {
  @sqlField('StructureId')
  @IsPositive()
  structureId?: number = undefined;

  @sqlField('IsControlActive') isControlActive: boolean = undefined;

  @sqlField('IsBuiltWithinSpecification')
  isBuiltWithinSpecification: boolean = undefined;

  @sqlField('IsDepthOfSedimentAcceptable')
  isDepthOfSedimentAcceptable: boolean = undefined;

  @sqlField('RequiresMaintenance') requiresMaintenance: boolean = undefined;

  @sqlField('RequiresRepairs') requiresRepairs: boolean = undefined;

  @sqlField('IsStandingWaterPresent')
  isStandingWaterPresent: boolean = undefined;

  @sqlField('AreWashoutsPresent') areWashoutsPresent: boolean = undefined;

  @sqlField('FloatablesRemovalRequired')
  floatablesRemovalRequired: boolean = undefined;

  @sqlField('IsOutletClogged') isOutletClogged: boolean = undefined;

  @sqlField('IsStructuralDamage') isStructuralDamage: boolean = undefined;

  @sqlField('IsErosionPresent') isErosionPresent: boolean = undefined;

  @sqlField('IsDownstreamErosionPresent')
  isDownstreamErosionPresent: boolean = undefined;

  @sqlField('IsIllicitDischargePresent')
  isIllicitDischargePresent: boolean = undefined;

  @sqlField('IsReturnInspectionRecommended')
  isReturnInspectionRecommended: boolean = undefined;

  @sqlField('Geometry.STY') lat: number = null;

  @sqlField('Geometry.STX') lng: number = null;

  @sqlField('GeometryError') geometryError: number = null;
}
