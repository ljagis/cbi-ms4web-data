import { Entity } from './entity';
import { IsNotEmpty } from 'class-validator';

import { idField, tableName, sqlField, internalField } from '../decorators';

@tableName('SwmpActivity')
export class SwmpActivity extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('GlobalId')
  @internalField()
  globalId?: string = undefined;

  @sqlField('CustomerId')
  @IsNotEmpty()
  customerId: string = undefined;

  @sqlField('SwmpId')
  @IsNotEmpty()
  swmpId: string = undefined;

  @sqlField('MgId')
  @IsNotEmpty()
  mgId: number = undefined;

  @sqlField('Date') date: Date = undefined;

  @sqlField('Quantity') quantity: string = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('DateAdded') dateAdded: Date = undefined;
}
