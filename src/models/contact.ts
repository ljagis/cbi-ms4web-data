import { Deletable } from './interfaces';
import { Entity } from './entity';

import { tableName, internalField, sqlField } from '../decorators';

import { IsNotEmpty } from 'class-validator';

@tableName('Contacts')
export class Contact extends Entity implements Deletable {
  // explicitly set to `undefined` for typescript to generate properties

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('ContactType')
  @IsNotEmpty()
  contactType: string = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('Company') company: string = undefined;

  @sqlField('Phone') phone: string = undefined;

  @sqlField('MobilePhone') mobilePhone: string = undefined;

  @sqlField('Fax') fax: string = undefined;

  @sqlField('Email') email: string = undefined;

  @sqlField('MailingAddressLine1') mailingAddressLine1: string = undefined;

  @sqlField('MailingAddressLine2') mailingAddressLine2: string = undefined;

  @sqlField('MailingCity') mailingCity: string = undefined;

  @sqlField('mailingState') mailingState: string = undefined;

  @sqlField('mailingZip') mailingZip: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted: boolean = undefined;
}
