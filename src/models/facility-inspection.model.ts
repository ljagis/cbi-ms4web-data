import { InspectionWithActions } from './inspection-with-actions';

import { tableName, sqlField } from '../decorators';

import { IsPositive } from 'class-validator';

@tableName('FacilityInspections')
export class FacilityInspection extends InspectionWithActions {
  @sqlField('FacilityId')
  @IsPositive()
  facilityId?: number = undefined;

  @sqlField('IsFacilityActive') isFacilityActive: boolean = undefined;

  @sqlField('IsFacilityPermitted') isFacilityPermitted: boolean = undefined;

  @sqlField('IsSWPPOnSite') isSwppOnSite: boolean = undefined;

  @sqlField('AreRecordsCurrent') areRecordsCurrent: boolean = undefined;

  @sqlField('AreSamplingDataEvaluationAcceptable')
  areSamplingDataEvaluationAcceptable: boolean = undefined;

  @sqlField('IsBMPAcceptable') isBmpAcceptable: boolean = undefined;

  @sqlField('IsGoodHouseKeeping') isGoodHouseKeeping: boolean = undefined;

  @sqlField('IsErosionPresent') isErosionPresent: boolean = undefined;

  @sqlField('AreWashoutsPresent') areWashoutsPresent: boolean = undefined;

  @sqlField('IsDownstreamErosionPresent')
  isDownstreamErosionPresent: boolean = undefined;

  @sqlField('AreFloatablesPresent') areFloatablesPresent: boolean = undefined;

  @sqlField('AreIllicitDischargesPresent')
  areIllicitDischargesPresent: boolean = undefined;

  @sqlField('AreNonStormWaterDischargePresent')
  areNonStormWaterDischargePresent: boolean = undefined;

  @sqlField('IsReturnInspectionRequired')
  isReturnInspectionRequired: boolean = undefined;

  @sqlField('Geometry.STY') lat: number = null;

  @sqlField('Geometry.STX') lng: number = null;

  @sqlField('GeometryError') geometryError: number = null;
}
