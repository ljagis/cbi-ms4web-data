import { Entity } from './entity';
import { IsNotEmpty, IsIn } from 'class-validator';

import { tableName, internalField, sqlField, idField } from '../decorators';
import { EntityTypes, InspectionType } from '../shared';
import { CustomField } from './custom-field';

@tableName('CustomFormTemplates')
export class CustomFormTemplate extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  /**
   * See {EntityTypes} for valid values
   *
   * @type {string}
   */
  @sqlField('EntityType')
  @IsNotEmpty()
  @IsIn([
    EntityTypes.CitizenReport,
    EntityTypes.ConstructionSiteInspection,
    EntityTypes.FacilityInspection,
    EntityTypes.IllicitDischarge,
    EntityTypes.OutfallInspection,
    EntityTypes.StructureInspection,
  ])
  entityType: InspectionType = undefined;

  @sqlField('Name')
  @IsNotEmpty()
  name: string = undefined;

  /** Determines if template is a system field and is not deletable */
  @sqlField('IsSystem') isSystem: boolean = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted: boolean = undefined;

  @sqlField('IsArchived')
  @internalField()
  isArchived: boolean = undefined;
}

export class CustomFormTemplateWithCustomFields extends CustomFormTemplate {
  customFields?: Partial<CustomField>[];
}
