import { Entity } from './entity';
import { IsNotEmpty } from 'class-validator';

import { idField, tableName, sqlField } from '../decorators';

interface AssignedBmp {
  id: number;
  title: string;
  minimumControlMeasures: string[];
}

@tableName('MG')
export class MG extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('BmpId')
  @IsNotEmpty()
  bmpId: number = undefined;

  @sqlField('Number') number: string = undefined;

  @sqlField('Title') title: string = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('IsQuantityRequired') isQuantityRequired: boolean = undefined;

  @sqlField('YearsImplemented') yearsImplemented: string | number[] = undefined;

  @sqlField('InformationUsed') informationUsed: string = undefined;

  @sqlField('Units') units: string = undefined;

  @sqlField('AnnualGoal') annualGoal: string = undefined;

  bmp: AssignedBmp;
}
