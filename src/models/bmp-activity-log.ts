import { Entity } from './entity';
import { Deletable } from './interfaces';

import { tableName, internalField, sqlField, idField } from '../decorators';

@tableName('BmpActivityLogs')
export class BmpActivityLog extends Entity implements Deletable {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('BmpDetailId') bmpDetailId: number = undefined;

  @sqlField('DateAdded') dateAdded: Date = undefined;

  @sqlField('ActivityDate') activityDate: Date = undefined;

  @sqlField('DataType') dataType: string = undefined;

  @sqlField('Quantity') quantity: number = undefined;

  @sqlField('Comments') comments: string = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;
}
