import { Entity } from './entity';

import { tableName, idField, sqlField } from '../decorators';

@tableName('ComplianceStatuses')
export class ComplianceStatus extends Entity {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('Level') level: number = undefined;

  @sqlField('EntityType') entityType: string = undefined;

  @sqlField('CustomerIds') customerIds: string = undefined;

  @sqlField('ExcludeCustomerIds') excludeCustomerIds: string = undefined;
}
