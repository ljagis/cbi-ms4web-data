import { Entity } from './entity';
import { Deletable } from './interfaces';

import { tableName, internalField, sqlField, idField } from '../decorators';

@tableName('BmpDetails')
export class BmpDetail extends Entity implements Deletable {
  @idField()
  @sqlField('Id')
  id?: number = undefined;

  @sqlField('GlobalId')
  @internalField()
  globalId?: string = undefined;

  @sqlField('ControlMeasureId') controlMeasureId: number = undefined;

  @sqlField('Name') name: string = undefined;

  @sqlField('Description') description: string = undefined;

  @sqlField('DateAdded') dateAdded: Date = undefined;

  @sqlField('DueDate') dueDate: Date = undefined;

  @sqlField('CompletionDate') completionDate: Date = undefined;

  @sqlField('IsDeleted')
  @internalField()
  isDeleted?: boolean = undefined;
}
