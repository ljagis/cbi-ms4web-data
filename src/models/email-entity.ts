import { Entity } from './entity';
import {
  tableName,
  idField,
  globalIdField,
  internalField,
  sqlField,
} from '../decorators';

@tableName('Emails')
export class EmailEntity extends Entity {
  @idField()
  @sqlField('Id')
  id: number = undefined;

  @globalIdField()
  @internalField()
  @sqlField('GlobalId')
  globalId: string = undefined;

  @internalField()
  @sqlField('GlobalIdFK')
  globalIdFk: string = undefined;

  @sqlField('CustomerId')
  @internalField()
  customerId: string = undefined;

  @sqlField('EntityType') entityType: string = undefined;

  @sqlField('To') emailTo: string[] = undefined;

  @sqlField('CC') emailCC: string[] = undefined;

  @sqlField('AddedDate') addedDate: Date = undefined;

  @sqlField('AddedBy') addedBy: string = undefined;

  @sqlField('Subject') subject: string = undefined;

  @sqlField('Body') body: string = undefined;
}
