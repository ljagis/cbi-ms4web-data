import { injectable } from 'inversify';
import { interfaces } from 'inversify-express-utils';
import { Request, Response, NextFunction } from 'express';
import { Principal, UserFull, Customer } from '../models';
import { inject } from 'inversify';
import {
  UserFullRepository,
  CustomerRepository,
  UserRepository,
} from '../data';
import * as Bluebird from 'bluebird';
import { CURRENT_CUSTOMER_FIELD } from '../shared/constants';

@injectable()
export class CustomAuthProvider implements interfaces.AuthProvider {
  @inject(UserRepository) private _userRepo: UserRepository;
  @inject(UserFullRepository) private _userFullRepo: UserFullRepository;
  @inject(CustomerRepository) private _customerRepo: CustomerRepository;

  async getUser(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<Principal> {
    let user: UserFull;
    let customer: Customer;
    if (req.user) {
      customer = await this._getCustomer(req);
      user = await this._userFullRepo.fetchByEmailAndCustomer(
        req.user.email,
        customer.id
      );
    }

    const principal = new Principal(user);
    principal.selectedCustomer = customer;

    // sets the req.principal for others to use
    req.principal = principal;
    return await principal;
  }

  // attempts to get selected Customer instance
  private async _getCustomer(req: Request): Bluebird<Customer> {
    if (!req.user) {
      return null;
    }
    const customerId = req.user.customData[CURRENT_CUSTOMER_FIELD];
    if (customerId) {
      // attempt to get customer
      const customer = await this._customerRepo.fetchById(customerId);
      if (customer) {
        return customer;
      }
    }
    const dbUser = await this._userRepo.fetchByEmail(req.user.email);
    if (!dbUser) {
      throw Error(`User ${req.user.email} not found`);
    }

    // okta customerId does not match, then get the first customer
    const customers = await this._customerRepo.fetchByUserId(dbUser.id);
    if (customers && customers.length) {
      const saveUser = Bluebird.promisify(req.user.save, { context: req.user });
      req.user.customData[CURRENT_CUSTOMER_FIELD] = customers[0].id;
      await saveUser();
      return customers[0];
    }
    throw Error(`Customer not found for user ${req.user.email}.  Login?`);
  }
}
