import * as moment from 'moment';
import { startCase, snakeCase } from 'lodash';
import { injectable } from 'inversify';

import { Weather } from '../models';
import axios, { AxiosInstance } from 'axios';
import { Env } from '../shared';
import {
  WeatherKitCurrentResponse,
  WeatherKitForecastDay,
  WeatherKitForecastResponse,
} from '../models/weather-kit';

const FORECAST_URL = 'https://weatherkit.apple.com/api/v1';

const ONE_MM_PER_INCH = 0.0393701;

@injectable()
export class WeatherKitService {
  env = process.env as Env;
  httpClient: AxiosInstance;

  constructor() {
    this.httpClient = axios.create({
      baseURL: `${FORECAST_URL}`,
      headers: {
        Authorization: `Bearer ${this.env.WEATHER_KIT_TOKEN}`,
      },
    });
  }

  /**
   * Gets simple weather data (what the app currently uses)
   * @param lat
   * @param lng
   */
  async forecastSimple(lat: number, lng: number) {
    const resp = await this.forecast<WeatherKitCurrentResponse>(
      lat,
      lng,
      'dataSets=currentWeather'
    );
    const last72 = await this.rainfall(lat, lng, resp.currentWeather.asOf, 72);
    const last24 = await this.rainfall(lat, lng, resp.currentWeather.asOf, 24);

    return <Weather>{
      time: moment(resp.currentWeather.asOf).toDate(),
      condition: startCase(resp.currentWeather.conditionCode),
      temperatureF: formatTemperature(resp.currentWeather.temperature),
      preciptiationIn: formatPrecipitationAmount(
        resp.currentWeather.precipitationIntensity
      ),
      windDirection: resp.currentWeather.windDirection,
      windSpeedMph: resp.currentWeather.windSpeed,
      icon: snakeCase(resp.currentWeather.conditionCode).toUpperCase(),
      last72,
      last24,
    };
  }

  async forecast<T>(lat: number, lng: number, query?: string): Promise<T> {
    const { data } = await this.httpClient.get(
      `/weather/en_US/${lat}/${lng}?${query}`
    );
    return data;
  }

  async rainfall(lat: number, lng: number, currentTime: string, hours: number) {
    const resp = await this.forecast<WeatherKitForecastResponse>(
      lat,
      lng,
      `dataSets=forecastDaily&dailyStart=${moment(currentTime)
        .subtract(hours, 'hours')
        .toISOString()}`
    );

    const days = resp.forecastDaily.days.reduce<WeatherKitForecastDay[]>(
      (acc, day) => {
        if (
          moment(currentTime).isAfter(moment(day.forecastStart)) &&
          ((hours === 24 && acc.length < 1) || (hours === 72 && acc.length < 3))
        ) {
          acc.push(day);
        }

        return acc;
      },
      []
    );

    const totalPrecipitationAmount = days.reduce(
      (acc, day) => (acc += day.precipitationAmount),
      0
    );

    return formatPrecipitationAmount(totalPrecipitationAmount);
  }
}

/** formats celsius to fahrenheit temperature from float to int */
function formatTemperature(temp: number) {
  return Math.round(temp * 9 / 5 + 32);
}

function formatPrecipitationAmount(amount: number) {
  return Math.floor(amount * ONE_MM_PER_INCH * 100) / 100;
}
