import * as moment from 'moment';

import { inject, injectable } from 'inversify';

import { DsForecastResponse } from '../models/dark-sky';
import { TYPES } from '../di/types';
import { Weather } from '../models';
import axios, { AxiosInstance } from 'axios';

const FORECAST_URL = 'https://api.darksky.net/forecast';

@injectable()
export class DarkSkyService {
  httpClient: AxiosInstance;

  constructor(@inject(TYPES.DarkSkyApiKey) public apiKey) {
    this.httpClient = axios.create({
      baseURL: `${FORECAST_URL}/${apiKey}`,
    });
  }

  /**
   * Gets simple weather data (what the app currently uses)
   * @param lat
   * @param lng
   */
  async forecastSimple(lat: number, lng: number) {
    const resp = await this.forecast(lat, lng);
    const last72 = await this.rainfall(lat, lng, resp.currently.time, 72);
    const last24 = await this.rainfall(lat, lng, resp.currently.time, 24);
    return <Weather>{
      time: moment.unix(resp.currently.time).toDate(),
      condition: formatIconToCondition(resp.currently.icon),
      temperatureF: formatTemperature(resp.currently.temperature),
      preciptiationIn: resp.currently.precipIntensity,
      windDirection: resp.currently.windBearing,
      windSpeedMph: resp.currently.windSpeed,
      icon: resp.currently.icon,
      last72,
      last24,
    };
  }

  async forecast(lat: number, lng: number): Promise<DsForecastResponse> {
    const { data } = await this.httpClient.get(`/${lat},${lng}`);
    return data;
  }

  /**
   * Dark Sky API returns hourly data for a single 24 hour day starting at 5:00am.
   * We need to make a separate request for each day we are interested in
   * Ex. For a 72 hour period ending at 12:00pm, we need to fetch the past 4 days of data
   */
  async rainfall(lat: number, lng: number, currentTime: number, hours: number) {
    const days = Math.ceil(hours / 24) + 1;

    let hourlyData = [];
    for (let i = 0; i < days; i++) {
      const secondsPerDay = 86400;
      const time = currentTime - i * secondsPerDay;

      const { data } = await this.httpClient.get(`/${lat},${lng},${time}`);

      hourlyData = [...hourlyData, ...data.hourly.data];
    }

    const secondsPerHour = 3600;
    const startTime = currentTime - hours * secondsPerHour;

    const totalRainfall = hourlyData.reduce(
      (total, { time, precipIntensity }) => {
        if (time < startTime || time > currentTime) {
          return total;
        }

        return total + precipIntensity;
      },
      0
    );

    return Math.floor(totalRainfall * 1000) / 1000;
  }
}

/** formats clear-day to Clear Day */
function formatIconToCondition(icon: string) {
  return icon
    .split('-')
    .map(word => word.charAt(0).toUpperCase() + word.substr(1).toLowerCase())
    .join(' ');
}

/** formats temperature from float to int */
function formatTemperature(temp: number) {
  return Math.round(temp);
}
