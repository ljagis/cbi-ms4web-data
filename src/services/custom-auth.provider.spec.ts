import { container } from '../inversify.config';
import { CustomAuthProvider } from './custom-auth.provider';
import { expect } from 'chai';
import * as httpMocks from 'node-mocks-http';
import * as sinon from 'sinon';
import { CURRENT_CUSTOMER_FIELD } from '../shared/constants';

describe(`CustomAuthProvider tests`, async () => {
  const authProvider = container.get<CustomAuthProvider>(CustomAuthProvider);
  const email = 'anguyen@lja.com';
  const customerId = 'LEWISVILLE';
  const responseMock = httpMocks.createResponse();

  it(`should return principal`, async () => {
    const saveFake = sinon.stub();
    const requestMock = httpMocks.createRequest({
      user: {
        email: email,
        customData: {
          [CURRENT_CUSTOMER_FIELD]: customerId,
        },
        save: saveFake,
      },
    });
    const principal = await authProvider.getUser(
      requestMock,
      responseMock,
      null
    );

    expect(principal).ok;
    expect(principal.details.email).eq(email);
    expect(principal.selectedCustomer).ok;
    expect(principal.selectedCustomer.id).eq(customerId);
    expect(saveFake.called).false;
  });

  it(`should save customData if customerId doesn't exists`, async () => {
    const fakeSave = sinon.stub().yields();
    const user = {
      email: email,
      customData: {
        [CURRENT_CUSTOMER_FIELD]: 'NonExistingCustomer',
      },
      save: sinon.stub().yields(),
    };

    const requestMock = httpMocks.createRequest({
      user: user,
    });
    const principal = await authProvider.getUser(
      requestMock,
      responseMock,
      null
    );

    expect(principal).ok;
    expect(principal.details.email).eq(email);
    expect(fakeSave.called);
  });
});
