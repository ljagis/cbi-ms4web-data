import * as sinon from 'sinon';

import { DarkSkyService } from './dark-sky.service';
import { DsCurrently } from '../models/dark-sky';
import axios from 'axios';
import { expect } from 'chai';

describe('Dark Sky Service tests', async () => {
  const apiKey = 'thekey';

  describe(`Api keys`, async () => {
    it('should set the key', () => {
      const svc = new DarkSkyService(apiKey);

      expect(svc.apiKey).eq(apiKey);
    });
  });

  describe(`forecast test`, async () => {
    let getStub: sinon.SinonStub;
    const forecastResp = {
      data: {
        test: 1,
      },
    };
    beforeEach(() => {
      getStub = sinon.stub(axios, 'get').resolves(forecastResp);
    });
    afterEach(() => {
      getStub.restore();
    });

    it('should call forecast', async () => {
      const svc = new DarkSkyService(apiKey);
      const expectedArg = 'https://api.darksky.net/forecast/thekey/1,1';
      await svc.forecast(1, 1);
      sinon.assert.calledWith(getStub, expectedArg);
    });

    it('should return data', async () => {
      const svc = new DarkSkyService(apiKey);
      const res = await svc.forecast(1, 1);
      expect(res).eq(forecastResp.data);
    });
  });

  describe(`forecast simple test`, async () => {
    let getStub: sinon.SinonStub;
    const forecastResp = {
      data: {
        currently: <Partial<DsCurrently>>{
          time: 1543875380,
          summary: 'Clear',
          temperature: 65.32,
          precipIntensity: 0.1,
          windBearing: 90,
          windSpeed: 12,
          icon: 'clear-day',
        },
      },
    };
    beforeEach(() => {
      getStub = sinon.stub(axios, 'get').resolves(forecastResp);
    });
    afterEach(() => {
      getStub.restore();
    });

    it('should return simple data', async () => {
      const svc = new DarkSkyService(apiKey);
      const weather = await svc.forecastSimple(1, 1);
      expect(weather.time.toISOString()).eq(`2018-12-03T22:16:20.000Z`);
      expect(weather.condition).eq('Clear Day');
      expect(weather.temperatureF).eq(65.32);
      expect(weather.preciptiationIn).eq(0.1);
      expect(weather.windDirection).eq(90);
      expect(weather.windSpeedMph).eq(12);
      expect(weather.icon).eq('clear-day');
    });
  });
});
