// helper file to import required modules for test

import 'tslib';
import 'core-js/shim';
import 'reflect-metadata';
import 'mocha';
import * as chai from 'chai';
const chaiHttp = require('chai-http');
chai.should();
chai.use(chaiHttp);

require('dotenv').config({
  path: '.env.yml',
});

// Note: cookie is based on the "test/test" user.  Test user should use the LEWISVILLE demo instance.
export const config = {
  baseUrl: 'http://localhost:9000',
  customerId: 'LEWISVILLE',
  // tslint:disable-next-line
  cookie: process.env.TEST_COOKIE, // put refresh_token cookie in .env.yml file
};
