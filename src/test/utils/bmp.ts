import { container } from '../../inversify.config';
import { BmpDetailRepository } from '../../data/bmp-detail.repository';
import { BmpControlMeasureRepository } from '../../data/bmp-control-measure.repository';
import { config } from '../helper';
import * as Knex from 'knex';
import { BmpControlMeasure } from '../../models';

const mcmRepo = container.get(BmpControlMeasureRepository);
const bmpDetailRepo = container.get(BmpDetailRepository);
const knex = container.get(Knex) as Knex;

export async function createTestMCM() {
  const mcm = await mcmRepo.insert(
    {
      name: 'Test MCM ' + new Date().getTime(),
      sort: 987,
    },
    config.customerId
  );
  return mcm;
}

export async function deleteMcm(mcmId: number) {
  await knex(BmpControlMeasure.getTableName())
    .del()
    .where(BmpControlMeasure.sqlField(f => f.id), mcmId);
}

export async function createTempBmpDetail(mcmId: number) {
  const time = new Date().getTime();
  const bmpDetail = await bmpDetailRepo.insert(config.customerId, {
    controlMeasureId: mcmId,
    name: 'Test BMPDetail ' + time,
    description: 'Description ' + time,
  });

  return bmpDetail;
}

export async function deleteBmpDetail(bmpId: number) {
  return await bmpDetailRepo.delete(config.customerId, bmpId, { force: true });
}
