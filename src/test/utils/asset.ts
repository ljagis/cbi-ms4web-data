import * as models from '../../models';

export function createConstructionSite() {
  const cs: Partial<models.ConstructionSite> = {
    trackingId: 'TEST 1',
    name: 'Test Construction Site',
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    projectId: null,
    projectType: models.projectTypes.commercial,
    permitStatus: models.permitStatus.approved,
    projectArea: 2.3,
    disturbedArea: 3.3,
    startDate: new Date(2016, 11, 1),
    estimatedCompletionDate: new Date(2017, 11, 1),
    latestInspectionDate: new Date(2016, 11, 1),
    completionDate: new Date(2017, 11, 2),
    projectStatus: models.projectStatus.active,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    npdes: true,
    usace404: false,
    state401: true,
    usacenwp: false,
    additionalInformation: 'Additional info...',
    complianceStatus: 'Compliant',
    lat: 33.003305,
    lng: -96.983596,
  };
  return cs;
}

export function createCitizenReport() {
  const cr: Partial<models.CitizenReport> = {
    citizenName: 'Alex Nguyen',
    dateReported: new Date(2016, 11, 1),
    complianceStatus: 'Active',
    lat: 33.056299,
    lng: -96.99825,
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    phoneNumber: '111-111-1111',
    cellNumber: '222-222-2222',
    faxNumber: '333-333-3333',
    report: 'Some sample report',
    responseDate: new Date(2016, 12, 1),
    response: 'Sample response',
    investigatorId: null,
    investigator2Id: null,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    additionalInformation: 'additional info',
  };
  return cr;
}

export function createFacility() {
  const fac: Partial<models.Facility> = {
    dateAdded: new Date(2016, 11, 1),
    name: 'City Hall',
    physicalAddress1: '123 Test',
    physicalAddress2: 'SUITE 1',
    physicalCity: 'Houston',
    physicalState: 'TX',
    physicalZip: '77042',
    complianceStatus: 'Compliant',
    lat: 33.047787,
    lng: -96.995385,
    additionalInformation: 'extra info',
    sector: 'City Facility',
    sicCode: 'SIC code',
    isHighRisk: true,
    inspectionFrequencyId: null,
    yearBuilt: 1999,
    squareFootage: 1.1,
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 3),
  };
  return fac;
}

export function createOutfall() {
  let outfall: Partial<models.Outfall> = {
    dateAdded: new Date(2016, 11, 1),
    location: 'Raldon LK Park',
    nearAddress: 'Near Raldon LK Park',
    trackingId: '555',
    complianceStatus: 'Not Screened',
    lat: 33.032482,
    lng: -97.003106,
    condition: models.outfallConditions[0],
    material: 'Concrete',
    obstruction: models.obstructionSeverities[0],
    outfallType: 'Pipe',
    outfallSizeIn: 1.1,
    outfallWidthIn: 2.2,
    outfallHeightIn: 3.3,
    additionalInformation: 'add info',
    communityId: null,
    watershedId: null,
    subwatershedId: null,
    receivingWatersId: null,
    latestInspectionDate: new Date(2016, 11, 4),
  };
  return outfall;
}
