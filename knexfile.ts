import 'tslib';
import 'reflect-metadata';
import 'core-js/shim';

// Update with your config settings.
require('dotenv').config({
  path: '.env.yml',
});

const pool = { max: 1 };
const migrations = {
  tableName: 'knex_migrations',
};
const connection = {
  host: process.env.DB_SERVER,
  database: process.env.DB_DATABASE,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
};

module.exports = {
  development: {
    client: 'mssql',
    debug: true,
    pool: pool,
    connection: connection,
    migrations: migrations,
  },

  staging: {
    client: 'mssql',
    connection: connection,
    pool: pool,
    migrations: migrations,
  },

  production: {
    client: 'mssql',
    connection: connection,
    pool: pool,
    migrations: migrations,
  },
};
