import { Facility } from '../../src/models';

export interface FacilityTemplate extends Facility {
  communityName: string;
  watershedName: string;
  subwatershedName: string;
  receivingWatersName: string;

  isHighRisk: any;

  // [cf: string]: string | any;
  facilityType: string;
  acres: string;

  contactName: string;
  contactTitle: string;
  contactEmail: string;
  contactPhone: string;
  contactType: string;
}
