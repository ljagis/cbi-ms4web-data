import { tableName } from '../../src/decorators';

@tableName('BestManagementPractices')
export class MCMOrig {
  NumericID: number = undefined;
  ControlMeasureID: string;
  Title: string;
  Description: string;
  ImplementationDate: string;
  Department: string;
  EstimatedCostPerCapita: string;
  Implemented: string;
  BMPSelected: string;
  Rationale: string;
  CollectionFrequency: string;
  Sequence: string;
  Count: string;
}
