import { tableName } from '../../src/decorators';

@tableName('List_Communities')
export class CommunityOrig {
  NumericID: number;
  CommunityName: string;
}

@tableName('List_ReceivingStreams')
export class ReceivingStreamOrig {
  NumericID: number;
  ReceivingStreamName: string;
}

@tableName('List_Watersheds')
export class WatershedOrig {
  NumericID: number;
  WatershedName: string;
}

@tableName('List_SubWatersheds')
export class SubWatershedOrig {
  NumericID: number;
  SubWatershedName: string;
}

@tableName('List_OwnerTypes')
export class OwnerTypeOrig {
  NumericID: number;
  OwnerTypeName: string;
}

@tableName('List_ProjectTypes')
export class ProjecTypeOrig {
  NumericID: number;
  ProjectTypeName: string;
}

@tableName('List_PermitStatuses')
export class PermitStatusOrig {
  NumericID: number;
  PermitStatusName: string;
}

@tableName('List_Sectors')
export class SectorOrig {
  NumericID: number;
  SectorName: string;
}

@tableName('List_DistanceUnits')
export class DistanceUnitsOrig {
  NumericID: number;
  DistanceUnitName: string;
}

@tableName('List_OutfallTypes')
export class OutfallTypeOrig {
  NumericID: number;
  OutfallTypeName: string;
}

@tableName('List_OutfallMaterials')
export class OutfallMaterialOrig {
  NumericID: number;
  OutfallMaterialName: string;
}

@tableName('List_Conditions')
export class ConditionOrig {
  NumericID: number;
  ConditionName: string;
}

@tableName('List_ObstructionSeverities')
export class ObstructionSeverityOrig {
  NumericID: number;
  ObstructionSeverityName: string;
}

@tableName('List_ControlTypes')
export class ControlTypeOrig {
  NumericID: number;
  ControlTypeName: string;
}

@tableName('List_InspectionFrequencies')
export class InspectionFrequencyOrig {
  NumericID: number;
  InspectionFrequencyName: string;
}

@tableName('List_InspectionTypes')
export class InspectionTypeOrig {
  NumericID: number;
  InspectionTypeName: string;
}

@tableName('Inspection_Color')
export class InspectionColorOrig {
  NumericID: number;
  Color: string;
}

@tableName('Inspection_Clarity')
export class InspectionClarityOrig {
  NumericID: number;
  Clarity: string;
}

@tableName('Inspection_Odor')
export class InspectionOdorOrig {
  NumericID: number;
  Odor: string;
}

@tableName('Inspection_Foam')
export class InspectionFoamOrig {
  NumericID: number;
  Foam: string;
}

@tableName('Inspection_Sheen')
export class InspectionSheenOrig {
  NumericID: number;
  Sheen: string;
}

@tableName('Inspection_SusSolid')
export class InspectionSusSolidOrig {
  NumericID: number;
  SusSolid: string;
}

@tableName('Inspection_FloatSolid')
export class InspectionFloatSolidOrig {
  NumericID: number;
  FloatSolid: string;
}

@tableName('Inspection_SetSolid')
export class InspectionSetSolidOrig {
  NumericID: number;
  SetSolid: string;
}

@tableName('CustomFieldSetup')
export class CustomFieldSetupOrig {
  NumericID: number;
  SiteSection: string;
  FieldNumber: number;
  FieldLabel: string;
  FieldType: string;
  DefaultValue: string;
  OrderId: number;
  IsVisible: string;
  IsDeleted: string;
  IsFilter: boolean;
}

@tableName('QuestionsSetup')
export class QuestionsSetupOrig {
  NumericID: number;
  SiteSection: string;
  Question: string;
  ControlType: string;
  Boolean: boolean;
  Text: string;
  Memo: string;
  DefaultAnswer: string;
  SubSection: string;
}

@tableName('Questions')
export class QuestionOrig {
  NumericID: number;
  RecordID: number;
  SiteSection: string;
  QuestionAnswer: string;
  ControlType: string;
  Boolean: string;
  Question: string;
  InspectionParentID: number;
}

@tableName('BMPFiles')
export class BmpFileOrig {
  NumericID: number;
  OwnerID: number;
  IsPhoto: boolean;
  OwnerType: number;
  Description: string;
  DateEntered: Date;
  Created: Date;
  FilePath: string;
  IsPublic: boolean;
  IsReport: boolean;
  PublicPath: string;
}

@tableName('List_FileOwnerTypes')
export class FileOwnerTypeOrig {
  NumericID: number;
  FileOwnerTypeName: string;
  FileRepository: string;
  PhotoRepository: string;
}

@tableName('List_Inspectors')
export class InspectorOrig {
  NumericID: number;
  InspectorName: string;
}
