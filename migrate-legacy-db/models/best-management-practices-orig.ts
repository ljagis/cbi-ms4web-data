import { tableName } from '../../src/decorators';

@tableName('BestManagementPractices')
export class BestManagementPracticeOrig {
  NumericID: number = undefined;
  ControlMeasureID: number;
  Title: string;
  Description: string;
  ImplementationDate: Date;
  Department: number;
  EstimatedCostPerCapita: string;
  Implemented: number;
  BMPSelected: number;
  Rationale: string;
  CollectionFrequency: number;
  Sequence: string;
  Count: number;
}
