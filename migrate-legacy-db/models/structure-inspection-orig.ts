import { tableName } from '../../src/decorators';

@tableName('StructureInspections')
export class StructureInspectionOrig {
  NumericID: number = undefined;
  Structure: number;
  SiteName: string;
  Address: string;
  City: string;
  State: string;
  PostalCode: string;
  Created: Date;
  InspectionDate: Date;
  Inspector: string;
  NPDES: string;
  USACE404: string;
  State401: string;
  USACENWP: string;
  EnforcedActions: string;
  DateResolved: Date;
  CorrectiveActions: string;
  ControlIsActive: boolean;
  BuiltWithinSpecification: boolean;
  DepthOfSediment: boolean;
  RequiresMaintenance: boolean;
  RequiresRepairs: boolean;
  StandingWater: boolean;
  WashoutsArePresent: boolean;
  FloatablesRemovalRequired: boolean;
  OutletIsClogged: boolean;
  StructuralDamage: boolean;
  ErosionIsPresent: boolean;
  DownstreamErosion: boolean;
  IllicitDischargePresent: boolean;
  RecommendedReturnInspection: boolean;
  Comments: string;
  Conversation: string;
}
