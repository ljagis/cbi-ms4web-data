import { tableName } from '../../src/decorators';

@tableName('IndustrialFacilityInspections')
export class FacilityInspectionOrig {
  NumericID: number = undefined;
  IndustrialFacility: number;
  SiteName: string;
  Address: string;
  City: string;
  State: string;
  PostalCode: string;
  Created: Date;
  InspectionDate: Date;
  Inspector: string;
  FacilityIsActive: boolean;
  FacilityIsPermitted: boolean;
  SWPPOnSite: boolean;
  RecordsCurrent: boolean;
  SamplingDataEvaluation: boolean;
  BestManagementPractices: boolean;
  GoodHouseKeeping: boolean;
  ErosionIsPresent: boolean;
  WashoutsArePresent: boolean;
  DownstreamErosion: boolean;
  FloatablesRemovalRequired: boolean;
  IllicitDischargePresent: boolean;
  NonStormWaterDischarges: boolean;
  RecommendedReturnInspection: boolean;
  EnforcedActions: string;
  DateResolved: Date;
  CorrectiveActions: string;
  Comments: string;
  InspectionType: string;
}
