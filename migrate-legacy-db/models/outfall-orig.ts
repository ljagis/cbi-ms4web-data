import { tableName } from '../../src/decorators';

@tableName('Outfalls')
export class OutfallOrig {
  NumericID: number = undefined;
  DateEntered: Date;
  Observer: number;
  OutfallSize: string;
  OutfallType: number;
  OutfallMaterial: number;
  TrackingID: string;
  Community: number;
  Watershed: number;
  SubWatershed: number;
  ReceivingStream: number;
  OutfallLocation: string;
  NearAddress: string;
  OutfallDescription: string;
  Comment: string;
  PhotoUrl: string;
  Latitude: string;
  Longitude: string;
  GISLatitude: number;
  GISLongitude: number;
  X: number;
  Y: number;
  SizeUnits: number;
  HeightUnits: number;
  WidthUnits: number;
  Condition: number;
  Obstruction: number;
  DryWeather: number;
  WetWeather: number;
  Discharging: number;
  Illicit: number;
  Acceptable: number;
  OutfallWidth: string;
  OutfallHeight: string;
  Area: number;
  CF1: string;
  CF2: string;
  CF3: string;
  CF4: string;
  CF5: string;
  CF6: string;
  CF7: string;
  CF8: string;
  CF9: string;
  CF10: string;
  CF11: string;
  CF12: string;
  CF13: string;
  CF14: string;
  CF15: string;
  CF16: string;
  CF17: string;
  CF18: string;
  CF19: string;
  CF20: string;
  CF21: string;
  CF22: string;
  CF23: string;
  CF24: string;
  CF25: string;
  CF26: string;
  CF27: string;
  CF28: string;
  CF29: string;
  CF30: string;
}
