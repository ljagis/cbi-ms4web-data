import * as Knex from 'knex';
import { WorkBook } from 'xlsx/types';
import * as Bluebird from 'bluebird';

export interface FileImportable {
  import(workbook: WorkBook, trx: Knex): Bluebird<any>;
}

export interface FileImportValidate {
  validateFile(workbook: WorkBook, trx: Knex): Bluebird<boolean>;
}
