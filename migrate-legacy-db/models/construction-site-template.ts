import { ConstructionSite } from '../../src/models';

export interface ConstructionSiteTemplate extends ConstructionSite {
  projectName: string;
  communityName: string;
  watershedName: string;
  subwatershedName: string;
  receivingWatersName: string;

  npdes: any;
  usace404: any;
  state401: any;
  usacenwp: any;

  // [cf: string]: string | any;

  ownerContact: string;
  ownerEmail: string;
  ownerPhone: string;
  operatorName: string;
  operatorTitle: string;
  operatorEmail: string;
  operatorPhone: string;
  operatorMobilePhone: string;
  operatorCompany: string;
  operatorContactType: string;
  operatorAddressLine1: string;
  operatorAddressLine2: string;
  operatorMailingCity: string;
  operatorMailingState: string;
  operatorMailingZip: string;
  secondaryOpName: string;
  secondaryOpTitle: string;
  secondaryOpPhone: string;
  secondaryOpMobilePhone: string;
  secondaryOpEmail: string;
}
