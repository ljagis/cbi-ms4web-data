import { tableName } from '../../src/decorators';

@tableName('ConstructionSiteInspections')
export class ConstructionSiteInspectionOrig {
  NumericID: number = undefined;
  ConstructionSite: number;
  Created: Date;
  InspectionDate: Date;
  Inspector: string;
  SiteIsActive: boolean;
  SitePlanOnSite: boolean;
  ErosionControls: boolean;
  StructuralControls: boolean;
  WasteManagement: boolean;
  MaintenanceOfControls: boolean;
  LocalControls: boolean;
  SiteIsPermitted: boolean;
  PlanRecordsCurrent: boolean;
  StabilizationControls: boolean;
  TrackingControls: boolean;
  OutfallVelocityControls: boolean;
  NonStormWaterControls: boolean;
  RecommendedReturnInspection: boolean;
  ReturnInspectionDate: Date;
  Comments: string;
  Conversation: string;
  EnforcedActions: string;
  DateResolved: Date;
  CorrectiveActions: string;
  InspectionType: string;
  EnforcementType: string;
}
