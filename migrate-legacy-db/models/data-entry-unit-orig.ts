import { tableName } from '../../src/decorators';

@tableName('List_DataEntryUnits')
export class DataEntryUnitOrig {
  NumericID: number = undefined;
  DataEntryUnitName: string = undefined;
}
