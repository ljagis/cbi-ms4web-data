export interface Config {
  LOG_LEVEL: string;
  LOG_PATH: string;

  ORIGINAL_DB_HOST: string;
  ORIGINAL_DB_NAME: string;
  ORIGINAL_DB_USER: string;
  ORIGINAL_DB_PASSWORD: string;

  DB_HOST: string;
  DB_NAME: string;
  DB_USER: string;
  DB_PASSWORD: string;

  CUSTOMER_ID: string;
  CUSTOMER_NAME: string;
  CUSTOMER_STATE: string;

  FILE_STORAGE_PATH: string;
  ORIGINAL_FILES_BASE_PATH: string;
}
