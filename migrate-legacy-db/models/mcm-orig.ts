import { tableName } from '../../src/decorators';

@tableName('MinimumControlMeasures')
export class MinimumControlMeasureOrig {
  NumericID: number = undefined;
  ControlMeasure: string;
  MeasureID: number;
  MeasureDescription: string;
  CostPerCapita: number;
}
