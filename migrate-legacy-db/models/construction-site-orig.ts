import { tableName } from '../../src/decorators';

@tableName('ConstructionSites')
export class ConstructionSiteOrig {
  NumericID: number = undefined;
  Created: Date = undefined;
  TrackingID: string = undefined;
  SiteName: string = undefined;
  ProjectName: string = undefined;
  OwnerType: number = undefined;
  ProjectType: number = undefined;
  SiteAddress: string = undefined;
  PermitStatus: number = undefined;

  Area: number;
  TotalArea: number;
  DistributedArea: number;
  StartDate: Date;
  EndDate: Date;
  Active: boolean;
  Completed: boolean;
  Approved: boolean;
  Community: number;
  Watershed: number;
  SubWatershed: number;
  ReceivingStream: number;

  NPDES: string;
  USACE404: string;
  State401: string;
  USACENWP: string;

  Comment: string;
  Latitude: string;
  Longitude: string;
  GISLatitude: number;
  GISLongitude: number;
  X: number;
  Y: number;

  PhotoUrl: string;

  Owner: string;
  OwnerContact: string;
  OwnerAddress: string;
  OwnerCity: string;
  OwnerState: string;
  OwnerPostalCode: string;
  OwnerPhoneNumber: string;
  OwnerCellPhone: string;

  /**
   * In Original MS4, this is both email and fax
   */
  OwnerFaxNumber: string;

  Operator: string;
  OperatorContact: string;
  OperatorAddress: string;
  OperatorCity: string;
  OperatorState: string;
  OperatorPostalCode: string;
  OperatorPhoneNumber: string;
  OperatorCellPhone: string;
  OperatorFaxNumber: string;

  CF1: string;
  CF2: string;
  CF3: string;
  CF4: string;
  CF5: string;
  CF6: string;
  CF7: string;
  CF8: string;
  CF9: string;
  CF10: string;
  CF11: string;
  CF12: string;
  CF13: string;
  CF14: string;
  CF15: string;
  CF16: string;
  CF17: string;
  CF18: string;
  CF19: string;
  CF20: string;
  CF21: string;
  CF22: string;
  CF23: string;
  CF24: string;
  CF25: string;
  CF26: string;
  CF27: string;
  CF28: string;
  CF29: string;
  CF30: string;
}
