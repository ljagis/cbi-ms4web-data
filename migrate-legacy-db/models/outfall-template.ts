import { Outfall } from '../../src/models';

export interface OutfallTemplate extends Outfall {
  communityName: string;
  watershedName: string;
  subwatershedName: string;
  receivingWatersName: string;

  // [cf: string]: string | any;
}
