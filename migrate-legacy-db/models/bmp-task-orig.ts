import { tableName } from '../../src/decorators';

@tableName('BMPImplementationTasks')
export class BMPImplementationTaskOrig {
  NumericID: number = undefined;
  ManagementPracticeID: number;
  Description: string;
  ImplementationDate: Date;
  Implemented: boolean;
  ImpTaskCompleteDate: Date;
}

@tableName('BMPData')
export class BMPDataOrig {
  NumericID: number = undefined;
  ManagementPracticeID: number;
  DateEntered: Date;
  Location: number;
  DataType: number;
  Quantity: number;
  DataEntryUnit: number;
  Cost: string;
  Comment: string;
  Activity: string;
}

@tableName('BMPMeasureableGoals')
export class BMPMeasureableGoalOrig {
  NumericID: number = undefined;
  ManagementPracticeID: number;
  Description: string;
  GoalDueDate: Date;
  PermitYear: number;
  GoalMet: boolean;
  DateGoalMet: Date;
  Years: number;
  Quantity: number;
  ActivityDescription: string;
  Metric: string;
  Module: string;
  CriticalData: string;
  Condition: string;
}
