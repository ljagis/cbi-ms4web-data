import { tableName } from '../../src/decorators';

@tableName('CitizensReports')
export class CitizenReportOrig {
  NumericID: number = undefined;
  CitizenName: string;
  DateEntered: Date;
  Owner: string;
  Address: string;
  City: string;
  State: string;
  PostalCode: string;
  PhoneNumber: string;
  CellPhone: string;
  FaxNumber: string;
  Report: string;
  ResponseDate: Date;
  Response: string;
  PhotoUrl: string;
  Latitude: string;
  Longitude: string;
  GISLatitude: number;
  GISLongitude: number;
  X: number;
  Y: number;
  Community: number;
  Watershed: number;
  SubWatershed: number;
  ReceivingStream: number;
  ReportTypeID: number;
  CompletionList: string;
}
