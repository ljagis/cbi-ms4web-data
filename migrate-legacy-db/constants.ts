import { EntityTypes } from '../src/shared/entity-types';
/**
 * Maps "SiteSection" in original table the new entity type string in code
 */
export const customFieldDataTypeMap = {
  A: 'string',
  C: 'boolean',
  D: 'string',
  T: 'string',
};

export const customFieldInputTypeMap = {
  A: 'textarea',
  C: 'checkbox',
  D: 'select',
  T: 'textbox',
};

export const questionsSetupDataTypeMap = {
  TextArea: 'string',
  CheckBox: 'boolean',
  DropDown: 'string',
  TextBox: 'string',
};

export const questionsSetupInputTypeMap = {
  TextArea: 'textarea',
  CheckBox: 'checkbox',
  DropDown: 'select',
  TextBox: 'textbox',
};

export const siteSectionEntityMap = {
  // These are on the CustomFieldSetup table
  Construction: EntityTypes.ConstructionSite,
  IndustrialFacility: EntityTypes.Facility,
  Outfalls: EntityTypes.Outfall,
  PostConstruction: EntityTypes.Structure,
  IllicitDischarge: EntityTypes.IllicitDischarge,
  // These are on the QuestionsSetup table
  CitizensReport: EntityTypes.CitizenReport,
  ConstructionSiteInspection: EntityTypes.ConstructionSiteInspection,
  IndustrialFacilityInspection: EntityTypes.FacilityInspection,
  OutfallInspection: EntityTypes.OutfallInspection,
  StructureInspection: EntityTypes.StructureInspection,
};

export const fileOwnerTypeToEntityMap = {
  'Best Management Practice': EntityTypes.BmpDetail,
  'Citizen Report': EntityTypes.CitizenReport,
  'Construction Site': EntityTypes.ConstructionSite,
  'Construction Site Inspection': EntityTypes.ConstructionSiteInspection,
  'Illicit Discharge': EntityTypes.IllicitDischarge,
  'Industrial Facility': EntityTypes.Facility,
  'Industrial Facility Inspection': EntityTypes.FacilityInspection,
  Outfall: EntityTypes.Outfall,
  'Outfall Inspection': EntityTypes.OutfallInspection,
  'Structural Control Inspection': EntityTypes.StructureInspection,
  Structure: EntityTypes.Structure,
  Training: '',
};
