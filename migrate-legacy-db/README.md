# Migration

# Development

`tsc -w -p migrate-legacy-db` - tells `tsc` to compile with the "migration" project and watch for changes

# Prod

Update `.env.migration.yml` and type `tsc -p migrate-legacy-db`

# Running

* `node migrate-legacy-db-dist/migrate-legacy-db/main.js --validate --config path/to/env.yml` validates
* `node migrate-legacy-db-dist/migrate-legacy-db/main.js --import --config path/to/env.yml` imports

# Import from Excel template file

* `ts-node migrate-legacy-db/main.ts --template [path-to-template-file]`
* Example: `ts-node migrate-legacy-db/main.ts --template ~/Downloads/ms4web_import_template_v1.xlsx`
* (OLD) `node migrate-legacy-db-dist/migrate-legacy-db/main.js --template [path-to-template-file]`
* (OLD) Example: `node migrate-legacy-db-dist/migrate-legacy-db/main.js --template ~/Downloads/ms4web_import_template_v1.xlsx`
