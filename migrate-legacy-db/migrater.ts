import * as winston from 'winston';
import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import * as XLSX from 'xlsx';

import {
  ConstructionSiteImport,
  FacilityImport,
  OutfallImport,
  StructureImport,
  CitizenReportImport,
  IllicitDischargeImport,
  CommonImport,
  TemplateImport,
  BmpImport,
} from './importers';
import { Config } from './models';

@injectable()
export class Migrater {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private _newDb: Knex,
    private _csImport: ConstructionSiteImport,
    private _facilityImport: FacilityImport,
    private _outfallImport: OutfallImport,
    private _structureImport: StructureImport,
    private _citizenReportImport: CitizenReportImport,
    private _illicitDischargeImport: IllicitDischargeImport,
    private _commonImport: CommonImport,
    private _bmpImport: BmpImport,
    @inject('config') private config: Config,
    private _templateImport: TemplateImport
  ) {}

  async importAll() {
    this._commonImport.ensureFileStoragePath();

    try {
      await this._newDb.transaction(async trx => {
        await this._commonImport.importCustomer(trx);

        await this._commonImport.importCommunities(trx);
        await this._commonImport.importWatersheds(trx);
        await this._commonImport.importSubWatersheds(trx);
        await this._commonImport.importReceivingWaters(trx);

        await this._commonImport.importInspectionTypes(trx);
        await this._commonImport.importControlTypes(trx); // for Structures

        // Custom Fields
        await this._commonImport.importCustomFields(trx);

        await this._bmpImport.import(trx);

        await this._csImport.import(trx);
        await this._csImport.importInspections(trx);
        await this._csImport.updateLatestInspectionDate(trx);

        await this._facilityImport.import(trx);
        await this._facilityImport.importInspections(trx);
        await this._facilityImport.updateLatestInspectionDate(trx);

        await this._outfallImport.import(trx);
        await this._outfallImport.importInspections(trx);
        await this._outfallImport.updateLatestInspectionDate(trx);

        await this._structureImport.import(trx);
        await this._structureImport.importInspections(trx);
        await this._structureImport.updateLatestInspectionDate(trx);

        await this._citizenReportImport.import(trx);
        await this._illicitDischargeImport.import(trx);

        // Summary
        await this._commonImport.validateConstructionSiteImport(trx);
        await this._commonImport.validateConstructionSiteInspectionImport(trx);
        await this._commonImport.validateFacilityImport(trx);
        await this._commonImport.validateFacilityInspectionImport(trx);
        await this._commonImport.validateOutfallImport(trx);
        await this._commonImport.validateOutfallInspectionImport(trx);
        await this._commonImport.validateStructureImport(trx);
        await this._commonImport.validateStructureInspectionImport(trx);
        await this._commonImport.validateCitizenReportImport(trx);
        await this._commonImport.validateIllicitDischargeImport(trx);
      });
    } catch (e) {
      console.error(e.stack || e);
    } finally {
      // cleanup
      await this.destroy();
    }
  }

  // one-time import files.
  async importBmpFiles() {
    this._commonImport.ensureFileStoragePath();

    try {
      await this._newDb.transaction(async trx => {
        await this._bmpImport.importBmpFiles(trx);
      });
    } catch (e) {
      console.error(e.stack || e);
    } finally {
      // cleanup
      await this.destroy();
    }
  }

  async validateAll() {
    this.logger.info(`Customer: ${this.config.CUSTOMER_ID}`);

    try {
      await this._newDb.transaction(async trx => {
        await this._commonImport.validateOutfallConditions(trx);
        await this._commonImport.validateObstructionSeverities(trx);
        await this._commonImport.validateOutfallMaterials(trx);
        await this._commonImport.validateOutfallTypes(trx);
        await this._commonImport.validatePermitStatuses(trx);
        await this._commonImport.validateSectors(trx);
      });
    } finally {
      await this.destroy();
    }
  }

  async importFile(filename: string) {
    const workbook = XLSX.readFile(filename);

    await this._newDb.transaction(async trx => {
      await this._templateImport.import(workbook, trx);
    });
  }

  async destroy() {
    await this.origKnex.destroy();
    await this._newDb.destroy();
  }
}
