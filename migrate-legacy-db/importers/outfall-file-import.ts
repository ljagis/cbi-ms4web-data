import { injectable, inject } from 'inversify';
import * as XLSX from 'xlsx';
import {
  Community,
  Watershed,
  ReceivingWater,
  CustomFieldValueMap,
  Outfall,
  OutfallMaterial,
  OutfallType,
} from '../../src/models';
import * as Bluebird from 'bluebird';
import {
  Config,
  OutfallTemplate,
  FileImportable,
  FileImportValidate,
} from '../models';
import { CommonImport } from './common-import';
import * as winston from 'winston';
import { CustomFieldRepository } from '../../src/data';
import * as Knex from 'knex';
import { getTableName, getSqlFields2 } from '../../src/decorators';
import { objectToSqlData, compareCaseInsensitive } from '../../src/shared';
import { BaseFileImport } from './base-file-import';
@injectable()
export class OutfallFileImport extends BaseFileImport
  implements FileImportable, FileImportValidate {
  constructor(
    @inject('config') private config: Config,
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('NewDb') private newKnex: Knex,
    private _commonImport: CommonImport,
    private _customFieldRepo: CustomFieldRepository
  ) {
    super();
  }

  async validateFile(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<boolean> {
    // maybe one day validate this
    return true;
  }

  async import(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<Outfall[]> {
    const outfallSheet = workbook.Sheets['Outfalls'];
    if (!outfallSheet) {
      return [];
    }
    const customFieldsFromTemplate = this.getCustomFields(outfallSheet);

    const outfallsFromSheet: OutfallTemplate[] = XLSX.utils.sheet_to_json<
      OutfallTemplate
    >(outfallSheet);
    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const allWatersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = allWatersheds.filter(w => !w.isSubWatershed).slice(0);
    const subwatersheds = allWatersheds.filter(w => w.isSubWatershed).slice(0);
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const materials = await this._commonImport.getLookup(OutfallMaterial, trx);
    const types = await this._commonImport.getLookup(OutfallType, trx);

    // TODO: include transaction
    const customFieldsFromDb = await this._customFieldRepo.getCustomFieldsForEntity(
      Outfall,
      this.config.CUSTOMER_ID
    );

    const sitesInserted = await Bluebird.mapSeries(
      outfallsFromSheet,
      async outfallImport => {
        const emptyOutfall = new Outfall();

        let outfallNew: Outfall = {
          ...emptyOutfall,
          customerId: this.config.CUSTOMER_ID,
          dateAdded: this.dateFromValue(outfallImport.dateAdded) || new Date(),
          trackingId: outfallImport.trackingId,
          location: outfallImport.location,
          nearAddress: outfallImport.nearAddress,
          complianceStatus: outfallImport.complianceStatus,
          condition: outfallImport.condition,
          // material: outfallImport.material, // see below
          // outfallType: outfallImport.outfallType, // see below
          obstruction: outfallImport.obstruction,
          outfallSizeIn: outfallImport.outfallSizeIn,
          outfallHeightIn: outfallImport.outfallHeightIn,
          outfallWidthIn: outfallImport.outfallWidthIn,
          latestInspectionDate: this.dateFromValue(
            outfallImport.latestInspectionDate
          ),
          additionalInformation: outfallImport.additionalInformation || '',
          lat: this.toNumber(outfallImport.lat),
          lng: this.toNumber(outfallImport.lng),
        };

        if (outfallImport.outfallType) {
          const outfallType = types.find(type =>
            compareCaseInsensitive(type.name, outfallImport.outfallType)
          );
          if (outfallType) {
            outfallNew.outfallType = outfallType.name;
          } else {
            outfallNew.additionalInformation =
              outfallNew.additionalInformation +
              `\r\nOutfall Type: ${outfallImport.outfallType}`;
          }
        }

        if (outfallImport.material) {
          const outfallMaterial = materials.find(mat =>
            compareCaseInsensitive(mat.name, outfallImport.material)
          );
          if (outfallMaterial) {
            outfallNew.material = outfallMaterial.name;
          } else {
            outfallNew.additionalInformation =
              outfallNew.additionalInformation +
              `\r\nMaterial: ${outfallImport.material}`;
          }
        }

        // community
        if (outfallImport.communityName) {
          const community = await this._commonImport.createCommunityIfNotExists(
            outfallImport.communityName,
            communities
          );
          if (community) {
            outfallNew.communityId = community.id;
          }
        }

        // ws
        if (outfallImport.watershedName) {
          const ws = await this._commonImport.createWatershedIfNotExists(
            outfallImport.watershedName,
            watersheds
          );
          if (ws) {
            outfallNew.watershedId = ws.id as number;
          }
        }

        // sws
        if (outfallImport.subwatershedName) {
          const sws = await this._commonImport.createWatershedIfNotExists(
            outfallImport.watershedName,
            subwatersheds,
            true
          );
          if (sws) {
            outfallNew.subwatershedId = sws.id as number;
          }
        }

        // receiving water
        if (outfallImport.receivingWatersName) {
          const rw = await this._commonImport.createReceivingWaterIfNotExists(
            outfallImport.receivingWatersName,
            receivingWaters
          );
          if (rw) {
            outfallNew.receivingWatersId = rw.id;
          }
        }

        const insertedOutfalls = await this.newKnex(getTableName(Outfall))
          .insert(objectToSqlData(Outfall, outfallNew, this.newKnex))
          .returning(
            getSqlFields2(Outfall, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Outfall>(0);
        this.logger.verbose(`Outfall: imported`, outfallNew);

        // custom fields
        const cfvs = customFieldsFromTemplate.reduce(
          (list, cfFromTemp) => {
            const cf = customFieldsFromDb.find(
              c => c.fieldLabel && c.fieldLabel.trim() === cfFromTemp.label
            );
            if (cf && outfallImport[cfFromTemp.originalField]) {
              list.push({
                id: cf.id as number,
                value: outfallImport[cfFromTemp.originalField],
              });
            }
            return list;
          },
          [] as CustomFieldValueMap[]
        );

        await this._customFieldRepo.updateCustomFieldsForEntity(
          Outfall,
          insertedOutfalls.id,
          cfvs,
          this.config.CUSTOMER_ID
        );

        return insertedOutfalls;
      }
    );
    return sitesInserted;
  }
}
