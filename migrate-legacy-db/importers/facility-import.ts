import { inject, injectable } from 'inversify';
import { ContactRepository, CustomFieldRepository } from '../../src/data';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  CustomFieldSetupOrig,
  FacilityOrig,
  SectorOrig,
  FacilityInspectionOrig,
  BmpFileOrig,
  FileOwnerTypeOrig,
  Config,
} from '../models';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  ComplianceStatus,
  CustomField,
  Contact,
  State,
  Sector,
  InspectionFrequency,
  Facility,
  FacilityInspection,
  CustomFieldValue,
} from '../../src/models';
import {
  EntityTypes,
  objectToSqlData,
  propertyNameFor,
} from '../../src/shared';
import { siteSectionEntityMap, fileOwnerTypeToEntityMap } from '../constants';
import * as Bluebird from 'bluebird';
import * as _ from 'lodash';
import { booleanToString, dateToStringOrNull } from '../utils/index';

@injectable()
export class FacilityImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _contactRepo: ContactRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const originalFacilities: FacilityOrig[] = await this._commonImport.getOriginalTable(
      FacilityOrig
    );
    this.logger.info(`Facility: Found ${originalFacilities.length} to import.`);

    const states = await this._commonImport.getLookup(State, trx);
    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const facilityComplianceStatuses = complianceStatuses.filter(
      s => s.entityType === 'Facility'
    );
    const contacts = await this._commonImport.getContacts(trx);

    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const sectors = await this._commonImport.getLookup(Sector, trx);
    const sectorsOrig = await this._commonImport.getOriginalTable(SectorOrig);

    const sectorNamesOrig = sectorsOrig.map(s => s.SectorName);
    const sectorNames = sectors.map(s => s.name);
    let facilityTypeCustomField: CustomField;

    // Per Ty: if sectors are different, then create a new CF named "Facility Type"
    if (
      sectorNamesOrig.length !== sectorNames.length ||
      _.difference(sectorNamesOrig, sectorNames).length
    ) {
      this.logger.warn(`Facility: Sector: Original sectors don't match`, {
        original: sectorNamesOrig,
        newSectors: sectorNames,
      });
      facilityTypeCustomField = await this._customFieldRepo
        .getfetchQueryBuilder(this.config.CUSTOMER_ID)
        .transacting(trx)
        .where(
          CustomField.getSqlField<CustomField>(cf => cf.fieldLabel),
          'Facility Type'
        )
        .get<CustomField>(0);
      if (!facilityTypeCustomField) {
        facilityTypeCustomField = new CustomField();
        facilityTypeCustomField.customerId = this.config.CUSTOMER_ID;
        facilityTypeCustomField.customFieldType = 'Customer';
        facilityTypeCustomField.entityType = 'Facility';
        facilityTypeCustomField.dataType = 'string';
        facilityTypeCustomField.inputType = 'select';
        facilityTypeCustomField.fieldLabel = 'Facility Type';
        facilityTypeCustomField.fieldOptions = sectorNamesOrig.join('|');
        facilityTypeCustomField = await this._customFieldRepo
          .insertQueryBuilder(facilityTypeCustomField, this.config.CUSTOMER_ID)
          .transacting(trx)
          .get<CustomField>(0);
      }
    }

    const inspectionFrequencies = await this._commonImport.getLookup(
      InspectionFrequency,
      trx
    );

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const facFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] === EntityTypes.Facility
    );
    if (!facFileOwnerType) {
      this.logger.error(`Facility: Could not find FileOwnerType`);
      return;
    }

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.entityType),
        EntityTypes.Facility
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(CustomField.getSqlField<CustomField>(c => c.isDeleted), 0);
    const origCFs = await this._commonImport.getOriginalTable(
      CustomFieldSetupOrig
    );
    const origFacilityCFs = origCFs.filter(
      c => siteSectionEntityMap[c.SiteSection] === EntityTypes.Facility
    );

    const existingFacilities: Facility[] = await this.newKnex(
      dh.getTableName(Facility)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(Facility, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        Facility.getSqlField<Facility>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Facility.getSqlField<Facility>(c => c.isDeleted), 0)
      .catch(this.logger.error);

    const importRecordsP = await Bluebird.each(
      originalFacilities,
      async originalFacility => {
        let existingFacility = existingFacilities.find(
          f => f.originalId === originalFacility.NumericID
        );

        if (existingFacility) {
          this.logger.warn(
            `Facility: "${existingFacility.name}" already exists.  Skipping.`,
            existingFacility
          );
          return;
        }
        const state = states.find(
          s =>
            originalFacility.State &&
            ((s.abbr &&
              s.abbr.toLowerCase() === originalFacility.State.toLowerCase()) ||
              (s.name &&
                s.name.toLowerCase() === originalFacility.State.toLowerCase()))
        );
        let complianceStatus = facilityComplianceStatuses.find(
          s =>
            (s.name || '').toLowerCase() ===
            (originalFacility.EnforcementStatus || '').toLowerCase()
        );
        if (!complianceStatus) {
          // Fall back to No Status
          complianceStatus = facilityComplianceStatuses.find(
            s => s.name === 'No Status'
          );
          this.logger.warn(
            `Facility: ComplianceStatus: Did not find matching EnforcementStatus ${
              originalFacility.EnforcementStatus
            }`
          );
        }

        const community = communities.find(
          c => c.originalId === originalFacility.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalFacility.Watershed
        );
        const subWatershed = watersheds.find(
          w =>
            w.isSubWatershed && w.originalId === originalFacility.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalFacility.ReceivingStream
        );

        const inspectionFreq = inspectionFrequencies.find(
          f => f.name === originalFacility.InspectionFrequency
        );

        let newFacility: Facility = Object.assign(new Facility(), <Partial<
          Facility
        >>{
          originalId: originalFacility.NumericID,
          customerId: this.config.CUSTOMER_ID,
          dateAdded: this._commonImport.toUtcDate(originalFacility.DateEntered),
          name: originalFacility.FacilityName,
          physicalAddress1: originalFacility.PAddress,
          physicalCity: originalFacility.PCity,
          physicalState: state ? state.abbr : null,
          physicalZip: originalFacility.PZip,
          complianceStatus: complianceStatus ? complianceStatus.name : null,
          additionalInformation: originalFacility.FacilityDescription,
          sicCode: originalFacility.SIC,
          lat: originalFacility.GISLatitude,
          lng: originalFacility.GISLongitude,

          communityId: community ? community.id : null,
          watershedId: watershed ? watershed.id : null,
          subwatershedId: subWatershed ? subWatershed.id : null,
          receivingWatersId: receivingWater ? receivingWater.id : null,

          isHighRisk: originalFacility.HighRisk,
          inspectionFrequencyId: inspectionFreq ? inspectionFreq.id : null,
        });

        const sectorOrig = sectorsOrig.find(
          s => s.NumericID === originalFacility.Sector
        );
        let sector: Sector;
        if (sectorOrig && sectorOrig.SectorName) {
          sector = sectors.find(
            s =>
              s.name &&
              s.name.toLowerCase() === sectorOrig.SectorName.toLowerCase()
          );
          if (!sector) {
            this.logger.warn(
              `Facility: Sector: Could not map sector "${
                sectorOrig.SectorName
              }".  Added to additional Info.`,
              sectorOrig
            );
            newFacility.additionalInformation = [
              newFacility.additionalInformation,
              `Sector: ${sectorOrig.SectorName}`,
            ].join('\r\n');
          } else {
            newFacility.sector = sector.name;
          }
        }

        const insertedFacility = await this.newKnex(dh.getTableName(Facility))
          .transacting(trx)
          .insert(objectToSqlData(Facility, newFacility, this.newKnex))
          .returning(
            dh.getSqlFields2(Facility, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Facility>(0);
        this.logger.verbose(
          `Facility: imported "${insertedFacility.name}"`,
          insertedFacility
        );

        // Custom Fields
        await this._commonImport.updateCustomFieldsFromCustomFieldSetup(
          Facility,
          insertedFacility.id,
          origFacilityCFs,
          customFields,
          originalFacility,
          trx
        );

        // Update facility type if exists
        if (facilityTypeCustomField) {
          await this._customFieldRepo.updateCustomFieldsForEntity(
            Facility,
            insertedFacility.id,
            [
              {
                id: facilityTypeCustomField.id as number,
                value: sectorOrig ? sectorOrig.SectorName : null,
              },
            ],
            this.config.CUSTOMER_ID,
            trx
          );
        }

        // Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            facFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            insertedFacility.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`Facility: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            Facility,
            insertedFacility.id,
            trx
          );
        });

        let existingOwnerContact = contacts.find(
          c =>
            c.contactType === 'Owner' &&
            c.name === originalFacility.Contact &&
            c.company === originalFacility.Owner &&
            c.mailingAddressLine1 === originalFacility.Address &&
            c.mailingZip === originalFacility.PostalCode
        );

        if (!existingOwnerContact) {
          const operatorContact = Object.assign(new Contact(), <Partial<
            Contact
          >>{
            contactType: 'Owner',
            customerId: this.config.CUSTOMER_ID,
            name: originalFacility.Contact,
            company: originalFacility.Owner,
            phone: originalFacility.PhoneNumber,
            fax: originalFacility.FaxNumber,
            email: originalFacility.IEmail,
            mailingAddressLine1: originalFacility.Address,
            mailingCity: originalFacility.City,
            mailingState: originalFacility.State,
            mailingZip: originalFacility.PostalCode,
          });

          existingOwnerContact = await this._commonImport.importContact(
            operatorContact,
            trx
          );

          contacts.push(existingOwnerContact);
          this.logger.verbose(
            `Facility: Owner Contact: Added name: "` +
              `${existingOwnerContact.name}", company: ${
                existingOwnerContact.company
              }`
          );
        } else {
          this.logger.verbose(
            `Facility: Owner Contact: Found existing Owner contact: "` +
              `${existingOwnerContact.name}", company: ${
                existingOwnerContact.company
              }`
          );
        }

        const entityContactId = await this._contactRepo.insertContactForEntity(
          Facility,
          insertedFacility.id,
          existingOwnerContact.id as number,
          this.config.CUSTOMER_ID,
          trx
        );
        this.logger.verbose(
          `Facility: Associated Operator contact to EntityContact id: ${entityContactId}`
        );
      }
    );

    return importRecordsP;
  }

  async importInspections(trx: Knex.Transaction) {
    const originalInspections: FacilityInspectionOrig[] = await this.origKnex(
      dh.getTableName(FacilityInspectionOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `FacilityInspection: Found ${originalInspections.length} to import.`
    );

    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const csComplianceStatuses = complianceStatuses.filter(
      c => c.entityType === 'Facility'
    );
    const noStatus = csComplianceStatuses.find(s => s.name === 'No Status');

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const facFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.FacilityInspection
    );
    if (!facFileOwnerType) {
      this.logger.error(`FacilityInspection: Could not find FileOwnerType`);
      return;
    }

    const facInspectiontype = await this._commonImport.getDefaultInspectionType(
      EntityTypes.FacilityInspection,
      trx
    );

    const facCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.FacilityInspection,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: facCft.id,
        [CustomField.sqlField(
          c => c.entityType
        )]: EntityTypes.FacilityInspection,
        [CustomField.sqlField(c => c.customerId)]: this.config.CUSTOMER_ID,
        [CustomField.getSqlField<CustomField>(c => c.isDeleted)]: 0,
      });

    const facilities: Facility[] = await this.newKnex(dh.getTableName(Facility))
      .transacting(trx)
      .where(
        Facility.getSqlField<Facility>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(
        dh.getSqlFields2<Facility>(Facility, {
          includeInternalField: true,
          asProperty: true,
        })
      );

    const selectFacilityId = this.newKnex(dh.getTableName(Facility))
      .transacting(trx)
      .where(
        Facility.getSqlField<Facility>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(Facility.getSqlField<Facility>(c => c.id));
    const existingInspections: FacilityInspection[] = await this.newKnex(
      dh.getTableName(FacilityInspection)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(FacilityInspection, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .whereIn(
        FacilityInspection.getSqlField<FacilityInspection>(c => c.facilityId),
        selectFacilityId
      )
      .where(
        FacilityInspection.getSqlField<FacilityInspection>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    await Bluebird.each(originalInspections, async originalInspection => {
      let existingInspection = existingInspections.find(
        f => f.originalId === originalInspection.NumericID
      );
      if (existingInspection) {
        this.logger.warn(
          `FacilityInspection: OriginalId "${
            existingInspection.originalId
          }" already exists.  Skipping.`,
          existingInspection
        );
        return;
      }
      // find the Facility
      const facility = facilities.find(
        cs => cs.originalId === originalInspection.IndustrialFacility
      );
      if (!facility) {
        this.logger.error(
          `FacilityInspection: Could not find original id: ${
            originalInspection.IndustrialFacility
          }`
        );
        return;
      }

      let newInspection: FacilityInspection = Object.assign(
        new FacilityInspection(),
        <Partial<FacilityInspection>>{
          originalId: originalInspection.NumericID,
          facilityId: facility.id,
          inspectionTypeId: facInspectiontype.id,
          customFormTemplateId: facCft.id,
          createdDate: this._commonImport.toUtcDate(originalInspection.Created),
          inspectionDate: this._commonImport.toUtcDate(
            originalInspection.InspectionDate
          ),
          complianceStatus: noStatus ? noStatus.name : null,
          dateResolved: this._commonImport.toUtcDate(
            originalInspection.DateResolved
          ),
          isFacilityActive: originalInspection.FacilityIsActive,
          isFacilityPermitted: originalInspection.FacilityIsPermitted,
          isSwppOnSite: originalInspection.SWPPOnSite,
          areRecordsCurrent: originalInspection.RecordsCurrent,
          areSamplingDataEvaluationAcceptable:
            originalInspection.SamplingDataEvaluation,
          isBmpAcceptable: originalInspection.BestManagementPractices,
          isGoodHouseKeeping: originalInspection.GoodHouseKeeping,
          isErosionPresent: originalInspection.ErosionIsPresent,
          areWashoutsPresent: originalInspection.WashoutsArePresent,
          isDownstreamErosionPresent: originalInspection.DownstreamErosion,
          areFloatablesPresent: originalInspection.FloatablesRemovalRequired,
          areIllicitDischargesPresent:
            originalInspection.IllicitDischargePresent,
          areNonStormWaterDischargePresent:
            originalInspection.NonStormWaterDischarges,
          isReturnInspectionRequired:
            originalInspection.RecommendedReturnInspection,

          additionalInformation: originalInspection.Comments,
        }
      );
      if (originalInspection.EnforcedActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Enforced Actions: ${originalInspection.EnforcedActions}`,
        ].join('\r\n');
      }
      if (originalInspection.CorrectiveActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Corrective Actions: ${originalInspection.CorrectiveActions}`,
        ].join('\r\n');
      }

      if (originalInspection.Inspector) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Inspector: ${originalInspection.Inspector}`,
        ].join('\r\n');
      }

      const insertedInspection = await this.newKnex(
        dh.getTableName(FacilityInspection)
      )
        .transacting(trx)
        .insert(
          objectToSqlData(FacilityInspection, newInspection, this.newKnex)
        )
        .returning(
          dh.getSqlFields2(FacilityInspection, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<FacilityInspection>(0);
      this.logger.verbose(
        `FacilityInspection: imported "${insertedInspection.originalId}"`,
        insertedInspection
      );

      // Update Custom Fields for Custom Form Template
      await this._updateCustomFieldValues(
        insertedInspection,
        customFields,
        trx
      );

      // Handle Original Custom Fields
      await this._commonImport.updateCustomFieldsFromQuestions(
        FacilityInspection,
        insertedInspection,
        insertedInspection.originalId,
        customFields,
        trx
      );

      // Files
      const origFiles: BmpFileOrig[] = await this._commonImport
        .getOriginal(BmpFileOrig)
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerType),
          facFileOwnerType.NumericID
        )
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerID),
          insertedInspection.originalId
        );
      await Bluebird.each(origFiles, async origFile => {
        this.logger.verbose(`FacilityInspection: File`, origFile);
        await this._commonImport.importFile(
          origFile,
          FacilityInspection,
          insertedInspection.id,
          trx
        );
      });
    });
  }

  /**
   * Sets inspectionDate for all Facilitys to be the latest date from inspection
   *
   */
  async updateLatestInspectionDate(trx: Knex.Transaction) {
    const selectMaxInspectionDate = this.newKnex(
      FacilityInspection.getTableName()
    )
      .transacting(trx)
      .max(
        FacilityInspection.getSqlField<FacilityInspection>(
          c => c.inspectionDate
        )
      )
      .whereRaw(`?? = ??`, [
        FacilityInspection.getSqlField<FacilityInspection>(
          i => i.facilityId,
          true
        ),
        Facility.getSqlField<Facility>(c => c.id, true),
      ])
      .where(
        FacilityInspection.getSqlField<FacilityInspection>(
          i => i.inspectionDate,
          true
        ),
        '<=',
        new Date()
      )
      .whereIn(
        FacilityInspection.getSqlField<FacilityInspection>(
          i => i.facilityId,
          true
        ),
        this.newKnex(Facility.getTableName())
          .select(Facility.getSqlField<Facility>(c => c.id))
          .where(
            Facility.getSqlField<Facility>(c => c.customerId),
            this.config.CUSTOMER_ID
          )
      );
    const updateQb = this.newKnex(Facility.getTableName())
      .transacting(trx)
      .update(
        Facility.getSqlField<Facility>(c => c.latestInspectionDate),
        this.newKnex.raw(selectMaxInspectionDate.toQuery()).wrap('(', ')') // knex doesnt' support querybuilder
      )
      .where(
        Facility.getSqlField<Facility>(c => c.customerId),
        this.config.CUSTOMER_ID
      );

    const rowCount = await updateQb;
    this.logger.info(
      `FacilityInspection: updated last inspected date`,
      rowCount
    );
  }

  private async _updateCustomFieldValues(
    inspection: FacilityInspection,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const getCf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    const cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: getCf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: getCf('Is Facility Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isFacilityActive),
      },
      {
        customFieldId: getCf('Is Facility Permitted').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isFacilityPermitted),
      },
      {
        customFieldId: getCf('Is Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isErosionPresent),
      },
      {
        customFieldId: getCf('Is Downstream Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDownstreamErosionPresent),
      },
      {
        customFieldId: getCf('Are Floatables Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areFloatablesPresent),
      },
      {
        customFieldId: getCf('Are Illicit Discharges Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areIllicitDischargesPresent),
      },
      {
        customFieldId: getCf('Are Non-Stormwater Disharges Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areNonStormWaterDischargePresent),
      },
      {
        customFieldId: getCf('Are Washouts Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areWashoutsPresent),
      },
      {
        customFieldId: getCf('Are Records Current').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areRecordsCurrent),
      },
      {
        customFieldId: getCf('Are Sampling Data Evaluation Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areSamplingDataEvaluationAcceptable),
      },
      {
        customFieldId: getCf('Is BMP Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isBmpAcceptable),
      },
      {
        customFieldId: getCf('Is Good House Keeping').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isGoodHouseKeeping),
      },
      {
        customFieldId: getCf('Is Return Inspection Required').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isReturnInspectionRequired),
      },
      {
        customFieldId: getCf('Is SWPP On Site').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSwppOnSite),
      },
    ];
    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }
}
