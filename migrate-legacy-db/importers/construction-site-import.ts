import { inject, injectable } from 'inversify';
import { ContactRepository } from '../../src/data';
import * as Knex from 'knex';
import * as winston from 'winston';
import * as _ from 'lodash';
import {
  ConstructionSiteOrig,
  OwnerTypeOrig,
  ProjecTypeOrig,
  PermitStatusOrig,
  CustomFieldSetupOrig,
  ConstructionSiteInspectionOrig,
  FileOwnerTypeOrig,
  BmpFileOrig,
  Config,
} from '../models';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  ComplianceStatus,
  CustomField,
  ConstructionSite,
  Project,
  Contact,
  permitStatus,
  projectStatus,
  ConstructionSiteInspection,
  CustomFieldValue,
} from '../../src/models';
import {
  EntityTypes,
  objectToSqlData,
  propertyNameFor,
} from '../../src/shared';
import { siteSectionEntityMap, fileOwnerTypeToEntityMap } from '../constants';
import * as Bluebird from 'bluebird';
import { booleanToString, dateToStringOrNull } from '../utils/index';
import { projectTypes as staticProjectTypes } from '../../src/models/static-metadata';

@injectable()
export class ConstructionSiteImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _contactRepo: ContactRepository,
    // private _customFieldRepo: CustomFieldRepository,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const originalRecords: ConstructionSiteOrig[] = await this.origKnex(
      dh.getTableName(ConstructionSiteOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `ConstructionSite: Found ${originalRecords.length} to import.`
    );

    const contacts = await this._commonImport.getContacts(trx);
    const currentProjects = await this._commonImport.getLookup(Project, trx, {
      customerId: this.config.CUSTOMER_ID,
    });

    const ownerTypes = await this._commonImport.getOriginalTable(OwnerTypeOrig);
    const projectTypes = await this._commonImport.getOriginalTable(
      ProjecTypeOrig
    );
    const permitStatuses = await this._commonImport.getOriginalTable(
      PermitStatusOrig
    );
    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );
    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const csComplianceStatuses = complianceStatuses.filter(
      c => c.entityType === EntityTypes.ConstructionSite
    );
    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const csFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.ConstructionSite
    );
    if (!csFileOwnerType) {
      this.logger.error(`ConstructionSite: Could not find FileOwnerType`);
      return;
    }

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(c => c.entityType)]: EntityTypes.ConstructionSite,
        [CustomField.sqlField(c => c.customerId)]: this.config.CUSTOMER_ID,
        [CustomField.sqlField(c => c.isDeleted)]: 0,
      });
    const origCustomFields = await this._commonImport.getOriginalTable(
      CustomFieldSetupOrig
    );
    const origCsCustomFields = origCustomFields.filter(
      c => siteSectionEntityMap[c.SiteSection] === EntityTypes.ConstructionSite
    );

    const existingRecords: ConstructionSite[] = await this.newKnex(
      dh.getTableName(ConstructionSite)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(ConstructionSite, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        ConstructionSite.getSqlField<ConstructionSite>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(
        ConstructionSite.getSqlField<ConstructionSite>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    const importRecordsP = await Bluebird.each(
      originalRecords,
      async originalCS => {
        let existingRecord = existingRecords.find(
          c => c.originalId === originalCS.NumericID
        );

        if (existingRecord) {
          this.logger.warn(
            `ConstructionSite: "${
              existingRecord.name
            }" already exists.  Skipping.`,
            existingRecord
          );
          return;
        }

        let project: Project = null;
        if (originalCS.ProjectName) {
          project = await this._commonImport.createProjectIfNotExists(
            originalCS.ProjectName,
            currentProjects,
            trx
          );
        }

        const projectType = this._handleCsProjectType(
          originalCS,
          ownerTypes,
          projectTypes
        );

        const community = communities.find(
          c => c.originalId === originalCS.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalCS.Watershed
        );
        const subWatershed = watersheds.find(
          w => w.isSubWatershed && w.originalId === originalCS.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalCS.ReceivingStream
        );

        let newConstructionsite: ConstructionSite = Object.assign(
          new ConstructionSite(),
          <Partial<ConstructionSite>>{
            originalId: originalCS.NumericID,
            customerId: this.config.CUSTOMER_ID,
            dateAdded: this._commonImport.toUtcDate(originalCS.Created),
            trackingId: originalCS.TrackingID,
            name: originalCS.SiteName,
            projectId: project ? project.id : null,
            projectType: projectType,
            physicalAddress1: originalCS.SiteAddress,
            // permitStatus: see _handleCsPermitStatus
            projectArea: originalCS.TotalArea,
            disturbedArea: originalCS.DistributedArea,
            startDate: this._commonImport.toUtcDate(originalCS.StartDate),
            estimatedCompletionDate: null,
            completionDate: this._commonImport.toUtcDate(originalCS.EndDate),
            // projectStatus: see _handleCsProjectStatus
            communityId: community ? community.id : null,
            watershedId: watershed ? watershed.id : null,
            subwatershedId: subWatershed ? subWatershed.id : null,
            receivingWatersId: receivingWater ? receivingWater.id : null,

            additionalInformation: originalCS.Comment,
            latestInspectionDate: null,
            lat: originalCS.GISLatitude,
            lng: originalCS.GISLongitude,
          }
        );

        // ComplianceStatus
        const noStatus = csComplianceStatuses.find(c => c.name === 'No Status');
        newConstructionsite.complianceStatus = noStatus ? noStatus.name : null;

        this._handleCsProjectStatus(originalCS, newConstructionsite);
        this._handleCsPermitStatus(
          originalCS,
          newConstructionsite,
          permitStatuses
        );

        // handle certifications: if not na, then set additionalInformation
        if (
          originalCS.NPDES &&
          ['n/a', 'na'].indexOf(originalCS.NPDES.toLowerCase()) < 0
        ) {
          newConstructionsite.additionalInformation = [
            newConstructionsite.additionalInformation,
            `NPDES: ${originalCS.NPDES}`,
          ].join('\r\n');
        }
        if (
          originalCS.USACE404 &&
          ['n/a', 'na'].indexOf(originalCS.USACE404.toLowerCase()) < 0
        ) {
          newConstructionsite.additionalInformation = [
            newConstructionsite.additionalInformation,
            `USACE404: ${originalCS.USACE404}`,
          ].join('\r\n');
        }
        if (
          originalCS.State401 &&
          ['n/a', 'na'].indexOf(originalCS.State401.toLowerCase()) < 0
        ) {
          newConstructionsite.additionalInformation = [
            newConstructionsite.additionalInformation,
            `State401: ${originalCS.State401}`,
          ].join('\r\n');
        }
        if (
          originalCS.USACENWP &&
          ['n/a', 'na'].indexOf(originalCS.USACENWP.toLowerCase()) < 0
        ) {
          newConstructionsite.additionalInformation = [
            newConstructionsite.additionalInformation,
            `USACENWP: ${originalCS.USACENWP}`,
          ].join('\r\n');
        }
        const insertedCs = await this.newKnex(dh.getTableName(ConstructionSite))
          .transacting(trx)
          .insert(
            objectToSqlData(ConstructionSite, newConstructionsite, this.newKnex)
          )
          .returning(
            dh.getSqlFields2(ConstructionSite, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<ConstructionSite>(0);
        this.logger.verbose(
          `ConstructionSite: imported "${insertedCs.name}"`,
          insertedCs
        );

        // Custom Fields
        await this._commonImport.updateCustomFieldsFromCustomFieldSetup(
          ConstructionSite,
          insertedCs.id,
          origCsCustomFields,
          customFields,
          originalCS,
          trx
        );

        // Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            csFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            insertedCs.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`ConstructionSite: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            ConstructionSite,
            insertedCs.id,
            trx
          );
        });

        // Contacts
        let existingOwnerContact = contacts.find(
          c =>
            c.contactType === 'Owner' &&
            c.name === originalCS.OwnerContact &&
            c.company === originalCS.Owner &&
            c.mailingAddressLine1 === originalCS.OwnerAddress &&
            c.mailingZip === originalCS.OwnerPostalCode
        );
        if (!existingOwnerContact) {
          const ownerContact: Partial<Contact> = Object.assign(
            new Contact(),
            <Partial<Contact>>{
              contactType: 'Owner',
              customerId: this.config.CUSTOMER_ID,
              name: originalCS.OwnerContact,
              company: originalCS.Owner,
              phone: originalCS.OwnerPhoneNumber,
              mobilePhone: originalCS.OwnerCellPhone,
              mailingAddressLine1: originalCS.OwnerAddress,
              mailingAddressLine2: null,
              mailingCity: originalCS.OwnerCity,
              mailingState: originalCS.OwnerState,
              mailingZip: originalCS.OwnerPostalCode,
            }
          );
          this._handleContactFaxEmail(ownerContact, originalCS.OwnerFaxNumber);
          existingOwnerContact = await this._commonImport.importContact(
            ownerContact,
            trx
          );
          contacts.push(existingOwnerContact);
          this.logger.verbose(
            `ConstructionSite: Owner Contact: Added name: "${
              existingOwnerContact.name
            }", company: ${existingOwnerContact.company}`
          );
        } else {
          this.logger.info(
            `ConstructionSite: Owner Contact: Found existing owner contact: "` +
              `${existingOwnerContact.name}", company: ${
                existingOwnerContact.company
              }`
          );
        }
        let entityContactId = await this._contactRepo.insertContactForEntity(
          ConstructionSite,
          insertedCs.id,
          existingOwnerContact.id as number,
          this.config.CUSTOMER_ID,
          trx
        );
        this.logger.verbose(
          `ConstructionSite: Associated Owner contact to EntityContact id: ${entityContactId}`
        );

        let existingOperatorContact = contacts.find(
          c =>
            c.contactType === 'Operator' &&
            c.name === originalCS.OperatorContact &&
            c.company === originalCS.Operator &&
            c.mailingAddressLine1 === originalCS.OperatorAddress &&
            c.mailingZip === originalCS.OperatorPostalCode
        );
        if (!existingOperatorContact) {
          const operatorContact = Object.assign(new Contact(), <Partial<
            Contact
          >>{
            contactType: 'Operator',
            customerId: this.config.CUSTOMER_ID,
            name: originalCS.OperatorContact,
            company: originalCS.Operator,
            phone: originalCS.OperatorPhoneNumber,
            mobilePhone: originalCS.OperatorCellPhone,
            mailingAddressLine1: originalCS.OperatorAddress,
            mailingAddressLine2: null,
            mailingCity: originalCS.OperatorCity,
            mailingState: originalCS.OperatorState,
            mailingZip: originalCS.OperatorPostalCode,
          });
          this._handleContactFaxEmail(
            operatorContact,
            originalCS.OperatorFaxNumber
          );
          existingOperatorContact = await this._commonImport.importContact(
            operatorContact,
            trx
          );
          contacts.push(existingOperatorContact);
          this.logger.verbose(
            `ConstructionSite: Operator Contact: Added name: "` +
              `${existingOperatorContact.name}", company: ${
                existingOperatorContact.company
              }`
          );
        } else {
          this.logger.info(
            `ConstructionSite: Operator Contact: Found existing Operator contact: "` +
              `${existingOperatorContact.name}", company: ${
                existingOperatorContact.company
              }`
          );
        }

        entityContactId = await this._contactRepo.insertContactForEntity(
          ConstructionSite,
          insertedCs.id,
          existingOperatorContact.id as number,
          this.config.CUSTOMER_ID,
          trx
        );
        this.logger.info(
          `ConstructionSite: Associated Operator contact to EntityContact id: ${entityContactId}`
        );
      }
    ).catch(this.logger.error);
    return importRecordsP;
  }

  async importInspections(trx: Knex.Transaction) {
    const originalInspections: ConstructionSiteInspectionOrig[] = await this.origKnex(
      dh.getTableName(ConstructionSiteInspectionOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `ConstructionSiteInspections: Found ${
        originalInspections.length
      } to import.`
    );

    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const csComplianceStatuses = complianceStatuses.filter(
      c => c.entityType === 'ConstructionSite'
    );
    const noStatus = csComplianceStatuses.find(s => s.name === 'No Status');

    const csiInspectiontype = await this._commonImport.getDefaultInspectionType(
      EntityTypes.ConstructionSiteInspection,
      trx
    );

    const csiCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.ConstructionSiteInspection,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: csiCft.id,
        [CustomField.sqlField(
          c => c.entityType
        )]: EntityTypes.ConstructionSiteInspection,
        [CustomField.sqlField(c => c.customerId)]: this.config.CUSTOMER_ID,
        [CustomField.sqlField(c => c.isDeleted)]: 0,
      });

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const csiFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.ConstructionSiteInspection
    );
    if (!csiFileOwnerType) {
      this.logger.error(
        `ConstructionSiteInspections: Could not find FileOwnerType`
      );
      return;
    }

    const sites: ConstructionSite[] = await this.newKnex(
      dh.getTableName(ConstructionSite)
    )
      .transacting(trx)
      .where(
        ConstructionSite.getSqlField<ConstructionSite>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(
        dh.getSqlFields2<ConstructionSite>(ConstructionSite, {
          includeInternalField: true,
          asProperty: true,
        })
      );

    const selectCsId = this.newKnex(dh.getTableName(ConstructionSite))
      .transacting(trx)
      .where(
        ConstructionSite.getSqlField<ConstructionSite>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(ConstructionSite.getSqlField<ConstructionSite>(c => c.id));
    const existingInspections: ConstructionSiteInspection[] = await this.newKnex(
      dh.getTableName(ConstructionSiteInspection)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(ConstructionSiteInspection, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .whereIn(
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          c => c.constructionSiteId
        ),
        selectCsId
      )
      .where(
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          c => c.isDeleted
        ),
        0
      )
      .catch(this.logger.error);

    await Bluebird.each(originalInspections, async originalInspection => {
      let existingInspection = existingInspections.find(
        f => f.originalId === originalInspection.NumericID
      );
      if (existingInspection) {
        this.logger.warn(
          `ConstructionSiteInspection: OriginalId "${
            existingInspection.originalId
          }" already exists.  Skipping.`,
          existingInspection
        );
        return;
      }
      // find the ConstructionSite
      const site = sites.find(
        cs => cs.originalId === originalInspection.ConstructionSite
      );

      if (!site) {
        this.logger.error(
          `ConstructionSiteInspection: Could not find original id: ${
            originalInspection.ConstructionSite
          }`
        );
        return;
      }
      const complianceStatus = csComplianceStatuses.find(
        s => s.name === originalInspection.EnforcementType
      );
      let newInspection: ConstructionSiteInspection = Object.assign(
        new ConstructionSiteInspection(),
        <Partial<ConstructionSiteInspection>>{
          originalId: originalInspection.NumericID,
          constructionSiteId: site.id,
          inspectionTypeId: csiInspectiontype.id,
          customFormTemplateId: csiCft.id,
          createdDate: this._commonImport.toUtcDate(originalInspection.Created),
          inspectionDate: this._commonImport.toUtcDate(
            originalInspection.InspectionDate
          ),
          complianceStatus: complianceStatus
            ? complianceStatus.name
            : noStatus.name,
          isSiteActive: originalInspection.SiteIsActive,
          isPlanOnSite: originalInspection.SitePlanOnSite,
          areErosionControlsAcceptable: originalInspection.ErosionControls,
          areStructuralControlsAcceptable:
            originalInspection.StructuralControls,
          isWasteManagementAcceptable: originalInspection.WasteManagement,
          isMaintenanceOfControlsAcceptable:
            originalInspection.MaintenanceOfControls,
          areLocalControlsAcceptable: originalInspection.LocalControls,
          isSitePermitted: originalInspection.SiteIsPermitted,
          arePlanRecordsCurrent: originalInspection.PlanRecordsCurrent,
          areStabilizationControlsAcceptable:
            originalInspection.StabilizationControls,
          areTrackingControlsAcceptable: originalInspection.TrackingControls,
          areOutfallVelocityControlsAcceptable:
            originalInspection.OutfallVelocityControls,
          areNonStormWaterControlsAcceptable:
            originalInspection.NonStormWaterControls,
          additionalInformation: originalInspection.Comments,

          dateResolved: this._commonImport.toUtcDate(
            originalInspection.DateResolved
          ),
        }
      );
      if (originalInspection.Conversation) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Conversation: ${originalInspection.Conversation}`,
        ].join('\r\n');
      }
      if (originalInspection.EnforcedActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Enforced Actions: ${originalInspection.EnforcedActions}`,
        ].join('\r\n');
      }
      if (originalInspection.CorrectiveActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Corrective Actions: ${originalInspection.CorrectiveActions}`,
        ].join('\r\n');
      }

      if (originalInspection.Inspector) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Inspector: ${originalInspection.Inspector}`,
        ].join('\r\n');
      }

      // TODO: FollowUpInspectionId

      const insertedInspection = await this.newKnex(
        dh.getTableName(ConstructionSiteInspection)
      )
        .transacting(trx)
        .insert(
          objectToSqlData(
            ConstructionSiteInspection,
            newInspection,
            this.newKnex
          )
        )
        .returning(
          dh.getSqlFields2(ConstructionSiteInspection, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<ConstructionSiteInspection>(0);
      this.logger.verbose(
        `ConstructionSiteInspection: imported "${
          insertedInspection.originalId
        }"`,
        insertedInspection
      );

      // Update Custom Fields for Custom Form Template
      await this._updateCustomFieldValues(
        insertedInspection,
        customFields,
        trx
      );

      // Handle Original Custom Fields
      await this._commonImport.updateCustomFieldsFromQuestions(
        ConstructionSiteInspection,
        insertedInspection,
        insertedInspection.originalId,
        customFields,
        trx
      );

      // Files
      const origFiles: BmpFileOrig[] = await this._commonImport
        .getOriginal(BmpFileOrig)
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerType),
          csiFileOwnerType.NumericID
        )
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerID),
          insertedInspection.originalId
        );
      await Bluebird.each(origFiles, async origFile => {
        this.logger.verbose(`ConstructionSiteInspection: File`, origFile);
        await this._commonImport.importFile(
          origFile,
          ConstructionSiteInspection,
          insertedInspection.id as number,
          trx
        );
      });
    });
  }

  /**
   * Sets inspectionDate for all ConstructionSites to be the latest date from inspection
   *
   */
  async updateLatestInspectionDate(trx: Knex.Transaction) {
    const selectMaxInspectionDate = this.newKnex(
      ConstructionSiteInspection.getTableName()
    )
      .transacting(trx)
      .max(
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          c => c.inspectionDate
        )
      )
      .whereRaw(`?? = ??`, [
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          i => i.constructionSiteId,
          true
        ),
        ConstructionSite.getSqlField<ConstructionSite>(c => c.id, true),
      ])
      .where(
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          i => i.inspectionDate,
          true
        ),
        '<=',
        new Date()
      )
      .whereIn(
        ConstructionSiteInspection.getSqlField<ConstructionSiteInspection>(
          i => i.constructionSiteId,
          true
        ),
        this.newKnex(ConstructionSite.getTableName())
          .select(ConstructionSite.getSqlField<ConstructionSite>(c => c.id))
          .where(
            ConstructionSite.getSqlField<ConstructionSite>(c => c.customerId),
            this.config.CUSTOMER_ID
          )
      );
    const updateQb = this.newKnex(ConstructionSite.getTableName())
      .transacting(trx)
      .update(
        ConstructionSite.getSqlField<ConstructionSite>(
          c => c.latestInspectionDate
        ),
        this.newKnex.raw(selectMaxInspectionDate.toQuery()).wrap('(', ')') // knex doesnt' support querybuilder
      )
      .where(
        ConstructionSite.getSqlField<ConstructionSite>(c => c.customerId),
        this.config.CUSTOMER_ID
      );

    const rowCount = await updateQb;
    this.logger.info(
      `ConstructionSiteInspection: updated last inspected date`,
      rowCount
    );
  }

  _handleCsProjectType(
    origCs: ConstructionSiteOrig,
    ownerTypes: OwnerTypeOrig[],
    projectTypes: ProjecTypeOrig[]
  ): string {
    const ownerType = ownerTypes.find(o => o.NumericID === origCs.OwnerType);
    const projectType = projectTypes.find(
      pt => pt.NumericID === origCs.ProjectType
    );

    // Municipal
    if (
      (ownerType && ownerType.OwnerTypeName === 'Municipal') ||
      (projectType && projectType.ProjectTypeName === 'Municipal')
    ) {
      return 'Municipal';
    }
    // Commercial
    if (projectType && projectType.ProjectTypeName === 'Commercial') {
      return 'Commercial';
    }
    if (projectType && projectType.ProjectTypeName === 'Industrial') {
      return staticProjectTypes.industrial;
    }

    // Residential
    if (
      projectType &&
      ['Residential', 'Subdivision'].indexOf(projectType.ProjectTypeName) >= 0
    ) {
      return 'Residential';
    }
    return null;
  }

  _handleCsPermitStatus(
    origCs: ConstructionSiteOrig,
    constructionSite: ConstructionSite,
    originalPermitStatuses: PermitStatusOrig[]
  ) {
    const originalPermitStatus = originalPermitStatuses.find(
      ps => ps.NumericID === origCs.PermitStatus
    );

    if (originalPermitStatus && originalPermitStatus.PermitStatusName) {
      const matchedPermitStatus = Object.keys(permitStatus).find(
        k =>
          permitStatus[k].toLowerCase() ===
          originalPermitStatus.PermitStatusName.toLowerCase()
      );
      if (matchedPermitStatus) {
        constructionSite.permitStatus = permitStatus[matchedPermitStatus];
        this.logger.verbose(
          `ConstructionSite: PermitStatus: Found matching Permit Status: ${
            originalPermitStatus.PermitStatusName
          }`
        );
      } else {
        /*
          Per Ty:
            Compliant , Non-Compliant, Under Enforcement, Completed = Approved
            Terminated, Suspended = Other
        */
        if (
          _.includes(
            ['compliant', 'non-compliant', 'under enforcement', 'completed'],
            originalPermitStatus.PermitStatusName.toLowerCase()
          )
        ) {
          constructionSite.permitStatus = permitStatus.approved;
        } else {
          constructionSite.permitStatus = permitStatus.other;
        }

        // if not found then set additionalInfo
        constructionSite.additionalInformation = [
          constructionSite.additionalInformation,
          `Permit Status: ${originalPermitStatus.PermitStatusName}`,
        ].join('\r\n');
        this.logger
          .warn(`ConstructionSite: PermitStatus: Could not match PermitStatus "${
          originalPermitStatus.PermitStatusName
        }".
          Appended to additionalInformation`);
      }

      // compliance status
      switch (originalPermitStatus.PermitStatusName.toLowerCase()) {
        case 'compliant':
          constructionSite.complianceStatus = 'Compliant';
          break;
        case 'under enforcement':
          constructionSite.complianceStatus = 'Notice of Violation';
          break;
        case 'non-compliant':
          constructionSite.complianceStatus = 'Written Warning Citation';
          break;
      }
    }
  }

  _handleCsProjectStatus(
    origCs: ConstructionSiteOrig,
    constructionSite: ConstructionSite
  ) {
    if (origCs.Completed) {
      constructionSite.projectStatus = projectStatus.completed;
      return;
    }
    if (origCs.Active) {
      constructionSite.projectStatus = projectStatus.active;
      return;
    }
    constructionSite.projectStatus = null;
  }

  // handle case if fax contains email
  private _handleContactFaxEmail(
    contact: Partial<Contact>,
    faxOrEmail: string
  ) {
    if (faxOrEmail && faxOrEmail.indexOf('@') > -1) {
      contact.email = faxOrEmail;
    } else {
      contact.fax = faxOrEmail;
    }
  }

  private async _updateCustomFieldValues(
    inspection: ConstructionSiteInspection,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const getCf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    const cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: getCf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: getCf('Is Site Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSiteActive),
      },
      {
        customFieldId: getCf('Is Site Permitted').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isSitePermitted),
      },
      {
        customFieldId: getCf('Acceptable Erosion Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areErosionControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Local Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areLocalControlsAcceptable),
      },
      {
        customFieldId: getCf('Has Plan On Site').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isPlanOnSite),
      },
      {
        customFieldId: getCf('Acceptable Non-stormwater Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areNonStormWaterControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Outfall Velocity Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areOutfallVelocityControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Stabilization Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areStabilizationControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Structural Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areStructuralControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Tracking Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areTrackingControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Maintenance of Controls').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isMaintenanceOfControlsAcceptable),
      },
      {
        customFieldId: getCf('Acceptable Waste Management').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isWasteManagementAcceptable),
      },
      {
        customFieldId: getCf('Has Current Plan Records').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.arePlanRecordsCurrent),
      },
    ];
    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }
}
