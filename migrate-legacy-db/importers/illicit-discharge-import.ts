import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  IllicitDischargeOrig,
  CustomFieldSetupOrig,
  BmpFileOrig,
  FileOwnerTypeOrig,
  Config,
} from '../models';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  ComplianceStatus,
  State,
  IllicitDischarge,
  CustomField,
  CustomFieldValue,
} from '../../src/models';
import {
  objectToSqlData,
  EntityTypes,
  propertyNameFor,
} from '../../src/shared';
import * as Bluebird from 'bluebird';
import { siteSectionEntityMap, fileOwnerTypeToEntityMap } from '../constants';
import { dateToStringOrNull } from '../utils/index';

@injectable()
export class IllicitDischargeImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const originalIllicits: IllicitDischargeOrig[] = await this.origKnex(
      dh.getTableName(IllicitDischargeOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `IllicitDischarge: Found ${originalIllicits.length} to import.`
    );

    const existingIllicits: IllicitDischarge[] = await this.newKnex(
      dh.getTableName(IllicitDischarge)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(IllicitDischarge, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        IllicitDischarge.getSqlField<IllicitDischarge>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(
        IllicitDischarge.getSqlField<IllicitDischarge>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    const states = await this._commonImport.getLookup(State, trx);
    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const idComplianceStatuses = complianceStatuses.filter(
      s => s.entityType === 'IllicitDischarge'
    );
    const noStatus = idComplianceStatuses.find(c => c.name === 'No Status');

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const illicitFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.IllicitDischarge
    );
    if (!illicitFileOwnerType) {
      this.logger.error(`IllicitDischarge: Could not find FileOwnerType`);
      return;
    }

    const idInspectiontype = await this._commonImport.getDefaultInspectionType(
      EntityTypes.IllicitDischarge,
      trx
    );

    const idCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.IllicitDischarge,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: idCft.id,
        [CustomField.getSqlField<CustomField>(
          c => c.entityType
        )]: EntityTypes.IllicitDischarge,
      })
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(CustomField.getSqlField<CustomField>(c => c.isDeleted), 0);
    const origCFs = await this._commonImport.getOriginalTable(
      CustomFieldSetupOrig
    );
    const origIllicitCFs = origCFs.filter(
      c => siteSectionEntityMap[c.SiteSection] === EntityTypes.IllicitDischarge
    );

    const importedRecords = await Bluebird.each(
      originalIllicits,
      async originalIllicit => {
        let existingIllicit = existingIllicits.find(
          f => f.originalId === originalIllicit.NumericID
        );
        if (existingIllicit) {
          this.logger.warn(
            `IllicitDischarge: "${
              existingIllicit.originalId
            }" already exists.  Skipping.`,
            existingIllicit
          );
          return;
        }

        const state = states.find(
          s =>
            originalIllicit.State &&
            ((s.abbr &&
              s.abbr.toLowerCase() === originalIllicit.State.toLowerCase()) ||
              (s.name &&
                s.name.toLowerCase() === originalIllicit.State.toLowerCase()))
        );

        const community = communities.find(
          c => c.originalId === originalIllicit.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalIllicit.Watershed
        );
        const subWatershed = watersheds.find(
          w => w.isSubWatershed && w.originalId === originalIllicit.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalIllicit.ReceivingStream
        );

        let newIllicit: IllicitDischarge = Object.assign(
          new IllicitDischarge(),
          <Partial<IllicitDischarge>>{
            originalId: originalIllicit.NumericID,
            customFormTemplateId: idCft.id,
            inspectionTypeId: idInspectiontype.id,
            customerId: this.config.CUSTOMER_ID,
            dateAdded: this._commonImport.toUtcDate(originalIllicit.Created),
            dateReported: this._commonImport.toUtcDate(originalIllicit.Created),
            name: originalIllicit.Name,
            physicalAddress1: originalIllicit.Address,
            physicalCity: originalIllicit.City,
            physicalState: state ? state.abbr : null,
            physicalZip: originalIllicit.PostalCode,
            phoneNumber: originalIllicit.Phonenumber,
            faxNumber: originalIllicit.FaxNumber,
            cellNumber: originalIllicit.CellPhone,
            dischargeDescription: originalIllicit.Report,
            dateEliminated: this._commonImport.toUtcDate(
              originalIllicit.ResolvedDate
            ),
            requestCorrectiveAction: originalIllicit.CorrectiveActions,
            conversation: originalIllicit.Conversation,

            communityId: community ? community.id : null,
            watershedId: watershed ? watershed.id : null,
            subwatershedId: subWatershed ? subWatershed.id : null,
            receivingWatersId: receivingWater ? receivingWater.id : null,

            lat: originalIllicit.GISLatitude,
            lng: originalIllicit.GISLongitude,

            complianceStatus: noStatus ? noStatus.name : null,
          }
        );

        const insertedIllicit = await this.newKnex(
          dh.getTableName(IllicitDischarge)
        )
          .transacting(trx)
          .insert(objectToSqlData(IllicitDischarge, newIllicit, this.newKnex))
          .returning(
            dh.getSqlFields2(IllicitDischarge, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<IllicitDischarge>(0);
        this.logger.verbose(
          `IllicitDischarge: imported "${insertedIllicit.originalId}"`,
          insertedIllicit
        );

        // Update Custom Fields for Custom Form Template
        await this._updateCustomFieldValues(insertedIllicit, customFields, trx);

        // Custom Fields TODO
        await this._updateCustomFieldsFromCustomFieldSetup(
          insertedIllicit,
          originalIllicit,
          origIllicitCFs,
          customFields,
          trx
        );

        // Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            illicitFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            insertedIllicit.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`IllicitDischarge: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            IllicitDischarge,
            insertedIllicit.id,
            trx
          );
        });

        return insertedIllicit;
      }
    );

    return importedRecords;
  }

  private async _updateCustomFieldValues(
    cr: IllicitDischarge,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === cr.customFormTemplateId &&
          f.fieldLabel === label
      );
    let cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: cf('Date Eliminated').id,
        globalIdFk: cr.globalId,
        value: dateToStringOrNull(cr.dateEliminated),
      },
      {
        customFieldId: cf('Phone Number').id,
        globalIdFk: cr.globalId,
        value: cr.phoneNumber,
      },
      {
        customFieldId: cf('Cell Number').id,
        globalIdFk: cr.globalId,
        value: cr.cellNumber,
      },
      {
        customFieldId: cf('Fax Number').id,
        globalIdFk: cr.globalId,
        value: cr.faxNumber,
      },
      {
        customFieldId: cf('Conversation').id,
        globalIdFk: cr.globalId,
        value: cr.conversation,
      },
      {
        customFieldId: cf('Corrective Action').id,
        globalIdFk: cr.globalId,
        value: cr.requestCorrectiveAction,
      },
    ];

    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }

  private async _updateCustomFieldsFromCustomFieldSetup(
    illicit: IllicitDischarge,
    originalIllicit: IllicitDischargeOrig,
    origCfs: CustomFieldSetupOrig[],
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const cfvs = origCfs
      .map(cfSetup => {
        const cf = customFields.find(c => c.fieldLabel === cfSetup.FieldLabel);
        if (!cf) {
          this.logger.warn(
            `CustomField: Could not find custom field ${cfSetup.FieldLabel}`
          );
          return;
        }
        let value: string = originalIllicit[`CF${cfSetup.FieldNumber}`];

        // handle checkbox type:
        // Yes -> "true", No -> "false"
        if (cfSetup.FieldType === 'C' && value) {
          switch (value.toLowerCase()) {
            case 'yes':
              value = 'true';
              break;
            case 'no':
              value = 'false';
              break;
          }
        }
        return <CustomFieldValue>{
          customFieldId: cf.id,
          globalIdFk: illicit.globalId,
          value,
        };
      })
      .filter(cf => cf);

    const rowsToInsert = cfvs.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );
    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }
}
