import { inject, injectable } from 'inversify';
import * as XLSX from 'xlsx';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  Config,
  FileImportable,
  FileImportValidate,
  FacilityTemplate,
} from '../models';
import { CustomFieldRepository, ContactRepository } from '../../src/data';
import * as Bluebird from 'bluebird';
import {
  Facility,
  Community,
  Watershed,
  ReceivingWater,
  CustomFieldValueMap,
  Contact,
} from '../../src/models';
import { CommonImport } from './common-import';
import * as _ from 'lodash';
import { getTableName, getSqlFields2 } from '../../src/decorators';
import { objectToSqlData } from '../../src/shared';
import { BaseFileImport } from './base-file-import';

@injectable()
export class FacilityFileImport extends BaseFileImport
  implements FileImportable, FileImportValidate {
  constructor(
    @inject('config') private config: Config,
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('NewDb') private newKnex: Knex,
    private _contactRepo: ContactRepository,
    private _commonImport: CommonImport,
    private _customFieldRepo: CustomFieldRepository
  ) {
    super();
  }

  async validateFile(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<boolean> {
    let validate = true;
    const csSheet = workbook.Sheets['Facilities'];
    if (!csSheet) {
      return true;
    }
    // const facilities = XLSX.utils.sheet_to_json<FacilityTemplate>(csSheet);

    const customFieldsFromTemplate = this.getCustomFields(csSheet)
      .filter(f => f)
      .map(f => f.label.trim());

    const customFields = await this._customFieldRepo.getCustomFieldsForEntity(
      Facility,
      this.config.CUSTOMER_ID
    );
    const customFieldNamesFromDb = customFields
      .filter(cf => cf && cf.fieldLabel)
      .map(cf => cf.fieldLabel.trim());

    const customFieldsMissing = _.difference(
      customFieldsFromTemplate,
      customFieldNamesFromDb
    );
    if (customFieldsMissing.length) {
      this.logger.error(
        'Custom Fields missing for Facility: ',
        customFieldsMissing
      );

      validate = false;
    }

    // TODO: Validate Compliance Status

    // TODO: Validate Sectors

    return validate;
  }

  async import(workbook: XLSX.WorkBook, trx): Bluebird<Facility[]> {
    const csSheet = workbook.Sheets['Facilities'];
    if (!csSheet) {
      return [];
    }
    const customFieldsFromTemplate = this.getCustomFields(csSheet);

    const facilities: FacilityTemplate[] = XLSX.utils.sheet_to_json<
      FacilityTemplate
    >(csSheet);

    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const allWatersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = allWatersheds.filter(w => !w.isSubWatershed).slice(0);
    const subwatersheds = allWatersheds.filter(w => w.isSubWatershed).slice(0);
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const customFieldsFromDb = await this._customFieldRepo.getCustomFieldsForEntity(
      Facility,
      this.config.CUSTOMER_ID
    );

    const facilitiesInserted = await Bluebird.mapSeries(
      facilities,
      async csImport => {
        let csNew: Facility = Object.assign(new Facility(), {
          customerId: this.config.CUSTOMER_ID,
          name: csImport.name,
          trackingId: csImport.trackingId,
          physicalAddress1: csImport.physicalAddress1,
          physicalAddress2: csImport.physicalAddress2,
          physicalCity: csImport.physicalCity,
          physicalState: csImport.physicalState,
          physicalZip: csImport.physicalZip,
          complianceStatus: csImport.complianceStatus,
          sector: csImport.sector,
          isHighRisk: this.yesNoToBoolean(csImport.isHighRisk),
          yearBuilt: this.toNumber(csImport.yearBuilt),
          squareFootage: this.toNumber(csImport.squareFootage),
          lat: this.toNumber(csImport.lat),
          lng: this.toNumber(csImport.lng),
          additionalInformation: csImport.additionalInformation,
          sicCode: csImport.sicCode,
          latestInspectionDate: csImport.latestInspectionDate,
          // convert inspectionFrequencyNames to Ids in spreadsheet
          inspectionFrequencyId: this.toNumber(csImport.inspectionFrequencyId),
        } as Facility);

        // community
        if (csImport.communityName) {
          const community = await this._commonImport.createCommunityIfNotExists(
            csImport.communityName,
            communities
          );
          if (community) {
            csNew.communityId = community.id;
          }
        }

        // ws
        if (csImport.watershedName) {
          const ws = await this._commonImport.createWatershedIfNotExists(
            csImport.watershedName,
            watersheds
          );
          if (ws) {
            csNew.watershedId = ws.id as number;
          }
        }

        // sws
        if (csImport.subwatershedName) {
          const sws = await this._commonImport.createWatershedIfNotExists(
            csImport.subwatershedName,
            subwatersheds,
            true
          );
          if (sws) {
            csNew.subwatershedId = sws.id as number;
          }
        }

        // receiving water
        if (csImport.receivingWatersName) {
          const rw = await this._commonImport.createReceivingWaterIfNotExists(
            csImport.receivingWatersName,
            receivingWaters
          );
          if (rw) {
            csNew.receivingWatersId = rw.id;
          }
        }

        const insertedCs = await this.newKnex(getTableName(Facility))
          .insert(objectToSqlData(Facility, csNew, this.newKnex))
          .returning(
            getSqlFields2(Facility, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Facility>(0);
        this.logger.verbose(`Facility: imported`, csNew);

        // custom fields
        const cfvs = customFieldsFromTemplate.reduce(
          (list, cfFromTemp) => {
            const cf = customFieldsFromDb.find(
              c => c.fieldLabel && c.fieldLabel.trim() === cfFromTemp.label
            );
            if (cf && csImport[cfFromTemp.originalField]) {
              list.push({
                id: cf.id as number,
                value: csImport[cfFromTemp.originalField],
              });
            }
            return list;
          },
          [] as CustomFieldValueMap[]
        );

        await this._customFieldRepo.updateCustomFieldsForEntity(
          Facility,
          insertedCs.id,
          cfvs,
          this.config.CUSTOMER_ID
        );

        // contacts
        if (csImport.contactName) {
          const contacts = await this._commonImport.getContacts(trx);

          let existingContact = contacts.find(
            c =>
              c.contactType === csImport.contactType &&
              c.name === csImport.contactName &&
              c.email === csImport.contactEmail &&
              c.phone === csImport.contactPhone
          );
          if (!existingContact) {
            const newContact: Partial<Contact> = Object.assign(
              new Contact(),
              <Partial<Contact>>{
                contactType: csImport.contactType,
                customerId: this.config.CUSTOMER_ID,
                name: csImport.contactName,
                email: csImport.contactEmail,
                phone: csImport.contactPhone,
              }
            );
            existingContact = await this._commonImport.importContact(
              newContact,
              trx
            );
            contacts.push(existingContact);
            this.logger.verbose(
              `Facility: Contact: Added name: "${existingContact.name}"`
            );
          } else {
            this.logger.info(
              `Facility: Contact: Found existing contact: "` +
                `${existingContact.name}"`
            );
          }

          const entityContactId = await this._contactRepo.insertContactForEntity(
            Facility,
            insertedCs.id,
            existingContact.id as number,
            this.config.CUSTOMER_ID,
            trx
          );
          this.logger.verbose(
            `Facility: Associated contact to EntityContact id: ${entityContactId}`
          );
        }

        // tslint:disable-next-line: no-console
        console.log('insertedCs: ', insertedCs);
        return insertedCs;
      }
    );
    return facilitiesInserted;
  }
}
