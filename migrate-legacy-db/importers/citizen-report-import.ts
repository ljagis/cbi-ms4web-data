import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  CitizenReportOrig,
  FileOwnerTypeOrig,
  BmpFileOrig,
  Config,
} from '../models';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  ComplianceStatus,
  State,
  CitizenReport,
  CustomField,
  CustomFieldValue,
} from '../../src/models';
import {
  objectToSqlData,
  EntityTypes,
  propertyNameFor,
} from '../../src/shared';
import * as Bluebird from 'bluebird';
import { fileOwnerTypeToEntityMap } from '../constants';
import { dateToStringOrNull } from '../utils/index';

@injectable()
export class CitizenReportImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const originalCRs: CitizenReportOrig[] = await this.origKnex(
      dh.getTableName(CitizenReportOrig)
    ).catch(this.logger.error);
    this.logger.info(`CitizenReport: Found ${originalCRs.length} to import.`);

    const existingCRs: CitizenReport[] = await this.newKnex(
      dh.getTableName(CitizenReport)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CitizenReport, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        CitizenReport.getSqlField<CitizenReport>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(CitizenReport.getSqlField<CitizenReport>(c => c.isDeleted), 0)
      .catch(this.logger.error);

    const states = await this._commonImport.getLookup(State, trx);
    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const crComplianceStatuses = complianceStatuses.filter(
      s => s.entityType === EntityTypes.CitizenReport
    );

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const crFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.CitizenReport
    );
    if (!crFileOwnerType) {
      this.logger.error(`CitizenReport: Could not find FileOwnerType`);
      return;
    }

    const crInspectiontype = await this._commonImport.getDefaultInspectionType(
      EntityTypes.CitizenReport,
      trx
    );

    const crCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.CitizenReport,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: crCft.id,
        [CustomField.sqlField(c => c.entityType)]: EntityTypes.CitizenReport,
      })
      .where(CustomField.sqlField(c => c.customerId), this.config.CUSTOMER_ID)
      .where(CustomField.sqlField(c => c.isDeleted), 0);

    const importedRecords = await Bluebird.each(
      originalCRs,
      async originalCR => {
        let existingCR = existingCRs.find(
          f => f.originalId === originalCR.NumericID
        );
        if (existingCR) {
          this.logger.warn(
            `CitizenReport: "${
              existingCR.originalId
            }" already exists.  Skipping.`,
            existingCR
          );
          return;
        }
        const state = states.find(
          s =>
            originalCR.State &&
            ((s.abbr &&
              s.abbr.toLowerCase() === originalCR.State.toLowerCase()) ||
              (s.name &&
                s.name.toLowerCase() === originalCR.State.toLowerCase()))
        );

        const community = communities.find(
          c => c.originalId === originalCR.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalCR.Watershed
        );
        const subWatershed = watersheds.find(
          w => w.isSubWatershed && w.originalId === originalCR.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalCR.ReceivingStream
        );

        let newCR: CitizenReport = Object.assign(new CitizenReport(), <Partial<
          CitizenReport
        >>{
          originalId: originalCR.NumericID,
          inspectionTypeId: crInspectiontype.id,
          customFormTemplateId: crCft.id,
          customerId: this.config.CUSTOMER_ID,
          citizenName: originalCR.CitizenName,
          dateAdded: this._commonImport.toUtcDate(originalCR.DateEntered),
          dateReported: this._commonImport.toUtcDate(originalCR.DateEntered),
          physicalAddress1: originalCR.Address,
          physicalCity: originalCR.City,
          physicalState: state ? state.abbr : null,
          physicalZip: originalCR.PostalCode,
          phoneNumber: originalCR.PhoneNumber,
          cellNumber: originalCR.CellPhone,
          faxNumber: originalCR.FaxNumber,
          report: originalCR.Report,
          responseDate: this._commonImport.toUtcDate(originalCR.ResponseDate),
          response: originalCR.Response,

          communityId: community ? community.id : null,
          watershedId: watershed ? watershed.id : null,
          subwatershedId: subWatershed ? subWatershed.id : null,
          receivingWatersId: receivingWater ? receivingWater.id : null,

          lat: originalCR.GISLatitude,
          lng: originalCR.GISLongitude,
        });

        const complianceStatus = crComplianceStatuses.find(
          c => c.name === originalCR.CompletionList
        );
        if (originalCR.CompletionList && !complianceStatus) {
          this.logger.warn(
            `CitizenReport: ComplianceStatus: Could not find ${
              originalCR.CompletionList
            }`
          );
          newCR.additionalInformation = [
            newCR.additionalInformation,
            `Completion List: ${originalCR.CompletionList}`,
          ].join('\r\n');
        } else if (complianceStatus) {
          newCR.complianceStatus = complianceStatus.name;
        }

        const insertedCR = await this.newKnex(dh.getTableName(CitizenReport))
          .transacting(trx)
          .insert(objectToSqlData(CitizenReport, newCR, this.newKnex))
          .returning(
            dh.getSqlFields2(CitizenReport, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<CitizenReport>(0);
        this.logger.verbose(
          `CitizenReport: imported "${insertedCR.originalId}"`,
          insertedCR
        );
        existingCR = insertedCR;

        // Update Custom Fields for Custom Form Template
        await this._updateCustomFieldValues(insertedCR, customFields, trx);

        // Handle Original Custom Fields - Citizen Report in QuestionsSetup table
        await this._commonImport.updateCustomFieldsFromQuestions(
          CitizenReport,
          insertedCR,
          insertedCR.originalId,
          customFields,
          trx
        );

        // Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            crFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            existingCR.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`CitizenReport: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            CitizenReport,
            existingCR.id,
            trx
          );
        });

        return existingCR;
      }
    );

    return importedRecords;
  }

  private async _updateCustomFieldValues(
    cr: CitizenReport,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const cf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === cr.customFormTemplateId &&
          f.fieldLabel === label
      );
    let cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: cf('Phone Number').id,
        globalIdFk: cr.globalId,
        value: cr.phoneNumber,
      },
      {
        customFieldId: cf('Cell Number').id,
        globalIdFk: cr.globalId,
        value: cr.cellNumber,
      },
      {
        customFieldId: cf('Fax Number').id,
        globalIdFk: cr.globalId,
        value: cr.faxNumber,
      },
      {
        customFieldId: cf('Response Date').id,
        globalIdFk: cr.globalId,
        value: dateToStringOrNull(cr.responseDate),
      },
      {
        customFieldId: cf('Response').id,
        globalIdFk: cr.globalId,
        value: cr.response,
      },
    ];
    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }
}
