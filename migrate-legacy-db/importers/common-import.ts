import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import * as _ from 'lodash';
import * as fs from 'fs-extra';
import * as mime from 'mime';
import * as moment from 'moment';
import * as path from 'path';
import * as sharp from 'sharp';
import * as uuid from 'uuid';
import * as winston from 'winston';

import {
  BMPDataOrig,
  BMPImplementationTaskOrig,
  BMPMeasureableGoalOrig,
  BestManagementPracticeOrig,
  BmpFileOrig,
  CitizenReportOrig,
  CommunityOrig,
  ConditionOrig,
  Config,
  ConstructionSiteInspectionOrig,
  ConstructionSiteOrig,
  ControlTypeOrig,
  CustomFieldSetupOrig,
  FacilityInspectionOrig,
  FacilityOrig,
  IllicitDischargeOrig,
  InspectionTypeOrig,
  MinimumControlMeasureOrig,
  ObstructionSeverityOrig,
  OutfallInspectionOrig,
  OutfallMaterialOrig,
  OutfallOrig,
  OutfallTypeOrig,
  PermitStatusOrig,
  QuestionOrig,
  QuestionsSetupOrig,
  ReceivingStreamOrig,
  SectorOrig,
  StructureInspectionOrig,
  StructureOrig,
  SubWatershedOrig,
  WatershedOrig,
} from '../models';
import {
  BmpActivity,
  BmpControlMeasure,
  BmpTask,
  CitizenReport,
  Community,
  ConstructionSite,
  ConstructionSiteInspection,
  Contact,
  CustomField,
  CustomFieldValue,
  CustomFieldValueMap,
  CustomFormTemplate,
  Customer,
  Entity,
  Facility,
  FacilityInspection,
  FileEntity,
  IllicitDischarge,
  Inspection,
  InspectionType,
  Outfall,
  OutfallInspection,
  OutfallMaterial,
  OutfallType,
  Project,
  ReceivingWater,
  Sector,
  Structure,
  StructureControlType,
  StructureInspection,
  Watershed,
  obstructionSeverities,
  outfallConditions,
  permitStatus,
} from '../../src/models';
import {
  CustomFieldRepository,
  CustomFormTemplateRepository,
  FileRepository,
} from '../../src/data';
import {
  EntityTypes,
  objectToSqlData,
  propertyNameFor,
} from '../../src/shared';
import {
  customFieldDataTypeMap,
  customFieldInputTypeMap,
  questionsSetupDataTypeMap,
  questionsSetupInputTypeMap,
  siteSectionEntityMap,
} from '../constants';
import {
  getSqlField,
  getSqlFields,
  getSqlFields2,
  getTableName,
} from '../../src/decorators';
import { inject, injectable } from 'inversify';

import { Defaults } from '../../src/shared/defaults';

const bmpjs = require('bmp-js');

@injectable()
export class CommonImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    // private _contactRepo: ContactRepository,
    private _customFieldRepo: CustomFieldRepository,
    private _cftRepo: CustomFormTemplateRepository,
    private _fileRepo: FileRepository
  ) {}

  /**
   * imports customer row, default form templates for inspections/investigations, and default inspection types
   * @param trx
   */
  async importCustomer(trx: Knex.Transaction) {
    // check if Customer exists
    const customer = await this.newKnex(Customer.getTableName())
      .first(
        ...getSqlFields2(Customer, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .where(Customer.getSqlField<Customer>(c => c.id), this.config.CUSTOMER_ID)
      .transacting(trx)
      .catch(this.logger.error);

    if (customer) {
      this.logger.warn(`"${customer.id}" exists.  Skipping.`, customer);
    } else {
      const newCustomer = Object.assign(new Customer(), <Partial<Customer>>{
        id: this.config.CUSTOMER_ID,
        fullName: this.config.CUSTOMER_NAME,
        stateAbbr: this.config.CUSTOMER_STATE,
        plan: 'premium',
      });

      const imported = await this.newKnex(Customer.getTableName())
        .insert({
          Id: this.config.CUSTOMER_ID,
          ...objectToSqlData(Customer, newCustomer, this.newKnex),
        })
        .returning(
          getSqlFields2(Customer, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .transacting(trx)
        .catch(this.logger.error)
        .get<Customer>(0);
      this.logger.info(`Customer: Imported ${imported.id}`, imported);

      await this._cftRepo.createDefaultFormTemplates(
        this.config.CUSTOMER_ID,
        trx
      );
    }

    return this;
  }

  async importCommunities(trx: Knex.Transaction) {
    const originalCommunities: CommunityOrig[] = await this.origKnex
      .select('*')
      .from(getTableName(CommunityOrig))
      .catch(this.logger.error);
    this.logger.info(
      `Community: Found ${originalCommunities.length} to import.`
    );

    const existingCommunities: Community[] = await this.newKnex
      .transacting(trx)
      .select(
        ...getSqlFields2(Community, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .from(getTableName(Community))
      .where(
        Community.getSqlField<Community>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .catch(this.logger.error);

    await Bluebird.each(originalCommunities, async origCommunity => {
      const existingComm = existingCommunities.find(
        c => c.originalId === origCommunity.NumericID
      );
      let newCommunity = Object.assign(new Community(), <Partial<Community>>{
        customerId: this.config.CUSTOMER_ID,
        name: origCommunity.CommunityName,
        originalId: origCommunity.NumericID,
      });

      if (existingComm) {
        this.logger.warn(
          `Community: ${existingComm.name} already exists.  Skipping.`,
          existingComm
        );
      } else {
        const inserted = await this.newKnex(getTableName(Community))
          .insert(objectToSqlData(Community, newCommunity, this.newKnex))
          .returning(
            getSqlFields2(Community, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .transacting(trx)
          .catch(this.logger.error)
          .get<Community>(0);
        this.logger.verbose(`Community: imported "${inserted.name}"`, inserted);
      }
    }).catch(this.logger.error);
    return this;
  }

  async importWatersheds(trx: Knex.Transaction) {
    const originalWatersheds: WatershedOrig[] = await this.origKnex(
      getTableName(WatershedOrig)
    )
      .select('*')
      .catch(this.logger.error);
    this.logger.info(
      `Watershed: Found ${originalWatersheds.length} to import.`
    );

    const existingWatersheds: Watershed[] = await this.newKnex(
      getTableName(Watershed)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(Watershed, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        Watershed.getSqlField<Watershed>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Watershed.getSqlField<Watershed>(w => w.isSubWatershed), 0)
      .catch(this.logger.error);

    await Bluebird.each(originalWatersheds, async origWs => {
      const existingWs = existingWatersheds.find(
        c => c.originalId === origWs.NumericID && !c.isSubWatershed
      );
      let newWs = Object.assign(new Watershed(), <Partial<Watershed>>{
        originalId: origWs.NumericID,
        customerId: this.config.CUSTOMER_ID,
        name: origWs.WatershedName,
        isSubWatershed: false,
      });

      if (existingWs) {
        this.logger.warn(
          `Watershed: "${existingWs.name}" already exists.  Skipping.`,
          existingWs
        );
      } else {
        const inserted = await this.newKnex(getTableName(Watershed))
          .insert(objectToSqlData(Watershed, newWs, this.newKnex))
          .returning(
            getSqlFields2(Watershed, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .transacting(trx)
          .catch(this.logger.error)
          .get<Watershed>(0);
        this.logger.verbose(`Watershed: imported "${inserted.name}"`, inserted);
      }
    });

    return this;
  }

  async importSubWatersheds(trx: Knex.Transaction) {
    const originalSubWatersheds: SubWatershedOrig[] = await this.origKnex(
      getTableName(SubWatershedOrig)
    )
      .select('*')
      .catch(this.logger.error);
    this.logger.info(
      `SubWatershed: Found ${originalSubWatersheds.length} to import.`
    );

    const existingSubWatersheds: Watershed[] = await this.newKnex(
      getTableName(Watershed)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(Watershed, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        Watershed.getSqlField<Watershed>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Watershed.getSqlField<Watershed>(w => w.isSubWatershed), 1);

    await Bluebird.each(originalSubWatersheds, async origWs => {
      const existingSubWs = existingSubWatersheds.find(
        c => c.originalId === origWs.NumericID && c.isSubWatershed
      );
      let newWs = Object.assign(new Watershed(), <Partial<Watershed>>{
        originalId: origWs.NumericID,
        customerId: this.config.CUSTOMER_ID,
        name: origWs.SubWatershedName,
        isSubWatershed: true,
      });

      if (existingSubWs) {
        this.logger.warn(
          `SubWatershed: "${existingSubWs.name}" already exists.  Skipping.`,
          existingSubWs
        );
      } else {
        const inserted = await this.newKnex(getTableName(Watershed))
          .transacting(trx)
          .insert(objectToSqlData(Watershed, newWs, this.newKnex))
          .returning(
            getSqlFields2(Watershed, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Watershed>(0);
        this.logger.verbose(
          `SubWatershed: imported "${inserted.name}"`,
          inserted
        );
      }
    }).catch(this.logger.error);

    return this;
  }

  async importContact(contact: Partial<Contact>, trx: Knex.Transaction) {
    contact.customerId = this.config.CUSTOMER_ID; // ensure customerId
    const data = objectToSqlData(Contact, contact, this.newKnex);
    let returnFields = getSqlFields(Contact, undefined, false, true);
    const queryBuilder = this.newKnex(Contact.tableName)
      .transacting(trx)
      .insert(data)
      .returning(returnFields);
    return queryBuilder.get<Contact>(0);
  }

  async importReceivingWaters(trx: Knex.Transaction) {
    const originalReceivingStreams: ReceivingStreamOrig[] = await this.origKnex(
      getTableName(ReceivingStreamOrig)
    )
      .select('*')
      .catch(this.logger.error);
    this.logger.info(
      `ReceivingWater: Found ${originalReceivingStreams.length} to import.`
    );

    const existingReceivingWaters: ReceivingWater[] = await this.newKnex(
      getTableName(ReceivingWater)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(ReceivingWater, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        ReceivingWater.getSqlField<ReceivingWater>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .catch(this.logger.error);

    await Bluebird.each(originalReceivingStreams, async origWs => {
      const existingSubWs = existingReceivingWaters.find(
        c => c.originalId === origWs.NumericID
      );
      let newWs = Object.assign(new ReceivingWater(), <Partial<ReceivingWater>>{
        originalId: origWs.NumericID,
        customerId: this.config.CUSTOMER_ID,
        name: origWs.ReceivingStreamName,
      });

      if (existingSubWs) {
        this.logger.warn(
          `ReceivingWater: "${existingSubWs.name}" already exists.  Skipping.`,
          existingSubWs
        );
      } else {
        const inserted = await this.newKnex(getTableName(ReceivingWater))
          .transacting(trx)
          .insert(objectToSqlData(ReceivingWater, newWs, this.newKnex))
          .returning(
            getSqlFields2(ReceivingWater, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<ReceivingWater>(0);
        this.logger.verbose(
          `ReceivingWater: imported "${inserted.name}"`,
          inserted
        );
      }
    }).catch(this.logger.error);
    return this;
  }

  async importControlTypes(trx: Knex.Transaction) {
    const origControlTypes = await this.getOriginalTable(ControlTypeOrig);
    this.logger.info(
      `StructureControlType: Found ${origControlTypes.length} to import.`
    );

    const existingControlTypes: StructureControlType[] = await this.newKnex(
      getTableName(StructureControlType)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(StructureControlType, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        StructureControlType.getSqlField<StructureControlType>(
          c => c.customerId
        ),
        this.config.CUSTOMER_ID
      )
      .catch(this.logger.error);

    await Bluebird.each(origControlTypes, async origControlType => {
      const existingCT = existingControlTypes.find(
        c => c.name === origControlType.ControlTypeName
      );
      if (existingCT) {
        this.logger.warn(
          `StructureControlType: "${
            existingCT.name
          }" already exists.  Skipping.`,
          existingCT
        );
      } else {
        let newRecord = Object.assign(new StructureControlType(), <Partial<
          StructureControlType
        >>{
          customerId: this.config.CUSTOMER_ID,
          name: origControlType.ControlTypeName,
        });
        const inserted = await this.newKnex(getTableName(StructureControlType))
          .transacting(trx)
          .insert(
            objectToSqlData(StructureControlType, newRecord, this.newKnex)
          )
          .returning(
            getSqlFields2(StructureControlType, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<StructureControlType>(0);
        this.logger.verbose(
          `StructureControlType: imported "${inserted.name}"`,
          inserted
        );
      }
    });
  }

  /**
   * Need to import 2 tables from original - CustomFieldSetup, QuestionsSetup
   *
   */
  async importCustomFields(trx: Knex.Transaction) {
    const origCustomFieldSetups = await this.getOriginalTable(
      CustomFieldSetupOrig
    );
    this.logger.info(
      `CustomField (from CustomFieldSetup): Found ${
        origCustomFieldSetups.length
      } to import.`
    );

    // order by entity type, field order
    const origQuestionSetups: QuestionsSetupOrig[] = await this.getOriginal(
      QuestionsSetupOrig
    ).orderByRaw('SiteSection, NumericId');
    this.logger.info(
      `CustomField (from QuestionsSetup): Found ${
        origQuestionSetups.length
      } to import.`
    );

    const existingCustomFields: CustomField[] = await this.newKnex(
      getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.customFieldType),
        'Customer'
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .catch(this.logger.error);

    const cfts: CustomFormTemplate[] = await this.getCustomFormTemplates(trx);

    await Bluebird.each(origCustomFieldSetups, async origCustomFieldSetup => {
      const existingCustomField = existingCustomFields.find(
        c =>
          c.entityType ===
            siteSectionEntityMap[origCustomFieldSetup.SiteSection] &&
          c.fieldLabel === origCustomFieldSetup.FieldLabel
      );
      if (existingCustomField) {
        this.logger.warn(
          `CustomField: "${
            existingCustomField.fieldLabel
          }" already exists for ${existingCustomField.entityType}.  ` +
            `Skipping.`,
          existingCustomField
        );
        return;
      }
      const entityType = siteSectionEntityMap[origCustomFieldSetup.SiteSection];
      if (!entityType) {
        this.logger.error(
          `CustomFieldSetup SiteSection not found`,
          origCustomFieldSetup
        );
        return;
      }

      let newRecord = Object.assign(new CustomField(), <Partial<CustomField>>{
        customerId: this.config.CUSTOMER_ID,
        customFieldType: 'Customer',
        entityType: entityType,
        dataType: customFieldDataTypeMap[origCustomFieldSetup.FieldType],
        inputType: customFieldInputTypeMap[origCustomFieldSetup.FieldType],
        fieldLabel: origCustomFieldSetup.FieldLabel,
        fieldOptions:
          origCustomFieldSetup.FieldType === 'D' &&
          origCustomFieldSetup.DefaultValue
            ? origCustomFieldSetup.DefaultValue.replace(/,/g, '|')
            : origCustomFieldSetup.DefaultValue,
        fieldOrder: origCustomFieldSetup.OrderId,
        isDeleted: origCustomFieldSetup.IsDeleted === 'Y',
      });
      const inserted = await this.newKnex(getTableName(CustomField))
        .transacting(trx)
        .insert(objectToSqlData(CustomField, newRecord, this.newKnex))
        .returning(
          getSqlFields2(CustomField, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<CustomField>(0);
      this.logger.verbose(
        `CustomField: imported type ${inserted.entityType} "${
          inserted.fieldLabel
        }"`,
        inserted
      );
    });

    const fixedQuestions = fixDuplicateQuestions(
      origQuestionSetups
    ) as QuestionsSetupOrig[];

    let fieldOrder = 1000;
    await Bluebird.each(fixedQuestions, async origQuestion => {
      const existingCustomField = existingCustomFields.find(
        c =>
          c.entityType === siteSectionEntityMap[origQuestion.SiteSection] &&
          c.fieldLabel === origQuestion.Question
      );
      if (existingCustomField) {
        this.logger.warn(
          `CustomField: "${
            existingCustomField.fieldLabel
          }" already exists for ${existingCustomField.entityType}.  ` +
            `Skipping.`,
          existingCustomField
        );
        return;
      }
      const entityType = siteSectionEntityMap[origQuestion.SiteSection];
      if (!entityType) {
        const err = `QuestionsSetup SiteSection not found`;
        this.logger.error(err, origQuestion);
        throw Error(err);
        return;
      }
      const cft = cfts.find(
        f => f.entityType === entityType && f.name.startsWith('Default ')
      );
      if (!cft) {
        throw Error(`Default Custom Form Template for ${entityType} not found`);
      }
      let newRecord = Object.assign(new CustomField(), <Partial<CustomField>>{
        customerId: this.config.CUSTOMER_ID,
        customFieldType: 'Customer',
        entityType: entityType,
        dataType: questionsSetupDataTypeMap[origQuestion.ControlType],
        inputType: questionsSetupInputTypeMap[origQuestion.ControlType],
        fieldLabel: origQuestion.Question,
        fieldOptions:
          origQuestion.ControlType === 'DropDown' && origQuestion.DefaultAnswer
            ? origQuestion.DefaultAnswer.replace(/,/g, '|')
            : origQuestion.DefaultAnswer,

        customFormTemplateId: cft.id,
        fieldOrder: ++fieldOrder,
      });
      const inserted = await this.newKnex(getTableName(CustomField))
        .transacting(trx)
        .insert(objectToSqlData(CustomField, newRecord, this.newKnex))
        .returning(
          getSqlFields2(CustomField, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<CustomField>(0);
      this.logger.verbose(
        `CustomField: imported type ${inserted.entityType} "${
          inserted.fieldLabel
        }"`,
        inserted
      );
    });
  }

  // import Construction Site inspection types
  async importInspectionTypes(trx: Knex.Transaction) {
    const origInspectionTypes = await this.getOriginalTable(InspectionTypeOrig);
    this.logger.info(
      `InspectionType: Found ${origInspectionTypes.length} to import.`
    );

    const existingInspectionTypes: InspectionType[] = await this.getInspectionTypes(
      trx
    );

    const csDefaultCft: CustomFormTemplate = await this.getCustomFormTemplates(
      trx
    )
      .where({
        [CustomFormTemplate.sqlField(
          f => f.entityType
        )]: EntityTypes.ConstructionSiteInspection,
      })
      .get<CustomFormTemplate>(0);

    if (
      !csDefaultCft ||
      csDefaultCft.name !== 'Default Construction Site Inspection'
    ) {
      throw Error(`Default Construction Site Inspection type not found`);
    }

    await Bluebird.each(origInspectionTypes, async origInspectionType => {
      const existingInspectionType = existingInspectionTypes.find(
        c => c.name === origInspectionType.InspectionTypeName
      );
      if (existingInspectionType) {
        this.logger.warn(
          `InspectionType: "${
            existingInspectionType.name
          }" already exists.  Skipping.`,
          existingInspectionType
        );
      } else {
        let newRecord = Object.assign(new InspectionType(), <Partial<
          InspectionType
        >>{
          customerId: this.config.CUSTOMER_ID,
          name: origInspectionType.InspectionTypeName,
          customFormTemplateId: csDefaultCft.id,
        });
        const inserted = await this.newKnex(getTableName(InspectionType))
          .transacting(trx)
          .insert(objectToSqlData(InspectionType, newRecord, this.newKnex))
          .returning(
            getSqlFields2(InspectionType, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<InspectionType>(0);
        this.logger.verbose(
          `InspectionType: imported "${inserted.name}"`,
          inserted
        );
      }
    });
  }

  async importControlMeasures(trx: Knex.Transaction) {
    const originalCMs: MinimumControlMeasureOrig[] = await this.origKnex(
      getTableName(MinimumControlMeasureOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `BmpControlMeasure: Found ${originalCMs.length} to import.`
    );

    const existingCMs: BmpControlMeasure[] = await this.newKnex(
      getTableName(BmpControlMeasure)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(BmpControlMeasure, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    const importRecords = await Bluebird.each(originalCMs, async originalCM => {
      let existingCM = existingCMs.find(
        f => f.name === originalCM.ControlMeasure
      );
      if (existingCM) {
        this.logger.warn(
          `BmpControlMeasure: "${existingCM.name}" already exists.  Skipping.`,
          existingCM
        );
        return;
      }
      const newCM: BmpControlMeasure = Object.assign(
        new BmpControlMeasure(),
        <Partial<BmpControlMeasure>>{
          customerId: this.config.CUSTOMER_ID,
          name: originalCM.ControlMeasure,
          originalId: originalCM.NumericID,
        }
      );

      const insertedRecord = await this.newKnex(getTableName(BmpControlMeasure))
        .transacting(trx)
        .insert(objectToSqlData(BmpControlMeasure, newCM, this.newKnex))
        .returning(
          getSqlFields2(BmpControlMeasure, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpControlMeasure>(0);
      this.logger.verbose(
        `BmpControlMeasure: imported "${insertedRecord.name}"`,
        insertedRecord
      );

      return insertedRecord;
    });

    return importRecords;
  }

  /**
   * Imports BMP Tasks and Activities
   *
   */
  async importBmpTasks() {
    const originalTasks: BestManagementPracticeOrig[] = await this.origKnex(
      getTableName(BestManagementPracticeOrig)
    ).catch(this.logger.error);
    this.logger.info(`BmpTask: Found ${originalTasks.length} to import.`);

    const existingCMs: BmpControlMeasure[] = await this.newKnex(
      getTableName(BmpControlMeasure)
    )
      .select(
        ...getSqlFields2(BmpControlMeasure, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);
    const existingBmpTasks: BmpTask[] = await this.newKnex(
      getTableName(BmpTask)
    )
      .select(
        ...getSqlFields2(BmpTask, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        BmpTask.getSqlField<BmpTask>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(BmpTask.getSqlField<BmpTask>(c => c.isDeleted), 0)
      .catch(this.logger.error);

    const importRecords = await Bluebird.each(
      originalTasks,
      async originalTask => {
        const cm = existingCMs.find(
          c => c.originalId === originalTask.ControlMeasureID
        );
        if (!cm) {
          this.logger.error(
            `BmpTask: Could not find BmpTask associated with ${
              originalTask.Title
            }`,
            originalTask
          );
          return;
        }
        const existingTask = existingBmpTasks.find(
          t => t.originalId === originalTask.NumericID
        );
        if (existingTask) {
          this.logger.warn(`BmpTask: ${existingTask.name} exists. Skipping.`);
          return;
        }
        const newTask: BmpTask = Object.assign(new BmpTask(), <Partial<
          BmpTask
        >>{
          customerId: this.config.CUSTOMER_ID,
          name: originalTask.Title,
          controlMeasureId: cm.id,
          originalId: originalTask.NumericID,
        });

        const insertedTask = await this.newKnex(getTableName(BmpTask))
          .insert(objectToSqlData(BmpTask, newTask, this.newKnex))
          .returning(
            getSqlFields2(BmpTask, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<BmpTask>(0);
        this.logger.verbose(
          `BmpTask: imported "${insertedTask.name}"`,
          insertedTask
        );

        // Add "Activities"
        await this.importActivities(
          insertedTask.id as number,
          originalTask.NumericID
        );

        return insertedTask;
      }
    );

    return importRecords;
  }

  async importActivities(taskId: number, originalTaskId: number) {
    // There are 3 tables to import from
    const implTasks: BMPImplementationTaskOrig[] = await this.origKnex(
      getTableName(BMPImplementationTaskOrig)
    ).where('ManagementPracticeID', originalTaskId);
    const bmpDatas: BMPDataOrig[] = await this.origKnex(
      getTableName(BMPDataOrig)
    ).where('ManagementPracticeID', originalTaskId);
    const measurableGoals: BMPMeasureableGoalOrig[] = await this.origKnex(
      getTableName(BMPMeasureableGoalOrig)
    ).where('ManagementPracticeID', originalTaskId);

    await Bluebird.each(implTasks, async implTask => {
      const newActivity: BmpActivity = Object.assign(
        new BmpActivity(),
        <Partial<BmpActivity>>{
          customerId: this.config.CUSTOMER_ID,
          taskId: taskId,
          dateAdded: this.toUtcDate(implTask.ImplementationDate),
          description: implTask.Description,
          completedDate: this.toUtcDate(implTask.ImpTaskCompleteDate),
          isCompleted: implTask.Implemented,
        }
      );
      const insertedTask = await this.newKnex(getTableName(BmpActivity))
        .insert(objectToSqlData(BmpActivity, newActivity, this.newKnex))
        .returning(
          getSqlFields2(BmpActivity, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivity>(0);
      this.logger.verbose(
        `BmpTask: imported "${insertedTask.description}"`,
        insertedTask
      );
    });

    await Bluebird.each(bmpDatas, async bmpData => {
      const newActivity: BmpActivity = Object.assign(
        new BmpActivity(),
        <Partial<BmpActivity>>{
          customerId: this.config.CUSTOMER_ID,
          taskId: taskId,
          dateAdded: this.toUtcDate(bmpData.DateEntered),
          description: bmpData.Activity,
        }
      );
      const insertedTask = await this.newKnex(getTableName(BmpActivity))
        .insert(objectToSqlData(BmpActivity, newActivity, this.newKnex))
        .returning(
          getSqlFields2(BmpActivity, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivity>(0);
      this.logger.verbose(
        `BmpTask: imported "${insertedTask.description}"`,
        insertedTask
      );
    });

    await Bluebird.each(measurableGoals, async goal => {
      const newActivity: BmpActivity = Object.assign(
        new BmpActivity(),
        <Partial<BmpActivity>>{
          customerId: this.config.CUSTOMER_ID,
          taskId: taskId,
          description: goal.Description,
          dueDate: this.toUtcDate(goal.GoalDueDate),
          isCompleted: goal.GoalMet,
          completedDate: this.toUtcDate(goal.DateGoalMet),
        }
      );
      const insertedTask = await this.newKnex(getTableName(BmpActivity))
        .insert(objectToSqlData(BmpActivity, newActivity, this.newKnex))
        .returning(
          getSqlFields2(BmpActivity, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivity>(0);
      this.logger.verbose(
        `BmpTask: imported "${insertedTask.description}"`,
        insertedTask
      );
    });
  }

  /**
   * check for existing Project and create if not exists
   *
   * @param {string} projectName
   * @param {Project[]} currentProjects
   *
   */
  async createProjectIfNotExists(
    projectName: string,
    currentProjects: Project[],
    trx: Knex.Transaction
  ) {
    let project = currentProjects.find(p => p.name === projectName);
    if (!project) {
      project = await this.createProject(projectName, trx);
      this.logger.verbose(`Project: Created project ${project.name}`, project);

      // add project to the list
      currentProjects.push(project);
    } else {
      this.logger.info(`Project: ${project.name} already exists`, project);
    }
    return project;
  }

  async createProject(projectName: string, trx: Knex.Transaction) {
    const projectData = objectToSqlData(
      Project,
      <Partial<Project>>{
        customerId: this.config.CUSTOMER_ID,
        name: projectName,
      },
      this.newKnex
    );
    const createdProject = await this.newKnex(Project.getTableName())
      .insert(projectData)
      .returning(
        getSqlFields2(Project, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .catch(this.logger.error)
      .get<Project>(0);

    return createdProject;
  }

  /**
   * check for existing Community and create if not exists
   *
   */
  async createCommunityIfNotExists(name: string, communities: Community[]) {
    if (!name) {
      return;
    }
    let community = communities.find(p => p.name === name);
    if (!community) {
      community = await this.createCommunity(name);
      this.logger.verbose(
        `Community: Created community ${community.name}`,
        community
      );
      communities.push(community);
    } else {
      this.logger.info(
        `Community: ${community.name} already exists`,
        community
      );
    }
    return community;
  }

  async createReceivingWaterIfNotExists(
    name: string,
    receivingWaters: ReceivingWater[]
  ) {
    if (!name) {
      return;
    }
    let rw = receivingWaters.find(p => p.name === name);
    if (!rw) {
      rw = await this.createReceivingWater(name);
      this.logger.verbose(
        `ReceivingWater: Created receiving water ${rw.name}`,
        rw
      );
      receivingWaters.push(rw);
    } else {
      this.logger.info(`ReceivingWater: ${rw.name} already exists`, rw);
    }
    return rw;
  }

  async createReceivingWater(name: string) {
    const data = objectToSqlData(
      ReceivingWater,
      <Partial<ReceivingWater>>{
        customerId: this.config.CUSTOMER_ID,
        name: name,
      },
      this.newKnex
    );
    const createdReceivingWater = await this.newKnex(
      ReceivingWater.getTableName()
    )
      .insert(data)
      .returning(
        getSqlFields2(ReceivingWater, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .catch(this.logger.error)
      .get<ReceivingWater>(0);
    return createdReceivingWater;
  }

  /**
   * check for existing Watershed and create if not exists
   *
   */
  async createWatershedIfNotExists(
    name: string,
    watersheds: Watershed[],
    isSubWatershed = false
  ) {
    if (!name) {
      return;
    }
    let ws;
    ws = !isSubWatershed
      ? watersheds.find(p => !p.isSubWatershed && p.name === name)
      : watersheds.find(p => p.isSubWatershed && p.name === name);
    if (!ws) {
      ws = await this.createWatershed(name, isSubWatershed);
      this.logger.verbose(`Watershed: Created community ${ws.name}`, ws);
      watersheds.push(ws);
    } else {
      this.logger.info(`Watershed: ${ws.name} already exists`, ws);
    }
    return ws;
  }

  async createWatershed(name: string, isSubWatershed = false) {
    const wsData = objectToSqlData(
      Watershed,
      <Partial<Watershed>>{
        customerId: this.config.CUSTOMER_ID,
        name: name,
        isSubWatershed: isSubWatershed,
      },
      this.newKnex
    );
    const createdWatershed = await this.newKnex(Watershed.getTableName())
      .insert(wsData)
      .returning(
        getSqlFields2(Watershed, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .catch(this.logger.error)
      .get<Watershed>(0);
    return createdWatershed;
  }

  async createCommunity(name: string) {
    const communityData = objectToSqlData(
      Community,
      <Partial<Community>>{
        customerId: this.config.CUSTOMER_ID,
        name: name,
      },
      this.newKnex
    );
    const createdCommunity = await this.newKnex(Community.getTableName())
      .insert(communityData)
      .returning(
        getSqlFields2(Community, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .catch(this.logger.error)
      .get<Community>(0);

    return createdCommunity;
  }

  async getContacts(trx: Knex.Transaction): Bluebird<Contact[]> {
    return await this.newKnex(Contact.getTableName())
      .transacting(trx)
      .select(
        getSqlFields2(Contact, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .where(
        Contact.getSqlField<Contact>(p => p.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Contact.getSqlField<Contact>(c => c.isDeleted), 0)
      .catch(this.logger.error);
  }

  async getOriginalTable<T>(ent: { new (): T }): Bluebird<T[]> {
    return await this.origKnex(getTableName(ent));
  }

  getOriginal(ent: { new () }) {
    return this.origKnex(getTableName(ent));
  }

  async getLookup<T>(
    ent: { new (): T },
    trx: Knex.Transaction,
    options?: {
      customerId: string;
    }
  ): Bluebird<T[]> {
    const qb = this.newKnex(getTableName(ent))
      .select(
        getSqlFields2(ent, { asProperty: true, includeInternalField: true })
      )
      .transacting(trx);
    if (options && options.customerId) {
      qb.where(getSqlField(ent, 'customerId'), options.customerId);
    }
    return await qb.catch(this.logger.error);
  }

  getTable(ent: { new () }, trx: Knex.Transaction) {
    const qb = this.newKnex(getTableName(ent))
      .transacting(trx)
      .select(
        getSqlFields2(ent, { asProperty: true, includeInternalField: true })
      );
    return qb;
  }

  /**
   * update custom fields from the CustomFieldSetup table
   *
   * @param {typeof Entity} Ent
   * @param {number} entityId
   * @param {CustomFieldSetupOrig[]} cfSetups - old table
   * @param {CustomField[]} customFields - new table
   * @param {any} originalRecord - record to look for CF# field
   *
   */
  async updateCustomFieldsFromCustomFieldSetup(
    Ent: typeof Entity,
    entityId: number,
    cfSetups: CustomFieldSetupOrig[],
    customFields: CustomField[],
    originalRecord:
      | ConstructionSiteOrig
      | FacilityOrig
      | OutfallOrig
      | StructureOrig,
    trx: Knex.Transaction
  ) {
    const cfvMap = cfSetups
      .map(cfSetup => {
        const cf = customFields.find(c => c.fieldLabel === cfSetup.FieldLabel);
        if (!cf) {
          this.logger.warn(
            `CustomField: Could not find custom field ${cfSetup.FieldLabel}`
          );
          return;
        }
        const id = cf.id;
        let value: string = originalRecord[`CF${cfSetup.FieldNumber}`];

        // handle checkbox type:
        // Yes -> "true", No -> "false"
        if (cfSetup.FieldType === 'C' && value) {
          switch (value.toLowerCase()) {
            case 'yes':
              value = 'true';
              break;
            case 'no':
              value = 'false';
              break;
          }
        }
        return <CustomFieldValueMap>{
          id,
          value,
        };
      })
      .filter(cf => cf);
    await this._customFieldRepo.updateCustomFieldsForEntity(
      Ent,
      entityId,
      cfvMap,
      this.config.CUSTOMER_ID,
      trx
    );
  }

  async updateCustomFieldsFromQuestions(
    EntityClass: typeof Entity,
    entity:
      | CitizenReport
      | ConstructionSiteInspection
      | FacilityInspection
      | OutfallInspection
      | StructureInspection,
    originalId: number,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const entityId = entity.id;
    const missingQuestions = [];
    const questions = await this.getQuestionsOrig(
      (<any>EntityClass).name,
      originalId
    );

    const fixedQuestions = fixDuplicateQuestions(questions) as QuestionOrig[];

    const cfvs = fixedQuestions
      .map(question => {
        // skip blank questions
        if (isBlank(question.Question)) {
          return;
        }
        const cf = customFields.find(c => c.fieldLabel === question.Question);
        if (!cf) {
          this.logger.warn(
            `CustomField: Could not find custom field ${question.Question}`
          );
          if (question.QuestionAnswer) {
            missingQuestions.push(
              `${question.Question}: ${question.QuestionAnswer}`
            );
          }
          return;
        } else {
          this.logger.verbose(`Found: ${question.QuestionAnswer}`);
        }

        let value = question.QuestionAnswer;

        // handle CheckBox type:
        if (question.ControlType === 'CheckBox') {
          value = question.Boolean ? 'true' : 'false';
        }
        if (isBlank(value)) {
          return;
        }
        return <CustomFieldValue>{
          customFieldId: cf.id,
          globalIdFk: entity.globalId,
          value,
        };
      })
      .filter(cf => cf); // skip empty

    // add to AdditionalInformation if there are missing questions
    if (missingQuestions.length) {
      const joined = missingQuestions.join('\r\n');
      if (EntityClass === CitizenReport) {
        const addInfo = CitizenReport.getSqlField<CitizenReport>(
          c => c.additionalInformation
        );
        await this.newKnex(CitizenReport.getTableName())
          .transacting(trx)
          .update(addInfo, this.newKnex.raw('?? + ?', [addInfo, joined]))
          .where(
            CitizenReport.getSqlField<CitizenReport>(c => c.customerId),
            this.config.CUSTOMER_ID
          )
          .where(CitizenReport.getSqlField<CitizenReport>(c => c.id), entityId);
      } else if (
        EntityClass === ConstructionSiteInspection ||
        EntityClass === FacilityInspection ||
        EntityClass === OutfallInspection ||
        EntityClass === StructureInspection
      ) {
        const addInfo = EntityClass.getSqlField<Inspection>(
          c => c.additionalInformation
        );
        await this.newKnex(EntityClass.getTableName())
          .transacting(trx)
          .update(
            addInfo,
            this.newKnex.raw('?? + ?', [addInfo, `\r\n${joined}`])
          )
          .where(EntityClass.getSqlField<Inspection>(c => c.id), entityId);
      }
    }

    const rowsToInsert = cfvs.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );
    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }

  async getQuestionsOrig(
    entity: string,
    recordId: number
  ): Bluebird<QuestionOrig[]> {
    // gets site section from EntityType
    const siteSection = Object.keys(siteSectionEntityMap).find(
      section => siteSectionEntityMap[section] === entity
    );
    return await this.origKnex(getTableName(QuestionOrig))
      .where(propertyNameFor<QuestionOrig>(q => q.SiteSection), siteSection)
      .where(propertyNameFor<QuestionOrig>(q => q.RecordID), recordId);
  }

  async importFile(
    fileOrig: BmpFileOrig,
    EntityWithFile: typeof Entity,
    entityId: number,
    trx: Knex.Transaction
  ) {
    if (!fileOrig.FilePath) {
      this.logger.warn(`BmpFile FilePath empty`, fileOrig);
      return;
    }
    const originalFilePath = path.join(
      this.config.ORIGINAL_FILES_BASE_PATH,
      fileOrig.FilePath.replace('~/wbin/', '').replace(/\\/g, '/')
    );

    if (!fs.existsSync(originalFilePath)) {
      this.logger.warn(`File: ${originalFilePath} not not exist`);
      return;
    }

    const mimeType = mime.lookup(originalFilePath);
    const unique = uuid.v4();
    const extension = path.extname(originalFilePath);
    const origFileName = path.basename(originalFilePath);

    const existingFile = await this.getTable(FileEntity, trx)
      .where(
        FileEntity.getSqlField<FileEntity>(f => f.originalFileName),
        origFileName
      )
      .where(
        FileEntity.getSqlField<FileEntity>(f => f.customerId),
        this.config.CUSTOMER_ID
      )
      .get<FileEntity>(0);
    if (existingFile) {
      this.logger.warn(
        `File exists: ${fileOrig.NumericID}: ${originalFilePath}`
      );
      return;
    }

    const fileName = `${unique}${extension}`;
    const fullFilePath = `${this.getCustomerPath()}/${fileName}`;
    let tnName: string = null;

    fs.copySync(originalFilePath, fullFilePath);

    const fileStat = fs.statSync(fullFilePath);

    // thumbnail version
    if (mimeType.startsWith('image')) {
      tnName = `${unique}_tn.jpg`;

      // Handle BMP differently
      if (extension === '.bmp') {
        const bmpDecoded = bmpjs.decode(fs.readFileSync(originalFilePath));
        await sharp(bmpDecoded.data, {
          raw: {
            height: bmpDecoded.height,
            width: bmpDecoded.width,
            channels: 4,
          },
        })
          .resize(Defaults.THUMBNAIL_MAX_WIDTH, Defaults.THUMBNAIL_MAX_HEIGHT)
          .max()
          .jpeg({
            quality: Defaults.THUMBNAIL_QUALITY,
          })
          .toFile(`${this.getCustomerPath()}/${tnName}`)
          .catch(err =>
            this.logger.warn(`File: Error resizing file ${originalFilePath}`)
          );
      } else {
        await sharp(originalFilePath)
          .resize(Defaults.THUMBNAIL_MAX_WIDTH, Defaults.THUMBNAIL_MAX_HEIGHT)
          .max()
          .jpeg({
            quality: Defaults.THUMBNAIL_QUALITY,
          })
          .toFile(`${this.getCustomerPath()}/${tnName}`)
          .catch(err =>
            this.logger.warn(`File: Error resizing file ${originalFilePath}`)
          );
      }
    }
    const fileEntity: FileEntity = Object.assign(new FileEntity(), <Partial<
      FileEntity
    >>{
      customerId: this.config.CUSTOMER_ID,
      entityType: (<any>EntityWithFile).name,
      originalFileName: origFileName,
      fileSize: fileStat.size,
      fileName: fileName,
      thumbnailName: tnName,
      mimeType: mimeType,
      addedDate: this.toUtcDate(fileOrig.Created),
      description: fileOrig.Description,
    });
    await this._fileRepo.insert(
      EntityWithFile,
      entityId,
      this.config.CUSTOMER_ID,
      fileEntity,
      trx
    );
  }

  async validateOutfallConditions(trx: Knex.Transaction) {
    const origConditions = await this.getOriginalTable(ConditionOrig);
    const origConditionNames = origConditions.map(c => c.ConditionName);
    const difference = _.difference(origConditionNames, outfallConditions);
    if (difference.length) {
      this.logger.warn(`Outfall Conditions don't match`, {
        originalVersion: origConditionNames,
        newVersion: outfallConditions,
        difference: difference,
      });
    } else {
      this.logger.info(`Outfall Conditions OK`);
    }
    return this;
  }

  async validateObstructionSeverities(trx: Knex.Transaction) {
    const origObs = await this.getOriginalTable(ObstructionSeverityOrig);
    const origObsNames = origObs.map(c => c.ObstructionSeverityName);
    const difference = _.difference(origObsNames, obstructionSeverities);
    if (difference.length) {
      this.logger.warn(`Obstruction Severities don't match`, {
        originalVersion: origObsNames,
        newVersion: obstructionSeverities,
        difference: difference,
      });
    } else {
      this.logger.info(`Obstruction Severities OK`);
    }

    return this;
  }

  async validateOutfallMaterials(trx: Knex.Transaction) {
    const origMaterials = await this.getOriginalTable(OutfallMaterialOrig);
    const origMaterialNames = origMaterials.map(c => c.OutfallMaterialName);
    const outfallMaterials = await this.getLookup(OutfallMaterial, trx);
    const outfallMaterialNames = outfallMaterials.map(m => m.name);
    const difference = _.difference(origMaterialNames, outfallMaterialNames);
    if (difference.length) {
      this.logger.warn(`Outfall Materials don't match`, {
        originalVersion: origMaterialNames,
        newVersion: outfallMaterialNames,
        difference: difference,
      });
    } else {
      this.logger.info(`Outfall Materials OK`);
    }
    return this;
  }

  async validateOutfallTypes(trx: Knex.Transaction) {
    const origTypes = await this.getOriginalTable(OutfallTypeOrig);
    const origNames = origTypes.map(c => c.OutfallTypeName);
    const newTypes = await this.getLookup(OutfallType, trx);
    const newNames = newTypes.map(m => m.name);
    const difference = _.difference(origNames, newNames);
    if (difference.length) {
      this.logger.warn(`Outfall Types don't match`, {
        originalVersion: origNames,
        newVersion: newNames,
        difference: difference,
      });
    } else {
      this.logger.info(`Outfall Types OK`);
    }
    return this;
  }

  async validatePermitStatuses(trx: Knex.Transaction) {
    const origPermitStatuses = await this.getOriginalTable(PermitStatusOrig);
    const origNames = origPermitStatuses.map(c => c.PermitStatusName);
    const newNames = Object.keys(permitStatus).map(k => permitStatus[k]);
    const difference = _.difference(origNames, newNames);
    if (difference.length) {
      this.logger.warn(`Permit Status don't match`, {
        originalVersion: origNames,
        newVersion: newNames,
        difference: difference,
      });
    } else {
      this.logger.info(`Permit Status OK`);
    }
    return this;
  }

  async validateSectors(trx: Knex.Transaction) {
    const origSectors = await this.getOriginalTable(SectorOrig);
    const origNames = origSectors.map(c => c.SectorName);
    const newSectors = await this.getLookup(Sector, trx);
    const newNames = newSectors.map(m => m.name);
    const difference = _.difference(origNames, newNames);
    if (difference.length) {
      this.logger.warn(`Sectors don't match`, {
        originalVersion: origNames,
        newVersion: newNames,
        difference: difference,
      });
    } else {
      this.logger.info(`Sectors OK`);
    }
    return this;
  }

  async getDefaultCustomFormTemplate(
    entityType: string,
    trx: Knex.Transaction
  ) {
    return await this.newKnex(CustomFormTemplate.tableName)
      .transacting(trx)
      .where({
        [CustomFormTemplate.sqlField(f => f.entityType)]: entityType,
        [CustomFormTemplate.sqlField(f => f.isSystem)]: 1,
        [CustomFormTemplate.sqlField(f => f.customerId)]: this.config
          .CUSTOMER_ID,
      })
      .select(
        getSqlFields2(CustomFormTemplate, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .get<CustomFormTemplate>(0);
  }

  async getDefaultInspectionType(entityType: string, trx: Knex.Transaction) {
    const typeMap = {
      [EntityTypes.ConstructionSiteInspection]:
        'Default Construction Site Inspection',
      [EntityTypes.FacilityInspection]: 'Default Facility Inspection',
      [EntityTypes.OutfallInspection]: 'Default Outfall Inspection',
      [EntityTypes.StructureInspection]: 'Default Structure Inspection',
      [EntityTypes.CitizenReport]: 'Default Citizen Report',
      [EntityTypes.IllicitDischarge]: 'Default Illicit Discharge',
    };
    return await this.newKnex(InspectionType.tableName)
      .transacting(trx)
      .where({
        [InspectionType.sqlField(f => f.entityType)]: entityType,
        [InspectionType.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
        [InspectionType.sqlField(f => f.name)]: typeMap[entityType],
      })
      .select(
        getSqlFields2(InspectionType, {
          asProperty: true,
          includeInternalField: true,
        })
      )
      .get<InspectionType>(0);
  }

  async validateConstructionSiteImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(ConstructionSite.tableName)
      .transacting(trx)
      .where({
        [ConstructionSite.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(
      getTableName(ConstructionSiteOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Construction Sites new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateConstructionSiteInspectionImport(trx: Knex.Transaction) {
    const customerId = this.config.CUSTOMER_ID;
    const newCount = await this.newKnex(ConstructionSiteInspection.tableName)
      .transacting(trx)
      .whereIn(
        ConstructionSiteInspection.sqlField(f => f.constructionSiteId),
        function(this: Knex.QueryBuilder) {
          this.from(ConstructionSite.tableName)
            .select(ConstructionSite.sqlField(f => f.id))
            .where({
              [ConstructionSite.sqlField(f => f.customerId)]: customerId,
              [ConstructionSite.sqlField(f => f.isDeleted)]: 0,
            });
        }
      )
      .where({
        [ConstructionSiteInspection.sqlField(f => f.isDeleted)]: 0,
      })
      .count('* as count')
      .get<any>(0);

    const originalCount = await this.origKnex(
      getTableName(ConstructionSiteInspectionOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Construction Site Inspections new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateFacilityInspectionImport(trx: Knex.Transaction) {
    const customerId = this.config.CUSTOMER_ID;
    const newCount = await this.newKnex(FacilityInspection.tableName)
      .transacting(trx)
      .whereIn(FacilityInspection.sqlField(f => f.facilityId), function(
        this: Knex.QueryBuilder
      ) {
        this.from(Facility.tableName)
          .select(Facility.sqlField(f => f.id))
          .where({
            [Facility.sqlField(f => f.customerId)]: customerId,
            [Facility.sqlField(f => f.isDeleted)]: 0,
          });
      })
      .where({
        [FacilityInspection.sqlField(f => f.isDeleted)]: 0,
      })
      .count('* as count')
      .get<any>(0);

    const originalCount = await this.origKnex(
      getTableName(FacilityInspectionOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Facility Inspections new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateOutfallInspectionImport(trx: Knex.Transaction) {
    const customerId = this.config.CUSTOMER_ID;
    const newCount = await this.newKnex(OutfallInspection.tableName)
      .transacting(trx)
      .whereIn(OutfallInspection.sqlField(f => f.outfallId), function(
        this: Knex.QueryBuilder
      ) {
        this.from(Outfall.tableName)
          .select(Outfall.sqlField(f => f.id))
          .where({
            [Outfall.sqlField(f => f.customerId)]: customerId,
            [Outfall.sqlField(f => f.isDeleted)]: 0,
          });
      })
      .where({
        [OutfallInspection.sqlField(f => f.isDeleted)]: 0,
      })
      .count('* as count')
      .get<any>(0);

    const originalCount = await this.origKnex(
      getTableName(OutfallInspectionOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Outfall Inspections new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateStructureInspectionImport(trx: Knex.Transaction) {
    const customerId = this.config.CUSTOMER_ID;
    const newCount = await this.newKnex(StructureInspection.tableName)
      .transacting(trx)
      .whereIn(StructureInspection.sqlField(f => f.structureId), function(
        this: Knex.QueryBuilder
      ) {
        this.from(Structure.tableName)
          .select(Structure.sqlField(f => f.id))
          .where({
            [Structure.sqlField(f => f.customerId)]: customerId,
            [Structure.sqlField(f => f.isDeleted)]: 0,
          });
      })
      .where({
        [StructureInspection.sqlField(f => f.isDeleted)]: 0,
      })
      .count('* as count')
      .get<any>(0);

    const originalCount = await this.origKnex(
      getTableName(StructureInspectionOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Structure Inspections new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateFacilityImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(Facility.tableName)
      .transacting(trx)
      .where({
        [Facility.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(getTableName(FacilityOrig))
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Facilities new/old: ${newCount.count}/${originalCount.count}`
    );
  }

  async validateOutfallImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(Outfall.tableName)
      .transacting(trx)
      .where({
        [Outfall.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(getTableName(OutfallOrig))
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Outfalls new/old: ${newCount.count}/${originalCount.count}`
    );
  }

  async validateStructureImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(Structure.tableName)
      .transacting(trx)
      .where({
        [Structure.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(getTableName(StructureOrig))
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Structures new/old: ${newCount.count}/${originalCount.count}`
    );
  }

  async validateCitizenReportImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(CitizenReport.tableName)
      .transacting(trx)
      .where({
        [CitizenReport.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(getTableName(CitizenReportOrig))
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Citizen Reports new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  async validateIllicitDischargeImport(trx: Knex.Transaction) {
    const newCount = await this.newKnex(IllicitDischarge.tableName)
      .transacting(trx)
      .where({
        [IllicitDischarge.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
      })
      .count('* as count')
      .get<any>(0);
    const originalCount = await this.origKnex(
      getTableName(IllicitDischargeOrig)
    )
      .count('* as count')
      .get<any>(0);

    this.logger.info(
      `Imported Illicit Discharges new/old: ${newCount.count}/${
        originalCount.count
      }`
    );
  }

  ensureFileStoragePath() {
    if (!fs.existsSync(this.config.FILE_STORAGE_PATH)) {
      const err = `FILE_STORAGE_PATH: ${
        this.config.FILE_STORAGE_PATH
      } does not exist`;
      this.logger.warn(err);
    }
    this.createCustomerPath();

    if (!fs.existsSync(this.config.ORIGINAL_FILES_BASE_PATH)) {
      const err = `ORIGINAL_FILES_PATH: ${
        this.config.ORIGINAL_FILES_BASE_PATH
      } does not exist`;
      this.logger.error(err);
      throw Error(err);
    }
  }

  getCustomerPath() {
    const customerPath = path.join(
      this.config.FILE_STORAGE_PATH,
      this.config.CUSTOMER_ID
    );
    if (!fs.existsSync(customerPath)) {
      fs.mkdirSync(customerPath);
    }
    return customerPath;
  }

  createCustomerPath() {
    const customerPath = path.join(
      this.config.FILE_STORAGE_PATH,
      this.config.CUSTOMER_ID
    );
    if (!fs.existsSync(customerPath)) {
      fs.mkdirSync(customerPath);
    }
    return this;
  }

  toUtcDate(dateField) {
    return moment(dateField).isValid()
      ? moment(dateField)
          .utc()
          .toDate()
      : null;
  }

  getInspectionTypes(trx?: Knex.Transaction) {
    const qb = this.newKnex(getTableName(InspectionType))
      .transacting(trx)
      .select(
        ...getSqlFields2(InspectionType, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [InspectionType.getSqlField<InspectionType>(c => c.customerId)]: this
          .config.CUSTOMER_ID,
        [InspectionType.getSqlField<InspectionType>(c => c.isDeleted)]: 0,
      });
    if (trx) {
      qb.transacting(trx);
    }
    return qb;
  }

  getCustomFormTemplates(trx?: Knex.Transaction) {
    const qb = this.newKnex(CustomFormTemplate.tableName)

      .select(getSqlFields2(CustomFormTemplate, { asProperty: true }))
      .where({
        [CustomFormTemplate.sqlField(f => f.customerId)]: this.config
          .CUSTOMER_ID,
        [CustomFormTemplate.sqlField(f => f.isDeleted)]: 0,
      });
    if (trx) {
      qb.transacting(trx);
    }

    return qb;
  }
}

function isBlank(str: string) {
  return str === null || str === undefined || !str.trim();
}

function fixDuplicateQuestions(
  questions: (QuestionsSetupOrig | QuestionOrig)[]
) {
  const questionCounts: any = {};
  return _.map(questions, question => {
    if (questionCounts[question.Question]) {
      questionCounts[question.Question]++;
      question.Question += ` - ${questionCounts[question.Question]}`;
    } else {
      questionCounts[question.Question] = 1;
    }
    return question;
  });
}
