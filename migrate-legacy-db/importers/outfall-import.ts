import { inject, injectable } from 'inversify';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  CustomFieldSetupOrig,
  OutfallOrig,
  DistanceUnitsOrig,
  OutfallTypeOrig,
  OutfallMaterialOrig,
  ConditionOrig,
  ObstructionSeverityOrig,
  OutfallInspectionOrig,
  InspectionColorOrig,
  InspectionClarityOrig,
  InspectionOdorOrig,
  InspectionFoamOrig,
  InspectionSheenOrig,
  InspectionSusSolidOrig,
  InspectionFloatSolidOrig,
  InspectionSetSolidOrig,
  BmpFileOrig,
  FileOwnerTypeOrig,
  InspectorOrig,
  Config,
} from '../models';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  CustomField,
  Outfall,
  outfallConditions,
  obstructionSeverities,
  OutfallInspection,
  OutfallMaterial,
  OutfallType,
  CustomFieldValue,
} from '../../src/models';
import {
  EntityTypes,
  objectToSqlData,
  propertyNameFor,
} from '../../src/shared';
import { siteSectionEntityMap, fileOwnerTypeToEntityMap } from '../constants';
import * as Bluebird from 'bluebird';
import * as _ from 'lodash';
import { numberToString } from '../utils/index';

@injectable()
export class OutfallImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    // private _contactRepo: ContactRepository,
    // private _customFieldRepo: CustomFieldRepository,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const originalOutfalls: OutfallOrig[] = await this.origKnex(
      dh.getTableName(OutfallOrig)
    ).catch(this.logger.error);
    this.logger.info(`Outfall: Found ${originalOutfalls.length} to import.`);

    const distanceUnits = await this._commonImport.getOriginalTable(
      DistanceUnitsOrig
    );
    const outfallTypesOrig = await this._commonImport.getOriginalTable(
      OutfallTypeOrig
    );
    const outfallMaterialsOrig = await this._commonImport.getOriginalTable(
      OutfallMaterialOrig
    );

    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );
    const outfallMaterials: OutfallMaterial[] = await this._commonImport.getLookup(
      OutfallMaterial,
      trx
    );
    const outfallTypes: OutfallType[] = await this._commonImport.getLookup(
      OutfallType,
      trx
    );

    const conditionsOrig = await this._commonImport.getOriginalTable(
      ConditionOrig
    );
    const obstructionsOrig = await this._commonImport.getOriginalTable(
      ObstructionSeverityOrig
    );

    const inspectorsOrig = await this._commonImport.getOriginalTable(
      InspectorOrig
    );

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const outfallFileOwnerType = fileOwnerTypes.find(
      f => fileOwnerTypeToEntityMap[f.FileOwnerTypeName] === EntityTypes.Outfall
    );
    if (!outfallFileOwnerType) {
      this.logger.error(`Outfall: Could not find FileOwnerType`);
      return;
    }

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.entityType),
        EntityTypes.Outfall
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(CustomField.getSqlField<CustomField>(c => c.isDeleted), 0);
    const origCFs = await this._commonImport.getOriginalTable(
      CustomFieldSetupOrig
    );
    const origOutfallCFs = origCFs.filter(
      c => siteSectionEntityMap[c.SiteSection] === EntityTypes.Outfall
    );

    const existingOutfalls: Outfall[] = await this.newKnex(
      dh.getTableName(Outfall)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(Outfall, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        Outfall.getSqlField<Outfall>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Outfall.getSqlField<Outfall>(c => c.isDeleted), 0)
      .catch(this.logger.error);

    const importRecordsP = await Bluebird.each(
      originalOutfalls,
      async originalOutfall => {
        let existingOutfall = existingOutfalls.find(
          f => f.originalId === originalOutfall.NumericID
        );
        if (existingOutfall) {
          this.logger.warn(
            `Outfall: "${
              existingOutfall.originalId
            }" already exists.  Skipping.`,
            existingOutfall
          );
          return;
        }

        const community = communities.find(
          c => c.originalId === originalOutfall.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalOutfall.Watershed
        );
        const subWatershed = watersheds.find(
          w => w.isSubWatershed && w.originalId === originalOutfall.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalOutfall.ReceivingStream
        );

        let widthFactor = 1;
        let heightFactor = 1;
        let sizeFactor = 1;
        const widthUnit = distanceUnits.find(
          u => u.NumericID === originalOutfall.WidthUnits
        );
        if (widthUnit && widthUnit.DistanceUnitName === 'Feet') {
          widthFactor = 12;
        }
        const heightUnit = distanceUnits.find(
          u => u.NumericID === originalOutfall.HeightUnits
        );
        if (heightUnit && heightUnit.DistanceUnitName === 'Feet') {
          heightFactor = 12;
        }
        const sizeUnit = distanceUnits.find(
          u => u.NumericID === originalOutfall.SizeUnits
        );
        if (sizeUnit && sizeUnit.DistanceUnitName === 'Feet') {
          sizeFactor = 12;
        }

        // ComplianceStatus
        let complianceStatus = 'Not Screened';
        if (originalOutfall.Acceptable) {
          complianceStatus = 'Compliant';
        } else if (originalOutfall.Discharging || originalOutfall.Illicit) {
          complianceStatus = 'Illicit Discharge';
        }

        const obstructionOrig = obstructionsOrig.find(
          o => o.NumericID === originalOutfall.Obstruction
        );
        const obstruction = obstructionSeverities.find(
          o =>
            obstructionOrig &&
            obstructionOrig.ObstructionSeverityName &&
            o === obstructionOrig.ObstructionSeverityName
        );
        let additionalInformation = null;
        if (!obstruction && obstructionOrig) {
          this.logger.warn(
            `Outfall Obstruction: cannot map ${
              obstructionOrig.ObstructionSeverityName
            }`
          );
          additionalInformation = [
            additionalInformation,
            `Obstruction Severity: ${obstructionOrig.ObstructionSeverityName}`,
          ].join('\r\n');
        }

        let newOutfall: Outfall = Object.assign(new Outfall(), <Partial<
          Outfall
        >>{
          originalId: originalOutfall.NumericID,
          customerId: this.config.CUSTOMER_ID,
          trackingId: originalOutfall.TrackingID,
          dateAdded: this._commonImport.toUtcDate(originalOutfall.DateEntered),
          location: originalOutfall.OutfallLocation,
          outfallWidthIn: +originalOutfall.OutfallWidth
            ? +originalOutfall.OutfallWidth * widthFactor
            : null,
          outfallHeightIn: +originalOutfall.OutfallHeight
            ? +originalOutfall.OutfallHeight * heightFactor
            : null,
          outfallSizeIn: +originalOutfall.OutfallSize
            ? +originalOutfall.OutfallSize * sizeFactor
            : null,
          nearAddress: originalOutfall.NearAddress,

          communityId: community ? community.id : null,
          watershedId: watershed ? watershed.id : null,
          subwatershedId: subWatershed ? subWatershed.id : null,
          receivingWatersId: receivingWater ? receivingWater.id : null,

          complianceStatus: complianceStatus,

          lat: originalOutfall.GISLatitude,
          lng: originalOutfall.GISLongitude,

          obstruction: obstruction,
          additionalInformation: additionalInformation,
        });

        const conditionOrig = conditionsOrig.find(
          c => c.NumericID === originalOutfall.Condition
        );
        const condition = outfallConditions.find(
          c =>
            conditionOrig &&
            conditionOrig.ConditionName &&
            c.toLowerCase() === conditionOrig.ConditionName.toLowerCase()
        );
        if (condition) {
          newOutfall.condition = condition;
        } else if (!condition && conditionOrig) {
          // Per Ty: Poor and Fair = Needs Repairs
          if (
            conditionOrig.ConditionName &&
            _.includes(
              ['poor', 'fair'],
              conditionOrig.ConditionName.toLowerCase()
            )
          ) {
            newOutfall.condition = 'Needs Repairs';
            this.logger.warn(
              `Outfall Condition: ${
                conditionOrig.ConditionName
              } mapped to "Needs Repairs".`
            );
          }
          this.logger.warn(
            `Outfall Condition: cannot map ${
              conditionOrig.ConditionName
            }.  Added to Additional Info.`
          );
          newOutfall.additionalInformation = [
            newOutfall.additionalInformation,
            `Type: ${conditionOrig.ConditionName}`,
          ].join('\r\n');
        }

        const outfallTypeOrig = outfallTypesOrig.find(
          t => t.NumericID === originalOutfall.OutfallType
        );
        let outfallType: OutfallType = null;
        if (outfallTypeOrig && outfallTypeOrig.OutfallTypeName) {
          outfallType = outfallTypes.find(
            t =>
              t.name &&
              t.name.toLowerCase() ===
                outfallTypeOrig.OutfallTypeName.toLowerCase()
          );
          if (!outfallType) {
            this.logger.warn(
              `Outfall: Type: Could not map Type ${
                outfallTypeOrig.OutfallTypeName
              }.  ` + `Setting to other and adding to additional information.`
            );
            newOutfall.additionalInformation = [
              newOutfall.additionalInformation,
              `Type: ${outfallTypeOrig.OutfallTypeName}`,
            ].join('\r\n');
            newOutfall.outfallType = 'Other';
          } else {
            newOutfall.outfallType = outfallType.name;
          }
        }

        const materialOrig = outfallMaterialsOrig.find(
          m => m.NumericID === originalOutfall.OutfallMaterial
        );
        let material: OutfallMaterial = null;
        if (materialOrig && materialOrig.OutfallMaterialName) {
          material = outfallMaterials.find(
            m =>
              m.name &&
              m.name.toLowerCase() ===
                materialOrig.OutfallMaterialName.toLowerCase()
          );
          if (!material) {
            this.logger.warn(
              `Outfall: OutfallMaterial: Could not map Material ${
                materialOrig.OutfallMaterialName
              }` + `Setting to other and adding to additional information.`
            );
            newOutfall.additionalInformation = [
              newOutfall.additionalInformation,
              `Material: ${materialOrig.OutfallMaterialName}`,
            ].join('\r\n');
            newOutfall.material = 'Other';
          } else {
            newOutfall.material = material.name;
          }
        }

        if (originalOutfall.OutfallDescription) {
          newOutfall.additionalInformation = [
            newOutfall.additionalInformation,
            originalOutfall.OutfallDescription,
          ].join('\r\n');
        }
        if (originalOutfall.Comment) {
          newOutfall.additionalInformation = [
            newOutfall.additionalInformation,
            originalOutfall.Comment,
          ].join('\r\n');
        }

        // Observer
        if (originalOutfall.Observer > 0) {
          const obs = inspectorsOrig.find(
            i => i.NumericID === originalOutfall.Observer
          );
          if (obs) {
            newOutfall.additionalInformation = [
              newOutfall.additionalInformation,
              `Inspector: ${obs.InspectorName}`,
            ].join('\r\n');
          }
        }

        const insertedOutfall = await this.newKnex(dh.getTableName(Outfall))
          .transacting(trx)
          .insert(objectToSqlData(Outfall, newOutfall, this.newKnex))
          .returning(
            dh.getSqlFields2(Outfall, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Outfall>(0);
        this.logger.verbose(
          `Outfall: imported OriginalId ${insertedOutfall.originalId}`,
          insertedOutfall
        );

        // Custom Fields
        await this._commonImport.updateCustomFieldsFromCustomFieldSetup(
          Outfall,
          insertedOutfall.id,
          origOutfallCFs,
          customFields,
          originalOutfall,
          trx
        );

        // Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            outfallFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            insertedOutfall.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`Outfall: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            Outfall,
            insertedOutfall.id,
            trx
          );
        });
      }
    );

    return importRecordsP;
  }

  async importInspections(trx: Knex.Transaction) {
    const originalInspections: OutfallInspectionOrig[] = await this.origKnex(
      dh.getTableName(OutfallInspectionOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `OutfallInspection: Found ${originalInspections.length} to import.`
    );

    const outfalls: Outfall[] = await this.newKnex(dh.getTableName(Outfall))
      .transacting(trx)
      .where(
        Outfall.getSqlField<Outfall>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(
        dh.getSqlFields2<Outfall>(Outfall, {
          includeInternalField: true,
          asProperty: true,
        })
      );

    const selectOutfallId = this.newKnex(dh.getTableName(Outfall))
      .transacting(trx)
      .where(
        Outfall.getSqlField<Outfall>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(Outfall.getSqlField<Outfall>(c => c.id));
    const existingInspections: OutfallInspection[] = await this.newKnex(
      dh.getTableName(OutfallInspection)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(OutfallInspection, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .whereIn(
        OutfallInspection.getSqlField<OutfallInspection>(c => c.outfallId),
        selectOutfallId
      )
      .where(
        OutfallInspection.getSqlField<OutfallInspection>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    const inspectorsOrig = await this._commonImport.getOriginalTable(
      InspectorOrig
    );

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const oiFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.OutfallInspection
    );
    if (!oiFileOwnerType) {
      this.logger.error(`OutfallInspection: Could not find FileOwnerType`);
      return;
    }

    const colors = await this._commonImport.getOriginalTable(
      InspectionColorOrig
    );
    const clarities = await this._commonImport.getOriginalTable(
      InspectionClarityOrig
    );
    const odors = await this._commonImport.getOriginalTable(InspectionOdorOrig);
    const foams = await this._commonImport.getOriginalTable(InspectionFoamOrig);
    const sheens = await this._commonImport.getOriginalTable(
      InspectionSheenOrig
    );
    const susSolids = await this._commonImport.getOriginalTable(
      InspectionSusSolidOrig
    );
    const floatSolids = await this._commonImport.getOriginalTable(
      InspectionFloatSolidOrig
    );
    const setSolids = await this._commonImport.getOriginalTable(
      InspectionSetSolidOrig
    );

    const outfallInspectionType = await this._commonImport.getDefaultInspectionType(
      EntityTypes.OutfallInspection,
      trx
    );

    const outfallCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.OutfallInspection,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: outfallCft.id,
        [CustomField.sqlField(
          c => c.entityType
        )]: EntityTypes.OutfallInspection,
        [CustomField.sqlField(c => c.customerId)]: this.config.CUSTOMER_ID,
        [CustomField.sqlField(c => c.isDeleted)]: 0,
      });

    await Bluebird.each(originalInspections, async originalInspection => {
      let existingInspection = existingInspections.find(
        f => f.originalId === originalInspection.NumericID
      );
      if (existingInspection) {
        this.logger.warn(
          `OutfallInspection: OriginalId "${
            existingInspection.originalId
          }" already exists.  Skipping.`,
          existingInspection
        );
        return;
      }
      // find the Outfall
      const outfall = outfalls.find(
        cs => cs.originalId === originalInspection.OutfallID
      );
      if (!outfall) {
        this.logger.error(
          `OutfallInspection: Could not find original id: ${
            originalInspection.OutfallID
          }`
        );
        return;
      }

      let dryOrWet: string = null;
      if (originalInspection.DryWeather) {
        dryOrWet = 'Dry';
      } else if (originalInspection.WetWeather) {
        dryOrWet = 'Wet';
      }
      const color = colors.find(
        c => c.NumericID === originalInspection.ColorID
      );
      const clarity = clarities.find(
        c => c.NumericID === originalInspection.ClarityID
      );
      const odor = odors.find(c => c.NumericID === originalInspection.OdorID);
      const foam = foams.find(c => c.NumericID === originalInspection.FoamID);
      const sheen = sheens.find(
        c => c.NumericID === originalInspection.SheenID
      );
      const susSolid = susSolids.find(
        c => c.NumericID === originalInspection.SustainedSolidsID
      );
      const floatSolid = floatSolids.find(
        c => c.NumericID === originalInspection.FloatingSolidsID
      );
      const setSolid = setSolids.find(
        c => c.NumericID === originalInspection.SetSolidsID
      );

      let complianceStatus = 'Not Screened';
      // ComplianceStatus
      if (originalInspection.Acceptable) {
        complianceStatus = 'Compliant';
      } else if (originalInspection.Discharging || originalInspection.Illicit) {
        complianceStatus = 'Illicit Discharge';
      }

      let newInspection: OutfallInspection = Object.assign(
        new OutfallInspection(),
        <Partial<OutfallInspection>>{
          originalId: originalInspection.NumericID,
          outfallId: outfall.id,
          inspectionTypeId: outfallInspectionType.id,
          customFormTemplateId: outfallCft.id,
          createdDate: this._commonImport.toUtcDate(originalInspection.Created),
          inspectionDate: this._commonImport.toUtcDate(
            originalInspection.InspectionDate
          ),
          complianceStatus: complianceStatus,
          daysSinceLastRain: originalInspection.DaysSinceLastRain,
          priorityNumber: originalInspection.PriorityNo,
          dischargeDescription: originalInspection.DischargeDescription,
          dryOrWetWeatherInspection: dryOrWet,
          color: color ? color.Color : null,
          clarity: clarity ? clarity.Clarity : null,
          odor: odor ? odor.Odor : null,
          foam: foam ? foam.Foam : null,
          sheen: sheen ? sheen.Sheen : null,
          sustainedSolids: susSolid ? susSolid.SusSolid : null,
          floatingSolids: floatSolid ? floatSolid.FloatSolid : null,
          setSolids: setSolid ? setSolid.SetSolid : null,
          pH: originalInspection.PH,
          temperatureF: originalInspection.TempF,
          domgl: originalInspection.DOmgl,
          turbidity: originalInspection.Turbidity,
          cl2: originalInspection.CL2,
          fecalColiform: originalInspection.FecalColiform,
          cond: originalInspection.Cond,
          doSaturation: originalInspection.DOSaturation,
          flowrateGpm: originalInspection.FlowrateGPM,
          copper: originalInspection.Copper,
          phenols: originalInspection.Phenols,
          ammonia: originalInspection.Ammonia,
          detergents: originalInspection.Detergents,
          tpo4: originalInspection.TPO4,
          bod: originalInspection.BOD,
          cod: originalInspection.COD,
          tss: originalInspection.TSS,
          no3: originalInspection.NO3,
          ecoli: originalInspection.EColi,
          additionalInformation: originalInspection.InspectionComment,
          weatherCondition: originalInspection.CurrentWeather,
        }
      );

      // calculate temperatureF if null
      if (!newInspection.temperatureF && originalInspection.TempC) {
        newInspection.temperatureF = originalInspection.TempC * 9 / 5 + 32;
      }

      if (originalInspection.Inspector > 0) {
        const inspector = inspectorsOrig.find(
          i => i.NumericID === originalInspection.Inspector
        );
        if (inspector) {
          newInspection.additionalInformation = [
            newInspection.additionalInformation,
            `Inspector: ${inspector.InspectorName}`,
          ].join('\r\n');
        }
      }
      if (originalInspection.Inspector2 > 0) {
        const inspector = inspectorsOrig.find(
          i => i.NumericID === originalInspection.Inspector2
        );
        if (inspector) {
          newInspection.additionalInformation = [
            newInspection.additionalInformation,
            `Inspector 2: ${inspector.InspectorName}`,
          ].join('\r\n');
        }
      }

      const insertedInspection = await this.newKnex(
        dh.getTableName(OutfallInspection)
      )
        .transacting(trx)
        .insert(objectToSqlData(OutfallInspection, newInspection, this.newKnex))
        .returning(
          dh.getSqlFields2(OutfallInspection, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<OutfallInspection>(0);
      this.logger.verbose(
        `OutfallInspection: imported OriginalId ${
          insertedInspection.originalId
        }`,
        insertedInspection
      );

      // Update Custom Fields for Custom Form Template
      await this._updateCustomFieldValues(
        insertedInspection,
        customFields,
        trx
      );

      // Handle Original Custom Fields
      await this._commonImport.updateCustomFieldsFromQuestions(
        OutfallInspection,
        insertedInspection,
        insertedInspection.originalId,
        customFields,
        trx
      );

      // Files
      const origFiles: BmpFileOrig[] = await this._commonImport
        .getOriginal(BmpFileOrig)
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerType),
          oiFileOwnerType.NumericID
        )
        .where(
          propertyNameFor<BmpFileOrig>(f => f.OwnerID),
          insertedInspection.originalId
        );
      await Bluebird.each(origFiles, async origFile => {
        this.logger.verbose(`OutfallInspection: File`, origFile);
        await this._commonImport.importFile(
          origFile,
          OutfallInspection,
          insertedInspection.id,
          trx
        );
      });
    });
  }

  /**
   * Sets inspectionDate for all Outfalls to be the latest date from inspection
   *
   */
  async updateLatestInspectionDate(trx: Knex.Transaction) {
    const selectMaxInspectionDate = this.newKnex(
      OutfallInspection.getTableName()
    )
      .transacting(trx)
      .max(
        OutfallInspection.getSqlField<OutfallInspection>(c => c.inspectionDate)
      )
      .whereRaw(`?? = ??`, [
        OutfallInspection.getSqlField<OutfallInspection>(
          i => i.outfallId,
          true
        ),
        Outfall.getSqlField<Outfall>(c => c.id, true),
      ])
      .where(
        OutfallInspection.getSqlField<OutfallInspection>(
          i => i.inspectionDate,
          true
        ),
        '<=',
        new Date()
      )
      .whereIn(
        OutfallInspection.getSqlField<OutfallInspection>(
          i => i.outfallId,
          true
        ),
        this.newKnex(Outfall.getTableName())
          .select(Outfall.getSqlField<Outfall>(c => c.id))
          .where(
            Outfall.getSqlField<Outfall>(c => c.customerId),
            this.config.CUSTOMER_ID
          )
      );
    const updateQb = this.newKnex(Outfall.getTableName())
      .transacting(trx)
      .update(
        Outfall.getSqlField<Outfall>(c => c.latestInspectionDate),
        this.newKnex.raw(selectMaxInspectionDate.toQuery()).wrap('(', ')') // knex doesnt' support querybuilder
      )
      .where(
        Outfall.getSqlField<Outfall>(c => c.customerId),
        this.config.CUSTOMER_ID
      );

    const rowCount = await updateQb;
    this.logger.info(
      `OutfallInspection: updated last inspected date`,
      rowCount
    );
  }

  private async _updateCustomFieldValues(
    inspection: OutfallInspection,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const getCf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );

    let cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: getCf('Dry or Wet Weather').id,
        globalIdFk: inspection.globalId,
        value: inspection.dryOrWetWeatherInspection,
      },
      {
        customFieldId: getCf('Days Since Last Rain').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.daysSinceLastRain),
      },
      {
        customFieldId: getCf('Color').id,
        globalIdFk: inspection.globalId,
        value: inspection.color,
      },
      {
        customFieldId: getCf('Clarity').id,
        globalIdFk: inspection.globalId,
        value: inspection.clarity,
      },
      {
        customFieldId: getCf('Odor').id,
        globalIdFk: inspection.globalId,
        value: inspection.odor,
      },
      {
        customFieldId: getCf('Foam').id,
        globalIdFk: inspection.globalId,
        value: inspection.foam,
      },
      {
        customFieldId: getCf('Sheen').id,
        globalIdFk: inspection.globalId,
        value: inspection.sheen,
      },
      {
        customFieldId: getCf('Suspended Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.sustainedSolids,
      },
      {
        customFieldId: getCf('Settled Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.setSolids,
      },
      {
        customFieldId: getCf('Floating Solids').id,
        globalIdFk: inspection.globalId,
        value: inspection.floatingSolids,
      },
      {
        customFieldId: getCf('PH').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.pH),
      },
      {
        customFieldId: getCf('Temperature(F)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.temperatureF),
      },
      {
        customFieldId: getCf('DO (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.domgl),
      },
      {
        customFieldId: getCf('Turbidity (NTU)').id,
        globalIdFk: inspection.globalId,
        value: inspection.turbidity,
      },
      {
        customFieldId: getCf('Cond (mOhms)').id,
        globalIdFk: inspection.globalId,
        value: inspection.cond,
      },
      {
        customFieldId: getCf('DO (%Sat)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.doSaturation),
      },
      {
        customFieldId: getCf('Flowrate (GPM)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.flowrateGpm),
      },
      {
        customFieldId: getCf('Copper (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.copper),
      },
      {
        customFieldId: getCf('Phenols (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.phenols),
      },
      {
        customFieldId: getCf('Ammonia (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.ammonia),
      },
      {
        customFieldId: getCf('Detergents (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.detergents),
      },
      {
        customFieldId: getCf('T.PO4 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.tpo4),
      },
      {
        customFieldId: getCf('Cl2 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.cl2),
      },
      {
        customFieldId: getCf('BOD (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.bod),
      },
      {
        customFieldId: getCf('COD (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.cod),
      },
      {
        customFieldId: getCf('TSS (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.tss),
      },
      {
        customFieldId: getCf('NO3 (mg/L)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.no3),
      },
      {
        customFieldId: getCf('Fecal Coliform (col/100mL)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.fecalColiform),
      },
      {
        customFieldId: getCf('E. Coli (col/100mL)').id,
        globalIdFk: inspection.globalId,
        value: numberToString(inspection.ecoli),
      },
      {
        customFieldId: getCf('Discharge Description').id,
        globalIdFk: inspection.globalId,
        value: inspection.dischargeDescription,
      },
    ];
    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }
}
