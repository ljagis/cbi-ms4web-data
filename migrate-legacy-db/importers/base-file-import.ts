import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { injectable } from 'inversify';
@injectable()
export abstract class BaseFileImport {
  getCustomFields(sheet: XLSX.Sheet) {
    const headers = [];
    const range = XLSX.utils.decode_range(sheet['!ref']);
    for (let i = range.s.c; i <= range.e.c; ++i) {
      const cell: XLSX.CellObject =
        sheet[XLSX.utils.encode_cell({ c: i, r: range.s.r })]; // find cell in first row;
      headers.push(cell.v);
    }

    const customFieldsNames = headers
      .filter((h: string) => h && h.toLowerCase().startsWith('cf:')) // filters only cf:
      .map((f: string) => {
        return {
          label: f.replace(/^cf:/i, '').trim(),
          originalField: f,
        };
      }); // removes cf:
    return customFieldsNames;
  }

  yesNoToBoolean(value: string) {
    if (value && ['yes', 'true', '1'].indexOf(value.toLowerCase()) >= 0) {
      return true;
    }
    if (value && ['no', 'false', '0'].indexOf(value.toLowerCase()) >= 0) {
      return false;
    }
    return null;
  }

  dateFromValue(value) {
    const d = moment(value);
    return d.isValid() ? d.toDate() : null;
  }

  toNumber(value) {
    const num = parseFloat(value);
    return isNaN(num) ? null : num;
  }
}
