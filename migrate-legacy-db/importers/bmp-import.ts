import * as Knex from 'knex';
import * as winston from 'winston';
import { inject, injectable } from 'inversify';
import * as Bluebird from 'bluebird';
import * as moment from 'moment';

import {
  BmpControlMeasure,
  BmpDetail,
  BmpActivityLog,
  BmpDataType,
} from '../../src/models';

import {
  objectToSqlData,
  propertyNameFor,
  EntityTypes,
} from '../../src/shared';
import {
  MinimumControlMeasureOrig,
  BestManagementPracticeOrig,
  BMPImplementationTaskOrig,
  BMPDataOrig,
  BMPMeasureableGoalOrig,
  Config,
  DataEntryUnitOrig,
  BmpFileOrig,
  FileOwnerTypeOrig,
} from '../models';

import { getSqlFields2, getTableName } from '../../src/decorators';
import { CommonImport } from './common-import';
import { fileOwnerTypeToEntityMap } from '../constants';

interface OriginalBmpActivities {
  tasks: BMPImplementationTaskOrig[];
  datas: BMPDataOrig[];
  measurableGoals: BMPMeasureableGoalOrig[];
}

@injectable()
export class BmpImport {
  private _originalDataEntryUnits: DataEntryUnitOrig[];
  private _bmpDetailFileOwnerType: FileOwnerTypeOrig;

  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _commonImport: CommonImport
  ) {}

  async import(trx: Knex.Transaction) {
    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    this._bmpDetailFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] === EntityTypes.BmpDetail
    );
    if (!this._bmpDetailFileOwnerType) {
      this.logger.error(`BmpDetail: Could not find FileOwnerType`);
      return;
    }
    await this.importDataTypes(trx);
    await this.importControlMeasures(trx);
  }

  async importBmpFiles(trx: Knex.Transaction) {
    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    this._bmpDetailFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] === EntityTypes.BmpDetail
    );
    if (!this._bmpDetailFileOwnerType) {
      this.logger.error(`BmpDetail: Could not find FileOwnerType`);
      return;
    }
    const controlMeasures = await this.getMcms(trx);
    const originalControlMeasures = await this._commonImport.getOriginalTable(
      MinimumControlMeasureOrig
    );

    const bmpDetails: BmpDetail[] = await this.newKnex(BmpDetail.tableName)
      .transacting(trx)
      .whereIn(
        BmpDetail.sqlField(f => f.controlMeasureId),
        this.newKnex(BmpControlMeasure.tableName)
          .where({
            [BmpControlMeasure.sqlField(f => f.isDeleted)]: 0,
            [BmpControlMeasure.sqlField(f => f.customerId)]: this.config
              .CUSTOMER_ID,
          })
          .select('Id')
      )
      .where({
        [BmpDetail.sqlField(f => f.isDeleted)]: 0,
      })
      .select(getSqlFields2(BmpDetail, { asProperty: true }));
    const originalBmps: BestManagementPracticeOrig[] = await this.origKnex(
      getTableName(BestManagementPracticeOrig)
    ).catch(this.logger.error);

    this.logger.info(`Original Bmps: Found ${originalBmps.length} to import.`);

    // BMP Files
    const origFiles: BmpFileOrig[] = await this._commonImport
      .getOriginal(BmpFileOrig)
      .where(
        propertyNameFor<BmpFileOrig>(f => f.OwnerType),
        this._bmpDetailFileOwnerType.NumericID
      );
    await Bluebird.each(origFiles, async origFile => {
      this.logger.verbose(`BMPDetail: File`, origFile);
      const origBmp = originalBmps.find(f => f.NumericID === origFile.OwnerID);
      if (!origBmp) {
        this.logger.warn(
          `Could not find Original BMP NumericID: ${origFile.OwnerID}`
        );
        return;
      }
      const origCm = originalControlMeasures.find(
        c => c.NumericID === origBmp.ControlMeasureID
      );
      const cm = controlMeasures.find(f => f.name === origCm.ControlMeasure);
      if (!cm) {
        return;
      }
      const foundBmpDetails = bmpDetails.filter(
        d =>
          d.controlMeasureId === cm.id && d.description === origBmp.Description
      );

      if (foundBmpDetails.length > 1) {
        this.logger.error(`More than 1 BmpDetails found`, foundBmpDetails[0]);
        throw Error(`More than 1 BmpDetails found`);
      }
      if (foundBmpDetails.length) {
        await this._commonImport.importFile(
          origFile,
          BmpDetail,
          foundBmpDetails[0].id,
          trx
        );
      }
    });
  }

  private async getMcms(trx: Knex.Transaction) {
    const existingCMs: BmpControlMeasure[] = await this.newKnex(
      getTableName(BmpControlMeasure)
    )
      .transacting(trx)
      .select(
        ...getSqlFields2(BmpControlMeasure, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(
        BmpControlMeasure.getSqlField<BmpControlMeasure>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);
    return existingCMs;
  }

  private async getBmpDetails(cmId: number, trx: Knex.Transaction) {
    const details: BmpDetail[] = await this.newKnex(getTableName(BmpDetail))
      .transacting(trx)
      .select(
        ...getSqlFields2(BmpDetail, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [BmpDetail.sqlField(f => f.controlMeasureId)]: cmId,
        [BmpDetail.sqlField(c => c.isDeleted)]: 0,
      })
      .catch(this.logger.error);
    return details;
  }

  private async getBmpDataTypes(trx: Knex.Transaction) {
    const existingDts: BmpDataType[] = await this.newKnex(BmpDataType.tableName)
      .transacting(trx)
      .where({
        [BmpDataType.sqlField(f => f.customerId)]: this.config.CUSTOMER_ID,
        [BmpDataType.sqlField(f => f.isDeleted)]: 0,
      })
      .returning(
        getSqlFields2(BmpDataType, {
          includeInternalField: true,
          asProperty: true,
        })
      );
    return existingDts;
  }

  private async importDataTypes(trx: Knex.Transaction) {
    const originalDts: DataEntryUnitOrig[] = await this.origKnex(
      getTableName(DataEntryUnitOrig)
    ).catch(this.logger.error);
    this._originalDataEntryUnits = originalDts;
    this.logger.info(
      `Bmp Data Entry Units: Found ${originalDts.length} to import.`
    );

    const existingDts = await this.getBmpDataTypes(trx);

    const insertedDts = await Bluebird.mapSeries(originalDts, async origDt => {
      const existing = existingDts.find(
        f => f.name === origDt.DataEntryUnitName
      );
      if (existing) {
        this.logger.warn(`BMPDataType exists: ${origDt.DataEntryUnitName}`);
        return;
      }
      const newDt: Partial<BmpDataType> = {
        customerId: this.config.CUSTOMER_ID,
        name: origDt.DataEntryUnitName,
      };
      const insertedDt: BmpDataType = await this.newKnex(BmpDataType.tableName)
        .transacting(trx)
        .insert(objectToSqlData(BmpDataType, newDt, this.newKnex))
        .returning(
          getSqlFields2(BmpDataType, {
            includeInternalField: true,
            asProperty: true,
          })
        );
      return insertedDt;
    });
    return insertedDts.filter(d => d);
  }

  private async importControlMeasures(trx: Knex.Transaction) {
    const originalCMs: MinimumControlMeasureOrig[] = await this.origKnex(
      getTableName(MinimumControlMeasureOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `BmpControlMeasure: Found ${originalCMs.length} to import.`
    );

    const originalBmps: BestManagementPracticeOrig[] = await this.origKnex(
      getTableName(BestManagementPracticeOrig)
    ).catch(this.logger.error);
    this.logger.info(`Original Bmps: Found ${originalBmps.length} to import.`);

    // There are 3 tables to import from: BMPImplementationTaskOrig, BMPDataOrig, BMPMeasureableGoalOrig
    // These are BMP Activity Logs
    const implTasks: BMPImplementationTaskOrig[] = await this.origKnex(
      getTableName(BMPImplementationTaskOrig)
    );
    const bmpDatas: BMPDataOrig[] = await this.origKnex(
      getTableName(BMPDataOrig)
    );
    const measurableGoals: BMPMeasureableGoalOrig[] = await this.origKnex(
      getTableName(BMPMeasureableGoalOrig)
    );

    const existingCMs = await this.getMcms(trx);

    const importRecords = await Bluebird.each(originalCMs, async originalCM => {
      let existingCM = existingCMs.find(
        f => f.name === originalCM.ControlMeasure
      );
      if (existingCM) {
        this.logger.warn(
          `BmpControlMeasure: "${existingCM.name}" already exists.  Skipping.`,
          existingCM
        );
      } else {
        const newCM: BmpControlMeasure = Object.assign(
          new BmpControlMeasure(),
          <Partial<BmpControlMeasure>>{
            customerId: this.config.CUSTOMER_ID,
            name: originalCM.ControlMeasure,
            originalId: originalCM.NumericID,
          }
        );

        const insertedMcm = (existingCM = await this.newKnex(
          getTableName(BmpControlMeasure)
        )
          .transacting(trx)
          .insert(objectToSqlData(BmpControlMeasure, newCM, this.newKnex))
          .returning(
            getSqlFields2(BmpControlMeasure, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<BmpControlMeasure>(0));
        this.logger.verbose(
          `BmpControlMeasure: imported "${insertedMcm.name}"`,
          insertedMcm
        );
      }

      await this.importBmpDetail(
        originalBmps.filter(f => f.ControlMeasureID === originalCM.NumericID),
        existingCM,
        trx,
        {
          tasks: implTasks,
          datas: bmpDatas,
          measurableGoals: measurableGoals,
        }
      );

      return existingCM;
    });

    return importRecords;
  }

  private async importBmpDetail(
    originalBmps: BestManagementPracticeOrig[],
    newMcm: BmpControlMeasure,
    trx: Knex.Transaction,
    originalActivities: OriginalBmpActivities
  ) {
    const existingDetails = await this.getBmpDetails(newMcm.id, trx);
    const importRecords = await Bluebird.each(
      originalBmps,
      async originalBmp => {
        const existingDetail = existingDetails.find(
          f => f.name === originalBmp.Title
        );
        if (existingDetail) {
          this.logger.warn(
            `BmpDetail exists for MCM: ${existingDetail.controlMeasureId} - ${
              existingDetail.name
            }`
          );
          return;
        }
        const newTask: BmpDetail = Object.assign(new BmpDetail(), <Partial<
          BmpDetail
        >>{
          name: originalBmp.Title,
          controlMeasureId: newMcm.id,
          originalId: originalBmp.NumericID,
          description: originalBmp.Description,
          dateAdded: this.toUtcDate(originalBmp.ImplementationDate),
          completionDate: originalBmp.Implemented
            ? this.toUtcDate(originalBmp.Implemented)
            : null,
        });

        const insertedBmpDetail = await this.newKnex(getTableName(BmpDetail))
          .insert(objectToSqlData(BmpDetail, newTask, this.newKnex))
          .transacting(trx)
          .returning(
            getSqlFields2(BmpDetail, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<BmpDetail>(0);
        this.logger.verbose(
          `BmpDetail: imported "${insertedBmpDetail.name}"`,
          insertedBmpDetail
        );

        // BMP Files
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            this._bmpDetailFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            originalBmp.NumericID
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`BMPDetail: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            BmpDetail,
            insertedBmpDetail.id,
            trx
          );
        });

        // Add "BmpActivityLog"
        await this.importBmpActivityLogs(originalBmp, insertedBmpDetail, trx, {
          tasks: originalActivities.tasks.filter(
            f => f.ManagementPracticeID === originalBmp.NumericID
          ),
          datas: originalActivities.datas.filter(
            f => f.ManagementPracticeID === originalBmp.NumericID
          ),
          measurableGoals: originalActivities.measurableGoals.filter(
            f => f.ManagementPracticeID === originalBmp.NumericID
          ),
        });

        return insertedBmpDetail;
      }
    );

    return importRecords;
  }

  // insert each task
  async importBmpActivityLogs(
    originalBmp: BestManagementPracticeOrig,
    newBmp: BmpDetail,
    trx: Knex.Transaction,
    originalActivities: OriginalBmpActivities
  ) {
    await Bluebird.each(originalActivities.tasks, async implTask => {
      const newActivity: BmpActivityLog = Object.assign(
        new BmpActivityLog(),
        <Partial<BmpActivityLog>>{
          bmpDetailId: newBmp.id,
          dateAdded: this.toUtcDate(implTask.ImplementationDate),
          activityDate:
            this.toUtcDate(implTask.ImplementationDate) ||
            this.toUtcDate(originalBmp.ImplementationDate) ||
            this.toUtcDate(new Date()),
          comments: implTask.Description,
          completedDate: this.toUtcDate(implTask.ImpTaskCompleteDate),
        }
      );
      const insertedActivityLog = await this.newKnex(
        getTableName(BmpActivityLog)
      )
        .transacting(trx)
        .insert(objectToSqlData(BmpActivityLog, newActivity, this.newKnex))
        .returning(
          getSqlFields2(BmpActivityLog, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivityLog>(0);
      this.logger.verbose(
        `BmpTask: imported "${insertedActivityLog.comments}"`,
        insertedActivityLog
      );
    });

    await Bluebird.each(originalActivities.datas, async originalBmpData => {
      let comments = '';
      if (originalBmpData.Activity) {
        comments += `Activity: ${originalBmpData.Activity}\r\n`;
      }
      if (originalBmpData.Location) {
        comments += `Location: ${originalBmpData.Location}\r\n`;
      }
      if (originalBmpData.Comment) {
        comments += `Comment: ${originalBmpData.Comment}\r\n`;
      }
      if (originalBmpData.Cost) {
        comments += `Cost: ${originalBmpData.Cost}\r\n`;
      }
      const origDt = this._originalDataEntryUnits.find(
        f => f.NumericID === originalBmpData.DataEntryUnit
      );
      const newActivityLog: BmpActivityLog = Object.assign(
        new BmpActivityLog(),
        <Partial<BmpActivityLog>>{
          bmpDetailId: newBmp.id,
          dateAdded: this.toUtcDate(originalBmpData.DateEntered),
          activityDate:
            this.toUtcDate(originalBmpData.DateEntered) ||
            this.toUtcDate(originalBmp.ImplementationDate) ||
            this.toUtcDate(new Date()),
          comments: comments,
          quantity: originalBmpData.Quantity,
          dataType: origDt ? origDt.DataEntryUnitName : null,
        }
      );
      const insertedActivityLog = await this.newKnex(
        getTableName(BmpActivityLog)
      )
        .transacting(trx)
        .insert(objectToSqlData(BmpActivityLog, newActivityLog, this.newKnex))
        .returning(
          getSqlFields2(BmpActivityLog, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivityLog>(0);
      this.logger.verbose(
        `BmpActivityLog: imported "${insertedActivityLog.id}"`,
        insertedActivityLog
      );
    });

    await Bluebird.each(originalActivities.measurableGoals, async origGoal => {
      let comments = '';
      if (origGoal.ActivityDescription) {
        comments += `ActivityDescription: ${origGoal.ActivityDescription}\r\n`;
      }
      if (origGoal.Description) {
        comments += `Description: ${origGoal.Description}\r\n`;
      }
      const newActivityLog: BmpActivityLog = Object.assign(
        new BmpActivityLog(),
        <Partial<BmpActivityLog>>{
          bmpDetailId: newBmp.id,
          comments: comments,
          activityDate:
            this.toUtcDate(origGoal.DateGoalMet) ||
            this.toUtcDate(originalBmp.ImplementationDate) ||
            this.toUtcDate(new Date()),
          quantity: origGoal.Quantity,
        }
      );
      const insertedActivityLog = await this.newKnex(
        getTableName(BmpActivityLog)
      )
        .transacting(trx)
        .insert(objectToSqlData(BmpActivityLog, newActivityLog, this.newKnex))
        .returning(
          getSqlFields2(BmpActivityLog, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<BmpActivityLog>(0);
      this.logger.verbose(
        `BmpActivityLog: imported "${insertedActivityLog.id}"`,
        insertedActivityLog
      );
    });
  }

  private toUtcDate(dateField) {
    return moment(dateField).isValid()
      ? moment(dateField)
          .utc()
          .toDate()
      : null;
  }
}
