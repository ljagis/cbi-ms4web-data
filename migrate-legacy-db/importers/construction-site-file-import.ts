import { injectable, inject } from 'inversify';
import * as XLSX from 'xlsx';
import {
  ConstructionSite,
  Community,
  Watershed,
  ReceivingWater,
  Project,
  CustomFieldValueMap,
  projectTypes,
  permitStatus,
  projectStatus,
  ComplianceStatus,
  Contact,
} from '../../src/models';
import * as Bluebird from 'bluebird';
import {
  ConstructionSiteTemplate,
  Config,
  FileImportable,
  FileImportValidate,
} from '../models';
import { CommonImport } from './common-import';
import * as winston from 'winston';
import * as _ from 'lodash';
import { CustomFieldRepository, ContactRepository } from '../../src/data';
import * as Knex from 'knex';
import { getTableName, getSqlFields2 } from '../../src/decorators';
import { objectToSqlData } from '../../src/shared';
import { BaseFileImport } from './base-file-import';
@injectable()
export class ConstructionSiteFileImport extends BaseFileImport
  implements FileImportable, FileImportValidate {
  constructor(
    @inject('config') private config: Config,
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('NewDb') private newKnex: Knex,
    private _contactRepo: ContactRepository,
    private _commonImport: CommonImport,
    private _customFieldRepo: CustomFieldRepository
  ) {
    super();
  }

  async validateFile(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<boolean> {
    let validate = true;
    const csSheet = workbook.Sheets['Construction Sites'];
    if (!csSheet) {
      return true;
    }

    const constructionSites = XLSX.utils.sheet_to_json<
      ConstructionSiteTemplate
    >(csSheet);

    // Custom Fields
    const customFieldsFromTemplate = this.getCustomFields(csSheet)
      .filter(f => f)
      .map(f => f.label.trim());
    const customFields = await this._customFieldRepo.getCustomFieldsForEntity(
      ConstructionSite,
      this.config.CUSTOMER_ID
    );
    const customFieldNamesFromDb = customFields
      .filter(cf => cf && cf.fieldLabel)
      .map(cf => cf.fieldLabel.trim());
    const customFieldsMissing = _.difference(
      customFieldsFromTemplate,
      customFieldNamesFromDb
    );
    if (customFieldsMissing.length) {
      this.logger.error(
        'Custom Fields missing for Construction Sites: ',
        customFieldsMissing
      );
      validate = false;
    }

    // Project Type
    const uniqueProjectTypes = _(constructionSites)
      .map(cs => cs.projectType)
      .uniq()
      .filter(t => t)
      .value();
    const projectTypesMissing = _.difference(
      uniqueProjectTypes,
      _.values(projectTypes)
    );
    if (projectTypesMissing.length) {
      this.logger.error(
        'Project Type mismatch for Construction Sites: ',
        projectTypesMissing
      );
      validate = false;
    }

    // Permit Status
    const uniquePermitStatuses = _(constructionSites)
      .map(cs => cs.permitStatus)
      .uniq()
      .filter(t => t)
      .value();
    const permitStatusMissing = _.difference(
      uniquePermitStatuses,
      _.values(permitStatus)
    );
    if (permitStatusMissing.length) {
      this.logger.error(
        'Permit Status mismatch for Construction Sites: ',
        permitStatusMissing
      );
      validate = false;
    }

    // Project status
    const uniqueProjectStatuses = _(constructionSites)
      .map(cs => cs.projectStatus)
      .uniq()
      .filter(t => t)
      .value();
    const projectStatusesMissing = _.difference(
      uniqueProjectStatuses,
      _.values(projectStatus)
    );
    if (projectStatusesMissing.length) {
      this.logger.error(
        'Project Status mismatch for Construction Sites: ',
        projectStatusesMissing
      );
      validate = false;
    }

    // Compliance Status
    const uniqueComplianceStatuses = _(constructionSites)
      .map(cs => cs.complianceStatus)
      .uniq()
      .filter(t => t)
      .value();
    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const csComplianceStatus = complianceStatuses
      .filter(c => c.entityType === 'ConstructionSite')
      .map(c => c.name);
    const complianceStatusesMissing = _.difference(
      uniqueComplianceStatuses,
      _.values(csComplianceStatus)
    );
    if (complianceStatusesMissing.length) {
      this.logger.error(
        'Compliance Status mismatch for Construction Sites: ',
        complianceStatusesMissing
      );
      validate = false;
    }

    return validate;
  }

  async import(workbook: XLSX.WorkBook, trx): Bluebird<ConstructionSite[]> {
    const csSheet = workbook.Sheets['Construction Sites'];
    if (!csSheet) {
      return [];
    }
    const customFieldsFromTemplate = this.getCustomFields(csSheet);

    const constructionSites: ConstructionSiteTemplate[] = XLSX.utils.sheet_to_json<
      ConstructionSiteTemplate
    >(csSheet);
    const projects = await this._commonImport.getLookup(Project, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const allWatersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = allWatersheds.filter(w => !w.isSubWatershed).slice(0);
    const subwatersheds = allWatersheds.filter(w => w.isSubWatershed).slice(0);
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    // TODO: include transaction
    const customFieldsFromDb = await this._customFieldRepo.getCustomFieldsForEntity(
      ConstructionSite,
      this.config.CUSTOMER_ID
    );

    const sitesInserted = await Bluebird.mapSeries(
      constructionSites,
      async csImport => {
        let csNew: ConstructionSite = Object.assign(new ConstructionSite(), {
          customerId: this.config.CUSTOMER_ID,
          dateAdded: this.dateFromValue(csImport.dateAdded) || new Date(),
          trackingId: csImport.trackingId,
          name: csImport.name,
          physicalAddress1: csImport.physicalAddress1,
          physicalAddress2: csImport.physicalAddress2,
          physicalCity: csImport.physicalCity,
          physicalState: csImport.physicalState,
          physicalZip: csImport.physicalZip,
          projectType: csImport.projectType,
          permitStatus: csImport.permitStatus,
          projectArea: this.toNumber(csImport.projectArea),
          disturbedArea: this.toNumber(csImport.disturbedArea),
          startDate: csImport.startDate
            ? this.dateFromValue(csImport.startDate)
            : null,
          estimatedCompletionDate: csImport.estimatedCompletionDate
            ? this.dateFromValue(csImport.estimatedCompletionDate)
            : null,
          latestInspectionDate: csImport.latestInspectionDate
            ? this.dateFromValue(csImport.latestInspectionDate)
            : null,
          completionDate: csImport.completionDate
            ? this.dateFromValue(csImport.completionDate)
            : null,
          projectStatus: csImport.projectStatus,
          npdes: this.yesNoToBoolean(csImport.npdes),
          usace404: this.yesNoToBoolean(csImport.usace404),
          state401: this.yesNoToBoolean(csImport.state401),
          usacenwp: this.yesNoToBoolean(csImport.usacenwp),
          complianceStatus: csImport.complianceStatus,
          additionalInformation: csImport.additionalInformation,
          lat: this.toNumber(csImport.lat),
          lng: this.toNumber(csImport.lng),
        } as ConstructionSite);

        // project
        if (csImport.projectName) {
          let project = await this._commonImport.createProjectIfNotExists(
            csImport.projectName,
            projects,
            trx
          );
          if (project) {
            csNew.projectId = project.id;
          }
        }

        // community
        if (csImport.communityName) {
          const community = await this._commonImport.createCommunityIfNotExists(
            csImport.communityName,
            communities
          );
          if (community) {
            csNew.communityId = community.id;
          }
        }

        // ws
        if (csImport.watershedName) {
          const ws = await this._commonImport.createWatershedIfNotExists(
            csImport.watershedName,
            watersheds
          );
          if (ws) {
            csNew.watershedId = ws.id as number;
          }
        }

        // sws
        if (csImport.subwatershedName) {
          const sws = await this._commonImport.createWatershedIfNotExists(
            csImport.subwatershedName,
            subwatersheds,
            true
          );
          if (sws) {
            csNew.subwatershedId = sws.id as number;
          }
        }

        // receiving water
        if (csImport.receivingWatersName) {
          const rw = await this._commonImport.createReceivingWaterIfNotExists(
            csImport.receivingWatersName,
            receivingWaters
          );
          if (rw) {
            csNew.receivingWatersId = rw.id;
          }
        }

        const insertedCs = await this.newKnex(getTableName(ConstructionSite))
          .insert(objectToSqlData(ConstructionSite, csNew, this.newKnex))
          .returning(
            getSqlFields2(ConstructionSite, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<ConstructionSite>(0);
        this.logger.verbose(`ConstructionSite: imported`, csNew);

        // custom fields
        const cfvs = customFieldsFromTemplate.reduce(
          (list, cfFromTemp) => {
            const cf = customFieldsFromDb.find(
              c => c.fieldLabel && c.fieldLabel.trim() === cfFromTemp.label
            );
            if (cf && csImport[cfFromTemp.originalField]) {
              list.push({
                id: cf.id as number,
                value: csImport[cfFromTemp.originalField],
              });
            }
            return list;
          },
          [] as CustomFieldValueMap[]
        );

        await this._customFieldRepo.updateCustomFieldsForEntity(
          ConstructionSite,
          insertedCs.id,
          cfvs,
          this.config.CUSTOMER_ID
        );

        // owner contacts
        if (csImport.ownerContact) {
          const contacts = await this._commonImport.getContacts(trx);

          let existingOwnerContact = contacts.find(
            c =>
              c.contactType === 'Owner' &&
              c.name === csImport.ownerContact &&
              c.email === csImport.ownerEmail &&
              c.phone === csImport.ownerPhone
          );
          if (!existingOwnerContact) {
            const ownerContact: Partial<Contact> = Object.assign(
              new Contact(),
              <Partial<Contact>>{
                contactType: 'Owner',
                customerId: this.config.CUSTOMER_ID,
                name: csImport.ownerContact,
                email: csImport.ownerEmail,
                phone: csImport.ownerPhone,
              }
            );
            existingOwnerContact = await this._commonImport.importContact(
              ownerContact,
              trx
            );
            contacts.push(existingOwnerContact);
            this.logger.verbose(
              `ConstructionSite: Owner Contact: Added name: "${
                existingOwnerContact.name
              }"`
            );
          } else {
            this.logger.info(
              `ConstructionSite: Owner Contact: Found existing owner contact: "` +
                `${existingOwnerContact.name}"`
            );
          }

          const entityContactId = await this._contactRepo.insertContactForEntity(
            ConstructionSite,
            insertedCs.id,
            existingOwnerContact.id as number,
            this.config.CUSTOMER_ID,
            trx
          );
          this.logger.verbose(
            `ConstructionSite: Associated Owner contact to EntityContact id: ${entityContactId}`
          );
        }

        // operator contacts
        if (csImport.operatorName) {
          const contacts = await this._commonImport.getContacts(trx);

          let existingOperatorContact = contacts.find(
            c =>
              c.contactType === 'Operator' &&
              c.name === csImport.operatorName &&
              c.company === csImport.operatorCompany &&
              c.mailingAddressLine1 === csImport.operatorAddressLine1 &&
              c.mailingZip === csImport.operatorMailingZip
          );
          if (!existingOperatorContact) {
            const operatorContact: Partial<Contact> = Object.assign(
              new Contact(),
              <Partial<Contact>>{
                contactType:
                  csImport.operatorTitle === 'Owner' ? 'Owner' : 'Operator',
                customerId: this.config.CUSTOMER_ID,
                name: csImport.operatorName,
                email: csImport.operatorEmail,
                phone: csImport.operatorPhone,
                mobilePhone: csImport.operatorMobilePhone,
                company: csImport.operatorCompany,
                mailingAddressLine1: csImport.operatorAddressLine1,
                mailingAddressLine2: csImport.operatorAddressLine2,
                mailingCity: csImport.operatorMailingCity,
                mailingState: csImport.operatorMailingState,
                mailingZip: csImport.operatorMailingZip,
              }
            );
            existingOperatorContact = await this._commonImport.importContact(
              operatorContact,
              trx
            );
            contacts.push(existingOperatorContact);
            this.logger.verbose(
              `ConstructionSite: Operator Contact: Added name: "${
                existingOperatorContact.name
              }", company: ${existingOperatorContact.company}`
            );
          } else {
            this.logger.info(
              `ConstructionSite: Operator Contact: Found existing operator contact: "` +
                `${existingOperatorContact.name}", company: ${
                  existingOperatorContact.company
                }`
            );
          }

          const entityContactId = await this._contactRepo.insertContactForEntity(
            ConstructionSite,
            insertedCs.id,
            existingOperatorContact.id as number,
            this.config.CUSTOMER_ID,
            trx
          );
          this.logger.info(
            `ConstructionSite: Associated Operator contact to EntityContact id: ${entityContactId}`
          );
        }

        // secondary contact
        if (csImport.secondaryOpName) {
          const contacts = await this._commonImport.getContacts(trx);

          const secondaryContact: Partial<Contact> = Object.assign(
            new Contact(),
            <Partial<Contact>>{
              contactType:
                csImport.secondaryOpTitle === 'Owner' ? 'Owner' : 'Operator',
              customerId: this.config.CUSTOMER_ID,
              name: csImport.secondaryOpName,
              email: csImport.secondaryOpEmail,
              phone: csImport.secondaryOpPhone,
              mobilePhone: csImport.secondaryOpMobilePhone,
              company: csImport.operatorCompany,
              mailingAddressLine1: csImport.operatorAddressLine1,
              mailingAddressLine2: csImport.operatorAddressLine2,
              mailingCity: csImport.operatorMailingCity,
              mailingState: csImport.operatorMailingState,
              mailingZip: csImport.operatorMailingZip,
            }
          );

          const importedContact = await this._commonImport.importContact(
            secondaryContact,
            trx
          );

          contacts.push(importedContact);

          await this._contactRepo.insertContactForEntity(
            ConstructionSite,
            insertedCs.id,
            importedContact.id as number,
            this.config.CUSTOMER_ID,
            trx
          );
        }

        return insertedCs;
      }
    );
    return sitesInserted;
  }
}
