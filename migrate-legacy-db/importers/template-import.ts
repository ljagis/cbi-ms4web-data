import { injectable, inject } from 'inversify';
import * as XLSX from 'xlsx';
import * as Bluebird from 'bluebird';
import { FileImportable, FileImportValidate } from '../models';
import * as winston from 'winston';
import * as Knex from 'knex';
import { OutfallFileImport } from './outfall-file-import';
import { ConstructionSiteFileImport } from './construction-site-file-import';
import { StructureFileImport } from './structure-file-import';
import { FacilityFileImport } from './facility-file-import';

@injectable()
export class TemplateImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    private _outfallFileImport: OutfallFileImport,
    private _csFileImport: ConstructionSiteFileImport,
    private _structureFileImport: StructureFileImport,
    private _facilitiesFileImport: FacilityFileImport
  ) {}
  async import(workbook: XLSX.WorkBook, trx: Knex.Transaction) {
    type ImportAndValidate = FileImportable & FileImportValidate;
    const importInfos = [
      {
        name: 'Construction Site',
        fileImport: this._csFileImport as ImportAndValidate,
      },
      {
        name: 'Outfalls',
        fileImport: this._outfallFileImport as ImportAndValidate,
      },
      {
        name: 'Structures',
        fileImport: this._structureFileImport as ImportAndValidate,
      },
      {
        name: 'Facilities',
        fileImport: this._facilitiesFileImport as ImportAndValidate,
      },
    ];

    const validFiles = await Bluebird.mapSeries(
      importInfos,
      async importInfo => {
        const isValid = await importInfo.fileImport.validateFile(workbook, trx);
        this.logger.info(`Validate ${importInfo.name}: ${isValid}`);
        return {
          name: importInfo.name,
          isValid,
        };
      }
    );

    validFiles.forEach(v => {
      if (!v.isValid) {
        throw Error(`ERROR importing on Sheet: ${v.name}`);
      }
    });

    await Bluebird.mapSeries(importInfos, async importInfo => {
      const importedItems = await importInfo.fileImport.import(workbook, trx);
      this.logger.info(`${importInfo.name}: inserted ${importedItems.length}.`);
    });
  }
}
