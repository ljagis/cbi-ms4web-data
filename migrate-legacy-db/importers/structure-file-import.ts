import { injectable, inject } from 'inversify';
import * as XLSX from 'xlsx';
import { CustomFieldValueMap, Structure } from '../../src/models';
import * as Bluebird from 'bluebird';
import { Config, FileImportable, FileImportValidate } from '../models';
import * as winston from 'winston';
import {
  CustomFieldRepository,
  StructureControlTypeRepository,
} from '../../src/data';
import * as Knex from 'knex';
import { getTableName, getSqlFields2 } from '../../src/decorators';
import { objectToSqlData, compareCaseInsensitive } from '../../src/shared';
import { BaseFileImport } from './base-file-import';
@injectable()
export class StructureFileImport extends BaseFileImport
  implements FileImportable, FileImportValidate {
  constructor(
    @inject('config') private config: Config,
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('NewDb') private newKnex: Knex,
    private _customFieldRepo: CustomFieldRepository,
    private _controlTypeRepo: StructureControlTypeRepository
  ) {
    super();
  }

  async validateFile(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<boolean> {
    // maybe one day validate this
    return true;
  }

  async import(
    workbook: XLSX.WorkBook,
    trx: Knex.Transaction
  ): Bluebird<Structure[]> {
    const structureSheet = workbook.Sheets['Structures'];
    if (!structureSheet) {
      return [];
    }
    const customFieldsFromTemplate = this.getCustomFields(structureSheet);

    const structuresFromSheet = XLSX.utils.sheet_to_json<Structure>(
      structureSheet
    );

    // TODO: include transaction
    const customFieldsFromDb = await this._customFieldRepo.getCustomFieldsForEntity(
      Structure,
      this.config.CUSTOMER_ID
    );

    const controlTypes = await this._controlTypeRepo.fetch(
      this.config.CUSTOMER_ID
    );

    const structuresInserted = await Bluebird.mapSeries(
      structuresFromSheet,
      async structureToImport => {
        const emptyStructure = new Structure();

        let newStructure: Structure = {
          ...emptyStructure,
          customerId: this.config.CUSTOMER_ID,
          name: structureToImport.name,
          dateAdded:
            this.dateFromValue(structureToImport.dateAdded) || new Date(),
          trackingId: structureToImport.trackingId,

          // control type see below
          // projectType not implemented
          physicalAddress1: structureToImport.physicalAddress1,
          physicalAddress2: structureToImport.physicalAddress2,
          physicalCity: structureToImport.physicalCity,
          physicalState: structureToImport.physicalState,
          physicalZip: structureToImport.physicalZip,

          additionalInformation: structureToImport.additionalInformation || '',
          complianceStatus: structureToImport.complianceStatus,
          lat: this.toNumber(structureToImport.lat),
          lng: this.toNumber(structureToImport.lng),
        };

        if (structureToImport.controlType) {
          const controlType = controlTypes.find(type =>
            compareCaseInsensitive(type.name, structureToImport.controlType)
          );
          if (controlType) {
            newStructure.controlType = controlType.name;
          } else {
            newStructure.additionalInformation =
              newStructure.additionalInformation +
              `\r\nControl Type: ${structureToImport.controlType}`;
          }
        }

        const insertedStructures = await this.newKnex(getTableName(Structure))
          .insert(objectToSqlData(Structure, newStructure, this.newKnex))
          .returning(
            getSqlFields2(Structure, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Structure>(0);
        this.logger.verbose(`Structure: imported`, newStructure);

        // custom fields
        const cfvs = customFieldsFromTemplate.reduce(
          (list, cfFromTemp) => {
            const cf = customFieldsFromDb.find(
              c => c.fieldLabel && c.fieldLabel.trim() === cfFromTemp.label
            );
            if (cf && structureToImport[cfFromTemp.originalField]) {
              list.push({
                id: cf.id as number,
                value: structureToImport[cfFromTemp.originalField],
              });
            }
            return list;
          },
          [] as CustomFieldValueMap[]
        );

        await this._customFieldRepo.updateCustomFieldsForEntity(
          Structure,
          insertedStructures.id,
          cfvs,
          this.config.CUSTOMER_ID
        );

        return insertedStructures;
      }
    );
    return structuresInserted;
  }
}
