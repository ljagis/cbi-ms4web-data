import { inject, injectable } from 'inversify';
import { ContactRepository } from '../../src/data';
import * as _ from 'lodash';
import * as Knex from 'knex';
import * as winston from 'winston';
import {
  CustomFieldSetupOrig,
  StructureOrig,
  ControlTypeOrig,
  ProjecTypeOrig,
  PermitStatusOrig,
  InspectionFrequencyOrig,
  StructureInspectionOrig,
  FileOwnerTypeOrig,
  BmpFileOrig,
  Config,
} from '../models';
import * as moment from 'moment';
import * as dh from '../../src/decorators';
import { CommonImport } from './common-import';
import {
  Community,
  Watershed,
  ReceivingWater,
  CustomField,
  Structure,
  ComplianceStatus,
  State,
  InspectionFrequency,
  Project,
  projectTypes,
  permitStatus,
  Contact,
  StructureInspection,
  CustomFieldValue,
} from '../../src/models';
import {
  EntityTypes,
  objectToSqlData,
  propertyNameFor,
} from '../../src/shared';
import { siteSectionEntityMap, fileOwnerTypeToEntityMap } from '../constants';
import * as Bluebird from 'bluebird';
import { dateToStringOrNull, booleanToString } from '../utils/index';

@injectable()
export class StructureImport {
  constructor(
    @inject('logger') private logger: winston.LoggerInstance,
    @inject('OriginalDb') private origKnex: Knex,
    @inject('NewDb') private newKnex: Knex,
    @inject('config') private config: Config,
    private _commonImport: CommonImport,
    private _contactRepo: ContactRepository
  ) {}

  async import(trx: Knex.Transaction) {
    const originalStructures: StructureOrig[] = await this.origKnex(
      dh.getTableName(StructureOrig)
    )
      .select('*')
      .catch(this.logger.error);
    this.logger.info(
      `Structure: Found ${originalStructures.length} to import.`
    );

    const existingStructures: Structure[] = await this.newKnex(
      dh.getTableName(Structure)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(Structure, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        Structure.getSqlField<Structure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(Structure.getSqlField<Structure>(c => c.isDeleted), 0)
      .catch(this.logger.error);

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const strucFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] === EntityTypes.Structure
    );
    if (!strucFileOwnerType) {
      this.logger.warn(`Structure: Could not find FileOwnerType`);
    }

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.entityType),
        EntityTypes.Structure
      )
      .where(
        CustomField.getSqlField<CustomField>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .where(CustomField.getSqlField<CustomField>(c => c.isDeleted), 0);
    const origCustomFields = await this._commonImport.getOriginalTable(
      CustomFieldSetupOrig
    );
    const origStructureCFs = origCustomFields.filter(
      c => siteSectionEntityMap[c.SiteSection] === EntityTypes.Structure
    );

    const controlTypesOrig = await this._commonImport.getOriginalTable(
      ControlTypeOrig
    );
    const currentProjects = await this._commonImport.getLookup(Project, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const projectTypesOrig = await this._commonImport.getOriginalTable(
      ProjecTypeOrig
    );
    const permitStatusesOrig = await this._commonImport.getOriginalTable(
      PermitStatusOrig
    );
    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const structureComplianceStatuses = complianceStatuses.filter(
      c => c.entityType === 'Structure'
    );
    const states = await this._commonImport.getLookup(State, trx);

    const communities = await this._commonImport.getLookup(Community, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const watersheds = await this._commonImport.getLookup(Watershed, trx, {
      customerId: this.config.CUSTOMER_ID,
    });
    const receivingWaters = await this._commonImport.getLookup(
      ReceivingWater,
      trx,
      {
        customerId: this.config.CUSTOMER_ID,
      }
    );

    const inspecitonFrequenciesOrig = await this._commonImport.getOriginalTable(
      InspectionFrequencyOrig
    );
    const inspectionFrequencies = await this._commonImport.getLookup(
      InspectionFrequency,
      trx
    );
    const contacts = await this._commonImport.getContacts(trx);

    const importRecords = await Bluebird.each(
      originalStructures,
      async originalStructure => {
        let existingStructure = existingStructures.find(
          f => f.originalId === originalStructure.NumericID
        );
        if (existingStructure) {
          this.logger.warn(
            `Structure: "${existingStructure.name}" already exists.  Skipping.`,
            existingStructure
          );
          return;
        }
        const controlTypeOrig = controlTypesOrig.find(
          c => c.NumericID === originalStructure.ControlType
        );
        let project: Project = null;
        if (originalStructure.ProjectName) {
          project = await this._commonImport.createProjectIfNotExists(
            originalStructure.ProjectName,
            currentProjects,
            trx
          );
        }

        const community = communities.find(
          c => c.originalId === originalStructure.Community
        );
        const watershed = watersheds.find(
          w => !w.isSubWatershed && w.originalId === originalStructure.Watershed
        );
        const subWatershed = watersheds.find(
          w =>
            w.isSubWatershed && w.originalId === originalStructure.SubWatershed
        );
        const receivingWater = receivingWaters.find(
          rw => rw.originalId === originalStructure.ReceivingStream
        );

        const state = states.find(
          s =>
            s &&
            originalStructure.State &&
            ((s.abbr &&
              s.abbr.toLowerCase() === originalStructure.State.toLowerCase()) ||
              (s.name &&
                s.name.toLowerCase() === originalStructure.State.toLowerCase()))
        );

        let newStructure: Structure = Object.assign(new Structure(), <Partial<
          Structure
        >>{
          originalId: originalStructure.NumericID,
          customerId: this.config.CUSTOMER_ID,
          trackingId: originalStructure.TrackingID,
          dateAdded: originalStructure.Created,
          controlType: controlTypeOrig ? controlTypeOrig.ControlTypeName : null,
          name: originalStructure.ControlName,
          projectId: project ? project.id : null,

          physicalAddress1: originalStructure.Address,
          physicalCity: originalStructure.City,
          physicalState: state ? state.abbr : null,
          physicalZip: originalStructure.PostalCode,

          maintenanceAgreement: originalStructure.MaintenanceAgreement,
          lat: originalStructure.GISLatitude,
          lng: originalStructure.GISLongitude,

          communityId: community ? community.id : null,
          watershedId: watershed ? watershed.id : null,
          subwatershedId: subWatershed ? subWatershed.id : null,
          receivingWatersId: receivingWater ? receivingWater.id : null,
          latestInspectionDate: originalStructure.LastInspection,
        });

        let projecTypeOrig = projectTypesOrig.find(
          p => p.NumericID === originalStructure.ProjectType
        );
        if (projecTypeOrig && projecTypeOrig.ProjectTypeName) {
          if (
            ['residential', 'subdivision'].indexOf(
              projecTypeOrig.ProjectTypeName.toLowerCase()
            ) > -1
          ) {
            newStructure.projectType = projectTypes.residential;
          } else if (
            projecTypeOrig.ProjectTypeName === projectTypes.commercial
          ) {
            newStructure.projectType = projectTypes.commercial;
          } else if (
            projecTypeOrig.ProjectTypeName === projectTypes.municipal
          ) {
            newStructure.projectType = projectTypes.municipal;
          } else {
            this.logger.warn(
              `Structure: Project Type: could not map "${
                projecTypeOrig.ProjectTypeName
              }"`
            );
            newStructure.additionalInformation = [
              newStructure.additionalInformation,
              `Project Type: `,
            ].join('\r\n');
          }
        }

        this._handleStructurePermitStatus(
          originalStructure,
          newStructure,
          permitStatusesOrig
        );

        // inspectionFrequencyId
        const inspectionFreqOrig = inspecitonFrequenciesOrig.find(
          f => f.NumericID === originalStructure.InspectionFrequency
        );
        if (inspectionFreqOrig) {
          const newFreq = inspectionFrequencies.find(
            i => i.name === inspectionFreqOrig.InspectionFrequencyName
          );
          if (newFreq) {
            newStructure.inspectionFrequencyId = newFreq.id;
          }
        }

        // ComplianceStatus
        const noStatus = structureComplianceStatuses.find(
          c => c.name === 'No Status'
        );
        newStructure.complianceStatus = noStatus ? noStatus.name : null;

        if (originalStructure.LocationOnProperty) {
          newStructure.additionalInformation = [
            newStructure.additionalInformation,
            `Location on Property: ${originalStructure.LocationOnProperty}`,
          ].join('\r\n');
        }

        if (originalStructure.NextInspection) {
          newStructure.additionalInformation = [
            newStructure.additionalInformation,
            `Next Inspection: ${moment(originalStructure.NextInspection).format(
              ''
            )}`,
          ].join('\r\n');
        }

        const insertedStructure = await this.newKnex(dh.getTableName(Structure))
          .transacting(trx)
          .insert(objectToSqlData(Structure, newStructure, this.newKnex))
          .returning(
            dh.getSqlFields2(Structure, {
              includeInternalField: true,
              asProperty: true,
            })
          )
          .catch(this.logger.error)
          .get<Structure>(0);
        this.logger.verbose(
          `Structure: imported "${insertedStructure.name}"`,
          insertedStructure
        );

        // Custom Fields
        await this._commonImport.updateCustomFieldsFromCustomFieldSetup(
          Structure,
          insertedStructure.id,
          origStructureCFs,
          customFields,
          originalStructure,
          trx
        );

        // Files
        if (strucFileOwnerType) {
          const origFiles: BmpFileOrig[] = await this._commonImport
            .getOriginal(BmpFileOrig)
            .where(
              propertyNameFor<BmpFileOrig>(f => f.OwnerType),
              strucFileOwnerType.NumericID
            )
            .where(
              propertyNameFor<BmpFileOrig>(f => f.OwnerID),
              insertedStructure.originalId
            );
          await Bluebird.each(origFiles, async origFile => {
            this.logger.verbose(`Structure: File`, origFile);
            await this._commonImport.importFile(
              origFile,
              Structure,
              insertedStructure.id,
              trx
            );
          });
        }

        // Owner
        let existingOwnerContact = contacts.find(
          c =>
            c.contactType === 'Owner' &&
            c.company === originalStructure.Owner &&
            c.company === originalStructure.Owner &&
            c.mailingAddressLine1 === originalStructure.Address &&
            c.mailingZip === originalStructure.PostalCode
        );
        if (!existingOwnerContact) {
          const ownerContact = Object.assign(new Contact(), <Partial<Contact>>{
            contactType: 'Owner',
            customerId: this.config.CUSTOMER_ID,
            name: originalStructure.Owner,
            company: originalStructure.Owner,
            phone: originalStructure.Phonenumber,
            mobilePhone: originalStructure.CellPhone,
            fax: originalStructure.FaxNumber,
            mailingAddressLine1: originalStructure.Address,
            mailingCity: originalStructure.City,
            mailingState: state ? state.abbr : null,
            mailingZip: originalStructure.PostalCode,
          });
          existingOwnerContact = await this._commonImport.importContact(
            ownerContact,
            trx
          );

          contacts.push(existingOwnerContact);
          this.logger.verbose(
            `Structure: Owner Contact: Added name: "${
              existingOwnerContact.name
            }", company: ${existingOwnerContact.company}`
          );
        } else {
          this.logger.verbose(
            `Structure: Owner Contact: Found existing owner contact: "` +
              `${existingOwnerContact.name}", company: ${
                existingOwnerContact.company
              }`
          );
        }
        let entityContactId = await this._contactRepo.insertContactForEntity(
          Structure,
          insertedStructure.id,
          existingOwnerContact.id as number,
          this.config.CUSTOMER_ID,
          trx
        );
        this.logger.verbose(
          `Structure: Associated Owner contact to EntityContact id: ${entityContactId}`
        );
      }
    );

    return importRecords;
  }

  async importInspections(trx: Knex.Transaction) {
    const originalInspections: StructureInspectionOrig[] = await this.origKnex(
      dh.getTableName(StructureInspectionOrig)
    ).catch(this.logger.error);
    this.logger.info(
      `StructureInspection: Found ${originalInspections.length} to import.`
    );

    const complianceStatuses = await this._commonImport.getLookup(
      ComplianceStatus,
      trx
    );
    const structureComplianceStatuses = complianceStatuses.filter(
      c => c.entityType === 'Structure'
    );
    const noStatus = structureComplianceStatuses.find(
      s => s.name === 'No Status'
    );

    const fileOwnerTypes = await this._commonImport.getOriginalTable(
      FileOwnerTypeOrig
    );
    const strucFileOwnerType = fileOwnerTypes.find(
      f =>
        fileOwnerTypeToEntityMap[f.FileOwnerTypeName] ===
        EntityTypes.StructureInspection
    );
    if (!strucFileOwnerType) {
      this.logger.warn(`StructureInspection: Could not find FileOwnerType`);
    }

    const structures: Structure[] = await this.newKnex(
      dh.getTableName(Structure)
    )
      .transacting(trx)
      .where(
        Structure.getSqlField<Structure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(
        dh.getSqlFields2<Structure>(Structure, {
          includeInternalField: true,
          asProperty: true,
        })
      );

    const selectStructureId = this.newKnex(dh.getTableName(Structure))
      .transacting(trx)
      .where(
        Structure.getSqlField<Structure>(c => c.customerId),
        this.config.CUSTOMER_ID
      )
      .select(Structure.getSqlField<Structure>(c => c.id));
    const existingInspections: StructureInspection[] = await this.newKnex(
      dh.getTableName(StructureInspection)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(StructureInspection, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .whereIn(
        StructureInspection.getSqlField<StructureInspection>(
          c => c.structureId
        ),
        selectStructureId
      )
      .where(
        StructureInspection.getSqlField<StructureInspection>(c => c.isDeleted),
        0
      )
      .catch(this.logger.error);

    const strucInspectionType = await this._commonImport.getDefaultInspectionType(
      EntityTypes.StructureInspection,
      trx
    );

    const strucCft = await this._commonImport.getDefaultCustomFormTemplate(
      EntityTypes.StructureInspection,
      trx
    );

    const customFields: CustomField[] = await this.newKnex(
      dh.getTableName(CustomField)
    )
      .transacting(trx)
      .select(
        ...dh.getSqlFields2(CustomField, {
          includeInternalField: true,
          asProperty: true,
        })
      )
      .where({
        [CustomField.sqlField(f => f.customFormTemplateId)]: strucCft.id,
        [CustomField.sqlField(
          c => c.entityType
        )]: EntityTypes.StructureInspection,
        [CustomField.sqlField(c => c.customerId)]: this.config.CUSTOMER_ID,
        [CustomField.sqlField(c => c.isDeleted)]: 0,
      });

    await Bluebird.each(originalInspections, async originalInspection => {
      let existingInspection = existingInspections.find(
        f => f.originalId === originalInspection.NumericID
      );
      if (existingInspection) {
        this.logger.warn(
          `StructureInspection: OriginalId "${
            existingInspection.originalId
          }" already exists.  Skipping.`,
          existingInspection
        );
        return;
      }
      // find the Structure
      const structure = structures.find(
        cs => cs.originalId === originalInspection.Structure
      );

      if (!structure) {
        this.logger.error(
          `StructureInspection: Could not find original id: ${
            originalInspection.Structure
          }`
        );
        return;
      }
      let newInspection: StructureInspection = Object.assign(
        new StructureInspection(),
        <Partial<StructureInspection>>{
          originalId: originalInspection.NumericID,
          structureId: structure.id,
          inspectionTypeId: strucInspectionType.id,
          customFormTemplateId: strucCft.id,
          createdDate: this._commonImport.toUtcDate(originalInspection.Created),
          inspectionDate: this._commonImport.toUtcDate(
            originalInspection.InspectionDate
          ),
          dateResolved: this._commonImport.toUtcDate(
            originalInspection.DateResolved
          ),
          isControlActive: originalInspection.ControlIsActive,
          isBuiltWithinSpecification:
            originalInspection.BuiltWithinSpecification,
          isDepthOfSedimentAcceptable: originalInspection.DepthOfSediment,
          requiresMaintenance: originalInspection.RequiresMaintenance,
          requiresRepairs: originalInspection.RequiresRepairs,
          isStandingWaterPresent: originalInspection.StandingWater,
          areWashoutsPresent: originalInspection.WashoutsArePresent,
          floatablesRemovalRequired:
            originalInspection.FloatablesRemovalRequired,
          isOutletClogged: originalInspection.OutletIsClogged,
          isStructuralDamage: originalInspection.StructuralDamage,
          isErosionPresent: originalInspection.ErosionIsPresent,
          isDownstreamErosionPresent: originalInspection.DownstreamErosion,
          isIllicitDischargePresent: originalInspection.IllicitDischargePresent,
          isReturnInspectionRecommended:
            originalInspection.RecommendedReturnInspection,
          additionalInformation: originalInspection.Comments,
          complianceStatus: noStatus ? noStatus.name : null,
        }
      );

      if (originalInspection.Conversation) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Conversation: ${originalInspection.Conversation}`,
        ].join('\r\n');
      }
      if (originalInspection.EnforcedActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Enforced Actions: ${originalInspection.EnforcedActions}`,
        ].join('\r\n');
      }
      if (originalInspection.CorrectiveActions) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Corrective Actions: ${originalInspection.CorrectiveActions}`,
        ].join('\r\n');
      }

      if (originalInspection.Inspector) {
        newInspection.additionalInformation = [
          newInspection.additionalInformation,
          `Inspector: ${originalInspection.Inspector}`,
        ].join('\r\n');
      }

      const insertedInspection = await this.newKnex(
        dh.getTableName(StructureInspection)
      )
        .transacting(trx)
        .insert(
          objectToSqlData(StructureInspection, newInspection, this.newKnex)
        )
        .returning(
          dh.getSqlFields2(StructureInspection, {
            includeInternalField: true,
            asProperty: true,
          })
        )
        .catch(this.logger.error)
        .get<StructureInspection>(0);
      this.logger.verbose(
        `StructureInspection: imported "${insertedInspection.originalId}"`,
        insertedInspection
      );

      // Update Custom Fields for Custom Form Template
      await this._updateCustomFieldValues(
        insertedInspection,
        customFields,
        trx
      );

      // Handle Original Custom Fields
      await this._commonImport.updateCustomFieldsFromQuestions(
        StructureInspection,
        insertedInspection,
        insertedInspection.originalId,
        customFields,
        trx
      );

      // Files
      if (strucFileOwnerType) {
        const origFiles: BmpFileOrig[] = await this._commonImport
          .getOriginal(BmpFileOrig)
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerType),
            strucFileOwnerType.NumericID
          )
          .where(
            propertyNameFor<BmpFileOrig>(f => f.OwnerID),
            insertedInspection.originalId
          );
        await Bluebird.each(origFiles, async origFile => {
          this.logger.verbose(`StructureInspection: File`, origFile);
          await this._commonImport.importFile(
            origFile,
            StructureInspection,
            insertedInspection.id,
            trx
          );
        });
      }
    });
  }

  /**
   * Sets inspectionDate for all Structures to be the latest date from inspection
   *
   */
  async updateLatestInspectionDate(trx: Knex.Transaction) {
    const selectMaxInspectionDate = this.newKnex(
      StructureInspection.getTableName()
    )
      .transacting(trx)
      .max(
        StructureInspection.getSqlField<StructureInspection>(
          c => c.inspectionDate
        )
      )
      .whereRaw(`?? = ??`, [
        StructureInspection.getSqlField<StructureInspection>(
          i => i.structureId,
          true
        ),
        Structure.getSqlField<Structure>(c => c.id, true),
      ])
      .where(
        StructureInspection.getSqlField<StructureInspection>(
          i => i.inspectionDate,
          true
        ),
        '<=',
        new Date()
      )
      .whereIn(
        StructureInspection.getSqlField<StructureInspection>(
          i => i.structureId,
          true
        ),
        this.newKnex(Structure.getTableName())
          .select(Structure.getSqlField<Structure>(c => c.id))
          .where(
            Structure.getSqlField<Structure>(c => c.customerId),
            this.config.CUSTOMER_ID
          )
      );
    const updateQb = this.newKnex(Structure.getTableName())
      .transacting(trx)
      .update(
        Structure.getSqlField<Structure>(c => c.latestInspectionDate),
        this.newKnex.raw(selectMaxInspectionDate.toQuery()).wrap('(', ')') // knex doesnt' support querybuilder
      )
      .where(
        Structure.getSqlField<Structure>(c => c.customerId),
        this.config.CUSTOMER_ID
      );

    const rowCount = await updateQb;
    this.logger.info(
      `StructureInspection: updated last inspected date`,
      rowCount
    );
  }

  private async _updateCustomFieldValues(
    inspection: StructureInspection,
    customFields: CustomField[],
    trx: Knex.Transaction
  ) {
    const getCf = label =>
      customFields.find(
        f =>
          f.customFormTemplateId === inspection.customFormTemplateId &&
          f.fieldLabel === label
      );
    const cfValues: Partial<CustomFieldValue>[] = [
      {
        customFieldId: getCf('Date Resolved').id,
        globalIdFk: inspection.globalId,
        value: dateToStringOrNull(inspection.dateResolved),
      },
      {
        customFieldId: getCf('Is Control Active').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isControlActive),
      },
      {
        customFieldId: getCf('Are Washouts Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.areWashoutsPresent),
      },
      {
        customFieldId: getCf('Removal of Floatables Required').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.floatablesRemovalRequired),
      },
      {
        customFieldId: getCf('Is Built within Specification').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isBuiltWithinSpecification),
      },
      {
        customFieldId: getCf('Is Depth of Sediment Acceptable').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDepthOfSedimentAcceptable),
      },
      {
        customFieldId: getCf('Is Downstream Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isDownstreamErosionPresent),
      },
      {
        customFieldId: getCf('Is Erosion Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isErosionPresent),
      },
      {
        customFieldId: getCf('Is Illict Discharge Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isIllicitDischargePresent),
      },
      {
        customFieldId: getCf('Is Outlet Clogged').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isOutletClogged),
      },
      {
        customFieldId: getCf('Is Return Inspection Recommended').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isReturnInspectionRecommended),
      },
      {
        customFieldId: getCf('Is Standing Water Present').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isStandingWaterPresent),
      },
      {
        customFieldId: getCf('Has Structural Damage').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.isStructuralDamage),
      },
      {
        customFieldId: getCf('Requires Maintenance').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.requiresMaintenance),
      },
      {
        customFieldId: getCf('Requires Repairs').id,
        globalIdFk: inspection.globalId,
        value: booleanToString(inspection.requiresRepairs),
      },
    ];

    const rowsToInsert = cfValues.map(v =>
      objectToSqlData(CustomFieldValue, v, this.newKnex)
    );

    await this.newKnex
      .batchInsert(CustomFieldValue.tableName, rowsToInsert, 100)
      .transacting(trx);
  }

  _handleStructurePermitStatus(
    origStructure: StructureOrig,
    structure: Structure,
    originalPermitStatuses: PermitStatusOrig[]
  ) {
    const originalPermitStatus = originalPermitStatuses.find(
      ps => ps.NumericID === origStructure.PermitStatus
    );

    if (originalPermitStatus && originalPermitStatus.PermitStatusName) {
      const matchedPermitStatus = Object.keys(permitStatus).find(
        k =>
          permitStatus[k].toLowerCase() ===
          originalPermitStatus.PermitStatusName.toLowerCase()
      );
      if (matchedPermitStatus) {
        structure.permitStatus = permitStatus[matchedPermitStatus];
        this.logger.verbose(
          `Structure: PermitStatus: Found matching Permit Status: ${
            originalPermitStatus.PermitStatusName
          }`
        );
      } else {
        /*
          Per Ty:
            Compliant , Non-Compliant, Under Enforcement, Completed = Approved
            Terminated, Suspended = Other
        */
        if (
          _.includes(
            ['compliant', 'non-compliant', 'under enforcement', 'completed'],
            originalPermitStatus.PermitStatusName.toLowerCase()
          )
        ) {
          structure.permitStatus = permitStatus.approved;
        } else {
          structure.permitStatus = permitStatus.other;
        }

        // compliance status
        switch (originalPermitStatus.PermitStatusName.toLowerCase()) {
          case 'compliant':
            structure.complianceStatus = 'Compliant';
            break;
          case 'under enforcement':
            structure.complianceStatus = 'Notice of Violation';
            break;
          case 'non-compliant':
            structure.complianceStatus = 'Written Warning Citation';
            break;
        }

        // if not found then set additionalInfo
        structure.additionalInformation = [
          structure.additionalInformation,
          `Permit Status: ${originalPermitStatus.PermitStatusName}`,
        ].join('\r\n');
        this.logger
          .warn(`Structure: PermitStatus: Could not match PermitStatus "${
          originalPermitStatus.PermitStatusName
        }".
          Appended to additionalInformation`);
      }
    }
  }
}
