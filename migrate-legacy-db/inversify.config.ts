import * as Knex from 'knex';
import { Container } from 'inversify';
import * as winston from 'winston';
import * as fs from 'fs';
import { Migrater } from './migrater';
import * as moment from 'moment';
import * as minimist from 'minimist';
import {
  ContactRepository,
  CustomerRepository,
  CustomFieldRepository,
  CustomFormTemplateRepository,
  FileRepository,
  StructureControlTypeRepository,
} from '../src/data';

import * as dotenv from 'dotenv';

import {
  BmpImport,
  CommonImport,
  ConstructionSiteImport,
  FacilityImport,
  OutfallImport,
  StructureImport,
  CitizenReportImport,
  IllicitDischargeImport,
  TemplateImport,
  OutfallFileImport,
  ConstructionSiteFileImport,
  StructureFileImport,
} from './importers';
import * as mkdirp from 'mkdirp';
import { Config } from './models';
import { CustomTXGarland } from './custom/tx-garland';
import { FacilityFileImport } from './importers/facility-file-import';

const args = minimist(process.argv.slice(2));

const envResult: any = dotenv.config({
  path: args['config'] || process.cwd() + '/.env.migration.yml',
});

if (envResult.error) {
  console.error('config not found.  Please provide --config');
  process.exit();
}

const config = envResult.parsed;

const LOG_LEVEL = config.LOG_LEVEL || 'info';
const LOG_PATH = `${config.LOG_PATH || '.logs'}/${config.CUSTOMER_ID}`;

const stamp = moment().format('YYYY-MM-DD hh-mm-ss');

// make dir if not exists
if (!fs.existsSync(LOG_PATH)) {
  mkdirp.sync(LOG_PATH);
}
const logger = new winston.Logger({
  exitOnError: false,
  transports: [
    new winston.transports.Console({
      level: LOG_LEVEL,
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
    new winston.transports.File({
      name: 'default file log',
      level: LOG_LEVEL,
      json: false,
      filename: `${LOG_PATH}/migration-${config.CUSTOMER_ID}_${stamp}.log`,
      timestamp: true,
      prettyPrint: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
    new winston.transports.File({
      name: 'verbose file log',
      level: 'verbose',
      json: false,
      filename: `${LOG_PATH}/migration-${
        config.CUSTOMER_ID
      }_${stamp}_verbose.log`,
      timestamp: true,
      prettyPrint: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
    new winston.transports.File({
      name: 'warn file log',
      level: 'warn',
      json: false,
      filename: `${LOG_PATH}/migration-${config.CUSTOMER_ID}_${stamp}_warn.log`,
      timestamp: true,
      prettyPrint: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
    new winston.transports.File({
      name: 'file json',
      level: LOG_LEVEL,
      json: true,
      filename: `${LOG_PATH}/migration-${config.CUSTOMER_ID}_${stamp}.json`,
      timestamp: true,
      prettyPrint: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }),
  ],
});

const origKnex = Knex({
  client: 'mssql',
  debug: process.env.NODE_ENV === 'development',
  pool: { min: 0, max: 7 },
  connection: {
    host: config.ORIGINAL_DB_HOST,
    database: config.ORIGINAL_DB_NAME,
    user: config.ORIGINAL_DB_USER,
    password: config.ORIGINAL_DB_PASSWORD,
  },
});

const newKnex = Knex({
  client: 'mssql',
  debug: process.env.NODE_ENV === 'development',
  pool: { min: 0, max: 7 },
  connection: {
    host: config.DB_HOST,
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
  },
});

const container = new Container();
container.bind<Knex>('OriginalDb').toConstantValue(origKnex);
container.bind<Knex>('NewDb').toConstantValue(newKnex);
container.bind<Knex>(Knex).toConstantValue(newKnex);
container.bind<Config>('config').toConstantValue(config);
container.bind<winston.LoggerInstance>('logger').toConstantValue(logger);

container.bind<Migrater>(Migrater).toSelf();

// Repositories
container.bind<ContactRepository>(ContactRepository).toSelf();
container.bind<CustomerRepository>(CustomerRepository).toSelf();
container.bind(CustomFieldRepository).toSelf();
container
  .bind<CustomFormTemplateRepository>(CustomFormTemplateRepository)
  .toSelf();
container.bind<FileRepository>(FileRepository).toSelf();

container.bind(StructureControlTypeRepository).toSelf();

// imports
container.bind<CommonImport>(CommonImport).toSelf();
container.bind<BmpImport>(BmpImport).toSelf();
container.bind<ConstructionSiteImport>(ConstructionSiteImport).toSelf();
container.bind<FacilityImport>(FacilityImport).toSelf();
container.bind<OutfallImport>(OutfallImport).toSelf();
container.bind<StructureImport>(StructureImport).toSelf();
container.bind<CitizenReportImport>(CitizenReportImport).toSelf();
container.bind<IllicitDischargeImport>(IllicitDischargeImport).toSelf();
container.bind<TemplateImport>(TemplateImport).toSelf();

container.bind<FacilityFileImport>(FacilityFileImport).toSelf();
container.bind<OutfallFileImport>(OutfallFileImport).toSelf();

container.bind<StructureFileImport>(StructureFileImport).toSelf();

container.bind<ConstructionSiteFileImport>(ConstructionSiteFileImport).toSelf();

container.bind(CustomTXGarland).toSelf();

export { container };
