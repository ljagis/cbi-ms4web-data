import * as moment from 'moment';

/**
 * converts date representation to string for custom fields
 */
export function dateToStringOrNull(date: Date): string {
  if (!date) {
    return null;
  }
  const m = moment.utc(date);
  return m.isValid() ? m.toISOString() : null;
}

/**
 * converts boolean representation to string for custom fields
 */
export function booleanToString(b: boolean): string {
  if (b === true) {
    return 'true';
  }
  if (b === false) {
    return 'false';
  }
  return null;
}

/**
 * converts number representation to string for custom fields
 */
export function numberToString(num: number): string {
  if (num === null || num === undefined) {
    return null;
  }
  return num.toString();
}
