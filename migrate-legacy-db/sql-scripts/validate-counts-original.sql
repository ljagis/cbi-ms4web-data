select count (*), 'ConstructionSites' from ConstructionSites
select count (*), 'ConstructionSiteInspections' from ConstructionSiteInspections
select count (*), 'IndustrialFacilities' from IndustrialFacilities
select count (*), 'IndustrialFacilityInspections' from IndustrialFacilityInspections
select count (*), 'Outfalls' from Outfalls
select count (*), 'OutfallInspections' from OutfallInspections
select count (*), 'Structures' from Structures
select count (*), 'StructureInspections' from StructureInspections

select count (*), 'CitizensReports' from CitizensReports
select count (*), 'Illicits' from Illicits

select count (*), 'MinimumControlMeasures' from MinimumControlMeasures
select count (*), 'BestManagementPractices' from BestManagementPractices

select count (*), 'BMPImplementationTasks' from BMPImplementationTasks
union
select count (*), 'BMPData' from BMPData
union
select count (*), 'BMPMeasureableGoals' from BMPMeasureableGoals