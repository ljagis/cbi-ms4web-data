-- Validate counts of new database 

DECLARE @customerId NVARCHAR(MAX)= 'LOWELL_AR_2'

SELECT 'ConstructionSites' AS [Table],
       COUNT(*) AS [Count]
FROM     ConstructionSites
WHERE   CustomerId = @customerId
UNION
SELECT 'ConstructionSiteInspections' AS [Table],
       COUNT(*) AS [Count]
FROM     ConstructionSiteInspections
WHERE   ConstructionSiteId IN
(
    SELECT id
    FROM   ConstructionSites
    WHERE  CustomerId = @customerId
)
UNION
SELECT 'Facilities' AS [Table],
       COUNT(*) AS [Count]
FROM     Facilities
WHERE   CustomerId = @customerId
UNION
SELECT 'FacilityInspections' AS [Table],
       COUNT(*) AS [Count]
FROM     FacilityInspections
WHERE   FacilityId IN
(
    SELECT id
    FROM   Facilities
    WHERE  CustomerId = @customerId
)
UNION
SELECT 'Outfalls' AS [Table],
       COUNT(*) AS [Count]
FROM     Outfalls
WHERE   CustomerId = @customerId
UNION
SELECT 'OutfallInspections' AS [Table],
       COUNT(*) AS [Count]
FROM     OutfallInspections
WHERE   OutfallId IN
(
    SELECT id
    FROM   Outfalls
    WHERE  CustomerId = @customerId
)
UNION
SELECT 'Structures' AS [Table],
       COUNT(*) AS [Count]
FROM     Structures
WHERE   CustomerId = @customerId
UNION
SELECT 'StructureInspections' AS [Table],
       COUNT(*) AS [Count]
FROM     StructureInspections
WHERE   StructureId IN
(
    SELECT id
    FROM   Structures
    WHERE  CustomerId = @customerId
)
UNION
SELECT 'CitizenReports' AS [Table],
       COUNT(*) AS [Count]
FROM     CitizenReports
WHERE   CustomerId = @customerId
UNION
SELECT 'IllicitDischarges' AS [Table],
       COUNT(*) AS [Count]
FROM     IllicitDischarges
WHERE   CustomerId = @customerId
UNION
SELECT 'BmpControlMeasures' AS [Table],
       COUNT(*) AS [Count]
FROM     BmpControlMeasures
WHERE   CustomerId = @customerId
UNION
SELECT 'BmpTasks' AS [Table],
       COUNT(*) AS [Count]
FROM     BmpTasks
WHERE   CustomerId = @customerId
UNION
SELECT 'BmpActivities' AS [Table],
       COUNT(*) AS [Count]
FROM   BMPActivities
WHERE  CustomerId = @customerId