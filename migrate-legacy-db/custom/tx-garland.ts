import { injectable, inject } from 'inversify';
import * as Knex from 'knex';
import { QuestionOrig } from '../models/index';
import { getTableName } from '../../src/decorators';

import * as Bluebird from 'bluebird';
import * as _ from 'lodash';

@injectable()
export class CustomTXGarland {
  constructor(@inject('OriginalDb') private origKnex: Knex) {}

  async run() {
    await this.origKnex.transaction(async trx => {
      const allQuestions: QuestionOrig[] = await this.origKnex(
        getTableName(QuestionOrig)
      )
        .transacting(trx)
        .where('SiteSection', 'IndustrialFacilityInspection');

      const recordsGrouped = _.groupBy<QuestionOrig>(
        allQuestions,
        q => q.RecordID
      );

      await Bluebird.each(Object.keys(recordsGrouped), async id => {
        await this._updateQuestions(
          'Initial Inspection',
          recordsGrouped[id],
          trx
        );
        await this._updateQuestions(
          'Recheck Inspection',
          recordsGrouped[id],
          trx
        );
        await this._updateQuestions('Score', recordsGrouped[id], trx);
      });
    });

    await this.origKnex.destroy();
  }

  private async _updateQuestions(
    question: string,
    questions: QuestionOrig[],
    trx: Knex.Transaction
  ) {
    const filteredQuestions = questions.filter(f => f.Question === question);
    // the first one is commercial
    if (filteredQuestions[0]) {
      await this.origKnex(getTableName(QuestionOrig))
        .transacting(trx)
        .update('Question', `${question} - commercial`)
        .where('NumericId', filteredQuestions[0].NumericID);
    }
    // the second one is industrial
    if (filteredQuestions[1]) {
      await this.origKnex(getTableName(QuestionOrig))
        .transacting(trx)
        .update('Question', `${question} - industrial`)
        .where('NumericId', filteredQuestions[1].NumericID);
    }
  }
}
