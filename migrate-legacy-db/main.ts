import 'reflect-metadata';
import 'core-js/shim';
import * as Bluebird from 'bluebird';
import { Migrater } from './migrater';
import * as minimist from 'minimist';

let mssql = require('mssql');
mssql.Promise = Bluebird; // use Bluebird as promise

import { container } from './inversify.config';
import { CustomTXGarland } from './custom/tx-garland';

const migrater = container.get(Migrater);

const args = minimist(process.argv.slice(2));

if (args['val'] || args['validate']) {
  migrater.validateAll();
} else if (args['i'] || args['import']) {
  migrater.importAll();
} else if (args['importBmpFiles']) {
  migrater.importBmpFiles();
} else if (args['template']) {
  migrater.importFile(args['template']).then(() => migrater.destroy());
} else if (args['tx-garland']) {
  const txGarland = container.get(CustomTXGarland);
  txGarland.run();
} else {
  // tslint:disable-next-line
  console.warn(`Please run with args --validate or --import`);
}
